C---------------------------------------------------------------------
CL                  C9.3     STORAGE OF MATRIX BLOCKS
C VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
       COMMON //
     +   A, 	AALL
       COMPLEX
     +   A,     AALL,	AIO
       DIMENSION
     +   A(MDLENG),	
     +   AALL(1-MDSTORE+2*MDPOL*MDCOL*MDSTORE,1-MDSTORE+MDPSI*MDSTORE),
     +   RA(2*MDLENG),   AIO(2*MDPOL*MDCOL)
       EQUIVALENCE
     +   (RA(1),A(1)),   (AIO(1),A(1))
