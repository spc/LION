subroutine assign_code_parameters(codeparameters, return_status)

  !-----------------------------------------------------------------------
  ! calls the XML parser for the code parameters and assign the
  ! resulting values to the corresponding variables
  ! NEEDS TO HAVE VARIABLES IN "lower case"
  !TODO: check an alternative and more elegant solution in Perl
  !-----------------------------------------------------------------------

  use prec_const
  use itm_types
  use euitm_schemas
  use euitm_xml_parser
  use string_manipulation_tools 
  use globals

  implicit none

  type (type_param), intent(in) :: codeparameters
  integer(ikind), intent(out) :: return_status 

  type(tree) :: parameter_list
  type(element), pointer :: temp_pointer
  integer :: nparm
  integer :: i, n_values
  character(len = 132) :: cname
  !  integer(ikind) :: ns, NEQDXTPO

  interface
     subroutine scan_str2bool(str, cbool, nval)
       use itm_types
       character(len = *) :: str
       logical, dimension(:) :: cbool
       integer :: nval   
     end subroutine scan_str2bool
  end interface


  !-- set path to XML schema

  return_status = 0      ! no error

  !-- parse xml-string codeparameters%parameters

  call euitm_xml_parse(codeparameters, nparm, parameter_list)
  !  IF (NVERBOSE .GE. 2) print *,'codeparameters%parameters= ',codeparameters%parameters
  ! Note: NVERBOSE only known while going through input parameters, so next statement depedns only on default value
  IF (NVERBOSE .GE. 2) print *,' nparm= ',nparm

  !-- assign variables

  temp_pointer => parameter_list%first

  outer: do
     cname = char2str(temp_pointer%cname)   ! necessary for AIX
     IF (NVERBOSE .GE. 4) print *,'cname = ', trim(cname)
     select case (cname)
     case ("parameters")
        temp_pointer => temp_pointer%child
        cycle
     case ("COCOS_IN")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, COCOS_IN)
        ! IF (NVERBOSE .GE. 2) print *,' COCOS_IN= ',COCOS_IN
     case ("COCOS_OUT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, COCOS_OUT)
        ! IF (NVERBOSE .GE. 2) print *,' COCOS_OUT= ',COCOS_OUT
     case ("MANCMP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, MANCMP)
        ! IF (NVERBOSE .GE. 2) print *,' MANCMP= ',MANCMP
     case ("MEQ")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, MEQ)
        ! IF (NVERBOSE .GE. 2) print *,' MEQ= ',MEQ
     case ("MFL")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, MFL)
        ! IF (NVERBOSE .GE. 2) print *,' MFL= ',MFL
     case ("NANTSHEET")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NANTSHEET)
        ! IF (NVERBOSE .GE. 2) print *,' NANTSHEET= ',NANTSHEET
     case ("NANTYP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NANTYP)
        ! IF (NVERBOSE .GE. 2) print *,' NANTYP= ',NANTYP
     case ("NANT_ITM")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NANT_ITM)
        ! IF (NVERBOSE .GE. 2) print *,' NANT_ITM= ',NANT_ITM
     case ("NBCASE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NBCASE)
        ! IF (NVERBOSE .GE. 2) print *,' NBCASE= ',NBCASE
     case ("NBTYPE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NBTYPE)
        ! IF (NVERBOSE .GE. 2) print *,' NBTYPE= ',NBTYPE
     case ("NCHI")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NCHI)
        ! IF (NVERBOSE .GE. 2) print *,' NCHI= ',NCHI
     case ("NCOLMN")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NCOLMN)
        ! IF (NVERBOSE .GE. 2) print *,' NCOLMN= ',NCOLMN
     case ("NCONTR")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NCONTR)
        ! IF (NVERBOSE .GE. 2) print *,' NCONTR= ',NCONTR
     case ("NCUT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NCUT)
        ! IF (NVERBOSE .GE. 2) print *,' NCUT= ',NCUT
     case ("NDA")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDA)
        ! IF (NVERBOSE .GE. 2) print *,' NDA= ',NDA
     case ("NDARG")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDARG)
        ! IF (NVERBOSE .GE. 2) print *,' NDARG= ',NDARG
     case ("NDDEG")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDDEG)
        ! IF (NVERBOSE .GE. 2) print *,' NDDEG= ',NDDEG
     case ("NDENS")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDENS)
        ! IF (NVERBOSE .GE. 2) print *,' NDENS= ',NDENS
     case ("NDES")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDES)
        ! IF (NVERBOSE .GE. 2) print *,' NDES= ',NDES
     case ("NDLT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDLT)
        ! IF (NVERBOSE .GE. 2) print *,' NDLT= ',NDLT
     case ("NDS")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NDS)
        ! IF (NVERBOSE .GE. 2) print *,' NDS= ',NDS
     case ("NELDTTMP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NELDTTMP)
        ! IF (NVERBOSE .GE. 2) print *,' NELDTTMP= ',NELDTTMP
     case ("NELDTTMPCOR")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NELDTTMPCOR)
        ! IF (NVERBOSE .GE. 2) print *,' NELDTTMPCOR= ',NELDTTMPCOR
     case ("NFAKAP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NFAKAP)
        ! IF (NVERBOSE .GE. 2) print *,' NFAKAP= ',NFAKAP
     case ("NHARM")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NHARM)
        ! IF (NVERBOSE .GE. 2) print *,' NHARM= ',NHARM
     case ("NPLTYP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NPLTYP)
        ! IF (NVERBOSE .GE. 2) print *,' NPLTYP= ',NPLTYP
     case ("NPOL")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NPOL)
        ! IF (NVERBOSE .GE. 2) print *,' NPOL= ',NPOL
     case ("NPRNT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NPRNT)
        ! IF (NVERBOSE .GE. 2) print *,' NPRNT= ',NPRNT
     case ("NPSI")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NPSI)
        ! IF (NVERBOSE .GE. 2) print *,' NPSI= ',NPSI
     case ("NREAD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NREAD)
        ! IF (NVERBOSE .GE. 2) print *,' NREAD= ',NREAD
     case ("NRSPEC")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NRSPEC)
        ! IF (NVERBOSE .GE. 2) print *,' NRSPEC= ',NRSPEC
     case ("NRUN")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NRUN)
        ! IF (NVERBOSE .GE. 2) print *,' NRUN= ',NRUN
     case ("NSADDL")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NSADDL)
        ! IF (NVERBOSE .GE. 2) print *,' NSADDL= ',NSADDL
     case ("NSAVE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NSAVE)
        ! IF (NVERBOSE .GE. 2) print *,' NSAVE= ',NSAVE
     case ("NSOURC")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NSOURC)
        ! IF (NVERBOSE .GE. 2) print *,' NSOURC= ',NSOURC
     case ("NTEMP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NTEMP)
        ! IF (NVERBOSE .GE. 2) print *,' NTEMP= ',NTEMP
     case ("NTORSP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NTORSP)
        ! IF (NVERBOSE .GE. 2) print *,' NTORSP= ',NTORSP
     case ("NUMBER")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NUMBER)
        ! IF (NVERBOSE .GE. 2) print *,' NUMBER= ',NUMBER
     case ("NVERBOSE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NVERBOSE)
        ! IF (NVERBOSE .GE. 2) print *,' NVERBOSE= ',NVERBOSE
     case ("NVAC")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NVAC)
        ! IF (NVERBOSE .GE. 2) print *,' NVAC= ',NVAC
     case ("LENGTH")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, LENGTH)
        ! IF (NVERBOSE .GE. 2) print *,' LENGTH= ',LENGTH
     case ("NITMOPT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NITMOPT)
        ! IF (NVERBOSE .GE. 2) print *,' NITMOPT= ',NITMOPT
     case ("AHEIGT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, AHEIGT)
        ! IF (NVERBOSE .GE. 2) print *,' AHEIGT= ',AHEIGT
     case ("ALARG")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ALARG)
        ! IF (NVERBOSE .GE. 2) print *,' ALARG= ',ALARG
     case ("AMASSE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, AMASSE)
        ! IF (NVERBOSE .GE. 2) print *,' AMASSE= ',AMASSE
     case ("ANTRAD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ANTRAD)
        ! IF (NVERBOSE .GE. 2) print *,' ANTRAD= ',ANTRAD
     case ("ANTRADMAX")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ANTRADMAX)
        ! IF (NVERBOSE .GE. 2) print *,' ANTRADMAX= ',ANTRADMAX
     case ("ANTUP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ANTUP)
        ! IF (NVERBOSE .GE. 2) print *,' ANTUP= ',ANTUP
     case ("ANU")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ANU)
        ! IF (NVERBOSE .GE. 2) print *,' ANU= ',ANU
     case ("ARSIZE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ARSIZE)
        ! IF (NVERBOSE .GE. 2) print *,' ARSIZE= ',ARSIZE
     case ("ASPCT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ASPCT)
        ! IF (NVERBOSE .GE. 2) print *,' ASPCT= ',ASPCT
     case ("ASYMB")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ASYMB)
        ! IF (NVERBOSE .GE. 2) print *,' ASYMB= ',ASYMB
     case ("BNOT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, BNOT)
        ! IF (NVERBOSE .GE. 2) print *,' BNOT= ',BNOT
     case ("CENTE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, CENTE)
        ! IF (NVERBOSE .GE. 2) print *,' CENTE= ',CENTE
     case ("CPSRF")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, CPSRF)
        ! IF (NVERBOSE .GE. 2) print *,' CPSRF= ',CPSRF
     case ("DELTA")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, DELTA)
        ! IF (NVERBOSE .GE. 2) print *,' DELTA= ',DELTA
     case ("DELTAF")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, DELTAF)
        ! IF (NVERBOSE .GE. 2) print *,' DELTAF= ',DELTAF
     case ("ELLIPT")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, ELLIPT)
        ! IF (NVERBOSE .GE. 2) print *,' ELLIPT= ',ELLIPT
     case ("EPSMAC")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EPSMAC)
        ! IF (NVERBOSE .GE. 2) print *,' EPSMAC= ',EPSMAC
     case ("EQALFD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQALFD)
        ! IF (NVERBOSE .GE. 2) print *,' EQALFD= ',EQALFD
     case ("EQDENS")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQDENS)
        ! IF (NVERBOSE .GE. 2) print *,' EQDENS= ',EQDENS
     case ("EQFAST")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQFAST)
        ! IF (NVERBOSE .GE. 2) print *,' EQFAST= ',EQFAST
     case ("EQKAPD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQKAPD)
        ! IF (NVERBOSE .GE. 2) print *,' EQKAPD= ',EQKAPD
     case ("EQKPTE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQKPTE)
        ! IF (NVERBOSE .GE. 2) print *,' EQKPTE= ',EQKPTE
     case ("EQTE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, EQTE)
        ! IF (NVERBOSE .GE. 2) print *,' EQTE= ',EQTE
     case ("FEEDUP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, FEEDUP)
        ! IF (NVERBOSE .GE. 2) print *,' FEEDUP= ',FEEDUP
     case ("FREQCY")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, FREQCY)
        ! IF (NVERBOSE .GE. 2) print *,' FREQCY= ',FREQCY
     case ("OMEGA")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, OMEGA)
        ! IF (NVERBOSE .GE. 2) print *,' OMEGA= ',OMEGA
     case ("QIAXE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, QIAXE)
        ! IF (NVERBOSE .GE. 2) print *,' QIAXE= ',QIAXE
     case ("RMAJOR")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, RMAJOR)
        ! IF (NVERBOSE .GE. 2) print *,' RMAJOR= ',RMAJOR
     case ("SAMIN")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, SAMIN)
        ! IF (NVERBOSE .GE. 2) print *,' SAMIN= ',SAMIN
     case ("SAMAX")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, SAMAX)
        ! IF (NVERBOSE .GE. 2) print *,' SAMAX= ',SAMAX
     case ("SIGMA")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, SIGMA)
        ! IF (NVERBOSE .GE. 2) print *,' SIGMA= ',SIGMA
     case ("THANTW")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, THANTW)
        ! IF (NVERBOSE .GE. 2) print *,' THANTW= ',THANTW
     case ("VBIRTH")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, VBIRTH)
        ! IF (NVERBOSE .GE. 2) print *,' VBIRTH= ',VBIRTH
     case ("WALRAD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, WALRAD)
        ! IF (NVERBOSE .GE. 2) print *,' WALRAD= ',WALRAD
     case ("WNTDEL")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, WNTDEL)
        ! IF (NVERBOSE .GE. 2) print *,' WNTDEL= ',WNTDEL
     case ("WNTORO")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, WNTORO)
        ! IF (NVERBOSE .GE. 2) print *,' WNTORO= ',WNTORO
     case ("NLCOLD")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLCOLD)
        ! IF (NVERBOSE .GE. 2) print *,' NLCOLD= ',NLCOLD
     case ("NLCOLE")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLCOLE)
        ! IF (NVERBOSE .GE. 2) print *,' NLCOLE= ',NLCOLE
     case ("NLDIP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLDIP)
        ! IF (NVERBOSE .GE. 2) print *,' NLDIP= ',NLDIP
     case ("NLDISO")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLDISO)
        ! IF (NVERBOSE .GE. 2) print *,' NLDISO= ',NLDISO
     case ("NLPHAS")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLPHAS)
        ! IF (NVERBOSE .GE. 2) print *,' NLPHAS= ',NLPHAS
     case ("NLFAST")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLFAST)
        ! IF (NVERBOSE .GE. 2) print *,' NLFAST= ',NLFAST
     case ("NLOTP0")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLOTP0)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP0= ',NLOTP0
     case ("NLTTMP")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, NLTTMP)
        ! IF (NVERBOSE .GE. 2) print *,' NLTTMP= ',NLTTMP
        !
        !
        ! Arrays
     case ("MPOLWN")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), MPOLWN, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' MPOLWN= ',MPOLWN
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NITMRUN")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), NITMRUN, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NITMRUN= ',NITMRUN
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NITMSHOT")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), NITMSHOT, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NITMSHOT= ',NITMSHOT
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("ACHARG")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), ACHARG, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' ACHARG= ',ACHARG
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("AD")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), AD, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' AD= ',AD
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("AMASS")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), AMASS, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' AMASS= ',AMASS
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("ANGLET")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), ANGLET, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' ANGLET= ',ANGLET
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("ATE")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), ATE, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' ATE= ',ATE
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("ATI")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), ATI, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' ATI= ',ATI
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("ATIP")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), ATIP, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' ATIP= ',ATIP
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CEN0")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CEN0, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CEN0= ',CEN0
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CENDEN")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CENDEN, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CENDEN= ',CENDEN
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CENTI")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CENTI, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CENTI= ',CENTI
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CENTIP")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CENTIP, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CENTIP= ',CENTIP
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CEOMCI")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CEOMCI, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CEOMCI= ',CEOMCI
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CURASY")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CURASY, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CURASY= ',CURASY
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("CURSYM")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), CURSYM, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' CURSYM= ',CURSYM
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("EQKAPF")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), EQKAPF, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' EQKAPF= ',EQKAPF
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("EQKAPT")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), EQKAPT, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' EQKAPT= ',EQKAPT
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("EQTI")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), EQTI, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' EQTI= ',EQTI
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("FRAC")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), FRAC, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' FRAC= ',FRAC
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("FRCEN")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), FRCEN, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' FRCEN= ',FRCEN
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("FRDEL")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), FRDEL, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' FRDEL= ',FRDEL
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("THANT")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), THANT, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' THANT= ',THANT
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("TIME_ITM")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2num(char2str(temp_pointer%cvalue), TIME_ITM, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' TIME_ITM= ',TIME_ITM
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLOTP1")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLOTP1, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP1= ',NLOTP1
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLOTP2")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLOTP2, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP2= ',NLOTP2
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLOTP3")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLOTP3, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP3= ',NLOTP3
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLOTP4")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLOTP4, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP4= ',NLOTP4
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLOTP5")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLOTP5, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLOTP5= ',NLOTP5
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values
     case ("NLPLO5")
        ! array
        if (allocated(temp_pointer%cvalue)) &
             call scan_str2bool(char2str(temp_pointer%cvalue), NLPLO5, n_values)
        ! IF (NVERBOSE .GE. 2) print *,' NLPLO5= ',NLPLO5
        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values

     case ("USE_FAST_POPULATIONS")
        ! scalar, boolean
        if (allocated(temp_pointer%cvalue)) &
             call char2bool(temp_pointer%cvalue, USE_FAST_POPULATIONS)


     case ("comments")
        ! char array
        if (allocated(temp_pointer%cvalue)) &
             comments = char2str(temp_pointer%cvalue)
        n_values = size(comments)
        ! n_values = len(comments)
        do i=1,n_values
           ! IF (NVERBOSE .GE. 2) print *,'comments(',i,') = ',comments(i)
        end do
        ! IF (NVERBOSE .GE. 2) print *,' size(comments)= ',size(comments)
        ! IF (NVERBOSE .GE. 2) print *,' len(comments)= ',len(comments)


!!$     case ("treeitm")
!!$        ! array
!!$        if (allocated(temp_pointer%cvalue)) &
!!$             treeitm = char2str(temp_pointer%cvalue)
!!$        n_values = len(treeitm)
!!$        ! IF (NVERBOSE .GE. 2) print *,' treeitm= ',treeitm
!!$        ! IF (NVERBOSE .GE. 2) print *,' n_values= ',n_values

     case default
        write(*, *) 'ERROR: invalid parameter', cname
        return_status = 1
        !       exit
     end select
     do
        if (associated(temp_pointer%sibling)) then
           temp_pointer => temp_pointer%sibling
           exit
        end if
        if (associated(temp_pointer%parent, parameter_list%first )) &
             exit outer
        if (associated(temp_pointer%parent)) then
           temp_pointer => temp_pointer%parent
        else
           write(*, *) 'ERROR: broken list.'
           return
        end if
     end do
  end do outer

  !-- destroy tree
  call destroy_xml_tree(parameter_list)

  return

end subroutine assign_code_parameters



subroutine scan_str2bool(str, cbool, nval)
  !  Scans string str for logical cbool, separated 
  !  by blanks, and returns nval parameters in value.
  !  Converts text to numbers by internal Fortran READ
  use itm_types
  use string_manipulation_tools 
  implicit none

  character(len = *) :: str
  logical, dimension(:) :: cbool
  integer :: nval   

  character(len = len(str)) :: cval
  logical :: bool
  integer :: lenarr, i, ie, ios

  cval = str
  lenarr = size(cbool)
  nval = 0
  ! scan string cval
  do i = 1, lenarr
     cval = adjustl(trim(cval))               ! remove blanks
     if (cval == '') then
        exit                                   ! exit if empty string
     end if
     ie = scan(cval,' ')                      ! look for end of first token
     if (ie < 1) ie = len_trim(cval)
     read(cval(1 : ie), *, iostat = ios) bool  ! convert to real*8
     if (ios /= 0) exit                       ! exit if any read error
     nval = nval + 1  
     cbool(nval) = bool
     cval = adjustl(cval(ie + 1 : ))          ! cut out value just found 
  end do

end subroutine scan_str2bool
