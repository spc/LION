#!/bin/sh 

cwd_testcases=$PWD
echo $cwd_testcases
rundir=/tmp/$USER/LION_standalone_test
mkdir $rundir
cp chease_namelist_for_LION_standalone_100 $rundir/chease_namelist
cp EQDSK_COCOS_02_POS.OUT.ITM_92436_270 $rundir/EXPEQ
cp LION_namelist $rundir/
cd $rundir
chease > o.EQDSK_COCOS_02_POS.OUT.ITM_92436_270_forLION #  (or chease)
cp NSAVE TAPE8
sed s/NPRNT.\*=.\*9,/NPRNT\ \ \ \ \ =\ \ \ \ \ 6,/ TAPE8 > TAPE8a; mv TAPE8a TAPE8
cp MEQ TAPE4
cp NDES TAPE16
cp NVAC TAPE17
cp LION_namelist o.LION_standalone
$cwd_testcases/../src_f90/LION_prog >> o.LION_standalone
cat fort.9 >> o.LION_standalone
cd -
