C---------------------------------------------------------------------
CL                  C3.3     AUXILIARY VARIABLES FOR LION3 AND 4
C VERSION 11     LDV      MAY 1991           CRPP LAUSANNE
C
       COMMON/COMAUX/
     +   CONA(6,6),  REACS,    RQB,      RTB,
     +   VAC(MDOVL), XC1(8),   XETA(5),  XF(16),   XKSI(5),  XM(6,6),
     +   XVETA(6),   XVKSI(6),
     +   NPLAC(6),   NLQUAD
       COMPLEX
     +   CONA,     REACS,    VAC,      XC1,      XETA,
     +   XKSI,     XM,       XVETA,    XVKSI
       LOGICAL
     +   NLQUAD
