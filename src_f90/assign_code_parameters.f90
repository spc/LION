module assign_code_parameters_module

  use globals, only: RKIND
  use xml2eg_mdl, only: xml2eg_parse_memory, xml2eg_get, &
       type_xml2eg_document, xml2eg_free_doc

  implicit none

  interface xml_get
     module procedure xml_get_int
     module procedure xml_get_real
     module procedure xml_get_logical
     module procedure xml_get_real_vector
     module procedure xml_get_integer_vector
     module procedure xml_get_logical_vector
  end interface xml_get

contains

  !> Assign LION xml input parameters to global variables
  subroutine assign_params(parameters, return_status)
    use globals

    ! Input/Output
    character(len=132), intent(in) :: parameters(:)
    integer, intent(out) :: return_status

    ! Internal
    type(type_xml2eg_document) :: doc
    logical :: execution_error

    call xml2eg_parse_memory( parameters , doc )
    !
    ! ual
    call xml_get(doc , 'ual/NITMOPT'   , NITMOPT   , execution_error)
    call xml_get(doc , 'ual/NITMSHOT'  , NITMSHOT  , execution_error)
    call xml_get(doc , 'ual/NITMRUN'   , NITMRUN   , execution_error)
    call xml_get(doc , 'ual/TIME_ITM'  , TIME_ITM  , execution_error)
    call xml_get(doc , 'ual/COCOS_IN'  , COCOS_IN  , execution_error)
    call xml_get(doc , 'ual/COCOS_OUT' , COCOS_OUT , execution_error)
    call xml_get(doc , 'ual/NANT_ITM'  , NANT_ITM  , execution_error)
    call xml_get(doc , 'ual/USE_FAST_POPULATIONS', USE_FAST_POPULATIONS, execution_error)
    !
    ! equilibrium
    call xml_get(doc , 'equilibrium/RMAJOR' , RMAJOR , execution_error)
    call xml_get(doc , 'equilibrium/ASPCT'  , ASPCT  , execution_error)
    call xml_get(doc , 'equilibrium/ELLIPT' , ELLIPT , execution_error)
    call xml_get(doc , 'equilibrium/BNOT'   , BNOT   , execution_error)
    call xml_get(doc , 'equilibrium/QIAXE'  , QIAXE  , execution_error)
    call xml_get(doc , 'equilibrium/CPSRF'  , CPSRF  , execution_error)
    !
    ! species
    call xml_get(doc , 'species/NRSPEC'  , NRSPEC , execution_error)
    call xml_get(doc , 'species/AMASS'   , AMASS  , execution_error)
    call xml_get(doc , 'species/ACHARG'  , ACHARG , execution_error)
    call xml_get(doc , 'species/CEOMCI'  , CEOMCI , execution_error)
    call xml_get(doc , 'species/AMASSE'  , AMASSE , execution_error)
    !
    ! kinetics, selection
    call xml_get(doc , 'kinetics/NDENS'  , NDENS  , execution_error)
    call xml_get(doc , 'kinetics/NTEMP'  , NTEMP  , execution_error)
    call xml_get(doc , 'kinetics/NLFAST' , NLFAST , execution_error)
    ! kinetics, density
    call xml_get(doc , 'kinetics/CENDEN' , CENDEN , execution_error)
    call xml_get(doc , 'kinetics/EQDENS' , EQDENS , execution_error)
    call xml_get(doc , 'kinetics/EQKAPD' , EQKAPD , execution_error)
    call xml_get(doc , 'kinetics/EQALFD' , EQALFD , execution_error)
    call xml_get(doc , 'kinetics/AD'     , AD     , execution_error)
    call xml_get(doc , 'kinetics/CEN0'   , CEN0   , execution_error)
    call xml_get(doc , 'kinetics/FRAC'   , FRAC   , execution_error)
    call xml_get(doc , 'kinetics/FRCEN'  , FRCEN  , execution_error)
    call xml_get(doc , 'kinetics/FRDEL'  , FRDEL  , execution_error)
    call xml_get(doc , 'kinetics/NDARG'  , NDARG  , execution_error)
    call xml_get(doc , 'kinetics/NDDEG'  , NDDEG  , execution_error)

    ! kinetics, electron temperature
    call xml_get(doc , 'kinetics/CENTE'  , CENTE  , execution_error)
    call xml_get(doc , 'kinetics/EQTE'   , EQTE   , execution_error)
    call xml_get(doc , 'kinetics/EQKPTE' , EQKPTE , execution_error)
    call xml_get(doc , 'kinetics/ATE'    , ATE    , execution_error)

    ! kinetics, ion temperature
    call xml_get(doc , 'kinetics/CENTI'  , CENTI  , execution_error)
    call xml_get(doc , 'kinetics/CENTIP' , CENTIP , execution_error)
    call xml_get(doc , 'kinetics/EQTI'   , EQTI   , execution_error)
    call xml_get(doc , 'kinetics/EQKAPT' , EQKAPT , execution_error)
    call xml_get(doc , 'kinetics/ATI'    , ATI    , execution_error)
    call xml_get(doc , 'kinetics/ATIP'   , ATIP   , execution_error)
    call xml_get(doc , 'kinetics/SIGMA'  , SIGMA  , execution_error)

    ! kinetics, fast
    call xml_get(doc , 'kinetics/EQKAPF' , EQKAPF , execution_error)
    call xml_get(doc , 'kinetics/EQFAST' , EQFAST , execution_error)
    call xml_get(doc , 'kinetics/VBIRTH' , VBIRTH , execution_error)
    call xml_get(doc , 'kinetics/NFAKAP' , NFAKAP , execution_error)

    ! antenna
    call xml_get(doc , 'antenna/NANTYP'    , NANTYP    , execution_error)
    call xml_get(doc , 'antenna/NSADDL'    , NSADDL    , execution_error)
    call xml_get(doc , 'antenna/NLDIP'     , NLDIP     , execution_error)
    call xml_get(doc , 'antenna/NLPHAS'    , NLPHAS    , execution_error)
    call xml_get(doc , 'antenna/ANTRAD'    , ANTRAD    , execution_error)
    call xml_get(doc , 'antenna/ANTRADMAX' , ANTRADMAX , execution_error)
    call xml_get(doc , 'antenna/ANTUP'     , ANTUP     , execution_error)
    call xml_get(doc , 'antenna/NANTSHEET' , NANTSHEET , execution_error)
    call xml_get(doc , 'antenna/WALRAD'    , WALRAD    , execution_error)
    call xml_get(doc , 'antenna/NRUN'      , NRUN      , execution_error)
    call xml_get(doc , 'antenna/MANCMP'    , MANCMP    , execution_error)
    call xml_get(doc , 'antenna/MPOLWN'    , MPOLWN    , execution_error)
    call xml_get(doc , 'antenna/FREQCY'    , FREQCY    , execution_error)
    call xml_get(doc , 'antenna/OMEGA'     , OMEGA     , execution_error)
    call xml_get(doc , 'antenna/DELTAF'    , DELTAF    , execution_error)
    call xml_get(doc , 'antenna/WNTORO'    , WNTORO    , execution_error)
    call xml_get(doc , 'antenna/WNTDEL'    , WNTDEL    , execution_error)
    call xml_get(doc , 'antenna/NTORSP'    , NTORSP    , execution_error)
    call xml_get(doc , 'antenna/THANT'     , THANT     , execution_error)
    call xml_get(doc , 'antenna/THANTW'    , THANTW    , execution_error)
    call xml_get(doc , 'antenna/CURASY'    , CURASY    , execution_error)
    call xml_get(doc , 'antenna/CURSYM'    , CURSYM    , execution_error)
    call xml_get(doc , 'antenna/FEEDUP'    , FEEDUP    , execution_error)
    call xml_get(doc , 'antenna/SAMIN'     , SAMIN     , execution_error)
    call xml_get(doc , 'antenna/SAMAX'     , SAMAX     , execution_error)

    ! models
    call xml_get(doc , 'models/MEQ'        , MEQ       , execution_error)
    call xml_get(doc , 'models/NLCOLD'     , NLCOLD    , execution_error)
    call xml_get(doc , 'models/NLCOLE'     , NLCOLE    , execution_error)
    call xml_get(doc , 'models/NELDTTMP'   , NELDTTMP  , execution_error)
    call xml_get(doc , 'models/NELDTTMPCOR', NELDTTMPCOR, execution_error)
    call xml_get(doc , 'models/NLTTMP'    , NLTTMP , execution_error)
    call xml_get(doc , 'models/NHARM'     , NHARM  , execution_error)
    call xml_get(doc , 'models/NBCASE'    , NBCASE , execution_error)
    call xml_get(doc , 'models/NBTYPE'    , NBTYPE , execution_error)
    call xml_get(doc , 'models/ANU'       , ANU    , execution_error)
    call xml_get(doc , 'models/DELTA'     , DELTA  , execution_error)
    call xml_get(doc , 'models/EPSMAC'    , EPSMAC , execution_error)

    ! numerics
    call xml_get(doc , 'numerics/NLDISO' , NLDISO  , execution_error)
    call xml_get(doc , 'numerics/NVAC'   , NVAC    , execution_error)
    call xml_get(doc , 'numerics/NDLT'   , NDLT    , execution_error)
    call xml_get(doc , 'numerics/NDS'    , NDS     , execution_error)
    call xml_get(doc , 'numerics/LENGTH' , LENGTH  , execution_error)

    ! output
    call xml_get(doc , 'output/NVERBOSE' , NVERBOSE, execution_error)
    call xml_get(doc , 'output/NLOTP0'   , NLOTP0  , execution_error)
    call xml_get(doc , 'output/NLOTP1'   , NLOTP1  , execution_error)
    call xml_get(doc , 'output/NLOTP2'   , NLOTP2  , execution_error)
    call xml_get(doc , 'output/NLOTP3'   , NLOTP3  , execution_error)
    call xml_get(doc , 'output/NLOTP4'   , NLOTP4  , execution_error)
    call xml_get(doc , 'output/NLOTP5'   , NLOTP5  , execution_error)
    call xml_get(doc , 'output/NLPLO5'   , NLPLO5  , execution_error)
    call xml_get(doc , 'output/NPRNT'    , NPRNT   , execution_error)
    call xml_get(doc , 'output/NSAVE'    , NSAVE   , execution_error)
    call xml_get(doc , 'output/NDA'      , NDA     , execution_error)
    call xml_get(doc , 'output/NSOURC'   , NSOURC  , execution_error)
    call xml_get(doc , 'output/NCHI'     , NCHI    , execution_error)
    call xml_get(doc , 'output/NCOLMN'   , NCOLMN  , execution_error)
    call xml_get(doc , 'output/NCONTR'   , NCONTR  , execution_error)
    call xml_get(doc , 'output/NCUT'     , NCUT    , execution_error)
    call xml_get(doc , 'output/NDES'     , NDES    , execution_error)
    call xml_get(doc , 'output/NPLTYP'   , NPLTYP  , execution_error)
    call xml_get(doc , 'output/NPOL'     , NPOL    , execution_error)
    call xml_get(doc , 'output/NPSI'     , NPSI    , execution_error)
    call xml_get(doc , 'output/ANGLET'   , ANGLET  , execution_error)
    call xml_get(doc , 'output/AHEIGT'   , AHEIGT  , execution_error)
    call xml_get(doc , 'output/ALARG'    , ALARG   , execution_error)
    call xml_get(doc , 'output/ARSIZE'   , ARSIZE  , execution_error)
    call xml_get(doc , 'output/ASYMB'    , ASYMB   , execution_error)
    call xml_get(doc , 'output/MFL'      , MFL     , execution_error)
    call xml_get(doc , 'output/NREAD'    , NREAD   , execution_error)
    call xml_get(doc , 'output/NUMBER'   , NUMBER  , execution_error)
    !call xml2eg_get(doc , 'comments'  ,comments  , execution_error)

    return_status = 0

  end subroutine assign_params


  ! Translate a char-array into a string
  function char2str(carray) result(cstring)
    implicit none
    character, dimension(:) :: carray
    character(len=:), allocatable :: cstring
    integer :: i
    allocate( character(len=size(carray)) :: cstring )
    do i = 1, size(carray)
      cstring(i : i) = carray(i)
    end do
    return
  end function char2str

  ! Translate a char-array into a boolean
  subroutine char2bool(carray, cbool)
    implicit none
    character, dimension(:) :: carray
    logical :: cbool
    character(len=:), allocatable :: cstring
    cstring = char2str(carray)
    read(cstring, *) cbool
    deallocate(cstring)
  end subroutine char2bool

  !> Count the number of word in a string. 
  !> Each word is separated by one or several blanks.
  subroutine count_words(str, nval)
    implicit none

    character(len = *), intent(in) :: str
    integer, intent(out) :: nval   

    character(len = len(str)) :: cval
    integer :: lenarr, i, ie, ios

    cval = str
    nval = 0
    ! scan string cval
    i=1
    do while (.true.)
       i=i+1
       cval = adjustl(trim(cval))               ! remove blanks
       if (cval == '') then
          exit                                   ! exit if empty string
       end if
       ie = scan(cval,' ')                      ! look for end of first token
       if (ie < 1) ie = len_trim(cval)
       nval = nval + 1  
       cval = adjustl(cval(ie + 1 : ))          ! cut out value just found 
    end do
  end subroutine count_words


  !>  Scans string str for logical cbool, separated 
  !>  by blanks, and returns nval parameters in value.
  !>  Converts text to numbers by internal Fortran READ
  subroutine scan_str2bool(str, cbool, nval)
    implicit none

    character(len = *) :: str
    logical, dimension(:) :: cbool
    integer :: nval   

    character(len = len(str)) :: cval
    logical :: bool
    integer :: lenarr, i, ie, ios

    cval = str
    lenarr = size(cbool)
    nval = 0
    ! scan string cval
    do i = 1, lenarr
       cval = adjustl(trim(cval))               ! remove blanks
       if (cval == '') then
          exit                                   ! exit if empty string
       end if
       ie = scan(cval,' ')                      ! look for end of first token
       if (ie < 1) ie = len_trim(cval)
       read(cval(1 : ie), *, iostat = ios) bool  ! convert to real*8
       if (ios /= 0) exit                       ! exit if any read error
       nval = nval + 1  
       cbool(nval) = bool
       cval = adjustl(cval(ie + 1 : ))          ! cut out value just found 
    end do
  end subroutine scan_str2bool


  !> Wrapper around xml2eg_get (integer), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_int(doc, path, out, execution_error)
    type(type_xml2eg_document), intent(in) :: doc
    character(*), intent(in) :: path
    integer, intent(inout) :: out
    logical, intent(out) :: execution_error
    integer :: tmp
    call xml2eg_get(doc, path, tmp, execution_error)
    if (.not. execution_error) out = tmp
  end subroutine xml_get_int

  !> Wrapper around xml2eg_get (double), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_real(doc, path, out, execution_error)
    type(type_xml2eg_document), intent(in) :: doc
    character(*), intent(in) :: path
    real(RKIND), intent(inout) :: out
    logical, intent(out) :: execution_error
    real(RKIND) :: tmp
    call xml2eg_get(doc, path, tmp, execution_error)
    if (.not. execution_error) out = tmp
  end subroutine xml_get_real

  !> Wrapper around xml2eg_get (logical), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_logical(doc, path, out, execution_error)
    type(type_xml2eg_document), intent(in) :: doc
    character(*), intent(in) :: path
    logical, intent(inout) :: out
    logical, intent(out) :: execution_error
    logical :: tmp
    call xml2eg_get(doc, path, tmp, execution_error)
    if (.not. execution_error) out = tmp
  end subroutine xml_get_logical

  !> Wrapper around xml2eg_get (integer-vector), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_integer_vector(doc, path, out, execution_error)
    type(type_xml2eg_document) :: doc
    character(*), intent(in) :: path
    integer, intent(inout) :: out(:)
    logical, intent(out) :: execution_error
    character(1024) :: tmp_str
    integer :: tmp(size(out))
    integer :: nvec
    call xml2eg_get(doc, path, tmp_str, execution_error)
    if (.not. execution_error) then
       call count_words(tmp_str, nvec)
       nvec = min( nvec , size(out) )
       call xml2eg_get(doc, path, tmp(1:nvec), execution_error)
       if (.not. execution_error) out(1:nvec) = tmp(1:nvec)
    end if
  end subroutine xml_get_integer_vector

  !> Wrapper around xml2eg_get (real-vector), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_real_vector(doc, path, out, execution_error)
    type(type_xml2eg_document) :: doc
    character(*), intent(in) :: path
    real(RKIND), intent(inout) :: out(:)
    logical, intent(out) :: execution_error
    character(1024) :: tmp_str
    real(RKIND) :: tmp(size(out))
    integer :: nvec
    call xml2eg_get(doc, path, tmp_str, execution_error)
    if (.not. execution_error) then
       call count_words(tmp_str, nvec)
       nvec = min( nvec , size(out) )
       call xml2eg_get(doc, path, tmp(1:nvec), execution_error)
       if (.not. execution_error) out(1:nvec) = tmp(1:nvec)
    end if
  end subroutine xml_get_real_vector

  !> Wrapper around xml2eg_get (logical-vector), but here the 
  !> result is INTENT(INOUT) and result is only provided 
  !> if the result is successfully written.
  subroutine xml_get_logical_vector(doc, path, out, execution_error)
    type(type_xml2eg_document) :: doc
    character(*), intent(in) :: path
    logical, intent(inout) :: out(:)
    logical, intent(out) :: execution_error
    character(1024) :: tmp_str
    integer :: nvec

    call xml2eg_get(doc, path, tmp_str, execution_error)
    if (.not. execution_error) then
       call scan_str2bool(tmp_str, out, nvec)
    end if
  end subroutine xml_get_logical_vector

end module assign_code_parameters_module
