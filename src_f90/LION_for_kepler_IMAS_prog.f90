program LION_for_kepler_IMAS_prog

  use ids_schemas
  use ids_routines
  use f90_file_reader, only: file2buffer

  implicit none

  INTERFACE
     SUBROUTINE LION_IMAS(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
       use ids_schemas
       ! Input
       type(ids_equilibrium),      intent(in)  :: equil_in
       type(ids_core_profiles),    intent(in)  :: coreprof_in
       type(ids_ic_antennas),      intent(in)  :: antennas_in
       type(ids_waves),            intent(in)  :: waves_in
       type(ids_waves),            intent(out) :: waves_out ! intent(out) kills any allocation, so better to avoid
       type(ids_parameters_input), intent(in)  :: param_code
       integer,                    intent(out) :: output_flag
       character(len=:), pointer,  intent(out) :: output_message
     END SUBROUTINE LION_IMAS
  END INTERFACE

  type(ids_equilibrium)      :: equil_in
  type(ids_core_profiles)    :: coreprof_in
  type(ids_ic_antennas)      :: antennas_in
  type(ids_waves)            :: waves_in
  type(ids_waves)            :: waves_out ! intent(out) kills any allocation, so better to avoid
  type(ids_parameters_input) :: param_code
  integer                    :: output_flag
  character(len=:), pointer  :: output_message
  integer :: iounit = 1

  ! For reading input IDSs
  integer :: shot_number, input_run, output_run,idx
  character(len=132) :: filename = 'input.LION_for_kepler_IMAS_prog'
  character(len=64) :: machine, user, local_user, version

  namelist /UAL_REFERENCE/ &
       machine     , &
       user        , &
       version     , &
       shot_number , &
       input_run   , &
       output_run

  write(*,*)'Read shot and run numbers from: ',trim(filename)
  open(1,file=filename)
  read(1,UAL_REFERENCE)
  close(1)
  write(*,*)' => machine="',trim(adjustl(machine)),'"', &
       ', user="',trim(adjustl(user)),'"', &
       ', version="',trim(adjustl(version)),'"'
  write(*,*)'  ',&
       '  shot=',int(shot_number,2), &
       ', run_in=',int(input_run,2), &
       ', run-out=',int(output_run,2)

  call getlog(local_user)

  !==== XML input files =====
  write(*,*) 'Read input XML files'
  call file2buffer('LION_input.xml'  , 1 , param_code%parameters_value)
  call file2buffer('LION_input.xml'  , 1 , param_code%parameters_default)
  call file2buffer('LION_schema.xsd' , 1 , param_code%schema)


  !==== READ IDSs FROM THE UAL =====
  write(*,*) 'Read input IDSs'
  call imas_open_env('ids', shot_number, input_run, idx, &
       trim(adjustl(user)), trim(adjustl(machine)), trim(adjustl(version)))
  write(*,*) '=> Read equilibrium'
  call ids_get(idx,'equilibrium',equil_in)
  write(*,*) '=> Read core_profiles'
  call ids_get(idx,'core_profiles',coreprof_in)
  write(*,*) '=> Read ic_antennas'
  call ids_get(idx,'ic_antennas',antennas_in)
  if (.false.) then
     write(*,*) '=> Read waves'
     call ids_get(idx,'waves',waves_in)
  else
     call get_simple_waves(waves_in)
  end if
  call imas_close(idx)


  !==== RUN LION_IMAS =====
  write(*,*) 'call LION_IMAS'
  call LION_IMAS(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
  if (output_flag/=0) then
     write(*,*)'ERROR recieved from LION, output_flag=',output_flag
     if (associated(output_message)) then
        write(*,*)'Output message:'
        write(*,*)'"',output_message,'"'
        deallocate(output_message)
     end if
     stop 'Abort in LION_for_kepler_ITM_prog (error in LION)'
  end if

  !==== WRITE OUTPUT IDSs TO THE UAL =====
  write(*,*) 'Write output IDSs, shot=',int(shot_number,2),', run=',int(output_run,2)
  call imas_create_env('ids', shot_number, output_run, 0, 0, idx, &
       trim(adjustl(local_user)), trim(adjustl(machine)), trim(adjustl(version)))
  write(*,*) '=> Write equilibrium'
  call ids_put(idx,'equilibrium',equil_in)
  write(*,*) '=> Write core_profiles'
  call ids_put(idx,'core_profiles',coreprof_in)
  write(*,*) '=> Write ic_antennas'
  call ids_put(idx,'ic_antennas',antennas_in)
  write(*,*) '=> Write waves-output'
  call ids_put(idx,'waves',waves_out)
  call imas_close(idx)

  call ids_deallocate(equil_in)
  call ids_deallocate(coreprof_in)
  call ids_deallocate(antennas_in)
  call ids_deallocate(waves_in)
  call ids_deallocate(waves_out)
contains

  subroutine get_simple_waves(W)
    type(ids_waves) :: W
    allocate(W%coherent_wave(1))
    allocate(W%coherent_wave(1)%global_quantities(1))
    allocate(W%coherent_wave(1)%global_quantities(1)%n_tor(1))
    W%coherent_wave(1)%global_quantities(1)%n_tor = 27_ids_int
    W%coherent_wave(1)%global_quantities(1)%frequency = 50e6_ids_real
  end subroutine get_simple_waves

end program LION_for_kepler_IMAS_prog
