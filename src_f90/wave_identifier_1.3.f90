
!> 
!> Translation table for wave field types.
!> Used in:
!> - distribution()/distri_vec()/waves_id/id
!> - waves()/coherentwave()/waves_id/id
!> 
!> \version "$Id: wave_types.xml 677 2015-03-24 13:05:12Z tjohnson $"
!> URL: https://gforge.efda-itm.eu/svn/itmshared/trunk/src/itm_constants
!> 

module wave_identifier



  integer, parameter :: unspecified = 0  !< unspecified
  integer, parameter :: EC = 1  !< Wave field for electron cyclotron heating and current drive
  integer, parameter :: LH = 2  !< Wave field for lower hybrid heating and current drive
  integer, parameter :: IC = 3  !< Wave field for ion cyclotron frequency heating and current drive

  interface get_type_description
     module procedure  get_type_description__name , get_type_description__ind
  end interface get_type_description

contains

  !> Function returning the VALUE of the type with name NAME.
  function get_type_value(NAME)
    integer :: get_type_value
    character(*) :: NAME  !< The name of the type
    get_type_value=-999999999
    select case (trim(NAME))
      case ('unspecified')
        get_type_value=0
      case ('EC')
        get_type_value=1
      case ('LH')
        get_type_value=2
      case ('IC')
        get_type_value=3
    end select
  end function get_type_value

  !> Function returning the NAME of the type with index IND.
  function get_type_name(IND)
    character(132) :: get_type_name
    integer :: IND  !< Type index
    get_type_name=''
    select case (IND)
      case (0)
        get_type_name='unspecified'
      case (1)
        get_type_name='EC'
      case (2)
        get_type_name='LH'
      case (3)
        get_type_name='IC'
    end select
  end function get_type_name

  !> Function returning the DESCRIPTION of the type with index IND.
  function get_type_description__ind(IND)
    character(132) :: get_type_description__ind
    integer :: IND  !< Type index
    get_type_description__ind=''
    select case (IND)
      case (0)
        get_type_description__ind=& 
'unspecified'
      case (1)
        get_type_description__ind=& 
'Wave field for electron cyclotron heating and current drive'
      case (2)
        get_type_description__ind=& 
'Wave field for lower hybrid heating and current drive'
      case (3)
        get_type_description__ind=& 
'Wave field for ion cyclotron frequency heating and current drive'
    end select
  end function get_type_description__ind

  !> Function returning the DESCRIPTION of the type with name NAME.
  function get_type_description__name(NAME)
    character(132) :: get_type_description__name
    character(*) :: NAME  !< The name of the type
    get_type_description__name=''
    select case (NAME)
      case ('unspecified')
        get_type_description__name=& 
'unspecified'
      case ('EC')
        get_type_description__name=& 
'Wave field for electron cyclotron heating and current drive'
      case ('LH')
        get_type_description__name=& 
'Wave field for lower hybrid heating and current drive'
      case ('IC')
        get_type_description__name=& 
'Wave field for ion cyclotron frequency heating and current drive'
    end select
  end function get_type_description__name

  !> Function returning the DESCRIPTION of the type with index IND.
  subroutine index2strings(IND,NAME,DESCRIPTION)
    integer :: IND  !< Type index
    character(132), pointer :: NAME(:)
    character(132), pointer, optional :: DESCRIPTION(:)

    allocate(NAME(1))
    NAME(1)=''
    if (present(DESCRIPTION)) then
      allocate(DESCRIPTION(1))
      DESCRIPTION(1)=''
    endif

    select case (IND)
      case (0)
        NAME(1)='unspecified'
        if (present(DESCRIPTION)) then
          DESCRIPTION(1)=& 
'unspecified'
        endif
      case (1)
        NAME(1)='EC'
        if (present(DESCRIPTION)) then
          DESCRIPTION(1)=& 
'Wave field for electron cyclotron heating and current drive'
        endif
      case (2)
        NAME(1)='LH'
        if (present(DESCRIPTION)) then
          DESCRIPTION(1)=& 
'Wave field for lower hybrid heating and current drive'
        endif
      case (3)
        NAME(1)='IC'
        if (present(DESCRIPTION)) then
          DESCRIPTION(1)=& 
'Wave field for ion cyclotron frequency heating and current drive'
        endif
    end select
  end subroutine index2strings


end module wave_identifier
