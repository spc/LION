!---------------------------------------------------------------------
!L                 N1        NAMELIST
! VERSION 16    LDV       MAY 1995           CRPP   LAUSANNE
!
       NAMELIST /NEWRUN/ &
     &   ACHARG,   AD,       AHEIGT,   ALARG,    AMASS,    AMASSE, &
     &   ANGLET,   ANTRAD,   ANTRADMAX,ANTUP,    ANU,      ARSIZE, &
     &   ASPCT,    ASYMB,    ATE,      ATI,      ATIP,     BNOT, &
     &	 CEN0,     CENDEN,   CENTE,    CENTI,    CENTIP, &
     &   CEOMCI,   COCOS_IN, COCOS_OUT,CPSRF,    CURASY,   CURSYM,   DELTA,    DELTAF, &
     &   ELLIPT,   EPSMAC,   EQALFD,   EQDENS,   EQFAST,   EQKAPD, &
     &   EQKAPF,   EQKAPT,   EQKPTE,   EQTE,     EQTI,     FEEDUP, &
     &   FRAC,     FRCEN,    FRDEL,    FREQCY,   OMEGA,    QIAXE, &
     &   RMAJOR,   SAMIN,    SAMAX,    SIGMA,    THANT,    THANTW,   TIME_ITM, &
     &   VBIRTH,   WALRAD,   WNTDEL,   WNTORO, &
     &   LENGTH,   MANCMP,   MEQ,      MFL,      MPOLWN,   NANTSHEET, &
     &	 NANTYP,   NANT_ITM, NBCASE,   NBTYPE, &
     &   NCHI,     NCOLMN,   NCONTR,   NCUT,     NDA,      NDARG, &
     &   NDDEG,    NDENS,    NDES,     NDLT,     NDS,      NELDTTMP, NELDTTMPCOR, &
     &   NFAKAP,   NHARM,    NPLTYP,   NPOL,     NPRNT,    NPSI, &
     &   NREAD,    NRSPEC,   NRUN,     NSADDL,   NSAVE, &
     &   NSOURC,   NTEMP,    NTORSP,   NUMBER,   NVERBOSE, &
     &   NVAC,     NLCOLD,   NLCOLE,   NLDIP,    NLDISO,   NLPHAS, &
     &   NLFAST,   NLOTP0,   NLOTP1,   NLOTP2,   NLOTP3,   NLOTP4, &
     &	 NLOTP5,   NLPLO5,   NLTTMP,   nitmopt,nitmrun, nitmshot, comments
