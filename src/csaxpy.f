C
C*DECK U33
         SUBROUTINE SAXPY(N,A,X,NX,Y,NY)
CC#####################################################################
C
C U.33 COMPUTES  Y = Y + A*X (REAL)
C
C#####################################################################
       DIMENSION   X(N),   Y(N)
       REAL     A,      X,       Y
C---------------------------------------------------------------------
         IF(ABS(A).EQ. 0.0   .OR.   N.LE.0) RETURN
         Y(1)=Y(1)+A*X(1)
         IF(N.EQ.1) RETURN
         NM1=N-1
         DO 10 I=1,NM1
         Y(I*NY+1)=Y(I*NY+1)+A*X(I*NX+1)
   10    CONTINUE
C
C        RETURN
         END
C
C*DECK U34
         SUBROUTINE CAXPY(N,A,X,NX,Y,NY)
C#####################################################################
C
C U.33 COMPUTES  Y = Y + A*X (COMPLEX)
C
C#####################################################################
       DIMENSION   X(N),   Y(N)
       COMPLEX     A,      X,       Y
C---------------------------------------------------------------------
         IF(CABS(A).EQ. 0.0   .OR.   N.LE.0) RETURN
         Y(1)=Y(1)+A*X(1)
         IF(N.EQ.1) RETURN
         NM1=N-1
         DO 10 I=1,NM1
         Y(I*NY+1)=Y(I*NY+1)+A*X(I*NX+1)
   10    CONTINUE
C
C        RETURN
         END
C
	COMPLEX FUNCTION CDOTU (N, X, NX, Y, NY)
C	**********************
C
C SCALAR PRODUCT X()*Y()
C***********************************************
	DIMENSION X(N), Y(N)
	COMPLEX   X,    Y,    Z
C-----------------------------------------------
	IF (N.LE.0) THEN
	 CDOTU = CMPLX(0.,0.)
	 RETURN
	END IF
	Z = CMPLX(0.,0.)
	Z = Z + X(1) * Y(1)
        IF(N.EQ.1) THEN
	 CDOTU = Z
	 RETURN
	END IF
	NM1 = N - 1
        DO 10 I=1,NM1
	 Z = Z + X(I*NX+1) * Y(I*NY+1)
   10	CONTINUE
	CDOTU = Z
	RETURN
	END
C
	COMPLEX FUNCTION CDOTC (N, X, NX, Y, NY)
C	**********************
C
C SCALAR PRODUCT CONJG(X()) * Y()
C***********************************************
	DIMENSION X(N), Y(N)
	COMPLEX   X,    Y,    Z
C-----------------------------------------------
	IF (N.LE.0) THEN
	 CDOTC = CMPLX(0.,0.)
	 RETURN
	END IF
	Z = CMPLX(0.,0.)
	Z = Z + CONJG(X(1)) * Y(1)
        IF(N.EQ.1) THEN
	 CDOTC = Z
	 RETURN
	END IF
	NM1 = N - 1
        DO 10 I=1,NM1
	 Z = Z + CONJG(X(I*NX+1)) * Y(I*NY+1)
   10	CONTINUE
	CDOTC = Z
	RETURN
	END
C
	SUBROUTINE CCOPY (N, X, NX, Y, NY)
C	****************
C
C COPIES X ONTO Y
C********************************************
	DIMENSION X(N), Y(N)
	COMPLEX   X,    Y
C--------------------------------------------
	IF (N.LE.0) RETURN
	Y(1) = X(1)
        IF(N.EQ.1) RETURN
	NM1 = N - 1
        DO 10 I=1,NM1
	 Y(I*NY+1) = X(I*NX+1)
   10	CONTINUE
	RETURN
	END
C
	SUBROUTINE CSCAL (N, A, X, NX)
C	****************
C
C SCALES X() BY FACTOR A
C********************************************
	DIMENSION X(N)
	COMPLEX   X,    A
C--------------------------------------------
	IF (N.LE.0) RETURN
	X(1) = A * X(1)
	IF(N.EQ.1) RETURN
	NM1 = N - 1
        DO 10 I=1,NM1
	 X(I*NX+1) = A * X(I*NX+1)
   10	CONTINUE
	RETURN
	END
C*DECK CRAY05
C*CALL PROCESS
         FUNCTION ISRCHFGE(N,PV,NX,TARGET)
C        ---------------------------------
C
C  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER OR EQUAL
C  THAN TARGET. PV IS INCREMENTED BY NX
C
CLV         INCLUDE 'DECLAR.inc'
         DIMENSION 
     R   PV(N*NX)
C
         I = 1
         IF (NX .LT. 0) STOP
C
         DO 1 J=1,N
            IF (PV(I) .GE. TARGET) GOTO 2
            I = I + NX
    1    CONTINUE
    2    ISRCHFGE = I
C
         RETURN
         END
C*DECK CRAY04
C*CALL PROCESS
         FUNCTION ISRCHFGT(N,PV,NX,TARGET)
C        ---------------------------------
C
C  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER THAN TARGET
C  PV IS INCREMENTED BY NX
C
CLV         INCLUDE 'DECLAR.inc'
         DIMENSION 
     R   PV(N*NX)
C
         I = 1
         IF (NX .LT. 0) STOP
C
         DO 1 J=1,N
            IF (PV(I) .GT. TARGET) GOTO 2
            I = I + NX
    1    CONTINUE
    2    ISRCHFGT = I
C
         RETURN
         END
C*DECK CRAY10
C*CALL PROCESS
         FUNCTION SSUM(N,PV,NX)
C        ----------------------
C
C  SUMS ALL ELEMENTS OF REAL ARRAY PV
C
CLV         INCLUDE 'DECLAR.inc'
         DIMENSION 
     R   PV(N*NX)
C
         SS = 0.
C
         IF (N .LE. 0) GOTO 2
C
         I = 1
         IF (NX .LT. 0) STOP
         SS = PV(I)
C
         IF (N .EQ. 1) GOTO 2
C
         NM1 = N - 1
C
         DO 1 J=1,NM1
            I = I + NX
            SS = SS + PV(I)
    1    CONTINUE
    2    CONTINUE
C   
         SSUM = SS
C
         RETURN
         END
