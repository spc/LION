subroutine load_itm_antennas(antennas_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the antennas type definitions
  IMPLICIT NONE
  type(type_antennas),pointer      :: antennas_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='antennas'
  character*5   :: treename
  !
  print *,'This routine is not supposed to be called. When linked to ITM database, use "with" routines'
  print *,'NITMOPT should always be -1 in such cases and routines load_itm and write_itm are not called'
  stop 'load_itm_antennas'

  return
end subroutine load_itm_antennas
