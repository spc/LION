subroutine write_itm(equilibrium_out,waves_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the waves type definitions
  use euITM_routines
  IMPLICIT NONE
  type(type_equilibrium),pointer      :: equilibrium_out(:)
  type(type_waves),pointer      :: waves_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, irefrun, i
  !
  character(len=5)  :: citmtree2
  !
  if (kitmshot .eq. 22) then
    print *,' Do not write since shot number to write: ',kitmshot
    return
  end if
  !
  !waves_out%datainfo%comment=''
  
  citmtree2 = trim(citmtree)
  ! At this stage, seems to need to do create_pulse in any case, so changed else part, but may be different in future
  if (kitmshot .lt. 0) then
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    kitmshot = abs(kitmshot)
    call euitm_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  else
    !     call euitm_open(citmtree2,kitmshot,kitmrun,idx)
    !     print *,'salut32, idx from open= ',idx
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    call euitm_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  end if
  ! print *,' waves_out(1)%...= ',waves_out(1)%...
  
  call euitm_put(idx,"equilibrium",equilibrium_out)
  call euitm_put(idx,"waves",waves_out)
  
  call euitm_close(idx,"euitm",kitmshot,kitmrun)
  
  return
end subroutine write_itm
