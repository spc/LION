SUBROUTINE waves_out_SI(waves_out, waves_in, eq_in, param_code)
!        ----------------
!
!  5.2.2. CONSTRUCT DESIRED PHYSICAL QUANTITIES FROM SOLUTION VECTOR
!-----------------------------------------------------------------------
USE euitm_schemas, only: type_waves, type_equilibrium, type_param
USE GLOBALS, only: rkind, iout_time_slice, nrspec, NPSI, NPOL, BNOT, CMU0, CMP, AMASS, &
     RMAJOR, CCR, CCZ, CCS, CCHI, CENDEN, EN, EP, DEPOS, DEPPSP, FLUPSP, FLUPOW, &
     acharg, amass, freqcy, wntoro, ANTENNA_NAME
USE INTERPOS_MODULE, only: interpos
USE ITM_GRID_COMMON, only: COORDTYPE_RHOTOR, COORDTYPE_THETA
USE ITM_GRID_STRUCTURED, only: gridSetupStructuredSep, gridStructWriteData, GRID_STRUCT_NODES
use LION_profiles, only: internal_profs
use wave_identifier, only: get_type_name, get_type_description
use itm_types, only: itm_is_valid
!
IMPLICIT NONE
!
! Input/Output:
type(type_waves),       intent(out) :: waves_out
type(type_waves),       intent(in)  :: waves_in
type(type_equilibrium), intent(in)  :: eq_in
type(type_param),       intent(in)  :: param_code
!
! Internal
complex(rkind), parameter :: ZI = CMPLX(0._RKIND,1._RKIND) ! Imaginary unit
integer :: nrspec_out
integer :: j, jout, npsi_out, ind_electrons
real(rkind) :: tens_def
real(rkind), allocatable :: s_waves(:)
real(rkind) :: norm_Efield_SI  ! Normalization factor; when multiplied by a normalized electric field componenet it yields the field in SI units.
real(rkind) :: norm_power_SI   ! Normalization factor; when multiplied by a power it yields the field in SI units.
real(rkind) :: norm_powdens_SI ! Normalization factor; when multiplied by a power density it yields the field in SI units.
real(rkind) :: ZMDEN           ! Mass density
real(rkind) :: ZPOWER          ! Total heating power
real(rkind) :: ZALFSP          ! Alfven speed
complex(rkind), allocatable :: efield(:,:)
real(rkind),    allocatable :: tmp_array(:)
!
!
if (.not. associated(waves_out%coherentwave)) then
   allocate(waves_out%coherentwave(1))
end if
!
! Time and codeparam
waves_out%time = eq_in%time
if (associated(param_code%parameters)) then
   allocate(waves_out%coherentwave(1)%codeparam%parameters(size(param_code%parameters)))
   waves_out%coherentwave(1)%codeparam%parameters = param_code%parameters
end if
allocate(waves_out%coherentwave(1)%codeparam%codename(1))
waves_out%coherentwave(1)%codeparam%codename(1) = 'LION'
allocate(waves_out%coherentwave(1)%codeparam%codeversion(1))
waves_out%coherentwave(1)%codeparam%codeversion(1) = 'V1_7'
allocate(waves_out%coherentwave(1)%global_param%f_assumption(1))
waves_out%coherentwave(1)%global_param%f_assumption(1) = 0
!
!
! Set the wave-type identifier
allocate( waves_out%coherentwave(1)%wave_id%name(1) )
waves_out%coherentwave(1)%wave_id%name(1) = ANTENNA_NAME
allocate( waves_out%coherentwave(1)%wave_id%type%id(1) )
allocate( waves_out%coherentwave(1)%wave_id%type%description(1) )
waves_out%coherentwave(1)%wave_id%index = 1 ! Only one wave; use index=1.
!
! Note: wave-type flag=3 for IC waves; use "3" to calculate the "id" and "description".
waves_out%coherentwave(1)%wave_id%type%flag = 3
waves_out%coherentwave(1)%wave_id%type%id(1) = get_type_name(3)
waves_out%coherentwave(1)%wave_id%type%description(1) = get_type_description(3)
!
!
! Output plasma Compositions:
nrspec_out = 0
do j=1,nrspec
   if (.not. internal_profs%ion(j)%is_fast_population) then
     nrspec_out = nrspec_out + 1
  end if
end do
! nrspec_out = nrspec
ind_electrons = nrspec+1
!
!
write(*,*)'=============================================='
write(*,*)'=============================================='
write(*,*)'Output waves CPO, nrspec=',nrspec, ', nrspec_out=',nrspec_out
write(*,*)'  mass=',amass
write(*,*)'  charge=',acharg
write(*,*)'  is_fast=', ( internal_profs%ion(j)%is_fast_population, j=1,nrspec )
write(*,*)'=============================================='
write(*,*)'=============================================='

allocate(waves_out%coherentwave(1)%compositions%ions(nrspec_out))
allocate(waves_out%coherentwave(1)%compositions%nuclei(nrspec_out))

jout=0
do j=1,nrspec
   if (.not. internal_profs%ion(j)%is_fast_population) then
      jout=jout+1
      waves_out%coherentwave(1)%compositions%nuclei(jout)%zn = acharg(j)
      waves_out%coherentwave(1)%compositions%nuclei(jout)%amn = amass(j)
      waves_out%coherentwave(1)%compositions%ions(jout)%nucindex = jout
      waves_out%coherentwave(1)%compositions%ions(jout)%zion = acharg(j)
   end if
end do
!
!
! Global parameters
allocate(waves_out%coherentwave(1)%global_param%ntor(1))
waves_out%coherentwave(1)%global_param%frequency = FREQCY
waves_out%coherentwave(1)%global_param%ntor(1) = WNTORO
!
waves_out%coherentwave(1)%global_param%power_tot = 0._rkind
if (associated( waves_in%coherentwave )) then
   do j = 1, size( waves_in%coherentwave )
      if (itm_is_valid( waves_in%coherentwave(j)%global_param%power_tot ) ) then
         ! Use the sum of the power in all coherent waves in input
         waves_out%coherentwave(1)%global_param%power_tot = &
              waves_out%coherentwave(1)%global_param%power_tot + &
              waves_in%coherentwave(j)%global_param%power_tot
      endif
   end do
   if ( waves_out%coherentwave(1)%global_param%power_tot < 1._rkind ) then
      waves_out%coherentwave(1)%global_param%power_tot = 1._rkind ! 1W
   endif
   !
   ! p_frac_ntor
   allocate(waves_out%coherentwave(1)%global_param%p_frac_ntor(1))
   if (associated(waves_in%coherentwave(1)%global_param%p_frac_ntor)) then
      waves_out%coherentwave(1)%global_param%p_frac_ntor(1) = waves_in%coherentwave(1)%global_param%p_frac_ntor(1)
   else
      waves_out%coherentwave(1)%global_param%p_frac_ntor(1) = 1._rkind
   end if
else
   waves_out%coherentwave(1)%global_param%power_tot = 1._rkind ! 1W
   waves_out%coherentwave(1)%global_param%p_frac_ntor(1) = 1._rkind
end if
!
!
! Grid in profiles_1d:
! in outp5, CCS(js)=EQ(5,.,.) used with FLUPSP(js)
! in EQ_from_equil, psi, chi mesh used from equil_inCOCOS2(iequil)%coord_sys%grid%dim1/dim2, if from chease, it is the same as equil%profiles_1d so could use equil_inCOCOS2(iequil)%coord_sys%grid%dim1 as 1D mesh.
! But since needs rho_tor as well, better to use directly  equil%profiles_1d
if (associated(waves_out%coherentwave(1)%grid_1d%rho_tor_norm)) then
   npsi_out = size(waves_out%coherentwave(1)%grid_1d%rho_tor_norm)
else
   npsi_out = size(eq_in%profiles_1d%psi)
   allocate(waves_out%coherentwave(1)%grid_1d%rho_tor_norm(npsi_out))
   allocate(waves_out%coherentwave(1)%grid_1d%rho_tor(npsi_out))
   allocate(waves_out%coherentwave(1)%grid_1d%psi(npsi_out))
   waves_out%coherentwave(1)%grid_1d%psi(1:npsi_out) = eq_in%profiles_1d%psi(1:npsi_out)
   waves_out%coherentwave(1)%grid_1d%rho_tor(1:npsi_out) = eq_in%profiles_1d%rho_tor(1:npsi_out)
   waves_out%coherentwave(1)%grid_1d%rho_tor_norm(1:npsi_out-1) = eq_in%profiles_1d%rho_tor(1:npsi_out-1) / eq_in%profiles_1d%rho_tor(npsi_out-1)
   waves_out%coherentwave(1)%grid_1d%rho_tor_norm(npsi_out) = 1._RKIND
end if
!
allocate(tmp_array(npsi_out))
allocate(s_waves(npsi_out))
s_waves(1:npsi_out) = sqrt( &
     (waves_out%coherentwave(1)%grid_1d%psi           - waves_out%coherentwave(1)%grid_1d%psi(1)) /  &
     (waves_out%coherentwave(1)%grid_1d%psi(npsi_out) - waves_out%coherentwave(1)%grid_1d%psi(1)) )
!
! Cumulative flux surfaces volume
allocate(waves_out%coherentwave(1)%grid_1d%volume( &
     size(waves_out%coherentwave(1)%grid_1d%rho_tor)))
call interpos( eq_in%profiles_1d%rho_tor , &
     eq_in%profiles_1d%volume , &
     size(eq_in%profiles_1d%rho_tor) , &
     nout=size(waves_out%coherentwave(1)%grid_1d%rho_tor) , &
     xout=waves_out%coherentwave(1)%grid_1d%rho_tor , &
     yout=waves_out%coherentwave(1)%grid_1d%volume)
! Cross sectional area of flux surfaces
allocate(waves_out%coherentwave(1)%grid_1d%area( &
     size(waves_out%coherentwave(1)%grid_1d%rho_tor)))
call interpos( eq_in%profiles_1d%rho_tor , &
     eq_in%profiles_1d%area , &
     size(eq_in%profiles_1d%rho_tor) , &
     nout=size(waves_out%coherentwave(1)%grid_1d%rho_tor) , &
     xout=waves_out%coherentwave(1)%grid_1d%rho_tor , &
     yout=waves_out%coherentwave(1)%grid_1d%area)
!
!
! Integrated powers
!
!
allocate(waves_out%coherentwave(1)%global_param%pow_i(nrspec_out))
allocate(waves_out%coherentwave(1)%global_param%pow_fi(nrspec_out))
allocate(waves_out%coherentwave(1)%global_param%pow_ntor_i(1,nrspec_out))
allocate(waves_out%coherentwave(1)%global_param%pow_ntor_e(1))
!
allocate(waves_out%coherentwave(1)%profiles_1d%pow_tot(npsi_out))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_e(npsi_out))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_i(npsi_out,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_fi(npsi_out,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_ntor(npsi_out,1))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_ntor_e(npsi_out,1))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_ntor_i(npsi_out,1,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%pow_ntor_fi(npsi_out,1,nrspec_out))
!
waves_out%coherentwave(1)%global_param%pow_i      = 0._RKIND
waves_out%coherentwave(1)%global_param%pow_fi     = 0._RKIND
waves_out%coherentwave(1)%global_param%pow_ntor_i = 0._RKIND
waves_out%coherentwave(1)%global_param%pow_ntor_e = 0._RKIND
!
waves_out%coherentwave(1)%profiles_1d%pow_tot     = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_e       = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_i       = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_fi      = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_ntor    = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_ntor_e  = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_ntor_i  = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%pow_ntor_fi = 0._RKIND
!
norm_power_SI = waves_out%coherentwave(1)%global_param%power_tot / FLUPOW(NPSI+1)
norm_powdens_SI = waves_out%coherentwave(1)%global_param%power_tot
!
tens_def = -0.1_rkind
LOOP_ION_POWER: DO J=1,NRSPEC

   jout = internal_profs%ion(J)%ion_index

   if (internal_profs%ion(J)%is_fast_population) then

      ! FAST POPULATION: Fill global_param%pow_fi
      waves_out%coherentwave(1)%global_param%pow_fi(jout) = &
           waves_out%coherentwave(1)%global_param%pow_fi(jout) + FLUPSP(NPSI+1,J) * norm_power_SI

      ! FAST POPULATION: Fill profiles_1d%pow_fi
      ! Integrate the power absorption
      ! Add point at s=1 since value known there thus can be used as BC. At 0 should probably use 1st derv=0
      call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPSP(1:npsi,J),FLUPSP(NPSI+1,J)/)*norm_power_SI,npsi+1, &
           nout=npsi_out,xout=s_waves,tension=tens_def, &
           yout=tmp_array, &
           nbc=(/0,2/),ybc=(/0._RKIND, FLUPSP(NPSI+1,J)*norm_power_SI/))
      waves_out%coherentwave(1)%profiles_1d%pow_fi(1:npsi_out,jout) = &
           waves_out%coherentwave(1)%profiles_1d%pow_fi(1:npsi_out,jout) + tmp_array

      ! FAST POPULATION: Fill profiles_1d%pow_ntor_fi
      waves_out%coherentwave(1)%profiles_1d%pow_ntor_fi(1:npsi_out,1,jout) = &
           waves_out%coherentwave(1)%profiles_1d%pow_ntor_fi(1:npsi_out,1,jout) + tmp_array

   else

      ! THERMAL POPULATION: Fill global_param%pow_i
      waves_out%coherentwave(1)%global_param%pow_i(jout)        = FLUPSP(NPSI+1,J) * norm_power_SI
      waves_out%coherentwave(1)%global_param%pow_ntor_i(1,jout) = FLUPSP(NPSI+1,J) * norm_power_SI

      ! THERMAL POPULATION: Fill profiles_1d%pow_i
      ! Interpolate power absorption with a unit antenna current.
      ! add point at s=1 since value known there thus can be used as BC. At 0 should probably use 1st derv=0
      call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPSP(1:npsi,J),FLUPSP(NPSI+1,J)/)*norm_power_SI,npsi+1, &
           nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
           yout=waves_out%coherentwave(1)%profiles_1d%pow_i(1:npsi_out,jout), &
           nbc=(/0,2/),ybc=(/0._RKIND, FLUPSP(NPSI+1,J)*norm_power_SI/))

      ! THERMAL POPULATION: Fill profiles_1d%pow_ntor_i
      waves_out%coherentwave(1)%profiles_1d%pow_ntor_i(1:npsi_out,1,J) = waves_out%coherentwave(1)%profiles_1d%pow_i(1:npsi_out,J)

   end if
END DO LOOP_ION_POWER
waves_out%coherentwave(1)%global_param%pow_e = FLUPSP(NPSI+1,ind_electrons) * norm_power_SI
waves_out%coherentwave(1)%global_param%pow_ntor_e(1) = waves_out%coherentwave(1)%global_param%pow_e
waves_out%coherentwave(1)%global_param%code_type = 2
!
call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPOW(1:npsi),FLUPOW(NPSI+1)/),npsi+1, &
     nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
     yout=waves_out%coherentwave(1)%profiles_1d%pow_tot(1:npsi_out)*norm_power_SI,&
     nbc=(/0,2/),ybc=(/0._RKIND, FLUPOW(NPSI+1)*norm_power_SI/))
!
call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPSP(1:npsi,ind_electrons),FLUPSP(NPSI+1,ind_electrons)/)*norm_power_SI,npsi+1, &
     nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
     yout=waves_out%coherentwave(1)%profiles_1d%pow_e(1:npsi_out), &
     nbc=(/0,2/),ybc=(/0._RKIND, FLUPSP(NPSI+1,ind_electrons)*norm_power_SI/))
!
waves_out%coherentwave(1)%profiles_1d%pow_ntor(1:npsi_out,1)   = waves_out%coherentwave(1)%profiles_1d%pow_tot(1:npsi_out)
waves_out%coherentwave(1)%profiles_1d%pow_ntor_e(1:npsi_out,1) = waves_out%coherentwave(1)%profiles_1d%pow_e(1:npsi_out)
!
!
! Powers densities
!
!
allocate(waves_out%coherentwave(1)%profiles_1d%powd_tot(npsi_out))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_i(npsi_out,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_fi(npsi_out,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_e(npsi_out))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_ntor(npsi_out,1))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_ntor_e(npsi_out,1))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_ntor_i(npsi_out,1,nrspec_out))
allocate(waves_out%coherentwave(1)%profiles_1d%powd_ntor_fi(npsi_out,1,nrspec_out))
!
waves_out%coherentwave(1)%profiles_1d%powd_tot = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_i = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_fi = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_e = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_ntor = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_ntor_e = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_ntor_i = 0._RKIND
waves_out%coherentwave(1)%profiles_1d%powd_ntor_fi = 0._RKIND
!
call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPOS(1:npsi),DEPOS(NPSI)/)*norm_powdens_SI,npsi+1, &
     nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
     yout=waves_out%coherentwave(1)%profiles_1d%powd_tot(1:npsi_out), &
     nbc=(/0,2/),ybc=(/0._RKIND, DEPOS(NPSI)*norm_powdens_SI/))
!
LOOP_ION_POWER_DENSITIES: do j=1,NRSPEC

   jout = internal_profs%ion(J)%ion_index

   if (internal_profs%ion(J)%is_fast_population) then
      !
      ! FAST POPULATION: Fill powd_fi; powd_i is zero.
      call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPPSP(1:npsi,J),DEPPSP(NPSI,J)/)*norm_powdens_SI,npsi+1, &
           nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
           yout=tmp_array,&
           nbc=(/0,2/),ybc=(/0._RKIND, DEPPSP(NPSI,J)*norm_powdens_SI/))
      waves_out%coherentwave(1)%profiles_1d%powd_fi(1:npsi_out,jout) = &
           waves_out%coherentwave(1)%profiles_1d%powd_fi(1:npsi_out,jout) + tmp_array
   else
      !
      ! THERMAL POPULATION: Fill powd_i; powd_fi is zero.
      call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPPSP(1:npsi,J),DEPPSP(NPSI,J)/)*norm_powdens_SI,npsi+1, &
           nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
           yout=waves_out%coherentwave(1)%profiles_1d%powd_i(1:npsi_out,jout),&
           nbc=(/0,2/),ybc=(/0._RKIND, DEPPSP(NPSI,J)*norm_powdens_SI/))
   end if
end do LOOP_ION_POWER_DENSITIES
!
call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPPSP(1:npsi,ind_electrons),DEPPSP(NPSI,ind_electrons)/)*norm_powdens_SI,npsi+1, &
     nout=npsi_out,xout=s_waves(1:npsi_out),tension=tens_def, &
     yout=waves_out%coherentwave(1)%profiles_1d%powd_e(1:npsi_out), &
     nbc=(/0,2/),ybc=(/0._RKIND, DEPPSP(NPSI,ind_electrons)*norm_powdens_SI/))
!
waves_out%coherentwave(1)%profiles_1d%powd_ntor(1:npsi_out,1)             = waves_out%coherentwave(1)%profiles_1d%powd_tot(1:npsi_out)
waves_out%coherentwave(1)%profiles_1d%powd_ntor_i(1:npsi_out,1,1:nrspec_out)  = waves_out%coherentwave(1)%profiles_1d%powd_i(1:npsi_out,1:nrspec_out)
waves_out%coherentwave(1)%profiles_1d%powd_ntor_fi(1:npsi_out,1,1:nrspec_out) = waves_out%coherentwave(1)%profiles_1d%powd_fi(1:npsi_out,1:nrspec_out)
waves_out%coherentwave(1)%profiles_1d%powd_ntor_e(1:npsi_out,1)           = waves_out%coherentwave(1)%profiles_1d%powd_e(1:npsi_out)


!!!----------------------------------------------------------
!!! START: UNDER DEVELOPMENT - 2D WAVE FIELD OUTPUT WITH GGD
!!!----------------------------------------------------------

if (.False.) then

!!! from Olivier 16/7, 2013
!!! 
!!! Efield_phys = sqrt(mu0 * v_A0 * P[watt] / Rmajor**2 / flupow(npsi+1) ) * Efield_LION
!!! 
!!! with v_A0 the alfven speed, Rmajor, Bnot calculated in LION.f90
!!! 
!!! And we have the fields in LION units as in OUTP5(12, 13, 14=E+, etc) using
!!! E_norm = EN(js,ichi)
!!! E_binorm = EP(js,ichi)
!!! 
!!! BN, BP, BPAR
!!! 
!!! The s-mesh is CCS and the chi mesh is CCHI
!!! 
!!! This should be added to waves_out_SI.f90...

call gridSetupStructuredSep(waves_out%coherentwave(1)%fullwave%grid , &
     ndim = 2, &
     c1 = COORDTYPE_RHOTOR, &
     x1 = waves_out%coherentwave(1)%grid_1d%rho_tor(1:NPSI) , &
     c2 = COORDTYPE_THETA , &
     x2 = CCHI(1:NPOL)    , & !!! CAREFUL!!! Should we use a different poloidal angle??
     id = '2d_structured' )

!     TOTAL MASS DENSITY ON MAGNETIC AXIS
ZMDEN = 0._RKIND
DO J=1,NRSPEC
   ZMDEN = ZMDEN + CENDEN(J)*AMASS(J)*CMP
END DO

!     ALFVEN SPEED ON MAGNETIC AXIS
ZALFSP = BNOT / SQRT(CMU0*ZMDEN)

!     Power [watt]
ZPOWER=waves_out%coherentwave(1)%global_param%power_tot

!     Factor for transforming electric fields in LION-unit to SI units
norm_Efield_SI = sqrt(cmu0 * ZALFSP * ZPOWER / RMAJOR**2 / abs(flupow(npsi+1)) )

allocate( efield(NPSI,NPOL) )
!
! Write 
efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) + ZI*EP(1:NPSI,1:NPOL))
call gridStructWriteData( waves_out%coherentwave(1)%fullwave%grid , &
    & waves_out%coherentwave(1)%fullwave%e_components%e_plus , &
    & GRID_STRUCT_NODES , &
    & efield )

efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) - ZI*EP(1:NPSI,1:NPOL))
call gridStructWriteData( waves_out%coherentwave(1)%fullwave%grid , &
    & waves_out%coherentwave(1)%fullwave%e_components%e_minus , &
    & GRID_STRUCT_NODES , &
    & efield )

deallocate( efield )

endif !!! if (.False.)

!!!--------------------------------------------------------
!!! END: UNDER DEVELOPMENT - 2D WAVE FIELD OUTPUT WITH GGD
!!!--------------------------------------------------------


!!!----------------------------------------------------------------------
!!! START: UNDER DEVELOPMENT - OLD FORMAT VERSION OF 2D WAVE FIELD OUTPUT
!!!----------------------------------------------------------------------

!     TOTAL MASS DENSITY ON MAGNETIC AXIS
ZMDEN = 0._RKIND
DO J=1,NRSPEC
   ZMDEN = ZMDEN + CENDEN(J)*AMASS(J)*CMP
END DO

!     ALFVEN SPEED ON MAGNETIC AXIS
ZALFSP = BNOT / SQRT(CMU0*ZMDEN)

!     Power [watt]
ZPOWER=waves_out%coherentwave(1)%global_param%power_tot

!     Factor for transforming electric fields in LION-unit to SI units
norm_Efield_SI = sqrt(cmu0 * ZALFSP * ZPOWER / RMAJOR**2 / abs(flupow(npsi+1)) )

allocate( waves_out%coherentwave(1)%grid_2d%rho_tor(NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%grid_2d%theta(NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%grid_2d%r(NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%grid_2d%z(NPSI,NPOL) )

allocate( waves_out%coherentwave(1)%fullwave%local%e_plus(1,NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%fullwave%local%e_minus(1,NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%fullwave%local%e_plus_ph(1,NPSI,NPOL) )
allocate( waves_out%coherentwave(1)%fullwave%local%e_minus_ph(1,NPSI,NPOL) )

! Fill grid_2d
do j=1,NPSI
   waves_out%coherentwave(1)%grid_2d%rho_tor( j , 1:NPOL ) = &
        waves_out%coherentwave(1)%grid_1d%rho_tor(j)
enddo
do j=1,NPOL
   waves_out%coherentwave(1)%grid_2d%theta( 1:NPSI , j ) = CCHI(j)
enddo
waves_out%coherentwave(1)%grid_2d%r( 1:NPSI , 1:NPOL ) = CCR( 1:NPSI , 1:NPOL )
waves_out%coherentwave(1)%grid_2d%z( 1:NPSI , 1:NPOL ) = CCZ( 1:NPSI , 1:NPOL )

waves_out%coherentwave(1)%grid_2d%grid_type=2 ! Here 2 represents a rectangular grid in (rho_tor,theta)

! Fill fullwave%local
allocate( efield(NPSI,NPOL) )
efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) - ZI*EP(1:NPSI,1:NPOL))
waves_out%coherentwave(1)%fullwave%local%e_minus(1,:,:) = abs(efield)
waves_out%coherentwave(1)%fullwave%local%e_minus_ph(1,:,:) = atan2(aimag(efield),real(efield))

efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) + ZI*EP(1:NPSI,1:NPOL))
waves_out%coherentwave(1)%fullwave%local%e_plus(1,:,:) = abs(efield)
waves_out%coherentwave(1)%fullwave%local%e_plus_ph(1,:,:) = atan2(aimag(efield),real(efield))
deallocate( efield )
deallocate( tmp_array )

!!!--------------------------------------------------------------------
!!! END: UNDER DEVELOPMENT - OLD FORMAT VERSION OF 2D WAVE FIELD OUTPUT
!!!--------------------------------------------------------------------

return
end SUBROUTINE waves_out_SI
