Compiling LION: (Version V0709)

   With gnu fotran 95:
   g95 -r8 -i4 -O3 -o lionf0709_g95_128.exe lionf0709.f csaxpy.f

   On pleiades2:
   ifort -O3 -xT -i-dynamic -r8 -i4 -o lionf0709_ifort_256_0wcz.exe lionf0709.f csaxpy.f 

Compiling CHEASE: (Version V0496CPC)

   With gnu fortran 95:
   g95 -o chease_g95.exe -r8 -O3 *.f

   On pleiades2:
   ifort -O3 -xT -r8 -i4 -o chease96wc.exe *.f


Running CHEASE:

   chease_g95_256.exe < datain_cjcirc33157_64 > out_cjcirc33157_64

This produces several files which are needed for LION, as follows:

   cp MEQ TAPE4
   cp NSAVE TAPE8
   cp NDES TAPE16
   cp NVAC TAPE17

Running LION:

   lionf0709_g95_128.exe < datain_ljcirc33157_64 > out_ljcirc33157_64

Test runs are:

datain_cjcirc33157_08: circular CHEASE equil, NPSIxNCHI=8x8
datain_cjcirc33157_64: circular CHEASE equil, NPSIxNCHI=64x64
datain_cjshaped33157_08: shaped (SND) CHEASE equil for JET #33157, NPSIxNCHI=8x8
datain_cjshaped33157_128: shaped (SND) CHEASE equil for JET #33157, NPSIxNCHI=128x128

datain_ljcirc33157_08 : one frequency in TAE range, 8x8
datain_ljcirc33157_64 : one frequency in TAE range, 64x64
datain_ljcirc33157_64_fast: id, including fast particle stability analysis, 64x64
datain_ljcirc33157_64_scan: frequency scan (10 frequencies, no fast particle stability analysis) 64x64
datain_ljshaped33157_08 : one frequency in TAE range, 8x8
datain_ljshaped33157_128 : one frequency in TAE range, 128x128

Outputs are in files:

out_cjxxxxx (for CHEASE)
out_ljxxxxx (for LION)
