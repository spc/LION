C---------------------------------------------------------------------
CL                  C3.1     (PSI,CHI) MESH VARIABLES
C VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
       COMMON/COMESH/
     R   CHI   ,
     I   NCHI  ,   NPOL  ,   NPSI
       DIMENSION
     R   CHI(MD2CP2)
