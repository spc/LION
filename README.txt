LION Version:
-------------

V0709: 	single source file; 
	does not include Hellsten et al. 's modifs for SELFO or SELFO-light; 
	does include some optimization and some cleanup;

How to compile the code: see in ~/src/README_how_to_compile

How to compile and run the code for test cases, see in ~/WK/README_compile_run_CHEASE_LION

MATRIXSIZE.m: Matlab function which computes the matrix size in memory
MATRIXSIZE(MDPSI,MDPOL,MDSTORE)
	MDSTORE=0: split matrix in radial blocks, i/o disk
	MDSTORE=1: store whole matrix in memory


LION F90 Version:

Create src_f90 and LION_v1_1.f90, plus additional files. Modified to run with new CHEASE version and modified to F90 standards, including implicit none and _RKIND

for comments on tagged versions, see README_tags
