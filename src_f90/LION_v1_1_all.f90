!CC*DECK P1C0S01
PROGRAM LION1
  !        -------------
  !
  CALL MASTER
  STOP
  !
  !
  !
  !
  !                  *******************************
  !                  *                             *
  !                  *    L I O N - FAST - 0709    *
  !                  *                             *
  !                  *******************************
  !
  !
  !                  2-D GLOBAL WAVE CODE FOR ALFVEN AND ICRF
  !
  !                  BY
  !
  !                  L. VILLARD, K. APPERT AND R. GRUBER
  !
  !
  !                                      CRPP-EPFL
  !                                      STATION 13
  !                                      CH-1015 LAUSANNE
  !                                      SWITZERLAND
  !
  !
  !
  !*******************************************************************************
  !*
  !*    THIS VERSION:
  !*
  !*    VERSION  0709     L. Villard      DECEMBER 2007           CRPP LAUSANNE
  !*    
  !*    (Previous VERSION  F1295     LDV      DECEMBER 1995)
  !*    (PREVIOUS VERSION: F0895, NLPHAS WITH CUT NEAR 2PI)
  !*    (PREVIOUS VERSION: F16 JUNE 1995, slion95y.f)
  !*    (PRE-PREVIOUS VERSION: FINAL VERSION FOR JET CONTRACT ARTICLE 14 NO. JT4/9007)
  !*         
  !*******************************************************************************
  !
  !
  !     THIS VERSION FEATURES:
  !     ----------------------
  !
  !>>>  0709: Reduced I/O disk for faster execution
  !
  !     - There is an added option (needs recompilation) so that the matrix is stored
  !       entirely in memory (AALL). This is selected in PARDIM.inc by setting
  !       PARAMETER (MDSTORE=1). 
  !
  !       The memory requirement for the matrix is 96*MDPSI*MDPOL**2 [Bytes]
  !       For example, a 256x256 grid requires 1.61 GB
  !         
  !       Setting MDSTORE=0 will cause the matrix to be split in blocks with i/o on disk, 
  !       exactly as was done in the previous version.
  !       Any change to MDSTORE setting, like for any other parameter setting in
  !       PARDIM.inc requires recompilation in order to be effective. The recommended 
  !       usage is to create two executables, one with MDSTORE=1, to be used as long as 
  !       there is enough processor memory, and the other wit hMDSTORE=0, to be used if 
  !       memory is in short supply.
  !
  !     - All equilibrium quantities from CHEASE are read once into a single EQ(j,jchi,js)
  !       Multiple reading / rewinding of MEQ file has now been removed.
  !
  !     - Some cumbersome legacy from previous versions was cured (NSAVE): the LION 
  !       namelist was rewritten and repetitively read at various places. This has now 
  !       been removed.
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -     POSSIBILITY TO EXTRACT THE RAPID POLIDAL PHASE VARIATION, 
  !>>>        USEFUL IN THE LOW FREQUENCY RANGE (TAE'S), OPTION NLPHAS.
  !>>>  -     THE POLOIDAL CUT IS NEAR CHI=PI.
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -	    POSSIBILITY TO DEFINE AN ANTENNA INSIDE THE PLASMA WITH VOLUME 
  !>>>	    CURRENT DENSITY (NANTYP=-1)
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -     POSSIBILITY TO PUT THE WALL RIGHT ON THE PLASMA BOUNDARY 
  !>>>        (WALRAD<=1., NANTYP=-1).
  !>>>
  !
  !     -     POSSIBILITY TO USE UP/DOWN ASYMMETRIC EQUILIBRIA. IN PARTICULAR
  !	    SINGLE-NULL CONFIGURATIONS CAN NOW BE STUDIED. (THE VICINITY
  !	    OF THE X-POINT(S) STILL CANNOT BE TREATED EXACTLY).
  !
  !     -     THE LION CODE IS COUPLED TO THE EQUILIBRIUM CODE CHEASE, WHICH
  !	    HAS PROVEN TO GIVE VERY ACCURATE EQUILIBRIA AND MAPPING. SEE
  !	    H. LUTJENS ET AL., COMPUT. PHYS. COMMUN. 69 (1992) 287.
  !
  !     -     MODIFIED WEAK FORM EXPRESSION TO INCLUDE THE POSSIBILITY
  !	    TO USE AN ARBITRARY DEFINITION OF THE POLOIDAL COORDINATE CHI
  !	    (GENERALIZED JACOBIAN).
  !
  !     -     SADDLE COILS ANTENNAS OF THE TYPE INSTALLED AT JET. THE FOLLOWING
  !	    POSSIBILITIES OF POLOIDAL ANTENNA PHASING ARE INCLUDED:
  !
  !		- ONE SADDLE COIL ONLY (EITHER THE TOP ONE OR THE BOTTOM ONE)
  !		- TWO SADLLE COILS IN PHASE (++) (M=2)
  !		- TWO SADDLE COILS IN PHASE OPPOSITION (+-) (M=1)
  !
  !     -     FREQUENCY SWEEP OF THE ANTENNA CURRENT.
  !
  !     -	    TOROIDAL MODE NUMBER SCAN.
  !
  !     -	    ELECTRON LANDAU DAMPING OF ALFVEN WAVE DUE TO E_PARALLEL AND 
  !	    MAGNETIC CURVATURE DRIFT (DIAGNOSTIC).
  !
  !     -     ELECTRON LANDAU & TTMP DAMPING OF FAST WAVE (IN DIELECTRIC TENSOR).
  !
  !     -     FUNDAMENTAL ION-CYCLOTRON DAMPING OF FAST WAVE (IN DIEL. TENSOR).
  !
  !     -     HARMONIC ION-CYCLOTRON DAMPING OF FAST WAVE (IN DIEL. TENSOR).
  !
  !     -     POWER ABSORPTION PER SPECIES (DIAGNOSTIC).
  !     
  !     -     MAGNETIC-SURFACE-AVERAGED POWER ABSORPTION DENSITY (DIAGNOSTIC).
  !
  !     -     FOURIER ANALYSIS (DIAGNOSTICS) OF THE SOLUTION IN THETA ANGLE
  !	    AND IN THE POLOIDAL COORDINATE CHI.
  !
  !      -    VARIOUS POSSIBILITIES TO DEFINE THE DENSITY PROFILE (INPUT).
  !
  !
  !>>>  For LION-F versions only:
  !     -------------------------
  !
  !	- Kinetic effects on low frequency modes (omega < omega_ci) on
  !	  bulk ions, electrons and fast ions species. Bulk species are
  !	  Maxwellian, fast ions are slowing-down isotropic.
  !
  !	- Includes electron and bulk ion Landau and TTMP dampings due to 
  !	  finite parallel wave electric field, to magnetic curvature and
  !	  to finite parallel wave magnetic field.
  !
  !	- Includes fast ion Landau and TTMP dampings due to magnetic 
  !	  curvature and finite parallel wave magnetic field.
  !
  !	- Includes fast ion drive due to spatial gradient of fast ion
  !	  pressure, magnetic curvature drift and finite B_parallel.
  !
  !	- The code computes the DKE wave-particle power transfers to the
  !	  various species. It finds the marginal stability point for the
  !	  fast ion central density, fast ion central beta and fast ion
  !	  volume-average beta.
  !<<<
  !
  !     THE FOLLOWING IMPROVEMENTS HAVE BEEN MADE ON THE NUMERICAL SIDE:
  !     ----------------------------------------------------------------
  !
  !     -     THE ALGORITHM FOR MATRIX CONSTRUCTION (FORMER "LION3")
  !           AND RESOLUTION (FORMER "LION4") HAS BEEN REWRITTEN TO
  !           MINIMIZE DISK I/O. WE NOW USE A "FRONTAL" METHOD WHERE
  !           MATRIX CONSTRUCTION, LDU DECOMPOSITION AND RESOLUTION
  !           OF LOWER TRIAGULAR SYSTEM IS DONE "BLOCK BY BLOCK", I.E.
  !           SURFACE BY SURFACE. SEE SUBROUTINE "FRONTB". DISK SPACE
  !           WAS REDUCED BY A FACTOR 3. VOLUME OF DATA TRANSFERRED
  !           WITH DISK I/O WAS REDUCED BY A FACTOR 6. THIS ALGORITHM,
  !           COMBINED WITH THE USE OF VECTORIZED I/O, REDUCED THE 
  !           I/O WAIT TIME TO A NEGLIGIBLE AMOUNT (0.1% OF CPU TIME).
  !
  !     -     THERE IS THE POSSIBILITY TO RUN LION WITHOUT ANY SCRATCH
  !	    DISK SPACE FOR MATRIX STORAGE BY SETTING NLDISO=F IN THE
  !	    NAMELIST INPUT $NEWRUN. WITH THIS OPTION THE TOTAL POWER
  !	    IS COMPUTED AND THE SOLUTION IS COMPUTED ONLY AT PLASMA-
  !	    VACUUM INTERFACE. NO OTHER DIAGNOSTIC OF THE SOLUTION IS 
  !	    PERFORMED.THIS OPTION IS RECOMMENDED FOR LARGE MESH SIZES
  !	    AND EXTENSIVE POWER TRACES (LARGE NRUN).
  !
  !     -     FORMER "LION1" (EQUILIBRIUM MAPPING) HAS BEEN REMOVED.
  !           THE LION CODE NOW USES THE EQUILIBRIUM AND MAPPING
  !           GENERATED BY THE BICUBIC HERMITE FINITE ELEMENTS
  !           EQUILIBRIUM CODE "CHEASE" BY H. LUTJENS. THE EQUILIBRIUM
  !           CODE "EQLAUS" IS NOT NEEDED ANYMORE.
  !
  !     -     THE INTERPOLATIONS IN FORMER "LION2" (VACUUM & ANTENNA),
  !           SUBROUTINES TETMSH AND ROTETA, HAVE BEEN REMOVED. WE NOW
  !           SIMPLY USE THE VALUES CALCULATED IN THE EQUILIBRIUM CODE
  !           "CHEASE". PART OF THE NUMERICAL INACCURACY AND EVEN
  !           SOMETIMES UNSATIFACTORILY BEHAVIOUR FOR ELONGATED CROSS-
  !           SECTIONS COULD THUS BE REMOVED. STILL REMAINS AN
  !           INADEQUATE TREATMENT OF THE GREEN'S FUNCTION SINGULAR
  !           BEHAVIOUR FOR ELONGATIONS ABOVE 2 AND NPOL > 200.
  !
  !     -     THE CODE HAS BEEN RESTRUCTURED. IT DOES NOT CONSIST OF
  !           FIVE OR FOUR SEPARATE MAIN PROGRAMS AS BEFORE, BUT HAS
  !           ONLY ONE (LION1). OF FORMER LION1 REMAINS ONLY SUBROU-
  !           TINES CLEAR, PRESET, LABRUN, DATA, AUXVAL. FORMER LION2
  !           IS REPLACED WITH "CALL VACUUM", FORMER LION3 AND LION4
  !           ARE REPLACED WITH "CALL ORGAN4", AND FORMER LION5 IS
  !           REPLACED WITH "CALL DIAGNO". NOTE THAT THREE DIFFERENT
  !           BLANK COMMON DEFINITIONS ARE USED IN THE THREE PARTS
  !           OF THE PROGRAM:
  !
  !              VACUUM & ALL DAUGHTER SUBROUTINES USE COMVID
  !              ORGAN4 & ALL DAUGHTER SUBROUTINES USE COMMTR
  !              DIAGNO & ALL DAUGHTER SUBROUTINES USE COMPLO
  !
  !     -     FOR A 100X100 MESH THE LION CODE NEEDS LESS THAN 2 MW OF 
  !	    MEMORY AND TAKES ABOUT 30 CPU S PER ANTENNA FREQUENCY AND
  !	    PER TOROIDAL WAVE NUMBER ON A CRAY YMP.
  !	    
  !
  !*******************************************************************************
  !
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE GENERATOR FREQUENCY ('FREQCY') OR THE TOROIDAL MODE NUMBER
  !     (WNTORO). IN OTHER WORDS IT IS POSSIBLE TO MAKE A SWEEP OF
  !     THE ANTENNA FREQUENCY OR A TOROIDAL MODE NUMBER SCAN.
  !
  !     THE NUMBER OF DIFFERENT FREQUENCIES IN THE SWEEP IS GIVEN IN THE
  !     NAMELIST $NEWRUN OF LION ('NRUN'). IN THIS CASE, THE INITIAL FREQUENCY
  !     IS GIVEN BY 'FREQCY' [HZ] AND IS INCREMENTED BY CONSTANT AMOUNTS.
  !     THE INCREMENT IS GIVEN IN THE NAMELIST $NEWRUN OF LION (DELTAF).
  !
  !     ALTERNATIVELY, THERE IS THE POSSIBILITY TO SPECIFY THE NUMBER OF
  !     TOROIDAL WAVE NUMBERS IN THE N-SCAN ('NTORSP'). IN THIS CASE, THE
  !     INITIAL TOROIDAL MODE NUMBER IS 'WNTORO' AND IS INCREMENTED BY
  !     CONSTANT AMOUNTS. THE INCREMENT IS GIVEN BY 'WNTDEL'.
  !
  !     NOTE THAT SPECIFYING NTORSP > 1 WILL FORCE NRUN TO 1, IN ORDER TO AVOID
  !     A TOO LARGE RUN (AND A TOO LARGE OUTPUT FILE...). THIS MEANS THAT THE
  !     TOROIDAL MODE NUMBER SCAN TAKES PRECEDENCE OVER THE FREQUENCY SWEEP.
  !*******************************************************************************
  !
  !     IN THIS VERSION THE EQUILIBRIUM CALCULATION AND MAPPING IS DONE
  !     IN THE CODE CHEASE (H. LUTJENS). THE DATA IS TRANSFERRED VIA 
  !     DISK I/O ON THE FOLLOWING CHANNELS:
  !
  !        NSAVE     NAMELIST VARIABLES FROM CHEASE (NPSI, NCHI, ETC.)
  !        MEQ       EQUILIBRIUM QUANTITIES (i,jchi),js=1,npsi+1 -> EQ(i,jchi,js)
  !        NVAC      QUANTITIES AT PLASMA-VACUUM INTERFACE
  !        NDES      R,Z,AND NORMALS TO PSI=CONST. R,Z OF P.V.I.
  !
  !     THEREFORE WE DO NOT USE 'LION1', FORMERLY 'ERATO1'.
  !
  !     PLEASE PAY ATTENTION TO THE FACT THAT THE NAMELIST $NEWRUN
  !     TRANSFERRED BY CHEASE IS READ IN SUBROUTINE AUXVAL, WHEREAS
  !     NAMELIST $NEWRUN CONTAINING THE INPUT PARAMETERS FOR LION
  !     IS READ IN SUBROUTINE DATA. THIS IS DONE TO AVOID CONFLICTS
  !     IN THE DEFINITIONS OF THESE NAMELISTS (THEY ARE DIFFERENT!).
  !     THE DEFINITIONS OF SOME OF THE NAMELIST VARIABLES ARE DIFFERENT.
  !     PLEASE LOOK INTO SUBROUTINE AUXVAL FOR MORE INFORMATION.
  !
  !*******************************************************************************
  !
  !
  !*******************************************************************************
  !***                                                                         ***
  !***                       U S E R    M A N U A L                            ***
  !***                                                                         ***
  !*******************************************************************************
  !
  !
  !     REFERENCES :
  !     ----------
  !               [1] L.VILLARD, K.APPERT, R.GRUBER AND J.VACLAVIK,
  !                   'GLOBAL WAVES IN COLD PLASMAS',LAUSANNE REPORT
  !                   LRP 275/85 (1985) CRPP, LAUSANNE, SWITZERLAND.
  !                   COMPUT. PHYS. REPORTS 4 (1986) 95.
  !
  !               [2] R.GRUBER ET AL., COMPUT. PHYS. COMMUN. 24
  !                   (1981) 323.
  !
  !               [3] K.APPERT ET AL., NUCL.FUS. 22 (1982) 903.
  !
  !               [4] L. VILLARD, 'PROPAGATION ET ABSORPTION D'ONDES
  !                   AUX FREQUENCES D'ALFVEN ET CYCLOTRONIQUES
  !                   IONIQUES DANS LES PLASMAS TORIQUES', THESE 
  !                   NO. 673, DEPARTEMENT DE PHYSIQUE, EPFL, 
  !                   & LAUSANNE REPORT LRP 322/87 (1987) CRPP, 
  !                   LAUSANNE, SWITZERLAND.
  !
  !		[5] L. Villard, G.Y. Fu, Nucl. Fusion 32 (1992) 1695.
  !
  !		[6] S. Brunner, J. Vaclavik, 'On Absorption of Low Frequency
  !		    Electromagnetic Fields', LRP 471/93 (1993) CRPP, Lausanne.
  !
  !		[7] L. Villard, S. Brunner, J. Vaclavik, 'Global Marginal
  !		    Stability of TAEs in the Presence of Fast Ions', accepted
  !		    for publication in  Nucl. Fusion (1995).
  !
  !******************************************************************************
  !***                                                                        ***
  !***             COMPLEMENTARY INFORMATION ABOUT INPUT VARIABLES            ***
  !***                                                                        ***
  !******************************************************************************
  !
  !
  !                   1. PLASMA PHYSICS
  !                      --------------
  !
  !        THE EQUILIBRIUM AND MAPPING IS DONE IN THE IDEAL MHD CODE CHEASE.
  !     PLEASE CONSULT THE CHEASE USER MANUAL FOR INFORMATION ABOUT INPUT
  !     VARIABLES SPECIFYING THE GEOMETRY, PROFILES AND MESH.
  !
  !     NOTE THAT THE $EQDATA INPUT NAMELIST OF CHEASE MUST CONTAIN 'NIDEAL=2,'
  !     SO THAT THE DATA TRANSFER FROM CHEASE TO LION IS DONE CORRECTLY.
  !
  !     THE DATA TRANSFERED FROM CHEASE CONTAINS THE FOLLOWING INFORMATION ON
  !     THE UNITS NAMED BELOW:
  !
  !     NSAVE    NAMELIST $NCHEAS CONTAINING, AMONG OTHER THINGS:
  !              'NPSI':  NUMBER OF RADIAL INTERVALS
  !              'NCHI':  NUMBER OF POLOIDAL INTERVALS ALL AROUND (PLEASE NOTE
  !                       THAT IN LION THIS BECOMES VARIABLE 'NPOL', AND THAT
  !                       'NCHI' IS DEFINED IN LION AS THE NUMBER OF POLOIDAL
  !                       INTERVALS IN THE UPPER HALF-PLANE)
  !              'CPSRF': PSI AT PLASMA SURFACE
  !              'QIAXE': 1 / Q AT MAGNETIC AXIS
  !              AND OTHER VARIABLES.
  !
  !     MEQ      EQUILIBRIUM QUANTITIES (i,jchi),js=1,npsi+1 -> EQ(i,jchi,js)
  !
  !     NVAC     SURFACE QUANTITIES FOR VACUUM CALCULATION
  !
  !     NDES     QUANTITIES NEEDED FOR PLOTS
  !
  !     THE FILES NSAVE, MEQ, NVAC AND NDES CAN BE SAVED AFTER RUNNING CHEASE
  !     SO THAT THEY CAN BE USED BY LION, IN CASE SEVERAL LION RUNS ARE MADE
  !     WITH THE SAME EQUILIBRIUM AND THE SAME MESH.
  !
  !------------------------------------------------------------------------------
  !
  !        ALL OTHER INPUT VARIABLES MUST BE SPECIFIED IN  NAMELIST $NEWRUN OF
  !     LION IN THE DEFAULT INPUT (TAPE5) FILE. SEE AN EXAMPLE OF INPUT BELOW.
  !
  !
  !        THEY CONSIST OF THE FOLLOWING:
  !
  !      
  !     'NLCOLD'  : SWITCH OFF ION CYCLOTRON DAMPING 
  !		  .T. ==> COLD PLASMA DIELECTRIC TENSOR
  !		  .F. ==> WARM IONS DIEL. TENS. (ORDER 0 IN LARMOR)
  !
  !     'NLCOLE'  : SWITCH OFF ELECTRON LANDAU & TTMP DAMPING OF FAST WAVE:
  !		  .T. ==> NO ADDITIONNAL TERM IN EPSILON_PERPPERP
  !		  .F. ==> ADDITIONNAL DAMPING TERM IN EPSILON_PERPPERP.
  !		  NOTE THAT THE ALFVEN WAVE ELECTRON LANDAU DAMPING RATE
  !		  IS EVALUATED AS A DIAGNOSTIC OF THE OBTAINED SOLUTION
  !		  IRRESPECTIVELY OF THE VALUE OF NLCOLE.
  !
  !     'ANU'     : COLLISIONAL DAMPING (NU/OMEGA) (COLD MODEL ONLY)
  !
  !     'DELTA'   : PHENOMENOLOGICAL DAMPING       (BOTH MODELS)
  !
  !     'NRSPEC'  : NUMBER OF ION SPECIES.
  !
  !     'ACHARG()': ATOMIC CHARGES OF ION SPECIES [ATOMIC UNITS]
  !
  !     'AMASS()' : ATOMIC MASSES OF ION SPECIES  [ATOMIC UNITS]
  !
  !     'CENDEN()': NUMBER DENSITIES OF ION SPECIES [M**-3] ON MAGNETIC
  !                 AXIS (M.A.).
  !
  !     'CENTI()' : PARALLEL ION TEMPERATURES [EV] ON M.A. (WARM MODEL ONLY)
  !
  !     'CENTIP() : PERPENDICULAR     "       [ "]    "    (WARM MODEL ONLY)
  !
  !     'CENTE'   : ELECTRON TEMPERATURE      [ "]    "    (BOTH MODELS)
  !
  !     'NHARM'   : NUMBER OF ION CYCLOTRON HARMONICS      (WARM MODEL ONLY)
  !
  !
  !     'NDENS', 'NDARG', 'NDDEG', 'AD(.)', 'EQDENS', 'EQKAPD'
  !      SPECIFY THE TOTAL MASS DENSITY PROFILE [NORMALIZED TO TOTAL MASS
  !      DENSITY ON MAGNETIC AXIS] :
  !
  !      NDENS = -2 ===> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !
  !         RHO = SQRT (P/P_AXIS)
  !
  !      NDENS = -1 ===> POLYNOMIAL FUNCTION OF
  !
  !                      S**2 IF NDARG = 1
  !                      S    IF NDARG = 2
  !
  !         RHO = 1. + SUM(J=1,NDDEG) {AD(J)*ARG**J}
  !
  !      NDENS = 0  ===>
  !
  !         RHO = (1. - EQDENS * S*S) **EQKAPD
  !
  !      NDENS = 1  ===>
  !
  !         RHO = 1.            FOR S < EQDENS
  !         RHO = EQKAPD + (1.-EQKAPD) * COS (PI/2*(S-EQDENS)) **2  
  !
  !      NDENS = 2  ===>
  !
  !         RHO = 1.            FOR S < EQDENS
  !         RHO = A CUBIC FUNCTION OF S SUCH THAT:
  !                  RHO     (S=EQDENS) = 1.
  !                  DRHO/DS (S=EQDENS) = 0.
  !                  RHO     (S=1)      = EQKAPD
  !                  DRHO/DS (S=1)      = EQALFD
  !
  !	NDENS = 3  ==>
  !
  !	  RHO = T^2/q^2 * q0^2/T0^2 (SO THAT OMEGAXTAE IS FLAT)
  !
  !         (FUNCTION DENSIT, P1C2S34)
  !
  !
  !     'EQTI()', EQKAPT()', 'NTEMP':
  !      SPECIFY THE ION PARALLEL AND PERPENDICULAR
  !      TEMPERATURE PROFILES [EV] :
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TI(PARALLEL) = CENTI(I) * SQRT (P/P_AXIS)
  !
  !        NTEMP = -1 ==> POLYNOMIAL FUNCTION OF
  !
  !                      S**2 IF NDARG = 1
  !                      S    IF NDARG = 2
  !
  !          TE/TI()/TIP() = CENTE/CENTI()/CENTIP() * 
  !                          (1. + SUM(J=1,NDDEG) {ATE/ATI/ATIP(J)*ARG**J})
  !
  !        NTEMP # -1 OR -2  ==>
  !
  !        TI(PARALLEL) = CENTI(I) * (1.-EQTI(I)*S*S) **EQKAPT(I)
  !        (SUBROUTINE TEMPI)
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TI(PERP) = CENTIP(I) * SQRT (P/P_AXIS)
  !
  !        NTEMP=-1   ==> POLYNOMIAL (SEE ABOVE)
  !
  !        NTEMP # -2 ==>
  !
  !        TI(PERP)    = CENTIP(I) * (1.-EQTI(I)*S*S) **EQKAPT(I)
  !        (SUBROUTINE TEMPRP)
  !
  !
  !     'EQTE', 'EQKPTE', 'NTEMP':
  !      SPECIFY THE ELECTRON TEMPERATURE PROFILE [EV] :
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TE = CENTE * SQRT (P/P_AXIS)
  !
  !        NTEMP=-1   ==> POLYNOMIAL (SEE ABOVE)
  !
  !        NTEMP # -1 OR -2 ==>
  !
  !        TE = CENTE * (1. - EQTE * S*S) ** EQKPTE
  !        (FUNCTION TEMPEL)
  !
  !      NOTE THAT THE CONSISTENCY OF BETA_AXIS WITH THE EQUILIBRIUM VALUE
  !      IS NOT CHECKED WITH NTEMP = -2 AND NDENS = -2.
  !
  !     'BNOT':   MAGNETIC FIELD ON M.A. [TESLA]. (NOT THE VACUUM FIELD)
  !
  !     'RMAJOR': MAJOR RADIUS (DISTANCE AXIS OF THE TORUS, M.A.) [M]
  !
  !--------------------------------------------------------------------------
  !>>>	For LION-F Versions only:
  !	-------------------------
  !
  !     'NLFAST': .T. ==> FAST PARTICLES ARE INTRODUCED (!DIAGNOSTIC!):
  !
  !	==>	'NRSPEC' IS FORCED TO 1 (1 BULK ION SPECIES) ;
  !
  !	==>	'ACHARG(2)' IS THE ATOMIC CHARGE OF FAST PARTICLES ;
  !
  !	==>	'AMASS(2)' IS THE ATOMIC MASS OF FAST PARTICLES ;
  !
  !	==>	'CENDEN(2)' IS THE DENSITY OF FAST PARTICLES ON
  !		 MAGNETIC AXIS [M**-3] (CENDEN(2) << CENDEN(1));
  !
  !	==>	 THE DENSITY PROFILE OF FAST PARTICLES IS SPECIFIED BY 
  !		'CENDEN(2)', 'EQFAST', 'EQKAPF(JF)'JF=1,NFAKAP :
  !
  !		 N_FAST = CENDEN(2) * (1.-EQFAST*S*S) **EQKAPF(JF) [M**-3]
  !
  !       ==>     'VBIRTH' IS THE BIRTH VELOCITY OF FAST PARTICLES [M/S]
  !
  !		THE CODE COMPUTES THE POWER ABSORPTIONS FROM DRIFT KINETIC
  !		MODEL, AND THE CENTRAL DENSITY OF FAST PARTICLES FOR
  !		MARGINAL STABILITY, FOR NFAKAP DIFFERENT FAST PARTICLE
  !		DENSITY PROFILES CHARACTERIZED BY EQKAPF(JF), JF=1..NFAKAP.
  !
  !		IT CAN ALSO, FOR EACH FAST PARTICLE DENSITY PROFILE, 
  !		COMPUTE THE MARGINAL STABILITY FOR VARIOUS BULK DENSITIES
  !		AND TEMPERATURES OR MAGNETIC FIELD VALUES WHICH ARE TAKEN
  !		SO AS TO MAKE A CONSTANT BULK BETA SCAN.
  !
  !	'NFAKAP': NUMBER OF FAST PARTICLE DESITY PROFILES
  !
  !	'NBCASE': NUMBER OF CASES FOR THE CONSTANT BETA SCAN
  !
  !	'EQKAPF()': PARAMETER FOR FAST PARTICLE DENSITY PROFILE
  !
  !	'EQFAST': PARAMETER FOR FAST PARTICLE DENSITY PROFILES
  !
  !	'CEN0()': CENTRAL ION DENSITIES FOR CONSTANT BETA SCAN
  !
  !	'NBTYPE': TYPE OF CONSTANT BETA SCAN:
  !
  !	      1 ==> n_i(o) IS VARIED (CEN0()), 
  !		    T_i(o) and T_e(o) as 1/n_i(o),
  !		    Bo is kept constant.          ==> v_A(o) is varied
  !
  !	      2 ==> n_i(o) IS VARIED (CEN0()),
  !		    Bo as sqrt(n_i(o)),		  ==> v_A(o) constant
  !		    T_i(o) and T_e(o) are kept constant
  !
  !	'NLTTMP': .F. ==> SWITCH OFF TTMP BY PUTTING B_PARALLEL TO 0 IN DKE
  !			  POWER EXPRESSIONS.
  !<<<
  !---------------------------------------------------------------------------
  !
  !                  2. ANTENNA AND WALL
  !                     ----------------
  !
  !
  !     THE VARIABLE 'NANTYP' SELECTS THE TYPE OF ANTENNA.
  !
  !     NANTYP =-1 ====> "Helical volume antenna". Volume antenna currents in 
  !			the plasma between s=SAMIN and s=SAMAX, directed 
  !			along psi=const surfaces, defined by:
  !
  !			j_a = grad psi x grad sigma, 
  !
  !			with sigma(s,chi,phi) = H(s-SAMIN) * H(SAMAX-s) *
  !			( sum[j=1..MANCMP] { CURSYM(j)*cos(MPOLWN(j)*chi)
  !			 		   + CURASY(j)*sin(MPOLWN(j)*chi) } )
  !			 * exp{i*WNTORO*phi}.
  !
  !			Note that in this case there is no antenna in the 
  !			vacuum region: the vacuum contribution to the right-
  !			hand side is put to zero by setting SAUTR(j) to zero.
  !
  !     NANTYP = 1 ====> "HELICAL ANTENNA". CURRENT SHEET AT A CONSTANT
  !                      DISTANCE OF THE PLASMA SURFACE. THE CURRENTS
  !                      ARE HARMONIC FUNCTIONS OF THE POLOIDAL ANGLE
  !                      THETA, WITH POLOIDAL WAVENUMBERS GIVEN BY
  !                      'MPOLWN(J)':
  !
  !                         SAUTR(THETA) = SUM(J=1 TO MANCMP) OF
  !                         CURSYM(J)*COS(MPOLWN(J)*THETA) +
  !                         I*CURASY(J)*SIN(MPOLWN(J)*THETA).
  !
  !                      THERE ARE NO FEEDERS.
  !
  !     NANTYP = 2 ====> LFS OR HFS ANTENNA. SPECIFIED BY THE INPUT
  !                      PARAMETERS THANT(J), J=1,4 AND CURSYM(1).
  !
  !                      THANT(J) ARE ANGLES GIVEN IN DEGREES, WITH VALUES
  !                      BETWEEN 0 AND 360. THANT(J) ARE MEASURED FROM THE
  !                      MAGNETIC AXIS HORIZONTAL.
  !                      THE LFS OR HFS ANTENNA IS A CURRENT SHEET WHICH,
  !                      BETWEEN THETA = THANT(2) AND THANT(3), IS AT A
  !                      CONSTANT DISTANCE OF THE PLASMA SURFACE AND
  !                      CARRIES CONSTANT PURE POLOIDAL CURRENTS :
  !
  !                         SAUTR(THETA) = CURSYM(1)
  !
  !                      BETWEEN THETA = THANT(1) AND THETA = THANT(2)
  !                      AND THETA = THANT(3) AND THETA = THANT(4) ARE
  !                      THE FEEDERS, WHERE THE DISTANCE FROM THE
  !                      PLASMA SURFACE INCREASES SMOOTHLY UP TO THE
  !                      WALL SURFACE.
  !
  !                      THE LFS ANTENNA EXTENDS ACROSS THE THETA=0
  !                      LINE. THEREFORE THANT(3) < THANT(4) < THANT(1)
  !                      < THANT(2).
  !                      THE HFS ANTENNA CANNOT CROSS THE THETA=0
  !                      LINE. THEREFORE THANT(1) < THANT(2) < THANT(3)
  !                      < THANT(4).
  !
  !                      THE SELECTION OF EITHER LFS OR HFS ANTENNA
  !                      AUTOMATIC :
  !
  !                            THANT(3).LT.THANT(2) SELECTS LFS ANTENNA
  !                            THANT(2).GT.THANT(3) SELECTS HFS ANTENNA
  !
  !                      NOTE THAT WE MUST HAVE THANT(1) < THANT(2)
  !                      AND THANT(3) < THANT(4).
  !
  !     NANTYP = 3 ====> TOP/BOTTOM ANTENNA. THE ANTENNA SURFACE IS UP/
  !    		       DOWN SYMMETRIC, AT CONSTANT DISTANCE OF THE 
  !  		       PLASMA SURFACE BETWEEN THETA = ANTUP AND
  !		       THETA = PI - ANTUP. THE CURRENTS ARE DEFINED
  !		       AS FOR NANTYP = 1.
  !
  !     NANTYP = 4 ====> SADDLE COIL ANTENNA. THE ANTENNA SURFACE IS THE
  !		       SAME AS FOR THE HELICAL ANTENNA: CURRENT SHEET
  !		       AT A DISTANCE ANTRAD-1 OF THE PLASMA SURFACE.
  !		       THE CURRENT = CURSYM(1) IN [THANT(1),THANT(2)]
  !		       AND IN [THANT(3),THANT(4)], SMOOTHLY DECAYING
  !		       TO ZERO NEAR THANT(J).
  !
  !	'NSADDL'	SELECTS THE TYPE OF SADDLE COIL PHASING IN
  !			THE POLOIDAL PLANE.
  !			THIS IS DISCARDED UNLESS NANTYP = 4.
  !
  !	 NSADDL = 0 ==>	ONLY 1 SADDLE COIL ANTENNA IS CONNECTED:
  !			BETWEEN THANT(1) AND THANT(2).
  !	 NSADDL = 1 ==>	2 SADDLE COILS ARE CONNECTED. THE CONNECTION
  !			IS DONE IN OPPOSITE DIRECTIONS FOR THE 2
  !			COILS, THUS DEFINING A PREDOMINANTLY 'M=1'
  !			ANTENNA CURRENT COMPONENT: (+-) PHASING.
  !	 NSADDL = 2 ==>	2 SADDLE COILS ARE CONNECTED. THE CONNECTION
  !			IS DONE IN THE SAME DIRECTION FOR THE 2
  !			COILS, THUS DEFINING A PREDOMINANTLY 'M=2'
  !			ANTENNA CURRENT COMPONENT: (++) PHASING.
  !			THIS IS THE DEFAULT VALUE.
  !
  !
  !     'NLDIP' SELECTS MONOPOLE OR DIPOLE ANTENNA. THE DIPOLE OPTION
  !             HAS NOT BEEN PROGRAMMED YET. DEFAULT: F ==> MONOPOLE.
  !
  !     'ANTRAD' DISTANCE ANTENNA-MAGNETIC AXIS IN UNITS OF THE MINOR 
  !              RADIUS IN THE Z=0 PLANE.
  !
  !     'NANTSHEET' Number of antenna current sheets. For NANTSHEET>1, 
  !                 the "power at antenna" might be wrong ...
  !                 and hopefully the "power at plasma surface" is right.
  !                 The current sheets are placed equidistantly between 
  !                 ANTRAD and ANTRADMAX. The current distribution as fct 
  !                 of theta is identical for all sheets.
  !
  !     'ANTRADMAX' Max distance antenna-magnetic axis
  !
  !     'WALRAD' DISTANCE WALL-MAGNETIC AXIS IN UNITS OF THE MINOR
  !              RADIUS IN THE Z=0 PLANE. 
  !
  !     'FREQCY' IS THE GENERATOR FREQUENCY IN HZ.
  !
  !     'DELTAF' IS THE FREQUENCY INCREMENT FOR FREQUENCY TRACES.
  !
  !     'NRUN'   IS THE NUMBER OF RUNS FOR FREQUENCY TRACES.
  !
  !     'WNTORO' IS THE TOROIDAL WAVE NUMBER.
  !
  !     'WNTDEL' IS THE TOROIDAL WAVENUMBER INCREMENT FOR TOROIDAL WN SCANS
  !
  !     'NTORSP' IS THE NUMBER OF TOROIDAL WN'S FOR TOROIDAL WN SCANS
  !
  !
  !
  !                  3. OUTPUT AND PLOT
  !                     ---------------
  !
  !     'NLDISO' : SWITCH COMPUTATION AND DIAGNOSTICS OF THE SOLUTION.
  !
  !		 .TRUE. ==> THE SOLUTION IS COMPUTED EVERYWHERE. 
  !			    DIAGNOSTICS ARE PERFORMED, PRINTED AND/OR
  !			    PLOTTED ACCORDING TO NLOTP5() AND NLPLO5()
  !			    (SEE BELOW).
  !			    WITH THIS OPTION (WHICH IS THE DEFAULT)
  !			    RUNNING THE LION CODE REQUIRES SCRATCH
  !			    DISK SPACE FOR MATRIX STORAGE:
  !			        96 * NPSI * NPOL**2  (BYTES)
  !
  !		 .FALSE.==> THE SOLUTION IS COMPUTED ONLY AT THE PLASMA-
  !			    VACUUM INTERFACE.
  !			    THE ONLY DIAGNOSTIC IS THE TOTAL POWER, 
  !			    WHICH IS PERMANENT OUTPUT. IT IS CORRECT
  !			    AS LONG AS THERE IS NO SOURCE INSIDE THE 
  !			    PLASMA.
  !			    NO OTHER DIAGNOSTICS ARE PERFOMED, IRRES-
  !			    PECTIVELY OF NLOTP5() AND NLPLO5().
  !			    WITH THIS OPTION THE LION CODE DOES NOT
  !			    USE DISK SPACE FOR MATRIX STORAGE, THEREFORE
  !			    THE TURNAROUND TIME IS REDUCED.
  !
  !     'NLOTP0' : GENERAL SWITCH FOR LINE-PRINTER OUTPUT AND GRAPHICS.
  !
  !                NOTE THAT THE TOTAL POWER IS PERMANENT OUTPUT.
  !
  !     'NLOTP1()' : LINE-PRINTER OUTPUT FOR EQUILIBRIUM QUANTITIES (LION1).
  !            (1) : 
  !            (2) : 
  !            (3) :
  !            (4) :
  !            (5) :
  !
  !     'NLOTP2()' : LINE-PRINTER OUTPUT FOR VACUUM QUANTITIES (LION2).
  !            (1) : GEOMETRICAL QUANTITIES AT PLASMA SURFACE.
  !            (2) : POSITIONS OF PLASMA SURFACE, ANTENNA AND WALL.
  !            (3) : ANTENNA CURRENT POTENTIAL VS CHI AND THETA.
  !            (4) : NON-HERMICITY OF VACUUM MATRIX.
  !            (5) :
  !
  !     'NLOTP3()' : LINE-PRINTER OUTPUT FOR MATRIX CONSTRUCTION (LION3).
  !            (1) : 
  !            (2) :
  !
  !     'NLOTP4()' : LINE-PRINTER OUTPUT FOR MATRIX SOLVER (LION4).
  !            (1) : NAMELIST
  !            (2) : OHM-VECTOR
  !            (3) : SOLUTION AT PLASMA BOUNDARY
  !            (4) :
  !            (5) :
  !
  !     'NLOTP5()' : LINE-PRINTER OUTPUT FOR SOLUTION DIAGNOSTICS (LION5).
  !            (1) : NAMELIST
  !            (2) : RADIAL POWER ABSORPTIONS AND OTHER DIAGNOSTICS
  !            (3) : EXTENDED OUTPUT OF RADIAL DIAGNOSTICS
  !            (4) : 2-D POWER ABSORPTION DENSITY
  !            (5) : 2-D POWER ABSORBED IN EACH CELL
  !            (6) : 2-D NORMAL COMPONENT OF POYNTING
  !            (7) : 2-D PERP   COMPONENT OF POYNTING
  !            (8) : 2-D PARALLEL COMPONENT OF POYNTING
  !            (9) :
  !           (10) : 2-D REAL PART OF E-NORMAL
  !           (11) : 2-D REAL PART OF E-PERP
  !           (12) : 2-D IMAGINARY PART OF E-NORMAL
  !           (13) : 2-D IMAGINARY PART OF E-PERP
  !           (14) : 2-D POLARAZATION NORM OF E-PLUS SQUARED
  !           (15) : 2-D POLARIZATION NORM OF E-MINUS SQUARED
  !           (16) : ELECTRIC FIELD ON OUTER EQUATORIAL PLANE (CHI=0)
  !           (17) :
  !           (18) : POLOIDAL FOURIER COMPONENTS OF E-NORMAL IN THETA
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (19) : POLOIDAL FOURIER COMPONENTS OF E-PERP   IN THETA
  !           (20) : POLOIDAL FOURIER COMPONENTS OF E-NORMAL IN CHI
  !           (21) : POLOIDAL FOURIER COMPONENTS OF E-PERP   IN CHI
  !           (22) : 2-D EPSILON SUB-N-N - N**2 / R**2
  !           (23) : 2-D IMAGINARY PART OF EPSILON SUB N-N
  !           (24) : 2-D OMEGA - OMEGACI
  !           (25) : SHEAR ALFVEN FREQUENCIES (NEGLECTING TOROIDAL
  !                  COUPLING; FOR SINGLE SPECIES PLASMA ONLY),
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (26) : DENSITY, MINOR AND MAJOR RADIUS, IN NORMALISED AND
  !                  S.I. UNITS, ON THE OUTER EQUATORIAL PLANE (CHI=0).
  !
  !
  !           (31) : POLOIDAL FOURIER COMPONENTS OF B_N IN THETA
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (32) : POLOIDAL FOURIER COMPONENTS OF B_B IN THETA
  !           (33) : POLOIDAL FOURIER COMPONENTS OF B_PAR IN THETA
  !           (34) : POLOIDAL FOURIER COMPONENTS OF B_N IN CHI
  !           (35) : POLOIDAL FOURIER COMPONENTS OF B_B IN CHI
  !           (36) : POLOIDAL FOURIER COMPONENTS OF B_PAR IN CHI
  !
  !     THE 2-D TABLES GIVE THE VALUES ON THE CENTERS OF THE CELLS
  !  OF THE (S,CHI) MESH. A LINE IN THE TABLE CORRESPONDS TO A PSI =
  !  CONST SURFACE. IT GOES FROM CHI=0 TO CHI=PI IN THE UPPER HALF-PLANE
  !  AND FROM CHI=PI TO CHI=2*PI IN THE LOWER HALF-PLANE. THE VALUES
  !  ARE NORMALIZED TO THEIR MAXIMUM VALUE. THE FIRST AND THE LAST LINES
  !  OF THE TABLES GIVE THE POLOIDAL NUMBERING OF THE CELLS. THE FIRST
  !  COLUMN GIVES THE RADIAL NUMBERING OF THE CELLS. ALL OUTPUT IS IN
  !  CODE-NORMALIZED UNITS UNLESS SPECIFIED.
  !
  !     'NLPLO5()' : GRAPHICAL OUTPUT FOR LION5

  !            (1) : GENERAL SWITCH FOR GRAPHICAL PLOTS

  !            (2) : RADIAL POWER ABSORPTION AND FLUX

  !            (3) : FAST ION BETA_CRITICAL AND P_DK(S). WRITES TABLES 
  !                  ON TAPE26 AND TAPE27
  !                  => MATLAB (plotfast.m AND plotpdks(.,.).m)

  !            (4) : 2-D GRAPHICAL PLOTS : 
  !
  !		 - IF NPLTYP = 1 (DEFAULT): PREPARES PLOT FILES FOR USE 
  !		   WITH THE GRAPHICAL PACKAGE BASPL:
  !		   WRITES A FILE coords (TAPE18) OF (R,Z)
  !		   COORDINATES OF MESH CELLS CENTERS AND A FILE fields
  !		   (TAPE19)
  !		   OF (R,Z) COMPONENTS OF E, POWER ABSORPTION DENSITY, 
  !		   NORMAL AND BINORMAL COMPONENTS OF E, NORMAL, BINORMAL
  !		   AND PARALLEL COMPONEMTS OF B. THE 
  !		   PLOTS ARE THEN DONE WITH THE GRAPHICAL PACKAGE BASPL.
  !		   IT ALLOWS TO MAKE COLOR PLOTS, ARROW PLOTS, CONTOUR
  !		   PLOTS, ... INTERACTIVELY.
  !
  !		 - IF NPLTYP = 2 : PLOT FILE FOR USE WITH THE GRAPHICAL
  !		   PACKAGE explorer:
  !		   WRITES A FILE corfields (TAPE19) CONTAINING COORDINATES
  !	           AND FIELDS.
  !
  !            (5) : POLOIDAL FOURIER COMPONENTS (CABS) OF E_n, E_b, B_n, 
  !                  B_b AND B_//. WRITES A TABLE ON TAPE25 =>
  !                  => MATLAB  (plotfour.m).
  !
  ! NOT ACTIVE:(6) : 2-D ARROWS OF POYNTING VECTOR (NORMAL,PERP)
  ! NOT ACTIVE:(7) : 2-D ARROWS OF REAL (E-NORMAL,E-PERP) AT VARIOUS
  !                  TOROIDAL ANGLES ('ANGLET(J)' J=1 TO 'NCUT')
  ! NOT ACTIVE:(8) : 2-D CONTOURS OF NORM OF E-PLUS
  ! NOT ACTIVE:(9) : 2-D CONTOURS OF REAL(E-NORMAL) AT ANGLET(J)
  ! NOT ACTIVE:(10) : 2-D CONTOURS OF REAL(E-PERP)   AT ANGLET(J)
  ! NOT ACTIVE:(11) : 2-D CONTOURS OF POYNTING VECTOR (PARALLEL)
  ! NOT ACTIVE:(12) : 2-D LINE EPSILON SUB (N,N) - N**2/R**2 = 0
  ! NOT ACTIVE:(13) : 2-D CONTOURS OF IMAGINARY PART OF EPSILON (N,N)
  ! NOT ACTIVE:(14) : 2-D LINE(S) OMEGA=OMEGACI
  !
  !           (15) TO (25) : FREE
  !
  !     THE DIMENSION OF THE 2-D PLOTS IS SPECIFIED BY 'ALARG' AND
  !  'AHEIGT'. THE NUMBER OF CONTOUR LINES IS GIVEN BY 'NCONTR'. THE
  !  ARROWS HAVE A SIZE NORMALIZED TO THEIR MAXIMUM VALUE. THIS SIZE
  !  IS CONTROLLED BY THE VARIABLE 'ARSIZE'.
  !
  !     THE FOURIER ANALYSIS IN POLOIDAL ANGLE (CHI AND/OR THETA) IS
  !  MADE FOR VALUES OF M = MFL, MFL+1, MFL+2, .. , MFU=MFL+MD2FP1-1.
  !  THE VALUE OF 'MFL' CAN BE GIVEN IN THE INPUT NAMELIST $NEWRUN.
  !
  !     THE SAME HOLDS FOR THE "CYLINDRICAL" ALFVEN FREQUENCIES.
  !
  !
  !                  4. MESHES
  !                     ------
  !
  !
  !     THE MESH IS SPECIFIED IN THE EQUILIBRIUM CODE CHEASE.
  !  PLEASE CONSULT THE CHEASE USER MANUAL FOR THE SPECIFICATION OF THE
  !  MESH SIZE (NPSI, NCHI) AND MESH PACKING. NOTE THAT THE NUMBER OF 
  !  POLOIDAL MESH POINTS IS 'NCHI' IN THE CHEASE CODE, AND 'NPOL' IN 
  !  THE LION CODE.
  !
  !
  !                  5. NUMERICS
  !                     --------
  !
  !     THERE IS THE POSSIBILITY TO EXTRACT THE RAPID POLOIDAL PHASE
  !  VARIATION WHICH IS USUAL FOR TAE MODES (AND KINKS) BY SETTING
  !
  !     'NLPHAS'  TO TRUE
  !
  !     The poloidal phase transformation (change of variables) is defined
  !  as:
  !
  !     E(s,chi) = E~(s,chi) * exp (-i*n*q*chi_sfl),       if chi<chi_cut;
  !                E~(s,chi) * exp (-i*n*q*(chi_sfl-2pi))  if chi>chi_cut.
  !
  !  In the above, chi_sfl is the chi angle defined with the "Princeton 
  !  Jacobian" (straight field line). It needs not to coincide with chi.
  !  The angle of cut, chi_cut, is near chi=pi, more precisely at the 
  !  index j_chi = NPOL/2 + 1 (=NCHI in LION).
  !     
  !
  !
  !*********************************************************************
  !***                     EXAMPLE OF INPUT                          ***
  !*********************************************************************
  !
  !     RUN THE EQUILIBRIUM CODE CHEASE
  !     -------------------------------
  !
  !     cat >datain <<'EOF'
  !     *** THESE 4 LINES CAN INCLUDE
  !     *** ANY COMMENT.
  !     *** HOWEVER, THEY MUST BE PRESENT
  !     ***
  !      $EQDATA
  !        NMESHA=1, NPOIDA=5, SOLPDA=0.2, APLACE=1.,.996,.939,.895,.62,
  !                                        AWIDTH(1)=.4,.01,.01,.007,.1,
  !        ASPCT=.3125,CQ0=1.10,NSOUR=4,
  !        ELLIPT=1.00,GAMMA=1.6666666667,
  !        NCHI=50,NEGP=0,NER=2,NIDEAL=2,
  !        NOPT=0,NPLOT=0,NPSI=100,
  !        NS= 40,NSURF=2,NT= 40,NDIFT=1,NTEST=1,
  !        NBAL=1,NDIFPS=0,NRSCAL=1,NTMF0=1,NSYM=1,MSMAX=1,
  !       NCSCAL=1, QSPEC=1.1, CSSPEC=0.,
  !       NPP=0, AP(1)=0.,0.,0.,0.,
  !       NSTTP=0, AT(1)=1.,0.,1.,0.3,
  !       NPROFZ=0,
  !      $END
  !      $NEWRUN
  !       NITMAX=15, NV= 0, REXT=0.00000, WNTORE=3.0, AL0=-3.30E-03,
  !       NLEINQ=.F., NLGREN=.F.,NVIT=1,
  !       ITEST=1, NFIG=2, ANGLE=0,90, ARROW=0.025,
  !       NWALL = 0,EPSCON=1.0E-04,
  !       WALL=0.3,0.3,0.3,0.3,0.20,0.20,0.20,0.20,0.0,0.0,
  !       NLDIAG=16*.F., NLDIAG=4*.T.,
  !      $END OF DATA FOR ERATO VERSION 4.1
  !     EOF
  !     $HOME/CHEASE/cheasev <datain
  !     cp MEQ NSAVE NDES NVAC $HOME/
  !
  !
  !     RUN THE LION CODE
  !     -----------------
  !
  !     cat >lionin <<'EOF'
  !     *** THESE 4 LINES CAN INCLUDE
  !     *** ANY COMMENT.
  !     *** HOWEVER, THEY MUST BE PRESENT.
  !     ***
  !      $NEWRUN
  !      NRSPEC=1, CENDEN(1)=3.0E+19, RMAJOR=2.4, BNOT=1.0,
  !      EQDENS=0.5, EQKAPD=0.05, NDENS=1,
  !      NANTYP=1, ANTRAD=1.2, WALRAD=1.5, WNTORO=1., MANCMP=2,
  !      MPOLWN(1)=-1,-2, CURSYM(1)=1.,1.,1., CURASY(1)=1.,1.,1.,
  !      FREQCY=74.00E3,
  !      ANU=0.01,
  !      NRUN=3, DELTAF=1000.,
  !      CENTE=2000., EQTE=0.95,
  !      NLOTP1(1)=.F.,.F.,
  !      NLOTP2(1)=.F.,.F.,.F.,
  !      NLOTP4(1)=.F.,.F.,.F.,
  !      NLOTP5(1)=.F.,.T.,.F.,.F.,
  !      NLOTP5(18)=.F.,.F.,
  !      NLOTP5(20)=.T.,.T.,
  !      NLOTP5(25)=.F.,
  !      $END
  !     EOF
  !     cp $HOME/MEQ TAPE4
  !     cp $HOME/NSAVE TAPE8
  !     cp $HOME/NDES TAPE16
  !     cp $HOME/NVAC TAPE17
  !     $HOME/lion/l91ex < lionin
  !     cd $HOME
  !     /u2/crpp/bin/s2 tftr.527 300 8
  !
  !     (THE LAST COMMAND IS CHAINING TO ANOTHER JOB)
  !
  !
  !*********************************************************************
  !***                     INDEX OF SUBPROGRAMS                      ***
  !*********************************************************************
  !
  !
  ! VERSION 12     LDV         MAY       1991      CRPP LAUSANNE
  !
  !*********************************************************************
  !***                  1. INITIALIZE CALCULATION                    ***
  !*********************************************************************
  !
  !  MASTER          ORGANIZE CALCULATION                         1.0.02
  !
  !  LABRUN          LABEL THE RUN                                1.1.01
  !  CLEAR           CLEAR COMMON VARIABLES AND ARRAYS            1.1.02
  !  PRESET          SET DEFAULT VALUES                           1.1.03
  !  DATA            DEFINE DATA SPECIFIC TO RUN                  1.1.04
  !  AUXVAL          SET AUXILIARY VALUES                         1.1.05
  !
  !  IODSK1(1)       DISK I/O OPERATIONS FOR LION1                1.3.02
  !
  !
  !*********************************************************************
  !***     2. VACUUM CONTRIBUTION TO WEAK FORM. GREEN'S FUNCTION     ***
  !*********************************************************************
  !
  !  VACUUM          ORGANIZE VACUUM CALCULATION                  2.2.01
  !  TETMSH          THETA MESH IN VACUUM                         2.2.02
  !  ABCDEF          MATRICES A , B , C , D , E , F               2.2.03
  !  ROTETA(3)       RO , TETA AT INTEGRATION POINTS              2.2.04
  !  WALL(5)         POSITION OF THE WALL                         2.2.05
  !  CONCEL(3)       CONTRIBUTION PER CELL                        2.2.06
  !  QCON            MATRIX Q                                     2.2.07
  !  ZERO(1)         REDUCES FOR N=0                              2.2.08
  !  WCON            VACUUM MATRIX                                2.2.09
  !  HERMIC          HERMICITY CONDITION                          2.2.10
  !  MULT(4)         MULTIPLIES TWO MATRICES                      2.2.11
  !  EKN(4)          ELLIPTIC INTEGRALS                           2.2.12
  !  EK(3)           COMPLETE ELLIPTIC INTEGRALS                  2.2.13
  !  IMGC(4)         MATRIX INVERSION                             2.2.14
  !  CURRENT         ANTENNA CURRENT.JUMP OF POTENTIAL           2.2.15
  !  MATVEC          REAL MATRIX * COMPLEX VECTOR                2.2.16
  !  MOPPOW          MATRIX OPERATIONS FOR POWER AT ANTENNA      2.2.18
  !  EKN             ELLIPTICAL INTEGRAL KN AND DKN/DPETA        2.2.19
  !  EKNSIE          ELLIPTICAL INTEGRAL WITH SERIES             2.2.20
  !  EKNLIM          ELLIPTICAL INTEGRAL WITH BESSEL             2.2.21
  !  BESMDI          MODIFIED BESSEL FUNCTION I                  2.2.22
  !  BESMDK          MODIFIED BESSEL FUNCTION K                  2.2.23
  !  NUM             UP/DOWN/UP/DOWN NUMBERING                   2.2.24
  !
  !  IODSK2          DISK I/O OPERATIONS FOR LION2               2.3.01
  !
  !
  !********************************************************************
  !***   3. MATRIX CONSTRUCTION AND RESOLUTION WITH FRONTAL METHOD  ***
  !********************************************************************
  !
  !  ORGAN4          MAIN SUBROUTINE                             4.1.01
  !
  !  FRONTB          ORGANIZE FRONTAL METHOD                     4.2.01
  !  INTEGR          CONTRIBUTION OF ONE CELL TO MATRIX A        4.2.02
  !  RHSHYB	   CONTRIBUTION OF ONE CELL TO RIGHT-HAND SIDE 4.2.02B
  !  ADDVAC          ADD VACUUM CONTRIBUTION                     4.2.03
  !  CALD            DECOMPOSE A BLOCK OF A INTO LDU             4.2.04
  !  CDLHXV          SOLVE UPPER TRIANGULAR SYSTEM               4.2.05
  !  GETRG           GET A SECTION OF VECTOR XT                  4.2.06
  !  PUTRG           PUT A VECTOR INTO XT                        4.2.07
  !  POWER           COMPUTE TOTAL POWER                         4.2.08
  !
  !  DEC             GLOBAL NUMBERING OF UNKNOWNS IN A CELL      4.3.01
  !  AHYBRD          CONSTRUCTS LOCAL 6*6 MATRIX (HYBRID ELEM.)  4.3.03
  !  AWAY            REMOVE A COLUMN AND A ROW OF LOCAL MATRIX   4.3.04
  !  STORE           ADD LOCAL CONTRIB. TO MATRIX BLOCK          4.3.05
  !
  !  QUAEQU          PHYSICAL LOCAL QUANTITIES                   4.4.01
  !  BASIS2          BASIS FUNCTIONS OF LINEAR HYBRID ELEMENTS   4.4.02
  !  CONST1          COEFFICIENTS C-J OF WEAK FORM TERMS         4.4.03
  !  CONST2          COEFFs. OF UNKNOWNS OF W.F. TERMS           4.4.04
  !  CONST3          COEFFs. OF UNKNOWNS OF W.F. TERMS (NLPHAS)  4.4.04
  !  VECT            MULTIPLY W.F.TERMS WITH BASIS FUNCTIONS     4.4.05
  !  DIADIC          CONTRIBUTION OF ONE W.F.TERM TO MATRIX      4.4.06
  !  ADD             ADD CONTRIB. OF ONE TERM TO LOCAL MATRIX    4.4.07
  !
  !  FRPROF          PROFILES OF ION DENSITIES                   4.5.01
  !  SHAPE           SHAPE OF PROFILE                            4.5.02
  !  DAMPIN          WAVE PHENOMENOLOGICAL DAMPING               4.5.03
  !  TEMPI           PARALLEL TEMPERATURE OF ION SPECIES         4.5.04
  !  TEMPRP          PERP. TEMPERATURE OF ION SPECIES            4.5.05
  !  TEMPEL          ELECTRON TEMPERATURE                        4.5.06
  !  FASTDR          K-PERP OF FAST WAVE FROM DISPERSION REL.    4.5.07
  !  DISPFN          FRIED-CONTE DISPESION FUNCTION              4.5.08
  !  ERRFC           ERROR FUNCTION                              4.5.09
  !  BESSEL          BESSEL ROUTINE FOR ERROR                    4.5.10
  !  BESHHH          BESSEL ROUTINE BY BACKWARD RECURSION        4.5.11
  !  CVZERO          ZEROES A COMPLEX VECTOR                     4.5.12
  !
  !  CAXPY           Y = Y + A * X  (COMPLEX)     ($SCILIB)        VS01
  !  CCOPY           COPIES X ONTO Y              ($SCILIB)        VS02
  !  CDOTU           SCALAR PRODUCT X*Y           ($SCILIB)        VS03
  !  CSCAL           SCALES A VECTOR BY A CONSTANT($SCILIB)        VS04
  !  CDOTC           SCALAR PRODUCT CONJG(X)*Y    ($SCILIB)        VS05
  !
  !  IODSK3          DISK I/O FOR LION3                          4.6.01
  !  IODSK4          DISK I/O FOR LION4                          4.6.02
  !
  !
  !
  !********************************************************************
  !***      4. DIAGNOSTICS, LINE-PRINTER AND GRAPHICAL OUTPUT       ***
  !********************************************************************
  !
  !  DIAGNO          ORGANIZE THE DIAGNOSTICS                    5.1.01
  !
  !  INIT            INITIALIZE LION5                            5.2.01
  !  PHYSQ           POWER, ENERGY AND FIELDS DIAGNOSTICS        5.2.02
  !  FOURIE          COMPLEX FOURIER ANALYSIS IN CHI             5.2.04
  !  LANDAU          ELECTRON LANDAU DAMPING RATE OF ALFVEN WAVE 5.2.06
  !  THEEND          TERMINATE THE CALCULATION                   5.2.99
  !
  !  CENTER          VALUE OF UNKNOWNS AT CENTER OF CELL         5.3.01
  !  ELECTR          ELECTRIC FIELD,NORMAL AND PERP              5.3.02
  !  LOCPOW          LOCAL POWER ABSORPTION                      5.3.03
  !  MAGNET          WAVE MAGNETIC FIELD                         5.3.04
  !  POYNTI          POYNTING VECTOR                             5.3.05
  !  SFLINT          CONTRIBUTION TO POYNTING FLUX               5.3.06
  !
  !  OUTP5           LINE-PRINTER OUTPUT                         5.4.01
  !  TABLE2          2-D PRINTOUT                                5.4.02
  !  TABLEF          PRINTOUT OF POLOIDAL FOURIER COMPONENTS     5.4.03
  !  PLOT5           GRAPHICAL OUTPUT                            5.4.04
  !  ARROW           2-D VECTOR FIELD PLOT                       5.4.05
  !  SCAPLO          2-D SCALAR FIELD PLOT                       5.4.06
  !  SURFPL          PLOT THE PLASMA SURFACE                     5.4.07
  !  LABPLO          PLOT THE LABELS                             5.4.08
  !  LEVEL           2-D SCALAR PLOT : CONTOUR LINES             5.4.10
  !  LZERO           PLOT THE ZEROS OF VOUT1                     5.4.11
  !  CONTOR          PLOT THE LINE VOUT1=0.0                     5.4.13
  !  ACROSS          LINEAR INTERPOLATION                        5.4.14
  !
  !  IODSK5          DISK I/O OPERATIONS FOR LION5               5.5.01
  !
  !
  !*********************************************************************
  !***                    INDEX OF COMMON BLOCKS                     ***
  !*********************************************************************
  !
  ! VERSION 11     LDV      MAY 1991           CRPP LAUSANNE
  !
  !L                  1.       GENERAL OLYMPUS DATA
  !  COMBAS          BASIC SYSTEM PARAMETERS                        C1.1
  !
  !L                  2.       PHYSICAL PROBLEM
  !  COMPHY          GENERAL PHYSICS VARIABLES                      C2.1
  !  COMEQU          EQUILIBRIUM QUANTITIES                         C2.2
  !
  !L                  3.       NUMERICAL SCHEME
  !  COMESH          (R,Z) AND (PSI,CHI) MESH VARIABLES             C3.1
  !  COMNUM          NUMERICAL VARIABLES                            C3.2
  !  COMAUX          AUXILIARY VARIABLES FOR LION3 AND 4            C3.3
  !  COMVEC          VECTORS FOR LION4                              C3.4
  !  COMIVI          NUMERICAL VARIABLES FOR LION4                  C3.5
  !
  !L                  5.       I/O AND DIAGNOSTICS
  !  COMCON          CONTROL VARIABLES                              C5.1
  !  COMOUT          I/O DISK CHANNELS NUMBERS                      C5.2
  !
  !L                  9.       BLANK COMMON
  !  COMVID          VACUUM QUANTITIES FOR LION2                    C9.2
  !  COMMTR          MATRIX BLOCKS FOR LION3 AND LION4              C9.3
  !  COMPLO          OUTPUT AND PLOT QUANTITIES FOR LION5           C9.5
  !
  !
  !*********************************************************************
  !***            ALPHABETIC INDEX OF ALL COMMON VARIABLES           ***
  !*********************************************************************
  !
  ! VERSION 12     LDV      MAY 1991         CRPP LAUSANNE
  !
  !  A1D(MDLENG)       MATRIX BLOCK OF DISCRETIZED WEAK FORM        CA 9.3
  !  ABSPOW(MDPSI)   POWER ABSORBED ON PSI=CONST.                 RA 9.5
  !  ACHARG(MDSPEC)  *ATOMIC CHARGES OF ION SPECIES               RA 2.1
  !  AD(10)	   *COEFF. FOR POLYNOMIAL DENSITY PROFILE	RA 2.1
  !  AHEIGT          *HEIGHT OF 2-D PLOTS                         R  5.1
  !  ALARG           *WIDTH OF 2-D PLOTS                          R  5.1
  !  AMASS(MDSPEC)   *ATOMIC MASSES OF ION SPECIES                RA 2.1
  !  AMASSE          *ATOMIC MASS OF ELECTRON                     R  2.1
  !  ANGLET(16)      *TOROIDAL CUTS (DEGREES)                     RA 5.1
  !  ANTR(MDPOL)     ANTENNA VECTOR                               CA 9.2
  !  ANTRAD          *ANTRAD-1.=DISTANCE ANTENNA-PLASMA           R  2.1
  !  ANTUP           *UPPER RIGHT POSITION OF TOP/BOTTOM ANTENNA  R  2.1
  !  ANU             *COLLISIONAL DAMPING NU/OMEGA                R  2.1
  !  APHI            TOROIDAL ANGLE PHI                           R  9.5
  !  ARSIZE          *SIZE OF ARROWS                              R  5.1
  !  ASPCT           *INVERSE ASPECT RATIO FOR SOLOVEV EQUILIBRIU R  2.1
  !  ASYMB           *SIZE OF SYMBOLS                             R  5.1
  !  BETA            BETA VALUE                                   R  2.1
  !  BETAP           BETA POLOIDAL                                R  2.1
  !  BETAS           BETA STAR PRINCETON                          R  2.1
  !  BNL             NORMAL COMPONENT OF WAVE MAGNETIC FIELD      C  9.5
  !  BNOT            *MAGNETIC FIELD AT MAGNETIC AXIS (TESLA)     R  2.1
  !  BPARL           PARALLEL COMP. OF WAVE MAGNETIC FIELD        C  9.5
  !  BPL             PERP. COMP. OF WAVE MAGNETIC FIELD           C  9.5
  !  CA              CONSTANT TO DEFINE WALL                      R  9.2
  !  CA(MDOVL)       OVERLAP SUBBLOCK OF MATRIX A                 CA 3.3
  !  CB              #                                            R  9.2
  !  CC              #                                            R  9.2
  !  CCHI(MDPOL)     CHI VALUES AT CENTER OF CELLS                RA 9.5
  !  CCR(MDPSI,MDPOL)
  !                  R AT CENTER OF CELLS                         RA 9.5
  !  CCS(MDPSI)      S VALUES AT CENTER OF CELLS                  RA 9.5
  !  CCZ(MDPSI,MDPOL)
  !                  Z AT CENTER OF CELLS                         RA 9.5
  !  CDEVIA          WIDTH OF POWER DISTRIBUTION IN CHI           R  9.5
  !  CELPOW(MDPSI,MDPOL)
  !                  POWER ARBSORBED IN THE CELLS                 RA 9.5
  !  CEN0(MDBCAS)	   *DENSITIES FOR CONST BETA SCAN OF DKE STAB.  RA 2.1
  !  CENDEN(MDSPEC)  *DENSITIES OF ION SPECIES AT MAGN.AXIS (M-3) RA 2.1
  !  CENTE           *ELECTRON TEMPERATURE AT MAGNETIC AXIS       R  2.1
  !  CENTI(MDSPEC)   *ION TEMPERATURES AT MAGN.AXIS (EV)          RA 2.1
  !  CENTIP(MDSPEC)  *PERP.ION TEMPERATURES AT MAGN. AXIS (EV)    RA 2.1
  !  CEOMCI(MDSPEC)  *NORMALIZED ION CYCLOTRON FREQUENCIES        RA 2.1
  !  CHI(MDCHI1)     CHI MESH VALUES                              RA 3.1
  !  CHIPOW(MDPOL)   POWER ABSORBED ON CHI=CONST.                 RA 9.5
  !  CMEAN           CENTER OF POWER DISTRIBUTION IN CHI          R  9.5
  !  CNR(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., R COMPONENT            RA 9.5
  !  CNZ(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., Z COMPONENT            RA 9.5
  !  CONA(6,6)       LOCAL (CELL) CONTRIBUTION TO A               CA 3.3
  !  COST(MDPSI,MDPOL)COSINUS OF ANGLE BETWEEN PHI AND B_0        RA 9.5
  !  CPL             POWER ABSORBED IN A CELL                     R  9.5
  !  CPLE            POWER IN A CELL DUE TO NU                    R  9.5
  !  CPLJ(MDSPC2)    POWER IN A CELL PER SPECIES                  RA 9.5
  !  CPSRF           *PSI AT PLASMA SURFACE                       R  2.1
  !  CURASY(5)  *AMPLITUDE OF SIN ANTENNA CURRENT (HELICAL)  RA 2.1
  !  CURSYM(5)  *AMPLITUDE OF ANTENNA CURRENT                RA 2.1
  !  D2ROP           D**2RHO/DTHETA**2 AT MID-POINT               R  9.2
  !  DC(MD2CP2)      DELTA-CHI                                    RA 9.2
  !  DELTA           *PHENOMENOLOGICAL DAMPING                    R  2.1
  !  DELTAF          *FREQUENCY INCREMENT FOR FREQUENCY TRACE     R  2.1
  !  DELTH(MD2CP2)   DELTA-THETA (TH(K+1)-TH(K))                  RA 9.2
  !  DENPOW(0:MDPSI,MDPOL1)
  !                  POWER ABSORPTION DENSITY                     RA 9.5
  !  DEPOS(MDPSI)    POWER DENS. AVERAGED ON PSI=CONST.           RA 9.5
  !  DPL             POWER ABSORPTION DENSITY AT CENTER OF CELL   R  9.5
  !  DPLE            POWER ABSORPTION DENSITY DUE TO NU           R  9.5
  !  DPLJ(MDSPC2)    POWER DENSITY PER SPECIES                    RA 9.5
  !  DRODT(4)        DRHO/DTHETA AT INTEGRATION POINTS            RA 9.2
  !  DROPDT(4)       DRHOPRIME/DTHETA                             RA 9.2
  !  DVDC            DV/DCHI                                      C  9.5
  !  DXDC            DX/DCHI                                      C  9.5
  !  DXDS            DX/DS                                        C  9.5
  !  ECHEL           SCALE FACTOR FOR 2-D PLOTS                   R  9.5
  !  ELEPOW          POWER DUE TO NU                              R  9.5
  !  ELLIPT          *ELLIPTICITY SQUARED FOR SOLOVEV EQUILIBRIUM R  2.1
  !  ENL             NORMAL COMPONENT OF WAVE ELECTRIC FIELD      C  9.5
  !  EPL             PERP. COMP. OF WAVE ELECTRIC FIELD           C  9.5
  !  EPSMAC          *ROUND-OFF ERROR OF COMPUTER                 R  3.5
  !  EQ(MDEQ,MDPOL,MDPSI1)  EQUILIBRIUM QUANTITIES                RA 2.2
  !  EQALFD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQDENS          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQFAST	   *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQKAPF(MDFAKA)  *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPT(MDSPEC)  *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  EQKPTE          *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTE            *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTI(MDSPEC)    *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  FEEDUP          *POSITION OF UPPER RIGHT FEED OF T/B ANTENNA R  2.1
  !  FLUPOW(MDPSI1)  POWER ABSORBED INSIDE PSI=CONST.             RA 9.5
  !  FLUPSP(MDPSI1,MDSPC2)
  !                  POWER ABSORBED INSIDE PSI=CONST.,PER SPECIES RA 9.5
  !  FRAC(MDSPEC)    *MASS FRACTION OF ION SPECIES                RA 2.1
  !  FRCEN(MDSPEC)   *CENTER OF ION DENSITY PROFILE               RA 2.1
  !  FRDEL(MDSPEC)   *WIDTH OF ION DENSITY PROFILE                RA 2.1
  !  FREN(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-N          CA 9.5
  !  FREP(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-PERP       CA 9.5
  !  FREQCY          *FREQUENCY OF GENERATOR (HZ)                 R  2.1
  !  FRINT(7,MDCHI)  INTEGRAL FOR FOURIER DECOMPOSITION           CA 9.5
  !  G(MDPOL,MDPOL,13)
  !                  ALL MATRICES                                 RA 9.2
  !  GAMMA           ADIABATICITY INDEX (NOT IN USE)              R  2.1
  !  LENGTH          *NB.OF ELEMENTS OF A MATRIX BLOCK            I  3.2
  !  M1              RANK OF MATRIX OVERLAP SUBBLOCK              I  3.5
  !  M11             M1+1                                         I  3.5
  !  M12             M1+M2 = RANK OF MATRIX BLOCK                 I  3.5
  !  M2              RANK OF MATRIX BLOCK - M1                    I  3.5
  !  MANCMP          *NB.OF POLOIDAL WAVE NUMBERS (HELICAL ANT.)  I  2.1
  !  MEQ             *EQUILIBIUM QUANTITIES I/O CHANNEL           I  5.2
  !  MFL             *LOWER M VALUE FOR FOURIER ANALYSIS          I  5.1
  !  MFU             UPPER M VALUE FOR FOURIER ANALYSIS           I  5.1
  !  MPOLWN(5)       *POLOIDAL WAVE NUMBERS (HELICAL ANT.)        IA 2.1
  !  N               NB. OF MATRIX BLOCKS (=NPSI)                 I  3.5
  !  NANTYP          *SELECTS TYPE OF ANTENNA   			I  2.1
  !  NBCASE	   *NB. OF CASES FOR CONST BETA SCAN DKE STAB.  I  2.1
  !  NBTYPE	   *TYPE OF CONST BETA SCAN DKE STAB.		I  2.1
  !  NCHI            *NUMBER OF CHI INTERVALS IN UPPER HALF-PLANE I  3.1
  !  NCOLMN          *RANK OF A MATRIX BLOCK                      I  3.2
  !  NCOMP           NB. OF ELEMENTS OF SOLUTION VECTOR           I  3.5
  !  NCONTR          *NUMBER OF CONTOUR LINES                     I  5.1
  !  NDA             *MATRIX A I/O CHANNEL                        I  5.2
  !  NDARG	   *ARGUMENT FOR POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDDEG	   *DEGREE OF POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDENS           *SELECTS TYPE OF DENSITY PROFILE             R  2.1
  !  NDES            *R,Z COORDINATES AND NORMALS I/O CHANNEL     I  5.2
  !  NDIM            DIMENSION OF MATRICES                        I  9.2
  !  NDLT            *DECOMPOSED MATRIX L,D,U I/O CHANNEL         I  5.2
  !  NDS             *SOLUTION VECTOR I/O CHANNEL                 I  5.2
  !  NCUT            *NUMBER OF TOROIDAL CUTS FOR PLOTS           I  5.1
  !  NFAKAP	   *NB. OF FAST PARTICLE PROFILES FOR DKE STAB. I  2.1
  !  NHARM           *NUMBER OF ION CYCLOTRON HARMONICS           I  2.1
  !  NLCOLD          *SELECTS COLD/WARM IONS PLASMA MODEL         L  2.1
  !  NLCOLE	   *SELECTS COLD/WARM ELECTRONS (FAST W. DAMP.) L  2.1
  !  NLDIP           *SELECTS MONO/DIPOLE ANTENNA FOR LFS OR HFS  L  2.1
  !  NLONG           NB. OF ELEMENTS IN A MATRIX BLOCK            I  3.5
  !  NLDISO	   *SWITCH COMPUTATION DIAGNOSTICS OF SOLUTION  L  5.1
  !  NLPHAS          *SWITCH POLOIDAL PHASE EXTRACTION            L  5.1
  !  NLOTP0          *GENERAL LINE-PRINTER OUTPUT SWITCH          L  5.1
  !  NLOTP1(4)       *OUTPUT SWITCHES FOR LION1                   LA 5.1
  !  NLOTP2(5)       *OUTPUT SWITCHES FOR LION2                   LA 5.1
  !  NLOTP3(2)       *OUTPUT SWITCHES FOR LION3                   LA 5.1
  !  NLOTP4(5)       *OUTPUT SWITCHES FOR LION4                   LA 5.1
  !  NLOTP5(40)      *OUTPUT SWITCHES FOR LION5                   LA 5.1
  !  NLPLO5(25)      *PLOT SWITCHES FOR LION5                     LA 5.1
  !  NLQUAD          QUADRATIC TERM IN THE WEAK FORM              L  3.3
  !  NPLAC(6)        GLOBAL NUMBERING OF CELL MESH POINTS         IA 3.3
  !  NPLTYP	   *=1 FOR BASPL PLOTS, =2 FOR EXPLORER PLOTS   I  5.1
  !  NPOL            *TOTAL NUMBER OF CHI INTERVALS               I  3.1
  !  NPRNT           *LINE-PRINTER OUTPUT                         I  5.2
  !  NPSI            *NUMBER OF S INTERVALS                       I  3.1
  !  NRSPEC          *NUMBER OF ION SPECIES                       I  2.1
  !  NRUN            *NUMBER OF RUNS FOR FREQUENCY SWEEP          I  5.1
  !  NRZSUR          NUMBER OF POINTS FOR PLOTTING THE SURFACE    I  9.5
  !  NSADDL	   *TYPE OF POLOIDAL PHASING FOR SADDLE COILS   I  2.1
  !  NSAVE           *NAMELIST I/O CHANNEL                        I  5.2
  !  NSHIFT          PERFORM SHIFT IN NQ                          I  3.1
  !  NSING           SINGULARITY INDICATOR (=-1 IF A IS SING.)    I  3.5
  !  NTEMP           *SELECTS TYPE OF TEMPERATURE PROFILES        I  2.1
  !  NTORSP	   *NUMBER OF TOR. WN'S FOR THE TOR.WN SCAN	I  5.1
  !  NUMBER          *RUN NUMBER                                  I  3.2
  !  NVAC            *VACUUM QUANTITIES I/O CHANNEL               I  5.2
  !  OHMR(MDPOL)     OHM VECTOR                                   CA 9.2
  !  OMEGA           *NORMALIZED FREQUENCY (*RMAJOR/ALFV.SPEED)   R  2.1
  !  QB              SAFETY FACTOR AT PLASMA SURFACE              R  9.2
  !  QIAXE           *1./Q(AXIS) FOR SOLOVEV EQUILIBRIUM          R  2.1
  !  QS(MDPSI1)      Q AT EDGES OF CELLS                          RA 2.1
  !  QTILDA(MDPSI1)  Q AT CENTERS OF CELLS                        RA 2.1
  !  REASCR          REACTANCE SCALAR                             C  9.2
  !  RMAJOR          *MAJOR RADIUS (M)                            R  2.1
  !  RO(4)           RHO AT INTEGRATION POINTS                    RA 9.2
  !  RO2             RHO AT THE CENTER OF CHI INTERVAL            R  9.2
  !  RO2P            RHO PRIME AT THE CENTER OF CHI INTERVAL      R  9.2
  !  ROEDGE(MD2CP2)  RHO PLASMA SURF. AT EDGES OF CHI INTERVALS   RA 9.2
  !  ROMID(MD2CP2)   RHO PLASMA SURF. AT MIDDLE CHI INTERVALS     RA 9.2
  !  ROP(4)          RHO PRIME AT INTEGRATION POINTS              RA 9.2
  !  SAMIN	   *INSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAMAX	   *OUTSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAUTR(MDPOL)    JUMP ACROSS ANTENNA SURFACE                  CA 9.2
  !  SAUTX(MDPOL)    TEMPORARY                                    CA 9.2
  !  SDEVIA          WIDTH OF POWER DISTRIBUTION IN S             R  9.5
  !  SFLUX(MDPSI)    POYNTING FLUX ACROSS PSI=CONST.              RA 9.5
  !  SIGMA           *NORM FACTOR FOR V-THEMAL (IONS)             R  2.1
  !  SINT(MDPSI,MDPOL)SINUS OF ANGLE BETWEEN PHI AND B_0          RA 9.5
  !  SMEAN           MINOR RADIUS OF HALF POWER ABSORPTION        R  9.5
  !  SN(MDPSI,MDPOL) NORMAL COMPONENT OF POYNTING                 RA 9.5
  !  SNL             NORMAL COMPONENT OF POYNTING                 R  9.5
  !  SOURCE(MDPOL)   SOURCE VECTOR ON PLASMA BOUNDARY             CA 9.2
  !  SOURCT(MDCOMP)  TOTAL VECTOR OF RIGHT-HAND SIDE		CA 3.4
  !  SPAR(MDPSI,MDPOL)
  !                  PARALLEL COMPONENT OF POYNTING               RA 9.5
  !  SPARL           PARALLEL COMPONENT OF POYNTING               R  9.5
  !  SPERP(MDPSI,MDPOL)
  !                  PERP. COMPONENT OF POYNTING                  RA 9.5
  !  SPL             PERP. COMP. OF POYNTING                      R  9.5
  !  SR(MD2CP2)      R(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  SURPSI(MDPSI1)  INTEGRAL FOR PSI-SURFACE                     RA 9.5
  !  SZ(MD2CP2)      Z(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  T(MD2CP2)       THETA AT MIDDLE CHI INTERVALS                RA 9.2
  !  TB              TOROIDAL MAGNETIC FLUX AT PLASMA SURFACE     R  9.2
  !  TH(MD2CP2)      THETA AT EDGES OF CHI INTERVALS              RA 9.2
  !  THANT(4)	   *THETA OF SADDLE COILS TOROIDAL SECTIONS	RA 2.1
  !  TP(4)           THETA PRIME AT INTEGRATION POINTS            RA 9.2
  !  TT(4)           THETA AT INTEGRATION POINTS                  RA 9.2
  !  U(MDCOL)        VECTOR OF UNKNOWS FOR ONE BLOCK              CA 3.4
  !  UT              TEMPORARY STORAGE                            C  3.4
  !  VA(MDCOL)       TEMPORARY STORAGE                            CA 3.4
  !  VC              V                                            C  9.5
  !  VOUT1(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  VOUT2(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  WALRAD          *1.-WALRAD = DISTANCE WALL-PLASMA            R  2.1
  !  WBETCH          NON-ORTHOGONALITY OF PSI,CHI (BETA-CHI)      R  2.2
  !  WBPOL2          POLOIDAL FIELD SQUARED                       R  2.2
  !  WBTOR2          TOROIDAL FIELD SQUARED                       R  2.2
  !  WBTOT           TOTAL FIELD                                  R  2.2
  !  WBTOT2          TOTAL FIELD SQUARED                          R  2.2
  !  WCHI            CHI AT CENTER OF THE CELL                    R  2.2
  !  WCOMEG          COMPLEX NORMALIZED FREQUENCY                 C  2.2
  !  WDCHI           CHI WIDTH OF CELL                            R  2.2
  !  WDCR2J          D/DCHI ( LOG(R**2/J) )                       R  2.2
  !  WDS             S WIDTH OF CELL                              R  2.2
  !  WEPS            DIELECTRIC TENSOR COMPONENT EPSILON N-N      C  2.2
  !  WEPSEL          ELECTRON CONTRIBUTION TO DIELECTRIC TENSOR   C  2.2
  !  WEPSI(MDSPEC)   ION CONTRIBUTIONS TO WEPS                    CA 2.2
  !  WEPSMC          PHENOMENOLOGICAL DAMPING CONTRIB. TO DIEL.   C  2.2
  !  WFRAC(MDSPEC)   MASS FRACTION OF ION SPECIES                 RA 2.2
  !  WG              DIELECTRIC TENSOR COMPONENT EPSILON N-PERP   C  2.2
  !  WGI(MDSPEC)     ION CONTRIBUTIONS TO WG                      CA 2.2
  !  WGRPS2          GRADIENT OF PSI SQUARED                      R  2.2
  !  WH              COEFFICIENT H                                R  2.2
  !  WJAC            JACOBIEN OF PSI,CHI COORDINATE SYSTEM        R  2.2
  !  WK              COEFFICIENT K                                R  2.2
  !  WNTDEL	   *TOR.WN. INCREMENT FOR TOR.WN. SCAN		R  5.1
  !  WNTORO          *TOROIDAL WAVE NUMBER N                      R  2.1
  !  WNU             WAVE DAMPING                                 R  2.2
  !  WOMCI(MDSPEC)   NORMALIZED ION-CYCLOTRON FREQUENCY           RA 2.2
  !  WPSI            PSI                                          R  2.2
  !  WQ              SAFETY FACTOR Q                              R  2.2
  !  WR2             R SQUARED                                    R  2.2
  !  WRHO            MASS DENSITY                                 R  2.2
  !  WS              S COORDINATE AT CENTER OF THE CELL           R  2.2
  !  WT              TOROIDAL FLUX FUNCTION T                     R  2.2
  !  WTQ             R**2 / J                                     R  2.2
  !  X(MDCOL)        VECTOR OF UNKNOWNS FOR ONE BLOCK             CA 3.4
  !  XC              X                                            C  9.5
  !  XC1(8)          CONSTANTS C-J OF WEAK FORM TERMS             CA 3.3
  !  XDCHI(MDPOL)    DELTA-CHI AT PLASMA SURFACE                  RA 3.4
  !  XDTH(MDPOL)     DELTA-THETA AT PLASMA SURFACE                RA 3.4
  !  XETA(5)         CONSTANTS OF TEST-FUNCTIONS                  CA 3.3
  !  XF(16)          BASIS FUNCTIONS AT MESH POINTS               RA 3.3
  !  XKSI(5)         CONSTANTS OF UNKNOWNS                        CA 3.3
  !  XM(6,6)         CONTRIBUTION OF ONE TERM OF THE WEAK FORM    CA 3.3
  !  XOHMR(MDPOL)    OHM VECTOR FOR POWER AT ANTENNA              CA 3.4
  !  XS(MDRZ)        R COORDINATES OF PLASMA SURFACE              RA 9.5
  !  XT(MDCOMP)      TOTAL VECTOR OF UNKNOWNS                     CA 3.4
  !  XVETA(6)        VECTOR OF COEFFICIENTS FOR TEST-FUNCTIONS    CA 3.3
  !  XVKSI(6)        VECTOR OF COEFFICIENTS FOR UNKNOWNS          CA 3.3
  !  YS(MDRZ)        Z COORDINATES OF PLASMA SURFACE              RA 9.5
  !
  !
  !*********************************************************************
  !***    ALPHABETIC INDEX OF COMMON VARIABLES COMMON PER COMMON     ***
  !*********************************************************************
  !
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !
  !L                  C2.1     GENERAL PHYSICS VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMPHY/
  !
  !  ACHARG(MDSPEC)  *ATOMIC CHARGES OF ION SPECIES               RA 2.1
  !  AD(10)	   *COEFF. FOR POLYNOMIAL DENSITY PROFILE	RA 2.1
  !  AMASS(MDSPEC)   *ATOMIC MASSES OF ION SPECIES                RA 2.1
  !  AMASSE          *ATOMIC MASS OF ELECTRON                     R  2.1
  !  ANTRAD          *ANTRAD-1.=DISTANCE ANTENNA-PLASMA           R  2.1
  !  ANTUP           *UPPER RIGHT POSITION OF TOP/BOTTOM ANTENNA  R  2.1
  !  ANU             *CAUSAL DAMPING ADDED TO DIELECTRIC TENSOR   R  2.1
  !  ASPCT           *INVERSE ASPECT RATIO FOR SOLOVEV EQUILIBRIU R  2.1
  !  BETA            BETA VALUE                                   R  2.1
  !  BETAP           BETA POLOIDAL                                R  2.1
  !  BETAS           BETA STAR PRINCETON                          R  2.1
  !  BNOT            *MAGNETIC FIELD AT MAGNETIC AXIS (TESLA)     R  2.1
  !  CEN0(MDBCAS)	   *DENSITIES FOR CONST BETA SCAN OF DKE STAB.  RA 2.1
  !  CENDEN(MDSPEC)  *DENSITIES OF ION SPECIES AT MAGN.AXIS (M-3) RA 2.1
  !  CENTE           *ELECTRON TEMPERATURE AT MAGNETIC AXIS       R  2.1
  !  CENTI(MDSPEC)   *ION TEMPERATURES AT MAGN.AXIS (EV)          RA 2.1
  !  CENTIP(MDSPEC)  *PERP.ION TEMPERATURES AT MAGN. AXIS (EV)    RA 2.1
  !  CEOMCI(MDSPEC)  *NORMALIZED ION CYCLOTRON FREQUENCIES        RA 2.1
  !  CPSRF           *PSI AT PLASMA SURFACE                       R  2.1
  !  CURASY(5)  *AMPLITUDE OF SIN ANTENNA CURRENT (HELICAL)  RA 2.1
  !  CURSYM(5)  *AMPLITUDE OF ANTENNA CURRENT                RA 2.1
  !  DELTA           *PHENOMENOLOGICAL DAMPING                    R  2.1
  !  DELTAF          *FREQUENCY INCREMENT FOR FREQUENCY TRACE     R  2.1
  !  ELLIPT          *ELLIPTICITY SQUARED FOR SOLOVEV EQUILIBRIUM R  2.1
  !  EQALFD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQDENS          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQFAST	   *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQKAPF(MDFAKA)  *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPT(MDSPEC)  *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  EQKPTE          *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTE            *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTI(MDSPEC)    *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  FEEDUP          *POSITION OF UPPER RIGHT FEED OF T/B ANTENNA R  2.1
  !  FRAC(MDSPEC)    *MASS FRACTION OF ION SPECIES                RA 2.1
  !  FRCEN(MDSPEC)   *CENTER OF ION DENSITY PROFILE               RA 2.1
  !  FRDEL(MDSPEC)   *WIDTH OF ION DENSITY PROFILE                RA 2.1
  !  FREQCY          *FREQUENCY OF GENERATOR (HZ)                 R  2.1
  !  GAMMA           ADIABATICITY INDEX (NOT IN USE)              R  2.1
  !  NBCASE	   *NB. OF CASES FOR CONST BETA SCAN DKE STAB.  I  2.1
  !  NBTYPE	   *TYPE OF CONST BETA SCAN DKE STAB.		I  2.1
  !  NDARG	   *ARGUMENT FOR POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDDEG	   *DEGREE OF POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDENS           *SELECTS TYPE OF DENSITY PROFILE             R  2.1
  !  NFAKAP	   *NB. OF FAST PARTICLE PROFILES FOR DKE STAB. I  2.1
  !  NHARM           *NUMBER OF ION CYCLOTRON HARMONICS           I  2.1
  !  NSADDL	   *TYPE OF POLOIDAL PHASING OF SADDLE COILS    I  2.1
  !  NTEMP           *SELECTS TYPE OF TEMPERATURE PROFILES        I  2.1
  !  OMEGA           *NORMALIZED FREQUENCY (*RMAJOR/ALFV.SPEED)   R  2.1
  !  QIAXE           *1./Q(AXIS) FOR SOLOVEV EQUILIBRIUM          R  2.1
  !  QS(MDPSI1)      Q AT EDGES OF CELLS                          RA 2.1
  !  QTILDA(MDPSI1)  Q AT CENTERS OF CELLS                        RA 2.1
  !  RMAJOR          *MAJOR RADIUS (M)                            R  2.1
  !  SAMIN	   *INSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAMAX	   *OUTSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SIGMA           *NORM FACTOR FOR V-THEMAL (IONS)             R  2.1
  !  THANT(4)	   *THETA OF SADDLE COILS TOROIDAL SECTIONS	RA 2.1
  !  WALRAD          *1.-WALRAD = DISTANCE WALL-PLASMA            R  2.1
  !  WNTORO          *TOROIDAL WAVE NUMBER N                      R  2.1
  !  MANCMP          *NB.OF POLOIDAL WAVE NUMBERS (HELICAL ANT.)  I  2.1
  !  MPOLWN(5)       *POLOIDAL WAVE NUMBERS (HELICAL ANT.)        IA 2.1
  !  NANTYP          *SELECTS TYPE OF ANTENNA   			I  2.1
  !  NRSPEC          *NUMBER OF ION SPECIES                       I  2.1
  !  NLCOLD          *SELECTS COLD OR LUKEWARM PLASMA MODEL       L  2.1
  !  NLCOLE	   *SELECTS COLD/WARM ELECTRONS (FAST W. DAMP.) L  2.1
  !  NLDIP           *SELECTS MONO/DIPOLE ANTENNA FOR LFS OR HFS  L  2.1
  !
  !L                  C2.2     EQUILIBRIUM QUANTITIES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMEQU/
  !
  !  WCOMEG          COMPLEX NORMALIZED FREQUENCY                 C  2.2
  !  WEPS            DIELECTRIC TENSOR COMPONENT EPSILON N-N      C  2.2
  !  WEPSEL          ELECTRON CONTRIBUTION TO DIELECTRIC TENSOR   C  2.2
  !  WEPSI(MDSPEC)   ION CONTRIBUTIONS TO WEPS                    CA 2.2
  !  WEPSMC          PHENOMENOLOGICAL DAMPING CONTRIB. TO DIEL.   C  2.2
  !  WG              DIELECTRIC TENSOR COMPONENT EPSILON N-PERP   C  2.2
  !  WGI(MDSPEC)     ION CONTRIBUTIONS TO WG                      CA 2.2
  !  EQ(MDEQ,MDPOL,MDPSI1)  EQUILIBRIUM QUANTITIES                RA 2.2
  !  WBETCH          NON-ORTHOGONALITY OF PSI,CHI (BETA-CHI)      R  2.2
  !  WBPOL2          POLOIDAL FIELD SQUARED                       R  2.2
  !  WBTOR2          TOROIDAL FIELD SQUARED                       R  2.2
  !  WBTOT           TOTAL FIELD                                  R  2.2
  !  WBTOT2          TOTAL FIELD SQUARED                          R  2.2
  !  WCHI            CHI AT CENTER OF THE CELL                    R  2.2
  !  WDCHI           CHI WIDTH OF CELL                            R  2.2
  !  WDCR2J          D/DCHI ( LOG(R**2/J) )                       R  2.2
  !  WDS             S WIDTH OF CELL                              R  2.2
  !  WFRAC(MDSPEC)   MASS FRACTION OF ION SPECIES                 RA 2.2
  !  WGRPS2          GRADIENT OF PSI SQUARED                      R  2.2
  !  WH              COEFFICIENT H                                R  2.2
  !  WJAC            JACOBIEN OF PSI,CHI COORDINATE SYSTEM        R  2.2
  !  WK              COEFFICIENT K                                R  2.2
  !  WNU             WAVE DAMPING                                 R  2.2
  !  WOMCI(MDSPEC)   NORMALIZED ION-CYCLOTRON FREQUENCY           RA 2.2
  !  WPSI            PSI                                          R  2.2
  !  WQ              SAFETY FACTOR Q                              R  2.2
  !  WR2             R SQUARED                                    R  2.2
  !  WRHO            MASS DENSITY                                 R  2.2
  !  WS              S COORDINATE AT CENTER OF THE CELL           R  2.2
  !  WT              TOROIDAL FLUX FUNCTION T                     R  2.2
  !  WTQ             R**2 / J                                     R  2.2
  !
  !L                  C3.1     (R,Z) AND (PSI,CHI) MESH VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMESH/
  !
  !  CHI(MD2CP2)     CHI MESH VALUES                              RA 3.1
  !  NCHI            *NUMBER OF CHI INTERVALS IN UPPER HALF-PLANE I  3.1
  !  NPOL            *TOTAL NUMBER OF CHI INTERVALS               I  3.1
  !  NPSI            *NUMBER OF S INTERVALS                       I  3.1
  !
  !L                  C3.2     NUMERICAL VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMNUM/
  !
  !  LENGTH          *NB.OF ELEMENTS OF A MATRIX BLOCK            I  3.2
  !  NCOLMN          *RANK OF A MATRIX BLOCK                      I  3.2
  !  NUMBER          *RUN NUMBER                                  I  3.2
  !
  !L                  C3.3     AUXILIARY VARIABLES FOR LION3 AND 4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMAUX/
  !
  !  CA(MDOVL)       OVERLAP SUBBLOCK OF MATRIX A                 CA 3.3
  !  CONA(6,6)       LOCAL (CELL) CONTRIBUTION TO A               CA 3.3
  !  XC1(8)          CONSTANTS C-J OF WEAK FORM TERMS             CA 3.3
  !  XETA(5)         CONSTANTS OF TEST-FUNCTIONS                  CA 3.3
  !  XKSI(5)         CONSTANTS OF UNKNOWNS                        CA 3.3
  !  XM(6,6)         CONTRIBUTION OF ONE TERM OF THE WEAK FORM    CA 3.3
  !  XVETA(6)        VECTOR OF COEFFICIENTS FOR TEST-FUNCTIONS    CA 3.3
  !  XVKSI(6)        VECTOR OF COEFFICIENTS FOR UNKNOWNS          CA 3.3
  !  XF(16)          BASIS FUNCTIONS AT MESH POINTS               RA 3.3
  !  NPLAC(6)        GLOBAL NUMBERING OF CELL MESH POINTS         IA 3.3
  !  NLQUAD          QUADRATIC TERM IN THE WEAK FORM              L  3.3
  !
  !L                  C3.4     VECTORS FOR LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMVEC/
  !
  !  SOURCT(MDCOMP)  TOTAL VECTOR OF RIGHT-HAND SIDE		CA 3.4
  !  U(MDCOL)        VECTOR OF UNKNOWS FOR ONE BLOCK              CA 3.4
  !  UT              TEMPORARY STORAGE                            C  3.4
  !  VA(MDCOL)       TEMPORARY STORAGE                            CA 3.4
  !  X(MDCOL)        VECTOR OF UNKNOWNS FOR ONE BLOCK             CA 3.4
  !  XOHMR(MDPOL)    OHM VECTOR FOR POWER AT ANTENNA              CA 3.4
  !  XT(MDCOMP)      TOTAL VECTOR OF UNKNOWNS                     CA 3.4
  !  XDCHI(MDPOL)    DELTA-CHI AT PLASMA SURFACE                  RA 3.4
  !  XDTH(MDPOL)     DELTA-THETA AT PLASMA SURFACE                RA 3.4
  !
  !L                  C3.5     NUMERICAL VARIABLES FOR LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMIVI/
  !
  !  EPSMAC          *ROUND-OFF ERROR OF COMPUTER                 R  3.5
  !  M1              RANK OF MATRIX OVERLAP SUBBLOCK              I  3.5
  !  M11             M1+1                                         I  3.5
  !  M12             M1+M2 = RANK OF MATRIX BLOCK                 I  3.5
  !  M2              RANK OF MATRIX BLOCK - M1                    I  3.5
  !  N               NB. OF MATRIX BLOCKS (=NPSI)                 I  3.5
  !  NCOMP           NB. OF ELEMENTS OF SOLUTION VECTOR           I  3.5
  !  NLONG           NB. OF ELEMENTS IN A MATRIX BLOCK            I  3.5
  !  NSING           SINGULARITY INDICATOR (=-1 IF A IS SING.)    I  3.5
  !
  !L                  C5.1     CONTROL VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMCON/
  !
  !  AHEIGT          *HEIGHT OF 2-D PLOTS                         R  5.1
  !  ALARG           *WIDTH OF 2-D PLOTS                          R  5.1
  !  ANGLET(16)      *TOROIDAL CUTS (DEGREES)                     RA 5.1
  !  ARSIZE          *SIZE OF ARROWS                              R  5.1
  !  ASYMB           *SIZE OF SYMBOLS                             R  5.1
  !  MFL             *LOWER M VALUE FOR FOURIER ANALYSIS          I  5.1
  !  MFU             UPPER M VALUE FOR FOURIER ANALYSIS           I  5.1
  !  NCONTR          *NUMBER OF CONTOUR LINES                     I  5.1
  !  NCUT            *NUMBER OF TOROIDAL CUTS                     I  5.1
  !  NLDISO	   *SWITCH COMPUTATION DIAGNOSTICS OF SOLUTION  L  5.1
  !  NLPHAS          *SWITCH POLOIDAL PHASE EXTRACTION            L  5.1
  !  NLOTP0          *GENERAL LINE-PRINTER OUTPUT SWITCH          L  5.1
  !  NLOTP1(4)       *OUTPUT SWITCHES FOR LION1                   LA 5.1
  !  NLOTP2(5)       *OUTPUT SWITCHES FOR LION2                   LA 5.1
  !  NLOTP3(2)       *OUTPUT SWITCHES FOR LION3                   LA 5.1
  !  NLOTP4(5)       *OUTPUT SWITCHES FOR LION4                   LA 5.1
  !  NLOTP5(40)      *OUTPUT SWITCHES FOR LION5                   LA 5.1
  !  NLPLO5(25)      *PLOT SWITCHES FOR LION5                     LA 5.1
  !  NPLTYP	   *=1 FOR BASPL PLOTS, =2 FOR EXPLORER PLOTS   I  5.1
  !  NRUN            *NUMBER OF RUNS FOR FREQUENCY TRACE          I  5.1
  !  NTORSP	   *NUMBER OF TOR. WN'S FOR THE TOR.WN SCAN	I  5.1
  !  WNTDEL	   *TOR.WN. INCREMENT FOR TOR.WN. SCAN		R  5.1
  !
  !L                  C5.2     I/O DISK CHANNELS NUMBERS
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMOUT/
  !
  !  MEQ             *EQUILIBIUM QUANTITIES                       I  5.2
  !  NDA             *MATRIX A                                    I  5.2
  !  NDES            *R,Z COORDINATES AND NORMALS                 I  5.2
  !  NDLT            *DECOMPOSED MATRIX L,D,U                     I  5.2
  !  NDS             *SOLUTION VECTOR                             I  5.2
  !  NPRNT           *LINE-PRINTER OUTPUT                         I  5.2
  !  NSAVE           *NAMELIST LINK LION1 TO 5                    I  5.2
  !  NVAC            *VACUUM QUANTITIES                           I  5.2
  !
  !
  !---------------------------------------------------------------------
  !L             9.          BLANK COMMON
  !
  !
  !
  !L                  C9.2     VACUUM QUANTITIES FOR LION2
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMVID)
  !
  !  ANTR(MDPOL)     ANTENNA VECTOR                               CA 9.2
  !  OHMR(MDPOL)     OHM VECTOR                                   CA 9.2
  !  REASCR          REACTANCE SCALAR                             C  9.2
  !  SAUTR(MDPOL)    JUMP ACROSS ANTENNA SURFACE                  CA 9.2
  !  SAUTX(MDPOL)    TEMPORARY                                    CA 9.2
  !  SOURCE(MDPOL)   SOURCE VECTOR                                CA 9.2
  !  CA              CONSTANT TO DEFINE WALL                      R  9.2
  !  CB              #                                            R  9.2
  !  CC              #                                            R  9.2
  !  D2ROP           D**2RHO/DTHETA**2 AT MID-POINT               R  9.2
  !  DC(MD2CP2)      DELTA-CHI                                    RA 9.2
  !  DELTH(MD2CP2)   DELTA-THETA (TH(K+1)-TH(K))                  RA 9.2
  !  DRODT(4)        DRHO/DTHETA AT INTEGRATION POINTS            RA 9.2
  !  DROPDT(4)       DRHOPRIME/DTHETA                             RA 9.2
  !  G(MDPOL,MDPOL,13)
  !                  ALL MATRICES                                 RA 9.2
  !  QB              SAFETY FACTOR AT PLASMA SURFACE              R  9.2
  !  RO(4)           RHO AT INTEGRATION POINTS                    RA 9.2
  !  RO2             RHO AT THE CENTER                            R  9.2
  !  RO2P            RHO PRIME AT THE CENTER                      R  9.2
  !  ROEDGE(MD2CP2)  RHO PLASMA SURF. AT EDGES OF CHI INTERVALS   RA 9.2
  !  ROMID(MD2CP2)   RHO PLASMA SURF. AT MIDDLE CHI INTERVALS     RA 9.2
  !  ROP(4)          RHO PRIME AT INTEGRATION POINTS              RA 9.2
  !  SR(MD2CP2)      R(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  SZ(MD2CP2)      Z(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  T(MD2CP2)       THETA AT MIDDLE CHI INTERVALS                RA 9.2
  !  TB              TOROIDAL MAGNETIC FLUX AT PLASMA SURFACE     R  9.2
  !  TH(MD2CP2)      THETA AT EDGES OF CHI INTERVALS              RA 9.2
  !  TP(4)           THETA PRIME AT INTEGRATION POINTS            RA 9.2
  !  TT(4)           THETA AT INTEGRATION POINTS                  RA 9.2
  !  NDIM            DIMENSION OF MATRICES                        I  9.2
  !
  !L                  C9.3     MATRIX BLOCKS FOR LION 3 AND LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMMTR)
  !
  !  A1D(MDLENG)       MATRIX BLOCK OF DISCRETIZED WEAK FORM        CA 9.3
  !
  !L                  C9.5     OUTPUT AND PLOT QUANTITIES FOR LION5
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMPLO)
  !
  !  ABSPOW(MDPSI)   POWER ABSORBED ON PSI=CONST.                 RA 9.5
  !  APHI            TOROIDAL ANGLE PHI                           R  9.5
  !  BNL             NORMAL COMPONENT OF WAVE MAGNETIC FIELD      C  9.5
  !  BPARL           PARALLEL COMP. OF WAVE MAGNETIC FIELD        C  9.5
  !  BPL             PERP. COMP. OF WAVE MAGNETIC FIELD           C  9.5
  !  CCHI(MDPOL)     CHI VALUES AT CENTER OF CELLS                RA 9.5
  !  CCR(MDPSI,MDPOL)
  !                  R AT CENTER OF CELLS                         RA 9.5
  !  CCS(MDPSI)      S VALUES AT CENTER OF CELLS                  RA 9.5
  !  CCZ(MDPSI,MDPOL)
  !                  Z AT CENTER OF CELLS                         RA 9.5
  !  CDEVIA          WIDTH OF POWER DISTRIBUTION IN CHI           R  9.5
  !  CELPOW(MDPSI,MDPOL)
  !                  POWER ARBSORBED IN THE CELLS                 RA 9.5
  !  CHIPOW(MDPOL)   POWER ABSORBED ON CHI=CONST.                 RA 9.5
  !  CMEAN           CENTER OF POWER DISTRIBUTION IN CHI          R  9.5
  !  CNR(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., R COMPONENT            RA 9.5
  !  CNZ(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., Z COMPONENT            RA 9.5
  !  COST(MDPSI,MDPOL)COSINUS OF ANGLE BETWEEN PHI AND B_0        RA 9.5
  !  CPL             POWER ABSORBED IN A CELL                     R  9.5
  !  CPLE            POWER IN A CELL DUE TO NU                    R  9.5
  !  CPLJ(MDSPC2)    POWER IN A CELL PER SPECIES                  RA 9.5
  !  DENPOW(0:MDPSI,MDPOL1)
  !                  POWER ABSORPTION DENSITY                     RA 9.5
  !  DEPOS(MDPSI)    POWER DENS. AVERAGED ON PSI=CONST.           RA 9.5
  !  DPL             POWER ABSORPTION DENSITY AT CENTER OF CELL   R  9.5
  !  DPLE            POWER ABSORPTION DENSITY DUE TO NU           R  9.5
  !  DPLJ(MDSPC2)    POWER DENSITY PER SPECIES                    RA 9.5
  !  DVDC            DV/DCHI                                      C  9.5
  !  DXDC            DX/DCHI                                      C  9.5
  !  DXDS            DX/DS                                        C  9.5
  !  ECHEL           SCALE FACTOR FOR 2-D PLOTS                   R  9.5
  !  ELEPOW          POWER DUE TO NU                              R  9.5
  !  ENL             NORMAL COMPONENT OF WAVE ELECTRIC FIELD      C  9.5
  !  EPL             PERP. COMP. OF WAVE ELECTRIC FIELD           C  9.5
  !  FLUPOW(MDPSI1)  POWER ABSORBED INSIDE PSI=CONST.             RA 9.5
  !  FLUPSP(MDPSI1,MDSPC2)
  !                  POWER ABSORBED INSIDE PSI=CONST.,PER SPECIES RA 9.5
  !  FREN(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-N          CA 9.5
  !  FREP(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-PERP       CA 9.5
  !  FRINT(7,MDCHI)  INTEGRAL FOR FOURIER DECOMPOSITION           CA 9.5
  !  NRZSUR          NUMBER OF POINTS FOR PLOTTING THE SURFACE    I  9.5
  !  SDEVIA          WIDTH OF POWER DISTRIBUTION IN S             R  9.5
  !  SFLUX(MDPSI)    POYNTING FLUX ACROSS PSI=CONST.              RA 9.5
  !  SINT(MDPSI,MDPOL)SINUS OF ANGLE BETWEEN PHI AND B_0          RA 9.5
  !  SMEAN           MINOR RADIUS OF HALF POWER ABSORPTION        R  9.5
  !  SN(MDPSI,MDPOL) NORMAL COMPONENT OF POYNTING                 RA 9.5
  !  SNL             NORMAL COMPONENT OF POYNTING                 R  9.5
  !  SPAR(MDPSI,MDPOL)
  !                  PARALLEL COMPONENT OF POYNTING               RA 9.5
  !  SPARL           PARALLEL COMPONENT OF POYNTING               R  9.5
  !  SPERP(MDPSI,MDPOL)
  !                  PERP. COMPONENT OF POYNTING                  RA 9.5
  !  SPL             PERP. COMP. OF POYNTING                      R  9.5
  !  SURPSI(MDPSI1)  INTEGRAL FOR PSI-SURFACE                     RA 9.5
  !  VC              V                                            C  9.5
  !  VOUT1(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  VOUT2(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  XC              X                                            C  9.5
  !  XS(MDRZ)        R COORDINATES OF PLASMA SURFACE              RA 9.5
  !  YS(MDRZ)        Z COORDINATES OF PLASMA SURFACE              RA 9.5
  !
  !*********************************************************************
  !*********************************************************************
  !
END PROGRAM LION1
!CC*DECK P1C0S02
SUBROUTINE MASTER
  !        -----------------
  !
  !  1.0.02. CONTROLS THE RUN
  !--------------------------------------------------------------------
  !     IN THIS VERSION THE EQUILIBRIUM CALCULATION AND MAPPING IS DONE
  !     IN THE CODE CHEASE (H. LUTJENS). THE DATA IS TRANSFERRED VIA 
  !     DISK I/O ON THE FOLLOWING CHANNELS:
  !
  !        NSAVE     NAMELIST VARIABLES FROM CHEASE (NPSI, NCHI, ETC.)
  !        MEQ       EQUILIBRIUM QUANTITIES EQ(I,Jchi,Jpsi)
  !        NVAC      QUANTITIES AT PLASMA-VACUUM INTERFACE
  !        NDES      R,Z,AND NORMALS TO PSI=CONST. R,Z OF P.V.I.
  !
  !     THEREFORE WE DO NOT USE 'LION1', FORMERLY 'ERATO1'.
  !
  !     PLEASE PAY ATTENTION TO THE FACT THAT THE NAMELIST $NEWRUN
  !     TRANSFERRED BY CHEASE IS READ IN SUBROUTINE AUXVAL, WHEREAS
  !     NAMELIST $NEWRUN CONTAINING THE INPUT PARAMETERS FOR LION
  !     IS READ IN SUBROUTINE DATA. THIS IS DONE TO AVOID CONFLICTS
  !     IN THE DEFINITIONS OF THESE NAMELISTS (THEY ARE DIFFERENT!).
  !     THE DEFINITIONS OF SOME OF THE NAMELIST VARIABLES ARE DIFFERENT.
  !     PLEASE LOOK INTO SUBROUTINE AUXVAL FOR MORE INFORMATION.
  !---------------------------------------------------------------------
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE GENERATOR FREQUENCY (FREQCY).
  !
  !     THE NUMBER OF DIFFERENT FREQUENCIES TO BE RUN IS GIVEN IN THE
  !     NAMELIST $NEWRUN OF LION (NRUN).
  !
  !     THE FREQUENCY IS INCREMENTED BY A CONSTANT AMOUNT AT EACH RUN.
  !     THIS AMOUNT IS GIVEN IN THE NAMELIST $NEWRUN OF LION (DELTAF).
  !---------------------------------------------------------------------
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE TOROIDAL WAVE NUMBER.
  !
  !     THE NUMBER OF TOR. W. N. IS NTORSP
  !
  !     THE TOR. W. N. IS INCREMENTED BY WNTDEL
  !---------------------------------------------------------------------
  USE globals
!!$  USE COMBAS ! .inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: t0, t1, t2, t3, t4, t5, t6, tend, ZOLDFR
  INTEGER :: JTORSP, JRUN, JPOL
  !---------------------------------------------------------------------

  CALL runtime (t0)
  !
  !     CLEAR COMMON BLOCKS
  CALL CLEAR
  !
  !     SET DEFAULT VAUES
  CALL PRESET
  !
  !     READ 4 LABEL CARDS AND PRINT TITLE
  CALL LABRUN
  !
  !     READ INPUT NAMELIST FOR LION
  CALL DATA
  !
  !     READ NAMELIST FROM CHEASE AND SET AUXILIARY VALUES
  CALL AUXVAL
  !
  !     read equilibrium quantities produced by CHEASE -> into EQ(.,.,.)
  CALL iodsk1(9)
  !
  !     OPEN AND CLOSE NSOURC (NEW FILE)
  CALL IODSK1(6)
  !
  !---------------------------------------------------------------------
  !L       TOROIDAL FOURIER SPECTRUM WITH NTORSP WAVENUMBERS
  !        EQUALLY SPACED BY WNTDEL
  !
  DO JTORSP = 1, NTORSP
    !
    IF (NTORSP.GT.1) THEN
      WRITE (NPRNT,9100) JTORSP, WNTORO
    END IF

    write(6,*) ' Before VACUUM'
    call runtime (t1)
    write(6,*) ' Elapsed time so far:', t1-t0
    !
    !     COMPUTE VACUUM CONTRIBUTION, INCLUDING ANTENNA
    CALL VACUUM

    write(6,*) ' After VACUUM, before ORGAN4'
    call runtime (t2)
    write(6,*) ' Elapsed time in VACUUM:', t2-t1
    !
    !     CONSTRUCT MATRIX OF THE DISCRETIZED WEAK FORM AND SOLVE IT
    CALL ORGAN4

    write(6,*) ' After ORGAN4, before DIAGNO'
    call runtime (t3)
    write(6,*) ' Elapsed time in ORGAN4:', t3-t2
    !
    !     MAKE DIAGNOSTICS OF THE SOLUTION
    CALL DIAGNO

    write(6,*) ' After DIAGNO'
    call runtime (t4)
    write(6,*) ' Elapsed time in DIAGNO:', t4-t3
    !
    !---------------------------------------------------------------------
    !L             FREQUENCY TRACE WITH NRUN FREQUENCIES EQUALLY
    !              SPACED BY DELTAF
    !
    IF (NRUN.GT.1) THEN
      !
      DO JRUN=2,NRUN
        !
        !     INCREMENT THE FREQUENCY (FREQCY) BY DELTAF
        ZOLDFR = FREQCY
        FREQCY = FREQCY + DELTAF
        !
        !     RESCALE THE NORMALIZED FREQUENCY (OMEGA)
        OMEGA  = OMEGA * FREQCY / ZOLDFR
        !
        !     PRINTOUT NEW FREQUENCIES
        WRITE (NPRNT,9200) JRUN, FREQCY, OMEGA
        !
        !     SWITCH OFF FREQUENCY-INDEPENDANT OUTPUT
        NLOTP5(25) = .FALSE.
        NLOTP5(26) = .FALSE.
        !lvC
        !lvC     OPEN NSAVE, REWRITE NAMLIST NEWRUN, AND CLOSE NSAVE
        !lv               CALL IODSK1(1)
        !lv               CALL IODSK1(2)
        !lv               CALL IODSK1(3)
        !
        !     RESCALE THE SOURCE VECTOR (SOURCE)
        CALL IODSK1 (4)
        DO JPOL=1,NPOL
          SOURCE(JPOL) = SOURCE(JPOL) * FREQCY / ZOLDFR
        END DO
        CALL IODSK1 (5)


        call runtime (t5)
        !
        !     CONSTRUCT MATRIX OF THE DISCRETIZED WEAK FORM AND SOLVE IT

        CALL ORGAN4
        write(6,9001) jrun
9001    format(' Frequency scan, JRUN=',i5,' after ORGAN4')
        call runtime (t6)
        !
        !     MAKE DIAGNOSTICS OF THE SOLUTION
        CALL DIAGNO
        !
      END DO
      !
    END IF
    !
    !---------------------------------------------------------------------
    !
    IF (JTORSP.LT.NTORSP) THEN

      !      INCREMENT WNTORO AND REWRITE NAMELIST ON NSAVE
      WNTORO = WNTORO + WNTDEL
      !
      !lvC     OPEN NSAVE, REWRITE NAMLIST NEWRUN, AND CLOSE NSAVE
      !lv               CALL IODSK1(1)
      !lv               CALL IODSK1(2)
      !lv               CALL IODSK1(3)
      !
    END IF
    !
  END DO ! JTORSP

  !
  !     close and delete NSOURC
  CALL IODSK1(8)

  write(6,*) ' At end of run'
  call runtime (tend)
  write(6,*) ' Total elapsed time in LION:', tend-t0
  !
  !---------------------------------------------------------------------
9100 FORMAT (//,1X,'***************************************',/, &
       &              1X,'TOROIDAL WAVE NUMBER SCAN -- RUN NO.',I3,/, &
       &              1X,'***************************************',/, &
       &              1X,'TOROIDAL WAVE NUMBER =',1PE13.3,/)
9200 FORMAT (///,1X,'******************************',/, &
       &               1X,'FREQUENCY TRACE --- RUN NO.',I3,/, &
       &               1X,'******************************',//, &
       &               1X,'FREQUENCY            =',1PE15.8,'  HZ',/, &
       &               1X,'NORMALIZED FREQUENCY =',1PE15.8,/)
  !---------------------------------------------------------------------
  RETURN
END SUBROUTINE MASTER
!CC*DECK P1C1S01
!
SUBROUTINE LABRUN
  !
  ! 1.1  LABEL THE RUN
  !
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !
  USE GLOBALS
!!$  USE COMBAS ! INCLUDE 'COMBAS.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !---------------------------------------------------------------------
  !
  !     READ LABELS
  READ(5,9900)LABEL1
  READ(5,9900)LABEL2
  READ(5,9900)LABEL3
  READ(5,9900)LABEL4
  !
  !     WRITE HEADING
  CALL BLINES(8)
  WRITE(NPRNT,9901)
  WRITE(NPRNT,9902)
  CALL BLINES(4)
  !
  !     WRITE LABELS
  CALL MESAGE(LABEL1)
  CALL BLINES(1)
  CALL MESAGE(LABEL2)
  CALL BLINES(1)
  CALL MESAGE(LABEL3)
  CALL BLINES(1)
  CALL MESAGE(LABEL4)
  CALL BLINES(1)
  !
  RETURN
9900 FORMAT(A80)
9901 FORMAT(50X,'P R O G R A M   L I O N - F A S T - 0709')
9902 FORMAT(50X,'****************************************')
END SUBROUTINE LABRUN
!CC*DECK P1C1S02
SUBROUTINE CLEAR
  !
  ! 1.1.2   CLEAR VARIABLES AND ARRAYS
  !
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
!!$  REAL(RKIND) :: 
!!$  INTEGER :: 
  REAL(RKIND) :: A1
  INTEGER :: I1, I2, II3, I5
  !---------------------------------------------------------------------
  !
  !>>>
  RETURN
!!$  !     CLEAR COMMON COMPHY
!!$  I1 = LOC(WNTORO)-LOC(ACHARG(1))+1
!!$  CALL RESETR(ACHARG,I1,0.0_RKIND)
!!$  CALL RESETI(MANCMP,8,0)
!!$  !     CLEAR COMMON COMEQU
!!$  I2 = LOC(WTQ)-LOC(EQ(1,1,1))+1
!!$  CALL RESETR(EQ,I2,0.0_RKIND)
!!$  !     CLEAR COMMON COMESH
!!$  CALL RESETR(CHI,MD2CP2,0.0_RKIND)
!!$  II3 = LOC(NPSI)-LOC(NCHI)+1
!!$  CALL RESETI(NCHI,II3,0)
!!$  !     CLEAR COMMON COMNUM
!!$  CALL RESETI(LENGTH,3,0)
!!$  !     CLEAR COMMON COMVID
!!$  I5 = LOC(REASCR)-LOC(CA)+2
!!$  CALL RESETR(A1,I5,0.0_RKIND)
!!$  CALL RESETI(NDIM,1,0)
  RETURN
END SUBROUTINE CLEAR
!CC*DECK P1C1S03
SUBROUTINE PRESET
  !
  ! 1.1.3   SET DEFAULT VALUES
  !
  USE GLOBALS
!!$  USE COMBAS ! INCLUDE 'COMBAS.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  INTEGER :: J, JF
  !---------------------------------------------------------------------
  !
  !
  !     PLASMA PHYSICS
  DO J=1,10
    ACHARG(J) = 1.0_RKIND
    AMASS (J) = 2.0_RKIND
    CENTIP(J) = 2.0E+3_RKIND
    CENTI (J) = 2.0E3_RKIND
    EQKAPT(J) = 1.0_RKIND
    EQTI  (J) = 0.9_RKIND
    FRCEN (J) = 0.0_RKIND
    FRDEL (J) = 1.0E6_RKIND
  END DO
  ACHARG(2) = 2.0_RKIND
  AMASS (2) = 4.0_RKIND
  AMASSE    = 5.446E-4_RKIND
  ANU       = 0.01_RKIND
  ASPCT     = 0.333_RKIND
  BNOT      = 3.4_RKIND
  CEN0(1)   = 0.6E19_RKIND
  CEN0(2)   = 0.8E19_RKIND
  CEN0(3)   = 1.25E19_RKIND
  CEN0(4)   = 2.0E19_RKIND
  CEN0(5)   = 3.0E19_RKIND
  CEN0(6)   = 4.1E19_RKIND
  CEN0(7)   = 6.0E19_RKIND
  CEN0(8)   = 9.0E19_RKIND
  CENDEN(1) = 4.0E19_RKIND
  CENDEN(2) = 4.0E18_RKIND
  CENTE     = CENTI(1)
  DO J=3,10
    CENDEN(J) = 0.0_RKIND
  END DO
  DELTA     = 0._RKIND
  ELLIPT    = 1.0_RKIND
  NBCASE    = 1
  NBTYPE    = 1
  NDARG     = 1
  NDDEG     = 1
  NFAKAP    = 1
  DO J=1,10
    AD(J)  = 0.0_RKIND
  END DO
  NDENS     = 0
  EQDENS    = 0.9_RKIND
  EQFAST    = 0.9_RKIND
  EQKAPD    = 1.0_RKIND
  DO JF = 1, MDFAKA
    EQKAPF(JF) = CMPLX(REAL(JF,RKIND), 0._RKIND)
  END DO
  EQALFD    = 0.0_RKIND
  EQKPTE    = EQKAPT(1)
  EQTE      = EQTI(1)
  NHARM     = 1
  NLCOLD    = .TRUE.
  NLCOLE    = .TRUE.
  NLFAST    = .FALSE.
  NLTTMP    = .TRUE.
  NRSPEC    = 1
  NTEMP     = 0
  QIAXE     = 1.0_RKIND
  RMAJOR    = 3.0_RKIND
  VBIRTH    = 1.3E+7_RKIND
  !
  !     ANTENNA AND WALL
  ANTRAD    = 1.1_RKIND
  ANTRADMAX = 1.15_RKIND
  ANTUP     = 70.0_RKIND
  CURASY(1) = 1.0_RKIND
  CURSYM(1) = 1.0_RKIND
  FEEDUP    = 40.0_RKIND
  FREQCY    = 3.3E7_RKIND
  MANCMP    = 1
  MPOLWN(1) = 1
  NANTSHEET = 1
  NANTYP    = 2
  NLDIP     = .FALSE.
  NSADDL    = 2
  SAMIN     = 0.1_RKIND
  SAMAX     = 0.3_RKIND
  THANT(1)  = 68.0_RKIND
  THANT(2)  = 111.0_RKIND
  THANT(3)  = 249.0_RKIND
  THANT(4)  = 292.0_RKIND
  THANTW    = 20.0_RKIND
  WALRAD    = 1.2_RKIND
  WNTORO    = -1._RKIND
  !
  !     OUTPUT AND PLOT
  AHEIGT    = 22.0_RKIND
  ALARG     = 20.0_RKIND
  DO J=1,16
    ANGLET(J) = REAL (J-1,RKIND) * 360._RKIND/16._RKIND
  END DO
  ARSIZE    = 2.0_RKIND
  ASYMB     = 1.2_RKIND
  NCONTR    = 9
  NCUT      = 1
  NLDISO    = .TRUE.
  NLOTP0    = .TRUE.
  DO J=1,25
    NLPLO5(J) = .FALSE.
  END DO
  NPLTYP    = 1
  !
  !     FOURIER ANALYSIS
  MFL       = -7
  !
  !     MESH
  NCHI   = MDCHI
  NPOL   = 2*NCHI-2
  NPSI   = MDPSI
  !
  !     INPUT/OUTPUT DISK CHANNEL NUMBERS
  MEQ    = 4
  NDA    = 7
  NDES   = 16
  NDLT   = 10
  NDS    = 14
  NREAD  = 5
  NPRNT  = 6
  NSAVE  = 8
  NSOURC = 15
  NVAC   = 17
  !
  !     NUMERICAL PARAMETERS
  NLPHAS = .FALSE.
  EPSMAC = 1.0E-12_RKIND
  NUMBER = 0
  !
  !     OTHERS
  GAMMA  = 5.0_RKIND / 3.0_RKIND
  NRUN   = 1
  DELTAF = 1000.0_RKIND
  NTORSP = 1
  WNTDEL = 1.0_RKIND
  !
  RETURN
END SUBROUTINE PRESET
!CC*DECK P1C1S04
SUBROUTINE DATA
  !
  ! 1.1.4   DEFINE DATA SPECIFIC TO RUN
  !
  USE GLOBALS
  IMPLICIT NONE
!!$  USE COMBAS ! INCLUDE 'COMBAS.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  INCLUDE 'NEWRUN.inc'
  !
  INTEGER :: J, IPSM1
  !---------------------------------------------------------------------
  !
  READ (NREAD,NEWRUN)
  !
  !     DEFINE CONSISTENT DATA WHERE POSSIBLE
  !
  !     IF NLFAST=.T., WE SET NRSPEC=1 (BULK ION SPECIES), WITH AN
  !     OUTPUT MESSAGE
  !
  IF (NLFAST) THEN
    NRSPEC = 1
    WRITE (NPRNT,9001)
  END IF
  !
  !     IF NLFAST=.F., WE SET NFAKAP=0 (NO FAST PARTICLE PROFILES)
  !
  IF (.NOT.NLFAST) THEN
    NFAKAP=0
  END IF
  !
  !     NFAKAP CANNOT BE LARGER THAN MDFAKA
  !
  IF (NFAKAP.GT.MDFAKA) THEN
    NFAKAP = MDFAKA
    WRITE (NPRNT,9011) MDFAKA
  END IF
  !
  !     NBCASE CANNOT BE LARGER THAN MDBCAS
  !
  IF (NBCASE.GT.MDBCAS) THEN
    NBCASE = MDBCAS
    WRITE (NPRNT,9012) MDBCAS
  END IF
  !
  !     TEST ANTENNA SPECIFICATION
  !
  !     ANTENNA IN VACUUM REGION, BETWEEN THE PLASMA AND THE WALL
  IF (NANTYP.GT.0) THEN
    IF (ANTRAD.LE.1._RKIND) STOP 'ANTRAD <= 1.0 NOT POSSIBLE'
    IF (ANTRADMAX.LE.1._RKIND) STOP 'ANTRADMAX <= 1.0 NOT POSSIBLE'
    IF (WALRAD.LE.1._RKIND) STOP 'WALRAD <= 1.0 NOT POSSIBLE'
    IF (WALRAD.LE.ANTRAD) STOP 'WALRAD <= ANTRAD NOT POSSIBLE'
    IF (WALRAD.LE.ANTRADMAX) STOP &
         &                              'WALRAD <= ANTRADMAX NOT POSSIBLE'
  END IF
  !
  IF (NANTYP.EQ.2) THEN
    IF ( (THANT(2).EQ.THANT(3)) .OR. (THANT(1).GE.THANT(2)) &
         &         .OR. (THANT(3).GE.THANT(4)) ) THEN
      STOP 'SET THANT(1) < THANT(2) # THANT(3) < THANT(4)'
    END IF
  END IF
  !
  IF ( (NANTYP.EQ.3) .AND. (ANTUP.EQ.FEEDUP) ) &
       &   THEN
    STOP 'FEEDUP EQ ANTUP NOT POSSIBLE'
  END IF
  !
  IF ((NANTYP.EQ.2).OR.(NANTYP.EQ.4)) MANCMP = 1
  !
  IF (NLDIP) THEN
    NLDIP = .FALSE.
    WRITE (*,*) ' DIPOLE ANTENNA NOT YET IMPLEMENTED'
    WRITE (*,*) ' NLDIP WAS FORCED TO .FALSE.'
  END IF
  !
  !     ANTENNA INSIDE THE PLASMA BETWEEN S=SAMIN AND S=SAMAX
  IF (NANTYP.EQ.-1) THEN
    IF (SAMIN.GE.SAMAX) STOP 'SAMIN >= SAMAX NOT POSSIBLE'
  END IF
  !
  !     SWITCH OFF OUTPUT AND PLOTS IF ASKED
  !
  IF (.NOT.NLOTP0) THEN
    !
    DO J=1,4
      NLOTP1(J) = .FALSE.
    END DO
    DO J=1,5
      NLOTP2(J) = .FALSE.
    END DO
    DO J=1,2
      NLOTP3(J) = .FALSE.
    END DO
    DO J=1,5
      NLOTP4(J) = .FALSE.
    END DO
    DO J=1,40
      NLOTP5(J) = .FALSE.
    END DO
    DO J=1,25
      NLPLO5(J) = .FALSE.
    END DO
    !
  END IF
  !
  IF (.NOT.NLPLO5(1)) THEN
    DO J=2,25
      NLPLO5(J) = .FALSE.
    END DO
  END IF
  !
  NPSI = MAX0 (3,NPSI)
  NCHI = MAX0 (4,NCHI)
  NPSI = MIN0 (MDPSI,NPSI)
  NCHI = MIN0 (MDCHI,NCHI)
  !
  IPSM1 = NPSI-1
  !
  !     CHOOSE BETWEEN FREQUENCY SCAN AND TOROIDAL WAVENUMBER SCAN
  IF ((NTORSP.GT.1).AND.(NRUN.GT.1)) THEN
    NRUN = 1
    WRITE (NPRNT,9100) NTORSP
  END IF
  !
9001 FORMAT(//,' MESSAGE FROM SUBROUTINE DATA:',//, &
       &		' NRSPEC WAS SET TO 1 SINCE YOU SET NLFAST=T.',/, &
       &		' THIS MEANS AN EVALUATION OF FAST PARTICLE DRIVE',/, &
       &		' THAT IS VALID FOR SINGLE BULK ION SPECIES AND',/, &
       &		' OMEGA << OMEGACI. THE FAST PARTICLE DENSITY IS',/, &
       &		' N_F = CENDEN(2) * (1.- EQFAST*S*S) **EQKAPF(JF)',/, &
       &		' ACHARG(2) AND AMASS(2) ARE Z AND A OF FASTS.',/, &
       &		' VBIRTH IS THE BIRTH VELOCITY OF FAST PARTICLES',/, &
       &		' THAT ASSUME A SLOWING-DOWN DISTRIBUTION.',/)
  !
9011 FORMAT(//,' MESSAGE FROM SUBROUTINE DATA:',//, &
       &		' NFAKAP WAS SET TO MDFAKA =', I4, /)
9012 FORMAT(//,' MESSAGE FROM SUBROUTINE DATA:',//, &
       &		' NBCASE WAS SET TO MDBCAS =', I4, /)
9100 FORMAT(//,' MESSAGE FROM SUBROUTINE DATA:',//, &
       &             ' NRUN SET TO 1 ==> TOROIDAL WAVENUMBER SCAN',/, &
       &             '                   WITH ',I3,' VALUES OF N',/)
  !
  RETURN
END SUBROUTINE DATA
!CC*DECK P1C1S05
SUBROUTINE AUXVAL
  !
  ! 1.1.5   SET AUXILIARY VALUES
  !
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: AL0
  REAL(RKIND) :: ARROW
  REAL(RKIND) :: EPSCON
  REAL(RKIND) :: QSURF
  REAL(RKIND) :: REXT
  REAL(RKIND) :: RITOT
  REAL(RKIND) :: TSURF
  REAL(RKIND) :: WNTORE
  REAL(RKIND) :: RINOR
  REAL(RKIND) :: QCYL
  REAL(RKIND) :: P0
  INTEGER :: NAL0AUTO
  INTEGER :: NER
  INTEGER :: NEGP
  INTEGER :: ITEST
  INTEGER :: NFIG
  INTEGER :: NITMAX
  INTEGER :: NUA1
  INTEGER :: NUA2
  INTEGER :: NUB1
  INTEGER :: NUB2
  INTEGER :: NUX
  INTEGER :: NUPL
  INTEGER :: NUSG
  INTEGER :: NV
  INTEGER :: NUWA
  INTEGER :: NVIT
  INTEGER :: NWALL
  INCLUDE 'NCHEAS.inc'
  !
  REAL(RKIND) :: ZE, ZMP, ZMU0, ZMDEN, ZALFSP
  INTEGER :: IPSI, J, NCON
  !---------------------------------------------------------------------
  !
  !     OPEN NSAVE
  CALL IODSK1 (1)
  !
  !     READ NAMELIST FROM EQUILIBRIUM CODE CHEASE
  REWIND NSAVE
  READ (NSAVE,NEWRUN)
  !
  !     READ AUXILIARY ARRAYS QS() AND QTILDA() CONTAINING Q-VALUES AT EDGES
  !     AND CENTERS OF CELLS, RESPECTIVELY. THE (NPSI+1)-TH VALUE IS Q AT 
  !     THE PLASMA SURFACE.
  READ (NSAVE,9001) IPSI
  READ (NSAVE,9002) (QS(J), J=1,IPSI)
  READ (NSAVE,9001) IPSI
  READ (NSAVE,9002) (QTILDA(J), J=1,IPSI)
  !
  !     REPAIR EQ(21,KC) = D/DS (INTEGRAL FROM 0 TO CHI OF T*J/R^2).
  !     FOR SOME REASON IT IS DEFINED IN CHEASE AS THE INTEGRAL
  !     FROM 0 TO CHI-2PI IF KC > NPOL/2+1 AND NPOL EVEN, OR
  !     IF KC > (NPOL+1)/2+1 AND NPOL ODD. SO WE MUST ADD 2PI*DQ/DS
  !     TO THE CORRESPONDING EQ(21,KC)
  !CCC         CALL IODSK1 (7)
  !
  !     NCHI IS DEFINED IN CHEASE AS THE NUMBER OF POLOIDAL INTERVALS
  !     ALL AROUND THE POLOIDAL SECTION, BUT IN LION NCHI IS DEFINED AS
  !     THE NUMBER OF POLOIDAL INTERVALS IN THE UPPER HALF PLANE.
  !     IN LION THE NUMBER OF POLOIDAL INTERVALS ALL AROUND IS NPOL.
  NPOL = NCHI
  NCHI = NPOL/2 + 1
  !
  !     TEST CONSISTENCY OF CHEASE DATA WITH DIMENSIONS IN LION
  IF (NPSI.GT.MDPSI) THEN
    WRITE(*,*) ' *** DIMENSION MDPSI TOO SMALL ***'
    WRITE(*,9000) NPSI
    STOP
  END IF
  IF (NCHI.GT.MDCHI) THEN
    WRITE(*,*) ' *** DIMENSION MDCHI TOO SMALL ***'
    WRITE(*,9000) NCHI
    STOP
  END IF
  !
  !     TEST IF EQUILIBRIUM IS UP/DOWN SYMMETRIC.
  IF (.NOT.NLSYM) THEN
    WRITE(*,*) ' *** UP/DOWN ASYMMETRIC EQUILIBRIUM ***'
  END IF
  !
  !     TEST OPTIONS NDENS or NTEMP = -2 ARE NOT POSSIBLE FOR BETA=0 
  !     EQUILIBRIUM
  IF (NDENS.EQ.-2) THEN
    WRITE(*,*) ' YOU HAVE SELECTED NDENS=-2'
    WRITE(*,*) ' This implies a mass density profile'
    WRITE(*,*) ' proportional to sqrt(equilibrium pressure)'
    WRITE(*,*) ' with ion species densities on axis'
    WRITE(*,*) ' specified by CENDEN(.).'
    WRITE(*,*) ' The code does not test the consistency of the'
    WRITE(*,*) ' beta_axis value with the equilibrium value.'
    IF (BETA.EQ.0._RKIND) THEN
      WRITE(*,*) ' NDENS=-2 impossible for beta=0 equilibrium'
      STOP
    END IF
  END IF
  !
  IF (NTEMP.EQ.-2) THEN
    WRITE(*,*) ' YOU HAVE SELECTED NTEMP=-2'
    WRITE(*,*) ' This implies temperature profiles'
    WRITE(*,*) ' proportional to sqrt(equilibrium pressure)'
    WRITE(*,*) ' with ion and electron temperatures on axis'
    WRITE(*,*) ' specified by CENTI(.),CENTIP(.),CENTE.'
    WRITE(*,*) ' The code does not test the consistency of the'
    WRITE(*,*) ' beta_axis value with the equilibrium value.'
    IF (BETA.EQ.0._RKIND) THEN
      WRITE(*,*) ' NTEMP=-2 impossible for beta=0 equilibrium'
      STOP
    END IF
  END IF

  !
  !     NUMERICAL PARAMETERS
  NCOLMN = 6*NCHI-6
  LENGTH = NCOLMN*NCOLMN
  NCON   = 6
  !
  !     S.I. FUNDAMENTAL UNITS
  ZE  = 1.6022E-19_RKIND
  ZMP = 1.673E-27_RKIND
  ZMU0 = 4.0E-7_RKIND * CPI
  !
  !     TOTAL MASS DENSITY ON MAGNETIC AXIS
  ZMDEN = 0._RKIND
  DO J=1,NRSPEC
    ZMDEN = ZMDEN + CENDEN(J)*AMASS(J)*ZMP
  END DO
  !
  !     ALFVEN SPEED ON MAGNETIC AXIS
  ZALFSP = BNOT / SQRT(ZMU0*ZMDEN)
  !
  !     NORMALIZED QUANTITIES
  OMEGA = 2._RKIND*CPI * FREQCY * RMAJOR / ZALFSP

  DO J=1,NRSPEC
    CEOMCI(J) = ACHARG(J)*ZE*BNOT*RMAJOR / (AMASS(J)*ZMP*ZALFSP)
    FRAC(J) = CENDEN(J) * AMASS(J) * ZMP / ZMDEN
  END DO
  !
  !     FACTOR FOR V-THERMAL IONS (NRL FORMULARY P.29, BUT S.I. UNITS),
  !     NORMALIZED TO THE ALFVEN VELOCITY ON MAGNETIC AXIS
  SIGMA = 9.79E3_RKIND / ZALFSP
  !
  !     FOURIER ANALYSIS: UPPER M THAT IS ANALYZED
  MFU = MFL + MD2FP1 - 1
  !
  !     WRITE LION NAMELIST
  CALL IODSK1 (2)
  !lvC
  !lvC     CLOSE NSAVE
  !lv         CALL IODSK1 (3)
  !----------------------------------------------------------------------
9000 FORMAT (1X,'*** PLEASE SET IT GREATER THAN OR EQUAL TO',I5,/, &
       &           1X,'*** PLEASE RECOMPILE LION AND RERUN IT')
9001 FORMAT (I5)
9002 FORMAT (4E30.13)
  !----------------------------------------------------------------------
  RETURN
END SUBROUTINE AUXVAL
!CC*DECK P1C3S02
SUBROUTINE IODSK1(K)
  !        -----------------
  !
  ! 1.3.2   HANDLES DISK FILES
  !----------------------------------------------------------------------
  USE GLOBALS
  use interpos_module
  IMPLICIT NONE
!!$  USE COMBAS ! INCLUDE 'COMBAS.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  INCLUDE 'NEWRUN.inc'
  !
  INTEGER ICHI, JPSI, NBCEFF(2), K, J, IN2, JCOL, JROW, JS, I, IPSI1, IEQ
  REAL(RKIND) :: YBCEFF(2), TENS_DEF
  !
  GO TO (100,200,300,400,500,600,700,800,900) , K
  !
  !---------------------------------------------------------------------
  !L             1.          OPEN NSAVE
  !
100 CONTINUE
  OPEN (UNIT=NSAVE, FILE='TAPE8', STATUS='OLD', FORM='FORMATTED')
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             2.          WRITE NAMELIST
  !
200 CONTINUE
  !lv         REWIND NSAVE
  !lv         WRITE (NSAVE,NEWRUN)
  WRITE (NPRNT,NEWRUN)
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             3.          CLOSE NSAVE
  !
300 CONTINUE
  CLOSE (NSAVE)
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             4.          OPEN NSOURC AND READ SOURCE VECTOR
  !
400 CONTINUE
  OPEN (UNIT=NSOURC, FILE='TAPE15', STATUS='OLD', &
       &         FORM='UNFORMATTED')
  IN2=2*NCHI-2
  REWIND NSOURC
  READ (NSOURC) IN2, (SOURCE(J),J=1,IN2)
  !
  READ (NSOURC) IN2, REASCR, QB, TB, (DC(J),J=1,IN2), &
       &                (DELTH(J),J=1,IN2), (OHMR(J),J=1,IN2)
  !
  READ (NSOURC) ((WVAC(JROW,JCOL),JCOL=1,IN2),JROW=1,IN2)
  !
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             5.          WRITE SOURCE VECTOR AND CLOSE NSOURC
  !
500 CONTINUE
  IN2=2*NCHI-2
  REWIND NSOURC
  WRITE (NSOURC) IN2, (SOURCE(J),J=1,IN2)
  !
  WRITE (NSOURC) IN2, REASCR, QB, TB, (DC(J),J=1,IN2), &
       &                (DELTH(J),J=1,IN2), (OHMR(J),J=1,IN2)
  !
  WRITE (NSOURC) ((WVAC(JROW,JCOL),JCOL=1,IN2),JROW=1,IN2)
  !
  CLOSE (NSOURC)
  !
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             6.          OPEN AND CLOSE NSOURC (NEW FILE)
  !
600 CONTINUE
  OPEN (UNIT=NSOURC, FILE='TAPE15', STATUS='NEW', &
       &         FORM='UNFORMATTED')
  CLOSE (NSOURC,STATUS='KEEP')
  RETURN
  !
  !-----------------------------------------------------------------------
  !              7.          REPAIR EQ(21,KCHI)
  !
  !     REPAIR EQ(21,KC) = D/DS (INTEGRAL FROM 0 TO CHI OF T*J/R^2).
  !     FOR SOME REASON IT IS DEFINED IN CHEASE AS THE INTEGRAL
  !     FROM 0 TO CHI-2PI IF KC > NPOL/2+1 AND NPOL EVEN, OR
  !     IF KC > (NPOL+1)/2+1 AND NPOL ODD. SO WE MUST ADD 2PI*DQ/DS
  !     TO THE CORRESPONDING EQ(21,KC)
  !
700 CONTINUE
  !cc         OPEN (UNIT=MEQ, FILE='TAPE4', STATUS='OLD',
  !cc     +         FORM='UNFORMATTED')
  !cc         REWIND MEQ
  !cc         DO 720 JS = 1, NPSI
  !cc            READ (MEQ) ((EQ(I,J),I=1,MDEQ),J=1,NPOL)
  !cc            ZDQDS = (QS(JS+1)-QS(JS)) / (EQ(3,1)-EQ(1,1))
  !cc            ICU = NPOL/2+2
  !cc            IF (MOD(NPOL,2).EQ.1) ICU = (NPOL+1)/2 + 1
  !cc            DO 710 JC = ICU, NPOL
  !cc               EQ(21,JC) = EQ(21,JC) + C2PI * ZDQDS
  !cc 710        CONTINUE
  !cc            BACKSPACE MEQ
  !cc            WRITE (MEQ) ((EQ(I,J),I=1,MDEQ),J=1,NPOL)
  !cc 720     CONTINUE
  !cc         CLOSE (MEQ)
  RETURN
  !
  !-----------------------------------------------------------------------
  !                8. Open, Close and delete NSOURC
800 CONTINUE
  OPEN (UNIT=NSOURC,FILE='TAPE15',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  CLOSE(NSOURC,STATUS='DELETE')
  RETURN
  !
  !-----------------------------------------------------------------------
  !                9. Read equilibrium quantities (CHEASE) -> to EQ(.,.,.)
900 CONTINUE
  OPEN (UNIT=MEQ,FILE='TAPE4',STATUS='OLD', &
       &         FORM='UNFORMATTED')
  DO js=1,npsi+1
    READ (MEQ)((EQ2(I,J,js),I=1,MDEQ),J=1,NPOL)
  END DO
  CLOSE (MEQ)
  !
  !     9.1 With new CHEASE, values are on CS(2:NS+1), interpolate back on to mid-mesh the relevant values
  !
  IPSI1 = NPSI + 1
  EQ = EQ2
  TENS_DEF = -0.1_RKIND
  nbceff(1)=2
  nbceff(2)=2
  DO IEQ=7,11
    ybceff(1) = EQ2(IEQ,1,1)
    ybceff(2) = EQ2(IEQ,1,IPSI1)
    call interpos(EQ2(1,1,:),EQ2(IEQ,1,:),NIN=IPSI1,NOUT=IPSI1, &
         &       TENSION=TENS_DEF,XOUT=EQ(5,1,:),YOUT=EQ(IEQ,1,:), &
         &       nbc=nbceff,ybc=ybceff)
    DO ICHI=2,NPOL
      EQ(IEQ,ICHI,:) = EQ(IEQ,1,:)
    END DO
  end do
  DO IEQ=26,26
    ybceff(1) = EQ2(IEQ,1,1)
    ybceff(2) = EQ2(IEQ,1,IPSI1)
    call interpos(EQ2(1,1,:),EQ2(IEQ,1,:),NIN=IPSI1,NOUT=IPSI1, &
         &       TENSION=TENS_DEF,XOUT=EQ(5,1,:),YOUT=EQ(IEQ,1,:), &
         &       nbc=nbceff,ybc=ybceff)
    DO ICHI=2,NPOL
      EQ(IEQ,ICHI,:) = EQ(IEQ,1,:)
    END DO
  END DO
  !   CHI dependent with BC: [1 2], [1e32 y(end)]
  nbceff(1)=1
  nbceff(2)=2
  ybceff(1) = 1.0E+32_RKIND
  DO IEQ=12,13
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  DO IEQ=15,15
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  DO IEQ=18,22
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  DO IEQ=24,25
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  DO IEQ=27,27
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  !   CHI dependent with BC: [2 2], [y(1) y(end)]
  nbceff(1)=2
  nbceff(2)=2
  DO IEQ=14,16,2
    DO ICHI=1,NPOL
      ybceff(1) = EQ2(IEQ,ICHI,1)
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  DO IEQ=23,26,3
    DO ICHI=1,NPOL
      ybceff(1) = EQ2(IEQ,ICHI,1)
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:),NIN=IPSI1, &
           &         NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
    END DO
  END DO
  !   CHI dependent with BC: [1 2], [1e32 y(end)*s(end)] AND interpolating on y*s from (2:end)
  nbceff(1)=1
  nbceff(2)=2
  ybceff(1) = 1.0E+32_RKIND
  DO IEQ=17,17
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)*EQ2(1,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:)*EQ2(1,ICHI,:), &
           &         NIN=IPSI1,NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
      EQ(IEQ,ICHI,:) = EQ(IEQ,ICHI,:) / EQ(5,ICHI,:)
    END DO
  END DO
  DO IEQ=28,29
    DO ICHI=1,NPOL
      ybceff(2) = EQ2(IEQ,ICHI,IPSI1)*EQ2(1,ICHI,IPSI1)
      call interpos(EQ2(1,ICHI,:),EQ2(IEQ,ICHI,:)*EQ2(1,ICHI,:), &
           &         NIN=IPSI1,NOUT=IPSI1,TENSION=TENS_DEF,XOUT=EQ(5,ICHI,:), &
           &         YOUT=EQ(IEQ,ICHI,:),nbc=nbceff,ybc=ybceff)
      EQ(IEQ,ICHI,:) = EQ(IEQ,ICHI,:) / EQ(5,ICHI,:)
    END DO
  END DO
  !   
  !%OS         OPEN (UNIT=MEQ,FILE='TAPE4_OUT',FORM='FORMATTED')
  !%OS         DO js=1,npsi+1
  !%OS            WRITE (MEQ,*)((EQ(I,J,js),I=1,MDEQ),J=1,NPOL)
  !%OS          END DO
  !%OS         CLOSE (MEQ)
  !
  !-----------------------------------------------------------------------
END SUBROUTINE IODSK1
!CC*DECK P2C2S01
SUBROUTINE VACUUM
  !
  ! 2.2.1   ORGANIZE VACUUM CONTRIBUTION
  !
  !
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !-----------------------------------------------------------------------
  !-----------------------------------------------------------------------
  !L             1.          OPEN DISK FILES AND INPUT QUANTITIES
  !
  CALL IODSK2(1)
  !
  !=======================================================================
  !     NO VACUUM (WALL ON THE PLASMA BOUNDARY)

  IF (WALRAD.LE.1._RKIND) THEN
    !
    CALL VACZER
    !
  END IF
  !
  !-----------------------------------------------------------------------
  !L             2.          THETA MESH
  !
  CALL TETMSH
  !
  !=======================================================================
  !     WITH VACUUM
  !
  IF (WALRAD.GT.1._RKIND) THEN
    !
    !-----------------------------------------------------------------------
    !L             3.          POTENTIAL JUMP AND VACUUM MATRICES
    !
    CALL PLANSH
    !
    CALL CURENT
    !
    CALL ABCDEF(0)
    !
    CALL MOPPOW(1)
    !
    !-----------------------------------------------------------------------
    !L             4.          CONSTRUCT GREEN MATRIX
    !
    CALL QCON
    !
    CALL MOPPOW(2)
    !
    !-----------------------------------------------------------------------
    !L             5.          TOTAL DELTA W VACUUM
    !
    CALL WCON
    !
    !-----------------------------------------------------------------------
    !L             7.          HERMICITY CONDITIONS
    !
    CALL HERMIC
    !
  END IF
  !=======================================================================
  !-----------------------------------------------------------------------
  !L             8.          STORE VACUUM CONTRIBUTION AND CLOSE FILES
  !
  CALL IODSK2(2)
  !
  RETURN
END SUBROUTINE VACUUM
!CC*DECK P2C2S00
SUBROUTINE VACZER
  !        -----------------
  !
  ! 2.2.0  PUT VACUUM CONTRIBUTION TO ZERO
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  INTEGER :: J
  !---------------------------------------------------------------------
  !
  CALL CVZERO (NPOL, SOURCE(1),1)
  !
  CALL CVZERO (NPOL, OHMR(1),1)
  !
  DO J = 1, NPOL
    CALL CVZERO(NPOL, WVAC(1,J), 1)
  END DO
  !
  REASCR =  CMPLX(0._RKIND,0._RKIND)
  !
  RETURN
END SUBROUTINE VACZER
!CC*DECK P2C2S02
SUBROUTINE TETMSH
  !        -----------------
  !
  ! 2.2.2   THETA MESH IN VACUUM REGION
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZRMIN, ZRMAX
  INTEGER :: J, IPOL1, JINT
  !-----------------------------------------------------------------------
  !
  !     T(J) : THETA AT MID-CHI INTERVALS
  !     TH(J): THETA AT EDGES OF CHI INTERVALS
  !     THESE ARRAYS HAVE BEEN READ IN IODSK2(1) DIRECTLY FROM EQUILIBRIUM
  !-----------------------------------------------------------------------
  !
  !     PARAMETERS FOR ANTENNA AND SHELL DEFINITION
  CA = WALRAD - 1.0_RKIND                  
  ZRMIN = 1.0E14_RKIND
  ZRMAX = 0._RKIND
  !
  ZRMIN = MIN(ZRMIN,MINVAL(SR(1:NPOL)))
  ZRMAX = MAX(ZRMAX,MAXVAL(SR(1:NPOL)))
  !
  CB = (ZRMAX - ZRMIN) * 0.5_RKIND
  CC = 0._RKIND
  CANTEN = ANTRAD -1.0_RKIND
  !
  !     FORM DELTA CHI AND DELTA THETA
  DO J=1,NPOL
    DC(J)=CHI(J+1)-CHI(J)
    DELTH(J) = TH(J+1) - TH(J)
  END DO
  !
  !     PRINTOUT
  IF (NLOTP2(1)) THEN
    !
    IPOL1 = NPOL + 1
    WRITE(NPRNT,1000) (SR(J),SZ(J),CHI(J),DC(J),T(J),TH(J), &
         &                         DELTH(J),J=1,IPOL1)
    WRITE(NPRNT,1100) 
    !
    DO J=1,IPOL1
      WRITE(NPRNT,1110) J,TH(J),ROEDGE(J)
      WRITE(NPRNT,1110) J,T(J),ROMID(J)
      WRITE(NPRNT,1112) (JINT,THINT(J,JINT),ROINT(J,JINT), &
           &                            DROINT(J,JINT),JINT=1,3)
    END DO
    !
  END IF
  !---------------------------------------------------------------------
1000 FORMAT(////,' 2.1 OUTPUT FROM TETMSH',/,1X,22('-')/// &
       &  '       SR            SZ             CHI              DC     ', &
       &  '       T             TH            DELTH'//(1P7E15.5))
1010 FORMAT(30X,1PE15.5)
1100 FORMAT(///'    JPOL,JINT        THETA           RHO      ', &
       &             '   DRHO/DTHETA'//)
1110 FORMAT(1X,I10,5X,1P2E15.5)
1112 FORMAT(1X,I15,1P3E15.5)
  !---------------------------------------------------------------------
  RETURN
END SUBROUTINE TETMSH
!CC*DECK P2C2S03
SUBROUTINE ABCDEF (KONLYANT)
  !        ----------------
  !
  ! 2.2.3   CALCULATES MATRICES OF INTEGRAL OPERATORS D_mu,nu E_mu,nu
  !         Eqs.(3.68)(3.69) of Ref.[4].
  !---------------------------------------------------------------------
  !     KONLYANT = 0 => ALL 13 MATRICES
  !     KONLYANT = 1 => ONLY ANTENNA-RELATED MATRICES (JM=7 TO 13)
  !---------------------------------------------------------------------
  !
  !    G(.,.,JM)
  !
  ! JM  1    2    3    4    5    6    7    8    9    10   11   12   13
  !
  !     A    B    C    D    E    F    GFAT HFAT FATK FATL FATM FATN RFAT
  !     
  !     Dpp  Epp  Dps  Dsp  Esp  Dss  Dpa  Epa  Dap  Daa  Eap  Eaa  Dsa
  !---------------------------------------------------------------------
  !
  USE GLOBALS
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZKN, ZKPN
  INTEGER :: ILONG, ICHI2, KONLYANT, ILONGNEW, ISTART, IEND, IN, JM, JP, J
  !
  !     Length (for memory storage) of array G(.,.,.)
  ILONG = 13 * MDPOL * MDPOL
  !     Dimension (for memory storage) of matrices
  !lv         NDIM=IFIX(SQRT(REAL(ILONG/13)+1.0))
  NDIM = MDPOL
  !     Rank of matrices
  ICHI2= NPOL
  !
  IF (KONLYANT.EQ.0) THEN
    ILONGNEW  = 13 * MDPOL * MDPOL
    ISTART =  1
    IEND   = 13
  ELSE
    ILONGNEW  =  7 * MDPOL * MDPOL
    ISTART = 7
    IEND   = 13
  END IF
  !
  IN=IFIX(ABS(WNTORO))
  IF (IN.NE.0) THEN
    CALL EKNSIE(10._RKIND,IN,ZKN,ZKPN)
  END IF
  !
  !
  !     Reset relevant matrices, i.e. all matrices for KONLYANT=0, matrices 7 to 13
  !     for KONLYANT=1
  CALL RESETR(G(1,1,ISTART),ILONGNEW,0.0_RKIND)
  !
  !     LOOP FOR THE 13 MATRICES
  !
  DO JM=ISTART,IEND
    !
    !     LOOP FOR THETA
    !
    DO J= 1,ICHI2
      !
      !     LOOP FOR THETA PRIME
      !
      DO JP= 1,ICHI2
        CALL ROTETA(JM,J,JP)
        CALL CONCEL(JM,J,JP)
      END DO
    END DO
  END DO
  !
  RETURN
END SUBROUTINE ABCDEF
!CC*DECK P2C2S04
SUBROUTINE ROTETA(KM,K,KP)
  !        -----------------
  !
  ! 2.2.4   RHO, DRHO/DTHETA AND THETA AT INTEGRATION POINTS
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !-----------------------------------------------------------------------
  REAL(RKIND) :: ZRO(4),   ZDRO(4), ZSQ6
  INTEGER :: IARG1(13),     IARG2(13),     IFLAG(13), J, K, KM, KP
  DATA IFLAG/ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0/
  DATA IARG1/ 1, 1, 1, 3, 3, 3, 1, 1, 2, 2, 2, 2, 3/
  DATA IARG2/ 1, 1, 3, 1, 1, 3, 2, 2, 1, 2, 1, 2, 2/
  !          MATRIX: A, B, C, D, E, F, G, H, K, L, M, N, R
  !      KM  NUMBER: 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13
  !
  !          IARG1,IARG2:  1=XP (PLASMA),  2=XA (ANTENNA),  3=XS (WALL)
  !-----------------------------------------------------------------------
  !
  !     THE INTEGRATION POINTS IN THETA ARE READ FROM CHEASE EQUILIBRIUM,
  !     WITH THE CORRESPONDING VALUES OF RHO AND DRHO/DTHETA AT THE PLASMA
  !     SURFACE, IN ARRAYS THINT(.,.), ROINT(.,.) AND DROINT(.,.).
  !
  !     IN THIS VERSION THE INTEGRATION POINTS ARE:
  !
  !     THINT(K,1) = (TH(K)+TH(K+1))/2 - 1/SRQT(6) * (TH(K+1)-TH(K))
  !     THINT(K,2) = (TH(K)+TH(K+1))/2
  !     THINT(K,3) = (TH(K)+TH(K+1))/2 + 1/SRQT(6) * (TH(K+1)-TH(K))
  !
  !     WHERE TH(K) IS THE THETA ANGLE AT THE EDGES OF CHI INTERVALS AT
  !     PLASMA SURFACE, ALSO READ FROM CHEASE EQUILIBRIUM.
  !-----------------------------------------------------------------------
  !
  ZSQ6=1._RKIND/SQRT(6._RKIND)
  !
  !-----------------------------------------------------------------------
  !L             1.  THETA, RHO, DRHO/DTHETA AT INTEGRATION POINTS 
  !                  FOR INTERVAL NO.K
  !
  !     THETA
  TT(1) = THINT(K,1)
  TT(2) = THINT(K,2)
  TT(3) = THINT(K,3)
  TT(4) = THINT(K,2)
  !
  !     RHO AND DRHO/DTHETA ON PLASMA SURFACE
  ZRO(1) = ROINT(K,1)
  ZRO(2) = ROINT(K,2)
  ZRO(3) = ROINT(K,3)
  ZRO(4) = ROINT(K,2)
  !
  ZDRO(1) = DROINT(K,1)
  ZDRO(2) = DROINT(K,2)
  ZDRO(3) = DROINT(K,3)
  ZDRO(4) = DROINT(K,2)
  !
  !     INTERVAL IS ON PLASMA SURFACE
  IF (IARG1(KM).EQ.1) THEN
    DO J=1,4
      RO(J) = ZRO(J)
      DRODT(J) = ZDRO(J)
    END DO
  END IF
  !
  !     INTERVAL IS ON THE ANTENNA
  IF (IARG1(KM).EQ.2) THEN
    DO J=1,4
      CALL ANTEN (TT(J),ZRO(J),ZDRO(J),CA,CB,CANTEN,RO(J), &
           &                     DRODT(J))
    END DO
  END IF
  !
  !     INTERVAL IS ON THE WALL
  IF (IARG1(KM).EQ.3) THEN
    DO  J=1,4
      CALL WALL_SUB (TT(J),ZRO(J),ZDRO(J),CA,CB,CC,RO(J),DRODT(J))
    END DO
  END IF
  !
  !     RHO AT MIDDLE INTEGRATION POINT FOR USE IN CONCEL
  RO2 = RO(2)
  !
  !-----------------------------------------------------------------------
  !L             2.  THETA, RHO, DRHO/DTHETA AT INTEGRATION POINTS 
  !                  FOR INTERVAL NO. K-PRIME (KP)
  !
  !     THETA
  TP(1) = THINT(KP,2)
  TP(2) = THINT(KP,1)
  TP(3) = THINT(KP,2)
  TP(4) = THINT(KP,3)
  !
  !     RHO AND DRHO/DTHETA ON PLASMA SURFACE
  ZRO(1) = ROINT(KP,2)
  ZRO(2) = ROINT(KP,1)
  ZRO(3) = ROINT(KP,2)
  ZRO(4) = ROINT(KP,3)
  !
  ZDRO(1) = DROINT(KP,2)
  ZDRO(2) = DROINT(KP,1)
  ZDRO(3) = DROINT(KP,2)
  ZDRO(4) = DROINT(KP,3)
  !
  !     INTERVAL IS ON PLASMA SURFACE
  IF (IARG2(KM).EQ.1) THEN
    DO J=1,4
      ROP(J) = ZRO(J)
      DROPDT(J) = ZDRO(J)
    END DO
  END IF
  !
  !     INTERVAL IS ON ANTENNA
  IF (IARG2(KM).EQ.2) THEN
    DO J=1,4
      CALL ANTEN (TP(J),ZRO(J),ZDRO(J),CA,CB,CANTEN,ROP(J), &
           &                     DROPDT(J))
    END DO
  END IF
  !
  !     INTERVAL IS ON WALL
  IF (IARG2(KM).EQ.3) THEN
    DO J=1,4
      CALL WALL_SUB (TP(J),ZRO(J),ZDRO(J),CA,CB,CC,ROP(J), &
           &                    DROPDT(J))
    END DO
  END IF
  !
  !     RHO AT MIDDLE INTEGRATION POINT FOR USE IN CONCEL
  RO2P = ROP(1)

  !
  !-----------------------------------------------------------------------
  !
  !     RHO AND RHO PRIME AT (TH(K+1)+TH(K))/2
  !     FOR USE IN CONCEL (NUMERICAL CORRECTION TO DIAGONAL TERMS)
  RO2 = RO(2)
  RO2P = ROP(1)
  !
  RETURN
END SUBROUTINE ROTETA
!CC*DECK P2C2S05
SUBROUTINE WALL_SUB (PT,PROPL,PDROPL,PA,PB,PC,PROWA,PDROWA)
  !        ---------------
  !
  ! 2.2.5   DEFINES RHO AND DRHO/DTHETA ON THE WALL, AT ANGLE THETA=PT
  !-----------------------------------------------------------------------
  USE GLOBALS
  IMPLICIT NONE
  !
  REAL(RKIND) :: PT, PROPL, PDROPL, PA, PB, PC, PROWA, PDROWA
  !
  PROWA = PROPL + PA * PB
  PDROWA = PDROPL
  !
  RETURN
END SUBROUTINE WALL_SUB
!CC*DECK P2C2S06
SUBROUTINE CONCEL(KM,K,KP)
  !        -----------------
  !
  ! 2.2.6   CONTRIBUTION OF A CELL FOR GREEN'S FUNCTION MATRICES D_mu,nu
  !         AND E_mu,nu.
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !-----------------------------------------------------------------------
  REAL(RKIND) :: ZD, ZRMIN, ZR, ZRO, ZROP, ZRODT, ZROPDT, ZT, ZTP, ZRP, &
       & ZDR, ZDT, ZA2, ZE, ZF, ZINT, ZFDRP, ZP, ZFDE, ZPDT, ZINT1, ZTM, ZRM, &
       & ZRPM, ZDEL, ZDEL2, ZDRM, ZI, ZKN, ZKN0, ZKNP, ZKNP0
  INTEGER :: IFLAG(13), IT2M2, IN, IN2, JNT, KP, K, KM
  DATA IFLAG/ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0/
  !
  !      IFLAG = 0 FOR D_mu,nu;  1 FOR E_mu,nu
  !-----------------------------------------------------------------------
  IT2M2 = 2*NCHI-2
  IN=IFIX(ABS(WNTORO))
  ZD = (TH(KP+1)-TH(KP))/8._RKIND/CPI
  !
  !-----------------------------------------------------------------------
  !L             1.          REGULAR INTEGRALS
  !
  ! *** GENERAL NUMERICAL TERM
  !>>>         ZRMIN=0.05*(1.0-ASPCT)
  ZRMIN = 0.02_RKIND
  !<<<
  !---------------------------------------------------------------
  !L             LOOP OVER INTEGRATION POINTS
  !
  DO JNT=1,4
    !
    ZRO=RO(JNT)
    ZROP=ROP(JNT)
    ZRODT=DRODT(JNT)
    ZROPDT=DROPDT(JNT)
    ZT=TT(JNT)
    ZTP=TP(JNT)
    ZR = 1._RKIND+ZRO*COS(ZT)
    ZRP = 1._RKIND+ZROP*COS(ZTP)
    !
    IF (ZR.GE.ZRMIN) GO TO 90
    ZR = ZRMIN
    ZRO = (ZR-1._RKIND)/COS(ZT)
90  CONTINUE
    !
    IF ( ZRP.GE.ZRMIN) GO TO 100
    ZRP= ZRMIN
    ZROP=(ZRP-1._RKIND)/COS(ZTP)
100 CONTINUE
    !
    ZDR=ZROP-ZRO
    ZDT=ZTP-ZT
    ZA2=ZDR*ZDR+4.0_RKIND*ZRO*ZROP*SIN(ZDT/2.0_RKIND)**2
    ZE=ZA2/(ZA2+4.0_RKIND*ZR*ZRP)
    ZF=2.0_RKIND*(-1.0_RKIND)**IN*SQRT((1.0_RKIND-ZE)/(ZR*ZRP))
    !
    CALL EKN(ZE,IN,ZKN,ZKNP)
    !
    ZINT = ZF*ZKN
    !
    IF(IFLAG(KM).EQ.1) GO TO 110
    ZFDRP=-ZF/(2.0_RKIND*ZRP)
    ZP=ZA2*(1.0_RKIND-(ZROP*COS(ZTP)+ZROPDT*SIN(ZTP))/ZRP)+ &
         &      ZDR*ZDR+2.0_RKIND*ZRO*(ZDR-ZROPDT*SIN(ZDT))
    ZP=4.0_RKIND*ZP*ZR*ZRP/(ZA2+4.0_RKIND*ZR*ZRP)**2
    ZFDE=-ZF/(2.0_RKIND*(1.0_RKIND-ZE))
    ZPDT=ZROP*COS(ZTP)+ZROPDT*SIN(ZTP)
    ZINT=(ZFDE*ZKN+ZF*ZKNP)*ZP+ZKN*ZFDRP*ZPDT
    ZINT=ZINT*ZRP
    CALL EKN(ZE,0,ZKN0,ZKNP0)
    ZINT1 = (ZFDE*ZKN0+ZF*ZKNP0)*ZP+ZKN0*ZFDRP*ZPDT
    ZINT1=ZINT1*ZRP*(-1._RKIND)**IN
    G(K,K,KM) = G(K,K,KM) - ZD*ZINT1
110 CONTINUE
    !
    G(K,KP,KM)=G(K,KP,KM)+ZD*ZINT
    !
    IF ((K .EQ. KP) .AND. (IFLAG(KM).NE.0)) THEN
      !
      !-----------------------------------------------------------------------
      ! *** NUMERICAL CORRECTION TO DIAGONAL TERM FOR B,E
      !
      ZTM=(TH(K+1)+TH(K)) /2.0_RKIND
      ZRM=1.0_RKIND+RO2*COS(ZTM)
      ZRPM=1.0_RKIND+RO2P*COS(ZTM)
      !
      IF (ZRM .GE. ZRMIN) GO TO 113
      ZRM=ZRMIN
      RO2=(ZRM-1.0_RKIND)/COS(ZTM)
113   CONTINUE
      !
      IF (ZRPM .GE. ZRMIN) GO TO 117
      ZRPM=ZRMIN
      RO2P=(ZRPM-1.0_RKIND)/COS(ZTM)
117   CONTINUE
      !
      ZDRM=RO2P-RO2
      ZDEL2=ZDRM**2/(RO2*RO2P)
      ZR=SQRT(ZRM*ZRPM)
      !
      G(K,K,KM)=G(K,K,KM)+ZD*LOG(ZDEL2+(ZT-ZTP)**2)/ZR
      !
    END IF
  END DO
  !
  !-----------------------------------------------------------------------
  !
  IF (K.NE.KP) RETURN
  IF(IFLAG(KM).EQ.0) RETURN
  !
  !-----------------------------------------------------------------------
  ! *** ANALYTICAL CORRECTION TO DIAGONAL TERM FOR B,E
  !
  ZDEL=SQRT(ZDEL2)
  ZDT=TH(K+1)-TH(K)
  ZI=0.0_RKIND
  IF (ZDEL2 .GT. 1.E-14_RKIND) ZI=ZDEL2*LOG(ZDEL2)+ &
       &         4.0_RKIND*ZDEL*ZDT*ATAN(ZDT/ZDEL)
  ZI=ZI-3.0_RKIND*ZDT**2+(ZDT**2-ZDEL2)*LOG(ZDT**2+ZDEL2)
  G(K,K,KM)=G(K,K,KM)-4.0_RKIND*ZD*ZI/(ZR*ZDT*ZDT)
  !
  RETURN
END SUBROUTINE CONCEL
!CC*DECK P2C2S07
SUBROUTINE QCON
  !        ---------------
  !
  ! 2.2.9   PERFORMS MATRIX OPERATIONS FOR Q-MATRIX AND VECTOR PHI_E
  !         SEE REF[1], EQ.(3.31B); REF[4], EQ.(3.73)
  !-----------------------------------------------------------------------
  USE GLOBALS
  IMPLICIT NONE
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  INCLUDE 'COMVID.inc'
  !
  REAL(RKIND) :: ZS, ZT, ZTRA, ZA, ZST
  INTEGER :: IN2, IN, J, I, NUL, JSHEET
  !-----------------------------------------------------------------------
  !
  !     NUMBER OF ROWS/COLUMNS IN THE MATRICES
  IN2 = NPOL
  !
  !     REDUCE MATRICES IF N=0
  IN=IFIX(ABS(WNTORO))
  IF (IN .EQ. 0) CALL ZERO(IN2)
  !
  !     A2D := A2D - 2   (Dpp-2)
  DO J=1,IN2
    G(J,J,1) = G(J,J,1)-2._RKIND
  END DO
  !
  !=======================================================================
  !     WALL AT INFINITY
  !
  IF (WALRAD.GT.1000._RKIND) THEN
    CALL IMGC (G(:,:,1),IN2,NDIM,NUL)
    !
    !=======================================================================
    !     WALL AT FINITE DISTANCE
  ELSE
    !
    !
    !-----------------------------------------------------------------------
    !L             1.          (C-2*I)*F**(-1)
    !                          (Dps-2) Dss^-1
    !
    DO J=1,IN2
      G(J,J,3) = G(J,J,3)-2._RKIND
    END DO
    IF (IN .NE. 0) CALL IMGC(G(:,:,6),IN2,NDIM,NUL)
    IF (IN .EQ. 0) CALL IMGC(G(:,:,6),IN2-1,NDIM,NUL)
    CALL MULT(G(:,:,3),G(:,:,6),IN2,NDIM)
    REWIND NSOURC
    WRITE (NSOURC) G(:,:,3)
    !
    !-----------------------------------------------------------------------
    !L             2.          (A2D-2*I-(C-2*I)*F**(-1)*D)**(-1)
    !                          Mpp^-1 = (Dpp - 2 - (Dps-2) Dss^-1 Dsp
    !
    CALL MULT(G(:,:,3),G(:,:,4),IN2,NDIM)
    DO J=1,IN2
      DO I=1,IN2
        G(I,J,1)=G(I,J,1)-G(I,J,3)
      END DO
    END DO
    CALL IMGC(G(:,:,1),IN2,NDIM,NUL)
    WRITE(NSOURC) G(:,:,1)
    !
    !-----------------------------------------------------------------------
    !L             3.          Q-MATRIX
    !                          Mpp^-1 (Epp - (Dps-2) Dss^-1 Esp
    !
    REWIND NSOURC
    READ (NSOURC) G(:,:,3)
    CALL MULT(G(:,:,3),G(:,:,5),IN2,NDIM)
    DO J=1,IN2
      DO I=1,IN2
        G(I,J,2)=G(I,J,2)-G(I,J,3)
      END DO
    END DO
    !
    !     END OF CASE WALL AT FINITE DISTANCE
  END IF
  !=======================================================================
  !
  CALL MULT (G(:,:,1),G(:,:,2),IN2,NDIM)
  !
  !     MATRIX A2D IS THE Q-MATRIX, EQ(31) LRP 157/79; EQS(3.72-73) REF.[4]
  !
  !     STORE IT FOR USE IN POWER
  READ(NSOURC) G(:,:,3)
  WRITE(NSOURC) G(:,:,1)
  !
  !     WE NOW MODIFY IT FOR USE IN DEL-W-VAC
  ZTRA = TB*TB
  DO J=1,IN2
    DO I=1,IN2
      G(J,I,1)=G(J,I,1)*ZTRA*DC(J)*DC(I)/(TH(I+1)-TH(I))
    END DO
  END DO
  !
  ZS=0.0_RKIND
  ZT=0.0_RKIND
  !
  !     SYMMETRIZE A2D(I,J) = A2D(J,I)
  DO J=1,IN2
    DO I=J,IN2
      ZA=(G(I,J,1)+G(J,I,1))/2.0_RKIND
      ZS=ZS+(G(I,J,1)-G(J,I,1))**2
      ZT=ZT+G(I,J,1)**2
      G(I,J,1)=ZA
      G(J,I,1)=ZA
    END DO
  END DO
  !
  ZST=ZS/ZT
  !
  !-----------------------------------------------------------------------
  !L             4.          ANTENNA VECTOR PHI_E
  !
  !
  !     READ (C-2*I)*F**(-1) INTO B
  !     READ (Dps-2) Dss^-1  INTO B
  !
  REWIND NSOURC
  READ(NSOURC) G(:,:,2)
  !
  !     READ (A2D-2*I-(C-2*I)*F**(-1)*D)**(-1) INTO C
  !     READ Mpp^-1 INTO C
  !
  READ(NSOURC) G(:,:,3)
  !
  !     PHI_E = Mpp^-1 ( (Dps-2) Dss^-1 Dsa - (Dpa-2) ) b_a
  !
!!$  CALL MATVEC(RFAT,SAUTR,ANTR,IN2,NDIM)
  CALL MATVEC(G(:,:,13),SAUTR,ANTR,IN2,NDIM)
!!$  CALL MATVEC(B,ANTR,SAUTX,IN2,NDIM)
  CALL MATVEC(G(:,:,2),ANTR,SAUTX,IN2,NDIM)
!!$  CALL MATVEC(GFAT,SAUTR,ANTR,IN2,NDIM)
  CALL MATVEC(G(:,:,7),SAUTR,ANTR,IN2,NDIM)
  !
  DO J=1,IN2
    SAUTX(J)=SAUTX(J)-ANTR(J)
  END DO
  !
!!$  CALL MATVEC(C,SAUTX,ANTR,IN2,NDIM)
  CALL MATVEC(G(:,:,3),SAUTX,ANTR,IN2,NDIM)
  !
  !---------------------------------------------------------------------
  !L             5.     ANTENNA VECTOR PHI_E FOR MULTIPLE ANTENNA SHEETS
  !
  IF (NANTSHEET.GT.1) THEN
    !
    DO JSHEET=2,NANTSHEET
      !
      !     ANTENNA - PLASMA DISTANCE
      CANTEN = CANTEN + (ANTRADMAX-ANTRAD)/(NANTSHEET-1)
      !lv
      write(*,*) ' CANTEN= ', CANTEN
      !
      !     RECONSTRUCT MATRICES D_{mu,nu} and E_{mu,nu} FOR ANTENNA ONLY
      CALL PLANSH
      CALL CURENT
      CALL ABCDEF(1)
      !CC               CALL MOPPOW(1)
      !
      !     STORE D_{pa}-2 INTO GFAT -->  MOPPOW(1)
      DO J=1,IN2
        G(J,J,7) = G(J,J,7)-2
      END DO
      !
      !     READ (C-2*I)*F**(-1) INTO B
      !     READ (Dps-2) Dss^-1  INTO B
      REWIND NSOURC
      READ(NSOURC) G(:,:,2)
      !
      !     READ (A2D-2*I-(C-2*I)*F**(-1)*D)**(-1) INTO C
      !     READ Mpp^-1 INTO C
      READ(NSOURC) G(:,:,3)
      !
      !     PHI_E = Mpp^-1 ( (Dps-2) Dss^-1 Dsa - (Dpa-2) ) b_a
      CALL CVZERO(IN2,ANTRX(1),1)
      CALL MATVEC(G(:,:,13),SAUTR,ANTRX,IN2,NDIM)
      CALL MATVEC(G(:,:,2),ANTRX,SAUTX,IN2,NDIM)
      CALL MATVEC(G(:,:,7),SAUTR,ANTRX,IN2,NDIM)
      DO J=1,IN2
        SAUTX(J)=SAUTX(J)-ANTRX(J)
      END DO
      CALL MATVEC(G(:,:,3),SAUTX,ANTRX,IN2,NDIM)
      !
      !     ADD SHEET CONTRIBUTION TO ANTENNA VECTOR PHI_E
      DO J=1,IN2
        ANTR(J) = ANTR(J) + ANTRX(J)
      END DO
      !
    END DO

  END IF
  !
  !*********************************************************************
  !
  !     THE ANTENNA VECTOR WILL BE NORMALIZED AT THE END OF MOPPOW(2)
  !
  !*********************************************************************
  RETURN
END SUBROUTINE QCON
!CC*DECK P2C2S08
SUBROUTINE ZERO(K)
  !        ---------------
  !
  ! 2.2.10  REDUCES MATRIX FOR N= 0
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  INTEGER :: I, K
  !----------------------------------------------------------------------
  DO I=1,K
    G(K,I,6)=0.0_RKIND
    G(I,K,6)=0.0_RKIND
    G(I,K,3)=0.0_RKIND
    G(K,I,4)=0.0_RKIND
    G(K,I,5)=0.0_RKIND
  END DO
  RETURN
END SUBROUTINE ZERO
!CC*DECK P2C2S09
SUBROUTINE WCON
  !        ---------------
  !
  ! 2.2.13  CONSTRUCT VACUUM MATRIX AND SOURCE VECTOR
  !         SEE REF.(1), EQ.(3.31); REF[4], EQS.(4.11-16).
  !         *** MODIFIED TO INCLUDE GENERALIZED JACOBIAN
  !         *** MODIFIED TO INCLUDE OPTION NLPHAS
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !-----------------------------------------------------------------------
  REAL(RKIND) :: ZAIM1, ZAIP1, ZAJM1, ZAJP1, ZA1, ZA2, ZA3, ZA4, ZARG, &
       & ZQI, ZQJ
  INTEGER :: IN2, I, INDEX, J, JINDEX, IP1, JP1
  COMPLEX &
       &   ZI,    ZBIM1,  ZBJM1,   ZBIP1,   ZBJP1,   ZSM1,    ZSP1, &
       &   ZAI,   ZAJ,    ZBI,     ZBJ,     ZEINQ2,  ZPHASI,  ZPHASJ, &
       &   ZCONA(2,2),    ZCONS(2)
  !-----------------------------------------------------------------------
  !
  ZI = CMPLX (0._RKIND,1._RKIND)
  IN2 = NPOL
  !
  !-----------------------------------------------------------------------
  !
  IF (.NOT.NLPHAS) THEN
    !
    !-----------------------------------------------------------------------
    !L                            LOOP ON I
    !
    DO I=1,IN2
      !
      !     FIND INDEX = I-1
      IF (I.EQ.1) THEN
        INDEX = IN2
      ELSE
        INDEX = I - 1
      END IF
      !
      !     AI = R**2 / (J * T * DELTA-CHI)
      ZAIM1 = R2J(INDEX) / (TB * DC(INDEX))
      ZAIP1 = R2J(I)     / (TB * DC(I)    )
      !
      !     BI = ( R**2/(J*T) * D/DCHI ( LOG(R**2/J) ) - ZI * WNTORO ) / 2.
      ZBIM1 = CMPLX (R2J(INDEX)*DCR2J(INDEX)/TB, -WNTORO) * 0.5_RKIND
      ZBIP1 = CMPLX (R2J(I)*DCR2J(I)/TB        , -WNTORO) * 0.5_RKIND
      !
      !-----------------------------------------------------------------------
      !L                           LOOP ON J
      !
      DO J=1,IN2
        !
        !     FIND JINDEX = J-1
        IF (J.EQ.1) THEN
          JINDEX = IN2
        ELSE
          JINDEX = J - 1
        END IF
        !
        !     AJ = R**2 / (J * T * DELTA-CHI)
        ZAJM1 = R2J(JINDEX) / (TB * DC(JINDEX))
        ZAJP1 = R2J(J)      / (TB * DC(J)    )
        !
        !     BJ =  ( R**2/(J*T) * D/DCHI ( LOG(R**2/J) ) + ZI * WNTORO ) / 2.
        ZBJM1 = CMPLX (R2J(JINDEX)*DCR2J(JINDEX)/TB, WNTORO) * 0.5_RKIND
        ZBJP1 = CMPLX (R2J(J)*DCR2J(J)/TB          , WNTORO) * 0.5_RKIND
        !
        !     Q-MATRIX ELEMENTS
        ZA1 = G(INDEX,JINDEX,1)
        ZA2 = G(INDEX,J     ,1)
        ZA3 = G(I    ,JINDEX,1)
        ZA4 = G(I    ,J     ,1)
        !
        !     VACUUM MATRIX WVAC is W_{ij} in Eq.(4.15) of Ref[4], modified for 
        !     general Jacobian
        WVAC(I,J) = ZA1 * (  ZAIM1 + ZBIM1 ) * (  ZAJM1 + ZBJM1 ) &
             &                + ZA2 * (  ZAIM1 + ZBIM1 ) * ( -ZAJP1 + ZBJP1 ) &
             &                + ZA3 * ( -ZAIP1 + ZBIP1 ) * (  ZAJM1 + ZBJM1 ) &
             &                + ZA4 * ( -ZAIP1 + ZBIP1 ) * ( -ZAJP1 + ZBJP1 )
        !
      END DO
      !                            END OF J-LOOP
      !---------------------------------------------------------------------
      !
      !     SOURCE(I) is b_i in Eq.(4.16) of Ref.[4], modified for general 
      !     Jacobian; 
      !     ANTR(.) is \Phi_{E}, see also Eq.(3.73) of Ref [4] and subroutine 
      !     QCON
      ZSM1 = ANTR (INDEX)
      ZSP1 = ANTR (I    )
      !
      SOURCE(I) = ZI * OMEGA * (  ZSM1 * ( ZAIM1 + ZBIM1 ) &
           &                             + ZSP1 * (-ZAIP1 + ZBIP1 ) )
      !
    END DO
    !                            END OF I-LOOP
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    !                WITH NLPHAS OPTION
    !
    !     WE MAKE A DOUBLE LOOP ON MESH INTERVALS (AND NOT ON MESH NODES
    !     AS ABOVE)
    !
  ELSE
    !
    !     INITIALIZE WVAC(I,J) AND SOURCE(I) TO (0.,0.)
    CALL CVZERO (NPOL, SOURCE(1),1)
    DO J = 1, NPOL
      CALL CVZERO (NPOL, WVAC(1,J),1)
    END DO
    ZARG = WNTORO * QB * C2PI
    ZEINQ2 = CMPLX(COS(ZARG),SIN(ZARG))
    !
    !---------------------------------------------------------------------
    !                            LOOP ON I
    DO I=1,NPOL
      !
      ZARG = - WNTORO * QB * (CHIOLD(I)+CHIOLD(I+1)) / 2.0_RKIND
      !
      IF (I.GT.NCHI) THEN
        ZARG = ZARG + WNTORO * QB * C2PI
      END IF
      !
      ZPHASI = CMPLX (COS(ZARG),SIN(ZARG))
      ZQI = TB / R2J(I)
      ZAI = ZPHASI / (ZQI*DC(I))
      ZBI = ZAI * DCR2J(I) * DC(I) / 2.0_RKIND
      IP1 = I + 1
      !
      !     PERIODICITY
      IF (I.EQ.NPOL) THEN
        IP1 = 1
      END IF
      !
      !     SOURCE VECTOR
      ZCONS(1) = ANTR(I) * CONJG (-ZAI+ZBI) * ZI * OMEGA
      ZCONS(2) = ANTR(I) * CONJG ( ZAI+ZBI) * ZI * OMEGA
      !
      !     JUMP CONDITION AT CHI=2PI
      IF (I.EQ.NCHI) THEN
        ZCONS(2) = ZCONS(2) * CONJG (ZEINQ2)
      END IF
      !
      !---------------------------------------------------------------------
      !                            LOOP ON J
      DO J=1,NPOL
        !
        ZARG = - WNTORO * QB * (CHIOLD(J)+CHIOLD(J+1)) / 2.0_RKIND
        !
        IF (J.GT.NCHI) THEN
          ZARG = ZARG + WNTORO * QB * C2PI
        END IF
        !
        ZPHASJ = CMPLX (COS(ZARG),SIN(ZARG))
        ZQJ = TB / R2J(J)
        ZAJ = ZPHASJ / (ZQJ*DC(J))
        ZBJ = ZAJ * DCR2J(J) * DC(J) / 2.0_RKIND
        JP1 = J + 1
        !
        !     PERIODICITY
        IF (J.EQ.NPOL) THEN
          JP1 = 1
        END IF
        !
        !     VACUUM MATRIX
        ZCONA(1,1) = G(I,J,1) * CONJG (-ZAI+ZBI) * (-ZAJ+ZBJ)
        ZCONA(1,2) = G(I,J,1) * CONJG (-ZAI+ZBI) * ( ZAJ+ZBJ)
        ZCONA(2,1) = G(I,J,1) * CONJG ( ZAI+ZBI) * (-ZAJ+ZBJ)
        ZCONA(2,2) = G(I,J,1) * CONJG ( ZAI+ZBI) * ( ZAJ+ZBJ)
        !
        !     JUMP CONDITION NEAR CHI=PI
        IF (I.EQ.NCHI) THEN
          ZCONA(2,1) = ZCONA(2,1) * CONJG (ZEINQ2)
          ZCONA(2,2) = ZCONA(2,2) * CONJG (ZEINQ2)
        END IF
        IF (J.EQ.NCHI) THEN
          ZCONA(1,2) = ZCONA(1,2) * ZEINQ2
          ZCONA(2,2) = ZCONA(2,2) * ZEINQ2
        END IF
        !
        !     ADD CONTRIBUTION TO WVAC
        WVAC(I  ,J  ) = WVAC(I  ,J  ) + ZCONA(1,1)
        WVAC(IP1,J  ) = WVAC(IP1,J  ) + ZCONA(2,1)
        WVAC(I  ,JP1) = WVAC(I  ,JP1) + ZCONA(1,2)
        WVAC(IP1,JP1) = WVAC(IP1,JP1) + ZCONA(2,2)
        !
      END DO
      !                            END OF J-LOOP
      !---------------------------------------------------------------------
      !
      !     ADD CONTRIBUTION TO SOURCE VECTOR
      SOURCE(I  ) = SOURCE(I  ) + ZCONS(1)
      SOURCE(IP1) = SOURCE(IP1) + ZCONS(2)
      !
    END DO
    !                            END OF J-LOOP
    !---------------------------------------------------------------------
    !
  END IF
  !
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  RETURN
END SUBROUTINE WCON
!CC*DECK P2C2S10
SUBROUTINE HERMIC
  !        -----------------
  !
  ! 2.2.14  CHECK NON-HERMICITY AND IMPOSE HERMICITY OF VACUUM MATRIX
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZS, ZT
  INTEGER :: I, J, IN2
  COMPLEX ZWVAC
  !---------------------------------------------------------------------
  !
  !     CHECK NON-HERMICITY OF WVAC AND
  !     HERMICIZE WVAC(I,J) = CONJG (WVAC(J,I))
  ZS = 0._RKIND
  ZT = 0._RKIND
  IN2 = NPOL
  DO J=1,IN2
    DO I=J,IN2
      ZWVAC = ( WVAC(I,J) + CONJG(WVAC(J,I)) ) * 0.5_RKIND
      ZS = ZS + (CABS ( WVAC(I,J) - CONJG(WVAC(J,I)) ) )**2
      ZT = ZT +  CABS ( WVAC(I,J) )**2
      WVAC(I,J) = ZWVAC
      WVAC(J,I) = CONJG (ZWVAC)
    END DO
  END DO
  !
  !     PRINT THE NON HERMICITY OF WVAC
  IF (NLOTP2(4)) THEN
    WRITE(NPRNT,9000) ZS, ZT, ZS/ZT
  END IF
9000 FORMAT (///,' ABS. NON-HERMICITY OF WVAC:',1PE15.6,/, &
       &               ' NORM SQUARED              :',1PE15.6,//, &
       &               ' REL. NON-HERMICITY OF WVAC:',1PE15.6,/, &
       &               ' **************************',///)
  RETURN
END SUBROUTINE HERMIC
!CC*DECK P2C2S11
SUBROUTINE MULT(A,B,N,ND)
  !
  ! 2.2.12  MULTIPLIES MATRICES
  !
  !     PRODUCT  OF  A  BY  B
  !        RESULT  STORE  IN  A
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) ::  A(ND,ND), B(ND,ND), XST(N), Z
  INTEGER :: I, J, K, N, ND
  !
  DO I=1,N
    DO J=1,N
      Z=0.0_RKIND
      DO K=1,N
        Z=Z+A(I,K)*B(K,J)
      END DO
      XST(J)=Z
    END DO
    DO K=1,N
      A(I,K)=XST(K)
    END DO
  END DO
  RETURN
END SUBROUTINE MULT
!CC*DECK P2C2S12
SUBROUTINE EKNREC(PETA,KN,PKN,PKPN)
  !        -----------------------------------
  !
  ! 2.2.7  CALCULATES KN AND DKN/PETA WITH RECURSION FORMULA
  !
  !
  !-----------------------------------------------------------------------
  !
  !   IT COMPUTES THE DEFINITE INTEGRAL BETWEEN 0 AND PI/2 OF THE
  !   FUNCTION COS(2*KN*U)/SQRT(1-(1-PETA)*SIN(U)**2) AS WELL AS ITS
  !   DERIVATIVE WITH RESPECT TO THE VARIABLE PETA
  !   PKN=VALUE OF THE FUNCTION
  !   PKPN= VALUE OF ITS DERIVATIVE
  !   RESTRICTION: 0<PETA<=1
  !
  !     FOR KN .GT. 1  A RECURSION FORMULA IS USED
  !-----------------------------------------------------------------------
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: Z(3),ZP(3), PETA, PK, PKN, PKPN, ZK2, ZJ, ZA, PE
  INTEGER :: KN, JSUP, J
  !
  ZK2=1/(1-PETA)
  CALL EK(PETA,PE,PK)
  Z(1)=PK
  ZP(1) = -.5_RKIND*(PK-PE/PETA)*ZK2
  IF (KN.LT.1) GO TO 10
  Z(2)=PK*(1-2*ZK2)+2*PE*ZK2
  ZP(2)=.5_RKIND*ZK2*((4*ZK2-1)*PK+(3-4*ZK2)*PE/PETA)
  IF (KN.LT.2) GO TO 20
  JSUP=KN-1
  DO J=1,JSUP
    ZJ = 2._RKIND/(2*J+1)
    ZA = 2._RKIND*J*ZJ*(1-2*ZK2)
    Z(3) = ZA*Z(2)+(ZJ-1)*Z(1)
    ZP(3) = ZA*ZP(2)+(ZJ-1)*ZP(1)+4*J*ZJ* Z(2)*ZK2**2
    Z(1) = Z(2)
    Z(2) = Z(3)
    ZP(1) = ZP(2)
    ZP(2) = ZP(3)
  END DO
  PKN = Z(3)
  PKPN = -ZP(3)
  GO TO 100
20 PKN=Z(2)
  PKPN = -ZP(2)
  GO TO 100
10 PKN=Z(1)
  PKPN = -ZP(1)
100 RETURN
END SUBROUTINE EKNREC
!CC*DECK P2C2S13
SUBROUTINE EK (PETA,PE,PK)
  !
  ! 2.2.8   COMPLETE ELLIPTICAL INTEGRALS
  !
  !
  !-----------------------------------------------------------------------
  !
  !   COMPLETE ELLIPTIC INTEGRALS
  !   1ST KIND: RESULT=PK
  !   2ND KIND: RESULT=PE
  !   ARGUMENT = SQRT(1-PETA)
  !   FROM C. HASTINGS IN 'APPROXIMATION FOR DIGITAL COMPUTERS' PRINCETON
  !   UNIV. PRESS, PRINCETON, N.J.
  !
  !-----------------------------------------------------------------------
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PETA, PE, PK, ZLNETA
  !
  ZLNETA=LOG(PETA)
  !   RESTRICTION: 0<PETA<=1
  PE = (((.01736506451_RKIND*PETA+.04757383546_RKIND)*PETA+ .0626060122_RKIND) &
       &   *PETA + .44325141463_RKIND)*PETA + 1._RKIND - ZLNETA*PETA*((( &
       &   .00526449639_RKIND*PETA + .04069697526_RKIND) * PETA + .09200180037_RKIND) &
       &   *PETA + .2499836831_RKIND)
  PK = (((0.01451196212_RKIND*PETA+0.03742563713_RKIND) * PETA+0.03590092383_RKIND) &
       &   * PETA+0.09666344259_RKIND) * PETA+1.38629436112_RKIND - ZLNETA * (((( &
       &   .00441787012_RKIND*PETA+.03328355346_RKIND) * PETA+.06880248576_RKIND) * PETA &
       &   +.12498593597_RKIND) * PETA+.5_RKIND)
  RETURN
END SUBROUTINE EK
!CC*DECK P2C2S14
SUBROUTINE IMGC(A,N,L,NUL)
  !     ---------------
  !
  ! 2.2.14. MATRIX INVERSION WITH SYMMETRIC DIAGONAL PRECONDITIONING
  !---------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: A(L,L), LL(L), ZDIAG(L), P, D
  INTEGER :: I, J, K, N, L, NUL, M
  !
  !---------------------------------------------------------------------
  !L          1. SYMMETRIC DIAGONAL PRECONDITIONING:
  !              CONSTRUCT ( (DIAG A)**-1/2 ) * A * ( (DIAG A)**-1/2 )
  !
  DO J=1,N
    !
    !     CONSTRUCT ZDIAG = (DIAG A) ** -1/2
    ZDIAG(J) = ABS (A(J,J))
    IF (ZDIAG(J).NE.0._RKIND) THEN
      ZDIAG(J) = 1._RKIND / SQRT (ZDIAG(J))
    ELSE
      ZDIAG(J) = 1._RKIND
    END IF
    !
    !     PRE AND POST MULTIPLY A WITH ZDIAG
    DO K=1,N
      A(J,K) = ZDIAG(J) * A(J,K)
      A(K,J) = A(K,J) * ZDIAG(J)
    END DO
    !
  END DO
  !
  !-------------------------------------------------------------
  !L          2. INVERT (ZDIAG*A*ZDIAG)
  !
  DO K=1,N
    !
    P=0._RKIND
    !
    !     FIND PIVOT
    !
    DO J=K,N
      D=ABS(A(J,K))
      IF (D.GE.P) THEN
        P=D
        M=J
      END IF
    END DO
    !
    !     TEST FOR NON ZERO PIVOT
    !
    IF (P .NE. 0._RKIND) GOTO 2
    NUL=0
    RETURN
    !
    !     STORE PIVOT NUMBER IN LL(K)
    !
2   P=A(M,K)
    LL(K)=M
    A(M,K)=A(K,K)
    A(K,K)=1._RKIND
    ! 
    DO I=1,N
      A(I,K)=A(I,K)/P
    END DO
    !
    DO J=1,N
      IF (K .NE. J) THEN
        D=A(M,J)
        A(M,J)=A(K,J)
        A(K,J)=0._RKIND
        IF (D .NE. 0._RKIND) THEN
          !
          DO I=1,N
            A(I,J)=A(I,J)-D*A(I,K)
          END DO
          !
        END IF
      END IF
    END DO
    !
  END DO
  !
  !
  NUL=1
  !
  !     UN - PIVOTING: RESORE INITIAL COLUMN ORDERING
  !
  DO J=N,1,-1
    IF (J.NE.LL(J)) THEN
      M = LL(J)
      DO K=1,N
        D = A(K,M)
        A(K,M) = A(K,J)
        A(K,J) = D
      END DO
    END IF
  END DO
  !
  !----------------------------------------------------------------------
  !L          3. PRE- AND POST-MULTIPLY WITH ZDIAG
  !
  DO J=1,N
    DO K=1,N
      A(J,K) = ZDIAG(J) * A(J,K)
      A(K,J) = A(K,J) * ZDIAG(J)
    END DO
  END DO
  !
  RETURN
END SUBROUTINE IMGC
!CC*DECK P2C2S15
SUBROUTINE CURENT
  !        -----------------
  !
  ! 2.2.15  JUMP OF POTENTIAL ACROSS ANTENNA DUE TO CURRENT
  !         SEE REF.(1), EQ.(3.22)
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZT(4), ZPOL, ZCPOL, ZFEED, ZCFEED, ZCLOSE, ZCCLOS, ZCONV, &
       & ZWIDTH, ZW, ZPOLWN, ZJSYM, ZJASY, ZDELTH, ZTHETM, ZARG, ZCOEF, ZSIGN, &
       & ZMAX, ZUP, ZCHI, ZTHETA, ZF, P, PL, PU
  INTEGER :: J, JA, ITH, ICH, JPL, IPOS
  COMPLEX :: ZI
  CHARACTER*1, DIMENSION(120) :: IPLOT
  !-----------------------------------------------------------------------
  !
  ZF (P, PL, PU) = SIN (0.5_RKIND*CPI*(P-PL)/(PU-PL)) **2
  !
  !=======================================================================
  !L           	INITIALIZE
  !
  ZI=CMPLX(0._RKIND,1._RKIND)
  !
  DO J=1,NPOL
    SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
  END DO
  !
  IF (NLOTP2(3)) THEN
    WRITE (NPRNT,900)
  END IF
  !
  !-----------------------------------------------------------------------
  !L              NANTYP=-1 : HELICAL ANTENNA CURRENTS IN THE PLASMA,
  !			    NO ANTENNA CURRENTS IN THE VACUUM REGION
  !
  IF (NANTYP.EQ.-1) THEN
    !
    IF (NLOTP2(3)) THEN
      WRITE (NPRNT,810) (MPOLWN(J),CURSYM(J),CURASY(J),J=1,MANCMP) &
           &			      ,SAMIN, SAMAX
    END IF
    !
    !
  END IF
  !

  !
  !-----------------------------------------------------------------------
  !L              NANTYP=1 : HELICAL ANTENNA
  !
  IF (NANTYP.EQ.1) THEN
    !
    IF (NLOTP2(3)) THEN
      WRITE (NPRNT,910) (MPOLWN(J),CURSYM(J),CURASY(J),J=1,MANCMP)
    END IF
    !
    !
  END IF
  !
  !------------------------------------------------------------------------
  !L              NANTYP=2 : LFS OR HFS ANTENNA
  !
  IF (NANTYP.EQ.2) THEN
    !
    !     LFS ANTENNA
    !
    IF (THANT(2).GT.THANT(3)) THEN
      !
      !     POLOIDAL EXTENT OF ANTENNA
      ZPOL = THANT(3)
      ZCPOL = THANT(2)
      !
      !     POSITION OF FEEDERS
      ZFEED = THANT(4)
      ZCFEED = THANT(1)
      !
      !     PRINTOUT
      IF (NLOTP2(3)) THEN
        WRITE (NPRNT,920)
        WRITE (NPRNT,923) ZPOL, ZCPOL, ZFEED, ZCFEED
      END IF
      !
      !     HFS ANTENNA
      !
    ELSE
      !
      ZPOL = THANT(2)
      ZCPOL = THANT(3)
      ZFEED = THANT(1)
      ZCFEED = THANT(4)
      IF (NLOTP2(3)) THEN
        WRITE (NPRNT,921)
        WRITE (NPRNT,923) ZPOL, ZCPOL, ZFEED, ZCFEED
      END IF
      !
    END IF
    !
    !     CURRENT CLOSE-UP
    ZCLOSE = ZFEED + ZFEED - ZPOL
    ZCCLOS = ZCFEED + ZCFEED - ZCPOL
    !
    !     ANGLES IN RADIANS
    ZCONV  = CPI/180._RKIND
    ZPOL   = ZCONV * ZPOL
    ZCPOL  = ZCONV * ZCPOL
    ZFEED  = ZCONV * ZFEED
    ZCFEED = ZCONV * ZCFEED
    ZCLOSE = ZCONV * ZCLOSE
    ZCCLOS = ZCONV * ZCCLOS
    !
  END IF
  !
  !--------------------------------------------------------------------
  !L                NANTYP=3 : TOP/BOTTOM
  !
  IF (NANTYP.EQ.3) THEN
    !
    IF (NLOTP2(3)) THEN
      WRITE (NPRNT,930)
    END IF
    !
  END IF
  !
  !--------------------------------------------------------------------
  !L		NANTYP=4 : SADDLE COIL ANTENNA
  !
  IF (NANTYP.EQ.4) THEN
    !
    !     POLOIDAL WIDTH (DEGREES) OF TOROIDAL CONDUCTOR
    ZWIDTH = THANTW
    !
    IF (NLOTP2(3)) THEN
      WRITE (NPRNT,940)
      IF (NSADDL.EQ.0) THEN
        WRITE (NPRNT,941) THANT(1), THANT(2)
      ELSE IF (NSADDL.EQ.1) THEN
        WRITE (NPRNT,942) (THANT(J), J=1,4)
      ELSE
        WRITE (NPRNT,943) (THANT(J), J=1,4)
      END IF
    END IF
    !
    !     ANGLES IN RADIANS
    ZCONV = CPI / 180.0_RKIND
    ZW = ZWIDTH * ZCONV
    DO J=1,4
      ZT(J) = THANT(J) * ZCONV
    END DO
    !
  END IF
  !
  !====================================================================
  !L                 CONSTRUCT SAUTR(J): ANTENNA CURRENT
  !
  !
  !     LOOP OVER POLOIDAL FOURIER COMPONENTS
  !
  DO JA=1,MANCMP
    !
    ZPOLWN=REAL(MPOLWN(JA))
    ZJSYM=CURSYM(JA)
    ZJASY=CURASY(JA)
    !CC      ZJSYM=-CURSYM(JA)/2.
    !CC      ZJASY=-CURASY(JA)/2.
    !
    !     LOOP OVER POLOIDAL INTERVALS
    !
    DO J=1,NPOL
      !
      ZDELTH=DELTH(J)
      ZTHETM=(TH(J+1)+TH(J))/2._RKIND
      !
      !-----------------------------------------------------------------------
      !L              NANTYP=-1 : HELICAL ANTENNA CURRENTS IN THE PLASMA,
      !			    NO ANTENNA CURRENTS IN THE VACUUM REGION
      !
      IF (NANTYP.EQ.-1) THEN
        SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
      END IF
      !
      !--------------------------------------------------------------------
      !L               NANTYP =1 : HELICAL OR NANTYP=3 : TOP/BOTTOM
      !
      IF ((NANTYP.EQ.1).OR.(NANTYP.EQ.3)) THEN
        !
        ZARG=ZPOLWN*ZDELTH/2._RKIND
        ZCOEF=1._RKIND
        IF(ABS(ZPOLWN).GT.0.1_RKIND) ZCOEF=SIN(ZARG)/ZARG
        !
        ZARG=ZPOLWN*ZTHETM
        SAUTR(J) = SAUTR(J) + ZCOEF*ZJSYM*COS(ZARG)
        SAUTR(J) = SAUTR(J) + ZCOEF*ZJASY*SIN(ZARG)*ZI
        !
      END IF
      !
      !--------------------------------------------------------------------
      !L          	NANTYP=2 : LFS OR HFS
      !
      IF (NANTYP.EQ.2) THEN
        !
        SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM
        !
        !     LFS
        !
        IF (THANT(2).GT.THANT(3)) THEN
          !
          !     CLOSE UPPER FEEDER CURRENT
          IF ((ZFEED.LT.ZTHETM).AND.(ZTHETM.LT.ZCLOSE)) THEN
            SAUTR(J) = SAUTR(J) * ( SIN ( (ZTHETM-ZCLOSE)*0.5_RKIND*CPI &
                 &		     / (ZFEED-ZCLOSE) ) )**2
          END IF
          !
          !     CLOSE LOWER FEEDER CURRENT
          IF ((ZCCLOS.LT.ZTHETM).AND.(ZTHETM.LT.ZCFEED)) THEN
            SAUTR(J) = SAUTR(J) * ( SIN ( (ZTHETM-ZCCLOS)*0.5_RKIND*CPI &
                 &		     / (ZCFEED-ZCCLOS) ) )**2
          END IF
          !
          !     ZERO CURRENT
          IF ((ZTHETM.GE.ZCLOSE).AND.(ZTHETM.LE.ZCCLOS)) THEN
            SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
          END IF
          !
          !     HFS
          !
        ELSE
          !
          !     CLOSE FIRST FEEDER CURRENT
          IF ((ZCLOSE.LT.ZTHETM).AND.(ZTHETM.LT.ZFEED)) THEN
            SAUTR(J) = SAUTR(J) * ( SIN ( (ZTHETM-ZCLOSE)*0.5_RKIND*CPI &
                 &		     / (ZFEED-ZCLOSE) ) )**2
          END IF
          !
          !     CLOSE SECOND FEEDER CURRENT
          IF ((ZCFEED.LT.ZTHETM).AND.(ZTHETM.LT.ZCCLOS)) THEN
            SAUTR(J) = SAUTR(J) * (SIN ( (ZTHETM-ZCCLOS)*0.5_RKIND*CPI &
                 &		     / (ZCFEED-ZCCLOS) ) )**2
          END IF
          !
          !     ZERO CURRENT
          IF ((ZTHETM.GE.ZCCLOS).OR.(ZTHETM.LE.ZCLOSE)) THEN
            SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
          END IF
          !
        END IF
        !
        !     SELECT MONO-/DI-POLE ANTENNA
        IF (NLDIP) THEN
          IF (J.GT.NCHI) SAUTR(J) = -SAUTR(J)
          IF ((J.EQ.NCHI).OR.(J.EQ.1)) SAUTR(J) =(0._RKIND,0._RKIND)
        END IF
        !
      END IF
      !
      !------------------------------------------------------------------------
      !L		NANTYP=4 : SADDLE COIL ANTENNA
      !
      IF (NANTYP.EQ.4) THEN
        !
        !     ONE SADDLE COIL ONLY
        !
        IF (NSADDL.EQ.0) THEN
          !
          IF ( (ZTHETM.GE.ZT(1)) .AND. (ZTHETM.LT.ZT(2)) ) THEN
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM
          ELSE IF ( (ZTHETM.LE.(ZT(1)-ZW)) .OR. (ZTHETM.GE.(ZT(2)+ZW)) ) &
               &		THEN
            SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
          ELSE IF (ZTHETM.LT.ZT(1)) THEN
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(1)-ZW,ZT(1))
          ELSE
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(2)+ZW,ZT(2))
          END IF
          !
        END IF
        !
        !     TWO SADDLE COILS PHASED (+-)
        !
        IF (NSADDL.EQ.1) THEN
          !
          IF (ZTHETM.GT.(ZT(2)+ZW)) THEN
            ZSIGN = -1.0_RKIND
          ELSE
            ZSIGN = 1.0_RKIND
          END IF
          !
          IF ( ( (ZTHETM.GE.ZT(1)) .AND. (ZTHETM.LT.ZT(2)) ) .OR. &
               &	      ( (ZTHETM.GE.ZT(3)) .AND. (ZTHETM.LT.ZT(4)) ) ) THEN
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZSIGN
          ELSE IF ( (ZTHETM.LE.(ZT(1)-ZW)) .OR. (ZTHETM.GE.(ZT(4)+ZW)) &
               &       .OR. ((ZTHETM.GE.(ZT(2)+ZW)) .AND.(ZTHETM.LE.(ZT(3)-ZW))) ) &
               &		THEN
            SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
          ELSE IF (ZTHETM.LT.ZT(1)) THEN
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(1)-ZW,ZT(1))
          ELSE IF (ZTHETM.LT.(ZT(2)+ZW)) THEN
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(2)+ZW,ZT(2))
          ELSE IF (ZTHETM.LT.ZT(3)) THEN
            SAUTR(J) = CMPLX(-1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(3)-ZW,ZT(3))
          ELSE
            SAUTR(J) = CMPLX(-1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(4)+ZW,ZT(4))
          END IF
          !
        END IF
        !
        !     TWO SADDLE COILS PHASED (++) (DEFAULT PRESETTED CASE)
        !
        IF (NSADDL.EQ.2) THEN
          !
          IF ( ( (ZTHETM.GE.ZT(1)) .AND. (ZTHETM.LT.ZT(2)) ) .OR. &
               &	      ( (ZTHETM.GE.ZT(3)) .AND. (ZTHETM.LT.ZT(4)) ) ) THEN
            !
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM
            !
          ELSE IF ( (ZTHETM.LE.(ZT(1)-ZW)) .OR. (ZTHETM.GE.(ZT(4)+ZW)) &
               &       .OR. ((ZTHETM.GE.(ZT(2)+ZW)) .AND.(ZTHETM.LE.(ZT(3)-ZW))) ) &
               &		THEN
            !
            SAUTR(J) = CMPLX(0._RKIND,0._RKIND)
            !
          ELSE IF (ZTHETM.LT.ZT(1)) THEN
            !
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(1)-ZW,ZT(1))
            !
          ELSE IF (ZTHETM.LT.(ZT(2)+ZW)) THEN
            !
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(2)+ZW,ZT(2))
            !
          ELSE IF (ZTHETM.LT.ZT(3)) THEN
            !
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(3)-ZW,ZT(3))
            !
          ELSE
            !
            SAUTR(J) = CMPLX(1._RKIND,0._RKIND) * ZJSYM * ZF(ZTHETM,ZT(4)+ZW,ZT(4))
            !
          END IF
          !
          !     ENDIF CASE NSADDL=2
        END IF
        !
        !     ENDIF CASE NANTYP=4
      END IF
      !
      !------------------------------------------------------------------------
      !
    END DO
  END DO
  !
  !========================================================================
  !L                 OUTPUT
  !
  IF(NLOTP2(3)) THEN

    !     PRINT ANTENNA CURRENT POTENTIAL
    !
    WRITE (NPRNT,9000) (SAUTR(J),TH(J),TH(J+1),J=1,NPOL)
    !
    !     LINE PRINTER PLOT OF ANTENNA CURRENT POTENTIAL
    !
    WRITE (NPRNT,9010)
    ZMAX=0._RKIND
    !
    DO J=1,NPOL
      ZMAX = MAX(ZMAX,ABS(AIMAG(SAUTR(J))),ABS(REAL(SAUTR(J))))
    END DO
    !
    ITH=1
    ICH=1
    !
    DO J=1,128
      !
      ZUP=REAL(J,RKIND)-0.5_RKIND
      DO JPL=1,120
        IPLOT(JPL)=' '
      END DO
      IPLOT(26)='0'
      IPLOT(95)='0'
      !
130   CONTINUE
      !
      IF(ICH.GT.NPOL) GO TO 140
      ZCHI=(CHI(ICH+1)+CHI(ICH))*10._RKIND
      IF(ZCHI.GT.ZUP) GO TO 140
      IPOS = 26.95_RKIND + AIMAG(SAUTR(ICH)) * 25._RKIND / ZMAX
      IPLOT(IPOS) = 'I'
      IPOS = 26.95_RKIND +  REAL(SAUTR(ICH)) * 25._RKIND / ZMAX
      IPLOT(IPOS) = 'R'
      ICH=ICH+1
      GO TO 130
      !
140   CONTINUE
      !
      IF(ITH.GT.NPOL) GO TO 150
      ZTHETA=T(ITH)*20._RKIND
      IF(ZTHETA.GT.ZUP) GO TO 150
      IPOS = 95.95_RKIND + AIMAG(SAUTR(ITH)) * 25._RKIND / ZMAX
      IPLOT(IPOS) = 'I'
      IPOS = 95.95_RKIND +  REAL(SAUTR(ITH)) * 25._RKIND / ZMAX
      IPLOT(IPOS) = 'R'
      ITH=ITH+1
      GO TO 140
      !
150   CONTINUE
      !
      WRITE (NPRNT,9020) IPLOT
      !
    END DO
    !
  END IF
  !
  !========================================================================
  !
900 FORMAT(////,' 2.3 OUTPUT FROM CURENT',/, &
       &		     ' ----------------------',/)
810 FORMAT(//,' HELICAL ANTENNA IN THE PLASMA',/, &
       &		   ' -----------------------------',//, &
       &          4X,'ANTENNA PARAMETERS:',3X,'POLOIDAL', &
       &		5X,'CURRENT',5X,'CURRENT',/, &
       &          26X,'WAVE NR.',4X,'COS(N*PHI)',2X,'SIN(N*PHI)',//, &
       &          (27X,I3,6X,1P2E12.3),//, &
       &	        ' BETWEEN SMIN AND SMAX =',1P2E12.3)
910 FORMAT(//,' HELICAL ANTENNA',/,' ---------------',//, &
       &          4X,'ANTENNA PARAMETERS:',3X,'POLOIDAL', &
       &		5X,'CURRENT',5X,'CURRENT',/, &
       &          26X,'WAVE NR.',4X,'COS(N*PHI)',2X,'SIN(N*PHI)',//, &
       &          (27X,I3,6X,1P2E12.3))
920 FORMAT (///,' LFS ANTENNA',/,1X,11('-'),//)
921 FORMAT (///,' HFS ANFENNA',/,1X,11('-'),//)
923 FORMAT (///,' POLOIDAL EXTENT OF ANTENNA :',1P2E14.3,//, &
       &                  ' POSITION OF FEEDERS :',1P2E14.3,//)
930 FORMAT (///,' TOP/BOTTOM ANTENNA',/,1X,18('-'),//)
940 FORMAT (///,' SADDLE COIL ANTENNA',/,1X,19('-'),//)
941 FORMAT (/,' ONE SADDLE COIL POLOIDALLY ONLY.',/, &
       &	 ' BETWEEN THETA =',1PE12.3,' AND THETA =',1PE12.3,//)
942 FORMAT (/,' TWO SADDLE COILS POLOIDALLY WITH (+-) PHASING',/, &
       &	 ' DOMINANT COMPONENT: M=1',/, &
       &	 ' BETWEEN THETA =',1PE12.3,' AND THETA =',1PE12.3,/, &
       &	 ' BETWEEN THETA =',1PE12.3,' AND THETA =',1PE12.3,//)
943 FORMAT (/,' TWO SADDLE COILS POLOIDALLY WITH (++) PHASING',/, &
       &	 ' DOMINANT COMPONENT: M=2',/, &
       &	 ' BETWEEN THETA =',1PE12.3,' AND THETA =',1PE12.3,/, &
       &	 ' BETWEEN THETA =',1PE12.3,' AND THETA =',1PE12.3,//)
9000 FORMAT(///,6X,'RE(SAUTR)',6X,'IM(SAUTR)',8X,'TH(J)',8X, &
       &   'TH(J+1)',//(1P4E15.3))
9010 FORMAT(///17X,'CURRENT VS. CHI',54X,'CURRENT VS. THETA',//)
9020 FORMAT(1X,120A1)
  !
  RETURN
END SUBROUTINE CURENT
!CC*DECK P2C2S16
SUBROUTINE MATVEC (PMATRX,PIN,POUT,KLONG,KDIM)
  !        ----------------------------------------------
  !
  !  2.2.16. MULTIPLIES A REAL MATRIX WITH A COMPLEX VECTOR
  !         RESULTING VECTOR IS POUT
  !
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PMATRX(KDIM,KDIM)
  INTEGER :: KLONG, KDIM, J1, J2
  COMPLEX   PIN(KDIM),  POUT(KDIM),  Z
  !-----------------------------------------------------------------------
  !
  DO J1=1,KLONG
    Z = CMPLX(0._RKIND,0._RKIND)
    DO J2=1,KLONG
      Z=Z+PMATRX(J1,J2)*PIN(J2)
    END DO
    POUT(J1)=Z
  END DO
  !
  RETURN
END SUBROUTINE MATVEC
!CC*DECK P2C2S18
SUBROUTINE MOPPOW (K)
  !        ---------------------
  !
  ! 2.2.18  MATRIX OPERATIONS FOR POWER AT ANTENNA
  !         SEE REF.(1), EQ.(3.35); REF[4], EQ.(3.87).
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZTRA
  INTEGER :: K, J, IN2, IN, INV, I, NUL
  COMPLEX   Z
  !-----------------------------------------------------------------------
  IN2=2*NCHI-2
  IN=IFIX(ABS(WNTORO))
  INV=IN2
  IF(IN.EQ.0) INV=IN2-1
  !
  ! *********  CASE  N = 0  HAS NOT YET BEEN TESTED. VERY POSSIBLY WRONG.
  !
  !
  GO TO (100,200,300), K
  !
  !-----------------------------------------------------------------------
  !L              1.        DETERMINE MATRICES U, V AND T
  !
  !     SAVE G-2 ON DISK (Dpa-2)
100 CONTINUE
  DO J=1,IN2
    G(J,J,7)=G(J,J,7)-2._RKIND
  END DO
  REWIND NSOURC
  WRITE(NSOURC) G(:,:,7)
  !
  !      (G-2)*L**(-1)
  CALL IMGC(G(:,:,10),INV,NDIM,NUL)
  CALL MULT(G(:,:,7),G(:,:,10),IN2,NDIM)
  !
  WRITE(NSOURC) G(:,:,7)
  !
  !     CONSTRUCT U = A2D-2-(G-2)*L**(-1)*K
  CALL MULT(G(:,:,7),G(:,:,9),IN2,NDIM)
  DO J=1,IN2
    DO I=1,IN2
      G(I,J,9)=G(I,J,1)-G(I,J,7)
    END DO
    G(J,J,9)=G(J,J,9)-2._RKIND
  END DO
  !
  !     CONSTRUCT V = B-(G-2)*L**(-1)*M
  REWIND NSOURC
  READ(NSOURC)G(:,:,7)
  READ(NSOURC) G(:,:,7)
  CALL MULT(G(:,:,7),G(:,:,11),IN2,NDIM)
  DO J=1,IN2
    DO I=1,IN2
      G(I,J,11)=G(I,J,2)-G(I,J,7)
    END DO
  END DO
  !
  !     CONSTRUCT T = H-(G-2)*L**(-1)*N
  REWIND NSOURC
  READ(NSOURC) G(:,:,7)
  READ(NSOURC) G(:,:,7)
  CALL MULT(G(:,:,7),G(:,:,12),IN2,NDIM)
  DO J=1,IN2
    DO I=1,IN2
      G(I,J,12)=G(I,J,8)-G(I,J,7)
    END DO
  END DO
  !
  !     RESTORE PREVIOUS VALUE OF  G-2
  REWIND NSOURC
  READ(NSOURC) G(:,:,7)
  !
  RETURN
  !
  !-----------------------------------------------------------------------
  !L              2.        OHM-VECTOR AND REACTANCE SCALAR
  !
200 CONTINUE
  !     T**(-1)
  CALL IMGC(G(:,:,12),INV,NDIM,NUL)
  !
  !     REACTANCE SCALAR
  CALL MATVEC(G(:,:,9),ANTR,SAUTX,IN2,NDIM)
  CALL MATVEC(G(:,:,12),SAUTX,OHMR,IN2,NDIM)
  REASCR = CMPLX (0._RKIND,0._RKIND)
  DO J=1,IN2
    REASCR = REASCR + (TH(J+1)-TH(J)) * SAUTR(J) * CONJG(OHMR(J))
  END DO
  !
  !     OHM-VECTORS
  !     READ Q INTO C
  READ(NSOURC) G(:,:,3)
  !     CALCULATE T**(-1)*(V-U*Q)
  CALL MULT(G(:,:,9),G(:,:,3),IN2,NDIM)
  DO J=1,IN2
    DO I=1,IN2
      G(I,J,11)=G(I,J,11)-G(I,J,9)
    END DO
  END DO
  CALL MULT(G(:,:,12),G(:,:,11),IN2,NDIM)
  !     COMPLEX OHM-VECTOR
  DO J=1,IN2
    Z = CMPLX(0._RKIND,0._RKIND)
    DO I=1,IN2
      Z=Z+(TH(I+1)-TH(I))*SAUTR(I)*G(I,J,12)
    END DO
    OHMR(J)=Z
  END DO
  !
300 CONTINUE
  !
  !***********************************************************************
  !     NORMALIZE THE ANTENNA VECTOR
  DO J=1,IN2
    ZTRA = TB*DC(J)
    ANTR(J) = ANTR(J)*ZTRA
  END DO
  !***********************************************************************
  RETURN
END SUBROUTINE MOPPOW
!CC*DECK P2C2S19
SUBROUTINE EKN(PETA,KN,PKN,PKPN)
  !        --------------------------------
  !
  ! 2.2.19  ELLIPTIC INTEGRAL KN AND DKN/PETA
  !
  !
  !-----------------------------------------------------------------------
  !     DEFINITE INTEGRAL BETWEEN 0 AND PI/2 OF
  !     COS(2*KN*U)/SQRT(1-(1-PETA)*SIN(U)**2) AND DERIVATIVE WITH
  !     RESPECT TO PETA.
  !     PKN = VALUE OF THE FUNCTION
  !     PKPN= VALUE OF ITS DERIVATIVE
  !-----------------------------------------------------------------------
  !     DEPENDING ON THE PARAMETERS KN AND PETA THREE DIFFERENT
  !     METHODS ARE USED.
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZETLIM(10), PETA, PKN, PKPN
  INTEGER :: KN
  DATA ZETLIM/.95_RKIND,.9,.75_RKIND,.5,.35_RKIND,.25,.2_RKIND,.15,.12_RKIND,.1/
  !-----------------------------------------------------------------------
  IF(KN.GT.10) GO TO 200
  !-----------------------------------------------------------------------
  !L              1.        KN SMALLER THAN 11
  !
  !     CHOOSE BETWEEN RECURSION FORMULA AND SERIES
  IF(KN.EQ.0) GO TO 100
  IF(PETA.GT.ZETLIM(KN)) GO TO 110
  !
  !     RECURSION FORMULA
100 CONTINUE
  CALL EKNREC(PETA,KN,PKN,PKPN)
  RETURN
  !
  !     SERIES
110 CONTINUE
  CALL EKNSIE(PETA,KN,PKN,PKPN)
  RETURN
  !-----------------------------------------------------------------------
  !L              2.        KN GREATER THAN 10
  !
200 CONTINUE
  !
  !     BESSEL APPROXIMATION
  CALL EKNLIM(PETA,KN,PKN,PKPN)
  RETURN
  !
END SUBROUTINE EKN
!CC*DECK P2C2S20
SUBROUTINE EKNSIE(PETA,KN,PKN,PKPN)
  !        -----------------------------------
  ! 2.2.20  CALCULATES KN AND DKN/PETA WITH SERIES
  !
  !-----------------------------------------------------------------------
  !     DEFINITE INTEGRAL BETWEEN 0 AND PI/2 OF
  !     COS(2*KN*U)/SQRT(1-(1-PETA)*SIN(U)**2) AND DERIVATIVE WITH
  !     RESPECT TO PETA.
  !     PKN = VALUE OF THE FUNCTION
  !     PKPN= VALUE OF ITS DERIVATIVE
  !-----------------------------------------------------------------------
  !     EVALUATION WITH INFINITE SUM GIVEN BY
  !     P.F. BYRD AND M.D. FRIEDMAN, HANDBOOK OF ELLIPTIC INTEGRALS FOR
  !     ENGINEERS AND SCIENTISTS, SPRINGER 1971, FORMULA 806.01
  !-----------------------------------------------------------------------
  !     WE SUBSTRACT FROM THE SUMS THE LEADING TERMS IN ORDER TO
  !     IMPROVE SLIGHTLY THE CONVERGENCE
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZPI, PETA, PKN, PKPN, ZPIHAF, ZPINV, ZN, ZALPHA, Z, ZCOEF, &
       & ZARG, ZPOWER, ZPART1, ZPART2, ZADD, ZADD1, ZADD2, ZTRA
  INTEGER :: IMESS, ILIM, KN, IN, INM1, J
  !
  DATA ZPI/3.141592653589793_RKIND/
  DATA IMESS,ILIM/0,100/
  !-----------------------------------------------------------------------
  !
  IF(ABS(PETA).LT.1._RKIND) GO TO 200
  !
  !L              1.        FIRST CALL FOR ORDER KN
  !
  ZPIHAF=ZPI/2._RKIND
  ZPINV=1._RKIND/ZPI
  IN=KN
  ZN=REAL(KN,RKIND)
  ZALPHA=1._RKIND
  DO J=2,KN
    Z=REAL(J,RKIND)
    ZALPHA=ZALPHA*(2._RKIND*Z-3._RKIND)/(Z-1._RKIND)/8._RKIND
  END DO
  ZALPHA=ZALPHA*(2._RKIND*ZN-1._RKIND)/8._RKIND/ZN
  RETURN
  !
  !L              2.        SUBSEQUENT CALLS FOR ORDER KN
  !
200 CONTINUE
  IF(KN.EQ.IN) GO TO 210
  !
  PRINT 1000
1000 FORMAT(///,' EKNLIM ERROR')
  STOP
  !
210 CONTINUE
  !     FIRST TERMS IN THE SUMS
  ZCOEF=ZALPHA
  ZARG=1._RKIND-PETA
  ZPOWER=ZARG**KN
  PKN=(ZCOEF-ZPINV/ZN)*ZPOWER
  PKPN=-PKN*ZN/ZARG
  !
  !     SUMS FROM 1 TO (N-1) OF LEADING TERMS
  INM1=KN-1
  ZPART1=0._RKIND
  ZPART2=0._RKIND
  ZADD1=1._RKIND
  ZADD2=1._RKIND/ZARG
  DO J=1,INM1
    Z=REAL(J,RKIND)
    ZADD1=ZADD1*ZARG
    ZADD2=ZADD2*ZARG
    ZPART1=ZPART1+ZADD1/Z
    ZPART2=ZPART2+ZADD2
  END DO
  ZPART1=-ZPART1/2._RKIND
  ZPART2=ZPART2/2._RKIND
  !
  !     EVALUATE SERIES
  J=KN
220 CONTINUE
  J=J+1
  Z=REAL(J,RKIND)
  ZTRA=2._RKIND*Z-1._RKIND
  ZCOEF=ZCOEF*ZTRA*ZTRA/4._RKIND/(Z-ZN)/(Z+ZN)
  PKPN=PKPN-(ZCOEF*Z-ZPINV)*ZPOWER
  ZPOWER=ZPOWER*ZARG
  ZADD=(ZCOEF-ZPINV/Z)*ZPOWER
  PKN=PKN+ZADD
  IF(ABS(ZADD) .LT. ABS(PKN)/1.E+09_RKIND) GO TO 230
  GO TO 220
230 CONTINUE
  !
  !     ADD LEADING TERMS
  PKN=ZPIHAF*PKN-0.5_RKIND*LOG(PETA)+ZPART1
  PKPN=ZPIHAF*PKPN-0.5_RKIND/PETA+ZPART2
  !
  !     FACTOR (-1) ** N
  IF (MOD(KN,2).EQ.1) THEN
    PKN = - PKN
    PKPN= - PKPN
  END IF
  !
  !        MESSAGE IF A LOT OF TERMS ARE USED IN THE SUM
  IF(J.LT.ILIM.OR.IMESS.GT.8) RETURN
  IMESS=IMESS+1
  ILIM=ILIM*3
  PRINT 1010,J
1010 FORMAT(///,' EKNSIE IS VERY TIME CONSUMING',/, &
       &         ' IT USES ',I10,' TERMS IN THE SUM',//)
  !
  RETURN
END SUBROUTINE EKNSIE
!CC*DECK P2C2S21
SUBROUTINE EKNLIM(PETA,KN,PKN,PKPN)
  !        -----------------------------------
  !
  ! 2.2.21  CALCULATES KN AND DKN/PETA FOR HIGH N AND SMALL ETA
  !
  !-----------------------------------------------------------------------
  !     DEFINITE INTEGRAL BETWEEN 0 AND PI/2 OF
  !     COS(2*KN*U)/SQRT(1-(1-PETA)*SIN(U)**2) AND DERIVATIVE WITH
  !     RESPECT TO PETA.
  !     PKN = VALUE OF THE FUNCTION
  !     PKPN= VALUE OF ITS DERIVATIVE
  !-----------------------------------------------------------------------
  !     THE INTEGRAL AND ITS DERIVATIVE ARE EXPRESSED IN TERMS OF
  !     THE MODIFIED BESSEL FUNCTION K OF ORDER 0 AND 1 RESP.
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PETA, PKN, PKPN, ZSQRT, ZN, ZARG, BESMDK
  INTEGER :: KN
  !
  ZSQRT=SQRT(PETA)
  ZN=REAL(KN,RKIND)
  ZARG=2._RKIND*ZN*ZSQRT
  PKN=BESMDK(0,ZARG)
  PKPN=-ZN*BESMDK(1,ZARG)/ZSQRT
  !
  !     FACTOR (-1) ** N
  IF (MOD(KN,2).EQ.1) THEN
    PKN = - PKN
    PKPN= - PKPN
  END IF
  RETURN
END SUBROUTINE EKNLIM
!CC*DECK P2C2S22
FUNCTION BESMDI(NORDN,XX)
  !        -------------------------
  !
  ! 2.2.22
  !
  !     MODIFIED BESSEL FUNCTION, I, OF ORDER 0 AND 1
  !     ACCORDING TO ABRAMOWITZ-STEGUN  9.8.1 - 9.8.4
  !-----------------------------------------------------------------------
  USE PREC_CONST
  INTEGER :: I, J, NORDN
  REAL(RKIND) :: A(7),     B(9),     C(7),     D(9), ZABS, ZSIGN, T, TT, Z, BESMDI, XX
  !-----------------------------------------------------------------------
  DATA &
       &  A/1._RKIND,3.5156229_RKIND,3.0899424_RKIND,1.2067492_RKIND,.2659732,.0360768_RKIND,.0045813/, &
       &  B/.39894228_RKIND,.01328592,.00225319_RKIND,-.00157565_RKIND,.00916281,-.02057706_RKIND, &
       &    .02635537_RKIND,-.01647633_RKIND,.00392377/, &
       &  C/.5_RKIND,.87890594,.51498869_RKIND,.15084934,.02658733_RKIND,.00301532,.00032411_RKIND &
       &/,D/.39894228_RKIND,-.03988024_RKIND,-.00362018_RKIND,.00163801,-.01031555_RKIND, &
       &    .02282967_RKIND,-.02895312_RKIND,.01787654,-.00420059_RKIND/
  !-----------------------------------------------------------------------
  ZABS=ABS(XX)
  ZSIGN=SIGN(1._RKIND,XX)
  T=ZABS/3.75_RKIND
  TT=T*T
  IF(NORDN.EQ.1) GO TO 210
  !-----------------------------------------------------------------------
  !L              1.        I - SUB - 0
  !
  IF(T.GT.1._RKIND) GO TO 120
  !
  Z=A(7)
  DO J=1,6
    I=7-J
    Z=TT*Z+A(I)
  END DO
  BESMDI=Z
  RETURN
  !
  !
120 CONTINUE
  Z=B(9)
  DO J=1,8
    I=9-J
    Z=Z/T+B(I)
  END DO
  BESMDI=Z*EXP(ZABS)/SQRT(ZABS)
  RETURN
  !
  !-----------------------------------------------------------------------
  !L              2.        I - SUB - 1
  !
210 CONTINUE
  IF(T.GT.1) GO TO 230
  Z=C(7)
  DO J=1,6
    I=7-J
    Z=TT*Z+C(I)
  END DO
  BESMDI=Z*XX
  RETURN
  !
  !
230 CONTINUE
  Z=D(9)
  DO J=1,8
    I=9-J
    Z=Z/T+D(I)
  END DO
  BESMDI=ZSIGN*Z*EXP(ZABS)/SQRT(ZABS)
  !-----------------------------------------------------------------------
  !
  RETURN
END FUNCTION BESMDI
!CC*DECK P2C2S23
FUNCTION BESMDK(NORDN,XX)
  !        -------------------------
  !
  ! 2.2.23
  !
  !     MODIFIED BESSEL FUNCTION, K, OF ORDER 0 AND 1
  !     ACCORDING TO ABRAMOWITZ-STEGUN, 9.8.5 - 9.8.8
  !
  !     ****   NEEDS :BESMDI:
  !
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) ::  A(7),     B(7),     C(7),     D(7), XX, BESMDK, ZABS, &
       & ZSIGN, T, TT, Z, BESMDI
  INTEGER :: NORDN, I, J
  !-----------------------------------------------------------------------
  DATA &
       &  A/-.57721566_RKIND,.42278420,.23069756_RKIND,.0348859,.00262698_RKIND,.00010750, &
       &    .00000740_RKIND/, &
       &  B/1.25331414_RKIND,-.07832358_RKIND,.02189568,-.01062446_RKIND,.00587872,-.0025154_RKIND &
       &   ,.00053208_RKIND/, &
       &  C/1._RKIND,.15443144_RKIND,-.67278579_RKIND,-.18156897_RKIND,-.01919402_RKIND,-.00110404_RKIND, &
       &    -.00004686_RKIND/, &
       &  D/1.25331414_RKIND,.23498619_RKIND,-.03655620_RKIND,.01504268,-.00780353_RKIND,.00325614 &
       &   ,-.00068245_RKIND/
  !-----------------------------------------------------------------------
  ZABS=ABS(XX)
  ZSIGN=SIGN(1._RKIND,XX)
  T=ZABS/2._RKIND
  TT=T*T
  IF(NORDN.EQ.1) GO TO 210
  !
  !-----------------------------------------------------------------------
  !L              1.        K - SUB - 0
  !
  IF(T.GT.1._RKIND) GO TO 120
  !
  Z=A(7)
  DO J=1,6
    I=7-J
    Z=TT*Z+A(I)
  END DO
  BESMDK=-LOG(T)*BESMDI(0,ZABS)+Z
  RETURN
  !
  !
120 CONTINUE
  Z=B(7)
  DO J=1,6
    I=7-J
    Z=Z/T+B(I)
  END DO
  BESMDK=Z*EXP(-ZABS)/SQRT(ZABS)
  RETURN
  !
  !-----------------------------------------------------------------------
  !L              2.        K - SUB - 1
  !
210 CONTINUE
  IF(T.GT.1._RKIND) GO TO 230
  Z=C(7)
  DO J=1,6
    I=7-J
    Z=TT*Z+C(I)
  END DO
  BESMDK=ZSIGN*(LOG(T)*BESMDI(1,ZABS)+Z/ZABS)
  RETURN
  !
  !
230 CONTINUE
  Z=D(7)
  DO J=1,6
    I=7-J
    Z=Z/T+D(I)
  END DO
  BESMDK=ZSIGN*Z/EXP(ZABS)/SQRT(ZABS)
  !-----------------------------------------------------------------------
  !
  RETURN
END FUNCTION BESMDK
!CC*DECK P2C2S24
SUBROUTINE NUM (K,KN,KCHI)
  !        --------------
  !
  !  2.2.24.  MONOTONIC CHI NUMBERING
  !-------------------------------------------------------------------
  !
  IMPLICIT NONE
  !
  INTEGER :: K, KN, KCHI, IN2
  !
  IN2 = 2*KCHI-2
  KN = K
  IF (K.GT.IN2) KN = KN - IN2
  RETURN
  IF (K.GT.KCHI) GO TO 100
  KN = (K-1)*2
  IF (K.EQ.1) KN=1
  RETURN
  !
100 CONTINUE
  KN = 4*KCHI - 2*K - 1
  RETURN
END SUBROUTINE NUM
!CC*DECK P2C2S25
SUBROUTINE ANTEN (PT,PROPL,PDROPL,PA,PB,PC,PROAN,PDROAN)
  !        ----------------
  !
  !  2.2.25. DEFINES POSITION OF ANTENNA AT ANGLE THETA=PT
  !
  !     IN:
  !          PT = THETA (DEFINED FROM 0 TO 2*PI)
  !          PROPL = RHO AT THETA=PT OF PLASMA SURFACE
  !          PDROPL = DRHO/DTHETA AT THETA=PT OF PLASMA SURFACE
  !          PA,PC = DISTANCE (PL.,WALL) AND (PL.,ANTENNA) IN UNITS OF
  !          PB = GEOMETRICAL MINOR RADIUS (RMAX-RMIN)/2
  !
  !     OUT:
  !          PROAN = RHO(THETA=PT) OF ANTENNA SURFACE
  !          PDROAN = DRHO/DTHETA (THETA=PT) OF ANTENNA SURFACE
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  REAL(RKIND) ::PT, PROPL, PDROPL, PA, PB, PC, PROAN, PDROAN, ZTRA, ZDZDT, &
       & ZANTSH, ZPOL, ZCPOL, ZFEED, ZCFEED, ZARG, ZTETDI, &
       & ZA1, ZF1, ZA2, ZF2, ZA3, ZF3, ZA4, ZF4
  !
  !********************************************************************
  !     PROAN = PROPL + ZTRA * PB, WHERE ZTRA CAN BE FUNCTION OF THETA
  !                                IF THERE ARE FEEDERS.
  !     PDROAN = PDROPL + ZDZDT * PB, WHERE ZDZDT = D (ZTRA)/D (THETA).
  !********************************************************************
  !L              HELICAL OR SADDLE COIL ANTENNA
  !
  IF ( (NANTYP.EQ.-1).OR.(NANTYP.EQ.1).OR.(NANTYP.EQ.4) ) THEN
    ZTRA = PC
    ZDZDT = 0._RKIND
  END IF
  !
  !********************************************************************
  !L              LFS OR HFS ANTENNA
  !
  IF (NANTYP.EQ.2) THEN
    ZTRA = CPI/180._RKIND
    !
    !     POSITION OF ANTENNA CLOSE TO WALL
    ZANTSH = PA - 0.001_RKIND
    !
    !---------------------------------------------------------------------
    !L          LOW FIELD SIDE ANTENNA
    !
    IF (THANT(2).GT.THANT(3)) THEN
      !
      !     POLOIDAL POSITION OF ANTENNA CLOSE TO PLASMA SURFACE
      ZPOL = THANT(3) * ZTRA
      ZCPOL = THANT(2) * ZTRA
      !
      !     POLOIDAL POSITION OF FEEDERS CLOSE TO WALL
      ZFEED = THANT(4) * ZTRA
      ZCFEED = THANT(1) * ZTRA
      !
      !     ANTENNA CLOSE TO PLASMA SURFACE
      IF ((PT.LE.ZPOL).OR.(PT.GE.ZCPOL)) THEN
        ZTRA = PC
        ZDZDT = 0._RKIND
        !
        !     ANTENNA CLOSE TO WALL
      ELSE IF ((PT.GE.ZFEED).AND.(PT.LE.ZCFEED)) THEN
        ZTRA = ZANTSH
        ZDZDT = 0._RKIND
        !
        !     UPPER FEEDER
      ELSE IF ((PT.GT.ZPOL).AND.(PT.LT.ZFEED)) THEN
        ZARG = 0.5_RKIND*CPI * (PT-ZPOL) / (ZFEED-ZPOL)
        ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
        ZDZDT = (ZANTSH-PC) * SIN(ZARG) * COS(ZARG) *CPI / &
             &              (ZFEED-ZPOL)
        !
        !     LOWER FEEDER
      ELSE IF ((PT.GT.ZCFEED).AND.(PT.LT.ZCPOL)) THEN
        ZARG = 0.5_RKIND*CPI * (PT-ZCPOL) / (ZCFEED-ZCPOL)
        ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
        ZDZDT = (ZANTSH-PC) * SIN(ZARG) * COS(ZARG) *CPI / &
             &              (ZCFEED-ZCPOL)
      ELSE
        WRITE(*,*) ' MESSAGE FROM SUBROUTINE ANTEN:'
        WRITE(*,*) ' CHECK ANTENNA SURFACE DEFINITION !!!'
      END IF
      !
      !     POSITION OF DIPOLE ANTENNA
      ZTETDI = (ZPOL + ZCPOL)*0.5_RKIND - CPI
      !
      !--------------------------------------------------------------------
      !L          HIGH FIELD SIDE ANTENNA
      !
    ELSE
      !
      !     POLOIDAL POSITION OF ANTENNA CLOSE TO PLASMA SURFACE
      ZPOL = THANT(2) * ZTRA
      ZCPOL = THANT(3) * ZTRA
      !
      !     POLOIDAL POSITION OF FEEDERS CLOSE TO WALL
      ZFEED = THANT(1) * ZTRA
      ZCFEED = THANT(4) * ZTRA
      !
      IF ((PT.GE.ZPOL).AND.(PT.LE.ZCPOL)) THEN
        ZTRA = PC
        ZDZDT = 0._RKIND
      ELSE IF ((PT.LE.ZFEED).OR.(PT.GE.ZCFEED)) THEN
        ZTRA = ZANTSH
        ZDZDT = 0._RKIND
      ELSE IF ((PT.GT.ZFEED).AND.(PT.LT.ZPOL)) THEN
        ZARG = 0.5_RKIND*CPI * (PT-ZPOL) / (ZFEED-ZPOL)
        ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
        ZDZDT = (ZANTSH-PC) * SIN(ZARG) * COS(ZARG) *CPI / &
             &              (ZFEED-ZPOL)
      ELSE IF ((PT.GT.ZCPOL).AND.(PT.LT.ZCFEED)) THEN
        ZARG = 0.5_RKIND*CPI * (PT-ZCPOL) / (ZCFEED-ZCPOL)
        ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
        ZDZDT = (ZANTSH-PC) * SIN(ZARG) * COS(ZARG) *CPI / &
             &              (ZCFEED-ZCPOL)
      ELSE
        WRITE(*,*) ' MESSAGE FROM SUBROUTINE ANTEN:'
        WRITE(*,*) ' CHECK ANTENNA SURFACE DEFINITION !!!'
      END IF
      !
      ZTETDI = (ZPOL + ZCPOL)*0.5_RKIND
      !
    END IF
    !
    !---------------------------------------------------------------------
    !
300 CONTINUE
    !
    !     SELECT MONO- OR DI-POLE ANTENNA
    IF (NLDIP) THEN
      IF((PT-ZTETDI).LT.1.0E-6_RKIND) ZTRA = ZANTSH
    END IF
    !
  END IF
  !
  !-------------------------------------------------------------------
  !L                NANTYP=3 : TOP/BOTTOM ANTENNA
  !
  IF (NANTYP.EQ.3) THEN
    ZA1 = ANTUP * CPI/180._RKIND
    ZF1 = FEEDUP * CPI/180._RKIND
    ZA2 = CPI - ZA1
    ZF2 = CPI - ZF1
    ZA3 = CPI + ZA1
    ZF3 = CPI + ZF1
    ZA4 = C2PI - ZA1
    ZF4 = C2PI - ZF1
    ZANTSH = PA - 0.001_RKIND
    !
    !      ANTENNA CLOSE TO PLASMA SURFACE
    IF ( ((PT.GE.ZA1).AND.(PT.LE.ZA2)) .OR. &
         &        ((PT.GE.ZA3).AND.(PT.LE.ZA4)) )  THEN
      ZTRA = PC
      ZDZDT = 0._RKIND
    END IF
    !
    !     ANTENNA CLOSE TO WALL
    IF ( ((PT.LE.ZF1).OR.(PT.GE.ZF4)) .OR. &
         &        ((PT.GE.ZF2).AND.(PT.LE.ZF3)) ) THEN
      ZTRA = ZANTSH
      ZDZDT = 0._RKIND
    END IF
    !
    !     UPPER RIGHT FEEDER
    IF ( (PT.GT.ZF1).AND.(PT.LT.ZA1) ) THEN
      ZARG = 0.5_RKIND*CPI * (PT-ZF1) / (ZA1-ZF1)
      ZTRA = ZANTSH + (PC-ZANTSH) * (SIN(ZARG))**2
      ZDZDT = (PC-ZANTSH)*SIN(ZARG)*COS(ZARG)*CPI / (ZA1-ZF1)
    END IF
    !
    !     UPPER LEFT FEEDER
    IF ( (PT.GT.ZA2).AND.(PT.LT.ZF2) ) THEN
      ZARG = 0.5_RKIND*CPI * (PT-ZA2) / (ZF2-ZA2)
      ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
      ZDZDT = (ZANTSH-PC)*SIN(ZARG)*COS(ZARG)*CPI / (ZF2-ZA2)
    END IF
    !
    !     LOWER LEFT FEEDER
    IF ( (PT.GT.ZF3).AND.(PT.LT.ZA3) ) THEN
      ZARG = 0.5_RKIND*CPI * (PT-ZF3) / (ZA3-ZF3)
      ZTRA = ZANTSH + (PC-ZANTSH) * (SIN(ZARG))**2
      ZDZDT = (PC-ZANTSH)*SIN(ZARG)*COS(ZARG)*CPI / (ZA3-ZF3)
    END IF
    !
    !     LOWER RIGHT FEEDER
    IF ( (PT.GT.ZA4).AND.(PT.LT.ZF4) ) THEN
      ZARG = 0.5_RKIND*CPI * (PT-ZA4) / (ZF4-ZA4)
      ZTRA = PC + (ZANTSH-PC) * (SIN(ZARG))**2
      ZDZDT = (ZANTSH-PC)*SIN(ZARG)*COS(ZARG)*CPI /(ZF4-ZA4)
    END IF
    !
  END IF
  !
  !     END IF OF NANTYP = 3
  !
  !
  !***********************************************************************
  !
  PROAN = PROPL + ZTRA * PB
  PDROAN = PDROPL + ZDZDT * PB
  !
  !***********************************************************************
  RETURN
END SUBROUTINE ANTEN
!CC*DECK P2C2S26
SUBROUTINE PLANSH
  !        -----------------
  !
  !  2.2.26. CHECK DEFINITIONS OF PLASMA,ANTENNA AND WALL SURFACES
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: Z(3), ZT, ZAP, ZSA, ZSP, ZRP, ZZP, ZRA, ZZA
  INTEGER :: IARG(3), IN2, JC, JSURF
  !
  DATA  IARG /3,9,4/
  !----------------------------------------------------------------------
  !
  IF (.NOT.NLOTP2(2)) RETURN
  IN2 = 2*NCHI - 2
  WRITE (NPRNT,9000)
  !
  DO JC=1,IN2
    DO JSURF=1,3
      CALL ROTETA (IARG(JSURF),JC,1)
      Z(JSURF) = RO(2)
    END DO
    ZT = TT(2)
    ZAP = Z(2) - Z(1)
    ZSA = Z(3) - Z(2)
    ZSP = Z(3) - Z(1)
    ZRP = Z(1)*COS(ZT)
    ZZP = Z(1)*SIN(ZT)
    ZRA = Z(2)*COS(ZT)
    ZZA = Z(2)*SIN(ZT)
    WRITE (NPRNT,9100) JC,ZRP,ZZP,ZRA,ZZA,ZAP,ZSA,ZSP
    !C    WRITE (NPRNT,9100) JC,TT(2),(Z(J),J=1,3),ZAP,ZSA,ZSP
  END DO
  !
  !     CHECK RHO VS THETA
  !        WRITE(NPRNT,9200)
  !        DO 200 JC=1,IN2
  !           CALL ROTETA (IARG(1),JC,1)
  !           WRITE(NPRNT,9310) JC,(RO(J),J=1,4)
  ! 200    CONTINUE
  !
  !     CHECK DERIVATIVE OF RHO VS THETA
  !        WRITE(NPRNT,9300)
  !        DO 300 JC=1,IN2
  !           CALL ROTETA (IARG(1),JC,1)
  !           WRITE(NPRNT,9310) JC,(DRODT(J),J=1,4)
  ! 300    CONTINUE
  !
9000 FORMAT (///,' 2.2 CHECK POSITION OF PLASMA,ANTENNA AND WALL',/, &
       &          1X,46('-'),//,1X,'J-CHI',4X,'R-PLA',8X,'Z-PLA',8X, &
       &         'R-ANT',8X,'Z-ANT',8X,'ANT-PL',7X,'ANT-SH',7X,'PL-SH',//)
9100 FORMAT (1X,I5,1P7E13.3)
9200 FORMAT(///,' CHECK RHO VS THETA',/, &
       &              ' *******************',/, &
       &1X,'J-CHI',9X,' RO1  ',17X,' RO2  ',17X,' RO3  ',17X,' RO4  ',/)
9300 FORMAT(///,' CHECK DERIVATIVE OF RHO VS THETA',/, &
       &              ' ********************************',/, &
       &1X,'J-CHI',9X,'DRODT1',17X,'DRODT2',17X,'DRODT3',17X,'DRODT4',/)
9310 FORMAT(1X,I5,1P4E23.13)
  !
  !
  RETURN
END SUBROUTINE PLANSH
!CC*DECK P2C3S01
SUBROUTINE IODSK2 (K)
  !        -----------------
  !
  ! 2.3.1   HANDLES DISK FILES
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMVID ! INCLUDE 'COMVID.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: Z(MD2CP2),  Z2(MD2CP2)
  INTEGER :: K, IPOL1, JINT, J, I, IN2, JROW, JCOL
  INCLUDE 'NEWRUN.inc'
  !-----------------------------------------------------------------------
  !
  GO TO (100,200),K
  !
  !-----------------------------------------------------------------------
  !L             1.          OPEN DISK FILES AND READ INPUT QUANTITIES
  !
100 CONTINUE
  !
  !lvC     OPEN AND READ NSAVE
  !lv         OPEN (UNIT=NSAVE,FILE='TAPE8',STATUS='OLD',
  !lv     +              FORM='FORMATTED')
  !lv         REWIND NSAVE
  !lv         READ (NSAVE,NEWRUN)

  !CC         IF (WALRAD .LT. 1.0) STOP
  IPOL1 = NPOL + 1
  !
  !     OPEN AND READ NVAC (FILE CREATED BY THE CHEASE CODE)
  OPEN (UNIT=NVAC,FILE='TAPE17',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  REWIND NVAC
  READ (NVAC) (SR(I),I=1,NPOL),(SZ(I),I=1,NPOL) &
       &              ,(CHI(I),I=1,IPOL1)
  READ (NVAC) QB,TB
  !
  READ (NVAC) (Z(J),J=1,NPOL),(Z2(J),J=1,NPOL)
  READ (NVAC) (R2J(J),J=1,NPOL)
  READ (NVAC) (DCR2J(J),J=1,NPOL)
  READ (NVAC) (CHIOLD(J),J=1,IPOL1)
  !
  READ (NVAC) (TH(J),J=1,IPOL1)
  READ (NVAC) (ROEDGE(J), J=1,IPOL1)
  READ (NVAC) (T(J),J=1,IPOL1)
  READ (NVAC) (ROMID(J), J=1,IPOL1)
  !
  DO JINT=1,3
    READ (NVAC) (THINT(J,JINT),J=1,NPOL)
  END DO
  DO JINT=1,3
    READ (NVAC) (ROINT(J,JINT),J=1,NPOL)
  END DO
  DO JINT=1,3
    READ(NVAC) (DROINT(J,JINT),J=1,NPOL)
  END DO
  !
  SR(IPOL1) = SR(1)
  SZ(IPOL1) = SZ(1)
  !
  !     OPEN NSOURC (ON WHICH THE SOURCE VECTOR, VACUUM MATRIX, OHM VECTORS
  !     ARE TO BE WRITTEN)
  OPEN (UNIT=NSOURC,FILE='TAPE15',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  RETURN
  !
  !-----------------------------------------------------------------------
  !L             2.          WRITE VACUUM CONTRIBUTION AND CLOSE FILES
  !
200 CONTINUE
  IN2=2*NCHI-2
  REWIND NSOURC
  WRITE (NSOURC) IN2, (SOURCE(J),J=1,IN2)
  !
  WRITE (NSOURC) IN2, REASCR, QB, TB, (DC(J),J=1,IN2), &
       &                (DELTH(J),J=1,IN2), (OHMR(J),J=1,IN2)
  !
  WRITE (NSOURC) ((WVAC(JROW,JCOL),JCOL=1,IN2),JROW=1,IN2)
  !
  !     CLOSE DISK FILES
  CLOSE (NVAC)
  CLOSE (NSOURC)
  !
  RETURN
END SUBROUTINE IODSK2
!CC*DECK P4C1S01
SUBROUTINE ORGAN4
  !        -----------------
  !
  !  4.1.01. CREATE AND SOLVE LINEAR SYSTEM WITH FRONTAL METHOD
  !--------------------------------------------------------------------
  IMPLICIT NONE
  CALL FRONTB
  RETURN
END SUBROUTINE ORGAN4
!CC*DECK P4C2S01
SUBROUTINE FRONTB
  !        -----------------
  !
  !  4.2.01. CONSTRUCT A AND SOLVE A*X=B WITH FRONTAL METHOD
  !  REF. J.K. REID, FINITE ELEMENTS IN PHYSICS (NORTH-HOLLAND PHYSICS
  !  PUBLISHING, AMSTERDAM, 1987) P. 395. (EDITOR R. GRUBER)
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMMTR ! INCLUDE 'COMMTR.inc'
  IMPLICIT NONE
  !
  INTEGER :: IX, JS, JC, JPSI, JCHI, &
       & MJ, J2, IU, JI, IM2, ILAST, IFIRST, IA, NEG, JROW
  !
  !===================================================================
  !L          1.    INITIALIZATIONS
  !===================================================================
  !
  !     OPEN DISK FILES AND REWIND
  !
  CALL IODSK4 (1)
  CALL IODSK4 (9)
  !
  !     DEFINE NUMERICAL AUXILIARY PARAMETERS:
  !
  !     M1    = NR. OF OVERLAPPING ROWS/COLUMMS OF MATRIX BLOCKS
  !     M2    = NR. OF NON-OVERLAPPING ROWS/COLUMMS OF MATRIX BLOCKS
  !     M12   = NR. OF ROWS/COLUMMS OF MATRIX BLOCKS
  !     N     = NR. OF MATRIX BLOCKS
  !     NCOMP = NR. OF ROWS/COLUMNS OF TOTAL MATRIX
  !     NLONG = NR. OF ELEMENTS IN A MATRIX BLOCK
  !     IX    = INDEX POINTING TO AN ELEMENT OF THE SOURCE VECTOR XT
  !
  M1    = NPOL
  M2    = 2*NPOL
  M11   = M1+1
  M12   = M1+M2
  N     = NPSI
  NCOMP = N*M2+M1
  NLONG = M12*M12
  IX    = M11
  !
  !     SET FIRST MATRIX BLOCK TO ZERO
  !
  CALL CVZERO (NLONG,A1D(1),1)
  !
  !     GET VACUUM CONTRIBUTION TO SOURCE VECTOR AND PUT IT INTO XT AND 
  !     TO SOURCT
  !
  CALL IODSK4 (6)
  !
  !     ARRAY U(1..M12) IS THE RIGHT-HAND SIDE CORRESPONDING TO ONE
  !     MATRIX BLOCK. SET IT INITIALLY TO ZERO.
  !
  CALL CVZERO (M12,U(1),1)
  !
  !---------------------------------------------------------------------
  !L          1B.   CONSTRUCT RIGHT-HAND SIDE VECTOR SOURCT, WITH A COPY
  !                 TO XT, IN CASE OF AN ANTENNA INSIDE THE PLASMA
  !
  IF (NANTYP.LT.0) THEN
    !
    DO JS = 1, NPSI
      !lvC
      !lvC     READ EQUILIBRIUM QUANTITIES
      !lv            CALL IODSK4 (4)
      !
      !     CONTRIBUTION OF ONE RANGE OF S=CONST CELLS
      DO JC = 1, NPOL
        CALL RHSHYB (NANTYP, JS,JC)
      END DO
    END DO
    !
    !     REWIND
    CALL IODSK4 (9)
  END IF
  !
  !---------------------------------------------------------------------
  !     COPY M1 FIRST ELEMENTS OF XT INTO U
  !
  CALL CCOPY (M1,XT(1),1,U(1),1)
  !
  !=====================================================================
  !L          2.    LOOP OVER THE BLOCKS OF MATRIX A. EACH BLOCK
  !L                OF A CORRESPONDS TO A RANGE OF S=CONST CELLS
  !=====================================================================
  !
  DO JPSI = 1 , NPSI
    !
    !---------------------------------------------------------------------
    !L          2.1.  MATRIX AND RIGHT-HAND SIDE VECTOR ASSEMBLY
    !---------------------------------------------------------------------
    !
    !lvC     READ EQ QUANTITIES
    !lvC
    !lv         CALL IODSK4 (4)
    !
    !     CONTRIBUTIONS OF ONE RANGE OF S=CONST.CELLS
    !
    DO JCHI = 1 , NPOL
      !
      !     CONTRIBUTION TO MATRIX
      CALL INTEGR (1,JPSI,JCHI)
      !
      !     CONTRIBUTION TO RIGHT-HAND-SIDE VECTOR
      !CC	    IF (NANTYP.LT.0) THEN
      !CC	       CALL RHSHYB (NANTYP, JPSI, JCHI)
      !CC	    END IF
      !
    END DO
    !
    !     BOUDARY CONDITIONS: ADD VACUUM MATRIX
    !
    IF (JPSI.EQ.NPSI) CALL ADDVAC
    !
    !---------------------------------------------------------------------
    !L          2.2.  ELIMINATION PROCEDURE
    !---------------------------------------------------------------------
    !
    !     DECOMPOSE A BLOCK OF A INTO LDU. FOR ALL BLOCKS EXCEPT THE
    !     LAST ONE WE DECOMPOSE THE FIRST M2 LINES AND COLUMNS OF A
    !     (THE NON-OVERLAPPING PART OF THE MATRIX BLOCKS). FOR THE LAST
    !     BLOCK, WE DECOMPOSE ALL M12 LINES AND COLUMNS.
    !
    IF (JPSI.NE.NPSI) THEN
      !
      CALL CALD (A1D,EPSMAC,M1,M2,NSING,NEG)
      !
    ELSE
      !
      CALL CALD (A1D,EPSMAC,0,M12,NSING,NEG)
      !
    END IF
    !
    !     SOLVE LOWER TRIANGULAR SYSTEM (L*Y = B) FOR THE FULLY ASSEMBLED
    !     UNKNOWNS (M2 FIRST EQUATIONS OF THE BLOCK, ALL M12 EQUATIONS OF
    !     THE LAST BLOCK).
    !
    !     COPY CORRESPONDING ELEMENTS OF XT TO ELEMENTS M1+1 TO M12 OF U.
    !     NOTE THAT ELEMENTS 1 TO M11 OF U ARE ALREADY IN PLACE: THEY HAVE
    !     BEEN PARTLY SOLVED WITH THE PREVIOUS, PARTLY DECOMPOSED BLOCK.
    !
    CALL GETRG (XT,U,IX,IX+M2-1,M1)
    !
    !     INCREMENT THE INDEX IX POINTING TO XT
    !
    IX = IX + M2
    !
    !     SCAN OVER M2 ROWS OF A (M12-1 ROWS FOR THE LAST BLOCK)
    !
    MJ = M2
    !
    IF (JPSI.EQ.NPSI) MJ = M12
    !
    !     FORWARD SUBSTITUTION OF SOURCE VECTOR
    !
    DO J2 = 1 , MJ
      IF (J2 .NE. M12) CALL CAXPY (M12-J2,-U(J2),A1D(J2*M12+J2),M12,U(J2+1),1)
    END DO
    !
    !     COPY ELEMENTS 1 TO MJ OF U TO CORRESPONDING ELEMENTS OF XT
    !
    IU = M2 * (JPSI-1) + 1
    CALL PUTRG (XT,U,IU,IU+MJ-1,0)
    !
    !     COPY M1 LAST ELEMENTS OF U TO THE M1 FIRSTS (NOT FOR LAST BLOCK)
    !
    IF (JPSI.NE.NPSI) THEN
      !
      DO JI = 1 , M1
        IM2 = JI + M2
        U(JI) = U(IM2)
      END DO
      !
      !     CLEAR ELEMENTS M1+1 TO M12 OF U (NOT FOR LAST BLOCK)
      !
      CALL CVZERO (M2,U(M11),1)
      !
    END IF
    !
    !---------------------------------------------------------------------
    !L          2.3.  PREPARE MATRIX BLOCK FOR THE NEXT RANGE OF CELLS
    !---------------------------------------------------------------------
    !
    !     WRITE THE FIRST M2 LINES OF THE MATRIX BLOCK ON SCRATCH FILE(S).
    !1206 Or copy 2*npol first lines of A into AALL(.,JPSI)
    !     (ONLY THE UPPER TRIANGULAR PART WILL BE NEEDED FOR THE BACK-
    !     SUBSTITUTION)
    !
    IF (MDSTORE.EQ.0) THEN
      CALL IODSK4 (3)
    ELSE IF (MDSTORE.EQ.1) THEN
      CALL CCOPY(2*NLONG/3,A1D(1),1,AALL(1,JPSI),1)
    END IF
    !
    !     COPY THE LAST OVERLAP ONTO THE FIRST, AND ZERO OTHER ELEMENTS
    !     (NOT FOR LAST BLOCK)
    !
    IF (JPSI.NE.NPSI) THEN
      !
      DO JROW = 1 , M1
        ILAST = M12 * (M2+JROW-1) + M2 + 1
        IFIRST= M12 * (   JROW-1)      + 1
        CALL CCOPY (M1,A1D(ILAST),1,A1D(IFIRST),1)
      END DO
      !
      DO JROW = 1 , M1
        IFIRST= M12 * (JROW-1) + M1 + 1
        CALL CVZERO (M2,A1D(IFIRST),1)
      END DO
      !
      IA = M12 * M1 + 1
      CALL CVZERO (M12*M2,A1D(IA),1)
      !
    END IF
    !
  END DO
  !
  !                  END OF LOOP ON BLOCKS OF A
  !=====================================================================
  !L          3.     REMAINING OPERATIONS
  !
  !     BACK-SUBSTITUTION: SOLVE UPPER TRIANGULAR SYSTEM
  !
  CALL CDLHXV
  !
  !     WRITE THE SOLUTION ONTO NDS
  !
  CALL IODSK4 (7)
  !
  !     COMPUTE THE TOTAL POWER
  !
  CALL POWER
  !
  !     CLOSE DISK FILES
  !
  CALL IODSK4 (10)
  !
  !=====================================================================
  RETURN
END SUBROUTINE FRONTB
!CC*DECK P4C2S02
SUBROUTINE INTEGR (K,KPSI,KCHI)
  !        -----------------
  !
  !  4.2.02.  CONTRIBUTION OF ONE CELL TO MATRIX A
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZARG
  INTEGER :: K, KPSI, KCHI, J
  COMPLEX   ZEIN1,    ZEIN12,   ZEIN2
  !--------------------------------------------------------------------
  !>>>> test printout
  if (nlotp4(4)) then
    write (nprnt,9990) kpsi, kchi, eq(21,kchi,kpsi), &
         &       eq(24,kchi,kpsi), &
         &       eq(25,kchi,kpsi), eq(2,kchi,kpsi), eq(4,kchi,kpsi), &
         &       eq(6,kchi,kpsi)
  end if
9990 format (2i5,1p6e15.5)
  !---------------------------------------------------------------------
  !L                  1.  INITIALIZE
  !
  !     DETECT NUMBERING SEQUENCE FOR THE CELL
  !
  CALL DEC (KPSI,KCHI,NPSI,NPOL,NPLAC)
  !
  !----------------------------------------------------------------------
  !L                  2.  CONTRIBUTION OF THE CELL
  !
  !     HYBRID ELEMENTS
  !
  CALL AHYBRD (5,KPSI,KCHI)
  !
  !     IMPOSE REGULARITY CONDITIONS ON AXIS (lim s->0 X = 0)
  !
  IF (KPSI.EQ.1) THEN
    CALL AWAY (K,6,1)
    CALL AWAY (K,6,2)
    !lv     impose also lim s->0 V = 0 : comment out the next 2 lines
    !lv            CALL AWAY (K,6,3)
    !lv            CALL AWAY (K,6,4)
  END IF
  !
  !     IMPOSE BOUNDARY CONDITIONS ON PLASMA SURFACE IF WALL ON PLASMA
  !
  IF ((KPSI.EQ.NPSI).AND.(WALRAD.LE.1._RKIND)) THEN
    CALL AWAY (K,6,5)
    CALL AWAY (K,6,6)
  END IF
  !
  !     IMPOSE JUMP CONTITION NEAR CHI=PI IF NLPHAS
  !
  IF ((KCHI.EQ.NCHI).AND.NLPHAS) THEN
    ZARG = WNTORO * QS(KPSI) * C2PI
    ZEIN1 = CMPLX ( COS(ZARG), SIN(ZARG) )
    ZARG = WNTORO * QTILDA(KPSI) * C2PI
    ZEIN12 = CMPLX ( COS(ZARG), SIN(ZARG) )
    ZARG = WNTORO * QS(KPSI+1) * C2PI
    ZEIN2 = CMPLX ( COS(ZARG), SIN(ZARG) )
    DO J = 1, 6
      CONA(2,J) = CONA(2,J) * CONJG(ZEIN1)
      CONA(4,J) = CONA(4,J) * CONJG(ZEIN12)
      CONA(6,J) = CONA(6,J) * CONJG(ZEIN2)
    END DO
    DO J = 1, 6
      CONA(J,2) = CONA(J,2) * ZEIN1
      CONA(J,4) = CONA(J,4) * ZEIN12
      CONA(J,6) = CONA(J,6) * ZEIN2
    END DO
  END IF
  !
  !     ADD CONTRIBUTION OF THE CELL TO MATRIX BLOCK
  !
  CALL STORE (K,1.0_RKIND)
  !
  !----------------------------------------------------------------------
  RETURN
END SUBROUTINE INTEGR
!CC*DECK P4C2S02B
SUBROUTINE RHSHYB (KANTYP, KPSI, KCHI)
  !	 -----------------
  !
  ! 4.2.02B. CONTRIBUTION OF ONE CELL TO THE RIGHT-HAND SIDE VECTOR FROM 
  !	   VOLUME ANTENNA CURRENTS.
  !>>>       MODIFIED TO INCLUDE THE OPTION NLPHAS.
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZARG, ZARG1, ZARG12, ZARG2
  INTEGER :: KANTYP, KPSI, KCHI, J, JA, ICON, JROW, JCON
  COMPLEX ZC, ZI, ZSI, ZDSIDC
  !--------------------------------------------------------------------
  !
  ZI = CMPLX(0._RKIND,1._RKIND)
  !
  !     DETECT NUMBERING SEQUENCE FOR THE CELL
  CALL DEC (KPSI,KCHI,NPSI,NPOL,NPLAC)
  !
  !     EQUILIBRIUM QUANTITIES
  CALL QUAEQU (KPSI, KCHI)
  !
  !     BASIS FUNCTIONS
  CALL BASIS2 (WS,WDS,WCHI,WDCHI,XF)
  !
  !     VOLUME ANTENNA CURRENT "POTENTIAL" SIGMA AND DSIGMA/DCHI
  ZSI = CMPLX(0._RKIND,0._RKIND)
  ZDSIDC = CMPLX(0._RKIND,0._RKIND)
  !
  IF ((WS.GT.SAMIN).AND.(WS.LT.SAMAX)) THEN
    DO JA = 1, MANCMP
      ZSI = ZSI + CMPLX( CURSYM(JA)*COS(MPOLWN(JA)*WCHI), &
           &			       CURASY(JA)*SIN(MPOLWN(JA)*WCHI) )
      ZDSIDC = ZDSIDC + MPOLWN(JA) * &
           &			CMPLX(-CURSYM(JA)*SIN(MPOLWN(JA)*WCHI), &
           &			       CURASY(JA)*COS(MPOLWN(JA)*WCHI) )
    END DO
  END IF
  !
  !    COEFFICIENT OF UNKNOWN X* IN INTEGRAND
  ZC = 2.0_RKIND * CPSRF * WS * WTQ *(ZDSIDC + ZI*WNTORO*WT/WTQ) * &
       &	      WDS * WDCHI * ZI * OMEGA
  !
  !     OPTION NLPHAS
  IF (NLPHAS) THEN
    ZARG = WNTORO * WQ * EQ(24,KCHI,kpsi)
    IF (KCHI.GT.NCHI) THEN
      ZARG = ZARG - WNTORO * WQ * C2PI
    END IF
    ZC = ZC * CMPLX ( COS(ZARG), SIN(ZARG) )
  END IF
  !
  !    COEFFICIENT #2 FOR X
  IF (.NOT.NLPHAS) THEN
    CALL CONST2 (9,KCHI,XETA,XKSI)
  ELSE
    CALL CONST3 (9,kpsi,KCHI,XETA,XKSI)
  END IF
  !
  !    MULTIPLY WITH BASIS FUNCTIONS TO OBTAIN VECTOR OF COEFFICIENTS
  CALL VECT (XKSI, XF, XVKSI)
  !
  !    MULTIPLY WITH COEFFICIENT OF UNKNOWN X* IN INTEGRAND
  DO J=1,6
    XVKSI(J) = XVKSI(J) * ZC
  END DO
  !
  !     IMPOSE REGULARITY CONDITIONS ON AXIS (lim s->0 X = 0)
  IF (KPSI.EQ.1) THEN
    XVKSI(1) = CMPLX(0._RKIND,0._RKIND)
    XVKSI(2) = CMPLX(0._RKIND,0._RKIND)
    !     impose also lim s->0 V = 0
    XVKSI(3) = CMPLX(0._RKIND,0._RKIND)
    XVKSI(4) = CMPLX(0._RKIND,0._RKIND)
  END IF
  !
  !     IMPOSE JUMP CONDITION NEAR CHI=PI IF NLPHAS
  IF (NLPHAS.AND.KCHI.EQ.NCHI) THEN
    ZARG1  =  WNTORO * QS    (KPSI)   * C2PI
    ZARG12 =  WNTORO * QTILDA(KPSI)   * C2PI
    ZARG2  =  WNTORO * QS    (KPSI+1) * C2PI
    XVKSI(2) = XVKSI(2) * CMPLX (COS(ZARG1) ,SIN(ZARG1) )
    XVKSI(4) = XVKSI(4) * CMPLX (COS(ZARG12),SIN(ZARG12))
    XVKSI(6) = XVKSI(6) * CMPLX (COS(ZARG2) ,SIN(ZARG2) )
  END IF
  !
  !>>>
  IF (NLOTP4(3)) THEN
    WRITE (NPRNT,9001) KPSI,KCHI,WS,WCHI,ZSI,ZDSIDC,ZC
    WRITE (NPRNT,9002) (XKSI(J),J=1,5)
    WRITE (NPRNT,9003) (XVKSI(J),J=1,6)
  END IF
9001 FORMAT(/,'KPSI, KCHI, WS, WCHI, ZSI, ZDSIDC, ZC',/, &
       &		1X,2I4,1P8E12.3)
9002 FORMAT(1X,1P10E12.3)
9003 FORMAT(1X,1P12E10.3)
  !<<<
  !
  !    ADD XVKSI TO THE TOTAL RHS VECTOR:
  !    SOURCT : RHS, SAVED FOR FURTHER USE IN SUBROUTINE POWER
  !    XT     : WILL BECOME THE SOLUTION VECTOR AFTER ELIMINATION
  ICON=6
  DO JROW = 1, ICON
    JCON = (KPSI-1) * 2 * NPOL + NPLAC(JROW)
    SOURCT(JCON) = SOURCT(JCON) + XVKSI(JROW)
    XT    (JCON) = XT    (JCON) + XVKSI(JROW)
  END DO
  !
  RETURN
END SUBROUTINE RHSHYB
!CC*DECK P4C2S03
SUBROUTINE ADDVAC
  !        -----------------
  !
  !  4.2.03. ADDS VACUUM CONTRIBUTION TO A
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMMTR ! INCLUDE 'COMMTR.inc'
  IMPLICIT NONE
  !
  INTEGER :: I1, ILEFTA, JROW, ILEFTV, JCOL, IA, IV
  !-------------------------------------------------------------------
  !
  !     OBTAIN VACUUM MATRIX AND ADD IT TO THE LAST SUBBLOCK OF A
  !
  CALL IODSK4 (2)
  !
  I1 = 2*NCHI - 2
  ILEFTA = LENGTH - NCOLMN * (I1-1) - I1
  !
  DO JROW=1,I1
    ILEFTV = (JROW-1) * I1
    DO JCOL=1,I1
      IA = ILEFTA +JCOL
      IV = ILEFTV +JCOL
      A1D(IA) = A1D(IA) - VAC(IV)
    END DO
    ILEFTA = ILEFTA + NCOLMN
  END DO
  !
  RETURN
END SUBROUTINE ADDVAC
!CC*DECK P4C2S04
SUBROUTINE CALD (A,EPS,MB1,MB2,NSING,NEG)
  !        ---------------
  !
  !  4.2.04. DECOMPOSE MB2 FIRST LINES AND COLUMNS OF A
  !------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: MB1, MB2, NSING, NEG, N, M, IKD, JIB, IA, IB, JJB, ISIDE, ITRA
  REAL(RKIND) :: EPS, AD
  COMPLEX     A((MB1+MB2+2)*MB2+1+(MB1+MB2-1)*(MB1+MB2)),        DIAG,     SIDE,    DM1
  !------------------------------------------------------------------
  !
  !     INITIALIZE
  !
  NSING=0
  N=MB2
  IF (MB1 .EQ. 0) N=MB2-1
  M=MB1+MB2
  IF (N .EQ. 1) RETURN
  IKD=1
  AD=CABS(A(1))*EPS
  !
  !     SCAN OVER ALL MB2 PIVOTS
  !
  DO JIB=1,N
    DIAG=A(IKD)
    !
    !     TEST FOR ZERO PIVOT
    !
    IF(CABS(DIAG).GT.AD) GO TO 5
    IF (NSING .EQ. -1) GO TO 5
    NSING=-1
    GO TO 110
    !
    !     SCAN DOWNWARDS FOR RECTANGULAR RULE
    !
5   CONTINUE
    IA=IKD+M+1
    IB=JIB+1
    AD=CABS(A(IA))*EPS
    !
    DO JJB=IB,M
      ISIDE=(JJB-JIB)*M+IKD
      SIDE=-A(ISIDE)/DIAG
      CALL CAXPY(M-IB+1,SIDE,A(IKD+1),1,A(ISIDE+1),1)
      !CC
      !CC	 DO 10 J10=1,M-IB+1
      !CC	 A(ISIDE+J10) = A(ISIDE+J10) + SIDE * A(IKD+J10)
      !CC   10	 CONTINUE
      !CC
      A(ISIDE)=-SIDE
    END DO
    !
    DM1=1._RKIND/DIAG
    CALL CSCAL(M-IB+1,DM1,A(IKD+1),1)
    IKD=IKD+M+1
  END DO
  IF (NSING .EQ. 0) GOTO 40
  NSING=0
  RETURN
40 CONTINUE
  !
  !     LAST DIAGONAL ELEMENT
  !
  IF (MB1 .NE. 0) RETURN
  IF(CABS(A(IKD)).GT.AD) RETURN
  NSING=-1
  !
110 CONTINUE
  ITRA=IKD
  PRINT 1100,ITRA,A(ITRA)
1100 FORMAT(/,' MESSAGE FROM CALD',/ &
       &          ' MATRIX SINGULAR, ELEMENT',I6,1P2E15.3,/)
  RETURN
END SUBROUTINE CALD
!CC*DECK P4C2S05
SUBROUTINE CDLHXV
  !        -----------------
  !
  !  4.2.05. SOLVE UPPER TRIANGULAR SYSTEM
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMMTR ! INCLUDE 'COMMTR.inc'
  IMPLICIT NONE
  !
  INTEGER :: IU, MJ, IKD, MJ1, I2, JN, I1, J2
  COMPLEX     UI2,    CDOTU
  !--------------------------------------------------------------------
  !  WHEN THIS ROUTINE IS CALLED, 'A' CONTAINS THE FULLY LDU-DECOMPOSED
  !  LAST BLOCK OF THE TOTAL MATRIX. THE SCRATCH FILE(S), IF ANY, CON-
  !  TAIN N RECORDS, EACH CONTAINING THE M2*M12 FIRST ELEMENTS OF THE
  !  LDU-DECOMPOSED BLOCKS. THE SCRATCH FILES ARE POSITIONED AFTER THE
  !  LAST (N-TH) RECORD.
  !--------------------------------------------------------------------
  !L    		1. SOLVE LAST TRIANGLE OF M1 EQUATIONS
  !--------------------------------------------------------------------
  !
  !     POINTER TO XT
  !
  IU = M2 * (N-1) + 1
  !
  !     COPY M12 LAST ELEMENTS OF RIGHT-HAND SIDE(XT) TO U
  !
  CALL GETRG (XT, U, IU, IU+M12-1, 0)
  !
  !     SOLVE LAST EQUATION
  !
  MJ = M12
  IKD = NLONG
  U(M12) = U(M12) / A1D(IKD)
  MJ1 = M1 - 1
  !
  !     SOLVE M1-1 LAST EQUATIONS
  !
  IF (M1 .GT. 1) THEN
    !
    DO J2 = 1, MJ1
      IKD = IKD - M12 - 1
      I2  = M12 - J2
      UI2 = U(I2) / A1D(IKD)
      UI2 = UI2 - CDOTU (J2, A1D(IKD+1), 1, U(I2+1), 1)
      U(I2) = UI2
    END DO
    !
  END IF
  !
  !     BACKSPACE SCRATCH FILE(S) ==> POSITION THEM BEFORE THE BLOCK 
  !     CORRESPONDING TO THE LAST RADIAL INTERVAL
  !1206
  IF (MDSTORE.EQ.0) THEN
    CALL IODSK4 (11)
  END IF
  !
  !---------------------------------------------------------------------
  !L    		2. LOOP OVER N BLOCKS, FROM LAST DOWNTO FIRST
  !---------------------------------------------------------------------
  !
  DO JN = 1, N
    !
    !     FOR NLDISO=F, WE SOLVE ONLY FOR THE LAST BLOCK (JN=1)
    !
    IF (NLDISO .OR. (JN.EQ.1)) THEN
      !
      !     ADDRESS OF DIAGONAL ELEMENT OF M2-TH ROW OF A
      !
      IKD = M2*M12-M1
      !
      !     SCAN DOWN M2 ROWS
      !
      DO J2 = 1, M2
        I2  = M2 - J2 + 1
        UI2 = U(I2) / A1D(IKD)
        I1  = J2 + M1 - 1
        UI2 = UI2 - CDOTU (I1, A1D(IKD+1), 1, U(I2+1), 1)
        U(I2) = UI2
        IKD = IKD - M12 - 1
      END DO
      !
      !     PUT MJ LINES OF U BACK TO XT
      !
      IU = M2 * (N-JN) + 1
      CALL PUTRG (XT, U, IU, IU+MJ-1, 0)
      IU = IU - M2
      MJ = M2
      !
      !     EXIT IF N BLOCKS ALREADY SOLVED
      !
      IF (JN .NE. N) THEN
        !
        !     REPLACE M1 VECTOR COMPONENTS
        !
        DO J2=1,M1
          I2=M2+J2
          U(I2)=U(J2)
        END DO
        !
        !     READ OLD VECTOR
        !
        CALL GETRG(XT,U,IU,IU+M2-1,0)
        !
        !     READ BACKWARDS THE DECOMPOSED MATRIX
        !1206 OR COPY THE DECOMPOSED MATRIX FROM AALL TO A
        !     ONLY THE FIRST M2=2*NPOL LINES ARE REQUIRED

        IF (MDSTORE.EQ.0) THEN
          CALL IODSK4 (5)
        ELSE IF (MDSTORE.EQ.1) THEN
          CALL CCOPY(2*NLONG/3,AALL(1,N-JN),1,A1D(1),1)
        END IF
        !
      END IF
      !
    END IF
    !
  END DO
  !---------------------------------------------------------------------
  RETURN
END SUBROUTINE CDLHXV
!CC*DECK P4C2S06
SUBROUTINE GETRG(PT,P,KS,KE,KP)
  !        ----------------
  !
  !  4.2.06. GET A SECTION OF VECTOR PT INTO P
  !---------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KS, KE, KP, IP, IT
  COMPLEX     PT,       P
  DIMENSION   PT(KE),    P(KP+KE-KS+1)
  !-----------------------------------------------------------------
  IP=KP
  DO IT=KS,KE
    IP=IP+1
    P(IP)=PT(IT)
  END DO
  RETURN
END SUBROUTINE GETRG
!CC*DECK P4C2S07
SUBROUTINE PUTRG(PT,P,KS,KE,KP)
  !        ----------------
  !
  !  4.2.07. PUT P INTO PT
  !-----------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KS, KE, KP, IP, IT
  COMPLEX     PT,       P
  DIMENSION   PT(KE),    P(KP+KE-KS+1)
  !-----------------------------------------------------------------
  IP=KP
  DO  IT=KS,KE
    IP=IP+1
    PT(IT)=P(IP)
  END DO
  RETURN
END SUBROUTINE PUTRG
!CC*DECK P4C2S08
SUBROUTINE POWER
  !        ----------------
  !
  !  4.2.08. CALCULATE TOTAL COMPLEX POWER
  !>>>       MODIFIED TO INCLUDE OPTION NLPHAS
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  INTEGER :: IN2, IS, I, J, ILEFTV, JROW, JX1
  REAL(RKIND) :: ZQB, ZTB, ZTRA, ZARG, ZREPOW, ZIMPOW, ZCOUPL, ZINV
  COMPLEX &
       &   CDOTC,   CDOTU,   ZCON,    ZPOWER,  ZPLPOW,   ZVAPOW &
       &  ,ZSCR,    ZSPOW,   ZWPOW,   ZI,      ZXJX1
  !----------------------------------------------------------------------
  !L                   1.  READ
  !
  !     READ SOURCE VECTOR (INTO U), REACTANCE SCALAR (REACS),
  !     OHM-VECTOR (XOHMR) AND VACUUM MATRIX (VAC)
  !
  CALL IODSK4 (8)
  ZSCR = REACS
  ZQB  = RQB
  ZTB  = RTB
  IN2  = NPOL
  IS = IN2
  !
  !     SOLUTION ON THE BOUNDARY: COPY NPOL LAST ELEMENTS OF XT TO X
  !
  I = NCOMP - IN2
  !
  DO J=1,IN2
    I=I+1
    X(J) = XT(I)
  END DO
  !
  !     PRINTOUT OF OHM-VECTOR AND SOLUTION ON THE BOUNDARY
  !
  IF (NLOTP4(2)) THEN
    WRITE (NPRNT,1002)
    WRITE (NPRNT,1000)
    WRITE (NPRNT,1010) (J,XDCHI(J),XDTH(J),XOHMR(J),J=1,IS)
  END IF
  !
  IF (NLOTP4(3)) THEN
    WRITE (NPRNT,1003)
    WRITE (NPRNT,1040) (J,X(J),X(J+1),J=1,IN2,2)
    WRITE (NPRNT,1051)
    WRITE (NPRNT,1055) (J,XT(J),SOURCT(J),J=1,NCOMP)
  END IF
  !
  !----------------------------------------------------------------------
  !L                2. CALCULATE POWER AT PLASMA SURFACE
  !                    USING EQ.(3.51) OF REF.(1)
  !
  ZI = CMPLX(0._RKIND,1._RKIND)
  !
  IF (NANTYP.GE.0) THEN
    ZSPOW = CDOTC (IN2,X(1),1,U(1),1) *ZI/OMEGA
  ELSE
    ZSPOW = CDOTC (NCOMP,XT(1),1,SOURCT(1),1) *ZI/OMEGA
  END IF
  !
  DO JROW = 1,IN2
    ILEFTV = (JROW-1) * IN2 + 1
    U(JROW) = CDOTU (IN2,VAC(ILEFTV),1,X(1),1)
  END DO
  !
  ZWPOW = CDOTC (IN2,X(1),1,U(1),1) *ZI/OMEGA
  !
  !     NORMALISATION
  !
  ZSPOW = ZSPOW * 0.5_RKIND * CPI
  ZWPOW =-ZWPOW * 0.5_RKIND * CPI
  ZPOWER = ZSPOW + ZWPOW
  IF (AIMAG(ZWPOW).NE.0._RKIND) THEN
    ZTRA = REAL(ZWPOW,RKIND) / AIMAG(ZWPOW)
  ELSE
    ZTRA = 0.0_RKIND
  END IF
  WRITE (NPRNT,9100) ZSPOW,ZTRA
  !
  !-----------------------------------------------------------------------
  !L                3. CALCULATE POWER AT THE ANTENNA
  !                    USING EQ.(3.35) OF REF.(1)
  !
  !lvC     READ EQ QUANTITIES ON (NPSI+1)-TH SURFACE = PLASMA SURFACE (S=1).
  !lvC
  !lv         CALL IODSK4 (4)         
  !
  !     (ROT E)-SUB-N * DSIGMA / DELTATHETA
  !
  DO J = 1,IN2
    !
    JX1 = J + 1
    !
    !     PERIODICITY
    IF (J.EQ.IN2) THEN
      JX1 = 1
    END IF
    !
    !     NORMAL CASE (NO POLOIDAL PHASE EXTRACTION)
    !
    IF (.NOT.NLPHAS) THEN
      !
      U(J) = EQ(19,J,npsi+1) * ( X(JX1) - X(J) &
           &          + CMPLX(EQ(20,J,npsi+1), WNTORO*ZTB/EQ(19,J,npsi+1)) &
           &          * (X(JX1) + X(J)) * 0.5_RKIND * XDCHI(J) ) / XDTH(J)
      !
      !     CASE WITH POLOIDAL PHASE EXTRACTION
      !
    ELSE
      !
      !     JUMP CONDITION NEAR CHI=PI
      IF (J.EQ.NCHI) THEN
        ZARG = WNTORO * ZQB * C2PI
        ZXJX1 = X(JX1) * CMPLX (COS(ZARG), SIN(ZARG))
      ELSE
        ZXJX1 = X(JX1)
      END IF
      !
      !     PHASE EXTRACTION
      ZARG = - WNTORO * ZQB * EQ(24,J,npsi+1)
      IF (J.GT.NCHI) THEN
        ZARG = ZARG + WNTORO * ZQB * C2PI
      END IF
      !
      U(J) = EQ(19,J,npsi+1) * ( ZXJX1 - X(J) + EQ(20,J,npsi+1) &
           &                                            * (ZXJX1 + X(J)) * &
           &          0.5_RKIND * XDCHI(J) ) * CMPLX (COS(ZARG), SIN(ZARG)) / &
           &          XDTH(J)
      !
    END IF
    !
  END DO
  !
  !     FACTORS FOR POWER
  ZCON= CPI * 0.5_RKIND
  !
  !     VACUUM POWER
  ZVAPOW = ZI * OMEGA * ZCON * ZSCR
  !
  !     PLASMA POWER
  ZPLPOW = CDOTC (IN2,U(1),1,XOHMR(1),1)
  ZPLPOW = ZCON * ZPLPOW
  !
  !     TOTAL POWER
  ZPOWER = ZPLPOW + ZVAPOW
  IF (REAL(ZPOWER).NE.0._RKIND) THEN
    ZCOUPL = AIMAG(ZPOWER) / REAL(ZPOWER)
    IF (ZCOUPL.NE.0._RKIND) THEN
      ZINV = 1._RKIND/ZCOUPL
    END IF
  END IF
  !
  WRITE (NPRNT,9200) ZPLPOW,ZVAPOW,ZPOWER,ZCOUPL,ZINV
  !
  ZREPOW = REAL(ZSPOW,RKIND)
  ZIMPOW = AIMAG (ZSPOW) + AIMAG(ZVAPOW)
  !
  WRITE (NPRNT,9210) OMEGA, FREQCY, ZREPOW, ZIMPOW
  !
  !----------------------------------------------------------------------
1000 FORMAT (///,' OUTPUT FROM POWER',//,' DC, D-TH, OHMR, OHMI',/)
1002 FORMAT (//,' OUTPUT 4.2',/)
1003 FORMAT (//,' OUTPUT 4.3',/)
1010 FORMAT (1X,I5,1P4E15.3)
1040 FORMAT (//,1X,' X ON PLASMA BOUNDARY',//,(1X,I5,1P4E15.3))
1051 FORMAT (//,'  JS   SOLUTION VECTOR            SOURCE VECTOR',/)
1055 FORMAT (1X,I5,1P4E12.3)
  !
9100 FORMAT (///,1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COMPLEX POWER AT PLASMA SURFACE',29X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),14X,'PLASMA(SURFACE)',1PE16.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'NON-HERMICITY OF VACUUM MATRIX',1PE15.5,15X, &
       &   3('*')/,1X,3('*'),62X,3('*'))
  !
9200 FORMAT (1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COMPLEX POWER AT ANTENNA       ',29X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),14X,'PLASMA(ANTENNA)',1PE16.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),14X,'VACUUM',1PE25.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),14X,'TOTAL ',1PE25.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COUPLING (IM POWER/RE POWER)',1PE17.5,15X, &
       &   3('*')/,1X,3('*'),2X,'INVERSE COUPLING',1PE29.5,15X,3('*')/, &
       &   1X,3('*'),62X,3('*')/,1X,68('*')//)
  !
9210 FORMAT (//,12X,'OMEGA',10X,'FREQUENCY',6X,'RE(POWER)',6X, &
       &	 'IM(POWER)',/,1X,1P4E15.5,'  987654321.0'/)
  !
  !----------------------------------------------------------------------
  RETURN
END SUBROUTINE POWER
!CC*DECK P4C3S01
SUBROUTINE DEC (KPSI,KCHI,KDPSI,KDPOL,KPLAC)
  !        --------------
  !
  !  4.3.01. RELATION BETWEEN LOCAL(CELL) AND GLOBAL(BLOCK) NUMBERINGS
  !          MONOTONIC CHI NUMBERING.  VERSION 4.10.85.  LDV
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  INTEGER :: KPSI, KCHI, KDPSI, KDPOL, KPLAC(6), J
  !
  DO J=1,5,2
    KPLAC(J) = KCHI + (J-1) * KDPOL / 2
    KPLAC(J+1) = KPLAC(J) + 1
    IF (KCHI.EQ.KDPOL) KPLAC(J+1) = KPLAC(J+1) - KDPOL
  END DO
  !
  RETURN
END SUBROUTINE DEC
!CC*DECK P4C3S03
SUBROUTINE AHYBRD (KINT,KS,KCHI)
  !        -----------------
  !
  !  4.3.03. HYBRID ELEMENTS : CONSTRUCTS LOCAL 6*6 MATRIX
  !>>>       MODIFIED TO INCLUDE NLPHAS OPTION (0895)
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  INTEGER :: KINT,KS,KCHI, J, JI, JJ
  !-----------------------------------------------------------------------
  !L               1.  INITIALIZE
  !
  CALL RESETR (CONA,2*6*6,0.0_RKIND)
  !
  !     EQUILIBRIUM QUANTITIES
  CALL QUAEQU (KS, KCHI)
  !
  !     BASIS FUNCTIONS
  CALL BASIS2 (WS,WDS,WCHI,WDCHI,XF)
  !
  !     COEFFICIENTS OF WEAK FORM TERMS
  CALL CONST1 (KCHI,XC1)
  !----------------------------------------------------------------------
  !L               2.  THE 8 TERMS OF THE WEAK FORM
  !
  DO J=1,8
    IF (.NOT.NLPHAS) THEN
      CALL CONST2 (J,KCHI,XETA,XKSI)
    ELSE
      CALL CONST3 (J,ks,KCHI,XETA,XKSI)
    END IF
    !
    !     TREAT NON QUADRATIC TERMS
    IF (.NOT.NLQUAD) THEN
      !
      !     FORM VECTORS OF COEFFICIENTS XVETA AND XVKSI
      CALL VECT (XETA,XF,XVETA)
      CALL VECT (XKSI,XF,XVKSI)
      !
      !     DIADIC MULTIPLICATION
      CALL DIADIC (6,J,XVETA,XVKSI,XC1,XM)
      !
      !     TREAT QUADRATIC TERMS
    ELSE
      CALL VECT (XKSI,XF,XVKSI)
      CALL DIADIC (6,J,XVKSI,XVKSI,XC1,XM)
    END IF
    !
    !     ADD TO CONA
    CALL ADD (6,XM,CONA)
    !
  END DO
  !-----------------------------------------------------------------------
  !L                 3.  MULTIPLY BY AREA OF INTERVAL
  !
  DO JI=1,6
    DO JJ=1,6
      CONA(JI,JJ) = WDS * WDCHI * CONA(JI,JJ)
    END DO
  END DO
  !
  RETURN
END SUBROUTINE AHYBRD
!CC*DECK P4C3S04
SUBROUTINE AWAY (K,L,KR)
  !        ---------------
  !
  !  4.3.04. REMOVE A COLUMN AND A ROW OF A LOCAL MATRIX
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
  IMPLICIT NONE
  INTEGER :: K, L, KR, J
  !--------------------------------------------------------------------
  !
  DO J=1,L
    CONA(KR,J ) = CMPLX(0._RKIND,0._RKIND)
    CONA(J ,KR) = CMPLX(0._RKIND,0._RKIND)
  END DO
  !
  CONA(KR,KR) = CMPLX(1._RKIND,0._RKIND)
  !
  RETURN
END SUBROUTINE AWAY
!CC*DECK P4C3S05
SUBROUTINE STORE (K,PWEIT)
  !        ----------------
  !
  !     ADDS CELL CONTRIBUTION (CONA) TO THE BLOCK (A)
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMMTR ! INCLUDE 'COMMTR.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, NCON, JROW, ILM1, JCOL, JCON
  REAL(RKIND) :: PWEIT
  !--------------------------------------------------------------------
  !
  NCON = 6
  !
  !     SUM UP CONTRIBUTIONS TO ELEMENTS OF A
  !
  DO JROW=1,NCON
    IF (NPLAC(JROW) .NE. 0) THEN
      ILM1 = NCOLMN * (NPLAC(JROW)-1)
      DO JCOL=1,NCON
        IF (NPLAC(JCOL) .NE. 0) THEN
          JCON = ILM1 + NPLAC(JCOL)
          !
          A1D(JCON) = A1D(JCON) + CONA(JROW,JCOL) * PWEIT
          !
        END IF
      END DO
    END IF
  END DO
  !
  RETURN
END SUBROUTINE STORE
!CC*DECK P4C4S01
SUBROUTINE QUAEQU (KS, KCHI)
  !        -----------------
  !
  !  4.4.01. PHYSICAL AND GEOMETRICAL LOCAL (CELL) QUANTITIES, INCLUDING
  !          EVALUATION OF THE DIELECTRIC TENSOR (EQ.3.19.D OF REF.1).
  !          INCLUDES HARMONIC ION-CYCLOTRON AND ELECTRON LANDAU + TTMP.
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !----------------------------------------------------------------------
  INTEGER :: KS, KCHI, JSPEC, J, JHARM
  COMPLEX &
       &   ZFC(5),   ZI,       ZROM2,    ZTRA,     ZX,       ZZ1,  ZZM1
  REAL(RKIND) :: ZBJ(101), ZSQPI, ZTE, ZVTHE, ZCON, ZKPAR, ZKAPPA, DENSIT, &
       & ZARGE, ZZE, ZTI, ZVTHI, ZARG1, ZARGM1, ZTPRP, ZVTPRP, ZARGB, &
       & ZARG2, ZZ2, ZKPERP, TEMPRP, TEMPEL, TEMPI
  !----------------------------------------------------------------------
  !L                 1. INITIALISATIONS
  !
  ZI     = (0._RKIND,1._RKIND)
  ZSQPI  = SQRT (CPI)
  WS     = EQ(5,KCHI,KS)
  WCHI   = EQ(6,KCHI,KS)
  WDS    = EQ(3,KCHI,KS) - EQ(1,KCHI,KS)
  WDCHI  = EQ(4,KCHI,KS) - EQ(2,KCHI,KS)
  WPSI   = WS * WS * CPSRF
  WP     = EQ(8,KCHI,KS) * CPSRF / (QIAXE * GAMMA)
  IF (KS.EQ.1) THEN
    WPAXIS = WP
  END IF
  WT     = EQ(9,KCHI,KS)
  WQ     = EQ(11,KCHI,KS) / QIAXE
  WR2    = EQ(14,KCHI,KS)
  WRHO   = DENSIT (WS)
  !
  WBPOL2 = WPSI * QIAXE / EQ(12,KCHI,KS)
  WGRPS2 = WBPOL2 * WR2
  WBTOR2 = WT * WT / WR2
  WBTOT2 = WBTOR2 + WBPOL2
  WBTOT  = SQRT(WBTOT2)
  !
  WBETCH = EQ(13,KCHI,KS)
  WH     = EQ(17,KCHI,KS)
  WK     = EQ(18,KCHI,KS)
  WTQ    = EQ(19,KCHI,KS)
  WJAC   = WR2 / WTQ
  WDCR2J = EQ(20,KCHI,KS)
  WDPT   = EQ(26,KCHI,KS)
  WJ0PHI = EQ(27,KCHI,KS)
  WDNGP  = EQ(28,KCHI,KS)
  WDPR2J = EQ(29,KCHI,KS)
  !
  !     LOCAL MASS FRACTIONS AND ION CYCLOTRON FREQUENCIES
  !
  CALL FRPROF (WS,WFRAC)
  DO JSPEC = 1,NRSPEC
    WOMCI(JSPEC) = CEOMCI(JSPEC) * WBTOT
  END DO
  !
  !     DIELECTRIC TENSOR COMPONENTS INITIALIZED TO ZERO
  WEPS = (0._RKIND,0._RKIND)
  WG   = (0._RKIND,0._RKIND)
  DO J=1,NRSPEC
    WEPSI(J) = (0._RKIND,0._RKIND)
    WGI(J)   = (0._RKIND,0._RKIND)
  END DO
  WEPSEL = (0._RKIND,0._RKIND)
  !
  !---------------------------------------------------------------------
  !L                 2. COLD IONS PLASMA DIELECTRIC TENSOR
  !
  IF (NLCOLD) THEN
    !
    !     DAMPING ANU = NU/OMEGA
    !
    WCOMEG = OMEGA * CMPLX (1._RKIND,ANU)
    !LV         WCOMEG = OMEGA * CMPLX (1.,ANU/WRHO)

    ZROM2  = WRHO * WCOMEG * WCOMEG
    !
    !     EQ. (3.42) THESIS
    !
    DO J=1,NRSPEC
      ZX = WCOMEG / WOMCI(J)
      ZTRA = 1._RKIND / (1._RKIND - ZX*ZX)
      WEPSI(J) = WFRAC(J) * ZTRA * ZROM2
      WGI(J)   = WFRAC(J) * ZTRA * ZX * ZROM2
      WEPS = WEPS + WEPSI(J)
      WG   = WG   + WGI(J)
    END DO
    !
    !     ADD ELECTRON LANDAU AND TTMP DAMPINGS OF FAST WAVE
    !
    IF (.NOT.NLCOLE) THEN
      !
      ZTE = TEMPEL (WS)
      ZVTHE = SIGMA * SQRT (2._RKIND * ZTE / AMASSE)
      ZCON = 0._RKIND
      !
      DO J=1,NRSPEC
        ZCON = ZCON + ACHARG(J) * WFRAC(J) * AMASSE / AMASS(J)
      END DO
      !
      !     K-PARALLEL APPROXIMATED BY N/R
      ZKPAR = ABS(WNTORO) / SQRT(WR2)
      !
      !     OBTAIN K-PERP FROM DISPERSION RELATION FOR FAST WAVE
      CALL FASTDR (ZKPERP)
      !
      ZKAPPA = WRHO * OMEGA * ZKPERP**2 * ZVTHE * ZCON / &
           &              (2._RKIND*ZKPAR)
      ZARGE = OMEGA / (ZKPAR * ZVTHE)
      ZARGE = ZARGE * ZARGE
      ZZE = ZSQPI * EXP (-ZARGE)
      !
      WEPSEL = ZI * ZKAPPA * ZZE
      !
    END IF
    !
    !---------------------------------------------------------------------
    !L               3. WARM IONS PLASMA DIELECTRIC TENSOR
    !
  ELSE
    !
    !     K-PARALLEL APPROXIMATED BY N/R
    ZKPAR = ABS(WNTORO) / SQRT(WR2)
    !
    !     EQ. (3.44) OF THESIS
    !
    DO J=1,NRSPEC
      !
      ZTI = TEMPI (J,WS)
      ZVTHI = SIGMA * SQRT (2._RKIND*ZTI/AMASS(J))
      ZKAPPA = WFRAC(J) * WOMCI(J)**2 / (2._RKIND * ZKPAR * ZVTHI)
      ZARG1 = (OMEGA - WOMCI(J)) / (ZKPAR * ZVTHI)
      ZARGM1 = (OMEGA + WOMCI(J)) / (ZKPAR * ZVTHI)
      CALL DISPFN (ZARG1,ZFC)
      ZZ1 = ZFC(1)
      CALL DISPFN (ZARGM1,ZFC)
      ZZM1 = ZFC(1)
      !
      WEPSI(J) = WRHO * OMEGA *   ZKAPPA * (ZZ1 + ZZM1)
      WGI(J)   = WRHO * OMEGA * ( ZKAPPA * (ZZ1 - ZZM1) &
           &                                - WFRAC(J) * WOMCI(J) )
      !
      WEPS = WEPS + WEPSI(J)
      WG   = WG   + WGI(J)
      !
    END DO
    !
    !     ADD HARMONIC ION-CYCLOTRON DAMPING OF FAST WAVE
    !
    !     OBTAIN K-PERP FROM DISPERSION RELATION FOR FAST WAVE
    CALL FASTDR (ZKPERP)
    !
    IF (NHARM.GE.2) THEN
      DO J=1,NRSPEC
        ZTI = TEMPI (J,WS)
        ZVTHI = SIGMA * SQRT (2._RKIND*ZTI/AMASS(J))
        ZTPRP = TEMPRP (J,WS)
        ZVTPRP = SIGMA * SQRT (2._RKIND*ZTPRP/AMASS(J))
        ZKAPPA = WFRAC(J) * WOMCI(J)**2 / (ZKPAR * ZVTHI)
        ZKAPPA = ZKAPPA * WRHO * OMEGA
        ZCON = 0._RKIND
        ZARGB = ZKPERP * ZVTPRP**2 / (ZVTHI * WOMCI(J))
        !
        !     BESSEL FUNCTIONS OF ORDER 0 TO NHARM-1, STORED INTO ZBJ()
        CALL BESHHH (NHARM,ZARGB,ZBJ)
        !
        !     LOOP OVER HARMONICS
        DO JHARM=2,NHARM
          ZARG2 = (OMEGA - JHARM*WOMCI(J)) / (ZKPAR * ZVTHI)
          ZARG2 = ZARG2 * ZARG2
          ZZ2 = ZSQPI * EXP (-ZARG2)
          ZCON = ZCON + ZBJ(JHARM)**2 * ZZ2
        END DO
        ZTRA = ZI * ZKAPPA * ZCON
        !
        !     ADD TO DIELECTRIC TENSOR COMPONENTS
        WEPSI(J) = WEPSI(J) + ZTRA
        WGI(J)   = WGI(J)   + ZTRA
        !
        WEPS = WEPS + ZTRA
        WG   = WG   + ZTRA
        !
      END DO
    END IF
    !
    !     ADD ELECTRON LANDAU AND TTMP DAMPINGS
    !
    IF (.NOT.NLCOLE) THEN
      !
      ZTE = TEMPEL (WS)
      ZVTHE = SIGMA * SQRT (2._RKIND * ZTE / AMASSE)
      ZCON = 0._RKIND
      DO J=1,NRSPEC
        ZCON = ZCON + ACHARG(J) * WFRAC(J) * AMASSE / AMASS(J)
      END DO
      ZKAPPA = WRHO * OMEGA * ZKPERP**2 * ZVTHE * ZCON / &
           &              (2._RKIND*ZKPAR)
      ZARGE = OMEGA / (ZKPAR * ZVTHE)
      ZARGE = ZARGE * ZARGE
      ZZE = ZSQPI * EXP (-ZARGE)
      !
      WEPSEL = ZI * ZKAPPA * ZZE
      !
    END IF
    !
  END IF
  !
  !---------------------------------------------------------------------
  !L               4. ADD PHENOMENOLOGICAL DAMPING (MODE CONVERSION)
  !
  WEPSMC = 2._RKIND * ZI * DELTA * WRHO * OMEGA * OMEGA
  !
  RETURN
END SUBROUTINE QUAEQU
!CC*DECK P4C4S02
SUBROUTINE BASIS2(PX,ZDX,PY,ZDY,PF)
  !        -----------------
  !
  !  4.4.02. BASIS FUNCTIONS OF LINEAR HYBRID ELEMENTS AS GIVEN
  !          IN EQ.(3.48) OF REF.(1)
  !------------------------------------------------------------------
  !
  !     DEFINITION OF BASIS FUNCTIONS PF
  !
  !     POINT  DXDC    X   DXDS  DVDC   V
  !
  !       1     1      2    3
  !       2     4      5    6
  !       3                       7     8
  !       4                       9    10
  !       5     11    12    13
  !       6     14    15    16
  !
  !     DEFINITION OF POSITION OF THE POINTS
  !
  !         2---4---6
  !         /   /   /
  !         /   /   /
  !         /   /   /
  !         1---3---5
  !
  !-------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  REAL(RKIND) :: PX, ZDX, PY, ZDY, PF(16)
  !-------------------------------------------------------------------
  !
  !     ALL FUNCTION VALUES
  !
  PF(2)=0.25_RKIND
  PF(5)=0.25_RKIND
  PF(8)=0.5_RKIND
  PF(10)=0.5_RKIND
  PF(12)=0.25_RKIND
  PF(15)=0.25_RKIND
  !
  !     ALL CHI-DERIVATIVES
  !
  PF(1)=-0.5_RKIND/ZDY
  PF(4)=-PF(1)
  PF(7)=-1.0_RKIND/ZDY
  PF(9)=-PF(7)
  PF(11)=PF(1)
  PF(14)=PF(4)
  !
  !     ALL S-DERIVATIVES
  !
  PF(3)=-0.5_RKIND/ZDX
  PF(6)=PF(3)
  PF(13)=-PF(3)
  PF(16)=PF(13)
  !
  RETURN
END SUBROUTINE BASIS2
!CC*DECK P4C4S03
SUBROUTINE CONST1 (KCHI,PC1)
  !        -----------------
  !
  !  4.4.03. DETERMINES COEFFICIENTS OF WEAK FORM TERMS
  !          ACCORDING TO EQ.(3.19C) OF REF.(1)
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  INTEGER :: KCHI
  COMPLEX   PC1,     ZI
  DIMENSION PC1(8)
  !-------------------------------------------------------------------
  !
  !     TERMS NR. 1 TO 4
  !
  ZI = CMPLX(0._RKIND,1._RKIND)
  PC1(1) = - 2._RKIND * CPSRF * WS * WTQ * (WEPS + WEPSEL + WEPSMC) &
       &            / WBPOL2
  PC1(2) = - WJAC * WGRPS2 * WT * WT * (WEPS + WEPSMC) &
       &            / (2._RKIND * CPSRF * WS * WBTOT2)
  PC1(3) = - WT * WR2 * ZI * (WG + WEPSMC) / WBTOT
  PC1(4) = - PC1(3)
  !
  !     TERMS NR. 5 TO 8,TAKEN FROM IDEAL MHD TERMS NR.1,2,3,5 (HERA 3.16)
  !
  PC1(5) = 2._RKIND * WPSI * WTQ / (WJAC * WJAC * WBPOL2 * WS)
  PC1(6) = WT * WT * WTQ / (2._RKIND * WS * CPSRF)
  PC1(7) = PC1(6) * WBPOL2 / WBTOR2
  PC1(8) = -2._RKIND * WR2 * WTQ * WK / (WS * QIAXE)
  !
  RETURN
END SUBROUTINE CONST1
!CC*DECK P4C4S04
SUBROUTINE CONST2 (K,KCHI,PETA,PKSI)
  !        -----------------
  !
  !  4.4.04. DETERMINES CONSTANTS FOR BILIN.COMBINATIONS OF X,V,X*,V*
  !          AND DERIVATIVES,(I_j and J_j, j=1..8, Eq.(3.40) of Ref.[4])
  !>>>       MODIFIED FOR GENERAL JACOBIAN.
  !---------------------------------------------------------------------
  !
  !     THE CONSTANTS ARE ARRANGED IN THE FOLLOWING WAY :
  !
  !            DXDCHI    X     DXDS    DVDCHI    V
  !              1       2       3       4       5
  !
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, KCHI
  DIMENSION   PETA(5),   PKSI(5)
  COMPLEX     PETA,      PKSI
  !---------------------------------------------------------------------
  !
  !     N.B. NLQUAD IS TRUE IF THE TERM OF THE WEAK FORM IS QUADRATIC
  !
  NLQUAD =.TRUE.
  CALL RESETR (PETA,10,0.0_RKIND)
  CALL RESETR (PKSI,10,0.0_RKIND)
  !
  GO TO (100,200,300,400,500,600,700,800,900) , K
  !---------------------------------------------------------------------
  !
  !     FIRST TERM OF THE WEAK FORM
100 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     SECOND TERM
200 CONTINUE
  PKSI(5) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(-WBETCH,0._RKIND)
  RETURN
  !
  !     THIRD TERM
300 CONTINUE
  NLQUAD = .FALSE.
  PETA(2) = CMPLX(1._RKIND,0._RKIND)
  PKSI(5) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     FOURTH TERM
400 CONTINUE
  NLQUAD = .FALSE.
  PETA(5) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     FIFTH TERM
500 CONTINUE
  PKSI(1) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(WDCR2J,WNTORO*WT/WTQ)
  RETURN
  !
  !     SIXTH TERM
600 CONTINUE
  PKSI(3) = CMPLX(1._RKIND,0._RKIND)
  PKSI(4) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     SEVENTH TERM
700 CONTINUE
  PKSI(1) = CMPLX(WBETCH,0._RKIND)
  PKSI(2) = CMPLX( WH + WBETCH*WDCR2J, WNTORO*WBETCH*WT/WTQ )
  PKSI(3) = CMPLX(1._RKIND,0._RKIND)
  PKSI(5) = CMPLX(0._RKIND,-WNTORO*WT/WTQ)
  RETURN
  !
  !     EIGHTH TERM
800 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     RIGHT-HAND SIDE FROM VOLUME ANTENNA CURRENTS
900 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !--------------------------------------------------------------------
END SUBROUTINE CONST2
!CC*DECK P4C4S04
SUBROUTINE CONST3 (K,ks,KCHI,PETA,PKSI)
  !        -----------------
  !
  !  4.4.04. DETERMINES CONSTANTS FOR BILIN.COMBINATIONS OF X,V,X*,V*
  !          AND DERIVATIVES,(I_j and J_j, j=1..8, Eq.(3.39) of Ref.[4])
  !>>>       MODIFIED FOR GENERAL JACOBIAN.
  !>>>       MODIFIED TO INCLUDE THE OPTION NLPHAS.
  !---------------------------------------------------------------------
  !
  !     THE CONSTANTS ARE ARRANGED IN THE FOLLOWING WAY :
  !
  !            DXDCHI    X     DXDS    DVDCHI    V
  !              1       2       3       4       5
  !
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, KCHI, ks
  DIMENSION   PETA(5),   PKSI(5)
  COMPLEX     PETA,      PKSI
  !---------------------------------------------------------------------
  !
  !     N.B. NLQUAD IS TRUE IF THE TERM OF THE WEAK FORM IS QUADRATIC
  !
  NLQUAD =.TRUE.
  CALL RESETR (PETA,10,0.0_RKIND)
  CALL RESETR (PKSI,10,0.0_RKIND)
  !
  GO TO (100,200,300,400,500,600,700,800,900) , K
  !---------------------------------------------------------------------
  !
  !     FIRST TERM OF THE WEAK FORM
100 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     SECOND TERM
200 CONTINUE
  PKSI(5) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(-WBETCH,0._RKIND)
  RETURN
  !
  !     THIRD TERM
300 CONTINUE
  NLQUAD = .FALSE.
  PETA(2) = CMPLX(1._RKIND,0._RKIND)
  PKSI(5) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     FOURTH TERM
400 CONTINUE
  NLQUAD = .FALSE.
  PETA(5) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     FIFTH TERM
500 CONTINUE
  PKSI(1) = CMPLX(1._RKIND,0._RKIND)
  PKSI(2) = CMPLX(WDCR2J,0._RKIND)
  RETURN
  !
  !     SIXTH TERM
600 CONTINUE
  PKSI(2) = CMPLX(0._RKIND,-WNTORO*EQ(21,KCHI,ks))
  PKSI(3) = CMPLX(1._RKIND,0._RKIND)
  PKSI(4) = CMPLX(1._RKIND,0._RKIND)
  PKSI(5) = CMPLX(0._RKIND,-WNTORO*WT/WTQ)
  RETURN
  !
  !     SEVENTH TERM
700 CONTINUE
  PKSI(1) = CMPLX(WBETCH,0._RKIND)
  PKSI(2) = CMPLX(WH+ WBETCH*WDCR2J, -WNTORO*EQ(21,KCHI,ks))
  PKSI(3) = CMPLX(1._RKIND,0._RKIND)
  PKSI(5) = CMPLX(0._RKIND,-WNTORO*WT/WTQ)
  RETURN
  !
  !     EIGHTH TERM
800 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !
  !     RIGHT-HAND SIDE FROM VOLUME ANTENNA CURRENTS
900 CONTINUE
  PKSI(2) = CMPLX(1._RKIND,0._RKIND)
  RETURN
  !--------------------------------------------------------------------
END SUBROUTINE CONST3
!CC*DECK P4C4S05
SUBROUTINE VECT (PC,PF,PV)
  !        ---------------
  !
  !  4.4.05. DETERMINES VECTOR OF COEFFICIENTS OF UNKNOWNS IN A MESH CELL
  !          THE LOCAL NUMBERING CORRESPONDS TO :
  !
  !                  2---4---6
  !                  /   /   /
  !                  /   /   /
  !                  1---3---5
  !
  !----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PF(16)
  COMPLEX     PC(5),      PV(6)
  !----------------------------------------------------------------------
  !
  !     X COMPONENT,POINTS 1 AND 2
  !
  PV(1) = PC(1)*PF(1) + PC(2)*PF(2) + PC(3)*PF(3)
  PV(2) = PC(1)*PF(4) + PC(2)*PF(5) + PC(3)*PF(6)
  !
  !     V COMPONENT,POINTS 3 AND 4
  !
  PV(3) = PC(4)*PF(7) + PC(5)*PF(8)
  PV(4) = PC(4)*PF(9) + PC(5)*PF(10)
  !
  !     X COMPONENT,POINTS 5 AND 6
  !
  PV(5) = PC(1)*PF(11) + PC(2)*PF(12) + PC(3)*PF(13)
  PV(6) = PC(1)*PF(14) + PC(2)*PF(15) + PC(3)*PF(16)
  !
  RETURN
END SUBROUTINE VECT
!CC*DECK P4C4S06
SUBROUTINE DIADIC (KV,KI,PV,PW,PC1,PM)
  !        -----------------
  !
  !  4.4.06. DIADIC MULTIPLICATION  PM = PC1 * CONJG(PV) * PW
  !----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KV, KI, JI, JJ
  REAL(RKIND) :: PF(16)
  COMPLEX     PC1(8),      PV(6),       PM(6,6),       PW(6)
  !----------------------------------------------------------------------
  !
  DO JI=1,KV
    DO JJ=1,KV
      PM(JI,JJ) = PC1(KI) * CONJG(PV(JI)) * PW(JJ)
    END DO
  END DO
  !
  RETURN
END SUBROUTINE DIADIC
!CC*DECK P4C4S07
SUBROUTINE ADD (KV,PM,PCON)
  !        --------------
  !
  !  4.4.07. ADDS CONTRIBUTION OF ONE TERM TO THE CONTRIBUTION OF THE CELL
  !----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KV, JI, JJ
  COMPLEX     PM(6,6),   PCON(6,6)
  !----------------------------------------------------------------------
  !
  DO JI=1,KV
    DO JJ=1,KV
      PCON(JI,JJ) = PCON(JI,JJ) + PM(JI,JJ)
    END DO
  END DO
  !
  RETURN
END SUBROUTINE ADD
!CC*DECK P4C5S01
SUBROUTINE FRPROF (PS,PFRAC)
  !        -----------------
  !
  !  4.5.01. DETERMINES LOCAL MASS DENSITY FRACTIONS OF ALL ION SPECIES
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !
  INTEGER :: JSPEC
  REAL(RKIND) :: PS, PFRAC(10), FRSHAP
  !----------------------------------------------------------------------
  !
  PFRAC(1) = 1.0_RKIND
  IF (NRSPEC.EQ.1) RETURN
  !
  DO JSPEC = 2,NRSPEC
    PFRAC(JSPEC) = FRAC(JSPEC) * FRSHAP (PS)
    PFRAC(1) = PFRAC(1) - PFRAC(JSPEC)
  END DO
  !
  IF (PFRAC(1).GE.0.0_RKIND) RETURN
  !
  !     BAD PROFILES MAKE THE FRACTION OF MAJORITY SPECIES NEGATIVE
  !
  PRINT 1000,PFRAC(1)
  STOP
1000 FORMAT(///,' INVALID MASS FRACTION PROFILE',1PE14.4)
  !
END SUBROUTINE FRPROF
!CC*DECK P4C5S02
FUNCTION FRSHAP (PS)
  !        ---------------
  !
  !  4.5.02. SHAPE OF FRACTIONNAL DENSITY PROFILE
  !------------------------------------------------------------
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: FRSHAP, PS
  FRSHAP = 1.0_RKIND
  RETURN
END FUNCTION FRSHAP
!CC*DECK P4C5S03
FUNCTION DAMPIN (PS,PCHI)
  !        ---------------
  !
  !  4.5.03. DAMPING
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  !------------------------------------------------------------------
  IMPLICIT NONE
  !
  REAL(RKIND) :: PS, PCHI, DAMPIN
  !
  DAMPIN = ANU
  RETURN
END FUNCTION DAMPIN
!CC*DECK P4C5S04
FUNCTION TEMPI (K,PS)
  !        --------------
  !
  !  4.5.04. PARALLEL TEMPERATURE OF K-TH ION SPECIES AT RADIUS PS
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  !-------------------------------------------------------------------
  IMPLICIT NONE
  !
  INTEGER :: K, JP
  REAL(RKIND) :: PS, TEMPI, ZARG, ZD
  !
  !L                 -2. PROFILE shape PROPORTIONAL TO SQRT(PRESSURE)
  !
  IF (NTEMP.EQ.-2) THEN
    !
    TEMPI = CENTI(K) * SQRT(WP/WPAXIS)
    !
    !-------------------------------------------------------------------
    !L                 -1. POLYNOMIAL FUNCTION
    !
  ELSE IF (NTEMP.EQ.-1) THEN
    !
    !     FUNCTION OF S**2
    IF (NDARG.EQ.1) THEN
      ZARG = PS*PS
      !
      !     FUNCTION OF S
    ELSE IF (NDARG.EQ.2) THEN
      ZARG = PS
    END IF
    !
    ZD = 1.0_RKIND
    DO JP = 1, NDDEG
      ZD = ZD + ATI(JP) * ZARG ** JP
    END DO
    TEMPI = CENTI(K) * ZD
    !
    !-------------------------------------------------------------------
    !L               ELSE. PROFILE GIVEN BY EQTI(K), EQKAPT(K)
  ELSE
    !
    ZARG = 1._RKIND - EQTI(K) * PS * PS
    TEMPI = CENTI(K) * ZARG ** EQKAPT(K)
    !
  END IF
  !
  RETURN
END FUNCTION TEMPI
!CC*DECK P4C5S05
FUNCTION TEMPRP (K,PS)
  !        ---------------
  !
  !  4.5.05. PERPENDICULAR TEMPERATURE OF K-TH ION SPECIES AT RADIUS PS
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  !-------------------------------------------------------------------
  IMPLICIT NONE
  !
  INTEGER :: K, JP
  REAL(RKIND) :: PS, TEMPRP, ZARG, ZD
  !
  !L                 -2. PROFILE shape PROPORTIONAL TO SQRT(PRESSURE)
  !
  IF (NTEMP.EQ.-2) THEN
    !
    TEMPRP = CENTIP(K) * SQRT(WP/WPAXIS)
    !
    !-------------------------------------------------------------------
    !L                 -1. POLYNOMIAL FUNCTION
    !
  ELSE IF (NTEMP.EQ.-1) THEN
    !
    !     FUNCTION OF S**2
    IF (NDARG.EQ.1) THEN
      ZARG = PS*PS
      !
      !     FUNCTION OF S
    ELSE IF (NDARG.EQ.2) THEN
      ZARG = PS
    END IF
    !
    ZD = 1.0_RKIND
    DO JP = 1, NDDEG
      ZD = ZD + ATIP(JP) * ZARG ** JP
    END DO
    TEMPRP = CENTIP(K) * ZD
    !
    !------------------------------------------------------------------
    !L               ELSE. PROFILE shape GIVEN BY EQTI(K), EQKAPT(K)
  ELSE
    !
    ZARG = 1 - EQTI(K) * PS * PS
    TEMPRP = CENTIP(K) * ZARG ** EQKAPT(K)
    !
  END IF
  !
  RETURN
END FUNCTION TEMPRP
!CC*DECK P4C5S06
FUNCTION TEMPEL (PS)
  !        ---------------
  !
  !  4.5.06. ELECTRON TEMPERATURE AT RADIUS PS
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  !-------------------------------------------------------------------
  IMPLICIT NONE
  !
  INTEGER :: JP
  REAL(RKIND) :: PS, TEMPEL, ZARG, ZD
  !
  !L                 -2. PROFILE shape PROPORTIONAL TO SQRT(PRESSURE)
  !
  IF (NTEMP.EQ.-2) THEN
    !
    TEMPEL = CENTE * SQRT(WP/WPAXIS)
    !
    !-------------------------------------------------------------------
    !L                 -1. POLYNOMIAL FUNCTION
    !
  ELSE IF (NTEMP.EQ.-1) THEN
    !
    !     FUNCTION OF S**2
    IF (NDARG.EQ.1) THEN
      ZARG = PS*PS
      !
      !     FUNCTION OF S
    ELSE IF (NDARG.EQ.2) THEN
      ZARG = PS
    END IF
    !
    ZD = 1.0_RKIND
    DO JP = 1, NDDEG
      ZD = ZD + ATE(JP) * ZARG ** JP
    END DO
    TEMPEL = CENTE * ZD
    !
    !------------------------------------------------------------------
    !L               ELSE. PROFILE shape GIVEN BY EQTI(K), EQKAPT(K)
  ELSE
    !
    ZARG = 1 - EQTE * PS * PS
    TEMPEL = CENTE * ZARG ** EQKPTE
    !
  END IF
  !
  RETURN
END FUNCTION TEMPEL
!CC*DECK P4C5S07
SUBROUTINE FASTDR (PKPERP)
  !        -----------------
  !
  !  4.5.07. K-PERP OF FAST WAVE FROM DISPERSION RELATION. EQ.52-53 OF
  !          KURT'S VARENNA PAPER, WITH KY = 0 AND KZ = N/R.
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: PKPERP, ZKZSQ, ZKF2
  COMPLEX   ZEPS,    ZG
  !------------------------------------------------------------------
  ZKZSQ = WNTORO * WNTORO / WR2
  ZEPS = WEPS / WBTOT2
  ZG   = WG   / WBTOT2
  ZKF2 = CABS ( ZEPS - ZKZSQ - ZG*ZG / (ZEPS - ZKZSQ) )
  PKPERP = SQRT (ZKF2)
  RETURN
END SUBROUTINE FASTDR
!CC*DECK P4C5S08                
SUBROUTINE DISPFN(PARG,PFN)
  !        ---------------------------
  !
  !  4.5.08. FRIED-CONTE FOR REAL ARGUMENT (NRL-FORMULARY: Z(ZETA))
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: J
  REAL(RKIND) :: PARG
  COMPLEX     PFN(5),      ZARG,     ZER(5),      ZI,       ZCOEF
  !-----------------------------------------------------------------------
  IF(ABS(PARG).LT.1.E-20_RKIND) PARG=1.E-20_RKIND
  !-----------------------------------------------------------------------
  !     WE USE TROYON'S SUBROUTINE WHICH CALCULATES COMPLEX ERROR FUNCTION
  !     FUNCTIONS. HERE WE GET THE FRIED AND CONTE FUNCTION TOGETHER WITH
  !     4 DERIVATIVES
  !
  !
  ZI=CMPLX(0._RKIND,1._RKIND)
  ZCOEF=(1._RKIND,0._RKIND)
  ZARG=CMPLX(-PARG,0._RKIND)
  CALL ERRFC(ZARG,ZER)
  DO J=1,5
    PFN(J)=ZI*ZCOEF*ZER(J)
    ZCOEF=ZCOEF*2._RKIND*ZI
  END DO
  RETURN
  !
END SUBROUTINE DISPFN
!CC*DECK P4C5S09
SUBROUTINE ERRFC(PZ,F)
  !       -----------------------
  !
  !  4.5.09. COMPUTATION OF ERROR FUNCTION IN COMPLEX PLANE
  !
  !     AUTHOR:  F. TROYON
  !
  !      *****************************************************************
  !      *******************                    *************************
  !      ******************** A T T E N T I O N *************************
  !      *******************                    *************************
  !      *****************************************************************
  !
  !      ERRFC NE CALCULE PAS LA FONCTION DE FRIED CONTE
  !      **********************************************
  !
  !      POUR OBTENIR LA FONCTION DE FRIED CONTE Z(S) ET SA DERIVEE ZP(S)
  !      IL FAUT FAIRE=
  !              PZ=-S
  !              CALL ERRFC(PZ,F)
  !      ALORS ON A =
  !              Z(S)=F(1)*(0.,1.)
  !              ZP(S)=-2.*F(2)
  !
  !     THE SO DEFINED FUNCTION Z(S) IS THE FUNCTION Z(ZETA)
  !     DEFINED IN THE NRL-FORMULARY
  !
  !      *****************************************************************
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: IETA, N, I, J
  REAL(RKIND) :: A(15,4), X, Y, ZMOD, SPI, FLI, G
  COMPLEX Z, AUX1, AUX2, V, B(30), FB(4), F(5), YC, BQ, FBC, XP, D, C(25), U, PZ
  !
  X=REAL(PZ,RKIND)
  Y=AIMAG(PZ)
  !         HALF  PLANE  DETERMINATION
  IF (Y)  1,1,2
1 IETA=1
  GO TO 3
2 X=-X
  Y=-Y
  IETA=2
3 Z=CMPLX((X**2-Y**2),(2*X*Y))
  ZMOD=CABS(Z)
  SPI=1.7724538509007_RKIND
  !       BRANCHING  TO  CONTINUOUS  FRACTION  (100)
  IF  (Y+1.5_RKIND)  100,100,4
4 V=1._RKIND/Z
  !       BRANCHING  TO  ASYMPTOTIC(110)  OR BESSEL
  IF (ZMOD - 26._RKIND)  5,110,110
  !       BESSEL  EXPANSION BRANCH
  !       NUMBER  OF  TERMS  IN  EXPANSION
5 IF ( ABS(REAL(Z)) - 1.E-8_RKIND) 6,7,7
6 IF ( ABS(AIMAG(Z)) -1.E-8_RKIND) 8,7,7
8 Z=CMPLX(1.E-8_RKIND,AIMAG(Z))
7 IF(ZMOD-2.E-4_RKIND)  21,21,22
21 N=2
  GO TO 120
22 IF(ZMOD-2.E-3_RKIND)  9,9,10
9 N=4
  GO TO  120
10 IF (ZMOD - .4_RKIND)  11,11,12
11 N=6
  GO TO 120
12 IF(ZMOD-1._RKIND)  13,13,14
13 N=8
  GO TO 120
14 IF (ZMOD - 2.5_RKIND) 15,15,16
15 N = 10
  GO TO 120
16 IF (ZMOD - 7._RKIND)  17,17,18
17 N=15
  GO TO 120
18 IF(ZMOD-14._RKIND)   19,19,20
19 N=20
  GO TO 120
20 N=29
120 CALL BESSEL (X,Y,N,F,Z,V,B)
  GO TO 35
  !       ASYMPTOTIC  EXPANSION
  !       RANGE  OF  N
110 IF(ZMOD-55._RKIND)  25,26,26
25 N=14
  GO TO 29
26 IF (ZMOD-100._RKIND) 27,28,28
27 N=10
  GO TO 29
28 N=7
29 AUX1=1._RKIND/(CMPLX(X,Y))
  !       COMPUTATION  OF  ALL  TERMS  IN  EXPANSION
  B(1)=1._RKIND/Z
  A(1,1)=.5_RKIND
  A(1,2)=.5_RKIND
  A(1,3)=.75_RKIND
  A(1,4)=1.5_RKIND
  DO I=2,N
    FLI=REAL(I,RKIND)
    A(I,1)=A(I-1,1)*(FLI-.5_RKIND)
    A(I,2)=A(I,1)*FLI
    A(I,3)=A(I,2)*(FLI+.5_RKIND)
    A(I,4)=A(I,3)*(FLI+1._RKIND)
    B(I)=V*B(I-1)
  END DO
  !        COMPUTATION  OF  THE  SUMS  FB
  DO I=1,4
    FB(I)=(0._RKIND,0._RKIND)
  END DO
  DO J=1,4
    DO I=1,N
      FB(J)=FB(J)+A(I,J)*B(I)
    END DO
  END DO
  !       COMPUTATION  OF F
  F(1)=-(0._RKIND,1.)*AUX1*(FB(1)+1._RKIND)
  F(2)=-FB(1)
  F(3)=(0._RKIND,1.)*AUX1*FB(2)
  F(4)=V*FB(3)
  F(5)=-(0._RKIND,1.)*V*AUX1*FB(4)
  GO TO 35
  !        CONTINUOUS  FRACTION
  !        COMPUTATION  OF Y
100 G=1._RKIND
  C(1)=(0._RKIND,0._RKIND)
  C(2)=(1._RKIND,0._RKIND)
  B(1)=(1._RKIND,0._RKIND)
  B(2)=-Z+4.5_RKIND
  YC=1._RKIND/B(2)
  DO I=2,24
    FLI=REAL(I,RKIND)+1._RKIND
    BQ=-Z+.5_RKIND+2._RKIND*FLI
    G=-.5_RKIND*FLI*(2._RKIND*FLI-1._RKIND)
    C(I+1)=BQ*C(I)+G*C(I-1)
    B(I+1)=BQ*B(I)+G*B(I-1)
    FBC=C(I+1)/B(I+1)
    U=(FBC-YC)/YC
    IF ( (ABS(REAL(U)) .LT. 1.E-6_RKIND) .AND. (ABS(AIMAG(U)) .LT. 1.E-6_RKIND) ) GO TO 34
    YC=FBC
  END DO
  !       COMPUTATION  OF  X  AND  D
34 AUX2=-Z+2.5_RKIND-3*YC
  XP=1._RKIND/AUX2
  D=-Z+.5_RKIND-.5*XP
  !          COMPUTATION  OF  F
  F(1)= CMPLX(0._RKIND,1._RKIND)*(CMPLX(X,Y))/D
  F(2)=.5_RKIND*((1._RKIND-XP)/D)
  F(3)=.5_RKIND*F(1)*XP
  F(4)=.75_RKIND*XP*((1._RKIND-YC)/D)
  F(5)=3*F(3)*YC
  !       CORRECTION  FOR  OTHER HALF  PLANE
35 GO TO (37,36),IETA
36 IF (CABS(Z).GT.100) GO TO 500
  X=-X
  Y=-Y
  B(1)=2._RKIND*SPI* EXP(-REAL(Z))*CMPLX(COS(AIMAG(Z)),-SIN(AIMAG(Z)))
  B(2)=-CMPLX(0._RKIND,1._RKIND)*CMPLX(X,Y)*B(1)
  DO I=3,5
    B(I)=.5_RKIND*REAL(I-2,RKIND)*B(I-2)-CMPLX(0._RKIND,1._RKIND)*CMPLX(X,Y)*B(I-1)
  END DO
  GO TO 300
500 CONTINUE
  DO I=1,5
    B(I)=0
  END DO
300 CONTINUE
  DO I=1,5
    F(I)=B(I)+(-1._RKIND)**I*F(I)
  END DO
37 RETURN
END SUBROUTINE ERRFC
!CC*DECK P4C5S10
SUBROUTINE BESSEL(X,Y,N,F,Z,V,B)
  !        --------------------------------
  !
  !  4.5.10. BESSEL ROUTINE FOR ERRFC
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: N, IMAX, I, K
  REAL(RKIND) :: X, Y, SPI
  COMPLEX :: SUM, SN, B(30), F(5), Z, AUX3, AUX4, V
  !-----------------------------------------------------------------------
  !       COMPUTATION OF BESSEL FUNCTIONS
  SPI=1.7724538509007_RKIND
  V=1._RKIND/Z
  Z=CMPLX((X**2-Y**2),(2*X*Y))
  B(N+1)=(0._RKIND,0._RKIND)
  B(N)=(1._RKIND,0._RKIND)
  IMAX=N-1
  DO I=1,IMAX
    K=N-I
    B(K)=B(K+2)-REAL(4*K+2,RKIND)*V*B(K+1)
  END DO
  !       COMPUTATION OF FIRST BESSEL FUNCTION
  AUX3=EXP(-REAL(Z))*CMPLX(COS(AIMAG(Z)),-SIN(AIMAG(Z)))
  !       COMPUTATION  OF  SCALING  FACTOR  S
  SN=(1._RKIND-AUX3)/B(1)
  !           COMPUTATION  OF  SUM
  SUM=(0._RKIND,0._RKIND)
  DO I=1,N
    SUM=SUM+B(I)
  END DO
  AUX4=SUM*SN
  !         COMPUTATION OF F(I)
  F(1)=SPI*AUX3-(2._RKIND /CMPLX(X,Y))*CMPLX(0._RKIND,1._RKIND)*AUX4
  F(2)=1._RKIND+CMPLX(X,Y)*F(1) *CMPLX(0._RKIND,-1._RKIND)
  DO I=3,5
    F(I)=F(I-2)*REAL(I-2,RKIND)*.5_RKIND+CMPLX(X,Y)*F(I-1)*CMPLX(0._RKIND,-1._RKIND)
  END DO
  RETURN
END SUBROUTINE BESSEL
!CC*DECK P4C5S11
SUBROUTINE BESHHH (LMAX,X,BJ)
  !        _________________
  !
  !  4.5.11. CALCULATE BESSEL FUNCTIONS BY BACKWARD RECURSION
  !
  !     ON RETURN: BJ(1)=J0(X), BJ(2)=J1(X), ... BJ(LMAX+1)=JLMAX(X)
  !-------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: MSTART, LMAX, L, LSTART
  REAL(RKIND) :: XMIN, XMAX, DEL, X, TS
  !
  PARAMETER (XMIN=.000001_RKIND,XMAX=50._RKIND,MSTART=101,DEL=1.E-60_RKIND)
  REAL(RKIND) :: TEMP(MSTART), BJ(LMAX+1)
  !
  IF (ABS(X).GT.XMAX) STOP 'BESSEL: ARGUMENT TOO BIG'
  IF ( LMAX .GT.50  ) STOP 'BESSEL: ORDER TOO BIG'
  IF (ABS(X).GT.XMIN) GOTO 20
  BJ(1)=1._RKIND
  IF (LMAX.EQ.0) RETURN
  DO L=1,LMAX
    BJ(L+1)=X/REAL(2*L, RKIND)*BJ(L)
  END DO
  RETURN
  !
20 LSTART=2*(5*INT(ABS(X))+(LMAX+1)/2+10)+1
  !.....ORDER OF HIGHEST BESSEL FUNCTION IS EVEN TO GUARANTEE CORRECT
  !.....PARITY (JN(X) = (-1)**N *JN(-X) )
  IF (LSTART.GT.MSTART) LSTART=MSTART
  TEMP(LSTART)=DEL
  TEMP(LSTART-1)=DEL*REAL(2*(LSTART-1))/X
  TS=TEMP(LSTART)**2
  DO L=LSTART-1,2,-1
    TEMP(L-1)=REAL(2*(L-1))/X*TEMP(L)-TEMP(L+1)
    TS=TS+TEMP(L)**2
  END DO
  TS=1._RKIND/SQRT(TEMP(1)**2+2._RKIND*TS)
  DO L=1,LMAX+1
    BJ(L)=TS*TEMP(L)
  END DO
  RETURN
END SUBROUTINE BESHHH
!CC*DECK P4C5S12
SUBROUTINE CVZERO (K,PX,KX)
  !        -----------------
  !
  !  4.5.12. ZEROES A COMPLEX VECTOR
  !-------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: K, KX, J, IM1
  COMPLEX :: PX(KX*(K-1)+1)
  !
  IF (K.LE.1) RETURN
  IM1 = K-1
  DO J=0,IM1
    PX(J*KX+1) = CMPLX(0._RKIND,0._RKIND)
  END DO
  RETURN
END SUBROUTINE CVZERO
!CC*DECK P4C5S13
FUNCTION DENSIT (PS)
  !        ---------------
  !
  !  4.5.13. TOTAL MASS DENSITY AT RADIUS PS, NORMALIZED TO 1. AT AXIS
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  INTEGER :: JP
  REAL(RKIND) :: PS, DENSIT, ZARG, ZD, ZC, ZKSI, ZALFA, ZT0
  !-------------------------------------------------------------------
  !L                 -2. PROFILE shape PROPORTIONAL TO SQRT(PRESSURE)
  !
  IF (NDENS.EQ.-2) THEN
    !
    DENSIT = SQRT (WP/WPAXIS)
    !
    !-------------------------------------------------------------------
    !L                 -1. POLYNOMIAL FUNCTION
    !
  ELSE IF (NDENS.EQ.-1) THEN
    !
    !     FUNCTION OF S**2
    IF (NDARG.EQ.1) THEN
      ZARG = PS*PS
      !
      !     FUNCTION OF S
    ELSE IF (NDARG.EQ.2) THEN
      ZARG = PS
    END IF
    !
    ZD = 1.0_RKIND
    DO JP = 1, NDDEG
      ZD = ZD + AD(JP) * ZARG ** JP
    END DO
    DENSIT = ZD
    !
    !-------------------------------------------------------------------
    !L                 0. PARABOLIC PROFILE ** EQKAPD
    !
  ELSE IF (NDENS.EQ.0) THEN
    !
    DENSIT = (1._RKIND - EQDENS*PS*PS) ** EQKAPD
    !
    !-------------------------------------------------------------------
    !L                 1. CONSTANT FOR S<EQDENS, COS**2 FOR S>EQDENS
    !                     EDGE VALUE = EQKAPD AND FLAT EDGE GRADIENT
    !
  ELSE IF (NDENS.EQ.1) THEN
    !
    IF (PS.LE.EQDENS) THEN
      DENSIT = 1._RKIND
    ELSE
      ZC = 3.1415926535898_RKIND * 0.5_RKIND / (1._RKIND-EQDENS)
      DENSIT = EQKAPD + (1._RKIND-EQKAPD) * &
           &                  COS( ZC*(PS-EQDENS) ) **2
    END IF
    !
    !-------------------------------------------------------------------
    !L                 2. CONSTANT FOR S<EQDENS, CUBIC FOR S>EQDENS
    !                     EDGE VALUE = EQKAPD, EDGE GRADIENT = EQALFD
    !
  ELSE IF (NDENS.EQ.2) THEN
    !
    IF (PS.LE.EQDENS) THEN
      DENSIT = 1._RKIND
    ELSE
      ZKSI = (PS - EQDENS) / (1._RKIND - EQDENS)
      ZALFA = EQALFD * (1._RKIND - EQDENS)
      DENSIT = ( ZALFA + 2._RKIND * (1._RKIND - EQKAPD) ) * ZKSI**3 &
           &                - ( ZALFA + 3._RKIND * (1._RKIND - EQKAPD) ) * ZKSI**2 &
           &                + 1._RKIND
    END IF
    !
    !-------------------------------------------------------------------
    !L		3. T^2/q^2 * q0^2/T0^2 (OMEGAXTAE = CONSTANT)
    !
  ELSE IF (NDENS.EQ.3) THEN
    !
    ZT0 = 1.0_RKIND
    !
    DENSIT = ( WT / (WQ * QIAXE * ZT0) )**2
    !
    !-------------------------------------------------------------------
  END IF
  !-------------------------------------------------------------------
  RETURN
END FUNCTION DENSIT
!CC*DECK P4C6S02
SUBROUTINE IODSK4 (K)
  !        -----------------
  !
  !  4.6.02. DISK I/O FOR LION4
  !--------------------------------------------------------------
  USE GLOBALS
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
!!$  USE COMMTR ! INCLUDE 'COMMTR.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, I2, I3, I4, IN2, IEND, IS, J
  REAL(RKIND) :: ZRTRA(MDPOL), ZR, ZQ, ZT
  !
  INCLUDE 'NEWRUN.inc'
  !---------------------------------------------------------------
  !
  GO TO (100,200,300,400,500,600,700,800,900,1000,1100),K
  !
  !---------------------------------------------------------------
  !L             1. OPEN FILES
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
100 CONTINUE
  !lv         NSAVE = 8
  !lv         OPEN (UNIT=NSAVE,FILE='TAPE8',STATUS='OLD',
  !lv     +              FORM='FORMATTED')
  !lv         REWIND NSAVE
  !lv         READ (NSAVE,NEWRUN)
  !lv         IF (NLOTP4(1)) THEN
  !lv            WRITE (NPRNT,NEWRUN)
  !lv         END IF
  !
  !lv         OPEN (UNIT=MEQ,FILE='TAPE4',STATUS='OLD',
  !lv     +         FORM='UNFORMATTED')
  OPEN (UNIT=NSOURC,FILE='TAPE15',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  OPEN (UNIT=NDS,FILE='TAPE14',STATUS='NEW', &
       &              FORM='UNFORMATTED')
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    OPEN (UNIT=NDLT,FILE='TAPE10',STATUS='NEW', &
         &            FORM='UNFORMATTED')
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    OPEN (UNIT=I2,FILE='TAPE20',STATUS='NEW',FORM='UNFORMATTED')
    OPEN (UNIT=I3,FILE='TAPE30',STATUS='NEW',FORM='UNFORMATTED')
    OPEN (UNIT=I4,FILE='TAPE40',STATUS='NEW',FORM='UNFORMATTED')
    !
  END IF
  !
  RETURN
  !
  !---------------------------------------------------------------------
  !L             2. READ VACUUM MATRIX. PUT IT INTO VAC.
  !
200 CONTINUE
  IN2 = 2*NCHI-2
  IEND = IN2*IN2
  REWIND NSOURC
  READ (NSOURC) IS,(VAC(J),J=1,IS)
  READ (NSOURC) IS,ZR,ZQ,ZT,(ZRTRA(J),J=1,IS), &
       &               (ZRTRA(J),J=1,IS),(VAC(J),J=1,IS)
  READ (NSOURC) (VAC(J),J=1,IEND)
  !
  RETURN
  !
  !---------------------------------------------------------------
  !L             3. WRITE DECOMPOSED BLOCK
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
300 CONTINUE
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    !     WRITE THE 2*NPOL FIRST LINES OF A ON NDLT
    !
    !CCCC    WRITE (NDLT) (A(J), J=1, 2*NLONG/3)
    !C>>>
    !C>>>  IN THIS VERSION
    !C>>>  WE STORE THE 2*NPOL FIRST LINES OF MATRIX A,
    !C>>>  I.E. THE 2*NLONG/3 FIRST ELEMENTS OF A.
    !C>>>  THE I/O OPERATIONS ARE SPLIT IN 4 ON 4 SEPARATE UNITS
    !
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    !
    WRITE (NDLT) (A1D(J), J=          1,   NLONG/6)
    WRITE (  I2) (A1D(J), J=  NLONG/6+1, 2*NLONG/6)
    WRITE (  I3) (A1D(J), J=2*NLONG/6+1, 3*NLONG/6)
    WRITE (  I4) (A1D(J), J=3*NLONG/6+1, 4*NLONG/6)
    !
    !C         BUFFER OUT (NDLT,0) (RA(1),          RA(2*NLONG/6))
    !C         BUFFER OUT (  I2,0) (RA(2*NLONG/6+1),RA(4*NLONG/6))
    !C         BUFFER OUT (  I3,0) (RA(4*NLONG/6+1),RA(6*NLONG/6))
    !C         BUFFER OUT (  I4,0) (RA(6*NLONG/6+1),RA(8*NLONG/6))
    !
    !C         Z = UNIT(NDLT)
    !C         IF (Z) 301,2000,2000
    !C  301    Z = UNIT(I2)
    !C         IF (Z) 302,2000,2000
    !C  302    Z = UNIT(I3)
    !C         IF (Z) 303,2000,2000
    !C  303    Z = UNIT(I4)
    !C         IF (Z) 304,2000,2000
    !C  304    CONTINUE
    !
    !>>>
    !>>>  IN THIS VERSION WE STORE THE 2*NPOL FIRST LINES OF A ON
    !>>>  DISK UNIT NDLT. WE USE VECTORIZED I/O. AIO CONTAINS THE
    !>>>  2*MDPOL*3*MDPOL FIRST ELEMENTS OF A.
    !>>>
    !>>>         WRITE (NDLT) AIO
    !>>>
    !
  END IF
  !
  RETURN
  !
  !-----------------------------------------------------------------
  !L             4. READ EQUILIBRIUM EQ(.,.) QUANTITIES
  !
400 CONTINUE
  !lv         READ (MEQ)((EQ(I,J),I=1,MDEQ),J=1,NPOL)
  RETURN
  !
  !---------------------------------------------------------------
  !L             5. READ BACKWARDS DECOMPOSED BLOCK
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
500 CONTINUE
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    !     READ 2*NPOL FIRST LINES OF MATRIX BLOCK (SEE IODSK4(3))
    !
    !CCCC	 BACKSPACE NDLT
    !CCCC	 READ (NDLT) (A(J), J=1, 2*NLONG/3)
    !CCCC	 BACKSPACE NDLT
    !
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    !
    BACKSPACE NDLT
    BACKSPACE I2
    BACKSPACE I3
    BACKSPACE I4
    !
    READ (NDLT) (A1D(J), J=          1,   NLONG/6)
    READ (  I2) (A1D(J), J=  NLONG/6+1, 2*NLONG/6)
    READ (  I3) (A1D(J), J=2*NLONG/6+1, 3*NLONG/6)
    READ (  I4) (A1D(J), J=3*NLONG/6+1, 4*NLONG/6)
    !
    !C         BUFFER IN (NDLT,0) (RA(1),          RA(2*NLONG/6))
    !C         BUFFER IN (  I2,0) (RA(2*NLONG/6+1),RA(4*NLONG/6))
    !C         BUFFER IN (  I3,0) (RA(4*NLONG/6+1),RA(6*NLONG/6))
    !C         BUFFER IN (  I4,0) (RA(6*NLONG/6+1),RA(8*NLONG/6))
    !
    !C         Z = UNIT(NDLT)
    !C         IF (Z) 501,2000,2000
    !C  501    Z = UNIT(I2)
    !C         IF (Z) 502,2000,2000
    !C  502    Z = UNIT(I3)
    !C         IF (Z) 503,2000,2000
    !C  503    Z = UNIT(I4)
    !C         IF (Z) 504,2000,2000
    !C  504    CONTINUE
    !
    BACKSPACE NDLT
    BACKSPACE I2
    BACKSPACE I3
    BACKSPACE I4
    !
    !>>>  IN THIS VERSION WE USE VECTORIZED I/O ON UNIT NDLT ONLY
    !>>>  AIO CONTAINS THE 2*MDPOL*3*MDPOL FIRST ELEMENTS OF A
    !>>>  SEE IODSK4 (3)
    !>>>
    !>>>         BACKSPACE NDLT
    !>>>         READ (NDLT) AIO
    !>>>         BACKSPACE NDLT
    !>>>
    !
  END IF
  !
  RETURN
  !
  !---------------------------------------------------------------
  !L             6. READ SOURCE VECTOR FROM LION2. PUT IT INTO XT
  !		  AND TO SOURCT
  !
600 CONTINUE
  REWIND NSOURC
  READ (NSOURC) IS, (X(J),J=1,IS)
  CALL CVZERO (NCOMP, XT(1), 1)
  CALL CCOPY  (IS, X(1), 1, XT(NCOMP-IS+1), 1)
  CALL CVZERO (NCOMP, SOURCT(1), 1)
  CALL CCOPY  (IS, X(1), 1, SOURCT(NCOMP-IS+1), 1)
  CALL CVZERO (IS, X(1), 1)
  RETURN
  !
  !---------------------------------------------------------------
  !L             7. WRITE THE SOLUTION ON NDS
  !
700 CONTINUE
  REWIND NDS
  WRITE (NDS) (XT(J),J=1,NCOMP)
  RETURN
  !
  !---------------------------------------------------------------
  !L             8. READ SOURCE VECTOR,REACTANCE SCALAR,OHM-VECTOR
  !                 AND VACUUM MATRIX
  !
800 CONTINUE
  REWIND NSOURC
  READ (NSOURC) IN2,(U(J),J=1,IN2)
  READ (NSOURC) IS,REACS,RQB,RTB,(XDCHI(J),J=1,IS), &
       &               (XDTH(J),J=1,IS),(XOHMR(J),J=1,IS)
  IEND = IN2*IN2
  READ (NSOURC) (VAC(J),J=1,IEND)
  RETURN
  !
  !---------------------------------------------------------------
  !L             9. REWIND DISK FILES
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
900 CONTINUE
  !
  !lv         REWIND MEQ
  REWIND NDS
  !lv         REWIND NSAVE
  REWIND NSOURC
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    REWIND NDLT
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    REWIND I2
    REWIND I3
    REWIND I4
    !
  END IF
  !
  RETURN
  !
  !-----------------------------------------------------------------
  !L            10. CLOSE DISK FILES
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
1000 CONTINUE
  !
  !lv         CLOSE (MEQ)
  !lv         CLOSE (NSAVE)
  CLOSE (NSOURC)
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    CLOSE (NDLT,STATUS='DELETE')
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    CLOSE (I2,STATUS='DELETE')
    CLOSE (I3,STATUS='DELETE')
    CLOSE (I4,STATUS='DELETE')
    !
  END IF
  !
  !     KEEP NDS ONLY IF NLDISO=T, OTHERWISE DELETE IT
  !
  IF (NLDISO) THEN
    CLOSE (NDS)
  ELSE
    CLOSE (NDS,STATUS='DELETE')
  END IF
  !
  RETURN
  !
  !--------------------------------------------------------------
  !L	      11. BACKSPACE SCRATCH FILE(S)
  !
  !***************************************************************
  !* 							       *
  !*   IMPORTANT NOTICE: IODSK4 (1), (3), (5), (9), (10), (11)   *
  !*   MUST BE DEFINED CONSISTENTLY			       *
  !*							       *
  !***************************************************************
  !
1100 CONTINUE
  !
  !     USE SCRATCH FILES FOR MATRIX STORAGE ONLY IF NLDISO=T
  !
  IF (NLDISO) THEN
    !
    BACKSPACE NDLT
    I2 = NDLT+10
    I3 = NDLT+20
    I4 = NDLT+30
    BACKSPACE I2
    BACKSPACE I3
    BACKSPACE I4
    !
  END IF
  !
  RETURN
  !
  !--------------------------------------------------------------
  !L            20. DISK ERROR
  !
2000 CONTINUE
  PRINT *,'EROR OS: SHOULD NOT BE HERE SINCE Z NOT DEFINED, comment next line'
  STOP 'OS error iodsk4'
  !OS WRITE (NPRNT,9020) K,Z
9020 FORMAT (/,' ******** DISK ERROR IN IODSK4 (',I2,') UNIT=', &
       &           1PE10.2,///)
  STOP
END SUBROUTINE IODSK4
!CC*DECK P5C1S01
SUBROUTINE DIAGNO
  !        -----------------
  !
  !  5.1.1. ORGANIZE THE DIAGNOSTICS
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: J
  !--------------------------------------------------------------------
  !
  !     DIAGNOSTICS ARE NOT PERFORMED IF NLDISO=F
  !
  IF (NLDISO) THEN
    !
    !     INITIALIZE
    !
    CALL INIT
    !
    !     CONSTRUCT DESIRED PHYSICAL QUANTITIES
    !
    CALL PHYSQ
    !
    !     LINE-PRINTER OUTPUT
    !
    DO J=2,40
      IF (NLOTP5(J)) CALL OUTP5(J)
    END DO
    !
    !     GRAPHICAL OUTPUT
    !
    DO J=2,15
      IF (NLPLO5(J)) CALL PLOT5(J)
    END DO
    !
    !     TERMINATE THE RUN
    !
    CALL THEEND
    !
  END IF
  !
  RETURN
END SUBROUTINE DIAGNO
!CC*DECK P5C2S01
SUBROUTINE INIT
  !        ---------------
  !
  !  5.2.1. INITIALISATIONS
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: JS, J, JM, ISPC2, JC
  !----------------------------------------------------------------------
  !
  !     DEFAULT VALUES
  ALARG = 20._RKIND
  AHEIGT = 25._RKIND
  ARSIZE = 0.8_RKIND
  ASYMB = 1.2_RKIND
  ANGLET(1) = 0._RKIND
  NCUT = 1
  NCONTR = 9
  NDES = 16
  ISPC2 = NRSPEC + 2
  !
  !lvC     READ/WRITE NAMELIST
  !lv         NSAVE = 8
  !lv         REWIND NSAVE
  !
  ! Open Disk Files
  CALL IODSK5(7)
  !
  !     REWIND DISKS
  CALL IODSK5(1)
  !
  !     READ (R,Z) AND SURFACE
  CALL IODSK5(4)
  !
  !     READ SOLUTION VECTOR
  CALL IODSK5(2)
  !
  !     PUT COUNTERS TO ZERO
  CMEAN  = 0.0_RKIND
  CDEVIA = 0.0_RKIND
  ELEPOW = 0.0_RKIND
  SMEAN  = 0.0_RKIND
  SDEVIA = 0.0_RKIND
  !
  DO JS=1,NPSI
    ABSPOW(JS) = 0.0_RKIND
    DO J = 1, ISPC2
      ABSPSP(JS,J) = 0.0_RKIND
    END DO
    FLUPOW(JS) = 0.0_RKIND
    SFLUX(JS)  = 0.0_RKIND
    SURPSI(JS) = 0.0_RKIND
  END DO
  !
  FLUPOW(NPSI+1) = 0.0_RKIND
  !
  DO JC=1,NPOL
    CHIPOW(JC)=0.0_RKIND
  END DO
  !
  DO JS=1,NPSI
    DO JM=1,MD2FP1
      FREN(JM,JS) = CMPLX(0._RKIND,0.)
      FREP(JM,JS) = CMPLX(0._RKIND,0.)
    END DO
  END DO
  !
  !     INITIALIZE THE PLOT
  IF (NLPLO5(1)) THEN
    CALL PLOT5(1)
  ELSE
    DO J=2,15
      NLPLO5(J) = .FALSE.
    END DO
  END IF
  !
  RETURN
END SUBROUTINE INIT
!CC*DECK P5C2S02
SUBROUTINE PHYSQ
  !        ----------------
  !
  !  5.2.2. CONSTRUCT DESIRED PHYSICAL QUANTITIES FROM SOLUTION VECTOR
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: J, ISPC2, JS, JCHI, JSPEC, JM
  REAL(RKIND) :: ZTRA(MDSPC2),    ZFLPSP(MDSPC2), ZFLPW, ZOMCI, ZSL, ZSU, &
       & ZFLCON, ZZ, ZOMCH, ZFAZ, ZMHD2, ZNE0, ZHALFP, ZDCHI
  !-----------------------------------------------------------------------
  !
  !     INITIALIZE THE POWER FLUXES AT EDGES OF S=CONST. CELLS.
  !     ZFLPW = TOTAL FLUX. ZFLPSP() = FLUXES PER SPECIES.
  ISPC2 = NRSPEC + 2
  ZFLPW = 0._RKIND
  DO J=1,ISPC2
    ZFLPSP(J) = 0._RKIND
  END DO
  !
  !--------------------------------------------------------------------
  !L            LOOP ON MAGNETIC SURFACES ("RADIAL") INDEX JS
  !--------------------------------------------------------------------
  !
  DO JS=1,NPSI
    !lvC
    !lvC     READ EQUILIBRIUM QUANTITIES
    !lv         CALL IODSK5(3)
    !
    !     COUNTERS FOR ABSORPTION PER SPECIES
    DO J=1,ISPC2
      ZTRA(J) = 0._RKIND
    END DO
    !
    !-------------------------------------------------------------------
    !              LOOP ON POLOIDAL DIRECTION INDEX JCHI
    !-------------------------------------------------------------------
    DO JCHI=1,NPOL
      !
      !     CONSTRUCT LOCAL (CELL) QUANTITIES
      CALL QUAEQU (JS,JCHI)
      CALL CENTER (JS,JCHI)
      CALL ELECTR
      CALL LOCPOW
      CALL MAGNET
      CALL POYNTI
      !
      !       STORE THE NECESSARY INFORMATION IN ARRAYS
      BN(JS,JCHI)     = BNL
      BB(JS,JCHI)     = BPL
      BPAR(JS,JCHI)   = BPARL
      EN(JS,JCHI)     = ENL
      EP(JS,JCHI)     = EPL
      DENPOW(JS,JCHI) = DPL
      CELPOW(JS,JCHI) = CPL
      SN(JS,JCHI)     = SNL
      SPERP(JS,JCHI)  = SPL
      SPAR(JS,JCHI)   = SPARL
      DSPREL(JS,JCHI) = REAL(WEPS) - WNTORO**2/WR2
      AIMEPS(JS,JCHI) = AIMAG(WEPS)
      COST(JS,JCHI)   = SQRT (WBTOR2) / WBTOT
      SINT(JS,JCHI)   = SQRT (WBPOL2) / WBTOT
      !
      ZOMCI = OMEGA - WOMCI(1)
      DO JSPEC=2,NRSPEC
        IF (WOMCI(JSPEC).NE.WOMCI(JSPEC-1)) THEN
          ZOMCI = ZOMCI * (OMEGA - WOMCI(JSPEC))
        END IF
      END DO
      OMCLIN(JS,JCHI) = ZOMCI
      !
      !     SURFACE-INTEGRATED POWER ABSORPTION
      ABSPOW(JS) = ABSPOW(JS) + CPL/WDS
      DO J = 1, ISPC2
        ABSPSP(JS,J) = ABSPSP(JS,J) + CPLJ(J)/WDS
      END DO
      CHIPOW(JCHI) = CHIPOW(JCHI) + CPL/WDCHI
      !
      !     INTEGRATE POYNTING FLUX THROUGH MAGNETIC SURFACE
      CALL SFLINT (JS)
      !
      !     INTEGRATE D (VOLUME INSIDE MAGN.SURF.) / D (PSI)
      SURPSI(JS) = SURPSI(JS) + WJAC*WDCHI*C2PI
      !
      !     ABSORPTION PER SPECIES (SUM FOR PSI=CONST. CELLS)
      DO J=1,ISPC2
        ZTRA(J) = ZTRA(J) + CPLJ(J)
      END DO
      !
      CCHI(JCHI) = WCHI
      !
    END DO
    !-------------------------------------------------------------------
    !                  END OF JCHI LOOP
    !-------------------------------------------------------------------
    !
    !     PSI=CONST. DIAGNOSTICS
    !
    CCS(JS) = WS
    ZSL = EQ(1,1,js)
    ZSU = EQ(3,1,js)
    !
    !     POWER FLUXES AT CENTER OF THE CELLS
    ZFLCON = ABSPOW(JS) * WDS
    ZZ = (WS*WS - ZSL*ZSL) / (ZSU*ZSU - ZSL*ZSL)
    FLUPOW(JS) = ZFLPW + ZFLCON * ZZ
    DO J=1,ISPC2
      FLUPSP(JS,J) = ZFLPSP(J) + ZTRA(J) * ZZ
    END DO
    !
    !     POWER FLUXES AT EDGE OF CELLS
    ZFLPW  = ZFLPW + ZFLCON
    DO J=1,ISPC2
      ZFLPSP(J) = ZFLPSP(J) + ZTRA(J)
    END DO
    !
    !     AMPLITUDE AND PHASE OF ELECTRIC FIELD ON OUTER EQUATORIAL
    !     MIDPLANE (CHI=0) (ASSUMED TO CORRESPOND TO JCHI=1)
    !
    ENCHI0(JS) = CABS ( EN(JS,1) )
    EPCHI0(JS) = CABS ( EP(JS,1) )
    IF ( REAL(EN(JS,1)) .NE. 0._RKIND ) THEN
      ENPHI0(JS) = ATAN2 ( AIMAG(EN(JS,1)), REAL(EN(JS,1)) )
    ELSE
      ENPHI0(JS) = CPI * 0.5_RKIND
    END IF
    IF ( REAL(EP(JS,1)) .NE. 0._RKIND ) THEN
      EPPHI0(JS) = ATAN2 ( AIMAG(EP(JS,1)), REAL(EP(JS,1)) )
    ELSE
      EPPHI0(JS) = CPI * 0.5_RKIND
    END IF
    !
    !     NORMALIZED ALFVEN CONTINUUM FREQUENCIES FOR M = MFL .. MFU
    !     FOR OMEGA << OMEGA_CI (2ND ORDER) , IN THE APPROXIMATION OF NO
    !     POLOIDAL MODE COUPLING. ASSUMES T_axis=1.0.
    !
    ZOMCH = WOMCI(1) * AMASS(1) / ACHARG(1)
    ZFAZ  = 0.0_RKIND
    !
    DO JSPEC = 1, NRSPEC
      ZFAZ = ZFAZ + WFRAC(JSPEC) * &
           &            ( AMASS(JSPEC) / ACHARG(JSPEC) )**2
    END DO
    !
    DO JM = MFL, MFU
      ZMHD2 = ( WT * (WNTORO + JM/WQ) )**2 / WRHO
      FRALF(JM-MFL+1,JS) = &
           &         SQRT ( ZMHD2 / (1._RKIND + ZFAZ * ZMHD2 / ZOMCH**2 ) )
    END DO
    !
    !     MAJOR AND MINOR RADII, MASS AND ELECTRON DENSITIES, SAFETY 
    !     FACTOR, CENTER OF TAE GAP VS S ON LOW FIELD SIDE EQUATORIAL 
    !     PLANE (CHI=0)
    !     
    EQRDEN(1,JS) = CCR(JS,1)
    EQRDEN(2,JS) = ( CCR(JS,1) - 1._RKIND ) / ( XS(1) - 1._RKIND)
    EQRDEN(3,JS) = CCR(JS,1) * RMAJOR
    EQRDEN(4,JS) = ( CCR(JS,1) - 1._RKIND ) * RMAJOR
    EQRDEN(5,JS) = WRHO
    EQRDEN(6,JS) = WQ
    EQRDEN(7,JS) = WT / (2.0_RKIND*WQ*SQRT(WRHO))
    ZNE0 = 0._RKIND
    DO JSPEC = 1, NRSPEC
      ZNE0 = ZNE0 + CENDEN(JSPEC) * ACHARG(JSPEC)
    END DO
    EQRDEN(8,JS) = WRHO * ZNE0
    !
  END DO
  !---------------------------------------------------------------------
  !                  END OF JS LOOP
  !---------------------------------------------------------------------
  !
  !     POLOIDAL FOURIER DECOMPOSITION OF ELECTRIC FIELD
  !
  CALL FOURIE
  !
  !     ELECTRON,ION LANDAU+TTMP DAMPING OF ALFVEN WAVE
  !     AND FAST PARTICLE DKE DRIVE
  !
  IF (NLFAST) THEN
    CALL LANDAU
  END IF
  !
  !     FLUX AT PLASMA SURFACE
  !
  FLUPOW(NPSI+1) = ZFLPW
  DO J=1,ISPC2
    FLUPSP(NPSI+1,J) = ZFLPSP(J)
  END DO
  !
  !     SMEAN = RADIUS OF HALF POWER ABSORBED
  !
  ZHALFP = FLUPOW(NPSI+1) * 0.5_RKIND
  DO JS=1,NPSI
    IF (FLUPOW(JS) .GE. ZHALFP) THEN
      SMEAN = (CCS(JS)-CCS(JS-1)) * (ZHALFP-FLUPOW(JS-1)) / &
           &              (FLUPOW(JS)-FLUPOW(JS-1))  +  CCS(JS-1)
      GO TO 1101
    END IF
  END DO
1101 CONTINUE
  !
  !     POWER ABSORPTION DENSITY AVERAGED OVER PSI=CONST SURFACE
  !     MULTIPLY WITH TOTAL POWER (WATTS) TO OBTAIN WATTS/M**3
  !
  DO JS=1,NPSI
    DEPOS(JS) = ABSPOW(JS) / (SURPSI(JS) &
         &       * RMAJOR**3 * FLUPOW(NPSI+1) * 2._RKIND * CPSRF * CCS(JS))
    DO J = 1, ISPC2
      DEPPSP(JS,J) = ABSPSP(JS,J) / (SURPSI(JS) &
           &        * RMAJOR**3 * FLUPOW(NPSI+1) * 2._RKIND * CPSRF * CCS(JS))
    END DO

  END DO
  !     
  !     CMEAN = MEAN ANGLE CHI OF ABSORBED POWER
  !     CDEVIA = CHI WIDTH OF POWER ABSORPTION
  !
  DO JCHI=1,NPOL
    ZDCHI = ABS( EQ(4,JCHI,1) - EQ(2,JCHI,1) )
    CMEAN =  CMEAN  + CCHI(JCHI)    * CHIPOW(JCHI) * ZDCHI
    CDEVIA = CDEVIA + CCHI(JCHI)**2 * CHIPOW(JCHI) * ZDCHI
  END DO
  !
  CMEAN = CMEAN / FLUPOW(NPSI)
  CDEVIA = CDEVIA / FLUPOW(NPSI)
  !CCCC    CDEVIA = SQRT(CDEVIA-CMEAN**2)
  !
  !-------------------------------------------------------------------
  !
  RETURN
END SUBROUTINE PHYSQ
!CC*DECK P5C2S04
SUBROUTINE FOURIE
  !        -----------------
  !
  !  5.2.4. COMPLEX FOURIER ANALYSIS IN CHI AND THETA
  !
  !     REVISION 7.2.96 LDV CRPP : ADD FOURIER ANALYSIS FOR B
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: JC, JM, JS, JT
  REAL(RKIND) :: ZTHETA(MD2CP2), ZCHI, ZDCHI, ZDT
  COMPLEX :: CDOTU,    ZEIM
  !
  !----------------------------------------------------------------------
  !L.         1. FOURIER ANALYSIS IN CHI ANGLE
  !
  !
  !     COMPUTE EXP(-I*M*CHI(JC)) * DELTACHI(JC) / 2PI FOR ALL POLOIDAL
  !     CENTERS OF INTERVALS (JC=1..NPOL) AND FOR M = MFL .. MFU
  !
  DO JC=1,NPOL
    !
    ZCHI = EQ(6,JC,1)
    ZDCHI = EQ(4,JC,1) - EQ(2,JC,1)
    !
    DO JM = MFL, MFU
      !
      FRINT(JC,JM-MFL+1) = &
           &         CMPLX ( COS (-JM*ZCHI), SIN(-JM*ZCHI) ) * ZDCHI / C2PI
      !
    END DO
  END DO
  !
  !     COMPUTE THE FOURIER AMPLITUDES OF E-NORMAL, FREN(M,S), 
  !     AND OF E-PERP, FREP(M,S) FOR ALL RADIAL CENTERS OF INTERVALS
  !     (JS=1..NPSI) AND M = MFL .. MFU
  !
  DO JS=1,NPSI
    !
    DO JM = MFL, MFU
      !
      FREN(JM-MFL+1,JS) = &
           &         CDOTU (NPOL, FRINT(1,JM-MFL+1), 1, EN(JS,1), MDPSI1)
      !
      FREP(JM-MFL+1,JS) = &
           &         CDOTU (NPOL, FRINT(1,JM-MFL+1), 1, EP(JS,1), MDPSI1)
      !
      FRBN(JM-MFL+1,JS) = &
           &         CDOTU (NPOL, FRINT(1,JM-MFL+1), 1, BN(JS,1), MDPSI1)
      !
      FRBB(JM-MFL+1,JS) = &
           &         CDOTU (NPOL, FRINT(1,JM-MFL+1), 1, BB(JS,1), MDPSI1)
      !
      FRBPAR(JM-MFL+1,JS) = &
           &         CDOTU (NPOL, FRINT(1,JM-MFL+1), 1, BPAR(JS,1), MDPSI1)
      !
    END DO
  END DO
  !
  !---------------------------------------------------------------------
  !L.            2.  FOURIER ANALYSIS IN THETA ANGLE
  !
  !
  !     CLEAR ARRAYS FREN2 AND FREP2
  CALL CVZERO (MD2FP1*NPSI, FREN2(1,1), 1)
  CALL CVZERO (MD2FP1*NPSI, FREP2(1,1), 1)
  !
  !     LOOP ON PSI=CONST. SURFACES
  DO JS=1,NPSI
    !
    !     FIND THETA ANGLES OF THE CENTERS OF CELLS
    DO JT=1,NPOL
      ZTHETA(JT) = ATAN2 (CCZ(JS,JT), CCR(JS,JT) - 1.0_RKIND)
    END DO
    !
    !     DEFINE THETA FROM 0 TO 2PI EXCEPT FIRST INTERVAL
    DO JT=2,NPOL
      IF (ZTHETA(JT).LT.0._RKIND) ZTHETA(JT) = ZTHETA(JT) + C2PI
    END DO
    ZTHETA(NPOL+1) = ZTHETA(1) + C2PI
    !
    !     LOOP ON POLOIDAL INTERVALS
    DO JT=1,NPOL
      !
      !     FIND DELTA-THETA OF THE CELL USING A LINEAR INTERPOLATION
      IF (JT.EQ.1) THEN
        ZDT = ( ZTHETA(2) - (ZTHETA(NPOL)-C2PI) ) * 0.5_RKIND
      ELSE
        ZDT = ( ZTHETA(JT+1) - ZTHETA(JT-1) ) * 0.5_RKIND
      END IF
      !
      !     ADD CONTIBUTIONS OF THE INTERVAL TO FOURIER COEFFICIENTS
      DO JM = MFL, MFU
        !
        ZEIM = CMPLX ( COS (-JM*ZTHETA(JT)), &
             &                            SIN (-JM*ZTHETA(JT)) ) * ZDT / C2PI
        !
        FREN2(JM-MFL+1,JS) = &
             &             FREN2(JM-MFL+1,JS) + EN(JS,JT) * ZEIM
        !
        FREP2(JM-MFL+1,JS) = &
             &             FREP2(JM-MFL+1,JS) + EP(JS,JT) * ZEIM
        !
        FRBN2(JM-MFL+1,JS) = &
             &             FRBN2(JM-MFL+1,JS) + BN(JS,JT) * ZEIM
        !
        FRBB2(JM-MFL+1,JS) = &
             &             FRBB2(JM-MFL+1,JS) + BB(JS,JT) * ZEIM
        !
        FRBPA2(JM-MFL+1,JS) = &
             &             FRBPA2(JM-MFL+1,JS) + BPAR(JS,JT) * ZEIM
        !
      END DO
    END DO
  END DO
  !
  RETURN
END SUBROUTINE FOURIE
!CC*DECK P5C2S06
SUBROUTINE LANDAU
  !        -----------------
  !
  !  5.2.6.-ELECTRON LANDAU DAMPING OF ALFVEN WAVE (TAE/EAE/GAE),
  !	  DUE TO E_PARALLEL AND DUE TO DRIFT,
  !	  REF NUCL.FUS. REVIEW (VACLAVIK), VARENNA 92 (VILLARD),
  !	  NUCL. FUS. 92 (VILLARD, FU).
  !	 -ELECTRON, ION AND FAST PARTICLE COLLISIONLESS LANDAU
  !	  DAMPING (E_PARALLEL AND DRIFT) & TTMP & FAST PARTICLE DRIVE
  !	  DUE TO DRIFTS. FROM DRIFT-KINETIC MODEL, REF. BRUNNER-
  !	  VACLAVIK (BV).
  !        -SINGLE BULK ION SPECIES AND OMEGA << OMEGACI IS ASSUMED.
  !	 -FAST PARTICLES ASSUME A SLOWING-DOWN DISTRIBUTION FUNCTION.
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
  IMPLICIT NONE
  !
  INTEGER :: JF, JCASE, ISMAX, JS, JC, ICM, ICP, JSPEC, J
  REAL(RKIND) :: TEMPEL, TEMPI, DENSIT
  REAL(RKIND) :: ZCENDS, ZBNOTS, ZCENTE, ZCENTI, ZCEOMC, ZFACT, ZSQPI, ZDAMPI, &
       & ZDAMPA, ZDAMPB, ZWENER, ZWETOT, ZNUMER, ZDENOM, ZFACTR, ZBFAV, ZBEAV, &
       & ZBIAV, ZBDEN, ZMDEN, ZALFV0, ZEPS0, ZINTUP, ZINTU2, ZINTU3, ZINTU4, ZINTU5, &
       & ZINTU6, ZINTDO, ZINTD3, ZINTD4, ZINTD5, ZINTD6, ZZJDC, ZDCM, ZDCP, ZTE, ZTI, &
       & ZNI, ZNE, ZNF, ZVC, ZVCM, ZVCP, ZC, ZCM, ZCP, ZOMPE2, ZOMPI2, ZOMPF2, ZNFM, &
       & ZNFP, ZOMPFM, ZOMPFP, ZKP, ZOMEGA, ZVPH, ZVBIRT, ZZZ, ZEZ0E, ZEZ0I, ZARG, &
       & ZTFAST, ZBFL, ZBEL, ZBIL, ZTFAS0, ZBF0, ZBE0, ZBI0, ZSQRIN, ZDSM, ZDSP, ZG0, &
       & ZG1, ZG2, ZALFV, ZVTESI, ZVTEN, ZVAVTE, ZOMKVT, ZGAMMA, ZCS, ZRL, ZDV, ZHP, &
       & ZHM, ZGRPSI, ZGRAD2, ZGRADA, ZGREPS, ZOMCI, ZOMCE, ZOMCF, ZNORM, ZR, ZBETPN, &
       & ZBETPB, ZBETNP, ZBETBP, ZAELE2, ZAION2, ZAALL2, ZSHAT, ZSHALF, ZRAT, ZBFCR, ZBF0CR
  REAL(RKIND) :: &
       &	ZCDELD(MDPSI), ZDENCE(MDPSI), ZDENKE(MDPSI), ZDENKT(MDPSI), &
       &	ZKELDR(MDPSI), ZNUMCE(MDPSI), ZNUMKE(MDPSI), &
       &  ZKPARA(MDPSI), ZKPAR2(MDPSI), ZKPAR3(MDPSI), &
       &	ZKPAR4(MDPSI), ZKPAR5(MDPSI), ZKPAR6(MDPSI), &
       &	ZOMVA(MDPSI),  ZINT(3), ZINTM(3), ZINTP(3), &
       &	ZPHELE(MDPSI), ZPHION(MDPSI), ZPHFAS(MDPSI), ZPINHF(MDPSI), &
       &	ZFHELE(MDPSI), ZFHION(MDPSI), ZFHFAS(MDPSI), ZFINHF(MDPSI), &
       &	ZFBALA(MDPSI), ZNFCRI(MDPSI), ZJDC(MDPSI), &
       &  ZVTE(MDPSI),   ZVTI(MDPSI)
  COMPLEX ZFM, ZF0, ZFP, ZDDS, ZDDC, ZDPSI, ZDPSIA, ZDPERP, ZI, &
       &	       ZDCEN, ZDCEB, ZBGEN, ZBGEB, ZBGV, ZBGX, ZFC(5), ZZE, &
       &	       ZDBDS, ZDBDC, ZGNEN, ZGNEB, ZGBEB, ZDIVEP, ZBPEP, &
       &	       ZBGCEB, ZBPARL, ZGBEN, ZCTRA
  !--------------------------------------------------------------------
  !
  !     SAVE INITIAL DENSITIY, TEMPERATURES, MAGNETIC FIELD AND
  !     LION-NORMALIZED CENTRAL ION CYCLOTRON FREQUENCY ( WHICH IS
  !     PROPORTIONAL TO SQRT(MASS DENSITY) )
  !
  ZCENDS = CENDEN(1)
  ZBNOTS = BNOT
  ZCENTE = CENTE
  ZCENTI = CENTI(1)
  ZCEOMC = CEOMCI(1)
  !
  !--------------------------------------------------------------------
  !     ANALYZE NFAKAP FAST PARTICLE PROFILES WITH EQKAPF(JF)
  !
  DO JF = 1, NFAKAP
    !
    !--------------------------------------------------------------------
    !     CONSTANT BULK BETA SCAN
    !
    DO JCASE = 1, NBCASE
      !
      !     VARY V0/VA0, VARY T0 AND N0 AS 1/T0, KEEP B0 CONSTANT
      IF (NBTYPE.EQ.1) THEN
        CENDEN(1) = CEN0(JCASE)
        CENTE = ZCENTE * ZCENDS / CEN0(JCASE)
        CENTI(1) = ZCENTI * ZCENDS / CEN0(JCASE)
        CEOMCI(1) = ZCEOMC * SQRT (CEN0(JCASE)/ZCENDS)
      END IF
      !
      !     KEEP V0/VA0, VARY B0 AND N0 AS B0**2, KEEP T0 CONSTANT
      IF (NBTYPE.EQ.2) THEN
        CENDEN(1) = CEN0(JCASE)
        BNOT = ZBNOTS * SQRT (CEN0(JCASE)/ZCENDS)
        CEOMCI(1) = ZCEOMC * SQRT (CEN0(JCASE)/ZCENDS)
      END IF
      !
      !lvC     REWIND MEQ
      !lv         CALL IODSK5(1)
      !
      !     INITIALIZE THE INTEGRALS
      ZI     = CMPLX(0._RKIND,1._RKIND)
      ZFACT  = SQRT (CPI) / 2._RKIND
      ZSQPI  = SQRT (CPI)
      ZDAMPI = 0._RKIND
      ZDAMPA = 0._RKIND
      ZDAMPB = 0._RKIND
      ZWENER = 0._RKIND
      ZWETOT = 0._RKIND
      ZNUMER = 0._RKIND
      ZDENOM = 0._RKIND
      ZFACTR = SQRT(8*CPI*AMASSE / AMASS(1))
      ISMAX  = NPSI - 5
      !
      ZBFAV = 0._RKIND
      ZBEAV = 0._RKIND
      ZBIAV = 0._RKIND
      ZBDEN = 0._RKIND
      !
      DO JS = 1, NPSI
        ZFHELE(JS) = 0._RKIND
        ZFHION(JS) = 0._RKIND
        ZFHFAS(JS) = 0._RKIND
        ZFINHF(JS) = 0._RKIND
        ZFBALA(JS) = 0._RKIND
        ZPHELE(JS) = 0._RKIND
        ZPHION(JS) = 0._RKIND
        ZPHFAS(JS) = 0._RKIND
        ZPINHF(JS) = 0._RKIND
        ZNFCRI(JS) = 0._RKIND
      END DO
      !
      !     ALFVEN VELOCITY ON MAGNETIC AXIS IN SI UNITS
      ZMDEN = 0._RKIND
      DO JSPEC = 1, NRSPEC
        ZMDEN = ZMDEN + CENDEN(JSPEC) * AMASS(JSPEC)
      END DO
      ZMDEN = ZMDEN * 1.673E-27_RKIND
      ZALFV0 = BNOT / SQRT (4._RKIND*CPI*1.0E-7_RKIND*ZMDEN)
      !
      !     EPSILON_0 IN LION UNITS
      ZEPS0 = CEPS0 * BNOT**2 / (CENDEN(1)*AMASS(1)*CMP)
      !
      !>>>	TEST-OUTPUT <<<
      IF ((JF.EQ.1).AND.NLOTP5(2)) THEN
        WRITE (NPRNT,7000) ZALFV0
7000    FORMAT(//,' V_ALFVEN ON MAGNETIC AXIS:', 1PE13.3,//, &
             &	 ' JS    S      ', &
             &	 ' KPARALLEL OM/KPVTE  OM/KPVTI  VBIRTH/VA VBIRT/VPH', &
             &	 ' VCRIT/VA  TFAST[EV] BETAFAST',/)
      END IF
      !lvC
      !lvC     SKIP FIRST MAGNETIC SURFACE
      !lv         CALL IODSK5(3)
      !
      !---------------------------------------------------------------------
      !     LOOP ON MAGNETIC SURFACES (INDEX JS)
      !
      DO JS = 2, NPSI-1
        !lvC
        !lvC     READ EQUILIBRIUM QUANTITIES
        !lv            CALL IODSK5(3)
        !
        !---------------------------------------------------------------------
        !     COMPUTE K_PARALLEL AVERAGED ON MAGNETIC SURFACE
        !     AND INTEGRAL OF JACOBIAN * DCHI.
        !     THE RESULT IS STORED IN ZKPAR2(JS) AND ZJDC(JS), SO THAT WE
        !     COMPUTE IT ONLY ONCE, SINCE IT ONLY DEPENDS ON THE EIGENMODE.
        !
        IF ((JF.EQ.1).AND.(JCASE.EQ.1)) THEN
          !
          !     INITIALIZE INTEGRALS FOR K_PARALLEL ON MAGNETIC SURFACE AND J
          ZINTUP = 0._RKIND
          ZINTU2 = 0._RKIND
          ZINTU3 = 0._RKIND
          ZINTU4 = 0._RKIND
          ZINTU5 = 0._RKIND
          ZINTU6 = 0._RKIND
          ZINTDO = 0._RKIND
          ZINTD3 = 0._RKIND
          ZINTD4 = 0._RKIND
          ZINTD5 = 0._RKIND
          ZINTD6 = 0._RKIND
          ZZJDC  = 0._RKIND
          !
          !---------------------------------------------------------------------
          !     1ST LOOP ON POLOIDAL INTERVALS (INDEX JC)
          !
          DO JC = 1, NPOL
            !
            CALL QUAEQU (JS,JC)
            CALL CENTER (JS,JC)
            !
            IF (JC.EQ.1) THEN
              ICM = NPOL
              ZDCM = CCHI(1) + C2PI - CCHI(NPOL)
            ELSE
              ICM = JC - 1
              ZDCM = ZDCP
            END IF
            !
            IF (JC.EQ.NPOL) THEN
              ICP = 1
              ZDCP = CCHI(1) + C2PI - CCHI(NPOL)
            ELSE
              ICP = JC + 1
              ZDCP = CCHI(JC+1) - CCHI(JC)
            END IF
            !
            !     D/DCHI OF E_N AND E_B WITH PARABOLIC INTERPOLATION
            !
            ZDCEN = ( ( EN(JS,ICP)-EN(JS,JC)  ) * ZDCM/ZDCP &
                 &  	    + ( EN(JS,JC) -EN(JS,ICM) ) * ZDCP/ZDCM ) &
                 &		    / ( ZDCM + ZDCP )
            ZDCEB = ( ( EP(JS,ICP)-EP(JS,JC)  ) * ZDCM/ZDCP &
                 &  	    + ( EP(JS,JC) -EP(JS,ICM) ) * ZDCP/ZDCM ) &
                 &		    / ( ZDCM + ZDCP )
            !
            !     B DOT GRAD OF E_N AND E_B
            !
            ZBGEN = ZDCEN / WJAC + ZI*WNTORO*WT * EN(JS,JC) / WR2
            ZBGEB = ZDCEB / WJAC + ZI*WNTORO*WT * EP(JS,JC) / WR2
            !
            !     B DOT GRAD OF V AND X
            !
            ZBGV = DVDC / WJAC + ZI*WNTORO*WT * VC / WR2
            ZBGX = DXDC / WJAC + ZI*WNTORO*WT * XC / WR2
            !
            !     INTEGRALS OVER CHI FOR AVERAGED K_PARALLEL
            !
            ZINTUP = ZINTUP + CABS ( CONJG (EN(JS,JC)) * ZBGEN &
                 &			    +         CONJG (EP(JS,JC)) * ZBGEB ) &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT
            !
            ZINTU2 = ZINTU2 + ( CABS(ZBGEN)**2 + CABS(ZBGEB)**2 ) &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT2
            !
            ZINTU3 = ZINTU3 + CABS(ZBGEN)**2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT2
            !
            ZINTU4 = ZINTU4 + CABS(ZBGEB)**2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT2
            !
            ZINTU5 = ZINTU5 + CABS(ZBGV)**2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT2
            !
            ZINTU6 = ZINTU6 + CABS(ZBGX)**2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI / WBTOT2
            !
            ZINTDO = ZINTDO + ( CABS (EN(JS,JC)) **2 &
                 &			    +   CABS (EP(JS,JC)) **2 ) &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI
            !
            ZINTD3 = ZINTD3 + CABS (EN(JS,JC)) **2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI
            !
            ZINTD4 = ZINTD4 + CABS (EP(JS,JC)) **2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI
            !
            ZINTD5 = ZINTD5 + CABS (VC) **2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI
            !
            ZINTD6 = ZINTD6 + CABS (XC) **2 &
                 &			    * WJAC * SQRT(WBPOL2) * WDCHI
            !
            !     INTEGRAL OVER CHI OF JACOBIAN
            !
            ZZJDC = ZZJDC + WJAC * WDCHI
            !
          END DO
          !
          !---------------------------------------------------------------------
          !     K_PARALLEL AVERAGED ON MAGNETIC SURFACES (NORMALIZED UNITS)
          !     COMPARE WITH OMEGA/V_ALFVEN
          !
          ZKPARA(JS) = ZINTUP / ZINTDO
          ZKPAR2(JS) = SQRT (ZINTU2 / ZINTDO)
          ZKPAR3(JS) = SQRT (ZINTU3 / ZINTD3)
          ZKPAR4(JS) = SQRT (ZINTU4 / ZINTD4)
          ZKPAR5(JS) = SQRT (ZINTU5 / ZINTD5)
          ZKPAR6(JS) = SQRT (ZINTU6 / ZINTD6)
          ZOMVA (JS) = OMEGA*SQRT(WRHO)
          !
          !---------------------------------------------------------------------
          !     S * DS * (INTEGRAL OF JACOBIAN * DCHI)
          ZJDC(JS) = ZZJDC * WS * WDS
          !
          !---------------------------------------------------------------------
          !     END OF CALCULATION OF K_PARALLEL AND JACOBIAN * DCHI
          !
        END IF
        !
        !---------------------------------------------------------------------
        !     EQIULIBRIUM QUANTITIES FUNCTION OF S ONLY
        !
        !****	WATCH FOR CONSISTENCY OF UNITS **** 
        !
        !     TEMPERATURES IN EV
        ZTE = TEMPEL (CCS(JS))
        ZTI = TEMPI  (1,CCS(JS))
        !
        !     DENSITIES AND THERMAL VELOCITIES IN SI UNITS
        ZNI = CENDEN(1) * DENSIT (CCS(JS))
        ZNE = ACHARG(1) * ZNI
        ZNF = CENDEN(2) * (1.0_RKIND - EQFAST*CCS(JS)**2)**EQKAPF(JF)
        ZVTE(JS) = 4.19E5_RKIND * SQRT (2._RKIND*ZTE)
        ZVTI(JS) = 9.79E3_RKIND * SQRT (2._RKIND*ZTI/AMASS(1)) 
        !
        !     CRITICAL VELOCITY V_c OF FAST PARTICLES (SI UNITS) (SLOWING-DOWN 
        !     DISTRIB.). EQ.(19) BV + NRL FORMULARY (1990 REVISED) P.29
        ZVC = ( 3.0_RKIND*SQRT(CPI)*(AMASS(2)+AMASS(1))*AMASSE/ &
             &	         (AMASS(2)*AMASS(1)) ) ** (1.0_RKIND/3.0_RKIND) &
             &	     * 4.19E5_RKIND * SQRT (ZTE)
        !
        !     V_C ON PREVIOUS AND NEXT MAGNETIC SURFACES (SI UNITS)
        ZVCM = ZVC * SQRT(TEMPEL(CCS(JS-1))/ZTE)
        ZVCP = ZVC * SQRT(TEMPEL(CCS(JS+1))/ZTE)
        !
        !     FACTOR C, EQ.(20) BV
        ZC = 3.0_RKIND / ( 4.0_RKIND * CPI * LOG ( (VBIRTH/ZVC)**3 + 1.0_RKIND) )
        !
        !     FACTOR C ON PREVIOUS AND NEXT MAGNETIC SURFACES
        ZCM = 3.0_RKIND / ( 4.0_RKIND * CPI * LOG ( (VBIRTH/ZVCM)**3 + 1.0_RKIND) )
        ZCP = 3.0_RKIND / ( 4.0_RKIND * CPI * LOG ( (VBIRTH/ZVCP)**3 + 1.0_RKIND) )
        !
        !     PLASMA FREQUENCIES SQUARED (SI UNITS)
        ZOMPE2 = ZNE * CHE**2 / (CEPS0*CME)
        ZOMPI2 = ZNI * (ACHARG(1)*CHE)**2 / (CEPS0*AMASS(1)*CMP)
        ZOMPF2 = ZNF * (ACHARG(2)*CHE)**2 / (CEPS0*AMASS(2)*CMP)
        !
        !     ZOMPF2 ON PREVIOUS AND NEXT MAGNETIC SURFACES (SI UNITS)
        ZNFM = CENDEN(2) * (1.0_RKIND - EQFAST*CCS(JS-1)**2)**EQKAPF(JF)
        ZNFP = CENDEN(2) * (1.0_RKIND - EQFAST*CCS(JS+1)**2)**EQKAPF(JF)
        ZOMPFM = ZNFM* (ACHARG(2)*CHE)**2 / (CEPS0*AMASS(2)*CMP)
        ZOMPFP = ZNFP* (ACHARG(2)*CHE)**2 / (CEPS0*AMASS(2)*CMP)
        !
        !     K_PARALLEL IN SI UNITS
        ZKP = ZKPAR2(JS) / RMAJOR
        !
        !     OMEGA IN SI UNITS
        ZOMEGA = OMEGA * ZALFV0 / RMAJOR
        !
        !     PARALLEL PHASE VELOCITY IN SI UNITS
        ZVPH = OMEGA / ZKPAR2(JS) * ZALFV0
        !
        !****	NOW WE TRANSFORM TO LION-NORMALIZED UNITS ****
        !
        ZVTE(JS) = ZVTE(JS) / ZALFV0
        ZVTI(JS) = ZVTI(JS) / ZALFV0
        ZVC  = ZVC  / ZALFV0
        ZVCM = ZVCM / ZALFV0
        ZVCP = ZVCP / ZALFV0
        ZVPH = ZVPH / ZALFV0
        ZVBIRT = VBIRTH / ZALFV0
        !
        ZZZ    = (RMAJOR / ZALFV0) **2
        ZOMPE2 = ZOMPE2 * ZZZ
        ZOMPI2 = ZOMPI2 * ZZZ
        ZOMPF2 = ZOMPF2 * ZZZ
        ZOMPFM = ZOMPFM * ZZZ
        ZOMPFP = ZOMPFP * ZZZ
        !
        ZKP = ZKP * RMAJOR
        !
        ZOMEGA = ZOMEGA * RMAJOR / ZALFV0
        !
        !     EXP (-Z0**2)
        ZEZ0E = EXP ( - (ZVPH/ZVTE(JS))**2 )
        ZEZ0I = EXP ( - (ZVPH/ZVTI(JS))**2 )
        !
        !     DISPERSION FUNCTION Z OF ELECTRONS. NOTE THAT DISPFN COMPUTES
        !     THE FRIED-CONTE DISPERSION FUNCTION (REF. NRL P.30). THE 
        !     Z FUNCTION TO BE USED HERE IS ACCORDING TO SHAFRANOV DEFINITION
        !     Z(ARG) = ARG * Z_FRIED-CONTE(ARG)
        ZARG = ZVPH/ZVTE(JS)
        CALL DISPFN (ZARG,ZFC)
        ZZE = 1.0_RKIND / (1.0_RKIND - ZARG * ZFC(1))
        !
        !     INTEGRALS I_0, I_1 AND I_2 FOR FAST PARTICLES (SLOWING-DOWN)
        !     EQS.(54-56) BV. LION UNITS.
        CALL SLOWIN (ZVPH, ZVBIRT, ZVC, ZVPH, ZINT)
        !
        !     INTEGRALS I_0, I_1 AND I_2 FOR FAST PARTICLES (SLOWING-DOWN)
        !     EQS.(54-56) BV. EVALUATED WITH SAME PHASE VELOCITY, BUT WITH
        !     ELECTRON TEMPERATURE OF PREVIOUS AND NEXT MAGNETIC SURFACES.
        !     LION UNITS.
        CALL SLOWIN (ZVPH, ZVBIRT, ZVCM, ZVPH, ZINTM)
        CALL SLOWIN (ZVPH, ZVBIRT, ZVCP, ZVPH, ZINTP)
        !
        !     EQUIVALENT FAST PARTICLE TEMPERATURE (M<V**2>/2) [EV]
        !     AVERAGE KINETIC ENERGY PER PARTICLE. EQS.(18)-(20), (55)(56) BV.
        ZTFAST = 4.0_RKIND * CPI * ZC * ( 0.5_RKIND*ZVBIRT**2 + ZVC**2/3.0_RKIND * &
             &	 ( 0.5_RKIND*LOG ( (ZVBIRT+ZVC)**2/(ZVBIRT**2-ZVC*ZVBIRT+ZVC**2) ) &
             &	 - SQRT(3._RKIND)*ATAN ( (2._RKIND*ZVBIRT-ZVC)/(ZVC*SQRT(3._RKIND)) ) &
             &	 + SQRT(3._RKIND)*ATAN ( -1._RKIND/SQRT(3._RKIND) ) ) ) * ZALFV0**2 &
             &	 * 0.5_RKIND * AMASS(2) * CMP / CHE
        !
        !     BETAS ("LOCAL") (N.B.: BNOT IS GIVEN IN TESLAS)
        ZBFL = 2._RKIND* CMU0 * CHE * ZNF * ZTFAST / BNOT**2
        ZBEL = 2._RKIND* CMU0 * CHE * ZNE * ZTE / BNOT**2
        ZBIL = 2._RKIND* CMU0 * CHE * ZNI * ZTI / BNOT**2
        !
        IF (JS.EQ.2) THEN
          ZTFAS0 = ZTFAST
          ZBF0 = ZBFL
          ZBE0 = ZBEL
          ZBI0 = ZBIL
        END IF
        !
        !     INTEGRALS FOR VOLUME-AVERAGED BETAS
        ZBFAV = ZBFAV + ZNF * ZTFAST * ZJDC(JS)
        ZBEAV = ZBEAV + ZNE * ZTE * ZJDC(JS)
        ZBIAV = ZBIAV + ZNI * ZTI * ZJDC(JS)
        ZBDEN = ZBDEN + ZJDC(JS)
        !
        !>>>	TEST-OUTPUT <<<
        !
        IF ((JF.EQ.1).AND.NLOTP5(2)) THEN
          !
          ZSQRIN = SQRT ( DENSIT (CCS(JS)) )
          WRITE (NPRNT,7010) JS, CCS(JS), ZKP, ZVPH/ZVTE(JS), &
               &     ZVPH/ZVTI(JS), ZVBIRT*ZSQRIN, ZVBIRT/ZVPH, ZVC*ZSQRIN, &
               &	   ZTFAST, ZBFL
7010      FORMAT (I4,1P12E10.3)
          !
        END IF
        !
        !<<<
        !
        !     D/DS (OMEGA_PFAST**2 * C * I_J) WITH PARABOLIC INTERPOLATION
        ZDSM = CCS(JS) - CCS(JS-1)
        ZDSP = CCS(JS+1) - CCS(JS)
        ZG0 = ( (ZOMPFP*ZCP*ZINTP(1) - ZOMPF2*ZC*ZINT(1)  ) &
             &			* ZDSM/ZDSP &
             &            + (ZOMPF2*ZC*ZINT(1)  - ZOMPFM*ZCM*ZINTM(1) ) &
             &			* ZDSP/ZDSM )    / (ZDSM + ZDSP)
        ZG1 = ( (ZOMPFP*ZCP*ZINTP(2) - ZOMPF2*ZC*ZINT(2)  ) &
             &			* ZDSM/ZDSP &
             &            + (ZOMPF2*ZC*ZINT(2)  - ZOMPFM*ZCM*ZINTM(2) ) &
             &			* ZDSP/ZDSM )    / (ZDSM + ZDSP)
        ZG2 = ( (ZOMPFP*ZCP*ZINTP(3) - ZOMPF2*ZC*ZINT(3)  ) &
             &			* ZDSM/ZDSP &
             &            + (ZOMPF2*ZC*ZINT(3)  - ZOMPFM*ZCM*ZINTM(3) ) &
             &			* ZDSP/ZDSM )    / (ZDSM + ZDSP)
        !
        !---------------------------------------------------------------------
        !     2ND LOOP ON POLOIDAL INTERVALS (INDEX JC)
        !
        DO JC = 1, NPOL
          !
          CALL QUAEQU (JS,JC)
          CALL CENTER (JS,JC)
          CALL MAGNET
          !
          !     ALFVEN VELOCITY IN SI UNITS
          ZALFV = ZALFV0 * WBTOT / SQRT (WRHO)
          !
          !     ELECTRON THERMAL VELOCITY ((2*K*TE/ME)**1/2) IN SI UNITS
          !     REF. NRL FORMULARY (1990 REVISED) P.29
          ZTE = TEMPEL (WS)
          ZVTESI = 4.19E5_RKIND * SQRT (2._RKIND*ZTE)
          !
          !     ELECTRON THERMAL VELOCITY IN LION NORMALIZED UNITS
          ZVTEN = ZVTESI / ZALFV0
          !
          !     ALFVEN VELOCITY / ELECTRON THERMAL VELOCITY
          ZVAVTE = ZALFV / ZVTESI
          !
          !     OMEGA / (K_PARALLEL*VTE)
          ZOMKVT = OMEGA / (ZKPAR2(JS) * ZVTEN)
          !
          !     ION ACOUSTIC (SOUND) VELOCITY IN SI UNITS
          !     REF. NRL FORMULARY (1990 REVISED) P.29
          ZGAMMA = 1.0_RKIND
          ZCS = 9.79E3_RKIND * SQRT (ZGAMMA * ACHARG(1) * ZTE / AMASS(1))
          !
          !     ION LARMOR RADIUS WITH ELECTRON TEMPERATURE IN SI UNITS
          !     REF. NRL FORMULARY (1990 REVISED) P.28
          ZRL = 1.02E-4_RKIND*SQRT(AMASS(1)*ZTE) / (ACHARG(1)*BNOT*WBTOT)
          !
          !     ION LARMOR RADIUS IN LION NORMALIZED UNITS
          ZRL = ZRL / RMAJOR
          !
          !     VOLUME OF CELL 
          ZDV = 2._RKIND*WJAC*CPSRF*WS*WDS*WDCHI
          !
          !     FOR KINETIC ELECTRON LANDAU DAMPING WE NEED TO DETERMINE K_LAMBDA.
          !     IT IS DONE HERE WITH A PARABOLIC INTERPOLATION WHICH IS NOT 
          !     APPLICABLE IN THE VICINITY OF THE MAGNETIC AXIS ==> WE SKIP THE
          !     FIRST MAGNETIC SURFACE. 
          !     MOREOVER, BOTH KINETIC AND CURVATURE DRIFT E.L.D.'S EXPRESSIONS
          !     ARE TAKEN FROM SLAB GEOMETRY ==> THEY ARE NOT APPLICABLE IN THE
          !     VICINITY OF THE MAGNETIC AXIS ANYWAY.
          !
          !     GRADIENT-SUB-LAMBDA OF E-SUB-PSI
          !
          !     D/DS OF E_PSI WITH PARABOLIC INTERPOLATION
          ZDSM = CCS(JS) - CCS(JS-1)
          ZDSP = CCS(JS+1) - CCS(JS)
          ZDDS = ( (EN(JS+1,JC) - EN(JS,JC)  ) * ZDSM/ZDSP &
               &             + (EN(JS,JC)   - EN(JS-1,JC)) * ZDSP/ZDSM ) &
               &             / (ZDSM + ZDSP)
          !
          !     D/DCHI OF E_PSI WITH PARABOLIC INTERPOLATION
          IF (JC.EQ.1) THEN
            ZHP = CCHI(2) - CCHI(1)
            ZHM = ZHP
            ZFP = EN(JS,2)
            ZF0 = EN(JS,1)
            ZFM = EN(JS,NPOL)
          ELSE IF (JC.EQ.NPOL) THEN
            ZHM = CCHI(NPOL) - CCHI(NPOL-1)
            ZHP = 2._RKIND*CPI - CCHI(NPOL)
            ZFP = EN(JS,1)
            ZF0 = EN(JS,NPOL)
            ZFM = EN(JS,NPOL-1)
          ELSE
            ZHM = CCHI(JC) - CCHI(JC-1)
            ZHP = CCHI(JC+1) - CCHI(JC)
            ZFP = EN(JS,JC+1)
            ZF0 = EN(JS,JC)
            ZFM = EN(JS,JC-1)
          END IF
          ZDDC = ( (ZFP - ZF0) * ZHM/ZHP &
               &             + (ZF0 - ZFM) * ZHP/ZHM ) &
               &             / (ZHM + ZHP)
          !
          !     D/DS OF E_BINORMAL WITH PARABOLIC INTERPOLATION
          ZDBDS = ( (EP(JS+1,JC) - EP(JS,JC)  ) * ZDSM/ZDSP &
               &              + (EP(JS,JC)   - EP(JS-1,JC)) * ZDSP/ZDSM ) &
               &            / (ZDSM + ZDSP)
          !
          !     D/DCHI OF E_BINORMAL WITH PARABOLIC INTERPOLATION
          IF (JC.EQ.1) THEN
            ZHP = CCHI(2) - CCHI(1)
            ZHM = ZHP
            ZFP = EP(JS,2)
            ZF0 = EP(JS,1)
            ZFM = EP(JS,NPOL)
          ELSE IF (JC.EQ.NPOL) THEN
            ZHM = CCHI(NPOL) - CCHI(NPOL-1)
            ZHP = 2._RKIND*CPI - CCHI(NPOL)
            ZFP = EP(JS,1)
            ZF0 = EP(JS,NPOL)
            ZFM = EP(JS,NPOL-1)
          ELSE
            ZHM = CCHI(JC) - CCHI(JC-1)
            ZHP = CCHI(JC+1) - CCHI(JC)
            ZFP = EP(JS,JC+1)
            ZF0 = EP(JS,JC)
            ZFM = EP(JS,JC-1)
          END IF
          ZDBDC = ( (ZFP - ZF0) * ZHM/ZHP &
               &              + (ZF0 - ZFM) * ZHP/ZHM ) &
               &            / (ZHM + ZHP)
          !
          !     PSI COMPONENT OF GRAD-LAMBDA E-PSI
          ZGRPSI = SQRT (WGRPS2)
          ZDPSI = ZGRPSI / (2._RKIND*CPSRF*WS) * (ZDDS + WBETCH * ZDDC)
          !     APPROX WITH ZDDS ONLY
          ZDPSIA = ZGRPSI / (2._RKIND*CPSRF*WS) * ZDDS
          !
          !     PERP COMPONENT OF GRAD-LAMBDA E-PSI
          ZDPERP = WT / (WBTOT * ZGRPSI*WJAC) * ZDDC &
               &             - ZGRPSI / (WBTOT*WR2) * ZI * WNTORO * EN(JS,JC)
          !
          !     MODULUS**2 OF GRAD-LAMBDA E-PSI
          ZGRAD2 = CABS (ZDPSI) **2 + CABS (ZDPERP) **2
          !     APPROX WITH ZDDS ONLY
          ZGRADA = CABS (ZDPSIA) **2
          !
          !     APPROX GRAD_LAMBDA E_PSI ON (R,Z) COORDINATES
          ZHP = ( (CCR(JS+1,JC) - CCR(JS,JC)) * CNR(JS,JC) &
               &            + (CCZ(JS+1,JC) - CCZ(JS,JC)) * CNZ(JS,JC) ) &
               &          /   SQRT ( CNR(JS,JC)**2 + CNZ(JS,JC)**2 )
          ZHM = ( (CCR(JS,JC) - CCR(JS-1,JC)) * CNR(JS,JC) &
               &            + (CCZ(JS,JC) - CCZ(JS-1,JC)) * CNZ(JS,JC) ) &
               &          /   SQRT ( CNR(JS,JC)**2 + CNZ(JS,JC)**2 )
          ZGREPS = CABS ( (EN(JS+1,JC) - EN(JS,JC)) * ZHM/ZHP &
               &		          + (EN(JS,JC) - EN(JS-1,JC)) * ZHP/ZHM ) &
               &		   / (ZHM + ZHP)
          !
          !     INTEGRAL OF DAMPING TERM (KINETIC E.L.D.)
          ZDAMPI = ZDAMPI + ZFACT * WRHO * ZRL**2 * ZVAVTE &
               &             * EXP (- ZVAVTE**2) * ZGRAD2 * ZDV
          !     APPROX WITH ZDDS ONLY (KINETIC E.L.D.)
          ZDAMPA = ZDAMPA + ZFACT * WRHO * ZRL**2 * ZVAVTE &
               &             * EXP (- ZVAVTE**2) * ZGRADA * ZDV
          !     APPROX GRAD_LAMBDA E_PSI ON (R,Z) COORDINATES (KINETIC E.L.D.)
          ZDAMPB = ZDAMPB + ZFACT * WRHO * ZRL**2 * ZVAVTE &
               &             * EXP (- ZVAVTE**2) * ZGREPS**2 * ZDV 
          !
          !     INTEGRAL OF WAVE ENERGY DENSITY (KINETIC E.L.D.)
          ZWENER = ZWENER + 0.5_RKIND*WRHO*CABS(EN(JS,JC))**2*ZDV
          ZWETOT = ZWETOT + 0.5_RKIND*WRHO*(CABS(EN(JS,JC))**2 &
               &                                + CABS(EP(JS,JC))**2)*ZDV
          !
          !     CONTRIBUTION TO NUMERATOR OF DRIFT E.L.D.
          ZNUMER = ZNUMER + ZFACTR * WQ*WQ * ZCS &
               &		   * EXP (- ZVAVTE**2) * CABS(EP(JS,JC))**2 * ZDV
          !
          !     CONTRIBUTION TO DENOMINATOR OF DRIFT E.L.D.
          ZDENOM = ZDENOM + ZALFV * (CABS(EN(JS,JC))**2 &
               &                               + CABS(EP(JS,JC))**2)*ZDV
          !
          !---------------------------------------------------------------------
          !     WAVE-PARTICLE POWER TRANSFER ACCORDING TO DKE MODEL
          !
          !    CYCLOTRON FREQUENCIES IN LION UNITS
          ZOMCI = WOMCI(1)
          ZOMCE = WOMCI(1)*AMASS(1)/(AMASSE*ACHARG(1))
          ZOMCF = WOMCI(1)*AMASS(1)*ACHARG(2)/(AMASS(2)*ACHARG(1))
          !
          ZNORM  = SQRT ( CNR(JS,JC)**2 + CNZ(JS,JC)**2 )
          ZGRPSI = SQRT (WGRPS2)
          ZR     = SQRT (WR2)
          !
          !     BETA_PARALLEL_N
          ZBETPN = - WT / (WR2*WBTOT) * ( CNZ(JS,JC) / ZNORM &
               &		   +  EQ(23,JC,js) / (2.0_RKIND*WBTOT2*WJAC*ZGRPSI) )
          !
          !     BETA_PARALLEL_B
          ZBETPB = ZGRPSI / (ZR*WBTOT2) * ( -WJ0PHI &
               &		   + WGRPS2 * WDNGP / ZR ) - CNR(JS,JC)/(ZNORM*ZR)
          !
          !     BETA_N_PARALLEL
          ZBETNP = WT * EQ(23,JC,js) / (2.0_RKIND*WBTOT*WJAC*WGRPS2*ZGRPSI)
          !
          !     BETA_B_PARALLEL
          ZBETBP = WBTOR2 / (WBTOT2*ZGRPSI) * ( ZR * WJ0PHI &
               &		   - WGRPS2 * WDNGP ) + CNR(JS,JC)/(ZNORM*ZR)
          !
          !     D(E_N)/DS WITH PARABOLIC INTERPOLATION : ZDDS, SEE ABOVE
          !     D(E_N)/DCHI WITH PARABOLIC INTERPOLATION : ZDDC, SEE ABOVE
          !     D(E_B)/DS WITH PARABOLIC INTERPOLATION : ZDBDS, SEE ABOVE
          !     D(E_B)/DCHI WITH PARABOLIC INTERPOLATION : ZDBDC, SEE ABOVE
          !
          !     GRAD_N (E_N)
          ZGNEN = ZGRPSI * (ZDDS + WBETCH*ZDDC) / (2.0_RKIND*CPSRF*WS)
          !
          !     GRAD_N (E_B)
          ZGNEB = ZGRPSI * (ZDBDS + WBETCH*ZDBDC) / (2.0_RKIND*CPSRF*WS)
          !
          !     GRAD_B (E_N)
          ZGBEN = WT * ZDDC / (WBTOT*ZGRPSI*WJAC) &
               &		  - ZGRPSI * ZI * WNTORO * EN(JS,JC) / (WBTOT*WR2)
          !
          !     GRAD_B (E_B)
          ZGBEB = WT * ZDBDC / (WBTOT*ZGRPSI*WJAC) &
               &		  - ZGRPSI * ZI * WNTORO * EP(JS,JC) / (WBTOT*WR2)
          !
          !     DIV (E_PERP)
          ZDIVEP = ZGNEN + ZGBEB + EN(JS,JC) * (ZBETBP - ZBETPB) &
               &		   + EP(JS,JC) * (ZBETPN - ZBETNP)
          !
          !     BETA_PERP DOT E_PERP
          ZBPEP = ZBETPN * EN(JS,JC) + ZBETPB * EP(JS,JC)
          !
          !     BETA_PERP DOT GRAD (CONJG(E_B))
          ZBGCEB = CONJG ( ZBETPN * ZGNEB + ZBETPB * ZGBEB )
          !C
          !C     B_PARALLEL
          !	    ZBPARL = ( ZGNEB - ZGBEN + ZBETNP * EN(JS,JC) 
          !     +		     + ZBETBP * EP(JS,JC) ) / ( ZI * OMEGA )
          !C
          !C>>>	TEST-OUTPUT  <<<
          !	    ZCTRA = BPARL - ZBPARL
          !	    WRITE(NPRNT,7700) ZBETPN, ZBETPB, ZBETNP, ZBETBP,
          !     +		  WGRPS2, EQ(23,JC,js), BPARL, ZBPARL, ZCTRA
          !	    WRITE(NPRNT,7700) EN(JS,JC), EP(JS,JC), ZDDS, ZDDC,
          !     +		  ZDBDS, ZDBDC
          !	    WRITE(NPRNT,7700) ZGNEN, ZGNEB, ZGBEN, ZGBEB
          ! 7700	 FORMAT(1X,1P12E10.3)
          !
          !     SWITCH TTMP BY SWITCHING B_PARALLEL
          IF (NLTTMP) THEN
            ZBPARL = BPARL
          ELSE
            ZBPARL = CMPLX (0.0_RKIND,0.0_RKIND)
          END IF
          !
          !     CABS(A_ELECTRON)**2 , EQ.(67) BV
          ZAELE2 = CABS ( ZZE * ( ZI * OMEGA/WOMCI(1) * ZDIVEP &
               &				  + ZBPEP ) ) **2
          !
          !     CABS(A_ION)**2 , EQ.(68) BV
          ZAION2 = CABS ( (ZTE/ZTI) * ZZE * ( ZI * OMEGA/WOMCI(1) &
               &		   * ZDIVEP  + ZBPEP ) - ZI * OMEGA * (1.0_RKIND + ZTE/ZTI) &
               &		   * ZBPARL + ( 1.0_RKIND + ZTE/ZTI + 2.0_RKIND * &
               &		   (ZVPH/ZVTI(JS))**2 )* ZBPEP ) **2
          !
          !     CABS (BETA_PERP DOT E_PERP - I OMEGA B_PARALLEL) **2
          ZAALL2 = CABS ( ZBPEP - ZI * OMEGA * ZBPARL ) **2
          !
          !     ELECTRON CONTRIBUTION P_HOMO, EQ.(66) BV
          ZPHELE(JS) = ZPHELE(JS) + ZSQPI * ZEPS0 * ZDV * ZOMPE2 &
               &			* ZVTE(JS) * ZEZ0E * (ZAELE2 + ZAALL2) &
               &			/ (4.0_RKIND * ZOMCE**2 * ZKP)
          !
          !     BULK ION CONTRIBUTION P_HOMO, EQ.(66) BV
          ZPHION(JS) = ZPHION(JS) + ZSQPI * ZEPS0 * ZDV * ZOMPI2 &
               &			* ZVTI(JS) * ZEZ0I * (ZAION2 + ZAALL2) &
               &			/ (4.0_RKIND * ZOMCI**2 * ZKP)
          !
          !     ADD FAST PARTICLE CONTRIBUTION ONLY IF VPHASE < VBIRTH
          IF (ZVPH.LE.ZVBIRT) THEN
            !
            !     FAST PARTICLE CONTRIBUTION P_HOMO, EQ.(70) BV
            ZPHFAS(JS) = ZPHFAS(JS) + CPI**2 * ZEPS0 * ZDV * ZOMPF2 &
                 &			* ZC * ( ( ZVPH**4 / (ZVPH**3 + ZVC**3) &
                 &			+ 2.0_RKIND * ZVPH**2 * ZINT(1) ) * CABS(ZBPEP)**2 &
                 &			+ 2.0_RKIND * OMEGA * ZVPH**2 * ZINT(1) &
                 &			* AIMAG ( ZBPARL * CONJG (ZBPEP) ) &
                 &			+ ZINT(2) * ZAALL2 ) / (ZKP * ZOMCF**2)
            !
            !     FAST PARTICLE CONTRIBUTION P_INHOMO, EQ.(71) BV
            ZPINHF(JS) = ZPINHF(JS) + CPI**2 * ZEPS0 * ZDV * ZGRPSI &
                 &		* AIMAG ( ( (ZVPH**4 * ZG0 + 0.5_RKIND * ZG1) * ZBPEP &
                 &		+ (0.5_RKIND * ZVPH**2 * ZG1 + 0.25_RKIND * ZG2) &
                 &		* (ZBPEP - ZI * OMEGA * ZBPARL) ) * ZBGCEB ) &
                 &		/ (OMEGA * ZKP * ZOMCF**3 * 2.0_RKIND * CPSRF * WS)
            !
          END IF
          !
        END DO
        !
        !---------------------------------------------------------------------
        !     KINETIC (FINITE E_PARALLEL) ELECTRON LANDAU DAMPING RATE 
        !     ACCORDING TO EQ.(18) OF NUCL.FUS. 32 (1992) 1695.
        !
        ZKELDR(JS) = ZDAMPA / ZWENER
        !
        ZNUMKE(JS) = ZDAMPA
        ZDENKE(JS) = ZWENER
        ZDENKT(JS) = ZWETOT
        !
        !---------------------------------------------------------------------
        !     "CURVATURE" DRIFT ELECTRON LANDAU DAMPING RATE
        !     (REF. JAN 23.10.92)
        !
        ZCDELD(JS) = ZNUMER / ZDENOM
        !
        ZNUMCE(JS) = ZNUMER
        ZDENCE(JS) = ZDENOM
        !
        !---------------------------------------------------------------------
        !      DRIFT KINETIC POWER ABSORPTIONS
        !
        !      DP/DS
        ZPHELE(JS) = ZPHELE(JS) / WDS
        ZPHION(JS) = ZPHION(JS) / WDS
        ZPHFAS(JS) = ZPHFAS(JS) / WDS
        ZPINHF(JS) = ZPINHF(JS) / WDS
        !
        !     SET PDK'S TO ZERO IF SMALLER THAN 1E-99
        IF (ZPHELE(JS).LT.1.0E-99_RKIND) THEN
          ZPHELE(JS) = 0.0_RKIND
        END IF
        IF (ZPHION(JS).LT.1.0E-99_RKIND) THEN
          ZPHION(JS) = 0.0_RKIND
        END IF
        IF (ABS(ZPHFAS(JS)).LT.1.0E-99_RKIND) THEN
          ZPHFAS(JS) = 0.0_RKIND
        END IF
        IF (ABS(ZPINHF(JS)).LT.1.0E-99_RKIND) THEN
          ZPINHF(JS) = 0.0_RKIND
        END IF
        !
        !      POWER FLUXES
        ZFHELE(JS) = ZFHELE(JS-1) + ZPHELE(JS) * WDS
        ZFHION(JS) = ZFHION(JS-1) + ZPHION(JS) * WDS
        ZFHFAS(JS) = ZFHFAS(JS-1) + ZPHFAS(JS) * WDS
        ZFINHF(JS) = ZFINHF(JS-1) + ZPINHF(JS) * WDS
        !
        ZFBALA(JS) = ZFHELE(JS) + ZFHION(JS) + ZFHFAS(JS) &
             &		    + ZFINHF(JS)
        !
        !     CRITICAL CENTRAL DENSITY OF FAST PARTICLES
        !
        IF ( ( CENDEN(2).NE.0._RKIND) .AND. &
             &	      ( ( ZFHFAS(JS) + ZFINHF(JS) ).NE.0._RKIND) ) THEN
          ZNFCRI(JS) = - ( ZFHELE(JS) + ZFHION(JS) ) &
               &		       /   ( ZFHFAS(JS) + ZFINHF(JS) ) * CENDEN(2)
        END IF
        !
        !     SET ZNFCRI TO 1E10 IF ZNFCRI<0 (MEANING STABLE)
        IF (ZNFCRI(JS).LE.0._RKIND) THEN
          ZNFCRI(JS) = 1.0E10_RKIND
        END IF
        !
        !---------------------------------------------------------------------
      END DO
      !
      !     VOLUME-AVERAGED BETAS
      ZZZ = 2._RKIND* CMU0 *CHE / ( BNOT**2 * ZBDEN )
      ZBFAV = ZBFAV * ZZZ
      ZBEAV = ZBEAV * ZZZ
      ZBIAV = ZBIAV * ZZZ
      !
      !---------------------------------------------------------------------
      !L		    OUTPUT
      !
      !     OUTPUT ONLY ONCE
      !
      IF (JF.EQ.1) THEN
        !
        !     ELECTRON LANDAU DAMPING RATES
        IF(NLOTP5(3)) THEN
          WRITE (NPRNT,8000)
          WRITE (NPRNT,8010) (J, CCS(J), ZKELDR(J), ZCDELD(J), &
               &	    ZNUMKE(J), ZDENKE(J), ZDENKT(J), ZNUMCE(J), ZDENCE(J), &
               &	    J=2,NPSI-1)
        END IF
        !
        WRITE (NPRNT,9000) ZKELDR(NPSI-1), ZCDELD(NPSI-1)
        !
        !     OUTPUT K_PARALLEL AVERAGED ON MAGNETIC SURFACES
        !     (TO BE COMPARED TO OMEGA/V_ALFVEN)
        IF(NLOTP5(2)) THEN
          WRITE (NPRNT,9100)
          WRITE (NPRNT,9110) (J,CCS(J),ZKPARA(J),ZKPAR2(J),ZKPAR3(J), &
               &	 ZKPAR4(J),ZKPAR5(J),ZKPAR6(J),ZOMVA(J),J=2,NPSI-1)
        END IF
        !
      END IF
      !
      !     OUTPUT FOR EACH FAST PARTICLE DISTRIBUTION
      !
      !     RADIUS WHERE D/DS(N_FAST) IS MAXIMAL
      ZSHAT = 1._RKIND / SQRT ( EQFAST * (2.0_RKIND * EQKAPF(JF) - 1) )
      !
      !     RADIUS WHERE N_FAST = 1/2 N_FAST_AXIS
      ZSHALF = SQRT ( ( 1._RKIND - 0.5_RKIND**(1._RKIND/EQKAPF(JF)) ) / EQFAST )
      !
      WRITE (NPRNT,9190) JF, EQKAPF(JF), ZSHAT, ZSHALF
      !
      !     BETAS
      WRITE (NPRNT,9195) ZBEAV, ZBIAV, ZBFAV, ZBE0, ZBI0, ZBF0
      !
      !     V0/VA0
      WRITE (NPRNT,9197) VBIRTH/ZALFV0
      !
      !     ELECTRON, ION AND FAST PARTICLE DKE POWERS
      IF (NLOTP5(3)) THEN
        WRITE (NPRNT,9200)
        WRITE (NPRNT,9210) (J, CCS(J), ZPHELE(J), ZPHION(J), &
             &	 ZPHFAS(J), ZPINHF(J), ZFBALA(J), ZNFCRI(J), J=2,NPSI-1)
      END IF
      !
      WRITE (NPRNT,9215) ZFHELE(NPSI-1), ZFHION(NPSI-1), &
           &	 ZFHFAS(NPSI-1), ZFINHF(NPSI-1), ZFBALA(NPSI-1), &
           &   ZNFCRI(NPSI-1)
      !
      !     CRITICAL FAST PARTICLE DENSITY, AVERAGED BETA_FAST AND CENTRAL
      !     BETA_FAST FOR MARGINAL STABILITY
      ZRAT   = ZNFCRI(NPSI-1) / CENDEN(2)
      ZBFCR  = ZBFAV*ZRAT
      ZBF0CR = ZBF0*ZRAT
      WRITE (NPRNT,9220) JF, JCASE, ZNFCRI(NPSI-1), ZBFCR, &
           &		            ZBF0CR
      !
      !     WRITE TABLE OF RESULTS FOR USE WITH MATLAB ON TAPE26&27
      IF (NLPLO5(3)) THEN
        WRITE (26,9260) VBIRTH/ZALFV0, ZSHALF, ZBFCR, ZBF0CR, &
             &         ZNFCRI(NPSI-1), ZKELDR(NPSI-1), ZCDELD(NPSI-1), &
             &         ZBEAV, ZBIAV, ZBE0, ZBI0, OMEGA/CEOMCI(1)
        IF ((JF.EQ.1).AND.(JCASE.EQ.1)) THEN
          WRITE(27,9270) ( CCS(J), OMEGA/(ZKPAR2(J)*ZVTE(J)), &
               &         OMEGA/(ZKPAR2(J)*ZVTI(J)), OMEGA/(ZKPAR2(J)*ZVBIRT), &
               &         ZOMVA(J)/ZKPAR2(J), J = 2, NPSI-1 )
        END IF

        WRITE(27,9270) ( CCS(J), ZPHELE(J), ZPHION(J), &
             &         ZPHFAS(J), ZPINHF(J), J = 2, NPSI-1 )
      END IF
      !
      !--------------------------------------------------------------------
      !     END OF LOOP ON CONST BETA SCANON JCASE
      !
    END DO
    !
    !--------------------------------------------------------------------
    !     END OF LOOP ON FAST PARTICLE PROFILES ON JF
    !
  END DO
  !
  !--------------------------------------------------------------------
  !     RESTORE INITIAL DENSITIY, TEMPERATURES, MAGNETIC FIELD AND
  !     LION-NORMALIZED CENTRAL ION CYCLOTRON FREQUENCY
  !
  CENDEN(1) = ZCENDS
  BNOT = ZBNOTS
  CENTE = ZCENTE
  CENTI(1) = ZCENTI
  CEOMCI(1) = ZCEOMC
  !
  !--------------------------------------------------------------------
  !	 
8000 FORMAT(//,' ELECTRON LANDAU DAMPING RATE OF ALFVEN WAVE',/, &
       &             ' FOR SINGLE ION SPECIES AND OMEGA << OMEGACI',/, &
       &             ' *******************************************',//, &
       &             '  JS         S      KINETIC ELD.  DRIFT ELD.', &
       &             ' KIN.DAMP   KIN.ENERGY(EPSI) KIN.ENERGY(TOT)', &
       &		   ' DRIFT.DAMP  DRIFT.ENERGY',//)
8010 FORMAT(1X,I4,1P8E13.3)
9000 FORMAT(//,' ELECTRON LANDAU DAMPING RATE OF ALFVEN WAVE',/, &
       &             ' FOR SINGLE ION SPECIES AND OMEGA << OMEGACI',/, &
       &             ' *******************************************',/, &
       &             ' * KINETIC ELDAMPRATE   ', 1PE15.3,  '     *',/, &
       &             ' * DRIFT   ELDAMPRATE   ', 1PE15.3,  '     *',/, &
       &             ' *******************************************',/)
  !
9100 FORMAT(//,' K_PARALLEL AVERAGED ON MAGNETIC SURFACES',//, &
       &   '  JS         S      K_PARALLEL(1)K_PARALLEL(2)', &
       &   'K_PARALLEL(3)K_PARALLEL(4)K_PARALLEL(5)K_PARALLEL(6)', &
       &	 ' OMEGA/V_A',//,23X,'E*GP_E      (GP_E)       (GP_EN)', &
       &	 '       (GP_EB)       (GP_V)       (GP_X)',/)
9110 FORMAT(1X,I4,1P8E13.3)
  !
9190 FORMAT(//,' FAST PARTICLE DENSITY PROFILE NO',I4, &
       &	 '  KAPPA_F =',1F7.3,/, &
       &	 ' RADIUS WHERE D/DS(N_FAST) IS MAXIMAL  S =',1F7.3,/, &
       &   ' RADIUS WHERE N_FAST = 1/2 N_FAST_AXIS S =',1F7.3,/)
  !
9195 FORMAT(/,' <BETA_E>  <BETA_I>  <BETA_F>  BETA_E(0)', &
       &	 ' BETA_I(0) BETA_F(0)',//,1X,1P6E10.3,/)
  !
9197 FORMAT(/,' VBIRTH / V_ALFVEN(0) =', 1PE11.4,/)
  !
9200 FORMAT(//,' POWER TRANSFER - DRIFT KINETIC MODEL',/, &
       &		   ' ************************************',//, &
       &	 '  JS         S        ELECTRONS       IONS', &
       &	 '     FAST(HOMO)  FAST(INHOMO) FLUX BALANCE', &
       &	 ' N0_FAST_CRITICAL [M**-3]',//)
9210 FORMAT(1X,I4,1P7E13.3)
9215 FORMAT(//,' TOTAL FLUXES :   ',1P6E13.3,/)
9220 FORMAT(//,2I4,' MARGINAL STABILITY : N_F_0 =',1PE11.3,' [M-3]', &
       &	 '  <BETA_F> =',1PE11.3,'  BETA_F(0) =',1PE11.3,/)
9260 FORMAT(1X,1P12E14.4)
9270 FORMAT(1X,1P5E14.4)
  !
  !
  RETURN
END SUBROUTINE LANDAU
!CC*DECK P5C2S07
SUBROUTINE SLOWIN (PL, PU, PA, PB, PINT)
  !	 -----------------
  !
  !	INTEGRALS I_J'S J=0,1,2 FOR SLOWING-DOWN DISTRIBUTION.
  !	REF. EQS. (54-56) BRUNNER-VACLAVIK DKE.
  !--------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PL, PU, PA, PB, PINT(3), &
       & ZLN, ZATAN, ZPRI0U, ZJ1U, ZPRI1U, ZPRI2U, ZPRI0L, ZJ1L, ZPRI1L, ZPRI2L
  !--------------------------------------------------------------------
  !
  !      PRIMITIVES AT X=PU
  ZLN = LOG ( (PU**2 - PA*PU + PA**2) / (PU + PA)**2 )
  ZATAN = ATAN ( (2.0_RKIND*PU - PA) / (PA*SQRT(3.0_RKIND)) )
  ZPRI0U = ZLN / (6.0_RKIND*PA) + ZATAN / (PA*SQRT(3.0_RKIND))
  ZJ1U = PU + PA * ZLN / 6.0_RKIND - PA * ZATAN / SQRT(3.0_RKIND)
  ZPRI1U = ZJ1U - PB**2 * ZPRI0U
  ZPRI2U = PU**3 / 3.0_RKIND - PA**3 * LOG (PU**3 + PA**3) / 3.0_RKIND &
       &		- 2.0_RKIND * PB**2 * ZJ1U + PB**4 * ZPRI0U
  !
  !      PRIMITIVES AT X = PL
  ZLN = LOG ( (PL**2 - PA*PL + PA**2) / (PL + PA)**2 )
  ZATAN = ATAN ( (2.0_RKIND*PL - PA) / (PA*SQRT(3.0_RKIND)) )
  ZPRI0L = ZLN / (6.0_RKIND*PA) + ZATAN / (PA*SQRT(3.0_RKIND))
  ZJ1L = PL + PA * ZLN / 6.0_RKIND - PA * ZATAN / SQRT(3.0_RKIND)
  ZPRI1L = ZJ1L - PB**2 * ZPRI0L
  ZPRI2L = PL**3 / 3.0_RKIND - PA**3 * LOG (PL**3 + PA**3) / 3.0_RKIND &
       &		- 2.0_RKIND * PB**2 * ZJ1L + PB**4 * ZPRI0L
  !
  !      PUT I_0, I_1, I_2 IN PINT(1), (2), (3), RESPECTIVELY
  PINT(1) = ZPRI0U - ZPRI0L
  PINT(2) = ZPRI1U - ZPRI1L
  PINT(3) = ZPRI2U - ZPRI2L
  !
  RETURN
END SUBROUTINE SLOWIN
!CC*DECK P5C2S99
SUBROUTINE THEEND
  !        -----------------
  !
  !  5.2.99. THE END
  !--------------------------------------------------------------------
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !- -----------------------------------------------------------------
  !
  !CCC     TERMINATE THE PLOTS
  !CCC         IF (NLPLO5(1)) CALL PLOTF
  !
  !     CLOSE DISK FILES
  CALL IODSK5(8)
  !
  RETURN
END SUBROUTINE THEEND
!CC*DECK P5C3S01
SUBROUTINE CENTER (KS,KCHI)
  !        -----------------
  !
  !  5.3.1. MULTIPLY SOLUTION VECTOR BY BASIS FUNCTIONS
  !         SEE EQ.(3.48) OF REF.(1)
  !>>>      MODIFIED TO INCLUDE OPTION NLPHAS
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: KS, KCHI, IPLAC(6), IN2, I0, J, I
  REAL(RKIND) :: ZARG1, ZARG12, ZARG2, ZARG
  COMPLEX :: Z(6), ZI, ZXC, ZVC, ZDXDC, ZDXDS, ZDVDC, ZPHASE
  !---------------------------------------------------------------------
  !
  !     NUMBERING SEQUENCE FOR THE CELL
  CALL DEC (KS,KCHI,NPSI,NPOL,IPLAC)
  !
  !     IDENTIFY UNKNOWNS OF THE CELL WITH TERMS OF SOL. VECT.
  IN2 = NPOL
  I0 = (KS-1)*IN2*2
  !
  DO J=1,6
    I = I0 + IPLAC(J)
    Z(J) = SOL(I)
  END DO
  !
  !--------------------------------------------------------------------
  !                   WITHOUT OPTION NLPHAS
  !
  IF (.NOT.NLPHAS) THEN
    !
    !     MULTIPLY BY BASIS FUNCTIONS
    XC = 0.25_RKIND*( Z(1) + Z(2) + Z(5) + Z(6) )
    VC = 0.5_RKIND *( Z(3) + Z(4) )
    DXDC = ( Z(2) + Z(6) - Z(1) - Z(5) ) *0.5_RKIND/WDCHI
    DXDS = ( Z(5) + Z(6) - Z(1) - Z(2) ) * 0.5_RKIND/WDS
    DVDC = ( Z(4) - Z(3) ) / WDCHI
    !
    !--------------------------------------------------------------------
    !                   WITH OPTION NLPHAS
    !
  ELSE
    !
    ZI = CMPLX (0._RKIND, 1._RKIND)
    !
    !     JUMP CONDITION NEAR CHI=PI
    IF (KCHI.EQ.NCHI) THEN
      ZARG1  = WNTORO * QS    (KS)   * C2PI
      ZARG12 = WNTORO * QTILDA(KS)   * C2PI
      ZARG2  = WNTORO * QS    (KS+1) * C2PI
      Z(2) = Z(2) * CMPLX (COS(ZARG1) , SIN(ZARG1) )
      Z(4) = Z(4) * CMPLX (COS(ZARG12), SIN(ZARG12))
      Z(6) = Z(6) * CMPLX (COS(ZARG2) , SIN(ZARG2) )
    END IF
    !
    !     MULTIPLY WITH BASIS FUNCTIONS
    ZXC = 0.25_RKIND*( Z(1) + Z(2) + Z(5) + Z(6) )
    ZVC = 0.5_RKIND *( Z(3) + Z(4) )
    ZDXDC = ( Z(2) + Z(6) - Z(1) - Z(5) ) *0.5_RKIND/WDCHI
    ZDXDS = ( Z(5) + Z(6) - Z(1) - Z(2) ) * 0.5_RKIND/WDS
    ZDVDC = ( Z(4) - Z(3) ) / WDCHI
    !
    !     BACK TO NON-TRANSFORMED VARIABLES
    !
    !     POLOIDAL PHASE
    ZARG = - WNTORO * WQ * EQ(24,KCHI,ks)
    IF (KCHI.GT.NCHI) THEN
      ZARG = ZARG + WNTORO * WQ * C2PI
    END IF
    !
    ZPHASE = CMPLX ( COS(ZARG), SIN(ZARG) )
    !
    XC = ZXC * ZPHASE
    VC = ZVC * ZPHASE
    !
    DXDC = ( ZDXDC - ZXC * ZI * WNTORO * WT / WTQ ) * ZPHASE
    DXDS = ( ZDXDS - ZXC * ZI * WNTORO * EQ(21,KCHI,ks) ) * ZPHASE
    DVDC = ( ZDVDC - ZVC * ZI * WNTORO * WT / WTQ ) * ZPHASE
    !
    !ccC     BACK TO NON-TRANSFORMED NODAL VALUES
    !ccC     WARNING: VALID FOR SFL JACOBIAN (NER=2, NEGP=0 IN CHEASE)
    !ccC
    !ccC     CHI_SFL AT NODAL POINTS
    !cc         ZCHOL1 = EQ(2,KCHI,ks)
    !cc         ZCHOL2 = EQ(4,KCHI,ks)
    !cc         ZCHOL3 = EQ(2,KCHI,ks)
    !cc         ZCHOL4 = EQ(4,KCHI,ks)
    !cc         ZCHOL5 = EQ(2,KCHI,ks)
    !cc         ZCHOL6 = EQ(4,KCHI,ks)
    !ccC
    !cc         ZARG = - WNTORO * QS(KS) * ZCHOL1
    !cc         Z(1) = Z(1) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !cc         ZARG = - WNTORO * QS(KS) * ZCHOL2
    !cc         Z(2) = Z(2) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !cc         ZARG = - WNTORO * QTILDA(KS) * ZCHOL3
    !cc         Z(3) = Z(3) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !cc         ZARG = - WNTORO * QTILDA(KS) * ZCHOL4
    !cc         Z(4) = Z(4) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !cc         ZARG = - WNTORO * QS(KS+1) * ZCHOL5
    !cc         Z(5) = Z(5) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !cc         ZARG = - WNTORO * QS(KS+1) * ZCHOL6
    !cc         Z(6) = Z(6) * CMPLX (COS(ZARG), SIN(ZARG))
    !ccC
    !ccC     BACK-TRANSFORMED CENTERED VALUES: MULTIPLY BY BASIS FUNCTIONS
    !cc         XC = 0.25*( Z(1) + Z(2) + Z(5) + Z(6) )
    !cc         VC = 0.5 *( Z(3) + Z(4) )
    !cc         DXDC = ( Z(2) + Z(6) - Z(1) - Z(5) ) *0.5/WDCHI
    !cc         DXDS = ( Z(5) + Z(6) - Z(1) - Z(2) ) * 0.5/WDS
    !cc         DVDC = ( Z(4) - Z(3) ) / WDCHI

    !
    !-------------------------------------------------------------------
  END IF
  !
  RETURN
END SUBROUTINE CENTER
!CC*DECK P5C3S02
SUBROUTINE ELECTR
  !        -----------------
  !
  !  5.3.2. ELECTRIC FIELD, NORMAL AND PERP. COMPONENTS
  !         SEE EQ.(3.16B) OF REF.(1)
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZGRPS, ZTRA
  !-------------------------------------------------------------------
  !
  ZGRPS = SQRT(WGRPS2)
  ZTRA = WT * ZGRPS / (2._RKIND * CPSRF * WS)
  !
  ENL = ZTRA * (VC - WBETCH * XC)
  EPL = - XC * WTQ * WBTOT / ZGRPS
  !
  RETURN
END SUBROUTINE ELECTR
!CC*DECK P5C3S03
SUBROUTINE LOCPOW
  !        -----------------
  !
  !  5.3.3. LOCAL POWER ABSORPTION. EQ. (3.38) OF REF.(1).
  !         INCLUDES HARMONIC ION-CYCLOTRON AND ELECTRON LANDAU + TTMP.
  !         COMPUTED FOR EACH SPECIES SEPARATELY.
  !--------------------------------------------------------------------
  USE GLOBALS
  IMPLICIT NONE
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
  !
  INTEGER :: J, ISPC2
  REAL(RKIND) :: ZFACT, ZDV, Z1, Z2
  COMPLEX :: ZI
  !--------------------------------------------------------------------
  !L                1. INITIALISATIONS
  !
  ZI = (0._RKIND,1._RKIND)
  !
  !     FACTORS PI FROM INTEGRATION OVER PHI, B0**2 FROM DEFINITION OF
  !     EPSILON-HAT AND G-HAT (EQ.3.42 AND 3.44 OF THESIS)
  ZFACT = 0.5_RKIND * CPI / (WBTOT2 * OMEGA)
  !
  !     VOLUME OF THE CELL (EQ.3.35 THESIS)
  ZDV = 2._RKIND * WJAC * CPSRF * WS * WDS * WDCHI
  !
  !     WAVE FIELDS
  Z1 = CABS (ENL + ZI*EPL)
  Z1 = Z1 * Z1
  Z2 = AIMAG (CONJG(ENL) * EPL)
  !
  !     FOR EACH SPECIES WE COMPUTE THE POWER ABSORPTION DENSITY DPLJ(J)
  !     AND THE POWER ABSORBED IN THE CELL CPLJ(J). NOTE THAT J=1..NRSPEC
  !     FOR IONS; J=NRSPEC+1 FOR ELECTRONS; J=NRSPEC+2 FOR MODE CONVERTED
  !     (RESONANCE ABSORPTION). THE TOTAL VALUES (SUM OVER SPECIES) ARE
  !     STORED IN DPL AND CPL, RESPECTIVELY.
  !----------------------------------------------------------------------
  !L                2. ION CYCLOTRON DAMPING
  !
  DO J=1,NRSPEC
    DPLJ(J) = ZFACT * ( AIMAG (WEPSI(J)) * Z1 + &
         &                          2._RKIND * AIMAG (WEPSI(J) - WGI(J)) * Z2 )

    CPLJ(J) = DPLJ(J) * ZDV
  END DO
  !
  !----------------------------------------------------------------------
  !L                3. ELECTRON LANDAU DAMPING
  !
  DPLJ(NRSPEC+1) = AIMAG (WEPSEL) * CONJG (EPL) * EPL * ZFACT
  CPLJ(NRSPEC+1) = DPLJ(NRSPEC+1) * ZDV
  !
  !----------------------------------------------------------------------
  !L                4. MODE CONVERTED POWER (DELTA) (EQ.3.94 THESIS)
  !
  DPLJ(NRSPEC+2) = AIMAG (WEPSMC) * Z1 * ZFACT
  CPLJ(NRSPEC+2) = DPLJ(NRSPEC+2) * ZDV
  !
  !----------------------------------------------------------------------
  !L                5. SUM OVER SPECIES
  !
  DPL = 0._RKIND
  CPL = 0._RKIND
  ISPC2 = NRSPEC + 2
  DO J=1,ISPC2
    DPL = DPL + DPLJ(J)
    CPL = CPL + CPLJ(J)
  END DO
  !
  RETURN
END SUBROUTINE LOCPOW
!CC*DECK P5C3S04
SUBROUTINE MAGNET
  !        -----------------
  !
  !  5.3.4. WAVE MAGNETIC FIELD
  !         SEE EQ.(3.18) OF REF.(1)
  !-------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZGRPS, ZR, ZBTOR, ZDSLNT, ZDSR2J, ZBPOL
  COMPLEX   ZI,     ZTRA,   ZA1,    ZA2,    ZA3
  !-------------------------------------------------------------------
  !
  !     NORMAL COMPONENT
  ZI = CMPLX(0._RKIND,1._RKIND)
  ZGRPS = SQRT(WGRPS2)
  ZTRA = WTQ / (WJAC*ZGRPS*ZI*OMEGA)
  BNL = ZTRA * (DXDC + (WDCR2J + ZI*WNTORO*WT/WTQ) * XC)
  !
  !     TOROIDAL AND POLOIDAL COMPONENTS OF ROT E , ZA2 AND ZA3
  ZBPOL = SQRT(WBPOL2)
  ZR = SQRT(WR2)
  ZBTOR = WT / ZR
  ZDSLNT = 2._RKIND*CPSRF*WS * WDPT / WT
  ZDSR2J = 2._RKIND*CPSRF*WS * WDPR2J
  !
  ZTRA = DXDS + DVDC + ZDSLNT * XC
  ZA2 = -ZBTOR*WTQ * ZTRA / (2._RKIND*CPSRF*WS)
  !
  ZTRA = - DXDS - WBETCH*DXDC - (ZDSR2J + WBETCH*WDCR2J)*XC &
       &          + ZI*WNTORO*WT/WTQ * (VC-WBETCH*XC)
  ZA3  = ZBPOL*WTQ * ZTRA / (2._RKIND*CPSRF*WS)
  !
  !     PERP. AND PARALLEL COMPONENTS OF B
  ZTRA = 1._RKIND / (WBTOT * ZI * OMEGA)
  BPL = (-ZBPOL*ZA2 + ZBTOR*ZA3) * ZTRA
  BPARL = (ZBTOR*ZA2 + ZBPOL*ZA3) * ZTRA
  !
  RETURN
END SUBROUTINE MAGNET
!CC*DECK P5C3S05
SUBROUTINE POYNTI
  !        -----------------
  !
  !  5.3.5.  POYNTING VECTOR RE ( E* CROSS B )
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !
  REAL(RKIND) :: ZGRPS
  COMPLEX ::  ZI,    ZTRA
  !---------------------------------------------------------------------
  !
  !     NORMAL COMPONENT,DIRECTLY FROM CENTERED VALUES
  ZI = CMPLX(0._RKIND,1._RKIND)
  ZGRPS = SQRT(WGRPS2)
  !
  ZTRA = WBPOL2 * ( - ZI*WNTORO*WT/WTQ * (VC - WBETCH*XC) &
       &                     + DXDS + WBETCH*DXDC ) &
       &        + WBTOR2 * ( DXDS + DVDC )
  !
  ZTRA = WTQ*WTQ * ZTRA / (2._RKIND*CPSRF*WS*ZGRPS*OMEGA)
  !
  SNL = AIMAG ( CONJG(XC) * ZTRA )
  !
  !     PERP. AND PARALLEL COMPONENTS
  SPL = REAL(-CONJG(ENL) * BPARL)
  SPARL = REAL( CONJG(ENL) * BPL - CONJG(EPL) * BNL)
  !
  RETURN
END SUBROUTINE POYNTI
!CC*DECK P5C3S06
SUBROUTINE SFLINT (KS)
  !        -----------------
  !
  !  5.3.6.  CONTRIBUTION TO POYNTING FLUX ACROSS MAGNETIC SURFACE
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
  IMPLICIT NONE
  !
  INTEGER :: KS
  COMPLEX :: ZI,   ZTRA
  !------------------------------------------------------------------
  !
  ZI = CMPLX(0._RKIND,1._RKIND)
  !
  ZTRA = WBPOL2 * ( - ZI*WNTORO*WT/WTQ * (VC - WBETCH*XC) &
       &                     + DXDS + WBETCH*DXDC ) &
       &        + WBTOR2 * ( DXDS + DVDC )
  !
  ZTRA = WR2*WTQ*WDCHI * ZTRA / (2._RKIND*CPSRF*WS*OMEGA)
  !
  SFLUX(KS) = SFLUX(KS) + AIMAG(CONJG(XC)*ZTRA) *0.5_RKIND*CPI
  !
  RETURN
END SUBROUTINE SFLINT
!CC*DECK P5C3S07
!CC*DECK P5C3S10
!CC*DECK P5C3S08
!CC*DECK P5C3S09
!CC*DECK P5C4S01
SUBROUTINE OUTP5 (K)
  !        ----------------
  !
  !  5.4.1. PRINTOUTS
  !---------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMBAS ! INCLUDE 'COMBAS.inc'
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, J, IROWS, JS, ISPC1, ISPC2, I, JC, JM, JI, ISRCHFGT, JJ
  REAL(RKIND) ::   ZBALAN(MDPSI),   ZFRA(MDSPC2),   ZMEAN(MDSPC2), &
       & ZPMAX, ZHALF, ZFACTOR
  COMPLEX :: ZI
  CHARACTER*8, DIMENSION(16,23) :: NPL
  !
  INCLUDE 'NEWRUN.inc'
  !---------------------------------------------------------------------
  !
  GO TO (100,200,300,400,500,600,700,800,900,1000,1100,1200,1300 &
       &         ,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400 &
       &         ,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500 &
       &         ,3600,3700,3800,3900,4000) , K
  !
  !---------------------------------------------------------------------
  !L.           1. WRITE NAMELIST
  !
100 CONTINUE
  WRITE (NPRNT,NEWRUN)
  WRITE(NPRNT,9010)
  RETURN
  !
  !----------------------------------------------------------------------
  !L.           2. POWER ABSORPTION AND FLUX FUNCTION OF S
  !
200 CONTINUE
  !
  DO J=1,NPSI
    ZBALAN(J) = FLUPOW(J) + SFLUX(J)
  END DO
  !
  WRITE(NPRNT,9020)
  WRITE(NPRNT,9022) (I,CCS(I),ABSPOW(I),FLUPOW(I),SFLUX(I), &
       &                      ZBALAN(I),I=1,NPSI)
  WRITE(NPRNT,9021) FLUPOW(NPSI+1)
  !
  IROWS = 23
  ZPMAX = MAX(0._RKIND,MAXVAL(ABSPOW(1:NPSI)))
  ZPMAX = 1.2_RKIND*ZPMAX
  CALL BLANK (IROWS,NPL)
  CALL ONEFUN (1,NPSI,CCS,FLUPOW,'*',0._RKIND,2,1._RKIND,127, &
       &                0._RKIND,FLUPOW(NPSI),IROWS,NPL,.FALSE.)
  CALL ONEFUN (1,NPSI,CCS,ABSPOW,'.',0._RKIND,2,1._RKIND,127, &
       &                0._RKIND,ZPMAX,IROWS,NPL,.TRUE.)
  CALL FRAME (2,127,IROWS,NPL)
  WRITE (NPRNT,9023) ((NPL(JI,JJ),JI=1,16),JJ=1,IROWS)
  !
  WRITE (NPRNT,9028) SMEAN
  !
  !     POWER FRACTIONS, MEAN RADII OF ABSORPTION AND FLUXES PER SPECIES
  ISPC1 = NRSPEC + 1
  ISPC2 = NRSPEC + 2
  !
  DO J=1,ISPC2
    ZFRA(J) = FLUPSP(NPSI+1,J) * 100._RKIND / FLUPOW(NPSI+1)
  END DO
  !
  DO J=1,ISPC2
    !
    ZHALF = FLUPSP(NPSI+1,J) * 0.5_RKIND
    I = ISRCHFGT(NPSI+1,FLUPSP(1,J),1,ZHALF)
    !
    !CC	    DO 220 JS = 1, NPSI+1
    !CC	    IF (FLUPSP(JS,J).LT.ZHALF) GO TO 220
    !CC	    I = JS
    !CC	    GO TO 221
    !CC  220	    CONTINUE
    !CCC
    !CC  221	    CONTINUE
    IF (I.GT.NPSI) THEN
      ZMEAN(J) = 0._RKIND
    ELSE
      ZMEAN(J) = CCS(I-1) + (ZHALF-FLUPSP(I-1,J)) * &
           &         (CCS(I)-CCS(I-1)) / (FLUPSP(I,J)-FLUPSP(I-1,J))
    END IF
    !
  END DO
  !
  WRITE (NPRNT,9920) ISPC1,ISPC2,(J,J=1,ISPC2)
  WRITE (NPRNT,9921) (ACHARG(J),AMASS(J),J=1,NRSPEC)
  WRITE (NPRNT,9922) (ZFRA(J),J=1,ISPC2)
  WRITE (NPRNT,9927) (ZMEAN(J),J=1,ISPC2)
  WRITE (NPRNT,9923) (J,J=1,ISPC2)
  WRITE (NPRNT,9926)
  DO JS=1,NPSI
    WRITE (NPRNT,9924) CCS(JS),(FLUPSP(JS,J),J=1,ISPC2)
  END DO
  WRITE (NPRNT,9925) (FLUPSP(NPSI+1,J),J=1,ISPC2)
  !
  !     POWER ABSORPTION DENSITY AVERAGED OVER PSI=CONST SURFACES
  !     MULTIPLY WITH TOTAL POWER(WATTS) TO OBTAIN WATTS/M**3
  WRITE(NPRNT,9025) ISPC1,ISPC2,(J,J=1,ISPC2)
  WRITE (NPRNT,9921) (ACHARG(J),AMASS(J),J=1,NRSPEC)
  WRITE (NPRNT,'(//)')
  DO JS = 1, NPSI
    WRITE(NPRNT,9924) CCS(JS), DEPOS(JS), &
         &                        (DEPPSP(JS,J),J=1,ISPC2)
  END DO
  RETURN
  !
  !----------------------------------------------------------------------
  !L.               3. POWER ABSORPTION FUNCTION OF CHI
  !
300 CONTINUE
  WRITE(NPRNT,9030)
  WRITE(NPRNT,9032) (JC,CCHI(JC),CHIPOW(JC),JC=1,NPOL)
  IROWS = 23
  ZPMAX = 0._RKIND
  DO JC=1,NPOL
    ZPMAX = MAX(CHIPOW(JC),ZPMAX)
    IF (CCHI(JC).LT.0._RKIND) CCHI(JC) = CCHI(JC) + C2PI
  END DO
  ZPMAX = 1.2_RKIND*ZPMAX
  CALL BLANK (IROWS,NPL)
  CALL ONEFUN (1,NPOL,CCHI,CHIPOW,'.',0._RKIND,2,C2PI,127, &
       &                0._RKIND,ZPMAX,IROWS,NPL,.TRUE.)
  CALL FRAME (2,127,IROWS,NPL)
  WRITE (NPRNT,9023) ((NPL(JI,JJ),JI=1,16),JJ=1,IROWS)
  DO JC=1,NPOL
    IF (CCHI(JC).GT.CPI) CCHI(JC) = CCHI(JC) - C2PI
  END DO
  WRITE(NPRNT,9034) CMEAN,CDEVIA
  RETURN
  !
  !--------------------------------------------------------------------
  !L.               4. POWER ABSORPTION DENSITY
  !
400 CONTINUE
  WRITE(NPRNT,9040)
  CALL TABLE2(DENPOW,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.               5. POWER ABSORBED IN EACH CELL
  !
500 CONTINUE
  WRITE(NPRNT,9050)
  CALL TABLE2(CELPOW,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.               6. NORMAL COMPONENT OF POYNTING
  !
600 CONTINUE
  WRITE(NPRNT,9060)
  CALL TABLE2(SN,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.               7. PERP. COMPONENT OF POYNTING
  !
700 CONTINUE
  WRITE(NPRNT,9070)
  CALL TABLE2(SPERP,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.               8. PARALLEL COMPONENT OF POYNTING
  !
800 CONTINUE
  WRITE(NPRNT,9080)
  CALL TABLE2(SPAR,-1._RKIND)
  RETURN
  !
900 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.              10. REAL PART OF E_N
  !
1000 CONTINUE
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = REAL(EN(JS,JC))
    END DO
  END DO
  WRITE(NPRNT,9100)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.              11. REAL PART OF E_B
  !
1100 CONTINUE
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = REAL(EP(JS,JC))
    END DO
  END DO
  WRITE(NPRNT,9110)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.              12. IMAGINARY PART OF E_N
  !
1200 CONTINUE
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = AIMAG(EN(JS,JC))
    END DO
  END DO
  WRITE(NPRNT,9120)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !---------------------------------------------------------------
  !L.                13. IMAGINARY PART OF E_B
  !
1300 CONTINUE
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = AIMAG(EP(JS,JC))
    END DO
  END DO
  WRITE(NPRNT,9130)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !---------------------------------------------------------------
  !L.                14. LEFT-HAND POLARISATION E+
  !
1400 CONTINUE
  ZI = CMPLX(0._RKIND,1._RKIND)
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = CABS( EN(JS,JC) + ZI*EP(JS,JC) )**2
    END DO
  END DO
  WRITE(NPRNT,9140)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !---------------------------------------------------------------
  !L.                15. RIGHT-HAND POLARISATION E-
  !
1500 CONTINUE
  ZI = CMPLX(0._RKIND,1._RKIND)
  DO JC=1,NPOL
    DO JS=1,NPSI
      VOUT1(JS,JC) = CABS( EN(JS,JC) - ZI*EP(JS,JC) )**2
    END DO
  END DO
  WRITE(NPRNT,9150)
  CALL TABLE2(VOUT1,-1._RKIND)
  RETURN
  !
  !------------------------------------------------------------------
  !L.         16. ELECTRIC FIELD ON OUTER EQUATORIAL PLANE (CHI=0)
  !
1600 CONTINUE
  WRITE(NPRNT,9160)
  DO JS = 1, NPSI
    WRITE(NPRNT,9162) JS, CCS(JS), ENCHI0(JS), ENPHI0(JS), &
         &                                     EPCHI0(JS), EPPHI0(JS)
  END DO
  RETURN
1700 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.         18. ABS. OF FOURIER COMPONENTS OF E_N IN THETA
  !
1800 CONTINUE
  WRITE(NPRNT,9180)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FREN2(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !------------------------------------------------------------------
  !L.         19. ABS. OF FOURIER COMPONENTS OF E_B IN THETA
  !
1900 CONTINUE
  WRITE(NPRNT,9190)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FREP2(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !------------------------------------------------------------------
  !L.           20. ABS. OF FOURIER COMPONENTS OF E_N IN CHI
  !
2000 CONTINUE
  WRITE(NPRNT,9200)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FREN(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !--------------------------------------------------------------------
  !L.          21. ABS. OF FOURIER COMPONENTS OF E_B IN CHI
  !
2100 CONTINUE
  WRITE(NPRNT,9210)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FREP(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !--------------------------------------------------------------------
  !L.          22. DISPERSION RELATION RE(EPS) - (N/R)**2
  !
2200 CONTINUE
  WRITE(NPRNT,9220)
  CALL TABLE2 (DSPREL,-1._RKIND)
  RETURN
  !
  !--------------------------------------------------------------------
  !L.          23. IMAGINARY PART OF EPSILON
  !
2300 CONTINUE
  WRITE(NPRNT,9230)
  CALL TABLE2 (AIMEPS,-1._RKIND)
  RETURN
  !
  !-------------------------------------------------------------------
  !L.          24. OMEGA - OMEGACI
  !
2400 CONTINUE
  WRITE(NPRNT,9240)
  CALL TABLE2 (OMCLIN,-1._RKIND)
  RETURN
  !
  !--------------------------------------------------------------------
  !L.          25. CONTINUUM ALFVEN FREQUENCIES, W<W_ci, NO TOROIDICITY
  !
2500 CONTINUE
  ZFACTOR = FREQCY / OMEGA
  WRITE(NPRNT,9250) WNTORO, ZFACTOR
  WRITE(NPRNT,9251) (JM,JM=MFL,MFU)
  !
  DO JS=1,NPSI
    WRITE(NPRNT,9252) CCS(JS),(FRALF(JM,JS),JM=1,MD2FP1)
  END DO
  !
  WRITE(NPRNT,9251) (JM,JM=MFL,MFU)
  !
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         26. MASS AND ELECTRON DENSITIES AND Q ON LOW FIELD SIDE
  !               EQUATORIAL PLANE (CHI=0)
2600 CONTINUE
  WRITE(NPRNT,9260)
  DO JS=1,NPSI
    WRITE(NPRNT,9262) JS,CCS(JS),(EQRDEN(J,JS),J=1,MDDEN)
  END DO
  RETURN
  !
  !---------------------------------------------------------------------
  !
2700 CONTINUE
  RETURN
2800 CONTINUE
  RETURN
2900 CONTINUE
  RETURN
3000 CONTINUE
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         31. ABS OF FOURIER COMPONENTS OF B_N IN THETA
  !
3100 CONTINUE
  WRITE(NPRNT,9310)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBN2(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         32. ABS OF FOURIER COMPONENTS OF B_B IN THETA
  !
3200 CONTINUE
  WRITE(NPRNT,9320)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBB2(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         33. ABS OF FOURIER COMPONENTS OF B_PAR IN THETA
  !
3300 CONTINUE
  WRITE(NPRNT,9330)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBPA2(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         34. ABS OF FOURIER COMPONENTS OF B_N IN CHI
  !
3400 CONTINUE
  WRITE(NPRNT,9340)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBN(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         35. ABS OF FOURIER COMPONENTS OF B_B IN CHI
  !
3500 CONTINUE
  WRITE(NPRNT,9350)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBB(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         36. ABS OF FOURIER COMPONENTS OF B_PAR IN CHI
  !
3600 CONTINUE
  WRITE(NPRNT,9360)
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      FROUT(JM,JS) = CABS(FRBPAR(JM,JS))
    END DO
  END DO
  CALL TABLEF (FROUT)
  RETURN
  !
  !--------------------------------------------------------------------
  !
3700 CONTINUE
  RETURN
3800 CONTINUE
  RETURN
3900 CONTINUE
  RETURN
4000 CONTINUE
  RETURN
  !
  !---------------------------------------------------------------------
  !
9010 FORMAT(///,' ABOVE NAMELIST IS OUTPUT 5.1',/,1X,28('-'),///)
9020 FORMAT(////,' OUTPUT 5.2',/,1X,10('-'),//, &
       &         ' ABSORBED POWER AND ENERGY FLUX ON PSI=CTE SURFACES',/, &
       &         1X,50('*'),//, &
       &         2X,'JS',10X,'S',12X,'POWER ABSORBED',8X, &
       &         'POWER FLUX',8X,'POYNTING FLUX',10X,'BALANCE',//)
9021 FORMAT(8X,'1.0000000E+00',26X,1PE14.7)
9022 FORMAT(1X,I3,1PE17.7,1P4E20.7)
9023 FORMAT(///(1X,16A8))
9024 FORMAT(///,' DERIVATIVE OF POYNTING FLUX VS.POWER ABSORBED',/, &
       &   1X,45('*'),//,10X,'S',12X,'POWER ABSORBED',8X, &
       &   'D(SFLUX)/DS',10X,'BALANCE',//)
9025 FORMAT(///,' POWER DEPOSITION PROFILE AVERAGED ON PSI=CONST', &
       &            /,' **********************************************', &
       &           //,' TO OBTAIN WATTS PER CUBIC METERS MULTIPLY WITH', &
       &              ' TOTAL EXPERIMENTAL POWER', &
       &           //,' SPECIES NR.',I3,' ===> ELECTRONS', &
       &    /,' SPECIES NR.',I3,' ===> PHENOMENOLOGICAL DAMPING',//, &
       &      ' SPECIES NR.:   TOTAL   ',I6,11I11)
9026 FORMAT(4(5X,1PE10.3,5X))
9027 FORMAT(3(5X,1PE10.3,5X))
9028 FORMAT(///,' MEAN RADIUS OF ABSORPTION',1PE13.4,/,1X,25('*'), &
       &          //)
9920 FORMAT (///,' POWER ABSORPTION AND FLUXES PER SPECIES',/,1X, &
       &               39('*'),///,' SPECIES NR.',I3,' ===> ELECTRONS', &
       &       /,' SPECIES NR.',I3,' ===> PHENOMENOLOGICAL DAMPING',//, &
       &         ' SPECIES NR.:',I6,11I11)
9921 FORMAT (/,' Z / A      :',12(F6.0,'/',F3.0,' '))
9922 FORMAT (/,' FRACTION OF',/,' POWER (%)  :',F9.3,11F11.3)
9923 FORMAT (//,' FLUXES :',/,1X,6('-'),//,'       S',12I11)
9924 FORMAT (2X,1P12E11.3)
9925 FORMAT ('    1.000E+00',1P12E11.3)
9926 FORMAT (/)
9927 FORMAT (/,' MEAN RADIUS',/,' OF ABSORPT.:',F9.3,11F11.3)
  !
9030 FORMAT(////,' OUTPUT 5.3',/,1X,10('-'),//, &
       &   ' ABSORBED POWER ON CHI=CTE SURFACES',/,1X,34('*'),//, &
       &   1X,'JCHI',8X,'CHI',11X,'POWER ABSORBED',//)
9032 FORMAT(1X,I3,1PE15.3,1PE20.3)
9034 FORMAT(///' MEAN ANGLE OF ABSORPTION',1PE13.4,/,1X,24('*'), &
       &   //,' WIDTH OF THE DISTRIBUTION',1PE13.4,/,1X,25('*'),//)
  !
9040 FORMAT(////,' OUTPUT 5.4',/,1X,10('-'),//, &
       &   ' POWER ABSORPTION DENSITY',/,1X,24('*'),//)
9050 FORMAT(////,' OUTPUT 5.5',/1X,10('-'),//, &
       &   ' POWER ABSORBED IN EACH CELL',/,1X,27('*'),//)
9060 FORMAT(////' OUTPUT 5.6',/,1X,10('-'),//, &
       &   ' NORMAL COMPONENT OF POYNTING',/,1X,28('*'),//)
9070 FORMAT(////' OUTPUT 5.7',/,1X,10('-'),//, &
       &   ' PERP COMPONENT OF POYNTING',/,1X,26('*'),//)
9080 FORMAT(////' OUTPUT 5.8',/,1X,10('-'),//, &
       &   ' PARALLEL COMPONENT OF POYNTING',/,1X,30('*'),//)
  !
9100 FORMAT(////,' OUTPUT 5.10',/,1X,11('-'),//, &
       &   ' REAL PART OF E_N',/,1X,16('*'),//)
9110 FORMAT(////,' OUTPUT 5.11',/,1X,11('-'),//, &
       &   ' REAL PART OF E_B',/,1X,16('*'),//)
9120 FORMAT(////,' OUTPUT 5.12',/,1X,11('-'),//, &
       &   ' IMAGINARY PART OF E_N',/,1X,16('*'),//)
9130 FORMAT(////,' OUTPUT 5.13',/,1X,11('-'),//, &
       &   ' IMAGINARY PART OF E_B',/,1X,16('*'),//)
9140 FORMAT(////' OUTPUT 5.14',/,1X,11('-'),//, &
       &   ' LEFT-HAND POLARISATION (E+)',/,1X,27('*'),//)
9150 FORMAT(////' OUTPUT 5.15',/,1X,11('-'),//, &
       &   ' RIGHT-HAND POLARISATION (E-)',/,1X,28('*'),//)
  !
9160 FORMAT(////,' OUTPUT 5.16',/,' -----------',//, &
       &   ' ELECTRIC FIELD ON OUTER EQUATORIAL PLANE (CHI=0)',/, &
       &   ' ************************************************',//, &
       &   '   JS      S',11X,'ABS(EN)',7X,'PHI(EN)',7X,'ABS(EP)',7X, &
       &   'PHI(EP)',//)
9162 FORMAT(1X,I4,1P5E14.5)
  !
9180 FORMAT(////,' OUTPUT 5.18',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF E_N IN THETA ANGLE',/, &
       &   1X,49('*'),//)
9190 FORMAT(////,' OUTPUT 5.19',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF E_B IN THETA ANGLE',/, &
       &   1X,49('*'),//)
  !
9200 FORMAT(////,' OUTPUT 5.20',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF E_N IN CHI ANGLE',/, &
       &   1X,47('*'),//)
9210 FORMAT(////,' OUTPUT 5.21',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF E_B IN CHI ANGLE',/, &
       &   1X,47('*'),//)
  !
9220 FORMAT(////,' OUTPUT 5.22',/,1X,11('-'),//, &
       &   ' DISPERSION RELATION RE(EPS) - (N/R)**2',/, &
       &   1X,38('*'),//)
9230 FORMAT(////,' OUTPUT 5.23',/,1X,11('-'),//, &
       &   ' IMAGINARY PART OF EPSILON',/,1X,25('*'),//)
9240 FORMAT(////,' OUTPUT 5.24',/,1X,11('-'),//, &
       &   ' OMEGA - OMEGACI',/,1X,15('*'),//)
  !
9250 FORMAT(////,' OUTPUT 5.25',/,1X,11('-'),//, &
       &   ' CONTINUUM ALFVEN FREQUENCIES FOR N =',F10.4,/, &
       &   ' **********************************************',/, &
       &   ' OMEGA<OMEGA_CI (2ND ORDER). WITHOUT TOROIDICITY.',/, &
       &   ' ***********************************************',/, &
       &   ' NORMALIZED TO ALFVEN TRANSIT TIME R_0/V_A0',/, &
       &   ' ******************************************',/, &
       &   ' MULTIPLY WITH ',1PE13.5,' TO OBTAIN [HZ]',/, &
       &   ' ******************************************',/)
9251 FORMAT(/,6X,'S',5X,15('M=',I3,3X),/)
9252 FORMAT(1X,16F8.3)
  !
9260 FORMAT(////,' OUTPUT 5.26',/,' -----------',//, &
       &   ' DENSITY AND Q VS RADIUS ON OUTER EQUATORIAL PLANE (CHI=0)',/, &
       &   ' *********************************************************',//, &
       &   '   JS      S',5X,'R MAJ',4X,'R MIN',2X,'R MAJ (M)',2X, &
       &   'R MIN (M)',2X,'RHO',6X,'Q',4X,'OMXTAE',3X,'NE (M-3)'//)
9262 FORMAT(1X,I4,8F9.4,1PE13.4)
  !
9310 FORMAT(////,' OUTPUT 5.31',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_N IN THETA ANGLE',/, &
       &   1X,49('*'),//)
9320 FORMAT(////,' OUTPUT 5.32',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_B IN THETA ANGLE',/, &
       &   1X,49('*'),//)
9330 FORMAT(////,' OUTPUT 5.33',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_PAR IN THETA ANGLE',/, &
       &   1X,51('*'),//)
  !
9340 FORMAT(////,' OUTPUT 5.34',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_N IN CHI ANGLE',/, &
       &   1X,47('*'),//)
9350 FORMAT(////,' OUTPUT 5.35',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_B IN CHI ANGLE',/, &
       &   1X,47('*'),//)
9360 FORMAT(////,' OUTPUT 5.36',/,1X,11('-'),//, &
       &   ' NORMS OF FOURIER COMPONENTS OF B_PAR IN CHI ANGLE',/, &
       &   1X,49('*'),//)
  !
  !----------------------------------------------------------------------
  !
END SUBROUTINE OUTP5
!CC*DECK P5C4S02
SUBROUTINE TABLE2 (PMAT,PMAX)
  !        -----------------
  !
  !  5.4.2. PRINTOUT OF PMAT(PSI,CHI)
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  INTEGER :: JC, JS, I
  REAL(RKIND) :: PMAX,  PMAT(MDPSI,MDPOL), ZMAX, ZFACT
  !-----------------------------------------------------------------------
  !
  !     PREPARE THE INTEGER MATRIX MOUT
  ZMAX = PMAX
  IF(PMAX.GT.0.0_RKIND) GO TO 150
  ZMAX = 0.0_RKIND
  DO JC=1,NPOL
    DO JS=1,NPSI
      IF (ABS(PMAT(JS,JC)).GT.ZMAX) ZMAX=ABS(PMAT(JS,JC))
    END DO
  END DO
  !
150 CONTINUE
  ZFACT = 999._RKIND/ZMAX
  DO JC=1,NPOL
    DO JS=1,NPSI
      MOUT(JS,JC) = IFIX(PMAT(JS,JC)*ZFACT)
    END DO
  END DO
  !
  !     PRINTOUT
  !
  ZFACT = 1._RKIND/ZFACT
  WRITE(NPRNT,9400) ZMAX,ZFACT
  WRITE(NPRNT,9000) (I,I=1,NCHI)
  DO JS=1,NPSI
    WRITE(NPRNT,9100) JS,(MOUT(JS,JC),JC=1,NCHI)
  END DO
  WRITE(NPRNT,9150) (I,I=1,NCHI)
  WRITE(NPRNT,9200) (I,I=NCHI,NPOL)
  DO JS=1,NPSI
    WRITE(NPRNT,9100) JS,(MOUT(JS,JC),JC=NCHI,NPOL)
  END DO
  WRITE(NPRNT,9150) (I,I=NCHI,NPOL)
  !
  RETURN
  !-------------------------------------------------------------------
9000 FORMAT(1X,'--- UPPER HALF-PLANE ---',//,5(5X,31I4,/),/)
9100 FORMAT(/,1X,32I4,/,4(5X,31I4,/))
9150 FORMAT(/,5(5X,31I4,/))
9200 FORMAT(//,1X,'--- LOWER HALF-PLANE ---',//,5(5X,31I4,/),/)
9400 FORMAT(///,' ABSOLUTE MAX. VALUE',1PE14.4,//, &
       &          ' NORM. FACTOR',1PE14.4,///)
  !-------------------------------------------------------------------
END SUBROUTINE TABLE2
!CC*DECK P5C4S03
SUBROUTINE TABLEF (PMAT)
  !        -----------------
  !
  !     5.4.3. PRINTOUT OF FOURIER COMPONENTS
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: JM, JS
  REAL(RKIND) :: PMAT(MD2FP1, NPSI),   ZAVM(MD2FP1), ZMAX, ZFACT
  !------------------------------------------------------------------
  !
  !     SEARCH MAXIMUM VALUE OF PMAT
  ZMAX = 0._RKIND
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      IF (ABS(PMAT(JM,JS)).GT.ZMAX) ZMAX = ABS(PMAT(JM,JS))
    END DO
  END DO
  !
  ZFACT = 10000.5_RKIND/ZMAX
  !
  !     NORMALIZE PMAT
  DO JM=1,MD2FP1
    DO JS=1,NPSI
      MOUTF(JM,JS) = IFIX(PMAT(JM,JS)*ZFACT)
    END DO
  END DO
  !
  !     PRINT OUT THE TABLE MOUTF
  WRITE(NPRNT,9001) (JM, JM=MFL,MFU)
  DO JS=1,NPSI
    WRITE(NPRNT,9100) CCS(JS),(MOUTF(JM,JS),JM=1,MD2FP1)
  END DO
  WRITE(NPRNT,9001) (JM, JM=MFL,MFU)
  !
  !     AVERAGE AND MAXIMUM VALUES
  DO JM=1,MD2FP1
    ZAVM(JM) = 0._RKIND
    DO JS=1,NPSI
      ZAVM(JM) = ZAVM(JM) + REAL(MOUTF(JM,JS),RKIND)
    END DO
    ZAVM(JM) = ZAVM(JM) / REAL(NPSI,RKIND)
  END DO
  WRITE(NPRNT,9200) (ZAVM(JM),JM=1,MD2FP1)
  WRITE(NPRNT,9300)  ZMAX
  !
  RETURN
  !------------------------------------------------------------------
9001 FORMAT(/,6X,'S',6X,15('M=',I3,2X),/)
9100 FORMAT(1X,F8.4,2X,15I7)
9200 FORMAT(//,' AVERAGE VALUES',/,11X,15F7.1)
9300 FORMAT(//,' MAXIMUM VALUE',1PE13.3)
  !---------------------------------------------------------------------
END SUBROUTINE TABLEF
!CC*DECK P5C4S04
SUBROUTINE PLOT5 (K)
  !        ----------------
  !
  !  5.4.4. GRAPHICAL PLOTS
  !--------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: K, JC, JS, ITOR, INODES, IFIELD, I0, JM, imin, imax, indexmin, indexmax, &
       & NBASCO, NBASFI, NMATFRI, NMATFO, NMATFA, NMATFP
  REAL(RKIND) :: ZRMAG, ZZMAG, ZZ, ZNORM, ZSINB, ZCOSB, ZER, ZEZ, ZDP, ZA, ZENORM, SSUM
  CHARACTER :: TEXT*10
  COMPLEX :: ZEIMPH,    ZI
  !--------------------------------------------------------------------
  !
  GO TO (100,200,300,400,500,600,700,800,900,1000,1100, &
       &          1200,1300,1400,1500) , K
  !
  !--------------------------------------------------------------------
  !L.               1. INITIALIZE THE PLOT
  !
100 CONTINUE
  !
  !     OPEN FILES
  !
  NBASCO = 18
  NBASFI = 19
  NMATFRI = 24
  NMATFO = 25
  NMATFA = 26
  NMATFP = 27
  OPEN (UNIT=NBASCO, FILE='TAPE18', STATUS='NEW', &
       &         FORM='FORMATTED')
  OPEN (UNIT=NBASFI, FILE='TAPE19', STATUS='NEW', &
       &         FORM='FORMATTED')
  OPEN (UNIT=NMATFRI, FILE='TAPE24', STATUS='NEW', &
       &         FORM='FORMATTED')
  OPEN (UNIT=NMATFO, FILE='TAPE25', STATUS='NEW', &
       &         FORM='FORMATTED')
  OPEN (UNIT=NMATFA, FILE='TAPE26', STATUS='NEW', &
       &         FORM='FORMATTED')
  OPEN (UNIT=NMATFP, FILE='TAPE27', STATUS='NEW', &
       &         FORM='FORMATTED')
  !
  !     WRITE CELLS COORDINATES (R,Z) ON FILE TAPE18 (FOR BASPL)
  !
  !     FIND COORDINATES OF MAGNETIC AXIS
  ZRMAG = 0.0_RKIND
  ZZMAG = 0.0_RKIND
  DO JC = 1, NPOL
    ZRMAG = ZRMAG + CCR(1,JC) / NPOL
    ZZMAG = ZZMAG + CCZ(1,JC) / NPOL
  END DO
  !
  !     LEAVE A SMALL 'HOLE' AROUND MAGNETIC AXIS
  DO JC = 1, NPOL
    CCR(0,JC) = 0.01_RKIND * (CCR(1,JC)-ZRMAG) + ZRMAG
    CCZ(0,JC) = 0.01_RKIND * (CCZ(1,JC)-ZZMAG) + ZZMAG
  END DO
  !
  !     CLOSE THE DOMAIN POLOIDALLY
  DO JS = 0, NPSI
    CCR(JS,NPOL+1) = CCR(JS,1)
    CCZ(JS,NPOL+1) = CCZ(JS,1)
  END DO
  !
  !     WRITE THE COORDS FILE (TAPE18) FOR BASPL
  IF (NPLTYP.EQ.1) THEN
    ITOR=1
    WRITE(NBASCO,9012) NPOL+1, NPSI+1, ITOR
    ZZ=0.0_RKIND
    DO JS = 0, NPSI
      DO JC = 1, NPOL+1
        WRITE(NBASCO,9015) CCR(JS,JC), CCZ(JS,JC), ZZ
      END DO
    END DO
  END IF
  !
9012 FORMAT (3I5)
9015 FORMAT (1P3E16.8)
  !
  RETURN
  !
  !-----------------------------------------------------------------
  !L.              2. RADIAL POWER ABSORPTION
  !
200 CONTINUE
  RETURN
  !
  !-----------------------------------------------------------------
  !L.               3. FAST ION MARGINAL STABILITY (DK)
  !                    SEE IN SUBROUTINE LANDAU : WRITE ON TAPE26
  !
300 CONTINUE
  RETURN
  !
  !----------------------------------------------------------------
  !L.               4. 2-D PLOTS IN (R,Z) SPACE
  !
400 CONTINUE
  !
  !     CONSTRUCT COMPONENTS IN CARTESIAN (R,Z) COORDINATES
  !
  DO JC=1,NPOL
    DO JS=1,NPSI
      ZNORM = SQRT ( CNR(JS,JC)**2 + CNZ(JS,JC)**2 )
      ZSINB = CNZ(JS,JC) / ZNORM
      ZCOSB = CNR(JS,JC) / ZNORM
      ER(JS,JC) = REAL (EN(JS,JC)) * ZCOSB &
           &	              - REAL (EP(JS,JC)) * ZSINB * COST(JS,JC)
      EZ(JS,JC) = REAL (EN(JS,JC)) * ZSINB &
           &		      + REAL (EP(JS,JC)) * ZCOSB * COST(JS,JC)
    END DO
  END DO
  !
  !     EXTRAPOLATE QUANTITIES TO MAGNETIC AXIS
  !
  ZER = SSUM (NPOL,ER(1,1),MDPSI1) / NPOL
  ZEZ = SSUM (NPOL,EZ(1,1),MDPSI1) / NPOL
  ZDP = SSUM (NPOL,DENPOW(1,1),MDPSI1) / NPOL
  DO JC=1,NPOL
    ER(0,JC) = ZER
    EZ(0,JC) = ZEZ
    EN(0,JC) = EN(1,JC)
    EP(0,JC) = EP(1,JC)
    BN(0,JC) = BN(1,JC)
    BB(0,JC) = BB(1,JC)
    BPAR(0,JC) = BPAR(1,JC)
    DENPOW(0,JC) = ZDP
  END DO
  !
  !     CLOSE THE DOMAIN POLOIDALLY
  !
  DO JS = 0, NPSI
    ER(JS,NPOL+1) = ER(JS,1)
    EZ(JS,NPOL+1) = EZ(JS,1)
    EN(JS,NPOL+1) = EN(JS,1)
    EP(JS,NPOL+1) = EP(JS,1)
    BN(JS,NPOL+1) = BN(JS,1)
    BB(JS,NPOL+1) = BB(JS,1)
    BPAR(JS,NPOL+1) = BPAR(JS,1)
    DENPOW(JS,NPOL+1) = DENPOW(JS,1)
  END DO
  !
  !     WRITE FIELD COMPONENTS TO FILE TAPE19 FOR BASPL
  !
  IF (NPLTYP.EQ.1) THEN
    ZA = 1.0_RKIND
    INODES = (NPSI+1) * (NPOL+1)
    !CC>>	 IFIELD = 9
    IFIELD = 6
    WRITE(19,9042) INODES, IFIELD
    DO JS = 0, NPSI
      DO JC = 1, NPOL+1
        ZENORM = SQRT ( ( CABS (EN(JS,JC)) ) **2 + &
             &			    ( CABS (EP(JS,JC)) ) **2 )
        WRITE(19,9045) &
             &      REAL(EN(JS,JC)),  REAL(EP(JS,JC)), &
             &	    REAL(BN(JS,JC),RKIND), REAL(BB(JS,JC),RKIND), REAL(BPAR(JS,JC)), &
             &	    ZA
        !CC>>+     +      ER(JS,JC), EZ(JS,JC), DENPOW(JS,JC),

        !CC     +			   AIMAG(EN(JS,JC)), AIMAG(EP(JS,JC)),
        !CC     +			   ZENORM, ZA
      END DO
    END DO
  END IF
  !
  !     WRITE COORDINATES AND FIELD COMPONENTS TO TAPE19 FOR EXPLORER
  !
  IF (NPLTYP.EQ.2) THEN
    ITOR   = 1
    IFIELD = 8
    I0     = 0
    ZZ     = 0.0_RKIND
    WRITE(19,9052) NPOL+1, NPSI/2+1, ITOR, IFIELD, I0
    DO JS = 0, NPSI, 2
      DO JC = 1, NPOL+1
        ZENORM = SQRT ( ( CABS (EN(JS,JC)) ) **2 + &
             &			       ( CABS (EP(JS,JC)) ) **2 )
        WRITE(19,9055) CCR(JS,JC), CCZ(JS,JC), ZZ, &
             &	       ER(JS,JC), EZ(JS,JC), DENPOW(JS,JC), &
             &	       REAL(EN(JS,JC)), REAL(EP(JS,JC)), &
             &	       REAL(BN(JS,JC),RKIND), REAL(BB(JS,JC),RKIND), REAL(BPAR(JS,JC))
        !CC     +			AIMAG(EN(JS,JC)), AIMAG(EP(JS,JC)),
        !CC     +			ZENORM
      END DO
    END DO
  END IF
  !
9042 FORMAT(2I5)
9045 FORMAT(1P9E13.5)
  !
9052 FORMAT(5I5)
9055 FORMAT(1P11E12.4)
  RETURN
  !
  !------------------------------------------------------------------
  !L.               5. POLOIDAL FOURIER DECOMPOSITION OF WAVEFIELDS
  !                    FOR USE WITH MATLAB
  !
500 CONTINUE

  DO JS = 1, NPSI
    WRITE(25, 9053) CCS(JS), &
         &                         (CABS(FREN(JM,JS)), JM = 1, MD2FP1)
  END DO
  DO JS = 1, NPSI
    WRITE(25, 9053) CCS(JS), &
         &                         (CABS(FREP(JM,JS)), JM = 1, MD2FP1)
  END DO
  DO JS = 1, NPSI
    WRITE(25, 9053) CCS(JS), &
         &                         (CABS(FRBN(JM,JS)), JM = 1, MD2FP1)
  END DO
  DO JS = 1, NPSI
    WRITE(25, 9053) CCS(JS), &
         &                         (CABS(FRBB(JM,JS)), JM = 1, MD2FP1)
  END DO
  DO JS = 1, NPSI
    WRITE(25, 9053) CCS(JS), &
         &                         (CABS(FRBPAR(JM,JS)), JM = 1, MD2FP1)
  END DO

  !
  !  Re and Im of Fourier components of En for m=-3,-2,-1
  !
  imin = -3
  imax = -1
  indexmin = imin - mfl + 1
  indexmax = imax - mfl + 1
  !
  do js = 1, npsi
    write(24, 9053) ccs(js), &
         &            (real(fren(jm,js)),  jm = indexmin, indexmax), &
         &            (aimag(fren(jm,js)), jm = indexmin, indexmax)
  end do

  RETURN
  !
  !------------------------------------------------------------------
  !L.             6. POYNTING FLUX
  !
600 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.             7. RE(ELECTRIC FIELD) AT VARIOUS PHI
  !
700 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.          8. LEFT POLARISATION
  !
800 CONTINUE
  RETURN
  !
  !----------------------------------------------------------------
  !L.            9. RE (E_N) AT VARIOUS PHI
  !
900 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.               10. RE (E_B) AT VARIOUS PHI
  !
1000 CONTINUE
  RETURN
  !
  !---------------------------------------------------------------------
  !L.         11. PARALLEL COMPONENT OF POYNTING
  !
1100 CONTINUE
  RETURN
  !
  !-------------------------------------------------------------------
  !L.          12. DISP.REL. RE(EPS) - (N/R)**2
  !
1200 CONTINUE
  RETURN
  !
  !--------------------------------------------------------------------
  !L.          13. IMAGINARY PART OF EPSILON
  !
1300 CONTINUE
  RETURN
  !
  !------------------------------------------------------------------
  !L.          14. OMEGA-OMEGACI
  !
1400 CONTINUE
  RETURN
  !
  !-------------------------------------------------------------------
1500 CONTINUE
  RETURN
  !
  !===================================================================
9053 FORMAT(1X,1P16E13.4)
  !===================================================================
END SUBROUTINE PLOT5
!CC*DECK P5C4S14
FUNCTION ACROSS (PX1,PX2,PF1,PF2)
  !        ---------------
  !
  !  5.4.14.  F(X) = 0 VIA LINEAR INTERPOLATION
  !---------------------------------------------------------------------
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PX1, PX2, PF1, PF2, ACROSS
  !
  RETURN
  !
  IF ((PF1.EQ.0._RKIND).AND.(PF2.EQ.0._RKIND)) THEN
    ACROSS = (PX1 + PX2) * 0.5_RKIND
    RETURN
  END IF
  ACROSS = (PX1*PF2 - PX2*PF1) / (PF2 - PF1)
  RETURN
END FUNCTION ACROSS
!CC*DECK P5C4S15
SUBROUTINE ONEFUN(K1,K2,PX,PY,KCHAR,PLEFT,KLEFT,PRIGHT,KRIGHT, &
     &                     PBOTTM,PTOP,KROWS,KPLOT,KLBAR)
  !        --------------------------------------------------------------
  !
  ! 3.5  FILLS ONE FUNCTION INTO THE PLOT FIELD
  !
  !     VERSION 2                JUNE 1984       KA        LAUSANNE
  !
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: K1, K2, KLEFT, KRIGHT, KROWS, ILEFT, IRIGHT, ISHIFT, INTVLS, J, &
       & IPOSX, IPACK, IPLACE, IPOSY, IYPRNT, IYEND, JBAR
  REAL(RKIND) ::  PX(K2),  PY(K2), PLEFT, PRIGHT, PBOTTM, PTOP, ZDELX, ZDELY
  LOGICAL     KLBAR
  CHARACTER &
       &   KPLOT*8,  IHOLL*8,  IPRES*1,  KCHAR*1
  DIMENSION ::  KPLOT(16,KROWS)
  !-----------------------------------------------------------------------
  !
  !L              1.        INITIAL OPERATIONS
  !
  !
  !     RANGE OF USED COLUMNS.
  !     PROVIDE PLACE FOR VALUES OUT OF RANGE
  ILEFT=KLEFT
  IRIGHT=KRIGHT
  IF(ILEFT.GT.1) GO TO 110
  ISHIFT=2-ILEFT
  ILEFT=2
  IRIGHT=IRIGHT+ISHIFT
110 CONTINUE
  IF(IRIGHT.GT.127) IRIGHT=127
  INTVLS=IRIGHT-ILEFT
  !     WIDTH OF PLOT-INTERVALS
  ZDELX=(PRIGHT-PLEFT)/REAL(INTVLS)
  ZDELY=(PTOP-PBOTTM)/REAL(KROWS-3)
  !
  !L              2.        LOOP OVER POINTS TO BE PLOTTED
  !
  DO J=K1,K2
    !
    !L                  2.1     HORIZONTAL POSITION
    !
    !     AS AN INTEGER NUMBER
    IPOSX=INT((PX(J)-PLEFT)/ZDELX+2.5_RKIND)
    IF(IPOSX.LT.ILEFT) IPOSX=ILEFT-1
    IF(IPOSX.GT.IRIGHT) IPOSX=IRIGHT+1
    !     AS A PACK NUMBER AND A PLACE WITHIN THAT PACK NUMBERED FROM RIGHT
    IPACK=(IPOSX-1)/8+1
    IPLACE=IPOSX-(IPACK-1)*8
    !
    !L                  2.2     VERTICAL POSITION
    !
    !     SEQUENTIALLY NUMBERED FROM BELOW
    IPOSY=INT((PY(J)-PBOTTM)/ZDELY+2.5_RKIND)
    IF(IPOSY.LT.2) IPOSY=1
    IF(IPOSY.GT.KROWS-1) IPOSY=KROWS
    !     AS A PRINT LINE NUMBERED FROM ABOVE
    IYPRNT=KROWS+1-IPOSY
    !
    !L                  2.3     CHANGE THE PLOT FIELD
    IYEND=IYPRNT
    IF(KLBAR) IYEND=KROWS-1
    DO JBAR=IYPRNT,IYEND
      !
      IHOLL=KPLOT(IPACK,JBAR)
      !
      IPRES=IHOLL(IPLACE:IPLACE)
      IF(ICHAR(IPRES).EQ.ICHAR(' ')) IPRES=KCHAR
      IHOLL(IPLACE:IPLACE)=IPRES
      KPLOT(IPACK,JBAR)=IHOLL
      !
    END DO
  END DO
  !
  RETURN
END SUBROUTINE ONEFUN
!CC*DECK P5C4S16
SUBROUTINE BLANK(KROWS,KPLOT)
  !        ---------------------------
  !
  ! 3.6  PUTS THE PLOT FIELD TO BLANK
  !
  !     VERSION 2                JUNE 1984       KA        LAUSANNE
  !
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KROWS, JJ, J
  CHARACTER :: KPLOT*8
  DIMENSION :: KPLOT(16,KROWS)
  !-----------------------------------------------------------------------
  DO JJ=1,KROWS
    DO J=1,16
      KPLOT(J,JJ)='        '
    END DO
  END DO
  !
  RETURN
END SUBROUTINE BLANK
!CC*DECK P5C4S17
SUBROUTINE FRAME(KLEFT,KRIGHT,KROWS,KPLOT)
  !        ------------------------------------------
  !
  ! 3.7  FILLS THE FRAME INTO THE PLOT FIELD
  !
  !     VERSION 2                JUNE 1984       KA        LAUSANNE
  !
  !-----------------------------------------------------------------------
  !
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KLEFT, KRIGHT, KROWS, ILEFT, IRIGHT, ISHIFT, JJ, I, J
  REAL(RKIND) :: ZBOX(2), ZFRAMX(20), ZFRAMY(20), ZLEFT, ZRIGHT, ZBOTTM, ZTOP, &
       & ZDELX, ZY, ZX
  CHARACTER :: KPLOT*8
  DIMENSION :: KPLOT(16,KROWS)
  !-----------------------------------------------------------------------
  !
  !L              1.        INITIAL OPERATIONS
  !
  !     RANGE OF USED COLUMNS
  !     PROVIDE PLACE FOR VALUES OUT OF RANGE
  ILEFT=KLEFT
  IRIGHT=KRIGHT
  IF(ILEFT.GT.1) GO TO 110
  ISHIFT=2-ILEFT
  ILEFT=2
  IRIGHT=IRIGHT+ISHIFT
110 CONTINUE
  IF(IRIGHT.GT.127) IRIGHT=127
  !     BOX BOUNDARIES
  ZLEFT=REAL(ILEFT)
  ZRIGHT=REAL(IRIGHT)
  ZBOTTM=2._RKIND
  ZTOP=REAL(KROWS-1)
  !     WIDTH OF PLOT INTERVALS
  ZDELX=1._RKIND
  !
  !L              2.        HORIZONTAL LINES OF BOX
  !
  ZBOX(1)=ZTOP
  ZBOX(2)=ZBOTTM
  DO JJ=1,2
    ZY=ZBOX(JJ)
    ZX=ZLEFT-ZDELX
    I=0
    DO J=ILEFT,IRIGHT
      I=I+1
      ZX=ZX+ZDELX
      ZFRAMX(I)=ZX
      ZFRAMY(I)=ZY
      IF ((I .LT. 20) .AND. (J .LT. IRIGHT)) then
        print *,'OS: next statement seems wrong so should stop to check'
!!$        stop 'OS check frame'
      END IF
      IF ((I .LT. 20) .AND. (J .LT. IRIGHT)) GO TO 210
      CALL ONEFUN(1,I,ZFRAMX,ZFRAMY,'-',ZLEFT,ILEFT,ZRIGHT,IRIGHT, &
           &                ZBOTTM,ZTOP,KROWS,KPLOT,.FALSE.)
      I=0
    END DO
210 CONTINUE
  END DO
  !
  !L              3.        VERTICAL LINES OF BOX
  !
  ZFRAMX(1)=ZLEFT
  ZFRAMY(1)=ZTOP
  ZFRAMX(2)=ZRIGHT
  ZFRAMY(2)=ZTOP
  CALL ONEFUN(1,2,ZFRAMX,ZFRAMY,'I',ZLEFT,ILEFT,ZRIGHT,IRIGHT, &
       &                ZBOTTM,ZTOP,KROWS,KPLOT,.TRUE.)
  !
  RETURN
END SUBROUTINE FRAME
!CC*DECK CUS1
SUBROUTINE MESAGE(KMESS)
  !        -----------------
  !
  ! U.1 PRINT CHARACTER MESSAGE ON OUTPUT CHANNEL
  !------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
  IMPLICIT NONE
  !
  CHARACTER*(*) KMESS
  !
  WRITE (NPRNT,9900) KMESS
  RETURN
9900 FORMAT(/,1X,A)
END SUBROUTINE MESAGE
!CC*DECK P5C5S01
SUBROUTINE IODSK5 (K)
  !        -----------------
  !
  !  5.5.1. HANDLES DISK FILES
  !--------------------------------------------------------------------
  USE GLOBALS
  use interpos_module
!!$  USE COMNUM ! INCLUDE 'COMNUM.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  INCLUDE 'NEWRUN.inc'
  !
  INTEGER :: K, ICOMP, JS, JC, J, I
  !
  !--------------------------------------------------------------------
  !
  GO TO (100,200,300,400,500,600,700,800) , K
  !
  !---------------------------------------------------------------------
  !L.             1. REWIND DISK FILES
  !
100 CONTINUE
  !lv         REWIND NSAVE
  !lv         REWIND MEQ
  REWIND NDS
  REWIND NDES
  RETURN
  !
  !---------------------------------------------------------------------
  !L.             2. READ SOLUTION VECTOR
  !
200 CONTINUE
  ICOMP = 2*(2*NCHI-2)*NPSI + 2*NCHI-2
  READ (NDS) (SOL(I),I=1,ICOMP)
  RETURN
  !
  !---------------------------------------------------------------------
  !L.             3. READ EQ QUANTITIES
  !
300 CONTINUE
  !lv         READ (MEQ) ((EQ(I,J),I=1,MDEQ),J=1,NPOL)
  RETURN
  !
  !--------------------------------------------------------------------
  !L.             4. READ (R,Z) COORDINATES OF CELLS AND SURFACE
  !
400 CONTINUE
  !         These are values on S(2:NPSI+1) surfaces, equivalent to CS(2:NPSI+1)
  DO JS=1,NPSI+1
    READ (NDES) (CCR2(JS,JC),JC=1,NPOL)
    READ (NDES) (CCZ2(JS,JC),JC=1,NPOL)
    READ (NDES) (CNR2(JS,JC),JC=1,NPOL)
    READ (NDES) (CNZ2(JS,JC),JC=1,NPOL)
  END DO
  DO JS=1,NPSI
    CCR(JS,1:NPOL) = 0.5_RKIND*(CCR2(JS,1:NPOL)+CCR2(JS+1,1:NPOL))
    CCZ(JS,1:NPOL) = 0.5_RKIND*(CCZ2(JS,1:NPOL)+CCZ2(JS+1,1:NPOL))
    CNR(JS,1:NPOL) = 0.5_RKIND*(CNR2(JS,1:NPOL)+CNR2(JS+1,1:NPOL))
    CNZ(JS,1:NPOL) = 0.5_RKIND*(CNZ2(JS,1:NPOL)+CNZ2(JS+1,1:NPOL))
  END DO
  !%OS         DO JS=1,NPSI
  !%OS           WRITE (22,*) (CCR(JS,JC),JC=1,NPOL)
  !%OS           WRITE (22,*) (CCZ(JS,JC),JC=1,NPOL)
  !%OS           WRITE (22,*) (CNR(JS,JC),JC=1,NPOL)
  !%OS           WRITE (22,*) (CNZ(JS,JC),JC=1,NPOL)
  !%OS         END DO
  !    interpolate back on mid-psi mesh
  do JC=1,NPOL
    call interpos(EQ(5,1,:),CCR(:,JC),NIN=NPSI,XOUT=EQ(5,1,:) &
         &       ,TENSION=-0.3_RKIND,nbc=(/2, 0/), ybc=(/0._RKIND, 0._RKIND/))
  end do
  !
  READ (NDES) NRZSUR
  READ (NDES) (XS(J),J=1,NRZSUR),(YS(J),J=1,NRZSUR)
  !%OS         WRITE (22,*) NRZSUR
  !%OS         WRITE (22,*) (XS(J),J=1,NRZSUR),(YS(J),J=1,NRZSUR)
  RETURN
  !
500 CONTINUE
  RETURN
600 CONTINUE
  RETURN
  !
  !---------------------------------------------------------------------
  !L.              7. READ NAMELIST
  !
700 CONTINUE
  !
  !lvC     OPEN NSAVE
  !lv         OPEN (UNIT=NSAVE,FILE='TAPE8',STATUS='OLD',
  !lv     +              FORM='FORMATTED')
  !lv         READ (NSAVE,NEWRUN)
  !lvcccc         READ(NREAD,NEWRUN)
  !
  !     OPEN DISK FILES
  !lv         OPEN (UNIT=MEQ,FILE='TAPE4',STATUS='OLD',
  !lv     +              FORM='UNFORMATTED')
  OPEN (UNIT=NDS,FILE='TAPE14',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  OPEN (UNIT=NDES,FILE='TAPE16',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  OPEN (UNIT=NSOURC,FILE='TAPE15',STATUS='OLD', &
       &              FORM='UNFORMATTED')
  IF(.NOT.NLOTP5(1)) GO TO 710
  WRITE (NPRNT,NEWRUN)
  WRITE (NPRNT,9010)
710 CONTINUE
  !
9010 FORMAT(////,' ABOVE NAMELIST IS OUTPUT 5.1',/,1X,28('-'),///)
  RETURN
  !
  !-----------------------------------------------------------------
  !L             8. CLOSE DISK FILES.
  !                 KEEP MEQ, NSAVE, NDES AND NSOURC FOR EVENTUAL
  !                 FURTHER USE WHEN DOING A FREQUENCY TRACE.
  !                 DELETE NDS (WHERE THE SOLUTION VECTOR IS).
  !
800 CONTINUE
  !lv         CLOSE (MEQ)
  !lv         CLOSE (NSAVE)
  CLOSE (NDS,STATUS='DELETE')
  CLOSE (NDES)
  CLOSE (NSOURC)
  RETURN
  !
END SUBROUTINE IODSK5
!CC*DECK U2
SUBROUTINE BLINES(K) 
  !        ---------- ------
  !
  ! U.2    INSERT K BLANK LINES ON OUTPUT CHANNEL
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  IMPLICIT NONE
  !
  INTEGER :: K, J
  !
  DO J=1,K
    WRITE (6,9900)
  END DO
  !
  RETURN
9900 FORMAT(' ')
END SUBROUTINE BLINES
!CC*DECK U31 
SUBROUTINE IARRAY(KNAME,KA,KDIM)
  !        ---------- ------
  !
  ! U.31   PRINT NAME AND VALUES OF INTEGER ARRAY
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  IMPLICIT NONE
  !
  INTEGER :: KA(KDIM), KDIM, J
  CHARACTER*(*) KNAME
  !
  CALL BLINES(1)
  WRITE (6,9900) KNAME 
  WRITE (6,9901) (KA(J),J=1,KDIM)
  !
  RETURN
9900 FORMAT(1X,A)
9901 FORMAT((1X,10(I13))) 
END SUBROUTINE IARRAY
!CC*DECK U30 
SUBROUTINE RARRAY(KNAME,PA,KDIM)
  !        ---------- ------
  !
  ! U.30   PRINT NAME AND VALUES OF REAL ARRAY
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KDIM, J
  REAL(RKIND) :: PA(KDIM)
  CHARACTER*(*) KNAME
  !
  CALL BLINES(1)
  WRITE (6,9900) KNAME 
  WRITE (6,9901) (PA(J),J=1,KDIM)
  !
  RETURN
9900 FORMAT(1X,A)
9901 FORMAT((1X,10(1PE13.4)))
END SUBROUTINE RARRAY
!CC*DECK U23 
SUBROUTINE IVAR(KNAME,KVALUE)
  !        ---------- ----
  !
  ! U.23   PRINT NAME AND VALUE OF INTEGER VARIABLE 
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  IMPLICIT NONE
  !
  INTEGER :: KVALUE
  CHARACTER*(*) KNAME
  !
  WRITE (6,9900) KNAME, KVALUE
  !
  RETURN
9900 FORMAT(/,1X,A,' =',I12)
END SUBROUTINE IVAR
!CC*DECK U20 
SUBROUTINE RVAR(KNAME,PVALUE)
  !        ---------- ----
  !
  ! U.20   PRINT NAME AND VALUE OF REAL VARIABLE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  REAL(RKIND) :: PVALUE
  CHARACTER*(*) KNAME
  !
  WRITE (6,9900) KNAME, PVALUE
  !
  RETURN
9900 FORMAT(/,1X,A,' =',1PE13.4)
END SUBROUTINE RVAR
!CC*DECK U41 
SUBROUTINE RESETI(KA,KDIM,KVALUE)
  !        ---------- ------
  !
  ! U.41   RESET INTEGER ARRAY TO SPECIFIED VALUE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  IMPLICIT NONE
  !
  INTEGER :: KDIM, KA(KDIM), KVALUE, J
  !
  DO J=1,KDIM
    KA(J) = KVALUE
  END DO
  !
  RETURN
END SUBROUTINE RESETI
!CC*DECK U40 
SUBROUTINE RESETR(PA,KDIM,PVALUE)
  !        ---------- ------
  !
  ! U.40   RESET REAL ARRAY TO SPECIFIED VALUE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: KDIM, J
  REAL(RKIND) :: PA(KDIM), PVALUE
  !
  DO J=1,KDIM
    PA(J) = PVALUE
  END DO
  !
  RETURN
END SUBROUTINE RESETR
!CC
SUBROUTINE RUNTIME (PTIME)
  !        ------------------
  !
  ! GETS RUN (CPU) TIME
  !-----------------------------------------------------------------------
  USE PREC_CONST
  IMPLICIT NONE
  !
  INTEGER :: IC, IR, IM
  REAL(RKIND) :: PTIME
  !
  CALL SYSTEM_CLOCK(COUNT=IC, COUNT_RATE=IR, COUNT_MAX=IM)
  PTIME = REAL(IC,RKIND)/REAL(IR,RKIND)
  RETURN
END SUBROUTINE RUNTIME


!lv         DIMENSION tarray(2)
!lv         REAL*4 tarray
!-----------------------------------------------------------------------
!lv         zetime = etime(tarray)
!lv         cptime = tarray(1)
!lv         write(6,9001) cptime
!lv 9001    format(' CPU time used so far = ', 1pe10.3,' [s]')
!lv: !!!!bugs the code, I do not know why... :        
!lv         call etime(tarray,zetime)
!lv         write(6,9002) tarray, zetime
!lv 9002    format(' User, system, run times = ', 1p3e10.3,' [s]')
!lv         return
!lv         end
