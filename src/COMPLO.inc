C ----------------------------------------------------------------------
CL                 C9.5      OUTPUT AND PLOT VARIABLES FOR LION5
C VERSION 16     LDV      MAY 1994          CRPP  LAUSANNE
       COMMON //
     C   BN,	   BB,	     BPAR,
     C   BNL,      BPARL,    BPL,      DVDC,     DXDC,     DXDS,
     C   EN,       ENL,      EP,       EPL,      FRBN,	   FRBN2,
     C	 FRBB,	   FRBB2,    FRBPAR,   FRBPA2,   FREN,     FREN2,
     C   FREP,     FREP2,    FRINT,    SOL,      VC,       XC,
     R   AIMEPS,   ABSPOW,   ABSPSP,   APHI,     CCHI,     CCR,
     R   CCS,      CCZ,      CDEVIA,   CELPOW,   CHIPOW,   CMEAN,
     R   CNR,      CNZ,      COST,     CPL,      CPLE,     CPLJ,
     R   DENPOW,   DEPOS,    DEPPSP,   DPL,      DPLE,     DPLJ,
     R   DSPREL,   ECHEL,    ELEPOW,   ENCHI0,   ENPHI0,   EPCHI0,
     R   EPPHI0,   EQRDEN,   ER,       EZ,       FLUPOW,   FLUPSP,       
     R   FROUT,    FRALF,    OMCLIN,   SDEVIA,   SFLUX,    SINT,    
     R   SMEAN,	   SN,       SNL,      SPAR,     SPARL,    SPERP,          
     R   SPL,      SURPSI,   VOUT1,    VOUT2,    XS,       YS,
     R   CCR2, CCZ2, CNR2, CNZ2
     I   MOUT,     MOUTF,    NRZSUR
       DIMENSION
     C   BN(0:MDPSI,MDPOL1),BB(0:MDPSI,MDPOL1),BPAR(0:MDPSI,MDPOL1),
     C   EN(0:MDPSI,MDPOL1), EP(0:MDPSI,MDPOL1), FRBN(MD2FP1,MDPSI),
     C	 FRBN2(MD2FP1,MDPSI), FRBB(MD2FP1,MDPSI),
     C	 FRBB2(MD2FP1,MDPSI), FRBPAR(MD2FP1,MDPSI),
     C	 FRBPA2(MD2FP1,MDPSI), FREN(MD2FP1,MDPSI),
     C   FREN2(MD2FP1,MDPSI),FREP(MD2FP1,MDPSI), FREP2(MD2FP1,MDPSI),
     C   FRINT(MDPOL,MD2FP1),SOL(MDCOMP),
     R   AIMEPS(MDPSI,MDPOL),ABSPOW(MDPSI),     ABSPSP(MDPSI,MDSPC2),
     R   CCHI(MDPOL),        CCR(0:MDPSI,MDPOL1),CCS(MDPSI),
     R   CCZ(0:MDPSI,MDPOL1),CELPOW(MDPSI,MDPOL),CHIPOW(MDPOL),
     R   CNR(MDPSI,MDPOL),   CNZ(MDPSI,MDPOL),   COST(MDPSI,MDPOL),
     R   CPLJ(MDSPC2),       DENPOW(0:MDPSI,MDPOL1),DEPOS(MDPSI),
     R   DEPPSP(MDPSI,MDSPC2),DPLJ(MDSPC2),
     R   DSPREL(MDPSI,MDPOL),ENCHI0(MDPSI),      ENPHI0(MDPSI),
     R   EPCHI0(MDPSI),      EPPHI0(MDPSI),      EQRDEN(MDDEN,MDPSI),
     R   ER(0:MDPSI,MDPOL1), EZ(0:MDPSI,MDPOL1), FLUPOW(MDPSI1),
     R   FLUPSP(MDPSI1,MDSPC2),                  FRALF(MD2FP1,MDPSI),
     R   FROUT(MD2FP1,MDPSI),
     R   OMCLIN(MDPSI,MDPOL),SFLUX(MDPSI),       SINT(MDPSI, MDPOL),
     R   SN(MDPSI,MDPOL),    SPAR(MDPSI,MDPOL),  SPERP(MDPSI,MDPOL),
     R   SURPSI(MDPSI),      VOUT1(MDPSI,MDPOL), VOUT2(MDPSI,MDPOL),
     R   XS( MDRZ),          YS( MDRZ),
     R   CCR2(0:MDPSI+1,MDPOL1), CCZ2(0:MDPSI+1,MDPOL1), 
     R   CNR2(MDPSI+1,MDPOL), CNZ2(MDPSI+1,MDPOL),
     I   MOUT(MDPSI,MDPOL),  MOUTF(MD2FP1,MDPSI)
       COMPLEX
     C   BN,	   BB,       BPAR,
     C   BNL,      BPARL,    BPL,      DVDC,     DXDC,     DXDS,
     C   EN,       ENL,      EP,       EPL,      FRBN,	   FRBN2,
     C   FRBB,     FRBB2,    FRBPAR,   FRBPA2,   FREN,     FREN2,
     C   FREP,     FREP2,    FRINT,    SOL,      VC,       XC
