program LION_for_kepler_ITM_prog

  use euitm_schemas
  use euitm_routines
  use deallocate_structures, only: deallocate_cpo
  use f90_file_reader, only: file2buffer

  implicit none

  INTERFACE
    SUBROUTINE LION(equil_in,coreprof_in,corefast_in,antennas_in,waves_in,waves_out,param_code,ierror,error_message)
       use euitm_schemas
       type(type_equilibrium), pointer, intent(in)  :: equil_in(:)
       type(type_coreprof),    pointer, intent(in)  :: coreprof_in(:)
       type(type_corefast),    pointer, intent(in)  :: corefast_in(:)
       type(type_antennas),    pointer, intent(in)  :: antennas_in(:)
       type(type_waves),       pointer, intent(in)  :: waves_in(:)
       type(type_waves),       pointer              :: waves_out(:)
       type(type_param),                intent(in)  :: param_code
       integer,                         intent(out) :: ierror
       character(len=:),       pointer, intent(out) :: error_message
     END SUBROUTINE LION
  END INTERFACE

  type(type_equilibrium), pointer :: equil_in(:)   => null()
  type(type_coreprof),    pointer :: coreprof_in(:) => null()
  type(type_corefast),    pointer :: corefast_in(:) => null()
  type(type_antennas),    pointer :: antennas_in(:) => null()
  type(type_waves),       pointer :: waves_in(:) => null()
  type(type_waves),       pointer :: waves_out(:) => null()
  type(type_param) :: param_code
  integer                    :: output_flag
  character(len=:), pointer  :: output_message
  integer :: iounit = 1

  ! For reading input IDSs
  integer :: shot_number, input_run, output_run,idx
  real(EUITM_R8) :: time = 0.0
  character(len=132) :: filename = 'input.LION_for_kepler_ITM_prog'

  namelist /UAL_REFERENCE/ &
       shot_number , &
       input_run   , &
       output_run  , &
       time

  write(*,*)'Read shot and run numbers from: ',trim(filename)
  open(1,file=filename)
  read(1,UAL_REFERENCE)
  close(1)
  write(*,*)'  shot       ='  , shot_number
  write(*,*)'  input_run  ='  , input_run
  write(*,*)'  output_run ='  , output_run
  write(*,*)'  time (s)   = ' , real(time,4)

  !==== XML input files =====
  write(*,*) 'Read input XML files'
  call file2buffer('LION_input.xml'  , 1 , param_code%parameters)
  call file2buffer('LION_input.xml'  , 1 , param_code%default_param)
  call file2buffer('LION_schema.xsd' , 1 , param_code%schema)


  !==== READ IDSs FROM THE UAL =====
  write(*,*) 'Read input CPOs'
  idx = -1
  call euitm_open('euitm',shot_number,input_run,idx)
  if (idx < 0) then
     write(*,*)'=== ERROR recieved from euitm_open, idx=',idx,' ==='
     stop '-> ABORT, failed to read from UAL database'
  end if
  write(*,*) '=> Read equilibrium'
  allocate(equil_in(1), coreprof_in(1), corefast_in(1), antennas_in(1))
  call euitm_get_slice(idx,'equilibrium',equil_in(1),time,1)
  write(*,*) '=> Read coreprof'
  call euitm_get_slice(idx,'coreprof',coreprof_in(1),time,1)
  write(*,*) '=> Read corefast'
  call euitm_get_slice(idx,'corefast',corefast_in(1),time,1)
  write(*,*) '=> Read antennas'
  call euitm_get_slice(idx,'antennas',antennas_in(1),time,1)
  if (.false.) then
     write(*,*) '=> Read waves'
     allocate(waves_in(1))
     call euitm_get_slice(idx,'waves',waves_in(1),time,1)
  else
     call get_simple_waves(waves_in)
  end if
  call euitm_close(idx)


  !==== RUN LION_IMAS =====
  write(*,*) 'call LION_IMAS'
  call LION(equil_in,coreprof_in,corefast_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
  if (output_flag/=0) then
     write(*,*)'ERROR recieved from LION, output_flag=',output_flag
     if (associated(output_message)) then
        write(*,*)'Output message:'
        write(*,*)'"',output_message,'"'
        deallocate(output_message)
     end if
     stop 'Abort in LION_for_kepler_ITM_prog (error in LION)'
  end if

  !==== WRITE OUTPUT IDSs TO THE UAL =====
  write(*,*) 'Write output IDSs'
  write(*,*)'  shot       ='  , shot_number
  write(*,*)'  output_run ='  , output_run
  write(*,*)'  size(waves_out(1)%coherentwave(1)%compositions%ions)=',size(waves_out(1)%coherentwave(1)%compositions%ions)


  call euitm_create('euitm',shot_number,output_run,0,0,idx)
  !write(*,*) '=> Write equilibrium'
  !call euitm_put(idx,'equilibrium',equil_in)
  !write(*,*) '=> Write core_profiles'
  !call euitm_put(idx,'core_profiles',coreprof_in)
  !write(*,*) '=> Write ic_antennas'
  !call euitm_put(idx,'ic_antennas',antennas_in)
  write(*,*) '=> Write waves-output'
  call euitm_put(idx,'waves',waves_out)
  call euitm_close(idx)

  call deallocate_cpo(equil_in)
  call deallocate_cpo(coreprof_in)
  call deallocate_cpo(corefast_in)
  call deallocate_cpo(antennas_in)
  call deallocate_cpo(waves_in)
  call deallocate_cpo(waves_out)

contains

  subroutine get_simple_waves(W)
    type(type_waves), pointer, intent(out) :: W(:)
    allocate(W(1))
    allocate(W(1)%coherentwave(1))
    allocate(W(1)%coherentwave(1)%global_param%ntor(1))
    W(1)%coherentwave(1)%global_param%ntor = 27
    W(1)%coherentwave(1)%global_param%frequency = 50e6
  end subroutine get_simple_waves

end program LION_for_kepler_ITM_prog
