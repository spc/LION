#!/usr/bin/perl
#
#   globals.pl file [file,...] takes as inputs the "include"
#   files and creates the files globals.f90 and globals_inits.f90
#
#   In the GLOBALS module are included:
#	- All the PARAMETER constants.
#	- The common and namelist variables transformed to
#	  STATIC initialized variables with the SAVE attributes.
#	- The common and namelist arrays transformed to
#	  ALLOCATABLE, SAVE initialized arrays.
#
#   All the  ALLOCATE statements are contained in the
#    globals_init subroutine
#
#   Notes: All the inputted files which do not contain COMMON blocks are
#          cre created in the asked directory.
#
die "Usage: $0 file [file,...] \n" if ($#ARGV < 0);
$ENV{"PATH"} = "../.:$ENV{'HOME'}/bin/f2f90:".$ENV{"PATH"};
#print '$ENV{"PATH"};
print "To which dir you want to save the created files? ";
$toDir = <STDIN>;
chomp($toDir);
unless (-d $toDir) {
    mkdir($toDir,0755) || die "Cannot create $toDir\n";
}
foreach $file (@ARGV) {
    open(IN, "< $file") || die "Cannot open $file\n";
    $iproc{$file} = 0;   # Mark processed files
    print STDERR "Parsing file $file...\n";
    undef $lookahead;
    while ($_ = &get_line()) {
#	print "$_\n";
######### COMMON statement
        if (/^\s*common\s+\/\s*(\w+)\/\s*/i) {
	    $iproc{$file} = 1;
            $c = &toLower($1);
	    print "  Common: $c\n";
	    $argVar = &toLower($');
	    $argVar =~ s/\s*//g;
	    @argVar = sort(split(/,/,$argVar));
	    foreach $v (@argVar) {
		unless ($type{$v}) {
		    if ($v =~ /^[a-ho-z]/) {
			$type{$v} = "REAL(RKIND)";
			$val{$v} = "0._RKIND";
		    } else {
			$type{$v} = "INTEGER";
			$val{$v} = "0";
		    }
		}
		$loc{$v} = "\/$c\/";
		push(@com_var,$v);
	    }
######### Namelist declaration statement
        } elsif (/^\s*namelist\s*\/\s*(.+)\s*\//i) {
	    $n = &toUpper($1);
	    push(@namelist,$n);
	    print "  NAMELIST: $n\n";
	    $argVar = &toLower($');
	    $argVar =~ s/\s*//g;
	    $namelist_var{$n} = &pretty(',',$argVar);
######### PARAMETER declaration statement
        } elsif (/^\s*parameter\s*\(\s*(.+)\)/i) {
	    $iproc{$file} = 1;
	    print "  PARAMETER\n";
	    $argVar = &toLower($1);
	    $argVar =~ s/\s*//g;
	    @argVar = split(/,/,$argVar);
	    foreach $vv (@argVar) {
		$i = index($vv,"=");
		next if ($i < 0);
		$v = substr($vv, 0, $i);
		unless ($type{$v}) {
		    if ($v =~ /^[a-ho-z]/) {
			$type{$v} = "REAL(RKIND)";
		    } else {
			$type{$v} = "INTEGER";
		    }
		}
		push(@param_var,$v);
		$val{$v} = substr($vv, $i+1);
	    }
######### Character type declaration statement
        } elsif (/^\s*character\s*\*([\d]+)\s+/i) {
#	    $iproc{$file} = 1;
	    $len = $1;
	    $argVar = &toLower($');
	    $argVar =~ s/\s*//g;
	    unless ($argVar =~ s/\)\s*,\s*/)S/g) {$argVar =~ s/\s*,\s*/S/g};
	    @argVar = sort(split(/S/,$argVar));
	    foreach $vv (@argVar) {
		$i = index($vv,"(");
		if ( $i > 1 ) {
		    $v = substr($vv, 0, $i);
		    $att{$v} = ", DIMENSION".substr($vv, $i);
		} else {
		    $v = $vv;
		}
		$type{$v} = "CHARACTER(len=$len)";
		$val{$v} = "' '";
#		print "$type{$v}$att{$v} :: $v = $val{$v}\n";
	    }
######### Type and dimension declaration statement
        } elsif (/^\s*(complex|real|integer|logical|dimension)[\s\*1-9]+\s*/i) {
#	    $iproc{$file} = 1;
	    $t = &toLower($1);
	    $argVar = &toLower($');
	    $argVar =~ s/\s*//g;
#	    print "$argVar\n";
	    unless ($argVar =~ s/\)\s*,\s*/)S/g) {$argVar =~ s/\s*,\s*/S/g};
#	    print "$argVar\n";
	    @argVar = sort(split(/S/,$argVar));
	    foreach $vv (@argVar) {
		$i = index($vv,"(");
		if ( $i > 0 ) {
		    $v = substr($vv, 0, $i);
		    $att{$v} = ", DIMENSION".substr($vv, $i);
		} else {
		    $v = $vv;
		}
#		print "$t $v $att{$v}\n";
		if ($t =~ "complex") {
		    $type{$v} = "COMPLEX(CKIND)";
		    $val{$v} = "0._RKIND";
		} elsif ($t =~ "real") {
		    $type{$v} = "REAL(RKIND)";
		    $val{$v} = "0._RKIND";
		} elsif ($t =~ "integer") {
		    $type{$v} = "INTEGER";
		    $val{$v} = "0";
		} elsif ($t =~ "logical") {
		    $type{$v} = "LOGICAL";
		    $val{$v} = ".FALSE.";
		} elsif ($t =~ "dimension") {
		} else {
		    print STDERR "Warning: Undefined typed for $v\n";
		    $type{$v} = "UNDEFINED";
		}
	    }
	}
    }
}
open(INC, ">.incfiles") || die "Cannot create .incfiles\n";
print INC "$toDir\n";
foreach  $f (sort keys (%iproc) ) {
    unless ($iproc{$f}) {
       ($ff = $f) =~ s/^.*\///;
       print INC "$ff\n";
       system("cp -p $f $toDir/$ff; cd $toDir; fix2free.pl $ff") ;
    }
}
close(INC);
######### Assignment statements in preset.f
#$file = "preset.f";
#open(IN, "< $file") || die "Cannot open $file\n";
#print STDERR "Parsing file $file...\n";
#undef $lookahead;
#while ($_ = &get_line()) {
#    if (/^\s*([a-zA-Z]\w*)\s*=\s*([\w\*\/\+\-\.]+)/) {
#	$lhs = &toLower($1);
#	($rhs = &toLower($2)) =~ s/\s+//g;
#	$val{$lhs} = $rhs;
#    }
#}
open(GLOBALS,">$toDir/globals.f90") || die "can not create globals.f90\n";
print GLOBALS <<EOD;
MODULE globals
!
!   Global vars and arrays
!
  USE prec_const
  IMPLICIT NONE
!
!!! Dimension constants
!
EOD
####
foreach $v (@param_var) {
    print GLOBALS "  $type{$v}$att{$v}, PARAMETER :: $v = $val{$v}\n";
}
####
foreach $n (sort @namelist) {
    ($argVar = $namelist_var{$n}) =~ s/[\s&]+//g;
    @argVar = split(/,/,$argVar);
    foreach $v (@argVar) {
	$loc{$v} .= " /$n/";
    }
}
####
print GLOBALS <<EOD;
!
!!! Static vars and arrays
!
EOD
####
#
undef @dynall;              #  Dynamic allocatable arrays     
foreach $v (sort @com_var) {
    $t = $att{$v};
    $val = " = $val{$v}";
    if ( $t =~ /DIMENSION/ ) {
      unless ($t =~ /\([\d,]+\)/) {
	$t =~ s/, DIMENSION\((.*)\)$/ $1/;

        $t =~ s/([, ])[\w\*\+\-\/\(\)]+/$1:/g;
        $t = ", DIMENSION($t), ALLOCATABLE";
	$val = "";
        push(@dynall, $v);
      }
    }
    $dcl = "$type{$v}$t, SAVE :: ";
    $loc = $loc{$v};
    write (GLOBALS);
}
####
####
#print GLOBALS <<EOD;
#!
#!!! Namelist definition
#EOD
####
#foreach $n (sort @namelist) {
#    print GLOBALS "!\n  NAMELIST /$n/ &\n & ";
#    print GLOBALS "$namelist_var{$n}\n";
#    ($argVar = $namelist_var{$n}) =~ s/[\s&]+//g;
#    @argVar = split(/,/,$argVar);
#    foreach $v (@argVar) {
#	print STDERR "|$v| in Namelist $n not typed\n" unless (defined $type{$v});
#	print STDERR "|$v| in Namelist $n not defined\n" unless (defined $val{$v});
#    }
#}
####
print GLOBALS <<EOD;
!
END MODULE globals
EOD
close(GLOBALS);
#
#   Create globals_init.f90
#
open(GLOBALS_INIT,">$toDir/globals_init.f90") || die "can not create globals_init.f90\n";
print GLOBALS_INIT <<EOD;
SUBROUTINE globals_init
!
!   Allocate dynamic arrays in globals
!
  USE globals
  IMPLICIT NONE
!---------------------------------------------------------------
!
!   Deallocate if required
!
EOD
foreach $v (@dynall) {
    print GLOBALS_INIT "  IF( ALLOCATED($v) ) DEALLOCATE($v)\n";
}
print GLOBALS_INIT "!\n!   Allocate and initialize\n!\n";
foreach $v (@dynall) {
    ($shape = $att{$v}) =~ s/, DIMENSION\(/\(/;
    print GLOBALS_INIT "  ALLOCATE($v$shape); $v = $val{$v}\n";
}
print GLOBALS_INIT "!\nEND SUBROUTINE globals_init\n";

#
#   Get a full fortran statement line
#
sub get_line {
    $lookahead = &getl() unless (defined $lookahead);
    $thisline = $lookahead;
  LINE1: while ($lookahead = &getl()) {
      if ($lookahead =~ /(^     \S)/) {
	  $lookahead =~ s/^     \S//;
	  $thisline .= $lookahead;
      } else {
	  last LINE1;
      }
    }
    $thisline;
}
#
#   Get a non-comment fortran line
#
sub getl {
  LINE: while (<IN>) {
      last LINE if /^[ \t]+/;
  }
    $line = $_;
    $i = index($line,"!");
    $line = substr($line,0,$i);
    $line;
}
#
# &toLower(string); --- convert string into lower case
#
sub toLower {
   local($string) = @_[0];
   $string =~ tr/A-Z/a-z/;
   $string;
   }
#
# &toUpper(string); --- convert string into upper case
#
sub toUpper {
   local($string) = @_[0];
   $string =~ tr/a-z/A-Z/;
   $string;
   }
#
# &pretty(sep,string); --- split string into shorter line at sep
#
sub pretty {
    local($sep,$str) = @_;
    local(@line) = split(/$sep/,$str);
    local($newline) = shift(@line);
    local($width) = 80;
    foreach $word (@line) {
	$l = length($word)+2;
	$newline = $newline.$sep." ";
	if ( $l + 1 > $width ) {
	    $newline = $newline." &\n & ";
	    $width = 80 - $l;
	} else {
	    $width -= $l + 1;
	}
	$newline = $newline.$word;
    }
    $newline;
}

############################################################################
format GLOBALS =
  @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<< @<<<<<<<<<<<<< !@<<<<<<<<<<<<<<<<
  $dcl,                                                        $v          $val           $loc
.
