subroutine write_itm(equilibrium_out,waves_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals ! contains ids_schemas
  use ids_routines
  !
  IMPLICIT NONE
  type(ids_equilibrium) :: equilibrium_out
  type(ids_waves)       :: waves_out
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, irefrun, i
  !
  character(len=5)  :: citmtree2
  !
  if (kitmshot .eq. 22) then
    print *,' Do not write since shot number to write: ',kitmshot
    return
  end if
  !
  citmtree2 = trim(citmtree)
  ! At this stage, seems to need to do create_pulse in any case, so changed else part, but may be different in future
  if (kitmshot .lt. 0) then
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    kitmshot = abs(kitmshot)
    call imas_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  else
    !     call euitm_open(citmtree2,kitmshot,kitmrun,idx)
    !     print *,'salut32, idx from open= ',idx
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    call imas_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  end if
  ! print *,' waves_out(1)%...= ',waves_out(1)%...
  
  call ids_put(idx,"equilibrium",equilibrium_out)
  call ids_put(idx,"waves",waves_out)
  
  call imas_close(idx,"ids",kitmshot,kitmrun)
  
  return
end subroutine write_itm
