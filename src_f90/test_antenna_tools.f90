program test_antenna_tools

  use  antenna_tools
  use prec_const, only: rkind, pi
  use euitm_schemas, only: type_equilibrium, type_antennas, type_antenna_ic

  implicit none

  integer :: nant      = 1
  integer :: nstrap    = 2
  integer :: ncoord    = 5

  real(8) :: rmin      = 3.99
  real(8) :: rmax      = 4.0
  real(8) :: zmin      =-0.02
  real(8) :: zmax      = 0.02

  real(8) :: freq      = 53d6
  real(8) :: power     = 5d6
  real(8) :: dist2wall = 0.05
  real(8) :: r_centre  = 3.0
  real(8) :: z_centre  = 0.0
  real(8) :: a_minor   = 0.8

  real(rkind) :: WNTOR       = 23   !< Toroidal mode number
  real(rkind) :: FEEDER_TILT = 15   !< The tilt of the feeder given as the difference
                                    !< in poloidal (in degrees) where the feeder join 
                                    !< the strap and where it joins the wall.

  write(*,*)'START: test_antenna_tools'
  call test_set_global_antenna_param
  write(*,*)'END: test_antenna_tools'

contains

  subroutine test_set_global_antenna_param
    use globals, only: THANT

    type(type_antenna_ic) :: antenna_ic
    type(type_equilibrium) :: equilibrium

    ! Antenna

    ! From antenna_position
    real(rkind) :: distance_antenna_axis !< Minimum distance between the magnetic axis and the antenna
    real(rkind) :: distance_wall_axis    !< Sum of \c distance_antenna_axis and antenna-wall distance
    real(rkind) :: theta1  !< Geometrical poloidal angle at the bottom corner of the antenna
    real(rkind) :: theta2  !< Geometrical poloidal angle at the top corner of the antenna

    ! From set_global_antenna_param
    integer :: ierror                 !< Error flag. For no error: ierror==0

    ! Counters
    integer :: jant, jstrap, jcoord

    ! Temp
    real(rkind) :: angle_alt(4)

    ! Test 1: empty antenna_ic
    write(*,*)
    write(*,*)'Doing Test 1...'
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror > -1) stop 'Unexpected errorflag from "Test 1: empty antenna_ic"'
    write(*,*)'SUCESSFUL! "Test 1: empty antenna_ic"',ierror
    write(*,*)

    ! Test 2: incomplete antenna_ic
    write(*,*)'Doing Test 2...'
    antenna_ic%power%value=power
    antenna_ic%frequency%value=freq
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror > -1) stop 'Unexpected errorflag from "Test 2: incomplete antenna_ic"'
    write(*,*)'SUCESSFUL! "Test 2: incomplete antenna_ic"'
    write(*,*)

    allocate(antenna_ic%setup%straps(nstrap))
    do jstrap=1,nstrap

       allocate(antenna_ic%setup%straps(jstrap)%coord_strap%r(ncoord))
       allocate(antenna_ic%setup%straps(jstrap)%coord_strap%z(ncoord))

       antenna_ic%setup%straps(jstrap)%dist2wall = dist2wall

       do jcoord=1,ncoord
          antenna_ic%setup%straps(jstrap)%coord_strap%r(jcoord) = &
               rmax+(rmin-rmax) * ( 2d0 * (jcoord-1)/(ncoord-1)  - 1d0 )**2
          antenna_ic%setup%straps(jstrap)%coord_strap%z(jcoord) = &
               zmin+(zmax-zmin)*(jcoord-1)/(ncoord-1)
       enddo
    enddo

    ! Test 3: complete antenna_ic and empty equilibrium
    write(*,*)'Doing Test 3...'
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror > -1) stop 'Unexpected errorflag from "Test 3: complete antenna_ic and empty equilibrium"'
    write(*,*)'SUCESSFUL! "Test 3: complete antenna_ic and empty equilibrium"'
    write(*,*)

    allocate(equilibrium%profiles_1d%r_outboard(2))
    equilibrium%profiles_1d%r_outboard(:) = r_centre + a_minor
    equilibrium%global_param%mag_axis%position%r = r_centre
    equilibrium%global_param%mag_axis%position%z = z_centre

    ! Test 4: complete input, LFS antenna
    write(*,*)'Doing Test 4...'
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror < 0) stop 'Unexpected errorflag from "Test 4: complete input, LFS antenna"'

    ! Check the antenna poloidal range
    angle_alt(:) = canonicalangle_vec( THANT + 180.0_RKIND , 360.0_RKIND ) - 180_RKIND
    if (minval(angle_alt(2:4)-angle_alt(1:3)) < 0) then
       write(*,*)'Inproper poloidal angles THANT=',THANT
       write(*,*)' THANT has to ordered anti-clockwise.'
       stop ' ABORT (improper THANT)'
    endif
    write(*,*)'SUCESSFUL! "Test 4: complete input, LFS antenna"'
    write(*,*)
    write(*,*)'---Print commonblock data:'
    call print_commonblock_antenna(0)
    write(*,*)

    ! Test 5: complete input, HFS antenna
    write(*,*)'Doing Test 5...'
    do jstrap=1,nstrap
       antenna_ic%setup%straps(jstrap)%coord_strap%r(:) = &
             2.0_RKIND * r_centre -  antenna_ic%setup%straps(jstrap)%coord_strap%r(:)
    enddo
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror < 0) stop 'Unexpected errorflag from "Test 5: complete input, HFS antenna"'

    ! Check the antenna poloidal range
    angle_alt(:) = canonicalangle_vec( THANT + 0.0_RKIND , 360.0_RKIND ) - 0.0_RKIND
    if (minval(angle_alt(2:4)-angle_alt(1:3)) < 0) then
       write(*,*)'Inproper poloidal angles THANT=',THANT
       write(*,*)' THANT has to ordered anti-clockwise.'
       stop ' ABORT (improper THANT)'
    endif
    write(*,*)'SUCESSFUL! "Test 5: complete input, HFS antenna"'
    write(*,*)
    write(*,*)'---Print commonblock data:'
    call print_commonblock_antenna(0)
    write(*,*)

    ! Test 6: complete input, Top-positioned antenna
    write(*,*)'Doing Test 6...'
    do jstrap=1,nstrap
       antenna_ic%setup%straps(jstrap)%coord_strap%z(:) = a_minor
       do jcoord=1,ncoord
          antenna_ic%setup%straps(jstrap)%coord_strap%r(:) = zmax * (dble(jcoord-1)/dble(ncoord-1) * 2d0 - 1d0)
       enddo
    enddo
    call set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)
    if (ierror < 0) stop 'Unexpected errorflag from "Test 6: complete input, Top-positioned antenna"'

    ! Check the antenna poloidal range
    angle_alt(:) = canonicalangle_vec( THANT + 0.0_RKIND , 360.0_RKIND ) - 0.0_RKIND
    if (minval(angle_alt(2:4)-angle_alt(1:3)) < 0) then
       write(*,*)'Inproper poloidal angles THANT=',THANT
       write(*,*)' THANT has to ordered anti-clockwise.'
       stop ' ABORT (improper THANT)'
    endif
    write(*,*)'SUCESSFUL! "Test 6: complete input, Top-positioned antenna"'
    write(*,*)
    write(*,*)'---Print commonblock data:'
    call print_commonblock_antenna(0)
    write(*,*)
    open(8576,file='out.test_set_global_antenna_param')
    call print_commonblock_antenna(8576)
    close(8576)
  end subroutine test_set_global_antenna_param


  real(rkind) function mean(vec)
    real(rkind) :: vec(:)
    mean=sum(vec)/dble(size(vec))
  end function mean

end program test_antenna_tools
