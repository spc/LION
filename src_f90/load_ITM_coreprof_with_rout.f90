subroutine load_itm_coreprof(coreprof_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the coreprof type definitions
  use euITM_routines
  IMPLICIT NONE
  type(type_coreprof),pointer      :: coreprof_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='coreprof'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call euitm_open(citmtree,kitmshot,kitmrun,idx)
  call euitm_get(idx,signal_name,coreprof_out)

  if (size(coreprof_out) .ge. 1) then
    if (associated(coreprof_out(1)%datainfo%comment)) then
      do i=1,size(coreprof_out(1)%datainfo%comment)
        write(6,'(A)') trim(coreprof_out(1)%datainfo%comment(i))
      end do
    end if

    print *,' coreprof_out(1)%time= ',coreprof_out(1)%time
  end if

  return
end subroutine load_itm_coreprof
