#!/usr/bin/perl
#
#   fix2free file [file,...] converts the comment and continuation
#   lines in f90 freeformat style. After the conversion, the resulting are:
#
#            original file |    f77 file      f90 file
#            --------------|--------------------------
#               *.f        |     *.f~          *.f90
#	        *.xxx      |     *.xxx~        *.xxx
#
die "Usage: $0 file [file,...] \n" if ($#ARGV < 0);
foreach $file (@ARGV) {
    print STDERR "Parsing file $file...\n";
    $fold = $file."~";
    $fnew = $file;
    rename($file,$fold) || die "Cannot rename $file to $fold\n";
    open(IN, "< $fold") || die "Cannot open $fold\n";
    open(OUT, "> $fnew") || die "Cannot create $fnew\n";
    while ($l = &cccl()) {
	print OUT $l;
    }
    close(OUT);
    close(IN);
    if ($fnew =~ /\.f/) {
	rename($fnew,"${fnew}90") || die "Cannot rename $fnew to ${fnew}90\n";;
    }
}
#
#   Convert continuation and comment lines
#
sub cccl {
    $lookahead = <IN> unless (defined $lookahead);
    $thisline = $lookahead;
  LINE1: while ($lookahead = <IN>) {
      if ($lookahead =~ /(^     \S)/) {
	  $lookahead =~ s/^     \S/     &/;
	  $thisline =~ s/\s+$/ &\n/;
	  $thisline .= $lookahead;
      } else {
	  last LINE1;
      }
  }
    $thisline =~ s/^\d/ $&/i;
    $thisline =~ s/^\S/!/;
    $thisline;
}
