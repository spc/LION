#!/bin/tcsh -ef
#
# Build libraries needed for generating Kepler actors in CPO based version of LION
#
make -C src_f90/ clean libraries IMAS_ENVIRONMENT_LOADED=yes

echo "To generated the LION actor: fc2k -kepler -docfile doc/LIONslice.txt fc2k/LIONslice.xml"
