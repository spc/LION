!
!*DECK U33
SUBROUTINE SAXPY(N,A,X,NX,Y,NY)
  !C#####################################################################
  !
  ! U.33 COMPUTES  Y = Y + A*X (REAL)
  !
  !#####################################################################
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX, NY
  REAL(RKIND) ::   A,      X((N-1)*NX+1),       Y((N-1)*NY+1)
  !---------------------------------------------------------------------
  IF(ABS(A).EQ. 0.0_RKIND   .OR.   N.LE.0) RETURN
  Y(1)=Y(1)+A*X(1)
  IF(N.EQ.1) RETURN
  NM1=N-1
  DO I=1,NM1
    Y(I*NY+1)=Y(I*NY+1)+A*X(I*NX+1)
  END DO
  !
  !        RETURN
END SUBROUTINE SAXPY
!
!*DECK U34
SUBROUTINE CAXPY(N,A,X,NX,Y,NY)
  !#####################################################################
  !
  ! U.33 COMPUTES  Y = Y + A*X (COMPLEX)
  !
  !#####################################################################
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX, NY
  COMPLEX ::   A,      X((N-1)*NX+1),       Y((N-1)*NY+1)
  !---------------------------------------------------------------------
  IF(CABS(A).EQ. 0.0_RKIND   .OR.   N.LE.0) RETURN
  Y(1)=Y(1)+A*X(1)
  IF(N.EQ.1) RETURN
  NM1=N-1
  DO I=1,NM1
    Y(I*NY+1)=Y(I*NY+1)+A*X(I*NX+1)
  END DO
  !
  !        RETURN
END SUBROUTINE CAXPY
!
COMPLEX FUNCTION CDOTU (N, X, NX, Y, NY)
  !	**********************
  !
  ! SCALAR PRODUCT X()*Y()
  !***********************************************
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX, NY
  COMPLEX ::  X((N-1)*NX+1),    Y((N-1)*NY+1),    Z
  !-----------------------------------------------
  IF (N.LE.0) THEN
    CDOTU = CMPLX(0._RKIND,0.)
    RETURN
  END IF
  Z = CMPLX(0._RKIND,0.)
  Z = Z + X(1) * Y(1)
  IF(N.EQ.1) THEN
    CDOTU = Z
    RETURN
  END IF
  NM1 = N - 1
  DO I=1,NM1
    Z = Z + X(I*NX+1) * Y(I*NY+1)
  END DO
  CDOTU = Z
  RETURN
END FUNCTION CDOTU
!
COMPLEX FUNCTION CDOTC (N, X, NX, Y, NY)
  !	**********************
  !
  ! SCALAR PRODUCT CONJG(X()) * Y()
  !***********************************************
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX, NY
  COMPLEX ::  X((N-1)*NX+1),    Y((N-1)*NY+1),    Z
  !-----------------------------------------------
  IF (N.LE.0) THEN
    CDOTC = CMPLX(0._RKIND,0.)
    RETURN
  END IF
  Z = CMPLX(0._RKIND,0.)
  Z = Z + CONJG(X(1)) * Y(1)
  IF(N.EQ.1) THEN
    CDOTC = Z
    RETURN
  END IF
  NM1 = N - 1
  DO I=1,NM1
    Z = Z + CONJG(X(I*NX+1)) * Y(I*NY+1)
  END DO
  CDOTC = Z
  RETURN
END FUNCTION CDOTC
!
SUBROUTINE CCOPY (N, X, NX, Y, NY)
  !	****************
  !
  ! COPIES X ONTO Y
  !********************************************
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX, NY
  COMPLEX ::  X((N-1)*NX+1),    Y((N-1)*NY+1)
  !--------------------------------------------
  IF (N.LE.0) RETURN
  Y(1) = X(1)
  IF(N.EQ.1) RETURN
  NM1 = N - 1
  DO I=1,NM1
    Y(I*NY+1) = X(I*NX+1)
  END DO
  RETURN
END SUBROUTINE CCOPY
!
SUBROUTINE CSCAL (N, A, X, NX)
  !	****************
  !
  ! SCALES X() BY FACTOR A
  !********************************************
  IMPLICIT NONE
  INTEGER :: N, NM1, I, NX
  COMPLEX :: X((N-1)*NX+1),    A
  !--------------------------------------------
  IF (N.LE.0) RETURN
  X(1) = A * X(1)
  IF(N.EQ.1) RETURN
  NM1 = N - 1
  DO I=1,NM1
    X(I*NX+1) = A * X(I*NX+1)
  END DO
  RETURN
END SUBROUTINE CSCAL
!*DECK CRAY05
!*CALL PROCESS
INTEGER FUNCTION ISRCHFGE(N,PV,NX,TARGET)
  !        ---------------------------------
  !
  !  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER OR EQUAL
  !  THAN TARGET. PV IS INCREMENTED BY NX
  !
  !LV         INCLUDE 'DECLAR.inc'
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, I, J, NX
  REAL(RKIND) ::  PV(N*NX+1), TARGET
  !
  I = 1
  IF (NX .LT. 0) STOP
  !
  DO J=1,N
    IF (PV(I) .GE. TARGET) GOTO 2
    I = I + NX
  END DO
2 CONTINUE
  ISRCHFGE = I
  !
  RETURN
END FUNCTION ISRCHFGE
!*DECK CRAY04
!*CALL PROCESS
INTEGER FUNCTION ISRCHFGT(N,PV,NX,TARGET)
  !        ---------------------------------
  !
  !  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER THAN TARGET
  !  PV IS INCREMENTED BY NX
  !
  !LV         INCLUDE 'DECLAR.inc'
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, I, J, NX
  REAL(RKIND) :: PV(N*NX+1), TARGET
  !
  I = 1
  IF (NX .LT. 0) STOP
  !
  DO J=1,N
    IF (PV(I) .GT. TARGET) GOTO 2
    I = I + NX
  END DO
2 CONTINUE
  ISRCHFGT = I
  !
  RETURN
END FUNCTION ISRCHFGT
!*DECK CRAY10
!*CALL PROCESS
FUNCTION SSUM(N,PV,NX)
  !        ----------------------
  !
  !  SUMS ALL ELEMENTS OF REAL ARRAY PV
  !
  !LV         INCLUDE 'DECLAR.inc'
  USE prec_const
  IMPLICIT NONE
  INTEGER :: N, I, J, NX, NM1
  REAL(RKIND) :: PV(N*NX+1), SSUM, SS
  !
  SS = 0._RKIND
  !
  IF (N .LE. 0) GOTO 2
  !
  I = 1
  IF (NX .LT. 0) STOP
  SS = PV(I)
  !
  IF (N .EQ. 1) GOTO 2
  !
  NM1 = N - 1
  !
  DO J=1,NM1
    I = I + NX
    SS = SS + PV(I)
  END DO
2 CONTINUE
  !   
  SSUM = SS
  !
  RETURN
END FUNCTION SSUM
