C---------------------------------------------------------------------
CL                  C0.1     ARRAY DIMENSIONS AND CONSTANTS
C VERSION 15    LDV       SEP 1993           CRPP LAUSANNE
C---------------------------------------------------------------------
       PARAMETER (MDPSI=128, MDCHI=65, MDR=301, MDZ=221)
C---------------------------------------------------------------------
C MDSTORE=0: with matrix split and with i/o on disk
C MDSTORE=1: with matrix in memory
       PARAMETER (MDSTORE=1)
C---------------------------------------------------------------------
       PARAMETER
     +  (MDPSI1 = MDPSI+1,   MDCHI1 = MDCHI+1,   MDIN2 = 2*MDCHI-2
     +  ,MDPOL = 2*MDCHI-2,  MD2CP2 = 2*MDCHI+2, MDRZ = MDR+2*MDZ
     +  ,MDCOL = 3*MDPOL,    MDCOMP = 2*MDPSI*MDPOL+MDPOL
     +  ,MDSPEC = 10,        MDSPC2 = MDSPEC+2,  MDEQ = 29
     +  ,MD2FP1 = 15,        MDDEN = 8,		 MDPOL1=MDPOL+1
     +  ,MDOVL = MDPOL*MDPOL,MDLENG = MDCOL*MDCOL, MDFAKA=20
     +	,MDBCAS = 20,	     MDPANT = 10,	 MDTANT = 4,
     +	 MDAD = 10)
C---------------------------------------------------------------------
       PARAMETER ( CPI = 3.1415926535898 )
       PARAMETER ( C2PI = 2.0 * CPI )
       PARAMETER ( CLIGHT = 2.99793E+8, 	CMU0 = 4.0E-7 * CPI,
     +	CEPS0 = 1.0 / (CMU0*CLIGHT*CLIGHT), 	CME = 9.1094E-31,
     +	CMP = 1.673E-27, 	CHE = 1.6022E-19 )
C---------------------------------------------------------------------
       PARAMETER (
     +	MSRAUX = 2*MDOVL+224, MSIAUX = 6, 	MSLAUX = 1,
     +	MSRCON = 21,	      MSICON = 7, 	MSLCON = 84,
     +	MSREQU = MDEQ*MDPOL + 6*MDSPEC + 41,
     +	MSRESH = MD2CP2,      MSIESH = 3,
     +	MSRIVI = 1,           MSIIVI = 8,
     +	MSRMTR = 2*MDLENG,
     +			      MSINUM = 3,
     +			      MSIOUT = 10,
     +	MSRPHY = MDSPEC*11 + MDAD + MDBCAS + MDPANT*3 + MDFAKA 
     +	       + MDPSI1*2  + MDTANT + 32,
     +			      MSIPHY = 13,	MSLPHY = 5,
     +  MSRVEC = 2*MDCOMP*2 + 2*MDCOL*3 + 2*MDPOL + MDPOL*2 + 2 )
