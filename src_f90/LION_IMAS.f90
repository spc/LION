SUBROUTINE LION(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
  !
  !
  !
  !
  !                  *******************************
  !                  *                             *
  !                  *    L I O N - FAST - 90    *
  !                  *                             *
  !                  *******************************
  !
  !
  !                  2-D GLOBAL WAVE CODE FOR ALFVEN AND ICRF
  !
  !                  BY
  !
  !                  L. VILLARD, K. APPERT AND R. GRUBER
  !
  !
  !                                      CRPP-EPFL
  !                                      STATION 13
  !                                      CH-1015 LAUSANNE
  !                                      SWITZERLAND
  !
  !
  !
  !*******************************************************************************
  !*
  !*    THIS VERSION:
  !*
  !*    - Electron Landau and TTMP damping in variational form (susceptibility)
  !*
  !*    0709B    L.Villard        June 2013        CRPP-EPFL
  !*    
  !*    (Previous VERSION  0709     L. Villard      DECEMBER 2007) 
  !*    (Previous VERSION  F1295     LDV      DECEMBER 1995)
  !*    (PREVIOUS VERSION: F0895, NLPHAS WITH CUT NEAR 2PI)
  !*    (PREVIOUS VERSION: F16 JUNE 1995, slion95y.f)
  !*    (PRE-PREVIOUS VERSION: FINAL VERSION FOR JET CONTRACT ARTICLE 14 NO. JT4/9007)
  !*         
  !*******************************************************************************
  !
  !
  !     THIS VERSION FEATURES:
  !     ----------------------
  !
  !>>>  0709: Reduced I/O disk for faster execution
  !
  !     - There is an added option (needs recompilation) so that the matrix is stored
  !       entirely in memory (AALL). This is selected in PARDIM.inc by setting
  !       PARAMETER (MDSTORE=1). 
  !
  !       The memory requirement for the matrix is 96*MDPSI*MDPOL**2 [Bytes]
  !       For example, a 256x256 grid requires 1.61 GB
  !         
  !       Setting MDSTORE=0 will cause the matrix to be split in blocks with i/o on disk, 
  !       exactly as was done in the previous version.
  !       Any change to MDSTORE setting, like for any other parameter setting in
  !       PARDIM.inc requires recompilation in order to be effective. The recommended 
  !       usage is to create two executables, one with MDSTORE=1, to be used as long as 
  !       there is enough processor memory, and the other wit hMDSTORE=0, to be used if 
  !       memory is in short supply.
  !
  !     - All equilibrium quantities from CHEASE are read once into a single EQ(j,jchi,js)
  !       Multiple reading / rewinding of MEQ file has now been removed.
  !
  !     - Some cumbersome legacy from previous versions was cured (NSAVE): the LION 
  !       namelist was rewritten and repetitively read at various places. This has now 
  !       been removed.
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -     POSSIBILITY TO EXTRACT THE RAPID POLIDAL PHASE VARIATION, 
  !>>>        USEFUL IN THE LOW FREQUENCY RANGE (TAE'S), OPTION NLPHAS.
  !>>>  -     THE POLOIDAL CUT IS NEAR CHI=PI.
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -	    POSSIBILITY TO DEFINE AN ANTENNA INSIDE THE PLASMA WITH VOLUME 
  !>>>	    CURRENT DENSITY (NANTYP=-1)
  !>>>
  !>>>  1295:
  !>>>
  !>>>  -     POSSIBILITY TO PUT THE WALL RIGHT ON THE PLASMA BOUNDARY 
  !>>>        (WALRAD<=1., NANTYP=-1).
  !>>>
  !
  !     -     POSSIBILITY TO USE UP/DOWN ASYMMETRIC EQUILIBRIA. IN PARTICULAR
  !	    SINGLE-NULL CONFIGURATIONS CAN NOW BE STUDIED. (THE VICINITY
  !	    OF THE X-POINT(S) STILL CANNOT BE TREATED EXACTLY).
  !
  !     -     THE LION CODE IS COUPLED TO THE EQUILIBRIUM CODE CHEASE, WHICH
  !	    HAS PROVEN TO GIVE VERY ACCURATE EQUILIBRIA AND MAPPING. SEE
  !	    H. LUTJENS ET AL., COMPUT. PHYS. COMMUN. 69 (1992) 287.
  !
  !     -     MODIFIED WEAK FORM EXPRESSION TO INCLUDE THE POSSIBILITY
  !	    TO USE AN ARBITRARY DEFINITION OF THE POLOIDAL COORDINATE CHI
  !	    (GENERALIZED JACOBIAN).
  !
  !     -     SADDLE COILS ANTENNAS OF THE TYPE INSTALLED AT JET. THE FOLLOWING
  !	    POSSIBILITIES OF POLOIDAL ANTENNA PHASING ARE INCLUDED:
  !
  !		- ONE SADDLE COIL ONLY (EITHER THE TOP ONE OR THE BOTTOM ONE)
  !		- TWO SADLLE COILS IN PHASE (++) (M=2)
  !		- TWO SADDLE COILS IN PHASE OPPOSITION (+-) (M=1)
  !
  !     -     FREQUENCY SWEEP OF THE ANTENNA CURRENT.
  !
  !     -	    TOROIDAL MODE NUMBER SCAN.
  !
  !     -	    ELECTRON LANDAU DAMPING OF ALFVEN WAVE DUE TO E_PARALLEL AND 
  !	    MAGNETIC CURVATURE DRIFT (DIAGNOSTIC).
  !
  !     -     ELECTRON LANDAU & TTMP DAMPING OF FAST WAVE (IN DIELECTRIC TENSOR).
  !
  !     -     FUNDAMENTAL ION-CYCLOTRON DAMPING OF FAST WAVE (IN DIEL. TENSOR).
  !
  !     -     HARMONIC ION-CYCLOTRON DAMPING OF FAST WAVE (IN DIEL. TENSOR).
  !
  !     -     POWER ABSORPTION PER SPECIES (DIAGNOSTIC).
  !     
  !     -     MAGNETIC-SURFACE-AVERAGED POWER ABSORPTION DENSITY (DIAGNOSTIC).
  !
  !     -     FOURIER ANALYSIS (DIAGNOSTICS) OF THE SOLUTION IN THETA ANGLE
  !	    AND IN THE POLOIDAL COORDINATE CHI.
  !
  !      -    VARIOUS POSSIBILITIES TO DEFINE THE DENSITY PROFILE (INPUT).
  !
  !
  !>>>  For LION-F versions only:
  !     -------------------------
  !
  !	- Kinetic effects on low frequency modes (omega < omega_ci) on
  !	  bulk ions, electrons and fast ions species. Bulk species are
  !	  Maxwellian, fast ions are slowing-down isotropic.
  !
  !	- Includes electron and bulk ion Landau and TTMP dampings due to 
  !	  finite parallel wave electric field, to magnetic curvature and
  !	  to finite parallel wave magnetic field.
  !
  !	- Includes fast ion Landau and TTMP dampings due to magnetic 
  !	  curvature and finite parallel wave magnetic field.
  !
  !	- Includes fast ion drive due to spatial gradient of fast ion
  !	  pressure, magnetic curvature drift and finite B_parallel.
  !
  !	- The code computes the DKE wave-particle power transfers to the
  !	  various species. It finds the marginal stability point for the
  !	  fast ion central density, fast ion central beta and fast ion
  !	  volume-average beta.
  !<<<
  !
  !     THE FOLLOWING IMPROVEMENTS HAVE BEEN MADE ON THE NUMERICAL SIDE:
  !     ----------------------------------------------------------------
  !
  !     -     THE ALGORITHM FOR MATRIX CONSTRUCTION (FORMER "LION3")
  !           AND RESOLUTION (FORMER "LION4") HAS BEEN REWRITTEN TO
  !           MINIMIZE DISK I/O. WE NOW USE A "FRONTAL" METHOD WHERE
  !           MATRIX CONSTRUCTION, LDU DECOMPOSITION AND RESOLUTION
  !           OF LOWER TRIAGULAR SYSTEM IS DONE "BLOCK BY BLOCK", I.E.
  !           SURFACE BY SURFACE. SEE SUBROUTINE "FRONTB". DISK SPACE
  !           WAS REDUCED BY A FACTOR 3. VOLUME OF DATA TRANSFERRED
  !           WITH DISK I/O WAS REDUCED BY A FACTOR 6. THIS ALGORITHM,
  !           COMBINED WITH THE USE OF VECTORIZED I/O, REDUCED THE 
  !           I/O WAIT TIME TO A NEGLIGIBLE AMOUNT (0.1% OF CPU TIME).
  !
  !     -     THERE IS THE POSSIBILITY TO RUN LION WITHOUT ANY SCRATCH
  !	    DISK SPACE FOR MATRIX STORAGE BY SETTING NLDISO=F IN THE
  !	    NAMELIST INPUT $NEWRUN. WITH THIS OPTION THE TOTAL POWER
  !	    IS COMPUTED AND THE SOLUTION IS COMPUTED ONLY AT PLASMA-
  !	    VACUUM INTERFACE. NO OTHER DIAGNOSTIC OF THE SOLUTION IS 
  !	    PERFORMED.THIS OPTION IS RECOMMENDED FOR LARGE MESH SIZES
  !	    AND EXTENSIVE POWER TRACES (LARGE NRUN).
  !
  !     -     FORMER "LION1" (EQUILIBRIUM MAPPING) HAS BEEN REMOVED.
  !           THE LION CODE NOW USES THE EQUILIBRIUM AND MAPPING
  !           GENERATED BY THE BICUBIC HERMITE FINITE ELEMENTS
  !           EQUILIBRIUM CODE "CHEASE" BY H. LUTJENS. THE EQUILIBRIUM
  !           CODE "EQLAUS" IS NOT NEEDED ANYMORE.
  !
  !     -     THE INTERPOLATIONS IN FORMER "LION2" (VACUUM & ANTENNA),
  !           SUBROUTINES TETMSH AND ROTETA, HAVE BEEN REMOVED. WE NOW
  !           SIMPLY USE THE VALUES CALCULATED IN THE EQUILIBRIUM CODE
  !           "CHEASE". PART OF THE NUMERICAL INACCURACY AND EVEN
  !           SOMETIMES UNSATIFACTORILY BEHAVIOUR FOR ELONGATED CROSS-
  !           SECTIONS COULD THUS BE REMOVED. STILL REMAINS AN
  !           INADEQUATE TREATMENT OF THE GREEN'S FUNCTION SINGULAR
  !           BEHAVIOUR FOR ELONGATIONS ABOVE 2 AND NPOL > 200.
  !
  !     -     THE CODE HAS BEEN RESTRUCTURED. IT DOES NOT CONSIST OF
  !           FIVE OR FOUR SEPARATE MAIN PROGRAMS AS BEFORE, BUT HAS
  !           ONLY ONE (LION1). OF FORMER LION1 REMAINS ONLY SUBROU-
  !           TINES CLEAR, PRESET, LABRUN, DATA, AUXVAL. FORMER LION2
  !           IS REPLACED WITH "CALL VACUUM", FORMER LION3 AND LION4
  !           ARE REPLACED WITH "CALL ORGAN4", AND FORMER LION5 IS
  !           REPLACED WITH "CALL DIAGNO". NOTE THAT THREE DIFFERENT
  !           BLANK COMMON DEFINITIONS ARE USED IN THE THREE PARTS
  !           OF THE PROGRAM:
  !
  !              VACUUM & ALL DAUGHTER SUBROUTINES USE COMVID
  !              ORGAN4 & ALL DAUGHTER SUBROUTINES USE COMMTR
  !              DIAGNO & ALL DAUGHTER SUBROUTINES USE COMPLO
  !
  !     -     FOR A 100X100 MESH THE LION CODE NEEDS LESS THAN 2 MW OF 
  !	    MEMORY AND TAKES ABOUT 30 CPU S PER ANTENNA FREQUENCY AND
  !	    PER TOROIDAL WAVE NUMBER ON A CRAY YMP.
  !	    
  !
  !*******************************************************************************
  !
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE GENERATOR FREQUENCY ('FREQCY') OR THE TOROIDAL MODE NUMBER
  !     (WNTORO). IN OTHER WORDS IT IS POSSIBLE TO MAKE A SWEEP OF
  !     THE ANTENNA FREQUENCY OR A TOROIDAL MODE NUMBER SCAN.
  !
  !     THE NUMBER OF DIFFERENT FREQUENCIES IN THE SWEEP IS GIVEN IN THE
  !     NAMELIST $NEWRUN OF LION ('NRUN'). IN THIS CASE, THE INITIAL FREQUENCY
  !     IS GIVEN BY 'FREQCY' [HZ] AND IS INCREMENTED BY CONSTANT AMOUNTS.
  !     THE INCREMENT IS GIVEN IN THE NAMELIST $NEWRUN OF LION (DELTAF).
  !
  !     ALTERNATIVELY, THERE IS THE POSSIBILITY TO SPECIFY THE NUMBER OF
  !     TOROIDAL WAVE NUMBERS IN THE N-SCAN ('NTORSP'). IN THIS CASE, THE
  !     INITIAL TOROIDAL MODE NUMBER IS 'WNTORO' AND IS INCREMENTED BY
  !     CONSTANT AMOUNTS. THE INCREMENT IS GIVEN BY 'WNTDEL'.
  !
  !     NOTE THAT SPECIFYING NTORSP > 1 WILL FORCE NRUN TO 1, IN ORDER TO AVOID
  !     A TOO LARGE RUN (AND A TOO LARGE OUTPUT FILE...). THIS MEANS THAT THE
  !     TOROIDAL MODE NUMBER SCAN TAKES PRECEDENCE OVER THE FREQUENCY SWEEP.
  !*******************************************************************************
  !
  !     IN THIS VERSION THE EQUILIBRIUM CALCULATION AND MAPPING IS DONE
  !     IN THE CODE CHEASE (H. LUTJENS). THE DATA IS TRANSFERRED VIA 
  !     DISK I/O ON THE FOLLOWING CHANNELS:
  !
  !        NSAVE     NAMELIST VARIABLES FROM CHEASE (NPSI, NCHI, ETC.)
  !        MEQ       EQUILIBRIUM QUANTITIES (i,jchi),js=1,npsi+1 -> EQ(i,jchi,js)
  !        NVAC      QUANTITIES AT PLASMA-VACUUM INTERFACE
  !        NDES      R,Z,AND NORMALS TO PSI=CONST. R,Z OF P.V.I.
  !
  !     THEREFORE WE DO NOT USE 'LION1', FORMERLY 'ERATO1'.
  !
  !     PLEASE PAY ATTENTION TO THE FACT THAT THE NAMELIST $NEWRUN
  !     TRANSFERRED BY CHEASE IS READ IN SUBROUTINE AUXVAL, WHEREAS
  !     NAMELIST $NEWRUN CONTAINING THE INPUT PARAMETERS FOR LION
  !     IS READ IN SUBROUTINE DATA. THIS IS DONE TO AVOID CONFLICTS
  !     IN THE DEFINITIONS OF THESE NAMELISTS (THEY ARE DIFFERENT!).
  !     THE DEFINITIONS OF SOME OF THE NAMELIST VARIABLES ARE DIFFERENT.
  !     PLEASE LOOK INTO SUBROUTINE AUXVAL FOR MORE INFORMATION.
  !
  !*******************************************************************************
  !
  !
  !*******************************************************************************
  !***                                                                         ***
  !***                       U S E R    M A N U A L                            ***
  !***                                                                         ***
  !*******************************************************************************
  !
  !
  !     REFERENCES :
  !     ----------
  !               [1] L.VILLARD, K.APPERT, R.GRUBER AND J.VACLAVIK,
  !                   'GLOBAL WAVES IN COLD PLASMAS',LAUSANNE REPORT
  !                   LRP 275/85 (1985) CRPP, LAUSANNE, SWITZERLAND.
  !                   COMPUT. PHYS. REPORTS 4 (1986) 95.
  !
  !               [2] R.GRUBER ET AL., COMPUT. PHYS. COMMUN. 24
  !                   (1981) 323.
  !
  !               [3] K.APPERT ET AL., NUCL.FUS. 22 (1982) 903.
  !
  !               [4] L. VILLARD, 'PROPAGATION ET ABSORPTION D'ONDES
  !                   AUX FREQUENCES D'ALFVEN ET CYCLOTRONIQUES
  !                   IONIQUES DANS LES PLASMAS TORIQUES', THESE 
  !                   NO. 673, DEPARTEMENT DE PHYSIQUE, EPFL, 
  !                   & LAUSANNE REPORT LRP 322/87 (1987) CRPP, 
  !                   LAUSANNE, SWITZERLAND.
  !
  !		[5] L. Villard, G.Y. Fu, Nucl. Fusion 32 (1992) 1695.
  !
  !		[6] S. Brunner, J. Vaclavik, 'On Absorption of Low Frequency
  !		    Electromagnetic Fields', LRP 471/93 (1993) CRPP, Lausanne.
  !
  !		[7] L. Villard, S. Brunner, J. Vaclavik, 'Global Marginal
  !		    Stability of TAEs in the Presence of Fast Ions', accepted
  !		    for publication in  Nucl. Fusion (1995).
  !
  !******************************************************************************
  !***                                                                        ***
  !***             COMPLEMENTARY INFORMATION ABOUT INPUT VARIABLES            ***
  !***                                                                        ***
  !******************************************************************************
  !
  !
  !                   1. PLASMA PHYSICS
  !                      --------------
  !
  !        THE EQUILIBRIUM AND MAPPING IS DONE IN THE IDEAL MHD CODE CHEASE.
  !     PLEASE CONSULT THE CHEASE USER MANUAL FOR INFORMATION ABOUT INPUT
  !     VARIABLES SPECIFYING THE GEOMETRY, PROFILES AND MESH.
  !
  !     NOTE THAT THE $EQDATA INPUT NAMELIST OF CHEASE MUST CONTAIN 'NIDEAL=2,'
  !     SO THAT THE DATA TRANSFER FROM CHEASE TO LION IS DONE CORRECTLY.
  !
  !     THE DATA TRANSFERED FROM CHEASE CONTAINS THE FOLLOWING INFORMATION ON
  !     THE UNITS NAMED BELOW:
  !
  !     NSAVE    NAMELIST $NCHEAS CONTAINING, AMONG OTHER THINGS:
  !              'NPSI':  NUMBER OF RADIAL INTERVALS
  !              'NCHI':  NUMBER OF POLOIDAL INTERVALS ALL AROUND (PLEASE NOTE
  !                       THAT IN LION THIS BECOMES VARIABLE 'NPOL', AND THAT
  !                       'NCHI' IS DEFINED IN LION AS THE NUMBER OF POLOIDAL
  !                       INTERVALS IN THE UPPER HALF-PLANE)
  !              'CPSRF': PSI AT PLASMA SURFACE
  !              'QIAXE': 1 / Q AT MAGNETIC AXIS
  !              AND OTHER VARIABLES.
  !
  !     MEQ      EQUILIBRIUM QUANTITIES (i,jchi),js=1,npsi+1 -> EQ(i,jchi,js)
  !
  !     NVAC     SURFACE QUANTITIES FOR VACUUM CALCULATION
  !
  !     NDES     QUANTITIES NEEDED FOR PLOTS
  !
  !     THE FILES NSAVE, MEQ, NVAC AND NDES CAN BE SAVED AFTER RUNNING CHEASE
  !     SO THAT THEY CAN BE USED BY LION, IN CASE SEVERAL LION RUNS ARE MADE
  !     WITH THE SAME EQUILIBRIUM AND THE SAME MESH.
  !
  !------------------------------------------------------------------------------
  !
  !        ALL OTHER INPUT VARIABLES MUST BE SPECIFIED IN  NAMELIST $NEWRUN OF
  !     LION IN THE DEFAULT INPUT (TAPE5) FILE. SEE AN EXAMPLE OF INPUT BELOW.
  !
  !
  !        THEY CONSIST OF THE FOLLOWING:
  !
  !      
  !     'NLCOLD'  : SWITCH OFF ION CYCLOTRON DAMPING 
  !		  .T. ==> COLD PLASMA DIELECTRIC TENSOR
  !		  .F. ==> WARM IONS DIEL. TENS. (ORDER 0 IN LARMOR)
  !
  !     'NLCOLE'  : SWITCH OFF ELECTRON LANDAU & TTMP DAMPING OF FAST WAVE:
  !		  .T. ==> NO ADDITIONNAL TERM IN EPSILON_PERPPERP
  !		  .F. ==> ADDITIONNAL DAMPING TERM IN EPSILON_PERPPERP.
  !		  NOTE THAT THE ALFVEN WAVE ELECTRON LANDAU DAMPING RATE
  !		  IS EVALUATED AS A DIAGNOSTIC OF THE OBTAINED SOLUTION
  !		  IRRESPECTIVELY OF THE VALUE OF NLCOLE.
  !     
  !     'NELDTTMP': Type of model for Electron Landau & TTMP damping
  !                 1 ==> Additional damping term in epsilon_{perp,perp}, 
  !                       with k_perp from Fast Wave dispersion relation;
  !                       see WEPSEL in subroutine QUAEQU
  !                 2 ==> Additional damping term propto B_parallel, 
  !                       consistent in the weak variational form;
  !                       see WEPSTTMP in subroutine QUAEQU, CONST1,2,3, etc.
  !                       Factor 1/2 for combined ELD and TTMP of fast waves
  !
  !     'NELDTTMPCOR': Correction (perturbative) to electron Landau & TTMP damping diagnostics
  !                 0 (default): do not correct
  !                 1 : do the correction; option valid only for NELDTTMP=1;
  !                    WARNING: the powers will not be consistent
  !
  !     'ANU'     : COLLISIONAL DAMPING (NU/OMEGA) (COLD MODEL ONLY)
  !
  !     'DELTA'   : PHENOMENOLOGICAL DAMPING       (BOTH MODELS)
  !
  !     'NRSPEC'  : NUMBER OF ION SPECIES. (negative in input limits max value read from database)
  !
  !     'ACHARG()': ATOMIC CHARGES OF ION SPECIES [ATOMIC UNITS]
  !
  !     'AMASS()' : ATOMIC MASSES OF ION SPECIES  [ATOMIC UNITS]
  !
  !     'CENDEN()': NUMBER DENSITIES OF ION SPECIES [M**-3] ON MAGNETIC
  !                 AXIS (M.A.).
  !
  !     'CENTI()' : PARALLEL ION TEMPERATURES [EV] ON M.A. (WARM MODEL ONLY)
  !
  !     'CENTIP() : PERPENDICULAR     "       [ "]    "    (WARM MODEL ONLY)
  !
  !     'CENTE'   : ELECTRON TEMPERATURE      [ "]    "    (BOTH MODELS)
  !
  !     'NHARM'   : Maximum absolute value of the harmonic number used in 
  !                 constructing the warm plasma dielectric tensor, i.e. the 
  !                 tensor includes components for harmonic numbers 
  !                 from -NHARM to +NHARM                  (WARM MODEL ONLY)
  !
  !     'NDENS', 'NDARG', 'NDDEG', 'AD(.)', 'EQDENS', 'EQKAPD'
  !      SPECIFY THE TOTAL MASS DENSITY PROFILE [NORMALIZED TO TOTAL MASS
  !      DENSITY ON MAGNETIC AXIS] :
  !
  !      NDENS = -3 ==> use database profiles and cubic spline fit
  !
  !      NDENS = -2 ===> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !
  !         RHO = SQRT (P/P_AXIS)
  !
  !      NDENS = -1 ===> POLYNOMIAL FUNCTION OF
  !
  !                      S**2 IF NDARG = 1
  !                      S    IF NDARG = 2
  !
  !         RHO = 1. + SUM(J=1,NDDEG) {AD(J)*ARG**J}
  !
  !      NDENS = 0  ===>
  !
  !         RHO = (1. - EQDENS * S*S) **EQKAPD
  !
  !      NDENS = 1  ===>
  !
  !         RHO = 1.            FOR S < EQDENS
  !         RHO = EQKAPD + (1.-EQKAPD) * COS (PI/2*(S-EQDENS)) **2  
  !
  !      NDENS = 2  ===>
  !
  !         RHO = 1.            FOR S < EQDENS
  !         RHO = A CUBIC FUNCTION OF S SUCH THAT:
  !                  RHO     (S=EQDENS) = 1.
  !                  DRHO/DS (S=EQDENS) = 0.
  !                  RHO     (S=1)      = EQKAPD
  !                  DRHO/DS (S=1)      = EQALFD
  !
  !	NDENS = 3  ==>
  !
  !	  RHO = T^2/q^2 * q0^2/T0^2 (SO THAT OMEGAXTAE IS FLAT)
  !
  !         (FUNCTION DENSIT, P1C2S34)
  !
  !
  !     'EQTI()', EQKAPT()', 'NTEMP':
  !      SPECIFY THE ION PARALLEL AND PERPENDICULAR
  !      TEMPERATURE PROFILES [EV] :
  !
  !        NTEMP = -3 ==> use database profiles and cubic spline fit
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TI(PARALLEL) = CENTI(I) * SQRT (P/P_AXIS)
  !
  !        NTEMP = -1 ==> POLYNOMIAL FUNCTION OF
  !
  !                      S**2 IF NDARG = 1
  !                      S    IF NDARG = 2
  !
  !          TE/TI()/TIP() = CENTE/CENTI()/CENTIP() * 
  !                          (1. + SUM(J=1,NDDEG) {ATE/ATI/ATIP(J)*ARG**J})
  !
  !        NTEMP # -1 OR -2  ==>
  !
  !        TI(PARALLEL) = CENTI(I) * (1.-EQTI(I)*S*S) **EQKAPT(I)
  !        (SUBROUTINE TEMPI)
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TI(PERP) = CENTIP(I) * SQRT (P/P_AXIS)
  !
  !        NTEMP=-1   ==> POLYNOMIAL (SEE ABOVE)
  !
  !        NTEMP # -2 ==>
  !
  !        TI(PERP)    = CENTIP(I) * (1.-EQTI(I)*S*S) **EQKAPT(I)
  !        (SUBROUTINE TEMPRP)
  !
  !
  !     'EQTE', 'EQKPTE', 'NTEMP':
  !      SPECIFY THE ELECTRON TEMPERATURE PROFILE [EV] :
  !
  !        NTEMP = -2 ==> PROPORTIONAL TO SQRT(EQUILIBRIUM_PRESSURE)
  !        TE = CENTE * SQRT (P/P_AXIS)
  !
  !        NTEMP=-1   ==> POLYNOMIAL (SEE ABOVE)
  !
  !        NTEMP # -1 OR -2 ==>
  !
  !        TE = CENTE * (1. - EQTE * S*S) ** EQKPTE
  !        (FUNCTION TEMPEL)
  !
  !      NOTE THAT THE CONSISTENCY OF BETA_AXIS WITH THE EQUILIBRIUM VALUE
  !      IS NOT CHECKED WITH NTEMP = -2 AND NDENS = -2.
  !
  !     'BNOT':   MAGNETIC FIELD ON M.A. [TESLA]. (NOT THE VACUUM FIELD)
  !
  !     'RMAJOR': MAJOR RADIUS (DISTANCE AXIS OF THE TORUS, M.A.) [M]
  !
  !--------------------------------------------------------------------------
  !>>>	For LION-F Versions only:
  !	-------------------------
  !
  !     'NLFAST': .T. ==> FAST PARTICLES ARE INTRODUCED (!DIAGNOSTIC!):
  !
  !	==>	'NRSPEC' IS FORCED TO 1 (1 BULK ION SPECIES) ;
  !
  !	==>	'ACHARG(2)' IS THE ATOMIC CHARGE OF FAST PARTICLES ;
  !
  !	==>	'AMASS(2)' IS THE ATOMIC MASS OF FAST PARTICLES ;
  !
  !	==>	'CENDEN(2)' IS THE DENSITY OF FAST PARTICLES ON
  !		 MAGNETIC AXIS [M**-3] (CENDEN(2) << CENDEN(1));
  !
  !	==>	 THE DENSITY PROFILE OF FAST PARTICLES IS SPECIFIED BY 
  !		'CENDEN(2)', 'EQFAST', 'EQKAPF(JF)'JF=1,NFAKAP :
  !
  !		 N_FAST = CENDEN(2) * (1.-EQFAST*S*S) **EQKAPF(JF) [M**-3]
  !
  !       ==>     'VBIRTH' IS THE BIRTH VELOCITY OF FAST PARTICLES [M/S]
  !
  !		THE CODE COMPUTES THE POWER ABSORPTIONS FROM DRIFT KINETIC
  !		MODEL, AND THE CENTRAL DENSITY OF FAST PARTICLES FOR
  !		MARGINAL STABILITY, FOR NFAKAP DIFFERENT FAST PARTICLE
  !		DENSITY PROFILES CHARACTERIZED BY EQKAPF(JF), JF=1..NFAKAP.
  !
  !		IT CAN ALSO, FOR EACH FAST PARTICLE DENSITY PROFILE, 
  !		COMPUTE THE MARGINAL STABILITY FOR VARIOUS BULK DENSITIES
  !		AND TEMPERATURES OR MAGNETIC FIELD VALUES WHICH ARE TAKEN
  !		SO AS TO MAKE A CONSTANT BULK BETA SCAN.
  !
  !	'NFAKAP': NUMBER OF FAST PARTICLE DESITY PROFILES
  !
  !	'NBCASE': NUMBER OF CASES FOR THE CONSTANT BETA SCAN
  !
  !	'EQKAPF()': PARAMETER FOR FAST PARTICLE DENSITY PROFILE
  !
  !	'EQFAST': PARAMETER FOR FAST PARTICLE DENSITY PROFILES
  !
  !	'CEN0()': CENTRAL ION DENSITIES FOR CONSTANT BETA SCAN
  !
  !	'NBTYPE': TYPE OF CONSTANT BETA SCAN:
  !
  !	      1 ==> n_i(o) IS VARIED (CEN0()), 
  !		    T_i(o) and T_e(o) as 1/n_i(o),
  !		    Bo is kept constant.          ==> v_A(o) is varied
  !
  !	      2 ==> n_i(o) IS VARIED (CEN0()),
  !		    Bo as sqrt(n_i(o)),		  ==> v_A(o) constant
  !		    T_i(o) and T_e(o) are kept constant
  !
  !	'NLTTMP': .F. ==> SWITCH OFF TTMP BY PUTTING B_PARALLEL TO 0 IN DKE
  !			  POWER EXPRESSIONS.
  !<<<
  !---------------------------------------------------------------------------
  !
  !                  2. ANTENNA AND WALL
  !                     ----------------
  !
  !
  !     THE VARIABLE 'NANTYP' SELECTS THE TYPE OF ANTENNA.
  !
  !     NANTYP =-1 ====> "Helical volume antenna". Volume antenna currents in 
  !			the plasma between s=SAMIN and s=SAMAX, directed 
  !			along psi=const surfaces, defined by:
  !
  !			j_a = grad psi x grad sigma, 
  !
  !			with sigma(s,chi,phi) = H(s-SAMIN) * H(SAMAX-s) *
  !			( sum[j=1..MANCMP] { CURSYM(j)*cos(MPOLWN(j)*chi)
  !			 		   + CURASY(j)*sin(MPOLWN(j)*chi) } )
  !			 * exp{i*WNTORO*phi}.
  !
  !			Note that in this case there is no antenna in the 
  !			vacuum region: the vacuum contribution to the right-
  !			hand side is put to zero by setting SAUTR(j) to zero.
  !
  !     NANTYP = 1 ====> "HELICAL ANTENNA". CURRENT SHEET AT A CONSTANT
  !                      DISTANCE OF THE PLASMA SURFACE. THE CURRENTS
  !                      ARE HARMONIC FUNCTIONS OF THE POLOIDAL ANGLE
  !                      THETA, WITH POLOIDAL WAVENUMBERS GIVEN BY
  !                      'MPOLWN(J)':
  !
  !                         SAUTR(THETA) = SUM(J=1 TO MANCMP) OF
  !                         CURSYM(J)*COS(MPOLWN(J)*THETA) +
  !                         I*CURASY(J)*SIN(MPOLWN(J)*THETA).
  !
  !                      THERE ARE NO FEEDERS.
  !
  !     NANTYP = 2 ====> LFS OR HFS ANTENNA. SPECIFIED BY THE INPUT
  !                      PARAMETERS THANT(J), J=1,4 AND CURSYM(1).
  !
  !                      THANT(J) ARE ANGLES GIVEN IN DEGREES, WITH VALUES
  !                      BETWEEN 0 AND 360. THANT(J) ARE MEASURED FROM THE
  !                      MAGNETIC AXIS HORIZONTAL.
  !                      THE LFS OR HFS ANTENNA IS A CURRENT SHEET WHICH,
  !                      BETWEEN THETA = THANT(2) AND THANT(3), IS AT A
  !                      CONSTANT DISTANCE OF THE PLASMA SURFACE AND
  !                      CARRIES CONSTANT PURE POLOIDAL CURRENTS :
  !
  !                         SAUTR(THETA) = CURSYM(1)
  !
  !                      BETWEEN THETA = THANT(1) AND THETA = THANT(2)
  !                      AND THETA = THANT(3) AND THETA = THANT(4) ARE
  !                      THE FEEDERS, WHERE THE DISTANCE FROM THE
  !                      PLASMA SURFACE INCREASES SMOOTHLY UP TO THE
  !                      WALL SURFACE.
  !
  !                      THE LFS ANTENNA EXTENDS ACROSS THE THETA=0
  !                      LINE. THEREFORE THANT(3) < THANT(4) < THANT(1)
  !                      < THANT(2).
  !                      THE HFS ANTENNA CANNOT CROSS THE THETA=0
  !                      LINE. THEREFORE THANT(1) < THANT(2) < THANT(3)
  !                      < THANT(4).
  !
  !                      THE SELECTION OF EITHER LFS OR HFS ANTENNA
  !                      AUTOMATIC :
  !
  !                            THANT(3).LT.THANT(2) SELECTS LFS ANTENNA
  !                            THANT(2).GT.THANT(3) SELECTS HFS ANTENNA
  !
  !                      NOTE THAT WE MUST HAVE THANT(1) < THANT(2)
  !                      AND THANT(3) < THANT(4).
  !
  !     NANTYP = 3 ====> TOP/BOTTOM ANTENNA. THE ANTENNA SURFACE IS UP/
  !    		       DOWN SYMMETRIC, AT CONSTANT DISTANCE OF THE 
  !  		       PLASMA SURFACE BETWEEN THETA = ANTUP AND
  !		       THETA = PI - ANTUP. THE CURRENTS ARE DEFINED
  !		       AS FOR NANTYP = 1.
  !
  !     NANTYP = 4 ====> SADDLE COIL ANTENNA. THE ANTENNA SURFACE IS THE
  !		       SAME AS FOR THE HELICAL ANTENNA: CURRENT SHEET
  !		       AT A DISTANCE ANTRAD-1 OF THE PLASMA SURFACE.
  !		       THE CURRENT = CURSYM(1) IN [THANT(1),THANT(2)]
  !		       AND IN [THANT(3),THANT(4)], SMOOTHLY DECAYING
  !		       TO ZERO NEAR THANT(J).
  !
  !     'NANT_ITM'   0 (default), 1 if uses antennas_in and antennas_tools to define the antenna geometry
  !
  !	'NSADDL'	SELECTS THE TYPE OF SADDLE COIL PHASING IN
  !			THE POLOIDAL PLANE.
  !			THIS IS DISCARDED UNLESS NANTYP = 4.
  !
  !	 NSADDL = 0 ==>	ONLY 1 SADDLE COIL ANTENNA IS CONNECTED:
  !			BETWEEN THANT(1) AND THANT(2).
  !	 NSADDL = 1 ==>	2 SADDLE COILS ARE CONNECTED. THE CONNECTION
  !			IS DONE IN OPPOSITE DIRECTIONS FOR THE 2
  !			COILS, THUS DEFINING A PREDOMINANTLY 'M=1'
  !			ANTENNA CURRENT COMPONENT: (+-) PHASING.
  !	 NSADDL = 2 ==>	2 SADDLE COILS ARE CONNECTED. THE CONNECTION
  !			IS DONE IN THE SAME DIRECTION FOR THE 2
  !			COILS, THUS DEFINING A PREDOMINANTLY 'M=2'
  !			ANTENNA CURRENT COMPONENT: (++) PHASING.
  !			THIS IS THE DEFAULT VALUE.
  !
  !
  !     'NLDIP' SELECTS MONOPOLE OR DIPOLE ANTENNA. THE DIPOLE OPTION
  !             HAS NOT BEEN PROGRAMMED YET. DEFAULT: F ==> MONOPOLE.
  !
  !     'ANTRAD' DISTANCE ANTENNA-MAGNETIC AXIS IN UNITS OF THE MINOR 
  !              RADIUS IN THE Z=0 PLANE.
  !
  !     'NANTSHEET' Number of antenna current sheets. For NANTSHEET>1, 
  !                 the "power at antenna" might be wrong ...
  !                 and hopefully the "power at plasma surface" is right.
  !                 The current sheets are placed equidistantly between 
  !                 ANTRAD and ANTRADMAX. The current distribution as fct 
  !                 of theta is identical for all sheets.
  !
  !     'ANTRADMAX' Max distance antenna-magnetic axis
  !
  !     'WALRAD' DISTANCE WALL-MAGNETIC AXIS IN UNITS OF THE MINOR
  !              RADIUS IN THE Z=0 PLANE. 
  !
  !     'FREQCY' IS THE GENERATOR FREQUENCY IN HZ.
  !
  !     'DELTAF' IS THE FREQUENCY INCREMENT FOR FREQUENCY TRACES.
  !
  !     'NRUN'   IS THE NUMBER OF RUNS FOR FREQUENCY TRACES.
  !
  !     'WNTORO' IS THE TOROIDAL WAVE NUMBER.
  !
  !     'WNTDEL' IS THE TOROIDAL WAVENUMBER INCREMENT FOR TOROIDAL WN SCANS
  !
  !     'NTORSP' IS THE NUMBER OF TOROIDAL WN'S FOR TOROIDAL WN SCANS
  !
  !
  !
  !                  3. OUTPUT AND PLOT
  !                     ---------------
  !
  !     'NLDISO' : SWITCH COMPUTATION AND DIAGNOSTICS OF THE SOLUTION.
  !
  !		 .TRUE. ==> THE SOLUTION IS COMPUTED EVERYWHERE. 
  !			    DIAGNOSTICS ARE PERFORMED, PRINTED AND/OR
  !			    PLOTTED ACCORDING TO NLOTP5() AND NLPLO5()
  !			    (SEE BELOW).
  !			    WITH THIS OPTION (WHICH IS THE DEFAULT)
  !			    RUNNING THE LION CODE REQUIRES SCRATCH
  !			    DISK SPACE FOR MATRIX STORAGE:
  !			        96 * NPSI * NPOL**2  (BYTES)
  !
  !		 .FALSE.==> THE SOLUTION IS COMPUTED ONLY AT THE PLASMA-
  !			    VACUUM INTERFACE.
  !			    THE ONLY DIAGNOSTIC IS THE TOTAL POWER, 
  !			    WHICH IS PERMANENT OUTPUT. IT IS CORRECT
  !			    AS LONG AS THERE IS NO SOURCE INSIDE THE 
  !			    PLASMA.
  !			    NO OTHER DIAGNOSTICS ARE PERFOMED, IRRES-
  !			    PECTIVELY OF NLOTP5() AND NLPLO5().
  !			    WITH THIS OPTION THE LION CODE DOES NOT
  !			    USE DISK SPACE FOR MATRIX STORAGE, THEREFORE
  !			    THE TURNAROUND TIME IS REDUCED.
  !
  !     'NLOTP0' : GENERAL SWITCH FOR LINE-PRINTER OUTPUT AND GRAPHICS.
  !
  !                NOTE THAT THE TOTAL POWER IS PERMANENT OUTPUT.
  !
  !     'NLOTP1()' : LINE-PRINTER OUTPUT FOR EQUILIBRIUM QUANTITIES (LION1).
  !            (1) : 
  !            (2) : 
  !            (3) :
  !            (4) :
  !            (5) :
  !
  !     'NLOTP2()' : LINE-PRINTER OUTPUT FOR VACUUM QUANTITIES (LION2).
  !            (1) : GEOMETRICAL QUANTITIES AT PLASMA SURFACE.
  !            (2) : POSITIONS OF PLASMA SURFACE, ANTENNA AND WALL.
  !            (3) : ANTENNA CURRENT POTENTIAL VS CHI AND THETA.
  !            (4) : NON-HERMICITY OF VACUUM MATRIX.
  !            (5) :
  !
  !     'NLOTP3()' : LINE-PRINTER OUTPUT FOR MATRIX CONSTRUCTION (LION3).
  !            (1) : 
  !            (2) :
  !
  !     'NLOTP4()' : LINE-PRINTER OUTPUT FOR MATRIX SOLVER (LION4).
  !            (1) : NAMELIST
  !            (2) : OHM-VECTOR
  !            (3) : SOLUTION AT PLASMA BOUNDARY
  !            (4) :
  !            (5) :
  !
  !     'NLOTP5()' : LINE-PRINTER OUTPUT FOR SOLUTION DIAGNOSTICS (LION5).
  !            (1) : NAMELIST
  !            (2) : RADIAL POWER ABSORPTIONS AND OTHER DIAGNOSTICS
  !            (3) : EXTENDED OUTPUT OF RADIAL DIAGNOSTICS
  !            (4) : 2-D POWER ABSORPTION DENSITY
  !            (5) : 2-D POWER ABSORBED IN EACH CELL
  !            (6) : 2-D NORMAL COMPONENT OF POYNTING
  !            (7) : 2-D PERP   COMPONENT OF POYNTING
  !            (8) : 2-D PARALLEL COMPONENT OF POYNTING
  !            (9) :
  !           (10) : 2-D REAL PART OF E-NORMAL
  !           (11) : 2-D REAL PART OF E-PERP
  !           (12) : 2-D IMAGINARY PART OF E-NORMAL
  !           (13) : 2-D IMAGINARY PART OF E-PERP
  !           (14) : 2-D POLARAZATION NORM OF E-PLUS SQUARED
  !           (15) : 2-D POLARIZATION NORM OF E-MINUS SQUARED
  !           (16) : ELECTRIC FIELD ON OUTER EQUATORIAL PLANE (CHI=0)
  !           (17) :
  !           (18) : POLOIDAL FOURIER COMPONENTS OF E-NORMAL IN THETA
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (19) : POLOIDAL FOURIER COMPONENTS OF E-PERP   IN THETA
  !           (20) : POLOIDAL FOURIER COMPONENTS OF E-NORMAL IN CHI
  !           (21) : POLOIDAL FOURIER COMPONENTS OF E-PERP   IN CHI
  !           (22) : 2-D EPSILON SUB-N-N - N**2 / R**2
  !           (23) : 2-D IMAGINARY PART OF EPSILON SUB N-N
  !           (24) : 2-D OMEGA - OMEGACI
  !           (25) : SHEAR ALFVEN FREQUENCIES (NEGLECTING TOROIDAL
  !                  COUPLING; FOR SINGLE SPECIES PLASMA ONLY),
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (26) : DENSITY, MINOR AND MAJOR RADIUS, IN NORMALISED AND
  !                  S.I. UNITS, ON THE OUTER EQUATORIAL PLANE (CHI=0).
  !
  !
  !           (31) : POLOIDAL FOURIER COMPONENTS OF B_N IN THETA
  !                  FOR M = 'MFL', MFL+1, .., MFU(=MFL+MD2FP1-1)
  !           (32) : POLOIDAL FOURIER COMPONENTS OF B_B IN THETA
  !           (33) : POLOIDAL FOURIER COMPONENTS OF B_PAR IN THETA
  !           (34) : POLOIDAL FOURIER COMPONENTS OF B_N IN CHI
  !           (35) : POLOIDAL FOURIER COMPONENTS OF B_B IN CHI
  !           (36) : POLOIDAL FOURIER COMPONENTS OF B_PAR IN CHI
  !
  !     THE 2-D TABLES GIVE THE VALUES ON THE CENTERS OF THE CELLS
  !  OF THE (S,CHI) MESH. A LINE IN THE TABLE CORRESPONDS TO A PSI =
  !  CONST SURFACE. IT GOES FROM CHI=0 TO CHI=PI IN THE UPPER HALF-PLANE
  !  AND FROM CHI=PI TO CHI=2*PI IN THE LOWER HALF-PLANE. THE VALUES
  !  ARE NORMALIZED TO THEIR MAXIMUM VALUE. THE FIRST AND THE LAST LINES
  !  OF THE TABLES GIVE THE POLOIDAL NUMBERING OF THE CELLS. THE FIRST
  !  COLUMN GIVES THE RADIAL NUMBERING OF THE CELLS. ALL OUTPUT IS IN
  !  CODE-NORMALIZED UNITS UNLESS SPECIFIED.
  !
  !     'NLPLO5()' : GRAPHICAL OUTPUT FOR LION5

  !            (1) : GENERAL SWITCH FOR GRAPHICAL PLOTS

  !            (2) : RADIAL POWER ABSORPTION AND FLUX

  !            (3) : FAST ION BETA_CRITICAL AND P_DK(S). WRITES TABLES 
  !                  ON TAPE26 AND TAPE27
  !                  => MATLAB (plotfast.m AND plotpdks(.,.).m)

  !            (4) : 2-D GRAPHICAL PLOTS : 
  !
  !		 - IF NPLTYP = 1 (DEFAULT): PREPARES PLOT FILES FOR USE 
  !		   WITH THE GRAPHICAL PACKAGE BASPL:
  !		   WRITES A FILE coords (TAPE18) OF (R,Z)
  !		   COORDINATES OF MESH CELLS CENTERS AND A FILE fields
  !		   (TAPE19)
  !		   OF (R,Z) COMPONENTS OF E, POWER ABSORPTION DENSITY, 
  !		   NORMAL AND BINORMAL COMPONENTS OF E, NORMAL, BINORMAL
  !		   AND PARALLEL COMPONEMTS OF B. THE 
  !		   PLOTS ARE THEN DONE WITH THE GRAPHICAL PACKAGE BASPL.
  !		   IT ALLOWS TO MAKE COLOR PLOTS, ARROW PLOTS, CONTOUR
  !		   PLOTS, ... INTERACTIVELY.
  !
  !		 - IF NPLTYP = 2 : PLOT FILE FOR USE WITH THE GRAPHICAL
  !		   PACKAGE explorer:
  !		   WRITES A FILE corfields (TAPE19) CONTAINING COORDINATES
  !	           AND FIELDS.
  !
  !            (5) : POLOIDAL FOURIER COMPONENTS (CABS) OF E_n, E_b, B_n, 
  !                  B_b AND B_//. WRITES A TABLE ON TAPE25 =>
  !                  => MATLAB  (plotfour.m).
  !
  ! NOT ACTIVE:(6) : 2-D ARROWS OF POYNTING VECTOR (NORMAL,PERP)
  ! NOT ACTIVE:(7) : 2-D ARROWS OF REAL (E-NORMAL,E-PERP) AT VARIOUS
  !                  TOROIDAL ANGLES ('ANGLET(J)' J=1 TO 'NCUT')
  ! NOT ACTIVE:(8) : 2-D CONTOURS OF NORM OF E-PLUS
  ! NOT ACTIVE:(9) : 2-D CONTOURS OF REAL(E-NORMAL) AT ANGLET(J)
  ! NOT ACTIVE:(10) : 2-D CONTOURS OF REAL(E-PERP)   AT ANGLET(J)
  ! NOT ACTIVE:(11) : 2-D CONTOURS OF POYNTING VECTOR (PARALLEL)
  ! NOT ACTIVE:(12) : 2-D LINE EPSILON SUB (N,N) - N**2/R**2 = 0
  ! NOT ACTIVE:(13) : 2-D CONTOURS OF IMAGINARY PART OF EPSILON (N,N)
  ! NOT ACTIVE:(14) : 2-D LINE(S) OMEGA=OMEGACI
  !
  !           (15) TO (25) : FREE
  !
  !     THE DIMENSION OF THE 2-D PLOTS IS SPECIFIED BY 'ALARG' AND
  !  'AHEIGT'. THE NUMBER OF CONTOUR LINES IS GIVEN BY 'NCONTR'. THE
  !  ARROWS HAVE A SIZE NORMALIZED TO THEIR MAXIMUM VALUE. THIS SIZE
  !  IS CONTROLLED BY THE VARIABLE 'ARSIZE'.
  !
  !     THE FOURIER ANALYSIS IN POLOIDAL ANGLE (CHI AND/OR THETA) IS
  !  MADE FOR VALUES OF M = MFL, MFL+1, MFL+2, .. , MFU=MFL+MD2FP1-1.
  !  THE VALUE OF 'MFL' CAN BE GIVEN IN THE INPUT NAMELIST $NEWRUN.
  !
  !     THE SAME HOLDS FOR THE "CYLINDRICAL" ALFVEN FREQUENCIES.
  !
  !
  !                  4. MESHES
  !                     ------
  !
  !
  !     THE MESH IS SPECIFIED IN THE EQUILIBRIUM CODE CHEASE.
  !  PLEASE CONSULT THE CHEASE USER MANUAL FOR THE SPECIFICATION OF THE
  !  MESH SIZE (NPSI, NCHI) AND MESH PACKING. NOTE THAT THE NUMBER OF 
  !  POLOIDAL MESH POINTS IS 'NCHI' IN THE CHEASE CODE, AND 'NPOL' IN 
  !  THE LION CODE.
  !
  !
  !                  5. NUMERICS
  !                     --------
  !
  !     THERE IS THE POSSIBILITY TO EXTRACT THE RAPID POLOIDAL PHASE
  !  VARIATION WHICH IS USUAL FOR TAE MODES (AND KINKS) BY SETTING
  !
  !     'NLPHAS'  TO TRUE
  !
  !     The poloidal phase transformation (change of variables) is defined
  !  as:
  !
  !     E(s,chi) = E~(s,chi) * exp (-i*n*q*chi_sfl),       if chi<chi_cut;
  !                E~(s,chi) * exp (-i*n*q*(chi_sfl-2pi))  if chi>chi_cut.
  !
  !  In the above, chi_sfl is the chi angle defined with the "Princeton 
  !  Jacobian" (straight field line). It needs not to coincide with chi.
  !  The angle of cut, chi_cut, is near chi=pi, more precisely at the 
  !  index j_chi = NPOL/2 + 1 (=NCHI in LION).
  !     
  !
  !
  !*********************************************************************
  !***                     EXAMPLE OF INPUT                          ***
  !*********************************************************************
  !
  !     RUN THE EQUILIBRIUM CODE CHEASE
  !     -------------------------------
  !
  !     cat >datain <<'EOF'
  !     *** THESE 4 LINES CAN INCLUDE
  !     *** ANY COMMENT.
  !     *** HOWEVER, THEY MUST BE PRESENT
  !     ***
  !      $EQDATA
  !        NMESHA=1, NPOIDA=5, SOLPDA=0.2, APLACE=1.,.996,.939,.895,.62,
  !                                        AWIDTH(1)=.4,.01,.01,.007,.1,
  !        ASPCT=.3125,CQ0=1.10,NSOUR=4,
  !        ELLIPT=1.00,GAMMA=1.6666666667,
  !        NCHI=50,NEGP=0,NER=2,NIDEAL=2,
  !        NOPT=0,NPLOT=0,NPSI=100,
  !        NS= 40,NSURF=2,NT= 40,NDIFT=1,NTEST=1,
  !        NBAL=1,NDIFPS=0,NRSCAL=1,NTMF0=1,NSYM=1,MSMAX=1,
  !       NCSCAL=1, QSPEC=1.1, CSSPEC=0.,
  !       NPP=0, AP(1)=0.,0.,0.,0.,
  !       NSTTP=0, AT(1)=1.,0.,1.,0.3,
  !       NPROFZ=0,
  !      $END
  !      $NEWRUN
  !       NITMAX=15, NV= 0, REXT=0.00000, WNTORE=3.0, AL0=-3.30E-03,
  !       NLEINQ=.F., NLGREN=.F.,NVIT=1,
  !       ITEST=1, NFIG=2, ANGLE=0,90, ARROW=0.025,
  !       NWALL = 0,EPSCON=1.0E-04,
  !       WALL=0.3,0.3,0.3,0.3,0.20,0.20,0.20,0.20,0.0,0.0,
  !       NLDIAG=16*.F., NLDIAG=4*.T.,
  !      $END OF DATA FOR ERATO VERSION 4.1
  !     EOF
  !     $HOME/CHEASE/cheasev <datain
  !     cp MEQ NSAVE NDES NVAC $HOME/
  !
  !
  !     RUN THE LION CODE
  !     -----------------
  !
  !     cat >lionin <<'EOF'
  !     *** THESE 4 LINES CAN INCLUDE
  !     *** ANY COMMENT.
  !     *** HOWEVER, THEY MUST BE PRESENT.
  !     ***
  !      $NEWRUN
  !      NRSPEC=1, CENDEN(1)=3.0E+19, RMAJOR=2.4, BNOT=1.0,
  !      EQDENS=0.5, EQKAPD=0.05, NDENS=1,
  !      NANTYP=1, ANTRAD=1.2, WALRAD=1.5, WNTORO=1., MANCMP=2,
  !      MPOLWN(1)=-1,-2, CURSYM(1)=1.,1.,1., CURASY(1)=1.,1.,1.,
  !      FREQCY=74.00E3,
  !      ANU=0.01,
  !      NRUN=3, DELTAF=1000.,
  !      CENTE=2000., EQTE=0.95,
  !      NLOTP1(1)=.F.,.F.,
  !      NLOTP2(1)=.F.,.F.,.F.,
  !      NLOTP4(1)=.F.,.F.,.F.,
  !      NLOTP5(1)=.F.,.T.,.F.,.F.,
  !      NLOTP5(18)=.F.,.F.,
  !      NLOTP5(20)=.T.,.T.,
  !      NLOTP5(25)=.F.,
  !      $END
  !     EOF
  !     cp $HOME/MEQ TAPE4
  !     cp $HOME/NSAVE TAPE8
  !     cp $HOME/NDES TAPE16
  !     cp $HOME/NVAC TAPE17
  !     $HOME/lion/l91ex < lionin
  !     cd $HOME
  !     /u2/crpp/bin/s2 tftr.527 300 8
  !
  !     (THE LAST COMMAND IS CHAINING TO ANOTHER JOB)
  !
  !
  !*********************************************************************
  !***                     INDEX OF SUBPROGRAMS                      ***
  !*********************************************************************
  !
  !
  ! VERSION 12     LDV         MAY       1991      CRPP LAUSANNE
  !
  !*********************************************************************
  !***                  1. INITIALIZE CALCULATION                    ***
  !*********************************************************************
  !
  !  MASTER          ORGANIZE CALCULATION                         1.0.02
  !
  !  LABRUN          LABEL THE RUN                                1.1.01
  !  CLEAR           CLEAR COMMON VARIABLES AND ARRAYS            1.1.02
  !  PRESET          SET DEFAULT VALUES                           1.1.03
  !  DATA            DEFINE DATA SPECIFIC TO RUN                  1.1.04
  !  AUXVAL          SET AUXILIARY VALUES                         1.1.05
  !
  !  IODSK1(1)       DISK I/O OPERATIONS FOR LION1                1.3.02
  !
  !
  !*********************************************************************
  !***     2. VACUUM CONTRIBUTION TO WEAK FORM. GREEN'S FUNCTION     ***
  !*********************************************************************
  !
  !  VACUUM          ORGANIZE VACUUM CALCULATION                  2.2.01
  !  TETMSH          THETA MESH IN VACUUM                         2.2.02
  !  ABCDEF          MATRICES A , B , C , D , E , F               2.2.03
  !  ROTETA(3)       RO , TETA AT INTEGRATION POINTS              2.2.04
  !  WALL(5)         POSITION OF THE WALL                         2.2.05
  !  CONCEL(3)       CONTRIBUTION PER CELL                        2.2.06
  !  QCON            MATRIX Q                                     2.2.07
  !  ZERO(1)         REDUCES FOR N=0                              2.2.08
  !  WCON            VACUUM MATRIX                                2.2.09
  !  HERMIC          HERMICITY CONDITION                          2.2.10
  !  MULT(4)         MULTIPLIES TWO MATRICES                      2.2.11
  !  EKN(4)          ELLIPTIC INTEGRALS                           2.2.12
  !  EK(3)           COMPLETE ELLIPTIC INTEGRALS                  2.2.13
  !  IMGC(4)         MATRIX INVERSION                             2.2.14
  !  CURRENT         ANTENNA CURRENT.JUMP OF POTENTIAL           2.2.15
  !  MATVEC          REAL MATRIX * COMPLEX VECTOR                2.2.16
  !  MOPPOW          MATRIX OPERATIONS FOR POWER AT ANTENNA      2.2.18
  !  EKN             ELLIPTICAL INTEGRAL KN AND DKN/DPETA        2.2.19
  !  EKNSIE          ELLIPTICAL INTEGRAL WITH SERIES             2.2.20
  !  EKNLIM          ELLIPTICAL INTEGRAL WITH BESSEL             2.2.21
  !  BESMDI          MODIFIED BESSEL FUNCTION I                  2.2.22
  !  BESMDK          MODIFIED BESSEL FUNCTION K                  2.2.23
  !  NUM             UP/DOWN/UP/DOWN NUMBERING                   2.2.24
  !
  !  IODSK2          DISK I/O OPERATIONS FOR LION2               2.3.01
  !
  !
  !********************************************************************
  !***   3. MATRIX CONSTRUCTION AND RESOLUTION WITH FRONTAL METHOD  ***
  !********************************************************************
  !
  !  ORGAN4          MAIN SUBROUTINE                             4.1.01
  !
  !  FRONTB          ORGANIZE FRONTAL METHOD                     4.2.01
  !  INTEGR          CONTRIBUTION OF ONE CELL TO MATRIX A        4.2.02
  !  RHSHYB	   CONTRIBUTION OF ONE CELL TO RIGHT-HAND SIDE 4.2.02B
  !  ADDVAC          ADD VACUUM CONTRIBUTION                     4.2.03
  !  CALD            DECOMPOSE A BLOCK OF A INTO LDU             4.2.04
  !  CDLHXV          SOLVE UPPER TRIANGULAR SYSTEM               4.2.05
  !  GETRG           GET A SECTION OF VECTOR XT                  4.2.06
  !  PUTRG           PUT A VECTOR INTO XT                        4.2.07
  !  POWER           COMPUTE TOTAL POWER                         4.2.08
  !
  !  DEC             GLOBAL NUMBERING OF UNKNOWNS IN A CELL      4.3.01
  !  AHYBRD          CONSTRUCTS LOCAL 6*6 MATRIX (HYBRID ELEM.)  4.3.03
  !  AWAY            REMOVE A COLUMN AND A ROW OF LOCAL MATRIX   4.3.04
  !  STORE           ADD LOCAL CONTRIB. TO MATRIX BLOCK          4.3.05
  !
  !  QUAEQU          PHYSICAL LOCAL QUANTITIES                   4.4.01
  !  BASIS2          BASIS FUNCTIONS OF LINEAR HYBRID ELEMENTS   4.4.02
  !  CONST1          COEFFICIENTS C-J OF WEAK FORM TERMS         4.4.03
  !  CONST2          COEFFs. OF UNKNOWNS OF W.F. TERMS           4.4.04
  !  CONST3          COEFFs. OF UNKNOWNS OF W.F. TERMS (NLPHAS)  4.4.04
  !  VECT            MULTIPLY W.F.TERMS WITH BASIS FUNCTIONS     4.4.05
  !  DIADIC          CONTRIBUTION OF ONE W.F.TERM TO MATRIX      4.4.06
  !  ADD             ADD CONTRIB. OF ONE TERM TO LOCAL MATRIX    4.4.07
  !
  !  FRPROF          PROFILES OF ION DENSITIES                   4.5.01
  !  SHAPE           SHAPE OF PROFILE                            4.5.02
  !  DAMPIN          WAVE PHENOMENOLOGICAL DAMPING               4.5.03
  !  TEMPI           PARALLEL TEMPERATURE OF ION SPECIES         4.5.04
  !  TEMPRP          PERP. TEMPERATURE OF ION SPECIES            4.5.05
  !  TEMPEL          ELECTRON TEMPERATURE                        4.5.06
  !  FASTDR          K-PERP OF FAST WAVE FROM DISPERSION REL.    4.5.07
  !  DISPFN          FRIED-CONTE DISPESION FUNCTION              4.5.08
  !  ERRFC           ERROR FUNCTION                              4.5.09
  !  BESSEL          BESSEL ROUTINE FOR ERROR                    4.5.10
  !  BESHHH          BESSEL ROUTINE BY BACKWARD RECURSION        4.5.11
  !  CVZERO          ZEROES A COMPLEX VECTOR                     4.5.12
  !
  !  CAXPY           Y = Y + A * X  (COMPLEX)     ($SCILIB)        VS01
  !  CCOPY           COPIES X ONTO Y              ($SCILIB)        VS02
  !  CDOTU           SCALAR PRODUCT X*Y           ($SCILIB)        VS03
  !  CSCAL           SCALES A VECTOR BY A CONSTANT($SCILIB)        VS04
  !  CDOTC           SCALAR PRODUCT CONJG(X)*Y    ($SCILIB)        VS05
  !
  !  IODSK3          DISK I/O FOR LION3                          4.6.01
  !  IODSK4          DISK I/O FOR LION4                          4.6.02
  !
  !
  !
  !********************************************************************
  !***      4. DIAGNOSTICS, LINE-PRINTER AND GRAPHICAL OUTPUT       ***
  !********************************************************************
  !
  !  DIAGNO          ORGANIZE THE DIAGNOSTICS                    5.1.01
  !
  !  INIT            INITIALIZE LION5                            5.2.01
  !  PHYSQ           POWER, ENERGY AND FIELDS DIAGNOSTICS        5.2.02
  !  FOURIE          COMPLEX FOURIER ANALYSIS IN CHI             5.2.04
  !  LANDAU          ELECTRON LANDAU DAMPING RATE OF ALFVEN WAVE 5.2.06
  !  THEEND          TERMINATE THE CALCULATION                   5.2.99
  !
  !  CENTER          VALUE OF UNKNOWNS AT CENTER OF CELL         5.3.01
  !  ELECTR          ELECTRIC FIELD,NORMAL AND PERP              5.3.02
  !  LOCPOW          LOCAL POWER ABSORPTION                      5.3.03
  !  MAGNET          WAVE MAGNETIC FIELD                         5.3.04
  !  POYNTI          POYNTING VECTOR                             5.3.05
  !  SFLINT          CONTRIBUTION TO POYNTING FLUX               5.3.06
  !
  !  OUTP5           LINE-PRINTER OUTPUT                         5.4.01
  !  TABLE2          2-D PRINTOUT                                5.4.02
  !  TABLEF          PRINTOUT OF POLOIDAL FOURIER COMPONENTS     5.4.03
  !  PLOT5           GRAPHICAL OUTPUT                            5.4.04
  !  ARROW           2-D VECTOR FIELD PLOT                       5.4.05
  !  SCAPLO          2-D SCALAR FIELD PLOT                       5.4.06
  !  SURFPL          PLOT THE PLASMA SURFACE                     5.4.07
  !  LABPLO          PLOT THE LABELS                             5.4.08
  !  LEVEL           2-D SCALAR PLOT : CONTOUR LINES             5.4.10
  !  LZERO           PLOT THE ZEROS OF VOUT1                     5.4.11
  !  CONTOR          PLOT THE LINE VOUT1=0.0                     5.4.13
  !  ACROSS          LINEAR INTERPOLATION                        5.4.14
  !
  !  IODSK5          DISK I/O OPERATIONS FOR LION5               5.5.01
  !
  !
  !*********************************************************************
  !***                    INDEX OF COMMON BLOCKS                     ***
  !*********************************************************************
  !
  ! VERSION 11     LDV      MAY 1991           CRPP LAUSANNE
  !
  !L                  1.       GENERAL OLYMPUS DATA
  !  COMBAS          BASIC SYSTEM PARAMETERS                        C1.1
  !
  !L                  2.       PHYSICAL PROBLEM
  !  COMPHY          GENERAL PHYSICS VARIABLES                      C2.1
  !  COMEQU          EQUILIBRIUM QUANTITIES                         C2.2
  !
  !L                  3.       NUMERICAL SCHEME
  !  COMESH          (R,Z) AND (PSI,CHI) MESH VARIABLES             C3.1
  !  COMNUM          NUMERICAL VARIABLES                            C3.2
  !  COMAUX          AUXILIARY VARIABLES FOR LION3 AND 4            C3.3
  !  COMVEC          VECTORS FOR LION4                              C3.4
  !  COMIVI          NUMERICAL VARIABLES FOR LION4                  C3.5
  !
  !L                  5.       I/O AND DIAGNOSTICS
  !  COMCON          CONTROL VARIABLES                              C5.1
  !  COMOUT          I/O DISK CHANNELS NUMBERS                      C5.2
  !
  !L                  9.       BLANK COMMON
  !  COMVID          VACUUM QUANTITIES FOR LION2                    C9.2
  !  COMMTR          MATRIX BLOCKS FOR LION3 AND LION4              C9.3
  !  COMPLO          OUTPUT AND PLOT QUANTITIES FOR LION5           C9.5
  !
  !
  !*********************************************************************
  !***            ALPHABETIC INDEX OF ALL COMMON VARIABLES           ***
  !*********************************************************************
  !
  ! VERSION 12     LDV      MAY 1991         CRPP LAUSANNE
  !
  !  A1D(MDLENG)       MATRIX BLOCK OF DISCRETIZED WEAK FORM        CA 9.3
  !  ABSPOW(MDPSI)   POWER ABSORBED ON PSI=CONST.                 RA 9.5
  !  ACHARG(MDSPEC)  *ATOMIC CHARGES OF ION SPECIES               RA 2.1
  !  AD(10)	   *COEFF. FOR POLYNOMIAL DENSITY PROFILE	RA 2.1
  !  AHEIGT          *HEIGHT OF 2-D PLOTS                         R  5.1
  !  ALARG           *WIDTH OF 2-D PLOTS                          R  5.1
  !  AMASS(MDSPEC)   *ATOMIC MASSES OF ION SPECIES                RA 2.1
  !  AMASSE          *ATOMIC MASS OF ELECTRON                     R  2.1
  !  ANGLET(16)      *TOROIDAL CUTS (DEGREES)                     RA 5.1
  !  ANTR(MDPOL)     ANTENNA VECTOR                               CA 9.2
  !  ANTRAD          *ANTRAD-1.=DISTANCE ANTENNA-PLASMA           R  2.1
  !  ANTUP           *UPPER RIGHT POSITION OF TOP/BOTTOM ANTENNA  R  2.1
  !  ANU             *COLLISIONAL DAMPING NU/OMEGA                R  2.1
  !  APHI            TOROIDAL ANGLE PHI                           R  9.5
  !  ARSIZE          *SIZE OF ARROWS                              R  5.1
  !  ASPCT           *INVERSE ASPECT RATIO FOR SOLOVEV EQUILIBRIU R  2.1
  !  ASYMB           *SIZE OF SYMBOLS                             R  5.1
  !  BETA            BETA VALUE                                   R  2.1
  !  BETAP           BETA POLOIDAL                                R  2.1
  !  BETAS           BETA STAR PRINCETON                          R  2.1
  !  BNL             NORMAL COMPONENT OF WAVE MAGNETIC FIELD      C  9.5
  !  BNOT            *MAGNETIC FIELD AT MAGNETIC AXIS (TESLA)     R  2.1
  !  BPARL           PARALLEL COMP. OF WAVE MAGNETIC FIELD        C  9.5
  !  BPL             PERP. COMP. OF WAVE MAGNETIC FIELD           C  9.5
  !  CA              CONSTANT TO DEFINE WALL                      R  9.2
  !  CA(MDOVL)       OVERLAP SUBBLOCK OF MATRIX A                 CA 3.3
  !  CB              #                                            R  9.2
  !  CC              #                                            R  9.2
  !  CCHI(MDPOL)     CHI VALUES AT CENTER OF CELLS                RA 9.5
  !  CCR(MDPSI,MDPOL)
  !                  R AT CENTER OF CELLS                         RA 9.5
  !  CCS(MDPSI)      S VALUES AT CENTER OF CELLS                  RA 9.5
  !  CCZ(MDPSI,MDPOL)
  !                  Z AT CENTER OF CELLS                         RA 9.5
  !  CDEVIA          WIDTH OF POWER DISTRIBUTION IN CHI           R  9.5
  !  CELPOW(MDPSI,MDPOL)
  !                  POWER ARBSORBED IN THE CELLS                 RA 9.5
  !  CEN0(MDBCAS)	   *DENSITIES FOR CONST BETA SCAN OF DKE STAB.  RA 2.1
  !  CENDEN(MDSPEC)  *DENSITIES OF ION SPECIES AT MAGN.AXIS (M-3) RA 2.1
  !  CENTE           *ELECTRON TEMPERATURE AT MAGNETIC AXIS       R  2.1
  !  CENTI(MDSPEC)   *ION TEMPERATURES AT MAGN.AXIS (EV)          RA 2.1
  !  CENTIP(MDSPEC)  *PERP.ION TEMPERATURES AT MAGN. AXIS (EV)    RA 2.1
  !  CEOMCI(MDSPEC)  *NORMALIZED ION CYCLOTRON FREQUENCIES        RA 2.1
  !  CHI(MDCHI1)     CHI MESH VALUES                              RA 3.1
  !  CHIPOW(MDPOL)   POWER ABSORBED ON CHI=CONST.                 RA 9.5
  !  CMEAN           CENTER OF POWER DISTRIBUTION IN CHI          R  9.5
  !  CNR(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., R COMPONENT            RA 9.5
  !  CNZ(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., Z COMPONENT            RA 9.5
  !  CONA(6,6)       LOCAL (CELL) CONTRIBUTION TO A               CA 3.3
  !  COST(MDPSI,MDPOL)COSINUS OF ANGLE BETWEEN PHI AND B_0        RA 9.5
  !  CPL             POWER ABSORBED IN A CELL                     R  9.5
  !  CPLE            POWER IN A CELL DUE TO NU                    R  9.5
  !  CPLJ(MDSPC2)    POWER IN A CELL PER SPECIES                  RA 9.5
  !  CPSRF           *PSI AT PLASMA SURFACE                       R  2.1
  !  CURASY(5)  *AMPLITUDE OF SIN ANTENNA CURRENT (HELICAL)  RA 2.1
  !  CURSYM(5)  *AMPLITUDE OF ANTENNA CURRENT                RA 2.1
  !  D2ROP           D**2RHO/DTHETA**2 AT MID-POINT               R  9.2
  !  DC(MD2CP2)      DELTA-CHI                                    RA 9.2
  !  DELTA           *PHENOMENOLOGICAL DAMPING                    R  2.1
  !  DELTAF          *FREQUENCY INCREMENT FOR FREQUENCY TRACE     R  2.1
  !  DELTH(MD2CP2)   DELTA-THETA (TH(K+1)-TH(K))                  RA 9.2
  !  DENPOW(0:MDPSI,MDPOL1)
  !                  POWER ABSORPTION DENSITY                     RA 9.5
  !  DEPOS(MDPSI)    POWER DENS. AVERAGED ON PSI=CONST.           RA 9.5
  !  DPL             POWER ABSORPTION DENSITY AT CENTER OF CELL   R  9.5
  !  DPLE            POWER ABSORPTION DENSITY DUE TO NU           R  9.5
  !  DPLJ(MDSPC2)    POWER DENSITY PER SPECIES                    RA 9.5
  !  DRODT(4)        DRHO/DTHETA AT INTEGRATION POINTS            RA 9.2
  !  DROPDT(4)       DRHOPRIME/DTHETA                             RA 9.2
  !  DVDC            DV/DCHI                                      C  9.5
  !  DXDC            DX/DCHI                                      C  9.5
  !  DXDS            DX/DS                                        C  9.5
  !  ECHEL           SCALE FACTOR FOR 2-D PLOTS                   R  9.5
  !  ELEPOW          POWER DUE TO NU                              R  9.5
  !  ELLIPT          *ELLIPTICITY SQUARED FOR SOLOVEV EQUILIBRIUM R  2.1
  !  ENL             NORMAL COMPONENT OF WAVE ELECTRIC FIELD      C  9.5
  !  EPL             PERP. COMP. OF WAVE ELECTRIC FIELD           C  9.5
  !  EPSMAC          *ROUND-OFF ERROR OF COMPUTER                 R  3.5
  !  EQ(MDEQ,MDPOL,MDPSI1)  EQUILIBRIUM QUANTITIES                RA 2.2
  !  EQALFD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQDENS          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQFAST	   *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQKAPF(MDFAKA)  *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPT(MDSPEC)  *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  EQKPTE          *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTE            *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTI(MDSPEC)    *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  FEEDUP          *POSITION OF UPPER RIGHT FEED OF T/B ANTENNA R  2.1
  !  FLUPOW(MDPSI1)  POWER ABSORBED INSIDE PSI=CONST.             RA 9.5
  !  FLUPSP(MDPSI1,MDSPC2)
  !                  POWER ABSORBED INSIDE PSI=CONST.,PER SPECIES RA 9.5
  !  FRAC(MDSPEC)    *MASS FRACTION OF ION SPECIES                RA 2.1
  !  FRCEN(MDSPEC)   *CENTER OF ION DENSITY PROFILE               RA 2.1
  !  FRDEL(MDSPEC)   *WIDTH OF ION DENSITY PROFILE                RA 2.1
  !  FREN(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-N          CA 9.5
  !  FREP(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-PERP       CA 9.5
  !  FREQCY          *FREQUENCY OF GENERATOR (HZ)                 R  2.1
  !  FRINT(7,MDCHI)  INTEGRAL FOR FOURIER DECOMPOSITION           CA 9.5
  !  G(MDPOL,MDPOL,13)
  !                  ALL MATRICES                                 RA 9.2
  !  GAMMA           ADIABATICITY INDEX (NOT IN USE)              R  2.1
  !  LENGTH          *NB.OF ELEMENTS OF A MATRIX BLOCK            I  3.2
  !  M1              RANK OF MATRIX OVERLAP SUBBLOCK              I  3.5
  !  M11             M1+1                                         I  3.5
  !  M12             M1+M2 = RANK OF MATRIX BLOCK                 I  3.5
  !  M2              RANK OF MATRIX BLOCK - M1                    I  3.5
  !  MANCMP          *NB.OF POLOIDAL WAVE NUMBERS (HELICAL ANT.)  I  2.1
  !  MEQ             *EQUILIBIUM QUANTITIES I/O CHANNEL           I  5.2
  !  MFL             *LOWER M VALUE FOR FOURIER ANALYSIS          I  5.1
  !  MFU             UPPER M VALUE FOR FOURIER ANALYSIS           I  5.1
  !  MPOLWN(5)       *POLOIDAL WAVE NUMBERS (HELICAL ANT.)        IA 2.1
  !  N               NB. OF MATRIX BLOCKS (=NPSI)                 I  3.5
  !  NANTYP          *SELECTS TYPE OF ANTENNA   			I  2.1
  !  NBCASE	   *NB. OF CASES FOR CONST BETA SCAN DKE STAB.  I  2.1
  !  NBTYPE	   *TYPE OF CONST BETA SCAN DKE STAB.		I  2.1
  !  NCHI            *NUMBER OF CHI INTERVALS IN UPPER HALF-PLANE I  3.1
  !  NCOLMN          *RANK OF A MATRIX BLOCK                      I  3.2
  !  NCOMP           NB. OF ELEMENTS OF SOLUTION VECTOR           I  3.5
  !  NCONTR          *NUMBER OF CONTOUR LINES                     I  5.1
  !  NDA             *MATRIX A I/O CHANNEL                        I  5.2
  !  NDARG	   *ARGUMENT FOR POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDDEG	   *DEGREE OF POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDENS           *SELECTS TYPE OF DENSITY PROFILE             R  2.1
  !  NDES            *R,Z COORDINATES AND NORMALS I/O CHANNEL     I  5.2
  !  NDIM            DIMENSION OF MATRICES                        I  9.2
  !  NDLT            *DECOMPOSED MATRIX L,D,U I/O CHANNEL         I  5.2
  !  NDS             *SOLUTION VECTOR I/O CHANNEL                 I  5.2
  !  NCUT            *NUMBER OF TOROIDAL CUTS FOR PLOTS           I  5.1
  !  NFAKAP	   *NB. OF FAST PARTICLE PROFILES FOR DKE STAB. I  2.1
  !  NHARM           *NUMBER OF ION CYCLOTRON HARMONICS           I  2.1
  !  NLCOLD          *SELECTS COLD/WARM IONS PLASMA MODEL         L  2.1
  !  NLCOLE	   *SELECTS COLD/WARM ELECTRONS (FAST W. DAMP.) L  2.1
  !  NLDIP           *SELECTS MONO/DIPOLE ANTENNA FOR LFS OR HFS  L  2.1
  !  NLONG           NB. OF ELEMENTS IN A MATRIX BLOCK            I  3.5
  !  NLDISO	   *SWITCH COMPUTATION DIAGNOSTICS OF SOLUTION  L  5.1
  !  NLPHAS          *SWITCH POLOIDAL PHASE EXTRACTION            L  5.1
  !  NLOTP0          *GENERAL LINE-PRINTER OUTPUT SWITCH          L  5.1
  !  NLOTP1(4)       *OUTPUT SWITCHES FOR LION1                   LA 5.1
  !  NLOTP2(5)       *OUTPUT SWITCHES FOR LION2                   LA 5.1
  !  NLOTP3(2)       *OUTPUT SWITCHES FOR LION3                   LA 5.1
  !  NLOTP4(5)       *OUTPUT SWITCHES FOR LION4                   LA 5.1
  !  NLOTP5(40)      *OUTPUT SWITCHES FOR LION5                   LA 5.1
  !  NLPLO5(25)      *PLOT SWITCHES FOR LION5                     LA 5.1
  !  NLQUAD          QUADRATIC TERM IN THE WEAK FORM              L  3.3
  !  NPLAC(6)        GLOBAL NUMBERING OF CELL MESH POINTS         IA 3.3
  !  NPLTYP	   *=1 FOR BASPL PLOTS, =2 FOR EXPLORER PLOTS   I  5.1
  !  NPOL            *TOTAL NUMBER OF CHI INTERVALS               I  3.1
  !  NPRNT           *LINE-PRINTER OUTPUT                         I  5.2
  !  NPSI            *NUMBER OF S INTERVALS                       I  3.1
  !  NRSPEC          *NUMBER OF ION SPECIES                       I  2.1
  !  NRUN            *NUMBER OF RUNS FOR FREQUENCY SWEEP          I  5.1
  !  NRZSUR          NUMBER OF POINTS FOR PLOTTING THE SURFACE    I  9.5
  !  NSADDL	   *TYPE OF POLOIDAL PHASING FOR SADDLE COILS   I  2.1
  !  NSAVE           *NAMELIST I/O CHANNEL                        I  5.2
  !  NSHIFT          PERFORM SHIFT IN NQ                          I  3.1
  !  NSING           SINGULARITY INDICATOR (=-1 IF A IS SING.)    I  3.5
  !  NTEMP           *SELECTS TYPE OF TEMPERATURE PROFILES        I  2.1
  !  NTORSP	   *NUMBER OF TOR. WN'S FOR THE TOR.WN SCAN	I  5.1
  !  NUMBER          *RUN NUMBER                                  I  3.2
  !  NVAC            *VACUUM QUANTITIES I/O CHANNEL               I  5.2
  !  OHMR(MDPOL)     OHM VECTOR                                   CA 9.2
  !  OMEGA           *NORMALIZED FREQUENCY (*RMAJOR/ALFV.SPEED)   R  2.1
  !  QB              SAFETY FACTOR AT PLASMA SURFACE              R  9.2
  !  QIAXE           *1./Q(AXIS) FOR SOLOVEV EQUILIBRIUM          R  2.1
  !  QS(MDPSI1)      Q AT EDGES OF CELLS                          RA 2.1
  !  QTILDA(MDPSI1)  Q AT CENTERS OF CELLS                        RA 2.1
  !  REASCR          REACTANCE SCALAR                             C  9.2
  !  RMAJOR          *MAJOR RADIUS (M)                            R  2.1
  !  RO(4)           RHO AT INTEGRATION POINTS                    RA 9.2
  !  RO2             RHO AT THE CENTER OF CHI INTERVAL            R  9.2
  !  RO2P            RHO PRIME AT THE CENTER OF CHI INTERVAL      R  9.2
  !  ROEDGE(MD2CP2)  RHO PLASMA SURF. AT EDGES OF CHI INTERVALS   RA 9.2
  !  ROMID(MD2CP2)   RHO PLASMA SURF. AT MIDDLE CHI INTERVALS     RA 9.2
  !  ROP(4)          RHO PRIME AT INTEGRATION POINTS              RA 9.2
  !  SAMIN	   *INSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAMAX	   *OUTSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAUTR(MDPOL)    JUMP ACROSS ANTENNA SURFACE                  CA 9.2
  !  SAUTX(MDPOL)    TEMPORARY                                    CA 9.2
  !  SDEVIA          WIDTH OF POWER DISTRIBUTION IN S             R  9.5
  !  SFLUX(MDPSI)    POYNTING FLUX ACROSS PSI=CONST.              RA 9.5
  !  SIGMA           *NORM FACTOR FOR V-THEMAL (IONS)             R  2.1
  !  SINT(MDPSI,MDPOL)SINUS OF ANGLE BETWEEN PHI AND B_0          RA 9.5
  !  SMEAN           MINOR RADIUS OF HALF POWER ABSORPTION        R  9.5
  !  SN(MDPSI,MDPOL) NORMAL COMPONENT OF POYNTING                 RA 9.5
  !  SNL             NORMAL COMPONENT OF POYNTING                 R  9.5
  !  SOURCE(MDPOL)   SOURCE VECTOR ON PLASMA BOUNDARY             CA 9.2
  !  SOURCT(MDCOMP)  TOTAL VECTOR OF RIGHT-HAND SIDE		CA 3.4
  !  SPAR(MDPSI,MDPOL)
  !                  PARALLEL COMPONENT OF POYNTING               RA 9.5
  !  SPARL           PARALLEL COMPONENT OF POYNTING               R  9.5
  !  SPERP(MDPSI,MDPOL)
  !                  PERP. COMPONENT OF POYNTING                  RA 9.5
  !  SPL             PERP. COMP. OF POYNTING                      R  9.5
  !  SR(MD2CP2)      R(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  SURPSI(MDPSI1)  INTEGRAL FOR PSI-SURFACE                     RA 9.5
  !  SZ(MD2CP2)      Z(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  T(MD2CP2)       THETA AT MIDDLE CHI INTERVALS                RA 9.2
  !  TB              TOROIDAL MAGNETIC FLUX AT PLASMA SURFACE     R  9.2
  !  TH(MD2CP2)      THETA AT EDGES OF CHI INTERVALS              RA 9.2
  !  THANT(4)	   *THETA OF SADDLE COILS TOROIDAL SECTIONS	RA 2.1
  !  TP(4)           THETA PRIME AT INTEGRATION POINTS            RA 9.2
  !  TT(4)           THETA AT INTEGRATION POINTS                  RA 9.2
  !  U(MDCOL)        VECTOR OF UNKNOWS FOR ONE BLOCK              CA 3.4
  !  UT              TEMPORARY STORAGE                            C  3.4
  !  VA(MDCOL)       TEMPORARY STORAGE                            CA 3.4
  !  VC              V                                            C  9.5
  !  VOUT1(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  VOUT2(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  WALRAD          *1.-WALRAD = DISTANCE WALL-PLASMA            R  2.1
  !  WBETCH          NON-ORTHOGONALITY OF PSI,CHI (BETA-CHI)      R  2.2
  !  WBPOL2          POLOIDAL FIELD SQUARED                       R  2.2
  !  WBTOR2          TOROIDAL FIELD SQUARED                       R  2.2
  !  WBTOT           TOTAL FIELD                                  R  2.2
  !  WBTOT2          TOTAL FIELD SQUARED                          R  2.2
  !  WCHI            CHI AT CENTER OF THE CELL                    R  2.2
  !  WCOMEG          COMPLEX NORMALIZED FREQUENCY                 C  2.2
  !  WDCHI           CHI WIDTH OF CELL                            R  2.2
  !  WDCR2J          D/DCHI ( LOG(R**2/J) )                       R  2.2
  !  WDS             S WIDTH OF CELL                              R  2.2
  !  WEPS            DIELECTRIC TENSOR COMPONENT EPSILON N-N      C  2.2
  !  WEPSEL          ELECTRON CONTRIBUTION TO DIELECTRIC TENSOR   C  2.2
  !  WEPSI(MDSPEC)   ION CONTRIBUTIONS TO WEPS                    CA 2.2
  !  WEPSMC          PHENOMENOLOGICAL DAMPING CONTRIB. TO DIEL.   C  2.2
  !  WFRAC(MDSPEC)   MASS FRACTION OF ION SPECIES                 RA 2.2
  !  WG              DIELECTRIC TENSOR COMPONENT EPSILON N-PERP   C  2.2
  !  WGI(MDSPEC)     ION CONTRIBUTIONS TO WG                      CA 2.2
  !  WGRPS2          GRADIENT OF PSI SQUARED                      R  2.2
  !  WH              COEFFICIENT H                                R  2.2
  !  WJAC            JACOBIEN OF PSI,CHI COORDINATE SYSTEM        R  2.2
  !  WK              COEFFICIENT K                                R  2.2
  !  WNTDEL	   *TOR.WN. INCREMENT FOR TOR.WN. SCAN		R  5.1
  !  WNTORO          *TOROIDAL WAVE NUMBER N                      R  2.1
  !  WNU             WAVE DAMPING                                 R  2.2
  !  WOMCI(MDSPEC)   NORMALIZED ION-CYCLOTRON FREQUENCY           RA 2.2
  !  WPSI            PSI                                          R  2.2
  !  WQ              SAFETY FACTOR Q                              R  2.2
  !  WR2             R SQUARED                                    R  2.2
  !  WRHO            MASS DENSITY                                 R  2.2
  !  WS              S COORDINATE AT CENTER OF THE CELL           R  2.2
  !  WT              TOROIDAL FLUX FUNCTION T                     R  2.2
  !  WTQ             R**2 / J                                     R  2.2
  !  X(MDCOL)        VECTOR OF UNKNOWNS FOR ONE BLOCK             CA 3.4
  !  XC              X                                            C  9.5
  !  XC1(8)          CONSTANTS C-J OF WEAK FORM TERMS             CA 3.3
  !  XDCHI(MDPOL)    DELTA-CHI AT PLASMA SURFACE                  RA 3.4
  !  XDTH(MDPOL)     DELTA-THETA AT PLASMA SURFACE                RA 3.4
  !  XETA(5)         CONSTANTS OF TEST-FUNCTIONS                  CA 3.3
  !  XF(16)          BASIS FUNCTIONS AT MESH POINTS               RA 3.3
  !  XKSI(5)         CONSTANTS OF UNKNOWNS                        CA 3.3
  !  XM(6,6)         CONTRIBUTION OF ONE TERM OF THE WEAK FORM    CA 3.3
  !  XOHMR(MDPOL)    OHM VECTOR FOR POWER AT ANTENNA              CA 3.4
  !  XS(MDRZ)        R COORDINATES OF PLASMA SURFACE              RA 9.5
  !  XT(MDCOMP)      TOTAL VECTOR OF UNKNOWNS                     CA 3.4
  !  XVETA(6)        VECTOR OF COEFFICIENTS FOR TEST-FUNCTIONS    CA 3.3
  !  XVKSI(6)        VECTOR OF COEFFICIENTS FOR UNKNOWNS          CA 3.3
  !  YS(MDRZ)        Z COORDINATES OF PLASMA SURFACE              RA 9.5
  !
  !
  !*********************************************************************
  !***    ALPHABETIC INDEX OF COMMON VARIABLES COMMON PER COMMON     ***
  !*********************************************************************
  !
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !
  !L                  C2.1     GENERAL PHYSICS VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMPHY/
  !
  !  ACHARG(MDSPEC)  *ATOMIC CHARGES OF ION SPECIES               RA 2.1
  !  AD(10)	   *COEFF. FOR POLYNOMIAL DENSITY PROFILE	RA 2.1
  !  AMASS(MDSPEC)   *ATOMIC MASSES OF ION SPECIES                RA 2.1
  !  AMASSE          *ATOMIC MASS OF ELECTRON                     R  2.1
  !  ANTRAD          *ANTRAD-1.=DISTANCE ANTENNA-PLASMA           R  2.1
  !  ANTUP           *UPPER RIGHT POSITION OF TOP/BOTTOM ANTENNA  R  2.1
  !  ANU             *CAUSAL DAMPING ADDED TO DIELECTRIC TENSOR   R  2.1
  !  ASPCT           *INVERSE ASPECT RATIO FOR SOLOVEV EQUILIBRIU R  2.1
  !  BETA            BETA VALUE                                   R  2.1
  !  BETAP           BETA POLOIDAL                                R  2.1
  !  BETAS           BETA STAR PRINCETON                          R  2.1
  !  BNOT            *MAGNETIC FIELD AT MAGNETIC AXIS (TESLA)     R  2.1
  !  CEN0(MDBCAS)	   *DENSITIES FOR CONST BETA SCAN OF DKE STAB.  RA 2.1
  !  CENDEN(MDSPEC)  *DENSITIES OF ION SPECIES AT MAGN.AXIS (M-3) RA 2.1
  !  CENTE           *ELECTRON TEMPERATURE AT MAGNETIC AXIS       R  2.1
  !  CENTI(MDSPEC)   *ION TEMPERATURES AT MAGN.AXIS (EV)          RA 2.1
  !  CENTIP(MDSPEC)  *PERP.ION TEMPERATURES AT MAGN. AXIS (EV)    RA 2.1
  !  CEOMCI(MDSPEC)  *NORMALIZED ION CYCLOTRON FREQUENCIES        RA 2.1
  !  CPSRF           *PSI AT PLASMA SURFACE                       R  2.1
  !  CURASY(5)  *AMPLITUDE OF SIN ANTENNA CURRENT (HELICAL)  RA 2.1
  !  CURSYM(5)  *AMPLITUDE OF ANTENNA CURRENT                RA 2.1
  !  DELTA           *PHENOMENOLOGICAL DAMPING                    R  2.1
  !  DELTAF          *FREQUENCY INCREMENT FOR FREQUENCY TRACE     R  2.1
  !  ELLIPT          *ELLIPTICITY SQUARED FOR SOLOVEV EQUILIBRIUM R  2.1
  !  EQALFD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQDENS          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQFAST	   *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPD          *PROFILE PARAMETER OF TOTAL MASS DENSITY     R  2.1
  !  EQKAPF(MDFAKA)  *PROFILE PARAMETER OF FAST PARTICLE DENSITY  R  2.1
  !  EQKAPT(MDSPEC)  *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  EQKPTE          *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTE            *PROFILE PARAMETER OF ELECTRON TEMPERATURE   R  2.1
  !  EQTI(MDSPEC)    *PROFILE OF ION TEMPERATURE                  RA 2.1
  !  FEEDUP          *POSITION OF UPPER RIGHT FEED OF T/B ANTENNA R  2.1
  !  FRAC(MDSPEC)    *MASS FRACTION OF ION SPECIES                RA 2.1
  !  FRCEN(MDSPEC)   *CENTER OF ION DENSITY PROFILE               RA 2.1
  !  FRDEL(MDSPEC)   *WIDTH OF ION DENSITY PROFILE                RA 2.1
  !  FREQCY          *FREQUENCY OF GENERATOR (HZ)                 R  2.1
  !  GAMMA           ADIABATICITY INDEX (NOT IN USE)              R  2.1
  !  NBCASE	   *NB. OF CASES FOR CONST BETA SCAN DKE STAB.  I  2.1
  !  NBTYPE	   *TYPE OF CONST BETA SCAN DKE STAB.		I  2.1
  !  NDARG	   *ARGUMENT FOR POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDDEG	   *DEGREE OF POLYNOMIAL DENSITY PROFILE	I  2.1
  !  NDENS           *SELECTS TYPE OF DENSITY PROFILE             R  2.1
  !  NFAKAP	   *NB. OF FAST PARTICLE PROFILES FOR DKE STAB. I  2.1
  !  NHARM           *NUMBER OF ION CYCLOTRON HARMONICS           I  2.1
  !  NSADDL	   *TYPE OF POLOIDAL PHASING OF SADDLE COILS    I  2.1
  !  NTEMP           *SELECTS TYPE OF TEMPERATURE PROFILES        I  2.1
  !  OMEGA           *NORMALIZED FREQUENCY (*RMAJOR/ALFV.SPEED)   R  2.1
  !  QIAXE           *1./Q(AXIS) FOR SOLOVEV EQUILIBRIUM          R  2.1
  !  QS(MDPSI1)      Q AT EDGES OF CELLS                          RA 2.1
  !  QTILDA(MDPSI1)  Q AT CENTERS OF CELLS                        RA 2.1
  !  RMAJOR          *MAJOR RADIUS (M)                            R  2.1
  !  SAMIN	   *INSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SAMAX	   *OUTSIDE EDGE OF ANTENNA INSIDE PLASMA (S)	R  2.1
  !  SIGMA           *NORM FACTOR FOR V-THEMAL (IONS)             R  2.1
  !  THANT(4)	   *THETA OF SADDLE COILS TOROIDAL SECTIONS	RA 2.1
  !  WALRAD          *1.-WALRAD = DISTANCE WALL-PLASMA            R  2.1
  !  WNTORO          *TOROIDAL WAVE NUMBER N                      R  2.1
  !  MANCMP          *NB.OF POLOIDAL WAVE NUMBERS (HELICAL ANT.)  I  2.1
  !  MPOLWN(5)       *POLOIDAL WAVE NUMBERS (HELICAL ANT.)        IA 2.1
  !  NANTYP          *SELECTS TYPE OF ANTENNA   			I  2.1
  !  NRSPEC          *NUMBER OF ION SPECIES                       I  2.1
  !  NLCOLD          *SELECTS COLD OR LUKEWARM PLASMA MODEL       L  2.1
  !  NLCOLE	   *SELECTS COLD/WARM ELECTRONS (FAST W. DAMP.) L  2.1
  !  NLDIP           *SELECTS MONO/DIPOLE ANTENNA FOR LFS OR HFS  L  2.1
  !
  !L                  C2.2     EQUILIBRIUM QUANTITIES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMEQU/
  !
  !  WCOMEG          COMPLEX NORMALIZED FREQUENCY                 C  2.2
  !  WEPS            DIELECTRIC TENSOR COMPONENT EPSILON N-N      C  2.2
  !  WEPSEL          ELECTRON CONTRIBUTION TO DIELECTRIC TENSOR   C  2.2
  !  WEPSI(MDSPEC)   ION CONTRIBUTIONS TO WEPS                    CA 2.2
  !  WEPSMC          PHENOMENOLOGICAL DAMPING CONTRIB. TO DIEL.   C  2.2
  !  WG              DIELECTRIC TENSOR COMPONENT EPSILON N-PERP   C  2.2
  !  WGI(MDSPEC)     ION CONTRIBUTIONS TO WG                      CA 2.2
  !  EQ(MDEQ,MDPOL,MDPSI1)  EQUILIBRIUM QUANTITIES                RA 2.2
  !  WBETCH          NON-ORTHOGONALITY OF PSI,CHI (BETA-CHI)      R  2.2
  !  WBPOL2          POLOIDAL FIELD SQUARED                       R  2.2
  !  WBTOR2          TOROIDAL FIELD SQUARED                       R  2.2
  !  WBTOT           TOTAL FIELD                                  R  2.2
  !  WBTOT2          TOTAL FIELD SQUARED                          R  2.2
  !  WCHI            CHI AT CENTER OF THE CELL                    R  2.2
  !  WDCHI           CHI WIDTH OF CELL                            R  2.2
  !  WDCR2J          D/DCHI ( LOG(R**2/J) )                       R  2.2
  !  WDS             S WIDTH OF CELL                              R  2.2
  !  WFRAC(MDSPEC)   MASS FRACTION OF ION SPECIES                 RA 2.2
  !  WGRPS2          GRADIENT OF PSI SQUARED                      R  2.2
  !  WH              COEFFICIENT H                                R  2.2
  !  WJAC            JACOBIEN OF PSI,CHI COORDINATE SYSTEM        R  2.2
  !  WK              COEFFICIENT K                                R  2.2
  !  WNU             WAVE DAMPING                                 R  2.2
  !  WOMCI(MDSPEC)   NORMALIZED ION-CYCLOTRON FREQUENCY           RA 2.2
  !  WPSI            PSI                                          R  2.2
  !  WQ              SAFETY FACTOR Q                              R  2.2
  !  WR2             R SQUARED                                    R  2.2
  !  WRHO            MASS DENSITY                                 R  2.2
  !  WS              S COORDINATE AT CENTER OF THE CELL           R  2.2
  !  WT              TOROIDAL FLUX FUNCTION T                     R  2.2
  !  WTQ             R**2 / J                                     R  2.2
  !
  !L                  C3.1     (R,Z) AND (PSI,CHI) MESH VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMESH/
  !
  !  CHI(MD2CP2)     CHI MESH VALUES                              RA 3.1
  !  NCHI            *NUMBER OF CHI INTERVALS IN UPPER HALF-PLANE I  3.1
  !  NPOL            *TOTAL NUMBER OF CHI INTERVALS               I  3.1
  !  NPSI            *NUMBER OF S INTERVALS                       I  3.1
  !
  !L                  C3.2     NUMERICAL VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMNUM/
  !
  !  LENGTH          *NB.OF ELEMENTS OF A MATRIX BLOCK            I  3.2
  !  NCOLMN          *RANK OF A MATRIX BLOCK                      I  3.2
  !  NUMBER          *RUN NUMBER                                  I  3.2
  !
  !L                  C3.3     AUXILIARY VARIABLES FOR LION3 AND 4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMAUX/
  !
  !  CA(MDOVL)       OVERLAP SUBBLOCK OF MATRIX A                 CA 3.3
  !  CONA(6,6)       LOCAL (CELL) CONTRIBUTION TO A               CA 3.3
  !  XC1(8)          CONSTANTS C-J OF WEAK FORM TERMS             CA 3.3
  !  XETA(5)         CONSTANTS OF TEST-FUNCTIONS                  CA 3.3
  !  XKSI(5)         CONSTANTS OF UNKNOWNS                        CA 3.3
  !  XM(6,6)         CONTRIBUTION OF ONE TERM OF THE WEAK FORM    CA 3.3
  !  XVETA(6)        VECTOR OF COEFFICIENTS FOR TEST-FUNCTIONS    CA 3.3
  !  XVKSI(6)        VECTOR OF COEFFICIENTS FOR UNKNOWNS          CA 3.3
  !  XF(16)          BASIS FUNCTIONS AT MESH POINTS               RA 3.3
  !  NPLAC(6)        GLOBAL NUMBERING OF CELL MESH POINTS         IA 3.3
  !  NLQUAD          QUADRATIC TERM IN THE WEAK FORM              L  3.3
  !
  !L                  C3.4     VECTORS FOR LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMVEC/
  !
  !  SOURCT(MDCOMP)  TOTAL VECTOR OF RIGHT-HAND SIDE		CA 3.4
  !  U(MDCOL)        VECTOR OF UNKNOWS FOR ONE BLOCK              CA 3.4
  !  UT              TEMPORARY STORAGE                            C  3.4
  !  VA(MDCOL)       TEMPORARY STORAGE                            CA 3.4
  !  X(MDCOL)        VECTOR OF UNKNOWNS FOR ONE BLOCK             CA 3.4
  !  XOHMR(MDPOL)    OHM VECTOR FOR POWER AT ANTENNA              CA 3.4
  !  XT(MDCOMP)      TOTAL VECTOR OF UNKNOWNS                     CA 3.4
  !  XDCHI(MDPOL)    DELTA-CHI AT PLASMA SURFACE                  RA 3.4
  !  XDTH(MDPOL)     DELTA-THETA AT PLASMA SURFACE                RA 3.4
  !
  !L                  C3.5     NUMERICAL VARIABLES FOR LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMIVI/
  !
  !  EPSMAC          *ROUND-OFF ERROR OF COMPUTER                 R  3.5
  !  M1              RANK OF MATRIX OVERLAP SUBBLOCK              I  3.5
  !  M11             M1+1                                         I  3.5
  !  M12             M1+M2 = RANK OF MATRIX BLOCK                 I  3.5
  !  M2              RANK OF MATRIX BLOCK - M1                    I  3.5
  !  N               NB. OF MATRIX BLOCKS (=NPSI)                 I  3.5
  !  NCOMP           NB. OF ELEMENTS OF SOLUTION VECTOR           I  3.5
  !  NLONG           NB. OF ELEMENTS IN A MATRIX BLOCK            I  3.5
  !  NSING           SINGULARITY INDICATOR (=-1 IF A IS SING.)    I  3.5
  !
  !L                  C5.1     CONTROL VARIABLES
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMCON/
  !
  !  AHEIGT          *HEIGHT OF 2-D PLOTS                         R  5.1
  !  ALARG           *WIDTH OF 2-D PLOTS                          R  5.1
  !  ANGLET(16)      *TOROIDAL CUTS (DEGREES)                     RA 5.1
  !  ARSIZE          *SIZE OF ARROWS                              R  5.1
  !  ASYMB           *SIZE OF SYMBOLS                             R  5.1
  !  MFL             *LOWER M VALUE FOR FOURIER ANALYSIS          I  5.1
  !  MFU             UPPER M VALUE FOR FOURIER ANALYSIS           I  5.1
  !  NCONTR          *NUMBER OF CONTOUR LINES                     I  5.1
  !  NCUT            *NUMBER OF TOROIDAL CUTS                     I  5.1
  !  NLDISO	   *SWITCH COMPUTATION DIAGNOSTICS OF SOLUTION  L  5.1
  !  NLPHAS          *SWITCH POLOIDAL PHASE EXTRACTION            L  5.1
  !  NLOTP0          *GENERAL LINE-PRINTER OUTPUT SWITCH          L  5.1
  !  NLOTP1(4)       *OUTPUT SWITCHES FOR LION1                   LA 5.1
  !  NLOTP2(5)       *OUTPUT SWITCHES FOR LION2                   LA 5.1
  !  NLOTP3(2)       *OUTPUT SWITCHES FOR LION3                   LA 5.1
  !  NLOTP4(5)       *OUTPUT SWITCHES FOR LION4                   LA 5.1
  !  NLOTP5(40)      *OUTPUT SWITCHES FOR LION5                   LA 5.1
  !  NLPLO5(25)      *PLOT SWITCHES FOR LION5                     LA 5.1
  !  NPLTYP	   *=1 FOR BASPL PLOTS, =2 FOR EXPLORER PLOTS   I  5.1
  !  NRUN            *NUMBER OF RUNS FOR FREQUENCY TRACE          I  5.1
  !  NTORSP	   *NUMBER OF TOR. WN'S FOR THE TOR.WN SCAN	I  5.1
  !  WNTDEL	   *TOR.WN. INCREMENT FOR TOR.WN. SCAN		R  5.1
  !
  !            Control variables related to ITM: Integrated Modeling
  !
  !  NITMOPT:  Uses ITM database: 0 (default) = no, 1 =reads from ITM, 10=writes on ITM, 11=reads and writes, 22=LION run as module within Kepler
  !  NITMSHOT(1:2): shot number for reading data (NITMSHOT(1)) and writing data (NITMSHOT(2)) (not used if NITMOPT=0 or 22)
  !  NITMRUN(1:2): run number for reading data (NITMRUN(1)) and writing data (NITMRUN(2)) (not used if NITMOPT=0 or 22)
  !  NANT_ITM: for antennas definition see above
  !  TIME_ITM(1:2): (default=-1e40) If time_itm(1)>-1e39 then uses time_itm to define on which time slices to run LION. Not used if NITMOPT=0 or 22.
  !                TIME_ITM(1) >= TIME_ITM(2): run only for one time-slice for time closest to TIME_ITM(1) in equil_in(:)
  !                TIME_ITM(1) < TIME_ITM(2): run for all times within intervale [TIME_ITM(1),TIME_ITM(2)]
  !                size of output waves depends on number slices run for.
  !
  !
  !L                  C5.2     I/O DISK CHANNELS NUMBERS
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON/COMOUT/
  !
  !  MEQ             *EQUILIBIUM QUANTITIES                       I  5.2
  !  NDA             *MATRIX A                                    I  5.2
  !  NDES            *R,Z COORDINATES AND NORMALS                 I  5.2
  !  NDLT            *DECOMPOSED MATRIX L,D,U                     I  5.2
  !  NDS             *SOLUTION VECTOR                             I  5.2
  !  NPRNT           *LINE-PRINTER OUTPUT                         I  5.2
  !  NSAVE           *NAMELIST LINK LION1 TO 5                    I  5.2
  !  NVAC            *VACUUM QUANTITIES                           I  5.2
  !
  !
  !---------------------------------------------------------------------
  !L             9.          BLANK COMMON
  !
  !
  !
  !L                  C9.2     VACUUM QUANTITIES FOR LION2
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMVID)
  !
  !  ANTR(MDPOL)     ANTENNA VECTOR                               CA 9.2
  !  OHMR(MDPOL)     OHM VECTOR                                   CA 9.2
  !  REASCR          REACTANCE SCALAR                             C  9.2
  !  SAUTR(MDPOL)    JUMP ACROSS ANTENNA SURFACE                  CA 9.2
  !  SAUTX(MDPOL)    TEMPORARY                                    CA 9.2
  !  SOURCE(MDPOL)   SOURCE VECTOR                                CA 9.2
  !  CA              CONSTANT TO DEFINE WALL                      R  9.2
  !  CB              #                                            R  9.2
  !  CC              #                                            R  9.2
  !  D2ROP           D**2RHO/DTHETA**2 AT MID-POINT               R  9.2
  !  DC(MD2CP2)      DELTA-CHI                                    RA 9.2
  !  DELTH(MD2CP2)   DELTA-THETA (TH(K+1)-TH(K))                  RA 9.2
  !  DRODT(4)        DRHO/DTHETA AT INTEGRATION POINTS            RA 9.2
  !  DROPDT(4)       DRHOPRIME/DTHETA                             RA 9.2
  !  G(MDPOL,MDPOL,13)
  !                  ALL MATRICES                                 RA 9.2
  !  QB              SAFETY FACTOR AT PLASMA SURFACE              R  9.2
  !  RO(4)           RHO AT INTEGRATION POINTS                    RA 9.2
  !  RO2             RHO AT THE CENTER                            R  9.2
  !  RO2P            RHO PRIME AT THE CENTER                      R  9.2
  !  ROEDGE(MD2CP2)  RHO PLASMA SURF. AT EDGES OF CHI INTERVALS   RA 9.2
  !  ROMID(MD2CP2)   RHO PLASMA SURF. AT MIDDLE CHI INTERVALS     RA 9.2
  !  ROP(4)          RHO PRIME AT INTEGRATION POINTS              RA 9.2
  !  SR(MD2CP2)      R(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  SZ(MD2CP2)      Z(CHI) DEFINING PLASMA SURFACE               RA 9.2
  !  T(MD2CP2)       THETA AT MIDDLE CHI INTERVALS                RA 9.2
  !  TB              TOROIDAL MAGNETIC FLUX AT PLASMA SURFACE     R  9.2
  !  TH(MD2CP2)      THETA AT EDGES OF CHI INTERVALS              RA 9.2
  !  TP(4)           THETA PRIME AT INTEGRATION POINTS            RA 9.2
  !  TT(4)           THETA AT INTEGRATION POINTS                  RA 9.2
  !  NDIM            DIMENSION OF MATRICES                        I  9.2
  !
  !L                  C9.3     MATRIX BLOCKS FOR LION 3 AND LION4
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMMTR)
  !
  !  A1D(MDLENG)       MATRIX BLOCK OF DISCRETIZED WEAK FORM        CA 9.3
  !
  !L                  C9.5     OUTPUT AND PLOT QUANTITIES FOR LION5
  ! VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
  !      COMMON//    (COMPLO)
  !
  !  ABSPOW(MDPSI)   POWER ABSORBED ON PSI=CONST.                 RA 9.5
  !  APHI            TOROIDAL ANGLE PHI                           R  9.5
  !  BNL             NORMAL COMPONENT OF WAVE MAGNETIC FIELD      C  9.5
  !  BPARL           PARALLEL COMP. OF WAVE MAGNETIC FIELD        C  9.5
  !  BPL             PERP. COMP. OF WAVE MAGNETIC FIELD           C  9.5
  !  CCHI(MDPOL)     CHI VALUES AT CENTER OF CELLS                RA 9.5
  !  CCR(MDPSI,MDPOL)
  !                  R AT CENTER OF CELLS                         RA 9.5
  !  CCS(MDPSI)      S VALUES AT CENTER OF CELLS                  RA 9.5
  !  CCZ(MDPSI,MDPOL)
  !                  Z AT CENTER OF CELLS                         RA 9.5
  !  CDEVIA          WIDTH OF POWER DISTRIBUTION IN CHI           R  9.5
  !  CELPOW(MDPSI,MDPOL)
  !                  POWER ARBSORBED IN THE CELLS                 RA 9.5
  !  CHIPOW(MDPOL)   POWER ABSORBED ON CHI=CONST.                 RA 9.5
  !  CMEAN           CENTER OF POWER DISTRIBUTION IN CHI          R  9.5
  !  CNR(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., R COMPONENT            RA 9.5
  !  CNZ(MDPSI,MDPOL)
  !                  NORMAL TO PSI=CONST., Z COMPONENT            RA 9.5
  !  COST(MDPSI,MDPOL)COSINUS OF ANGLE BETWEEN PHI AND B_0        RA 9.5
  !  CPL             POWER ABSORBED IN A CELL                     R  9.5
  !  CPLE            POWER IN A CELL DUE TO NU                    R  9.5
  !  CPLJ(MDSPC2)    POWER IN A CELL PER SPECIES                  RA 9.5
  !  DENPOW(0:MDPSI,MDPOL1)
  !                  POWER ABSORPTION DENSITY                     RA 9.5
  !  DEPOS(MDPSI)    POWER DENS. AVERAGED ON PSI=CONST.           RA 9.5
  !  DPL             POWER ABSORPTION DENSITY AT CENTER OF CELL   R  9.5
  !  DPLE            POWER ABSORPTION DENSITY DUE TO NU           R  9.5
  !  DPLJ(MDSPC2)    POWER DENSITY PER SPECIES                    RA 9.5
  !  DVDC            DV/DCHI                                      C  9.5
  !  DXDC            DX/DCHI                                      C  9.5
  !  DXDS            DX/DS                                        C  9.5
  !  ECHEL           SCALE FACTOR FOR 2-D PLOTS                   R  9.5
  !  ELEPOW          POWER DUE TO NU                              R  9.5
  !  ENL             NORMAL COMPONENT OF WAVE ELECTRIC FIELD      C  9.5
  !  EPL             PERP. COMP. OF WAVE ELECTRIC FIELD           C  9.5
  !  FLUPOW(MDPSI1)  POWER ABSORBED INSIDE PSI=CONST.             RA 9.5
  !  FLUPSP(MDPSI1,MDSPC2)
  !                  POWER ABSORBED INSIDE PSI=CONST.,PER SPECIES RA 9.5
  !  FREN(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-N          CA 9.5
  !  FREP(13,MDPSI)  FOURIER DECOMPOSITION IN CHI OF E-PERP       CA 9.5
  !  FRINT(7,MDCHI)  INTEGRAL FOR FOURIER DECOMPOSITION           CA 9.5
  !  NRZSUR          NUMBER OF POINTS FOR PLOTTING THE SURFACE    I  9.5
  !  SDEVIA          WIDTH OF POWER DISTRIBUTION IN S             R  9.5
  !  SFLUX(MDPSI)    POYNTING FLUX ACROSS PSI=CONST.              RA 9.5
  !  SINT(MDPSI,MDPOL)SINUS OF ANGLE BETWEEN PHI AND B_0          RA 9.5
  !  SMEAN           MINOR RADIUS OF HALF POWER ABSORPTION        R  9.5
  !  SN(MDPSI,MDPOL) NORMAL COMPONENT OF POYNTING                 RA 9.5
  !  SNL             NORMAL COMPONENT OF POYNTING                 R  9.5
  !  SPAR(MDPSI,MDPOL)
  !                  PARALLEL COMPONENT OF POYNTING               RA 9.5
  !  SPARL           PARALLEL COMPONENT OF POYNTING               R  9.5
  !  SPERP(MDPSI,MDPOL)
  !                  PERP. COMPONENT OF POYNTING                  RA 9.5
  !  SPL             PERP. COMP. OF POYNTING                      R  9.5
  !  SURPSI(MDPSI1)  INTEGRAL FOR PSI-SURFACE                     RA 9.5
  !  VC              V                                            C  9.5
  !  VOUT1(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  VOUT2(MDPSI,MDPOL)
  !                  TEMPORARY STORAGE                            RA 9.5
  !  XC              X                                            C  9.5
  !  XS(MDRZ)        R COORDINATES OF PLASMA SURFACE              RA 9.5
  !  YS(MDRZ)        Z COORDINATES OF PLASMA SURFACE              RA 9.5
  !
  !*********************************************************************
  !*********************************************************************
  !
  !CC*DECK P1C0S02
  !        -----------------
  !
  !  1.0.02. CONTROLS THE RUN
  !--------------------------------------------------------------------
  !     IN THIS VERSION THE EQUILIBRIUM CALCULATION AND MAPPING IS DONE
  !     IN THE CODE CHEASE (H. LUTJENS). THE DATA IS TRANSFERRED VIA 
  !     DISK I/O ON THE FOLLOWING CHANNELS:
  !
  !        NSAVE     NAMELIST VARIABLES FROM CHEASE (NPSI, NCHI, ETC.)
  !        MEQ       EQUILIBRIUM QUANTITIES EQ(I,Jchi,Jpsi)
  !        NVAC      QUANTITIES AT PLASMA-VACUUM INTERFACE
  !        NDES      R,Z,AND NORMALS TO PSI=CONST. R,Z OF P.V.I.
  !
  !     THEREFORE WE DO NOT USE 'LION1', FORMERLY 'ERATO1'.
  !
  !     PLEASE PAY ATTENTION TO THE FACT THAT THE NAMELIST $NEWRUN
  !     TRANSFERRED BY CHEASE IS READ IN SUBROUTINE AUXVAL, WHEREAS
  !     NAMELIST $NEWRUN CONTAINING THE INPUT PARAMETERS FOR LION
  !     IS READ IN SUBROUTINE DATA. THIS IS DONE TO AVOID CONFLICTS
  !     IN THE DEFINITIONS OF THESE NAMELISTS (THEY ARE DIFFERENT!).
  !     THE DEFINITIONS OF SOME OF THE NAMELIST VARIABLES ARE DIFFERENT.
  !     PLEASE LOOK INTO SUBROUTINE AUXVAL FOR MORE INFORMATION.
  !---------------------------------------------------------------------
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE GENERATOR FREQUENCY (FREQCY).
  !
  !     THE NUMBER OF DIFFERENT FREQUENCIES TO BE RUN IS GIVEN IN THE
  !     NAMELIST $NEWRUN OF LION (NRUN).
  !
  !     THE FREQUENCY IS INCREMENTED BY A CONSTANT AMOUNT AT EACH RUN.
  !     THIS AMOUNT IS GIVEN IN THE NAMELIST $NEWRUN OF LION (DELTAF).
  !---------------------------------------------------------------------
  !     THIS VERSION INCLUDES THE POSSIBILITY TO MAKE SEVERAL RUNS IN
  !     ONE, WITH THE SAME EQUILIBRIUM AND PHYSICAL PARAMETERS OTHER
  !     THAN THE TOROIDAL WAVE NUMBER.
  !
  !     THE NUMBER OF TOR. W. N. IS NTORSP
  !
  !     THE TOR. W. N. IS INCREMENTED BY WNTDEL
  !---------------------------------------------------------------------
  !
  !
  use globals
  use ids_schemas, only: r8=>ids_real
  ! already in globals:  use ids_schemas
  use antenna_tools, only: set_global_antenna_param
  use imas_wave_identifier, only: WAVEID => wave_identifier
  use cocos_module
  use LION_profiles, only: internal_profs, &
       push_internal_profs_to_globals, write_internal_profs, &
       free_internal_core_profiles
  use LION_profile_tools_IMAS, only: init_internal_profs
  use assign_code_parameters_module, only: assign_params
  ! TESTING ONLY: use write_global_data, only: write_globals
  !
  IMPLICIT NONE

  interface
     SUBROUTINE EQ_from_equil(equil_in,in_time_slice, error_flag,output_message)
       !
       use ids_schemas
       !
       IMPLICIT NONE
       !
       type(ids_equilibrium), intent(in)      :: equil_in
       integer,               intent(in)      :: in_time_slice
       integer,               intent(out)     :: error_flag
       character(len=:), allocatable, intent(out) :: output_message
       !
     end SUBROUTINE EQ_from_equil
     !
     subroutine COCOStransform(equilibrium_in, equilibrium_out, COCOS_in, COCOS_out, IPsign_out, B0sign_out)
       !
       use ids_schemas
       IMPLICIT NONE
       type (ids_equilibrium)  ::  equilibrium_in
       type (ids_equilibrium)  ::  equilibrium_out
       integer, optional, intent(IN) :: COCOS_in
       integer, optional, intent(IN) :: COCOS_out
       integer, optional, intent(IN) :: IPsign_out
       integer, optional, intent(IN) :: B0sign_out
     end subroutine COCOStransform
  end interface
  !
  ! Inputs/output assumed in case of ITM link
  ! CPO_in: equilibrium, coreprof, antennas, waves 
  ! CPO_out: waves
  !
  type(ids_equilibrium),         intent(in)    :: equil_in
  type(ids_core_profiles),       intent(in)    :: coreprof_in
  type(ids_ic_antennas),         intent(in)    :: antennas_in
  type(ids_waves),               intent(in)    :: waves_in
  type(ids_waves),               intent(inout) :: waves_out ! intent(out) kills any allocation, so better to avoid
  type(ids_parameters_input),    intent(in)    :: param_code
  integer,                       intent(out)   :: output_flag
  character(len=:), allocatable, intent(out)   ::output_message
  !
  ! INTERNAL VARIABLES
  !
  type(ids_equilibrium) :: equil_inCOCOS2
  !
  integer :: istatus, iout_size, i, isize_comment, COCOS_LION, IPsign_out, B0sign_out
  !
  REAL(RKIND) :: t0, t1, t2, t3, t4, t5, t6, tend, ZOLDFR, R0EXP, B0EXP, EDGE_TE
  INTEGER :: JTORSP, JRUN, JPOL, ispec, npsi_out, in_time_slice, in_time_slice1, in_time_slice2
  CHARACTER(132), allocatable :: ANTENNA_NAME(:)
  !---------------------------------------------------------------------
  !
  !**********************************************************************
  !
  ! If within KEPLER, do here what was performed in LION_prog.f90, except loading shot
  !
  !  NITMOPT=22
  IF (NVERBOSE .GE. 0) write(0,*) '=== START: LION ==='
  IF (NVERBOSE .GE. 0) write(0,*) 'NITMOPT = ',NITMOPT
  if (associated(equil_in%time_slice)) then
    IF (NVERBOSE .GE. 1) write(*,*) 'equil_in%time_slice(1)%global_quantities%magnetic_axis%r = ', &
      & equil_in%time_slice(1)%global_quantities%magnetic_axis%r
  end if
  IF (NITMOPT .EQ. 22) THEN
    !call system('rm -f TAPE*')
    !
    ! ASSUME WITHIN KEPLER, IN/OUT EQUILIBRIUM VIA ARGUMENTS ONLY
    ! NAMELIST VALUES WITHIN EQUIL_IN(1)%CODEPARAM%PARAMETERS(:)
    !
    CALL PRESET
    call assign_params(param_code%parameters_value,istatus)
    !! For testing only: call write_globals('out.globalsIMAS.txt.new')
    !
    NITMOPT=22 ! Ignore value of NITMOPT from xml input; we've already started running with NITMOPT=22.
    if (istatus /= 0) then
      output_flag=-9
      output_message = 'ERROR: Could not assign some code parameters.'
      write(*,*)output_message
      return
    end if
    !
    ! avoid: call ids_copy(equil_in,eqchease_in) so no need for module ids_routines
!!$     call ids_copy(equil_in,eqchease_in) 
!!$    eqchease_in => equil_in
    IF ((NVERBOSE .GE. 1) .and. associated(equil_in%time_slice)) &
         write(*,*) 'equil_in%time_slice(1)%global_quantities%magnetic_axis%r = ', &
      & equil_in%time_slice(1)%global_quantities%magnetic_axis%r
    !
    IF (NVERBOSE .GE. 1) print *,'(size(equil_in%time)= ',size(equil_in%time),' .NE. 1), not sure is ok now, add copy slice'
    !
    ! Assign some values related to codeparam when passing within kepler
    isize_comment = size(comments)
    do i=1,isize_comment
      if (adjustl(trim(comments(i))) == '') then
        ! comments not given in codeparam (and no labels from chease_namelist file possible
        comments(i) = 'LION run from Kepler'
        exit
      end if
    end do
  END IF
  !
  ! Validate input
  !
  IF (NITMOPT .EQ. 22) THEN
     IF (.not. associated(equil_in%time_slice)) then
        output_flag = -10
        output_message = 'ERROR in LION: equil_in%time_slice not associated'
        write(*,*)output_message
        return
     END IF
     IF (.not. associated(coreprof_in%profiles_1d)) then
        output_flag = -11
        output_message = 'ERROR in LION: coreprof_in%profiles_1d not associated'
        write(*,*)output_message
        return
     END IF
     IF (.not. associated(waves_in%coherent_wave)) then
        output_flag = -12
        output_message = 'ERROR in LION: waves_in%coherent_wave not associated'
        write(*,*)output_message
        return
     END IF
     IF (.not. associated(antennas_in%antenna)) then
        output_flag = -13
        output_message = 'ERROR in LION: antennas_in%antenna not associated'
        write(*,*)output_message
        return
     END IF
     IF (.not. associated(param_code%parameters_value)) then
        output_flag = -14
        output_message = 'ERROR in LION: param_code%parameters_value not associated'
        write(*,*)output_message
        return
     END IF
  END IF
  !
  ! Determine time slices to run LION on
  !
  in_time_slice1 = 1
  in_time_slice2 = in_time_slice1
  if (NITMOPT .NE. 0) then
    if (MOD(NITMOPT,10) .EQ. 0) then
      ! inputs from file, thus only 1 time slice
      in_time_slice1 = 1
      in_time_slice2 = in_time_slice1
    else
      ! inputs from ITM, might have several time slices to run for
      !
      ! check that size of each inputs are consistent
      if ((size(equil_in%time_slice) .NE. size(coreprof_in%profiles_1d)) .or. &
           (size(equil_in%time_slice) .NE. size(antennas_in%antenna(1)%frequency%data)) .or. &
           (size(equil_in%time_slice) .NE. size(waves_in%coherent_wave(1)%global_quantities))) then
        write(6,*) 'input ids do not have same size therefore use just first time slice'
        call flush(6)
        in_time_slice1 = 1
        in_time_slice2 = in_time_slice1
      elseif (size(equil_in%time_slice) .EQ. 1) then
        ! input size is 1
        in_time_slice1 = 1
        in_time_slice2 = in_time_slice1
      else
        ! size(equil_in%time_slice) > 1
        if (time_itm(1) .LE. -1.e39_rkind) then
          ! time_itm not defined, use just first time slice
          in_time_slice1 = 1
          in_time_slice2 = in_time_slice1
        elseif (time_itm(1) .GE. time_itm(2)) then
          ! find closest time to time_ITM(1) and run LION for that time slice
          in_time_slice1 = 1
          do i=2,size(equil_in%time_slice)
            if (equil_in%ids_properties%homogeneous_time .eq. 1) then
              if (abs(equil_in%time(i)-time_itm(1)) .LT. abs(equil_in%time(in_time_slice1)-time_itm(1))) then
                in_time_slice1 = i
              end if
            else
              if (abs(equil_in%time_slice(i)%time-time_itm(1)) .LT. abs(equil_in%time_slice(in_time_slice1)%time-time_itm(1))) then
                in_time_slice1 = i
              end if
            end if
          end do
          in_time_slice2 = in_time_slice1
        elseif (time_itm(1) .LT. time_itm(2)) then
          ! find all slices within [time_itm(1),time_itm(2)]
          in_time_slice1 = size(equil_in%time_slice)
          in_time_slice2 = 1
          do i=1,size(equil_in%time_slice)
            ! find first index with t>=time_itm(1)
            if (equil_in%ids_properties%homogeneous_time .eq. 1) then
              if ( (equil_in%time(i) .GE. time_itm(1)) .AND. (i .le. in_time_slice1)) then
                in_time_slice1 = i
              end if
              ! find last index with t<=time_itm(2)
              if (equil_in%time(i) .LE. time_itm(2)) then
                in_time_slice2 = i
              end if
            else
              if ( (equil_in%time_slice(i)%time .GE. time_itm(1)) .AND. (i .le. in_time_slice1)) then
                in_time_slice1 = i
              end if
              ! find last index with t<=time_itm(2)
              if (equil_in%time_slice(i)%time .LE. time_itm(2)) then
                in_time_slice2 = i
              end if
            end if
          end do
          ! check
          if (in_time_slice2 .lt. in_time_slice1) then
            write(6,*) 'problem with time interval: in_time_slice1=',in_time_slice1,' > in_time_slice2=',in_time_slice2
            if (equil_in%ids_properties%homogeneous_time .eq. 1) then
              write(6,*) 'equil_in%time(in_time_slice1)= ',equil_in%time(in_time_slice1)
            else
              write(6,*) 'equil_in%time_slice(in_time_slice1)%time= ',equil_in%time_slice(in_time_slice1)%time
            end if
          end if
        end if
        IF (NVERBOSE .GE. 3) then
          if (equil_in%ids_properties%homogeneous_time .eq. 1) then
            write(6,*) 'equil_in%time(in_time_slice1)= ',equil_in%time(in_time_slice1)
            write(6,*) 'equil_in%time(in_time_slice2)= ',equil_in%time(in_time_slice2)
          else
            write(6,*) 'equil_in%time_slice(in_time_slice1)%time= ',equil_in%time_slice(in_time_slice1)%time
            write(6,*) 'equil_in%time_slice(in_time_slice2)%time= ',equil_in%time_slice(in_time_slice2)%time
          end if
          write(6,*) 'time_itm(1:2) = ',time_itm(1:2)
        end IF
      end if
    END IF
  end if
  !
  iout_size = in_time_slice2 - in_time_slice1 + 1
  !
  if (iout_size > 1) print *,'iout_size = ',iout_size,' > 1: is it correct?'
  !
  ! to avoid using module with ids_copy, using pointer assignement for waves_out, thus cannot deallocate wavesLION_out
  ! Otherwise makes kepler crash when run 2nd time, so just (re-)allocate here
  allocate(wavesLION_out%coherent_wave(1)) ! one frequency at this stage
  allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_size))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_size))
  do iout_time_slice=1,iout_size
    in_time_slice = in_time_slice1 + iout_time_slice - 1
    ! copy input parameters to codeparam
    if (associated(param_code%parameters_value)) then
      allocate(wavesLION_out%code%parameters(size(param_code%parameters_value)))
      wavesLION_out%code%parameters = param_code%parameters_value
    end if
    if (associated(equil_in%time_slice)) then
      if (equil_in%ids_properties%homogeneous_time .eq. 1) then      
        wavesLION_out%ids_properties%homogeneous_time = 1
        allocate(wavesLION_out%time(iout_time_slice))
        wavesLION_out%time(iout_time_slice) = equil_in%time(in_time_slice)
      else
        wavesLION_out%ids_properties%homogeneous_time = 0
        wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%time = equil_in%time_slice(in_time_slice)%time
      end if
    else
      wavesLION_out%ids_properties%homogeneous_time = 1
      allocate(wavesLION_out%time(iout_time_slice))
      wavesLION_out%time(iout_time_slice) = -1._rkind
    end if
    allocate(wavesLION_out%code%name(1))
    wavesLION_out%code%name(1) = 'LION'
    allocate(wavesLION_out%code%version(1))
    wavesLION_out%code%version(1) = 'V1_4'
    wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%electrons%distribution_assumption = 0
  end do
  !
  CALL runtime (t0)
  !
  ! Transform equilibrium to COCOS=2 AND Ip, B0 positive (to have q>0 as well) since this is LION convention
  !
  IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .GE. 1) THEN
    IPsign_out = +1
    B0sign_out = +1
    COCOS_LION = 2
    call COCOStransform(equil_in, equil_inCOCOS2, COCOS_in, COCOS_LION, IPsign_out, B0sign_out)
    ! call copy_arr_ids_equilibrium(equil_in,equil_inCOCOS2)
    if (.not. associated(equil_inCOCOS2%time_slice(in_time_slice)%coordinate_system%tensor_contravariant)) then
       output_message = 'ERROR in LION.f90: equil_inCOCOS2%time_slice(1)%coordinate_system%tensor_contravariant not associated'
      write(0,*)output_message
      output_flag=-51
      return
    endif
    equil_inCOCOS2%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(:,:,1,3) = 0._R8
    equil_inCOCOS2%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(:,:,2,3) = 0._R8
  end IF
  !
  ! Steps over all time slices
  !
  do iout_time_slice=1,iout_size
    in_time_slice = in_time_slice1 + iout_time_slice - 1
    !
    !     "READ" NAMELIST FROM CHEASE AND SET AUXILIARY VALUES
    IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .GE. 1) THEN
      ! If inputs from ITM database, use input CPOs
      ! from equilibrium:
      NPSI = size(equil_inCOCOS2%time_slice(in_time_slice)%coordinate_system%grid%dim1) - 1
      NCHI = size(equil_inCOCOS2%time_slice(in_time_slice)%coordinate_system%grid%dim2)
      QS(1:NPSI+1) = equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%q(1:NPSI+1)
      QTILDA(1:NPSI+1) = QS(1:NPSI+1)
      BETA = equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%beta_tor
      QIAXE = 1._RKIND / equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%q(1)
      write(*,*) 'should check if R0, B0 should not be taken from vacuum instead of magnetic_axis'
      R0EXP = equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%magnetic_axis%r
      B0EXP = equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%magnetic_axis%b_field_tor
      BNOT = equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%magnetic_axis%b_field_tor
      RMAJOR = equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%magnetic_axis%r
      CPSRF = (equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%psi_boundary - &
        & equil_inCOCOS2%time_slice(in_time_slice)%global_quantities%psi_axis)/R0EXP**2/B0EXP
      !    NLDISO = .F.
      !
      ! from coreprof
      !
      NTEMP = -3
      NDENS = -3
      !
      USE_INTERNAL_PROF: if ( (NDENS==-3) .OR. (NTEMP==-3) ) then
         !
         ! Initialise spline representations of plasma profiles:
         call init_internal_profs(coreprof_in,in_time_slice,istatus)
         if (istatus /= 0) then
            IF (NVERBOSE .GE. 0) write(0, *) 'ERROR: problem in init_internal_profs, error status = ',istatus
            output_flag = -103
            return
         end if
         call push_internal_profs_to_globals(istatus)
         if (istatus /= 0) then
            IF (NVERBOSE .GE. 0) write(0, *) 'ERROR: problem in push_internal_profs_to_globals, error status = ',istatus
            output_flag = -104
            return
         end if
         !
         !if (NVERBOSE > 3) then
            open(unit=631,file='out.internal_profs.ids.yml')
            call write_internal_profs(631)
            close(631)
         !end if
         !
      else ! USE_INTERNAL_PROF
         !
         if (.not. associated(coreprof_in%profiles_1d(in_time_slice)%ion)) then
            write(0,*)"ERROR in LION.f90, coreprof_in%profiles_1d(in_time_slice)%ion not associated"
            output_flag = -102
            return
         end if
         !
!!! What is this??        ispec = nrspec
!!! What is this??        ...
!!! What is this??        if (ispec .lt. 0) nrspec=min(-ispec,nrspec)
        ispec = 0
        do i=1,size(coreprof_in%profiles_1d(in_time_slice)%ion)
           if (coreprof_in%profiles_1d(in_time_slice)%ion(i)%multiple_states_flag == 0) then
              ispec=ispec+1
              if (associated(coreprof_in%profiles_1d(in_time_slice)%ion(i)%element)) then
                 if (size(coreprof_in%profiles_1d(in_time_slice)%ion(i)%element) .eq. 1) then
                    acharg(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(i)%element(1)%z_n
                    amass(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(i)%element(1)%a
                 else
                    acharg(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(i)%element(1)%z_n
                    amass(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(i)%element(1)%a
                    write(0,*) &
                         & 'size(coreprof_in%profiles_1d(in_time_slice)%ion(i)%element) > 1, not sure what to do in LION'
                 end if
              else
                 write(0,*)"ERROR in LION.f90: No ion/element allocated in coreprof"
                 output_flag = -101
                 return
              end if
           end if
        end do
        nrspec=ispec
        !
        ! Set profiles parameters; on-axis values and profiles-shape parameters
        do ispec=1,nrspec
           cenden(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(ispec)%density(1)
           centi(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(ispec)%temperature(1)
           centip(ispec) = coreprof_in%profiles_1d(in_time_slice)%ion(ispec)%temperature(1)
           ! EQKAPT(ispec) = 100._rkind ! for shot=10, run=2
           ! EQKAPT(ispec) = 2._rkind ! for shot=20, run=1
           EQTI(ispec) = 1._rkind - (coreprof_in%profiles_1d(in_time_slice)%ion(ispec)%temperature( &
                size(coreprof_in%profiles_1d(in_time_slice)%ion(ispec)%temperature)-4)/centi(ispec))**(1_rkind/EQKAPT(ispec))
        end do
        ! EQKAPD = 100._rkind ! for shot=10, run=2
        ! EQKAPD = 2._rkind ! for shot=20, run=1
        EQDENS = 1._rkind - (0.99)**(1_rkind/EQKAPD)
        cente = coreprof_in%profiles_1d(in_time_slice)%electrons%temperature(1)
        EDGE_TE = max( coreprof_in%profiles_1d(in_time_slice)%electrons%temperature( &
             size(coreprof_in%profiles_1d(in_time_slice)%electrons%temperature)-4) , &
             1e-3_RKIND*maxval(coreprof_in%profiles_1d(in_time_slice)%electrons%temperature) )
        ! EQKPTE = 100._rkind ! for shot=10, run=2
        ! EQKPTE = 2._rkind ! for shot=20, run=1
        !
        !write(*,*)'TE=', &
        !     coreprof_in%profiles_1d(in_time_slice)%electrons%temperature
        !write(*,*)'cente =', cente, coreprof_in%profiles_1d(in_time_slice)%electrons%temperature(1)
        !
        !write(*,*)'calculating EQTE:', &
        !     (coreprof_in%profiles_1d(in_time_slice)%electrons%temperature( &
        !     size(coreprof_in%profiles_1d(in_time_slice)%electrons%temperature)-4)) , cente , &
        !     (coreprof_in%profiles_1d(in_time_slice)%electrons%temperature( &
        !     size(coreprof_in%profiles_1d(in_time_slice)%electrons%temperature)-4)/cente)
        !
        EQTE = 1._rkind - (EDGE_TE/cente)**(1_rkind/EQKPTE)
      endif USE_INTERNAL_PROF
      !
      !
      ! from waves_in
      !
      if (.not. associated(waves_in%coherent_wave)) then
        output_flag = -201
        output_message = "ERROR in LION.f90: incomplete input, waves_in%coherent_wave not associated"
        write(*,*)output_message
        return
      endif
      if (.not. associated(waves_in%coherent_wave(1)%global_quantities)) then
        output_flag = -202
        output_message = "ERROR in LION.f90: incomplete input, waves_in%coherent_wave(1)%global_quantities not associated"
        write(*,*)output_message
        return
      endif
      if (size(waves_in%coherent_wave(1)%global_quantities) < in_time_slice) then
        output_flag = -203
        output_message = "ERROR in LION.f90: incomplete input, size(waves_in%coherent_wave(1)%global_quantities) < in_time_slice"
        write(*,*)output_message
        return
      endif
      !
      if (associated(waves_in%coherent_wave(1)%global_quantities(in_time_slice)%n_tor)) then
        WNTORO = real(waves_in%coherent_wave(1)%global_quantities(in_time_slice)%n_tor(1),rkind)
      else
         output_flag = -204
         output_message = 'ERROR in LION: waves_in%...%n_tor not associated'
         write(*,*)output_message
         return
      end if
      ! WNTORO = 27._rkind
      ! FREQCY = antennas_in(in_time_slice)%antenna_ic(1)%frequency%value
      FREQCY = waves_in%coherent_wave(1)%global_quantities(in_time_slice)%frequency
      if (nverbose .ge. 1) write(*,*)'FREQCY=', FREQCY
      if ((FREQCY.LT.1E0) .OR. (FREQCY.GT.1e10)) then
         output_flag = -205
         output_message= 'ERROR in LION: unrealistic value for the RF frequency'
         write(*,*)output_message
         return
      end if
      !
      !---------------------------------------------------------
      ! START: ADDED BY THOMAS JOHNSON (20130302)
      !---------------------------------------------------------
      ! Set antenna information from ANTENNAS CPO
      if (NANT_ITM .EQ. 1) THEN
        if (.not. associated(antennas_in%antenna)) then
           output_flag = -301
           output_message = 'ERROR in LION_IMAS.f90: antennas_in%antenna not associated'
           return
        end if
        if (size(antennas_in%antenna) == 0) then
           output_flag = -302
           output_message = 'ERROR in LION_IMAS.f90: size(antennas_in%antenna)=0'
           return
        end if
        if (associated(antennas_in%antenna(1)%name)) then
           allocate( ANTENNA_NAME(size(antennas_in%antenna(1)%name)) )
           ANTENNA_NAME = antennas_in%antenna(1)%name
        endif
        !
        IF (NVERBOSE .GE. 2) write(*,*)'---- SETTING IC ANTENNA ----' 
        !
        call set_global_antenna_param(antennas_in, equil_inCOCOS2,WNTORO, &
             & 10._rkind, & !!!!  SHOULD BE THE FEEDER TILT, assume need 10 degrees (see documentation in antenna_tools.f90) !!!!
             & output_flag)
        if (nverbose .ge. 2) then
           write(*,*) 'after set_global_antenna_param:'
           write(*,*) 'ANTRAD = ',ANTRAD
           write(*,*) 'WALRAD = ',WALRAD
           write(*,*) 'THANT  = ',THANT
           write(*,*) 'NANTYP = ',NANTYP
           write(*,*) 'FREQCY = ',FREQCY
           write(*,*) 'WNTORO = ',WNTORO
        end if
        if (output_flag > 0) then
           output_message = 'in LION.f90: Error handling not yet implemented. ABORT'
           write(0,*) output_message
           return
        endif
        !
        IF (NVERBOSE .GE. 2) write(*,*)'---- DONE: SETTING IC ANTENNA ----' 
        !
      end if
      !---------------------------------------------------------
      ! END: ADDED BY THOMAS JOHNSON (20130302)
      !---------------------------------------------------------
      !
      ! copy input values to output waves
      !
      wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power = 0._rkind
      do i=1,size(waves_in%coherent_wave)
        if (ids_is_valid( waves_in%coherent_wave(i)%global_quantities(in_time_slice)%power)) then
           if (nverbose .ge. 2) write(*,*) 'OS: in most places one assume only one coherant_wave and one n_tor, ',&
                'so not really consistent here, except makes sure at least total power considered?'
           ! Use the sum of the power in all coherent waves
           wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power = &
                wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power + &
                waves_in%coherent_wave(i)%global_quantities(in_time_slice)%power
        endif
      enddo
      if ( wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power < 1.0e-30_rkind ) then
        wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power = 1._rkind ! 1W
      endif
      !
      if (nverbose .ge. 2) write(*,*) 'OS: at this stage assume only one n_tor (and one coherent_wave)'
      allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power_n_tor(1))
      if (associated(waves_in%coherent_wave(1)%global_quantities(in_time_slice)%power_n_tor)) then
        wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power_n_tor(1) = &
             waves_in%coherent_wave(1)%global_quantities(in_time_slice)%power_n_tor(1)
      else
        wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power_n_tor(1) = 1._rkind
      end if
      !
      ! prepare 1d mesh
      ! in outp5, CCS(js)=EQ(5,.,.) used with FLUPSP(js)
      ! in EQ_from_equil, psi, chi mesh used from equil_inCOCOS2(iequil)%coord_sys%grid%dim1/dim2, if from chease, it is the same as equil%profiles_1d so could use equil_inCOCOS2(iequil)%coord_sys%grid%dim1 as 1D mesh.
      ! But since needs rho_tor as well, better to use directly  equil%profiles_1d
      !
      if (associated(equil_inCOCOS2%time_slice)) then
        npsi_out = size(equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%psi)
        allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor_norm(npsi_out))
        allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor(npsi_out))
        allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(npsi_out))
        wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(1:npsi_out) = &
             equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%psi(1:npsi_out)
        wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor(1:npsi_out) = &
             equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%rho_tor(1:npsi_out)
        wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor_norm(1:npsi_out-1) = &
             equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%rho_tor(1:npsi_out-1) / &
             equil_inCOCOS2%time_slice(in_time_slice)%profiles_1d%rho_tor(npsi_out-1)
        wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor_norm(npsi_out) = 1._RKIND
      end if
      !
    ELSE
      !
      wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power = 1._rkind ! 1W
      allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power_n_tor(1))
      wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power_n_tor(1) = 1._rkind
    END IF
    !
    allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(nrspec))
    do ispec=1,nrspec
      wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(ispec)%distribution_assumption = 0
    end do
    !
    ! Set the wave-type identifier
    allocate(wavesLION_out%coherent_wave(1)%identifier%type%name(1))
    allocate(wavesLION_out%coherent_wave(1)%identifier%type%description(1))
    wavesLION_out%coherent_wave(1)%identifier%type%index = WAVEID%IC
    wavesLION_out%coherent_wave(1)%identifier%type%name(1) = WAVEID%description(WAVEID%IC)
    wavesLION_out%coherent_wave(1)%identifier%type%description(1) = WAVEID%description(WAVEID%IC)
    if (allocated(ANTENNA_NAME)) then
      allocate(wavesLION_out%coherent_wave(1)%identifier%antenna_name(size(ANTENNA_NAME)) )
      wavesLION_out%coherent_wave(1)%identifier%antenna_name = ANTENNA_NAME
    endif
    wavesLION_out%coherent_wave(1)%identifier%index_in_antenna = 1 ! Only one wave from this antenna, thus "index=1".
    !
    wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%frequency = FREQCY
    allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%n_tor(1))
    wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%n_tor(1) = WNTORO
    allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(nrspec))
    do ispec=1,nrspec
      allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(ispec)%element(1))
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(ispec)%element(1)%z_n = acharg(ispec)
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(ispec)%element(1)%a = amass(ispec)
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(ispec)%multiple_states_flag = 0
    end do
    !
    IF ((NITMOPT.LE.0) .OR. (MOD(NITMOPT,10) .EQ. 0) .OR. (NVERBOSE .LE. 0)) NPRNT = 6
    if (NPRNT .EQ. 9) THEN
      OPEN (UNIT=NPRNT,FILE='~/tmp/fort.9')
      close(NPRNT,status='delete')
      OPEN (UNIT=NPRNT,FILE='~/tmp/fort.9')
    end if
    !
    !     SET some data related to INPUT NAMELIST FOR LION
    CALL DATA
    !
    CALL AUXVAL
    !
    ! Namelist has been updated, write to NPRNT if NVERBOSE>=2, write to output_diag if within ITM
    if (NVERBOSE .GE. 2) CALL IODSK1(2)
    IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .GE. 1) THEN
      CALL write_to_output_diag(1)
      print *,'called write_to_output_diag'
    else
      print *,'did not call write_to_output_diag, NITMOPT= ',NITMOPT
    end IF
    !
    !     read equilibrium quantities produced by CHEASE -> into EQ(.,.,.)
    IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .GE. 1) THEN
      ! use equilibrium metrics to compute EQs and "Vacuum" quantities
      call EQ_from_equil(equil_inCOCOS2,in_time_slice, output_flag, output_message)
      if (output_flag < 0) then
         write(*,*) 'in LION, error message recieved from EQ_from_equil, output_flag=',output_flag
         return
      END IF
!!$    for debugging
!!$    EQ2 = EQ
!!$    CALL iodsk1(9)
    ELSE
      ! Read EQs from MEQ file
      CALL iodsk1(9)
    END IF
!!$    for debugging
!!$  OPEN (UNIT=MEQ,FILE='~/tmp/MEQ_out_ascii_ITM',FORM='FORMATTED')
!!$  DO js=1,npsi+1
!!$    WRITE (MEQ,*)((EQ(I,J,js),I=1,MDEQ),J=1,NPOL)
!!$  END DO
!!$  CLOSE (MEQ)
!!$  OPEN (UNIT=MEQ,FILE='~/tmp/MEQ_out_ascii_file',FORM='FORMATTED')
!!$  DO js=1,npsi+1
!!$    WRITE (MEQ,*)((EQ(I,J,js),I=1,MDEQ),J=1,NPOL)
!!$  END DO
!!$  CLOSE (MEQ)
    !
    ! Adapt EQs from single S mesh to LION CS, CSM mesh
    CALL iodsk1(10)
    !
!!$  OPEN (UNIT=MEQ,FILE='~/tmp/MEQ_out_midmesh',FORM='FORMATTED')
!!$  DO js=1,npsi+1
!!$    WRITE (MEQ,*)((EQ(I,J,js),I=1,MDEQ),J=1,NPOL)
!!$  END DO
!!$  CLOSE (MEQ)
    !
    !     OPEN AND CLOSE NSOURC (NEW FILE)
    !  IF (NITMOPT.LE.0 .OR. MOD(NITMOPT,10) .EQ. 0) THEN
    CALL IODSK1(6)
    !  END IF
    !
    IF ((NITMOPT.EQ.0) .OR. (MOD(NITMOPT,10) .EQ. 0) .OR. (.NOT.  associated(equil_inCOCOS2%time_slice))) THEN
      npsi_out = npsi+1
      allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor_norm(npsi_out))
      allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor(npsi_out))
      allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(npsi_out))
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(1:npsi_out) = (/EQ(5,1,1:npsi), 1.0_rkind /)**2 * CPSRF
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor_norm = (/EQ(5,1,1:npsi), 1.0_rkind /)  !! WARNING Assigning rho_pol_norm to rho_tor_norm
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor = (/EQ(5,1,1:npsi), 1.0_rkind /)       !! WARNING Assigning rho_pol_norm to rho_tor
      ! q profile = EQ(11,1,:) / qiaxe
    END IF
    !
    !
    print *,' CEOMCI= ',CEOMCI
    print *,' BNOT= ',BNOT
    print *,' RMAJOR= ',RMAJOR
    !
    !---------------------------------------------------------------------
    !L       TOROIDAL FOURIER SPECTRUM WITH NTORSP WAVENUMBERS
    !        EQUALLY SPACED BY WNTDEL
    !
    DO JTORSP = 1, NTORSP
      !
      IF (NTORSP.GT.1) THEN
        WRITE (NPRNT,9100) JTORSP, WNTORO
      END IF

      write(6,*) ' Before VACUUM'
      call runtime (t1)
      write(6,*) ' Elapsed time so far:', t1-t0
      !
      !     COMPUTE VACUUM CONTRIBUTION, INCLUDING ANTENNA

      IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .GE. 1) THEN
        ! use equilibrium metrics to compute EQs
        ! NVAC quantities computed in EQ_from_equil(equil_inCOCOS2)
        OPEN (UNIT=NSOURC,FILE='TAPE15',STATUS='OLD', &
          &              FORM='UNFORMATTED')
      ELSE
        CALL IODSK2(1)
      END IF
      !
      ! vacuum without call iodsk2(1)
      CALL VACUUM
      write(6,*) ' After VACUUM, before ORGAN4'
      call runtime (t2)
      write(6,*) ' Elapsed time in VACUUM:', t2-t1
      !
      !     CONSTRUCT MATRIX OF THE DISCRETIZED WEAK FORM AND SOLVE IT
      CALL ORGAN4

      write(6,*) ' After ORGAN4, before DIAGNO'
      call runtime (t3)
      write(6,*) ' Elapsed time in ORGAN4:', t3-t2
      !
      !     MAKE DIAGNOSTICS OF THE SOLUTION
      CALL DIAGNO

      write(6,*) ' After DIAGNO'
      call runtime (t4)
      write(6,*) ' Elapsed time in DIAGNO:', t4-t3

      ! save in waves_out structure and SI units and interpolate on 1d mesh from center to edge
      CALL WAVES_OUT_SI
      !
      !---------------------------------------------------------------------
      !L             FREQUENCY TRACE WITH NRUN FREQUENCIES EQUALLY
      !              SPACED BY DELTAF
      !
      IF (NRUN.GT.1) THEN
        !
        DO JRUN=2,NRUN
          !
          !     INCREMENT THE FREQUENCY (FREQCY) BY DELTAF
          ZOLDFR = FREQCY
          FREQCY = FREQCY + DELTAF
          !
          !     RESCALE THE NORMALIZED FREQUENCY (OMEGA)
          OMEGA  = OMEGA * FREQCY / ZOLDFR
          !
          !     PRINTOUT NEW FREQUENCIES
          WRITE (NPRNT,9200) JRUN, FREQCY, OMEGA
          !
          !     SWITCH OFF FREQUENCY-INDEPENDANT OUTPUT
          NLOTP5(25) = .FALSE.
          NLOTP5(26) = .FALSE.
          !lvC
          !lvC     OPEN NSAVE, REWRITE NAMLIST NEWRUN, AND CLOSE NSAVE
          !lv               CALL IODSK1(1)
          !lv               CALL IODSK1(2)
          !lv               CALL IODSK1(3)
          !
          !     RESCALE THE SOURCE VECTOR (SOURCE)
          CALL IODSK1 (4)
          DO JPOL=1,NPOL
            SOURCE(JPOL) = SOURCE(JPOL) * FREQCY / ZOLDFR
          END DO
          CALL IODSK1 (5)


          call runtime (t5)
          !
          !     CONSTRUCT MATRIX OF THE DISCRETIZED WEAK FORM AND SOLVE IT

          CALL ORGAN4
          write(6,9001) jrun
9001      format(' Frequency scan, JRUN=',i5,' after ORGAN4')
          call runtime (t6)
          !
          !     MAKE DIAGNOSTICS OF THE SOLUTION
          CALL DIAGNO
          !
        END DO
        !
      END IF
      !
      !---------------------------------------------------------------------
      !
      IF (JTORSP.LT.NTORSP) THEN

        !      INCREMENT WNTORO AND REWRITE NAMELIST ON NSAVE
        WNTORO = WNTORO + WNTDEL
        !
        !lvC     OPEN NSAVE, REWRITE NAMLIST NEWRUN, AND CLOSE NSAVE
        !lv               CALL IODSK1(1)
        !lv               CALL IODSK1(2)
        !lv               CALL IODSK1(3)
        !
      END IF
      !
    END DO ! JTORSP

    !
    !     close and delete NSOURC
!!$  IF (NITMOPT.LE.0 .OR. MOD(NITMOPT,10) .EQ. 0) THEN
    CALL IODSK1(8)
!!$  END IF
    !
  END DO
  !
  allocate(wavesLION_out%code%output_flag(1))
  wavesLION_out%code%output_flag(1) = 0
  waves_out = wavesLION_out
  !
  call free_internal_core_profiles(internal_profs)
  !
  write(6,*) ' At end of run'
  call runtime (tend)
  write(6,*) ' Total elapsed time in LION:', tend-t0
  !
  if (NPRNT .EQ. 9) THEN
    close(NPRNT)
  end if
  !
  !---------------------------------------------------------------------
9100 FORMAT (//,1X,'***************************************',/, &
    &              1X,'TOROIDAL WAVE NUMBER SCAN -- RUN NO.',I3,/, &
    &              1X,'***************************************',/, &
    &              1X,'TOROIDAL WAVE NUMBER =',1PE13.3,/)
9200 FORMAT (///,1X,'******************************',/, &
    &               1X,'FREQUENCY TRACE --- RUN NO.',I3,/, &
    &               1X,'******************************',//, &
    &               1X,'FREQUENCY            =',1PE15.8,'  HZ',/, &
    &               1X,'NORMALIZED FREQUENCY =',1PE15.8,/)
  !---------------------------------------------------------------------
  RETURN
END SUBROUTINE LION


module lion_iwrap

  contains

SUBROUTINE LION_IMAS(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
  use ids_schemas
  use globals, only: NITMOPT
  ! Input
  type(ids_equilibrium),      intent(in)  :: equil_in
  type(ids_core_profiles),    intent(in)  :: coreprof_in
  type(ids_ic_antennas),      intent(in)  :: antennas_in
  type(ids_waves),            intent(in)  :: waves_in
  type(ids_waves),            intent(out) :: waves_out ! intent(out) kills any allocation, so better to avoid
  type(ids_parameters_input), intent(in)  :: param_code
  integer,                    intent(out) :: output_flag
  character(len=:), pointer,  intent(out) :: output_message
  !
  ! Internal
  character(len=:), allocatable :: internal_message

  INTERFACE
     SUBROUTINE LION(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,output_message)
       use ids_schemas
       type(ids_equilibrium),         intent(in)    :: equil_in
       type(ids_core_profiles),       intent(in)    :: coreprof_in
       type(ids_ic_antennas),         intent(in)    :: antennas_in
       type(ids_waves),               intent(in)    :: waves_in
       type(ids_waves),               intent(inout) :: waves_out ! intent(out) kills any allocation, so better to avoid
       type(ids_parameters_input),    intent(in)    :: param_code
       integer,                       intent(out)   :: output_flag
       character(len=:), allocatable, intent(out)   :: output_message
     END SUBROUTINE LION
  END INTERFACE

  NITMOPT = 22

  call LION(equil_in,coreprof_in,antennas_in,waves_in,waves_out,param_code,output_flag,internal_message)

  if (allocated(internal_message)) then
     allocate(character(len=len(internal_message)) :: output_message)
     output_message = internal_message
  end if

END SUBROUTINE LION_IMAS

end module lion_iwrap
