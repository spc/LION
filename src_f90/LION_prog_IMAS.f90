program LION_prog
  !------------------------------------------------------------------
  ! Wrapper program for LION routine to be called with equil_in, xxx_in, waves_out and codeparam for parameters
  ! It should allow to run LION independently from the ITM structure, gateway or with libraries 
  ! allowing read/write from ITM structure.
  ! If LION is called from Kepler. Then this program is not run and LION routine directly called with NITMOPT=22
  ! Similar to CHEASE
  !
  ! Thus the namelist should be read here and then if needed the equil_in and other input CPOs
  !------------------------------------------------------------------
  !
  use lion_iwrap
  use globals    ! needed here since keep option read(..,newrun) namelist for backward compatibility
  use ids_schemas                       ! module containing the equilibrium type definitions
  ! use ids_routines

  IMPLICIT NONE

  INCLUDE 'NEWRUN.inc'

  !
  interface 

     subroutine assign_code_parameters(codeparameters, return_status)
       use prec_const
       use ids_schemas
       implicit none
       type (ids_parameters_input), intent(in) :: codeparameters
       integer(ikind), intent(out) :: return_status 
     end subroutine assign_code_parameters

     subroutine load_itm(equil_in,kitmopt,kitmshot,kitmrun,citmtree)
       use globals
       use ids_schemas                       ! module containing the equilibrium type definitions
       !avoid for no ITM compatibility       use ids_routines
       IMPLICIT NONE
       type(ids_equilibrium)      :: equil_in
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine load_itm

     subroutine write_itm(equilibrium_out,waves_out,kitmopt,kitmshot,kitmrun,citmtree)
       use globals
       use ids_schemas                       ! module containing the waves type definitions
       !       use ids_routines
       IMPLICIT NONE
       type(ids_equilibrium)      :: equilibrium_out
       type(ids_waves)      :: waves_out
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine write_itm

     subroutine load_itm_coreprof(coreprof_out,kitmopt,kitmshot,kitmrun,citmtree)
       !
       use globals
       use ids_schemas                       ! module containing the core_profiles type definitions
       ! use ids_routines
       IMPLICIT NONE
       type(ids_core_profiles)      :: coreprof_out
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine load_itm_coreprof
     subroutine load_itm_antennas(antennas_out,kitmopt,kitmshot,kitmrun,citmtree)
       !
       use globals
       use ids_schemas                       ! module containing the antennas type definitions
       ! use ids_routines
       IMPLICIT NONE
       type(ids_ic_antennas)      :: antennas_out
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine load_itm_antennas
     subroutine load_itm_waves(waves_out,kitmopt,kitmshot,kitmrun,citmtree)
       !
       use globals
       use ids_schemas                       ! module containing the waves type definitions
       ! use ids_routines
       IMPLICIT NONE
       type(ids_waves)      :: waves_out
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine load_itm_waves
  end interface
  !
  type(ids_equilibrium) :: equil_in
  type(ids_core_profiles)    :: coreprof_in
  type(ids_ic_antennas)    :: antennas_in
  type(ids_waves)       :: waves_in
!!$  type(ids_equilibrium),  SAVE ::              eqchease_in
  type(ids_waves)       :: waves_out
  type(ids_parameters_input)       :: codeparam_param
  integer               :: itmopt, i

  character(len = 132), target :: codename(1) = 'LION'
  character(len = 132), target :: codeversion(1) = 'version 1_2'

  character(len = 132), allocatable :: parameters(:)
  target :: parameters
  character(len = 132) :: xml_line
  character(len=132) :: file_xml_schema
  integer(ikind) :: file_length, n_lines, ios, ios2, INXML, istatus, INAMELIST
  logical :: ex_dp, ex_fdf, ex_j_tor
  CHARACTER(len=120), DIMENSION(2) ::  treeitm     ='ids'
  real(rkind) :: t0

  !*********************************************************************
  !
  !
  ! Initialize NITMOPT depending on local installation:
  ! NITMOPT = -1: if no itm stuff/libraries installed (default)
  ! NITMOPT =  0: if linked with ITM/mds modules (so can define ITM shot in namelist)

  NITMOPT = 0

  ! CALL runtime (t0)
  !
  !     SET DEFAULT VAUES
  CALL PRESET
  !
  ! if itmopt<0 allows to force negative value independent of namelist value later
  itmopt = nitmopt

  !*********************************************************************
  ! Read namelist in xml format in file LION_input.xml
  ! or can read namelist as former namelist BUT in file: LION_namelist
  !

  !-- read LION_input.xml
  INXML = 12
  OPEN (unit = INXML, file = "LION_input.xml", status = 'old', &
       action = 'read', iostat = ios)
  print *,'opened LION_input.xml'

  IF (ios /= 0) THEN    
     IF (NVERBOSE .GE. 0) write(0,*) ' WARNING:  ''LION_input.xml'' file missing,'
     IF (NVERBOSE .GE. 0) write(0,*) '            now will see if namelist given in file: ''LION_namelist'''
     INAMELIST = 13
     OPEN (unit = INAMELIST, file = "LION_namelist", status = 'old', &
          action = 'read', iostat = ios2)
     IF (ios2 /= 0) THEN
        IF (NVERBOSE .GE. 0) THEN
          write(0,*) ' ERROR: ''LION_namelist'' does not exist either'
          write(0,*) ' Note: LION has been modified to allow for xml type inputs,'
          write(0,*) '       therefore input data should be given through files.'
          write(0,*) ' LION accepts xml type input in file ''LION_input.xml'' or '
          write(0,*) '        namelist type as before but in file named ''LION_namelist'' '
          write(0,*) ' Thus copy your input datafile to the file ''LION_namelist'''
          write(0,*) '      and then run LION with just the command: LION'
        END IF
        stop
     END IF
     rewind(INAMELIST)
     rewind(INAMELIST)
     rewind(INAMELIST)
     READ (INAMELIST,1000) LABEL1
     comments(1) = LABEL1
     READ (INAMELIST,1000) LABEL2
     comments(2) = LABEL2
     READ (INAMELIST,1000) LABEL3
     comments(3) = LABEL3
     READ (INAMELIST,1000) LABEL4
     comments(4) = LABEL4
     rewind(INAMELIST)
1000 FORMAT(A)

     READ (INAMELIST,NEWRUN)
     close(INAMELIST)
     IF (NVERBOSE .GE. 1) THEN
       CALL MESAGE('R O G R A M   L I O N - F A S T - 90 - V 1_1')
       CALL BLINES(10)
       CALL MESAGE(LABEL1)
       CALL BLINES(1)
       CALL MESAGE(LABEL2)
       CALL BLINES(1)
       CALL MESAGE(LABEL3)
       CALL BLINES(1)
       CALL MESAGE(LABEL4)
     END IF
     if (associated(codeparam_param%parameters_value)) deallocate(codeparam_param%parameters_value)
     ! write(*,NEWRUN)
     IF (NVERBOSE .GE. 1) PRINT *,' namelist read from file LION_namelist'
     IF (NVERBOSE .GE. 1) PRINT *,'nitmshot= ',nitmshot
  ELSE
     REWIND(INXML)
     IF (NVERBOSE .GE. 1) PRINT *,' namelist will be taken from file LION_input.xml'
     n_lines = 0
     DO
        READ (INXML, '(a)', iostat = ios) xml_line
        if (ios == 0) then
           n_lines = n_lines + 1
        else
           exit
        end if
     END DO
     rewind INXML
     allocate(parameters(n_lines))
     do i = 1, n_lines
        read (INXML, '(a)', iostat = ios) parameters(i)
     end do
     close(INXML)
     !  PRINT *,'parameters'
     !  PRINT *,parameters(:)

     !-- assign code parameters to internal variables defined in globals
     allocate(codeparam_param%parameters_value(n_lines))
     codeparam_param%parameters_value = parameters
     allocate(codeparam_param%parameters_default(n_lines))
     codeparam_param%parameters_default = parameters
     !
     ! need also schemas in codeparam_param%schema now
     !
     ! file_xml_schema = 'LION_schema.xml'
     OPEN (unit = INXML, file = "LION_schema.xsd", status = 'old', &
          action = 'read', iostat = ios)
     n_lines = 0
     DO
        READ (INXML, '(a)', iostat = ios) xml_line
        if (ios == 0) then
           n_lines = n_lines + 1
        else
           exit
        end if
     END DO
     rewind INXML
     deallocate(parameters)
     allocate(parameters(n_lines))
     do i = 1, n_lines
        read (INXML, '(a)', iostat = ios) parameters(i)
     end do
     close(INXML)
     ! PRINT *,'parameters'
     ! PRINT *,parameters(:)
     allocate(codeparam_param%schema(n_lines))
     codeparam_param%schema = parameters
     IF (NVERBOSE .GE. 2) print *,codeparam_param%parameters_value
     call assign_code_parameters(codeparam_param,istatus)

     IF (NVERBOSE .GE. 3) print *,'FREQCY= ',FREQCY
     IF (NVERBOSE .GE. 3) print *,'THANT= ',THANT
     IF (NVERBOSE .GE. 3) print *,'NLOTP5= ',NLOTP5
     if (istatus /= 0) then
       IF (NVERBOSE .GE. 0) write(*, *) 'ERROR: Could not assign some code parameters.'
        stop 'assign_code_parameters'
     end if

  end if

  if (itmopt .lt. 0) then
     ! LION not compiled with itm libraries, so keep itmopt=-1
     NITMOPT = itmopt
  end if

  !
  !   fetch equilibrium from ITM database if required
  !
  IF (NVERBOSE .GE. 1) print *,'NITMOPT= ',NITMOPT
  IF (NVERBOSE .GE. 1) print *,'NITMSHOT= ',NITMSHOT
  !  if (associated(eqchease_in)) deallocate(eqchease_in)
  IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .EQ. 1) THEN
    !     if ( ids_is_valid_int(equil_in%ids_properties%homogeneous_time)) then
    if ( (equil_in%ids_properties%homogeneous_time .EQ. 0) .or. (equil_in%ids_properties%homogeneous_time .EQ. 1)) then
      ! assume equil_in already loaded
    else
      IF (NVERBOSE .GE. 1) PRINT *,'load with treeitm(1), nitmshot(1), nitmrun(1)= ', &
        & trim(treeitm(1)), nitmshot(1), nitmrun(1)
      CALL LOAD_ITM(equil_in,nitmopt,nitmshot(1),nitmrun(1),treeitm(1))
       ! Also load coreprof
       CALL LOAD_ITM_coreprof(coreprof_in,nitmopt,nitmshot(1),nitmrun(1),treeitm(1))
       ! Also load antennas
       CALL LOAD_ITM_antennas(antennas_in,nitmopt,nitmshot(1),nitmrun(1),treeitm(1))
       ! Also load waves
       CALL LOAD_ITM_waves(waves_in,nitmopt,nitmshot(1),nitmrun(1),treeitm(1))
     end if
     !
     IF (NVERBOSE .GE. 2) PRINT *,'salut1, size(equil_in%time)= ',size(equil_in%time)
     ! allocate(eqchease_in(1))
     ! call ids_copy_slice2slice_equilibrium(equil_in(2),eqchease_in(1))

!!$     eqchease_in => equil_in

     IF ((NVERBOSE .GE. 2) .and. (equil_in%ids_properties%homogeneous_time .EQ. 1) .and. (size(equil_in%time)>0)) write(*,*) 'equil_in%time(1) = ',equil_in%time(1)
     write(*,*) 'equil_in%time_slice(1)%boundary%geometric_axis%r = ',equil_in%time_slice(1)%boundary%geometric_axis%r
     write(*,*) 'equil_in%vacuum_toroidal_field%r0 = ',equil_in%vacuum_toroidal_field%r0
     write(*,*) 'equil_in%vacuum_toroidal_field%b0 = ',equil_in%vacuum_toroidal_field%b0
     write(*,*) 'equil_in%ids_properties%comment(:) = ',equil_in%ids_properties%comment(:)
     if (size(equil_in%time_slice(1)%boundary%outline%r)>0) then
        ! PRINT *,' equil_in%time_slice(1)%boundary%outline%r(1:2)= ',equil_in%time_slice(1)%boundary%outline%r(1:2)
     else
       IF (NVERBOSE .GE. 0) PRINT *,'equil_in%time_slice(1)%boundary%outline%r not associated'
       stop 'equil_in%time_slice(1)%boundary%outline%r'
     end if

     ! print *,'size(equil_in(1)%codeparam%parameters_value(:)) then equil_in(1)%codeparam%parameters_value(:)', &
     !      size(equil_in(1)%codeparam%parameters_value(:))
     ! print *,equil_in(1)%codeparam%parameters_value(:)
     ! print *,'associated(codeparam_param%parameters_value)= ',associated(codeparam_param%parameters_value)
     if (associated(codeparam_param%parameters_value)) then
       equil_in%code%parameters => codeparam_param%parameters_value
        ! do i=1,size(equil_in%code%parameters)
        !    print *,equil_in%code%parameters(i)
        ! end do
     else
       IF (NVERBOSE .GE. 0) write(0,*) ' parameters not associated'
     end if
  else
    IF (NVERBOSE .GE. 1) PRINT *,'equilibrium not taken from ITM structure, thus equil_in not allocated'
  end IF
  !
  !
  call LION(equil_in,coreprof_in,antennas_in,waves_in,waves_out,codeparam_param)
  !
  ! write data to ITM structure if needed
  !
  IF (NITMOPT .GE. 10) THEN
    IF (NVERBOSE .GE. 1) PRINT *,'write with treeitm(2), nitmshot(2), nitmrun(2)= ', &
      & trim(treeitm(2)), nitmshot(2), nitmrun(2)
    CALL WRITE_ITM(equil_in,waves_out,nitmopt,nitmshot(2),nitmrun(2),treeitm(2))
  end IF
  !
end program LION_prog
