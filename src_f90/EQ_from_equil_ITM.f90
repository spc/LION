SUBROUTINE EQ_from_equil(equil_in_slice, error_flag)
  !
  ! Compute EQs from equil_in CPO
  !
  ! Use equil_metric_derivatives(equil_in,d_ddim1,d_ddim2,d_dsqrtdim1norm) to compute metrics derivatives
  !
  use itm_types
  USE globals
  USE interpos_module
  use euITM_schemas                       ! module containing the equilibrium type definitions
  !
  IMPLICIT NONE
  !
  type(type_equilibrium), intent(in)      :: equil_in_slice
  integer,                intent(out)     :: error_flag
  !
  interface
     !
     SUBROUTINE equil_metric_derivatives(equil_in,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag)
       use itm_types
       USE interpos_module
       use euITM_schemas                       ! module containing the equilibrium type definitions
       type(type_equilibrium), intent(in) :: equil_in
       type(type_coord_sys), pointer , intent(out) :: d_ddim1(:), d_ddim2(:), d_dsqrtdim1norm(:)
       integer, intent(out) :: error_flag
     end SUBROUTINE equil_metric_derivatives
     !
     subroutine write_itm2(equilibrium_out,kitmopt,kitmshot,kitmrun,citmtree)
       use globals
       use euITM_schemas                       ! module containing the equilibrium type definitions
       !       use euITM_routines
       IMPLICIT NONE
       type(type_equilibrium),pointer      :: equilibrium_out(:)
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun
     end subroutine write_itm2
     SUBROUTINE COCOS(KCOCOS,Kexp_Bp,Ksigma_Bp,Ksigma_RphiZ,Ksigma_rhothetaphi,Ksign_q_pos,Ksign_pprime_pos)
       !
       ! return values of exp_Bp, sigma_Bp, sigma_rhothetaphi, sign_q_pos, sign_pprime_pos
       ! from the input value KCOCOS and according to O. Sauter and S. Yu. Medvevdev paper and Table I
       ! (see paper in chease directory)
       !
       IMPLICIT NONE
       INTEGER, intent(in) :: KCOCOS
       INTEGER, intent(out) :: Kexp_Bp, Ksigma_Bp, Ksigma_RphiZ, Ksigma_rhothetaphi, Ksign_q_pos, Ksign_pprime_pos
       !
       ! cocos=i or 10+i have similar coordinate conventions except psi/2pi for cocos=i and psi for cocos=10+i
       !
     end SUBROUTINE COCOS
  end interface
  !
  type(type_coord_sys), pointer :: d_ddim1(:), d_ddim2(:), d_dsqrtdim1norm(:)
  !
  real(R8) :: tens_def, abspsimin, R0EXP, B0EXP, mu0, ZZ, zchi_a, zsign_psi, zrmag, zzmag, zgamma
  real(R8), allocatable :: sqrtdim1norm(:), dq_dsqrtdim1norm(:)
  real(R8), allocatable :: thetastraight(:,:), lnR2oJ(:,:), dlnR2oJ_ddim1(:,:), dlnR2oJ_ddim2(:,:), zz2(:), &
    & dim2plus(:), ZTETA(:), ZRHO(:)
  integer :: nbequil, iequil, idim1, idim2, inum
  integer :: iexp_Bp, isigma_Bp, isigma_RphiZ, isigma_rhothetaphi, isign_q_pos, isign_pprime_pos
  integer, allocatable :: ndim1(:), ndim2(:)
  character*120  :: citmtree='euitm'
  !  
  nbequil = 1
  iequil = 1
  if (nbequil .lt. 1) then
    print *,' problem in equil_metric_derivatives with size of equil_in = ', nbequil
    return
  end if
  allocate(ndim1(nbequil))
  allocate(ndim2(nbequil))
  !
  ! Compute metrics derivatives
  !
  call equil_metric_derivatives(equil_in_slice,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag)
  if (error_flag < 0) then
     write(0,*)'in EQ_from_equil: error recieved from equil_metric_derivatives'
  endif
  tens_def = -0.1_R8
  !
  ! Use only 1st equil_in
  !
  ndim1(iequil) = size(equil_in_slice%coord_sys%grid%dim1)
  ndim2(iequil) = size(equil_in_slice%coord_sys%grid%dim2)
  !
  cocos_in = equil_in_slice%datainfo%cocos
  CALL COCOS(COCOS_IN,iexp_Bp,isigma_Bp,isigma_RphiZ,isigma_rhothetaphi,isign_q_pos,isign_pprime_pos)
  ! To have increasing psi (dim1) we need: d|psi-psi_axis| = sigma_Ip sigma_Bp dpsi
  zsign_psi = real(isigma_Bp * sign(1._R8,equil_in_slice%global_param%i_plasma),R8)
  !
  ! Values for normalization
  abspsimin = abs(equil_in_slice%coord_sys%grid%dim1(ndim1(1))-equil_in_slice%coord_sys%grid%dim1(1))
  mu0 = 2.e-07_R8 * twopi
  R0EXP = equil_in_slice%eqgeometry%geom_axis%r
  B0EXP = equil_in_slice%global_param%toroid_field%b0 * equil_in_slice%global_param%toroid_field%r0 / R0EXP
  R0EXP = equil_in_slice%global_param%mag_axis%position%r
  B0EXP = equil_in_slice%global_param%mag_axis%bphi
  !
  ! Assume equil_in CPO has psi, chi as coord_sys, thus first check
  ! Should know from grid_type, but not yet sure so check with profiles_1d.psi
  zz = sum(abs(equil_in_slice%coord_sys%grid%dim1-equil_in_slice%profiles_1d%psi))
  if (zz .gt. 1.E-4*abs(equil_in_slice%coord_sys%grid%dim1(1)+equil_in_slice%coord_sys%grid%dim1(ndim1(1)))) then
    print *,' It seems equil_in_slice%coord_sys%grid%dim1 is not psi, since different from equil_in_slice%profiles_1d%psi'
    print *,'equil_in_slice%coord_sys%grid%dim1 = ',equil_in_slice%coord_sys%grid%dim1
    print *,'equil_in_slice%profiles_1d%psi = ',equil_in_slice%profiles_1d%psi
    return
  end if
  !
  ! Pre-compute some arrays
  !
  allocate(sqrtdim1norm(ndim1(1)))
  allocate(dq_dsqrtdim1norm(ndim1(1)))
  sqrtdim1norm = sqrt((equil_in_slice%coord_sys%grid%dim1-equil_in_slice%coord_sys%grid%dim1(1))/(equil_in_slice%coord_sys%grid%dim1(ndim1(1))-equil_in_slice%coord_sys%grid%dim1(1)))
  !
  ! straight-field line coordinate thetastar = 1/q int(0,chi) F_dia Jacobian_psichiphi/R^2 dchi
  ! chi being dim2
  allocate(thetastraight(ndim1(1),ndim2(1)))
  do idim1=1,ndim1(1)
    call interpos(equil_in_slice%coord_sys%grid%dim2(1:ndim2(1)), &
      & equil_in_slice%profiles_1d%F_dia(idim1) *equil_in_slice%coord_sys%jacobian(idim1,1:ndim2(1))/equil_in_slice%coord_sys%position%r(idim1,1:ndim2(1))**2, &
      & ndim2(1),tension=tens_def, youtint=thetastraight(idim1,1:ndim2(1)),nbc=-1,ybc=twopi)
    thetastraight(idim1,1:ndim2(1)) = thetastraight(idim1,1:ndim2(1)) / equil_in_slice%profiles_1d%q(idim1)
  end do
  !
  ! d ln(R^2/J)
  !
  allocate(lnR2oJ(ndim1(1),ndim2(1)))
  allocate(dlnR2oJ_ddim1(ndim1(1),ndim2(1)))
  allocate(dlnR2oJ_ddim2(ndim1(1),ndim2(1)))
  allocate(zz2(ndim2(1)))
  DO idim1=1,ndim1(1)
    EQ(19,1:ndim2(1),idim1) = equil_in_slice%coord_sys%position%r(idim1,1:ndim2(1))**2 / equil_in_slice%coord_sys%jacobian(idim1,1:ndim2(1))/B0EXP/R0EXP
    lnR2oJ(idim1,1:ndim2(1)) = log(EQ(19,1:ndim2(1),idim1))
    call interpos(equil_in_slice%coord_sys%grid%dim2(1:ndim2(1)),lnR2oJ(idim1,1:ndim2(1)),ndim2(1),tension=tens_def, &
        & youtp=dlnR2oJ_ddim2(idim1,1:ndim2(1)),nbc=-1,ybc=twopi)
  END DO
  DO idim2=1,ndim2(1)
    call interpos(zsign_psi*equil_in_slice%coord_sys%grid%dim1, &
      & zsign_psi*lnR2oJ(1:ndim1(1),idim2),ndim1(1),tension=tens_def, &
      & youtp=dlnR2oJ_ddim1(1:ndim1(1),idim2),nbc=(/2,2/),ybc=(/lnR2oJ(1,idim2), lnR2oJ(ndim1(1),idim2)/))
  END DO
  !
  ! dim2 related. Cannot find exact EQ(2) and (4) but assume almost equidistant near chi(1)=-delta
  zchi_a = (twopi - equil_in_slice%coord_sys%grid%dim2(ndim2(1))) / 2._R8
  EQ(2,1,1:ndim1(1)) = -zchi_a;
  EQ(4,ndim2(1),1:ndim1(1)) = twopi - zchi_a;
  EQ(6,1,1:ndim1(1)) = equil_in_slice%coord_sys%grid%dim2(1)
  DO idim2=2,ndim2(1)
    ! chim has been put in dim2
    EQ(6,idim2,1:ndim1(1)) = equil_in_slice%coord_sys%grid%dim2(idim2)
    EQ(2,idim2,1:ndim1(1)) = 2._R8*EQ(6,idim2-1,1:ndim1(1)) - EQ(2,idim2-1,1:ndim1(1))
    EQ(4,idim2-1,1:ndim1(1)) = EQ(2,idim2,1:ndim1(1))
  END DO
  !
  ! dim1 related
  ZGAMMA     = 5._R8 / 3._R8
  DO idim2=1,ndim2(1)
    EQ(1,idim2,1:ndim1(1)) = sqrtdim1norm(1:ndim1(1))
    EQ(3,idim2,1:ndim1(1)-1) = sqrtdim1norm(2:ndim1(1))
    EQ(5,idim2,1:ndim1(1)-1) = 0.5_R8*(sqrtdim1norm(1:ndim1(1)-1) + sqrtdim1norm(2:ndim1(1)))
    EQ(5,idim2,ndim1(1)) = sqrtdim1norm(ndim1(1))
    EQ(8,idim2,1:ndim1(1)) = zgamma*equil_in_slice%profiles_1d%pressure/equil_in_slice%profiles_1d%q(1)/abspsimin*mu0*R0EXP**2/B0EXP
    EQ(9,idim2,1:ndim1(1)) = equil_in_slice%profiles_1d%F_dia(1:ndim1(1)) / R0EXP / B0EXP
    ! 10: free but seems psi was in there, good to have
!!$    EQ(10,idim2,1:ndim1(1)) = equil_in_slice%coord_sys%grid%dim1
    EQ(11,idim2,1:ndim1(1)) = equil_in_slice%profiles_1d%q(1:ndim1(1)) / equil_in_slice%profiles_1d%q(1)
    !
    ! NOTE: EQ(12,j,k) has an apparent singularity at r->0, i.e. for k=1
    EQ(12,idim2,2:ndim1(1)) = equil_in_slice%coord_sys%grid%dim1(2:ndim1(1)) * equil_in_slice%coord_sys%position%r(2:ndim1(1),idim2)**2 &
      & / equil_in_slice%profiles_1d%q(1) / equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2) / R0EXP**2 * B0EXP
    !
    ! NOTE: EQ(13,j,k) has an apparent singularity at r->0, i.e. for k=1,2
    EQ(13,idim2,2:ndim1(1)) = 2._R8 * sqrtdim1norm * abspsimin * equil_in_slice%coord_sys%g_12(2:ndim1(1),idim2) &
      & / equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2)
    !
    EQ(14,idim2,1:ndim1(1)) = equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2)**2 / R0EXP**2
    EQ(15,idim2,1:ndim1(1)) = 2._R8 / equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2) * d_dsqrtdim1norm(1)%position%r(1:ndim1(1),idim2)
    EQ(16,idim2,1:ndim1(1)) = 2._R8 / equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2) * d_ddim2(1)%position%r(1:ndim1(1),idim2)
    EQ(27,idim2,1:ndim1(1)) = - (equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2)*mu0*equil_in_slice%profiles_1d%pprime(1:ndim1(1)) &
      & + equil_in_slice%profiles_1d%ffprime(1:ndim1(1)) / equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2)) * R0EXP / B0EXP
    !
    ! d/d_|n = d/ddim1_at_cst_dim2 + g_12/g_11 d/ddim2_at_cst_dim1
    ! NOTE: EQ(28,j,k) has a singularity at r->0, i.e. at k->1
    EQ(28,idim2,2:ndim1(1)) = 0.5_R8 / equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2) * (d_ddim1(1)%g_11(2:ndim1(1),idim2) + &
      & equil_in_slice%coord_sys%g_12(2:ndim1(1),idim2) / equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2) * d_ddim2(1)%g_11(2:ndim1(1),idim2)) * R0EXP**2 * B0EXP
    !
    EQ(29,idim2,1:ndim1(1)) = dlnR2oJ_ddim1(1:ndim1(1),idim2) * R0EXP**2 * B0EXP
!!$    EQ(29,idim2,1:ndim1(1)) = 2._R8 / equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2) * &
!!$      & d_ddim1(1)%position%r(1:ndim1(1),idim2)*R0EXP**2 * B0EXP - &
!!$      & R0EXP**2 * B0EXP / equil_in_slice%coord_sys%jacobian(1:ndim1(1),idim2) * d_ddim1(1)%jacobian(1:ndim1(1),idim2)
    !
    ! NOTE: EQ(17,j,k) has a singularity at r->0, i.e. for k=1
    EQ(17,idim2,2:ndim1(1)) = - 2._R8 * sqrtdim1norm(2:ndim1(1)) * abspsimin/R0EXP**2/B0EXP * (equil_in_slice%coord_sys%position%r(2:ndim1(1),idim2) * &
      & EQ(27,idim2,2:ndim1(1))/equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2)*R0EXP*B0EXP**2 - EQ(29,idim2,2:ndim1(1)))
    !
    ! NOTE: EQ(18,j,k) has an apparent singularity at r->0, i.e. for k=1
    EQ(18,idim2,2:ndim1(1)) = 2._R8*equil_in_slice%coord_sys%grid%dim1(2:ndim1(1))/R0EXP**2/B0EXP/equil_in_slice%profiles_1d%q(1) * ( &
      & EQ(27,idim2,2:ndim1(1))**2/equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2)*R0EXP**2 * B0EXP**2 &
      & - EQ(27,idim2,2:ndim1(1))/equil_in_slice%coord_sys%position%r(2:ndim1(1),idim2)*R0EXP*EQ(28,idim2,2:ndim1(1)) &
      & - equil_in_slice%profiles_1d%pprime(2:ndim1(1))*mu0*R0EXP**4*(d_ddim1(1)%position%r(2:ndim1(1),idim2) &
      &   + equil_in_slice%coord_sys%g_12(2:ndim1(1),idim2) &
      & / equil_in_slice%coord_sys%g_11(2:ndim1(1),idim2) * d_ddim2(1)%position%r(2:ndim1(1),idim2)) )
    !
    EQ(20,idim2,1:ndim1(1)) = dlnR2oJ_ddim2(1:ndim1(1),idim2)
    call interpos(sqrtdim1norm,equil_in_slice%profiles_1d%q(1:ndim1(1))*thetastraight(1:ndim1(1),idim2),ndim1(1),tension=tens_def, &
      & youtp=EQ(21,idim2,1:ndim1(1)),nbc=(/2,2/), &
      & ybc=(/equil_in_slice%profiles_1d%q(1)*thetastraight(1,idim2), equil_in_slice%profiles_1d%q(ndim1(1))*thetastraight(ndim1(1),idim2)/))
    EQ(23,idim2,1:ndim1(1)) = d_ddim2(1)%g_11(1:ndim1(1),idim2) / R0EXP**2 / B0EXP**2
    EQ(24,idim2,1:ndim1(1)) = thetastraight(1:ndim1(1),idim2)
    EQ(26,idim2,1:ndim1(1)) = equil_in_slice%profiles_1d%ffprime(1:ndim1(1)) / equil_in_slice%profiles_1d%F_dia(1:ndim1(1)) * R0EXP
  END DO
  do idim2=1,ndim2(1)-1
    ! 25: chistraight at ichi+1
    EQ(25,idim2,1:ndim1(1)) = EQ(24,idim2+1,1:ndim1(1))
  end do
  EQ(25,ndim2(1),1:ndim1(1)) = twopi
  ! Special case for EQ(21):
  call interpos(sqrtdim1norm,equil_in_slice%profiles_1d%q(1:ndim1(1)),ndim1(1),tension=tens_def, &
      & youtp=dq_dsqrtdim1norm(1:ndim1(1)),nbc=(/2,2/), &
      & ybc=(/equil_in_slice%profiles_1d%q(1), equil_in_slice%profiles_1d%q(ndim1(1))/))
  if (mod(ndim2(1),2) .EQ. 0) then
    do idim2=ndim2(1)/2+2,ndim2(1)
      EQ(21,idim2,1:ndim1(1)) = EQ(21,idim2,1:ndim1(1)) - twopi * dq_dsqrtdim1norm(1:ndim1(1))
    END do
  else
    do idim2=(ndim2(1)+1)/2+2,ndim2(1)
      EQ(21,idim2,1:ndim1(1)) = EQ(21,idim2,1:ndim1(1)) - twopi * dq_dsqrtdim1norm(1:ndim1(1))
    END do
  end if
  ! 22 not used but fill in while checking. It is OK
  do idim2=1,ndim2(1)
    call interpos(sqrtdim1norm(1:ndim1(1)),thetastraight(1:ndim1(1),idim2),ndim1(1),tension=tens_def, &
      & youtp=EQ(22,idim2,1:ndim1(1)),nbc=(/2,2/), ybc=(/thetastraight(1,idim2), thetastraight(ndim1(1),idim2)/))
    EQ(22,idim2,1:ndim1(1)) = EQ(22,idim2,1:ndim1(1)) + &
      & EQ(13,idim2,1:ndim1(1)) * equil_in_slice%profiles_1d%F_dia(1:ndim1(1)) &
      & / equil_in_slice%profiles_1d%q(1:ndim1(1)) * equil_in_slice%coord_sys%jacobian(1:ndim1(1),idim2) &
      & / equil_in_slice%coord_sys%position%r(1:ndim1(1),idim2)**2
  END do
  !
  EQ(7,1:ndim2(1),1:ndim1(1)) = 1.0_R8
  !
  ! Values on axis
  !
  DO idim2=1,ndim2(1)
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(12,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(12,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(12,idim2,2), EQ(12,idim2,ndim1(1))/))
    !
    ! NOTE: For EQ(13,*,*) we need to fill both EQ(13,*,1) and EQ(13,*,2)
    call interpos(EQ(1,idim2,3:ndim1(1)),EQ(13,idim2,3:ndim1(1)),ndim1(1)-2,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(13,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(13,idim2,3), EQ(13,idim2,ndim1(1))/))
    call interpos(EQ(1,idim2,3:ndim1(1)),EQ(13,idim2,3:ndim1(1)),ndim1(1)-2,xscal=EQ(1,idim2,2),tension=tens_def, &
      & yscal=EQ(13,idim2,2),nbcscal=(/2,2/),ybcscal=(/EQ(13,idim2,3), EQ(13,idim2,ndim1(1))/))
    !
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(17,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(17,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(17,idim2,2), EQ(17,idim2,ndim1(1))/))
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(18,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(18,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(18,idim2,2), EQ(18,idim2,ndim1(1))/))
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(22,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(22,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(22,idim2,2), EQ(22,idim2,ndim1(1))/))
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(28,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(28,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(28,idim2,2), EQ(28,idim2,ndim1(1))/))
    call interpos(EQ(1,idim2,2:ndim1(1)),EQ(29,idim2,2:ndim1(1)),ndim1(1)-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(29,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(29,idim2,2), EQ(29,idim2,ndim1(1))/))
  END DO
  !write(*,*)
  !write(*,*)'EQ_from_equil_ITM.f90: Have we got the right limit value for r->0 ?'
  !write(*,'(A20,5F10.4)')'   EQ(12,100,1:5)=', EQ(12,100,1:5)
  !write(*,'(A20,5F10.4)')'   EQ(13,100,1:5)=', EQ(13,100,1:5)
  !write(*,'(A20,5F10.0)')'   EQ(28,100,1:5)=', EQ(28,100,1:5)
  !write(*,'(A20,5F10.2)')'   EQ(17,100,1:5)=', EQ(17,100,1:5)
  !write(*,'(A20,5F10.4)')'   EQ(18,100,1:5)=', EQ(18,100,1:5)
  !
  ! Values transferred via NVAC (since needs dR/dchi)
  !
  SR(1:ndim2(1)) = equil_in_slice%coord_sys%position%r(ndim1(1),1:ndim2(1)) / R0EXP
  SZ(1:ndim2(1)) = equil_in_slice%coord_sys%position%z(ndim1(1),1:ndim2(1)) / R0EXP
  SR(ndim2(1)+1) = SR(1)
  SZ(ndim2(1)+1) = SZ(1)
  CHI(1:ndim2(1)) = EQ(2,1:ndim2(1),ndim1(1))
  CHI(ndim2(1)+1) = twopi + EQ(2,1,ndim1(1))
  QB = equil_in_slice%profiles_1d%q(ndim1(1))
  TB = equil_in_slice%profiles_1d%F_dia(ndim1(1)) / R0EXP / B0EXP
  R2J(1:ndim2(1)) = EQ(19,1:ndim2(1),ndim1(1))
  DCR2J(1:ndim2(1)) = EQ(20,1:ndim2(1),ndim1(1))
  CHIOLD(1:ndim2(1)) = EQ(24,1:ndim2(1),ndim1(1))
  CHIOLD(ndim2(1)+1) = CHIOLD(1) + twopi
  ! Compute on normlized quantities
  ZRMAG = equil_in_slice%coord_sys%position%r(1,1) / R0EXP
  ZZMAG = equil_in_slice%coord_sys%position%z(1,1) / R0EXP
  ! T is theta at (R,Z) on EQ(6) mid-points
  T(1) = atan2(SZ(1)-ZZMAG,SR(1)-ZRMAG)
  do idim2=2,ndim2(1)
    T(idim2) = atan2(SZ(idim2)-ZZMAG,SR(idim2)-ZRMAG)
    if (T(idim2) .LT. 0._R8) T(idim2) = T(idim2) + twopi
  end do
  T(ndim2(1)+1) = T(1) + twopi
  ! TH is theta on EQ(2) points, need to interpolate
  allocate(dim2plus(ndim2(1)+2))
  dim2plus(1) = EQ(6,ndim2(1),ndim1(1)) - twopi
  dim2plus(2:ndim2(1)+1) = EQ(6,1:ndim2(1),ndim1(1))
  dim2plus(ndim2(1)+2) = EQ(6,1,ndim1(1)) + twopi
  call interpos(dim2plus(1:ndim2(1)+2),(/ T(ndim2(1))-twopi, T(1:ndim2(1)+1) /),NIN=ndim2(1)+2, &
    & tension=tens_def, xout=EQ(2,1:ndim2(1),ndim1(1)), yout=TH(1:ndim2(1)),nbc=(/2, 2/),ybc=(/T(ndim2(1))-twopi, T(ndim2(1)+1)/))
  TH(ndim2(1)+1) = TH(1)+ twopi
  ROMID(1:ndim2(1)) = sqrt((SR(1:ndim2(1))-ZRMAG)**2 + (SZ(1:ndim2(1))-ZZMAG)**2)
  ROMID(ndim2(1)+1) = ROMID(1)
  call interpos(EQ(6,1:ndim2(1),ndim1(1)),ROMID(1:ndim2(1)),NIN=ndim2(1),tension=tens_def, &
        & xout=EQ(2,1:ndim2(1),ndim1(1)), yout=ROEDGE(1:ndim2(1)),nbc=-1,ybc=twopi)
  ROEDGE(ndim2(1)+1) = ROEDGE(1)
  ! on Gaussian points for TH intervals
  THINT(1:ndim2(1),2) = 0.5_R8 * (TH(1:ndim2(1)) + TH(2:ndim2(1)+1))
  THINT(1:ndim2(1),1) = THINT(1:ndim2(1)+1,2) - (TH(2:ndim2(1)+1)-TH(1:ndim2(1)))/sqrt(6._R8)
  THINT(1:ndim2(1),3) = THINT(1:ndim2(1)+1,2) + (TH(2:ndim2(1)+1)-TH(1:ndim2(1)))/sqrt(6._R8)
  call interpos(T(1:ndim2(1)),ROMID(1:ndim2(1)),NIN=ndim2(1),tension=tens_def, &
        & xout=THINT(1:ndim2(1),1), yout=ROINT(1:ndim2(1),1),youtp=DROINT(1:ndim2(1),1),nbc=-1,ybc=twopi)
  call interpos(T(1:ndim2(1)),ROMID(1:ndim2(1)),NIN=ndim2(1),tension=tens_def, &
        & xout=THINT(1:ndim2(1),2), yout=ROINT(1:ndim2(1),2),youtp=DROINT(1:ndim2(1),2),nbc=-1,ybc=twopi)
  call interpos(T(1:ndim2(1)),ROMID(1:ndim2(1)),NIN=ndim2(1),tension=tens_def, &
        & xout=THINT(1:ndim2(1),3), yout=ROINT(1:ndim2(1),3),youtp=DROINT(1:ndim2(1),3),nbc=-1,ybc=twopi)
  !  write(23,'(i4,1p9e14.6)') (idim2,THINT(idim2,1),THINT(idim2,2),THINT(idim2,3),ROINT(idim2,1),ROINT(idim2,2),ROINT(idim2,3),DROINT(idim2,1),DROINT(idim2,2),DROINT(idim2,3),idim2=1,ndim2(1))
  !
  ! Values transferred via NDES
  !
  DO idim1=1,NPSI
    CCR(idim1,1:NPOL) = 0.5_R8*(equil_in_slice%coord_sys%position%r(idim1,1:NPOL)+equil_in_slice%coord_sys%position%r(idim1+1,1:NPOL))
    CCZ(idim1,1:NPOL) = 0.5_R8*(equil_in_slice%coord_sys%position%z(idim1,1:NPOL)+equil_in_slice%coord_sys%position%z(idim1+1,1:NPOL))
  END DO
  CCR = CCR / R0EXP
  CCZ = CCZ / R0EXP
  DO idim1=1,NPSI+1
    CNR2(idim1,1:NPOL) = equil_in_slice%coord_sys%g_11(idim1,1:ndim2(1)) * d_ddim1(1)%position%r(idim1,1:ndim2(1)) &
      & + equil_in_slice%coord_sys%g_12(idim1,1:ndim2(1)) * d_ddim2(1)%position%r(idim1,1:ndim2(1))
    CNZ2(idim1,1:NPOL) = equil_in_slice%coord_sys%g_11(idim1,1:ndim2(1)) * d_ddim1(1)%position%z(idim1,1:ndim2(1)) &
      & + equil_in_slice%coord_sys%g_12(idim1,1:ndim2(1)) * d_ddim2(1)%position%z(idim1,1:ndim2(1))
  END DO
  DO idim1=1,NPSI
    CNR(idim1,1:NPOL) = 0.5_R8*(CNR2(idim1,1:NPOL)+CNR2(idim1+1,1:NPOL)) /R0EXP/B0EXP
    CNZ(idim1,1:NPOL) = 0.5_R8*(CNZ2(idim1,1:NPOL)+CNZ2(idim1+1,1:NPOL)) /R0EXP/B0EXP
  END DO
!!$  write(23,'(1p4e14.6)') (CCR(20,idim2), CCZ(20,idim2), CNR(20,idim2), CNZ(20,idim2), idim2=1,ndim2(1))
  !
  INUM = 12*40
  ALLOCATE(ZTETA(INUM))
  ALLOCATE(ZRHO(INUM))
  ZTETA(1:INUM) = TWOPI/REAL(INUM,R8) * (/ (REAL(IDIM1,R8) -1.5_R8, IDIM1=1,INUM) /)
  call interpos(T(1:ndim2(1)),ROMID(1:ndim2(1)),NIN=ndim2(1),tension=tens_def, &
        & xout=ZTETA(1:INUM), yout=ZRHO(1:INUM),nbc=-1,ybc=twopi)
  XS(1:INUM) = ZRMAG + ZRHO(1:INUM) * cos(ZTETA(1:INUM))
  YS(1:INUM) = ZZMAG + ZRHO(1:INUM) * sin(ZTETA(1:INUM))
!  write(23,'(1p2e14.6)') (XS(idim2), YS(idim2), idim2=1,inum)
  !
END SUBROUTINE EQ_from_equil
