SUBROUTINE write_to_output_diag(KOPT)
  !
  ! Write codeparam%output_diag, to have minimum of history
  !
  ! KOPT = 1: namelist 
  ! KOPT = 2: the various powers as diagnostics of convergence
  !
  use globals
  use euITM_schemas                       ! module containing the equilibrium type definitions
  !
  IMPLICIT NONE
  !
  INCLUDE 'NEWRUN.inc'
  !
  integer :: KOPT
  !
  integer :: idummy, ios, n_lines, i
  character(len = 132) :: dummy_line
  character(len = 150) :: dummy_line2
  !
  print *,'KOPT = ',KOPT
  SELECT CASE (KOPT)
  CASE (1)
    ! write namelist, at this stage write to local tmp file, then copy each line as comments to output_diag
    idummy = 999
    OPEN (unit = idummy, file = "tempfile_to_write_namelist", iostat = ios)
    IF (ios == 0) THEN    
      write(idummy, newrun)
      rewind idummy
      n_lines = 0
      DO
        READ (idummy, '(a)', iostat = ios) dummy_line
        if (ios == 0) then
          n_lines = n_lines + 1
        else
          exit
        end if
      END DO
      rewind idummy
      ! 30 lines written in power
      print *,'n_lines= ',n_lines
      allocate(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag(n_lines+30))
      wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag(:) = ''
      print *,'associated(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag), size= ', &
        & size(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag)
      if (.not. associated(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag)) then
        print *,' salut should not be here not associated'
      else
        print *,' associated in write_...'
      end if
      do i = 1, n_lines
        read (idummy, '(a)', iostat = ios) dummy_line
        write(dummy_line2,'(A,A,A)') '<!--',trim(dummy_line),'-->'
        wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag(i) = trim(dummy_line2(1:132))
      end do
      close(idummy,status='delete')
    end IF
    !
  CASE (2)
    ! write various total absorbed power to be able to check if case converged (std output of LION)
    
  CASE DEFAULT
    ! Should not be here since all cases defined
    PRINT *,' ERROR IN write_to_output_diag: case KOPT = ',KOPT,' NOT DEFINED'
    RETURN
  END SELECT
  !
  return
  !
end SUBROUTINE write_to_output_diag
