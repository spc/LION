C----------------------------------------------------------------------
CL                 N2        NAMELIST FROM CHEASE
C                  VERSION   27.02.91  H. LUTJENS CRPP LAUSANNE
C
       DIMENSION
     R   WALL(10), ANGLE(16)
C
       LOGICAL
     L   NLDIAG(1:30),       NLEINQ,   NLGREN,   NLSYM
C
       NAMELIST /NEWRUN/
     R   AL0,      ANGLE,    ARROW,    BETA,     BETAP,    CPSRF,
     R   EPSCON,   EPSMAC,   QIAXE,    QSURF,    REXT,     RITOT,
     R   TSURF,    WNTORE,   WALL,     RINOR,    QCYL,     P0,
     I	 NAL0AUTO,
     I   NER,      NEGP,     ITEST,    MEQ,      NCHI,     NDES,
     I   NFIG,     NITMAX,   NPRNT,    NPSI,     NSAVE,    NUA1,
     I   NUA2,     NUB1,     NUB2,     NUX,      NUPL,     NUSG,
     I   NV,       NUWA,     NVAC,     NVIT,     NWALL,
     L   NLDIAG,   NLEINQ,   NLGREN,   NLSYM
