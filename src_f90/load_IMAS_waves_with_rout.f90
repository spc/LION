subroutine load_itm_waves(waves_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals ! contains ids_schemas
  use ids_routines
  IMPLICIT NONE
  type(ids_waves)      :: waves_out
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='waves'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call imas_open(citmtree,kitmshot,kitmrun,idx)
  call ids_get(idx,signal_name,waves_out)

  if (associated(waves_out%ids_properties%comment)) then
    do i=1,size(waves_out%ids_properties%comment)
      write(6,'(A)') trim(waves_out%ids_properties%comment(i))
    end do
  end if
    
  if (waves_out%ids_properties%homogeneous_time .eq. 1) then
    print *,' waves_out%time(1) = ',waves_out%time(1)
  else
    print *,' waves_out%coherent_wave(1)%global_quantities(1)%time = ',waves_out%coherent_wave(1)%global_quantities(1)%time
  end if

  return
end subroutine load_itm_waves
