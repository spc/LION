SUBROUTINE equil_metric_derivatives(equil_in,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag)
  !
  ! Computes d_metrics_d_dim1/dim2/sqrt(dim1/dim1(end)) from equil_in(i).coord_sys for each i equilibrium input
  ! Thus if dim1, dim2 are psi, chi. These give derivatives vs psi_at_fixed_chi, chi_at_fixed_psi, rhopol_norm_at_fixed_chi
  !
  ! Note: If you need the straight-field line coordinate "thetastar" you can do:
  ! straight-field line coordinate thetastar = 1/q int(0,chi) [F_dia Jacobian_psichiphi/R^2 dchi]
  ! chi being dim2
  !! allocate(thetastraight(ndim1(ieq),ndim2(ieq))
  !! do idim1=1,ndim1(ieq)
  !!   call interpos(equil_in(ieq)%coord_sys%grid%dim2, &
  !!     & equil_in(ieq)%profiles_1d%F_dia *equil_in(ieq)%coord_sys%jacobian(idim1,:)/equil_in(ieq)%coord_sys%position%r(idim1,:)**2, &
  !!     & ndim2(ieq),tension=tens_def, youtint=thetastraight(:,idim2),nbc=(/-1, -1/),ybc=(/ twopi, twopi /))
  !!   thetastraight(:,idim2) = thetastraight(:,idim2) / equil_in(ieq)%profiles_1d%q
  !! end do
  !
  use itm_types
  USE interpos_module
  use euITM_schemas                       ! module containing the equilibrium type definitions
  !
  IMPLICIT NONE
  !
  REAL(R8), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_R8
  type(type_equilibrium), intent(in) :: equil_in
  type(type_coord_sys), pointer, intent(out)     :: d_ddim1(:), d_ddim2(:), d_dsqrtdim1norm(:)
  integer, intent(out) :: error_flag
  !
  interface
     SUBROUTINE COCOS(KCOCOS,Kexp_Bp,Ksigma_Bp,Ksigma_RphiZ,Ksigma_rhothetaphi,Ksign_q_pos,Ksign_pprime_pos)
       !
       ! return values of exp_Bp, sigma_Bp, sigma_rhothetaphi, sign_q_pos, sign_pprime_pos
       ! from the input value KCOCOS and according to O. Sauter and S. Yu. Medvevdev paper and Table I
       ! (see paper in chease directory)
       !
       IMPLICIT NONE
       INTEGER, intent(in) :: KCOCOS
       INTEGER, intent(out) :: Kexp_Bp, Ksigma_Bp, Ksigma_RphiZ, Ksigma_rhothetaphi, Ksign_q_pos, Ksign_pprime_pos
       !
       ! cocos=i or 10+i have similar coordinate conventions except psi/2pi for cocos=i and psi for cocos=10+i
       !
     end SUBROUTINE COCOS
  end interface
  !
  real(R8) :: tens_def, tens_def2, deltapsi, zsign_psi
  real(R8), allocatable :: sqrtdim1norm(:)
  integer :: nbequil, ieq, ilen, idim1, idim2
  integer :: cocos_in, iexp_Bp, isigma_Bp, isigma_RphiZ, isigma_rhothetaphi, isign_q_pos, isign_pprime_pos
  integer, allocatable :: ndim1(:), ndim2(:)
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  error_flag=0
  if (.not. associated(equil_in%coord_sys%grid%dim1)) then
     write(*,*)'ERROR in equil_metric_derivatices.f90'
     write(*,*)'  (.not. asociated(equil_in%coord_sys%grid%dim1))'
     error_flag=-1
  endif
  if (.not. associated(equil_in%coord_sys%grid%dim2)) then
     write(*,*)'ERROR in equil_metric_derivatices.f90'
     write(*,*)'  (.not. asociated(equil_in%coord_sys%grid%dim2))'
     error_flag=-1
  endif
  nbequil = 1
  if (nbequil .lt. 1) then
    print *,' problem in equil_metric_derivatives with size of equil_in = ', nbequil
    return
  end if
  allocate(d_ddim1(nbequil))
  allocate(d_ddim2(nbequil))
  allocate(d_dsqrtdim1norm(nbequil))
  allocate(ndim1(nbequil))
  allocate(ndim2(nbequil))
  !
  tens_def = -0.1_R8
  ! tens_def2 to get stronger smoothing. Needed for g12, but may be others
  tens_def2 = -3.0_R8
  do ieq=1,nbequil
    ! general part
    ndim1(ieq) = size(equil_in%coord_sys%grid%dim1)
    ndim2(ieq) = size(equil_in%coord_sys%grid%dim2)
    !
    ! Could assume cocos convention same for each equilibrium index
    cocos_in = equil_in%datainfo%cocos
    CALL COCOS(COCOS_IN,iexp_Bp,isigma_Bp,isigma_RphiZ,isigma_rhothetaphi,isign_q_pos,isign_pprime_pos)
    ! To have increasing psi (dim1) we need: d|psi-psi_axis| = sigma_Ip sigma_Bp dpsi
    zsign_psi = real(isigma_Bp * sign(1._R8,equil_in%global_param%i_plasma),R8)
    !
    ilen = size(equil_in%coord_sys%grid_type)
    allocate(d_ddim1(ieq)%grid_type(ilen))
    d_ddim1(ieq)%grid_type = equil_in%coord_sys%grid_type
    d_ddim1(ieq)%grid = equil_in%coord_sys%grid
    allocate(d_ddim2(ieq)%grid_type(ilen))
    d_ddim2(ieq)%grid_type = equil_in%coord_sys%grid_type
    d_ddim2(ieq)%grid = equil_in%coord_sys%grid
    allocate(d_dsqrtdim1norm(ieq)%grid_type(ilen))
    d_dsqrtdim1norm(ieq)%grid_type = equil_in%coord_sys%grid_type
    d_dsqrtdim1norm(ieq)%grid = equil_in%coord_sys%grid
    if (allocated(sqrtdim1norm)) deallocate(sqrtdim1norm)
    allocate(sqrtdim1norm(ndim1(ieq)))
    deltapsi = equil_in%coord_sys%grid%dim1(ndim1(ieq))-equil_in%coord_sys%grid%dim1(1)
    sqrtdim1norm(1:ndim1(ieq)) = sqrt((equil_in%coord_sys%grid%dim1(1:ndim1(ieq))-equil_in%coord_sys%grid%dim1(1))/deltapsi)
    !
    ! d/d_dim1 at fixed dim2. Assumes dim1 is psi like (thus rho^2 and at magnetic axis for dim1(1)
    ! can check if dim1(1) is small to see if dim1 is R, but should use grid_type (normally grid_type(2) integer value...)
    ! At this stage, assumes dim1 is psi as standard from CHEASE output
    !
    ! d/d_dim1:
    ! for those leading to infinite values with d/dim1 and not with d/sqrt(dim1), do on the latter, than extrapolate for central value
    ! this is for R, Z, J, g_12 and g_33 as far as I know
    allocate(d_ddim1(ieq)%jacobian(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_11(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_12(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_13(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_22(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_23(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%g_33(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%position%r(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim1(ieq)%position%z(ndim1(ieq),ndim2(ieq)))
    !
    do idim2=1,ndim2(ieq)
      ! Jacobian
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%jacobian(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%jacobian(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%jacobian(1,idim2), equil_in%coord_sys%jacobian(ndim1(ieq),idim2)/))
      d_ddim1(ieq)%jacobian(2:ndim1(ieq),idim2) = d_ddim1(ieq)%jacobian(2:ndim1(ieq),idim2) / 2._R8 / sqrtdim1norm(2:ndim1(ieq)) / deltapsi
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%jacobian(2:ndim1(ieq),idim2),n=ndim1(ieq)-1,xscal=sqrtdim1norm(1), &
        & tension=tens_def, &
        & yscal=d_ddim1(ieq)%jacobian(1,idim2),nbcscal=(/2,2/),ybcscal=(/d_ddim1(ieq)%jacobian(2,idim2), d_ddim1(ieq)%jacobian(ndim1(ieq),idim2)/))
      ! g_ij, multiply psi and function by zsign_psi to have same derivative, when grid%dim1 is used
      call interpos(zsign_psi*equil_in%coord_sys%grid%dim1, &
        & zsign_psi*equil_in%coord_sys%g_11(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%g_11(1:ndim1(ieq),idim2),nbc=(/2,2/),ybc=(/equil_in%coord_sys%g_11(1,idim2), equil_in%coord_sys%g_11(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_12(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def2, &
        & youtp=d_ddim1(ieq)%g_12(1:ndim1(ieq),idim2),nbc=(/2,2/),ybc=(/equil_in%coord_sys%g_12(1,idim2), equil_in%coord_sys%g_12(ndim1(ieq),idim2)/))
      d_ddim1(ieq)%g_12(2:ndim1(ieq),idim2) = d_ddim1(ieq)%g_12(2:ndim1(ieq),idim2) / 2._R8 / sqrtdim1norm(2:ndim1(ieq)) / deltapsi
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%g_12(2:ndim1(ieq),idim2),n=ndim1(ieq)-1,xscal=sqrtdim1norm(1), &
        & tension=tens_def, &
        & yscal=d_ddim1(ieq)%g_12(1,idim2),nbcscal=(/2,2/),ybcscal=(/d_ddim1(ieq)%g_12(2,idim2), d_ddim1(ieq)%g_12(ndim1(ieq),idim2)/))
      call interpos(zsign_psi*equil_in%coord_sys%grid%dim1(1:ndim1(ieq)), &
        & zsign_psi*equil_in%coord_sys%g_13(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%g_13(1:ndim1(ieq),idim2),nbc=(/2,2/),ybc=(/equil_in%coord_sys%g_13(1,idim2), equil_in%coord_sys%g_13(ndim1(ieq),idim2)/))
      ! Assume g_22=g_22tild/dim1^2 (if dim1 is psi, could put dim^1, but use dim1^2 to work even if dim1 is rho-like) to construct derivative better close to axis
      call interpos(zsign_psi*equil_in%coord_sys%grid%dim1(2:ndim1(ieq)), &
        & zsign_psi*equil_in%coord_sys%g_22(2:ndim1(ieq),idim2)*equil_in%coord_sys%grid%dim1(2:ndim1(ieq))**2, &
        & nin=ndim1(ieq)-1, tension=tens_def, youtp=d_ddim1(ieq)%g_22(2:ndim1(ieq),idim2), nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_22(2,idim2)*equil_in%coord_sys%grid%dim1(2)**2, &
        &       equil_in%coord_sys%g_22(ndim1(ieq),idim2)*equil_in%coord_sys%grid%dim1(ndim1(ieq))**2/))
      d_ddim1(ieq)%g_22(2:ndim1(ieq),idim2) = d_ddim1(ieq)%g_22(2:ndim1(ieq),idim2) / equil_in%coord_sys%grid%dim1(2:ndim1(ieq))**2 - &
        & 2._R8 * equil_in%coord_sys%g_22(2:ndim1(ieq),idim2) / equil_in%coord_sys%grid%dim1(2:ndim1(ieq))
      call interpos(sqrtdim1norm(2:ndim1(ieq)),log(abs(d_ddim1(ieq)%g_22(2:ndim1(ieq),idim2))), &
        & N=ndim1(ieq)-1,xscal=sqrtdim1norm(1),tension=tens_def,yscal=d_ddim1(ieq)%g_22(1,idim2),nbcscal=(/2,2/), &
        & ybcscal=(/log(abs(d_ddim1(ieq)%g_22(2,idim2))), &
        &           log(abs(d_ddim1(ieq)%g_22(ndim1(ieq),idim2))) /))
      d_ddim1(ieq)%g_22(1,idim2) =  sign(1._R8,d_ddim1(ieq)%g_22(2,idim2))*exp(d_ddim1(ieq)%g_22(1,idim2))
      !
      call interpos(zsign_psi*equil_in%coord_sys%grid%dim1, &
        & zsign_psi*equil_in%coord_sys%g_23(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%g_23(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_23(1,idim2), equil_in%coord_sys%g_23(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_33(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%g_33(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_33(1,idim2), equil_in%coord_sys%g_33(ndim1(ieq),idim2)/))
      d_ddim1(ieq)%g_33(2:ndim1(ieq),idim2) = d_ddim1(ieq)%g_33(2:ndim1(ieq),idim2) / 2._R8 / sqrtdim1norm(2:ndim1(ieq)) / deltapsi
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%g_33(2:ndim1(ieq),idim2),n=ndim1(ieq)-1,xscal=sqrtdim1norm(1), &
        & tension=tens_def, &
        & yscal=d_ddim1(ieq)%g_33(1,idim2),nbcscal=(/2,2/), &
        & ybcscal=(/d_ddim1(ieq)%g_33(2,idim2), d_ddim1(ieq)%g_33(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%position%r(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%position%r(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%position%r(1,idim2), equil_in%coord_sys%position%r(ndim1(ieq),idim2)/))
      d_ddim1(ieq)%position%r(2:ndim1(ieq),idim2) = d_ddim1(ieq)%position%r(2:ndim1(ieq),idim2) / 2._R8 / sqrtdim1norm(2:ndim1(ieq)) / deltapsi
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%position%r(2:ndim1(ieq),idim2),n=ndim1(ieq)-1,xscal=sqrtdim1norm(1), &
        & tension=tens_def, yscal=d_ddim1(ieq)%position%r(1,idim2),nbcscal=(/2,2/), &
        & ybcscal=(/d_ddim1(ieq)%position%r(2,idim2), d_ddim1(ieq)%position%r(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%position%z(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_ddim1(ieq)%position%z(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%position%z(1,idim2), equil_in%coord_sys%position%z(ndim1(ieq),idim2)/))
      d_ddim1(ieq)%position%z(2:ndim1(ieq),idim2) = d_ddim1(ieq)%position%z(2:ndim1(ieq),idim2) / 2._R8 / sqrtdim1norm(2:ndim1(ieq)) / deltapsi
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%position%z(2:ndim1(ieq),idim2),n=ndim1(ieq)-1,xscal=sqrtdim1norm(1), &
        & tension=tens_def, yscal=d_ddim1(ieq)%position%z(1,idim2),nbcscal=(/2,2/), &
        & ybcscal=(/d_ddim1(ieq)%position%z(2,idim2), d_ddim1(ieq)%position%z(ndim1(ieq),idim2)/))
    end do
    !
    ! d/d_sqrtdim1norm with sqrtdim1norm = sqrt((dim1-dim1(1))/(dim1(end)-dim1(1)))
    !
    allocate(d_dsqrtdim1norm(ieq)%jacobian(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_11(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_12(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_13(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_22(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_23(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%g_33(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%position%r(ndim1(ieq),ndim2(ieq)))
    allocate(d_dsqrtdim1norm(ieq)%position%z(ndim1(ieq),ndim2(ieq)))
    do idim2=1,ndim2(ieq)
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%jacobian(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%jacobian(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%jacobian(1,idim2), equil_in%coord_sys%jacobian(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_11(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%g_11(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_11(1,idim2), equil_in%coord_sys%g_11(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_12(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def2, &
        & youtp=d_dsqrtdim1norm(ieq)%g_12(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_12(1,idim2), equil_in%coord_sys%g_12(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_13(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%g_13(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_13(1,idim2), equil_in%coord_sys%g_13(ndim1(ieq),idim2)/))
      ! Assume g_22=g_22tild/sqrtdim1^2 to construct derivative better close to axis
      call interpos(sqrtdim1norm(2:ndim1(ieq)),equil_in%coord_sys%g_22(2:ndim1(ieq),idim2)*sqrtdim1norm(2:ndim1(ieq))**2, &
        & nin=ndim1(ieq)-1, tension=tens_def, youtp=d_dsqrtdim1norm(ieq)%g_22(2:ndim1(ieq),idim2), nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_22(2,idim2)*sqrtdim1norm(2)**2, &
        &       equil_in%coord_sys%g_22(ndim1(ieq),idim2)*sqrtdim1norm(ndim1(ieq))**2/))
      d_dsqrtdim1norm(ieq)%g_22(2:ndim1(ieq),idim2) = d_dsqrtdim1norm(ieq)%g_22(2:ndim1(ieq),idim2) / sqrtdim1norm(2:ndim1(ieq))**2 - &
        & 2._R8 * equil_in%coord_sys%g_22(2:ndim1(ieq),idim2) / sqrtdim1norm(2:ndim1(ieq))
      call interpos(sqrtdim1norm(2:ndim1(ieq)),log(abs(d_dsqrtdim1norm(ieq)%g_22(2:ndim1(ieq),idim2))), &
        & N=ndim1(ieq)-1,xscal=sqrtdim1norm(1),yscal=d_dsqrtdim1norm(ieq)%g_22(1,idim2),tension=tens_def,nbcscal=(/2,2/), &
        & ybcscal=(/log(abs(d_dsqrtdim1norm(ieq)%g_22(2,idim2))), &
        &           log(abs(d_dsqrtdim1norm(ieq)%g_22(ndim1(ieq),idim2))) /))
      d_dsqrtdim1norm(ieq)%g_22(1,idim2) =  sign(1._R8,d_dsqrtdim1norm(ieq)%g_22(2,idim2))*exp(d_dsqrtdim1norm(ieq)%g_22(1,idim2))
      !
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_23(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%g_23(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_23(1,idim2), equil_in%coord_sys%g_23(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%g_33(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%g_33(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%g_33(1,idim2), equil_in%coord_sys%g_33(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%position%r(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%position%r(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%position%r(1,idim2), equil_in%coord_sys%position%r(ndim1(ieq),idim2)/))
      call interpos(sqrtdim1norm(1:ndim1(ieq)),equil_in%coord_sys%position%z(1:ndim1(ieq),idim2),ndim1(ieq),tension=tens_def, &
        & youtp=d_dsqrtdim1norm(ieq)%position%z(1:ndim1(ieq),idim2),nbc=(/2,2/), &
        & ybc=(/equil_in%coord_sys%position%z(1,idim2), equil_in%coord_sys%position%z(ndim1(ieq),idim2)/))
    end do
    !
    ! d/d_dim2 at fixed dim2. Assumes dim1 is psi like (thus rho^2 and at magnetic axis for dim1(1)
    ! can check if dim1(1) is small to see if dim1 is R, but should use grid_type (normally grid_type(2) integer value...)
    ! At this stage, assumes dim1 is psi as standard from CHEASE output
    !
    ! d/d_dim2:
    allocate(d_ddim2(ieq)%jacobian(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_11(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_12(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_13(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_22(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_23(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%g_33(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%position%r(ndim1(ieq),ndim2(ieq)))
    allocate(d_ddim2(ieq)%position%z(ndim1(ieq),ndim2(ieq)))
    !
    do idim1=1,ndim1(ieq)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%jacobian(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%jacobian(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_11(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_11(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_12(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_12(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_13(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_13(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_22(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_22(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_23(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_23(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%g_33(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%g_33(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%position%r(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%position%r(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
      call interpos(equil_in%coord_sys%grid%dim2(1:ndim2(ieq)),equil_in%coord_sys%position%z(idim1,1:ndim2(ieq)),ndim2(ieq),tension=tens_def, &
        & youtp=d_ddim2(ieq)%position%z(idim1,1:ndim2(ieq)),nbc=-1,ybc=twopi)
    end do
  end do
  !
end SUBROUTINE equil_metric_derivatives
