#!/usr/bin/perl
#
#   f90con *.f90 : Add RKIND to real constants and REAL intrinsics
#
foreach $file (@ARGV) {
    $fold = $file.".orig";
    $fnew = $file;
    rename($file,$fold) || die "Cannot rename $file to $fold\n";
    open(IN, "< $fold") || die "Cannot open $fold\n";
    open(OUT, "> $fnew") || die "Cannot create $fnew\n";
    $nmod1 = 0;
    $nmod2 = 0;
    $use = 1;
    while ($l = <IN>) {
	$use = 0 if ($l =~ /^\s*use\s+(globals|prec_const)/i);
	if ($l =~ /^\s*include/i && $use > 0) {
	    print OUT "      USE prec_const\n";
	    $use = 0;
	    $nmod1++;
	}
	$l =~ s/float/REAL/ig;
	unless ($l =~ /^[!]/ || $l =~ /format\s*\(.+\)/i ) {
            if ($l =~ s/(\W[\+\-]?\d*\.?\d*[ed][\+\-]?\d+)(\W)/$1_RKIND$2/ig) {
		$nmod1++;
            }
            if ($l =~ s/(\W[\+\-]?\d+\.\d*)(\W)/$1_RKIND$2/g) {
		$nmod1++;
            } 
	    if ($l =~ s/(\W[\+\-]?\d*\.\d+)(\W)/$1_RKIND$2/g) {
		$nmod1++;
            }
	    unless ($l =~ /::/) {
		if ($l =~ s/\b(real\s*\([^real]+)\)/$1,RKIND\)/ig) {
		    $nmod2++ ;
		}
	    }
	    $l =~ s/([,_])RKIND[,_]RKIND/$1RKIND/ig;
	    $l =~ s/RKINDRKIND/RKIND/ig;
        }
	print OUT "$l";
    }
    close(IN);
    close(OUT);
    if ( $nmod1 || $nmod2 ) {
	print "File: $file: nmod1 = $nmod1, nmod2 = $nmod2\n";
    } else {
	rename($fold,$fnew)|| die "Cannot rename $fold to $fnew\n";
    }
}
