C---------------------------------------------------------------------
CL                  C2.2     EQUILIBRIUM QUANTITIES
C VERSION 0709     LDV      SEPTEMBER 2007           CRPP LAUSANNE
       COMMON/COMEQU/
     C   WCOMEG,   WEPS,     WEPSEL,   WEPSI,    WEPSMC,   WEPSTTMP,
     C   WG,       WGI,
     R   EQ    ,   WBETCH,   WBPOL2,   WBTOR2,   WBTOT ,   WBTOT2,
     R   WCHI  ,   WCHISF,   WDCHI ,   WDS   ,   WFRAC ,   WGRPS2,   
     R   WH    ,   WJAC  ,   WK    ,   WNU   ,   WOMCI ,   
     R	 WP,	   WPAXIS,   WPSI  ,   
     R   WQ    ,   WR2   ,   WRHO  ,   WS    ,   WT    ,   WTQ   ,   
     R   WDCR2J,   WDPT  ,   WJ0PHI,   WDNGP,    WDPR2J,   WDQCDS, EQ2
       COMPLEX
     C   WCOMEG,   WEPS,     WEPSEL,   WEPSI,    WEPSMC,   WEPSTTMP,
     C   WG,       WGI
       DIMENSION
     C   WEPSI(MDSPEC),          WGI(MDSPEC), EQ2(MDEQ,MDPOL,MDPSI1),
     R   EQ(MDEQ,MDPOL,MDPSI1),  WFRAC(MDSPEC),  WOMCI(MDSPEC)
