module LION_profile_tools_ITM

  implicit none

contains

  !> Initialises the global variable internal_profs, defined in LION_profiles 
  !> using input from the ITM CPO coreprof.
  !>
  !> by Thomas 2017-12-10
  subroutine init_internal_profs(coreprof, return_status, corefast, coreimpur)
    use GLOBALS, only: RKIND, cmp
    use euitm_schemas, only: type_coreprof, type_corefast, type_coreimpur
    use LION_profiles, only: internal_profs, free_internal_core_profiles, &
         update_mass_density, update_density_fractions

    implicit none

    ! Input
    type(type_coreprof), intent(in) :: coreprof
    integer, intent(out) :: return_status
    type(type_corefast),  optional, intent(in) :: corefast
    type(type_coreimpur), optional, intent(in) :: coreimpur

    ! Internal
    integer :: nr_spec, start_index, output_index

    ! Tidy up if anything is allocated already
    call free_internal_core_profiles(internal_profs)

    ! Allocate the number of ions in LION (inc. contributions from coreprof, coreimpur, corefast...)
    nr_spec = count_ions_in_coreprof(coreprof)
    if (present(coreimpur)) then
       nr_spec = nr_spec + count_ions_in_coreimpur(coreimpur)
    end if
    if (present(corefast)) then
       nr_spec = nr_spec + count_ions_in_corefast(corefast)
    end if
    allocate(internal_profs%ion(nr_spec))

    ! Add coreprof data
    start_index = 0
    call add_coreprof_to_internal_profs(coreprof, start_index, output_index, return_status)
    if (return_status /= 0) then
       write(*,*)'in init_internal_profs, error recieved from add_coreprof_to_internal_profs'
       return
    end if

    if (present(coreimpur)) then
       ! Add coreimpur data
       start_index=output_index
       call add_coreimpur_to_internal_profs(coreimpur, start_index, output_index, return_status)
       if (return_status /= 0) then
          write(*,*)'in init_internal_profs, error recieved from add_coreimpur_to_internal_profs'
          return
       end if
    end if

    if (present(corefast)) then
       ! Add corefast data
       start_index=output_index
       call add_corefast_to_internal_profs(corefast, start_index, output_index, return_status)
       if (return_status /= 0) then
          write(*,*)'in init_internal_profs, error recieved from '
          return
       end if
    end if

    call update_mass_density(internal_profs,return_status)
    if (return_status /= 0) then
       write(*,*)'in init_internal_profs, error recieved from update_mass_density'
       return
    end if
    call update_density_fractions(internal_profs,return_status)
    if (return_status /= 0) then
       write(*,*)'in init_internal_profs, error recieved from update_density_fractions'
       return
    end if

  end subroutine init_internal_profs


  integer function count_ions_in_coreprof(coreprof)
    use euitm_schemas, only: type_coreprof
    type(type_coreprof), intent(in) :: coreprof
    count_ions_in_coreprof = 0
    if (associated(coreprof%ni%value) .and. associated(coreprof%ti%value)) then
       if (size(coreprof%ni%value,1) == size(coreprof%ti%value,1)) then
          if (size(coreprof%ni%value,2) == size(coreprof%ti%value,2)) then
             count_ions_in_coreprof = size( coreprof%ni%value , 2 )
          end if
       end if
    end if
  end function count_ions_in_coreprof


  integer function count_ions_in_coreimpur(coreimpur)
    use euitm_schemas, only: type_coreimpur
    type(type_coreimpur), intent(in) :: coreimpur
    count_ions_in_coreimpur = 0
  end function count_ions_in_coreimpur


  integer function count_ions_in_corefast(corefast)
    use euitm_schemas, only: type_corefast
    type(type_corefast), intent(in) :: corefast
    integer :: ind
    count_ions_in_corefast = 0
    if (associated(corefast%values) .and. associated(corefast%compositions%ions)) then
       do ind=1,size(corefast%values)
          if (associated(corefast%values(ind)%ni)) then
             if (size(corefast%values(ind)%ni,2) == size(corefast%compositions%ions)) then
                count_ions_in_corefast = count_ions_in_corefast + &
                     number_fast_populations_in_corefast_value(corefast%values(ind))
             end if
          end if
       end do
    end if
  end function count_ions_in_corefast


  integer function number_fast_populations_in_corefast_value(val)
    use euitm_schemas, only: type_corefast_values
    type(type_corefast_values) :: val
    integer :: jion
    number_fast_populations_in_corefast_value = 0
    do jion=1,size(val%ni)
       if (valid_ion_index_in_core_fast_values(val,jion)) then
          number_fast_populations_in_corefast_value = &
               number_fast_populations_in_corefast_value + 1
       end if
    end do
  end function number_fast_populations_in_corefast_value


  logical function valid_ion_index_in_core_fast_values(val,ion_index)
    use GLOBALS, only: RKIND
    use euitm_schemas, only: type_corefast_values
    real(RKIND), parameter :: to_ev = 1.0_RKIND / 1.60217662e-19_RKIND
    real(RKIND), parameter :: MIN_DENS = 1.0e14_RKIND  ! Minimum density of fast ions.
    real(RKIND), parameter :: MIN_TEMP = 1.0e3_RKIND   ! Minimum temperature of fast ions.
    type(type_corefast_values) :: val
    integer :: ion_index
    valid_ion_index_in_core_fast_values = .false. ! Preliminary, until proven otherwise
    if ( associated(val%psi) .and. &
         associated(val%ni) .and. &
         associated(val%pi) ) then
       if ( (size(val%ni,1) == size(val%pi,1)) .and. &
            (size(val%ni,2) == size(val%pi,2)) .and. &
            (size(val%ni,2) >= ion_index) ) then
          if ( maxval(abs(val%ni(:,ion_index))) > MIN_DENS ) then
             if ( to_ev * maxval(abs(val%pi(:,ion_index))) / maxval(abs(val%ni(:,ion_index))) > MIN_TEMP ) then
                valid_ion_index_in_core_fast_values = .true.
                return
             end if
          end if
       end if
    end if
  end function valid_ion_index_in_core_fast_values


  !> Add data from coreprof to the global variable internal_profs, defined in LION_profiles.
  !>
  !> by Thomas 2017-12-10
  subroutine add_coreprof_to_internal_profs(coreprof, start_index, output_index, return_status)
    use GLOBALS, only: RKIND, cmp
    use euitm_schemas, only: type_coreprof
    use LION_profiles, only: internal_profs, set_splinecurve

    implicit none

    ! Input
    type(type_coreprof), intent(in) :: coreprof
    integer, intent(in)  :: start_index
    integer, intent(out) :: output_index
    integer, intent(out) :: return_status

    ! Internal
    real(RKIND) :: mass_dens( size(coreprof%te%value) )
    integer :: npsi
    integer :: ispec
    integer :: nr_spec

    npsi   = size( coreprof%ni%value , 1 )
    nr_spec = size( coreprof%ni%value , 2 )
    allocate( internal_profs%s(npsi) )

    !
    ! Initialise the grid in s=rho_pol=sqrt(psi_normalised)
    internal_profs%s = sqrt( ( coreprof%psi%value - coreprof%psi%value(1) ) / &
         ( coreprof%psi%value(npsi) - coreprof%psi%value(1) ) )

    call set_splinecurve(internal_profs%s, mass_dens / mass_dens(1), &
         internal_profs%mass_density, return_status)
    if (return_status /= 0) then
       write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_coreprof_to_internal_profs'
       write(*,*)'Failed to generate spline curve for internal_profs%mass_density'
    end if
    !
    ! Set electron temperature
    call set_splinecurve(internal_profs%s, coreprof%te%value, &
         internal_profs%electrons%temperature, return_status)
    if (return_status /= 0) then
       write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_coreprof_to_internal_profs'
       write(*,*)'Failed to generate spline curve for internal_profs%electrons%temperature'
    end if
    !
    ! Set ion profiles
    output_index = start_index
    do ispec=1,nr_spec
       output_index = output_index + 1

       internal_profs%ion(output_index)%ion_index = ispec
       internal_profs%ion(output_index)%impurity_index = 0
       internal_profs%ion(output_index)%is_fast_population = .false.

       internal_profs%ion(output_index)%mass = coreprof%compositions%nuclei( &
            coreprof%compositions%ions(ispec)%nucindex )%amn
       internal_profs%ion(output_index)%charge = coreprof%compositions%ions(ispec)%zion
       internal_profs%ion(output_index)%density_on_axis = coreprof%ni%value(1,ispec) + 1.0_RKIND ! Add 1.0 to avoid zero on axis values.

       ! NOTE: we need to avoid on axis value to be zero, thus we artificially add 1 particle/m^3.
       call set_splinecurve(internal_profs%s, coreprof%ni%value(:,ispec) + 1.0_RKIND, &
            internal_profs%ion(output_index)%density, return_status)
       if (return_status /= 0) then
          write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_coreprof_to_internal_profs'
          write(*,*)'Failed to generate spline curve for internal_profs%ion(output_index)%density'
       end if

       call set_splinecurve(internal_profs%s, coreprof%ti%value(:,ispec), &
            internal_profs%ion(output_index)%temperature_parallel, return_status)
       if (return_status /= 0) then
          write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_coreprof_to_internal_profs'
          write(*,*)'Failed to generate spline curve for '// &
               'internal_profs%ion(output_index)%temperature_parallel'
       end if

       call set_splinecurve(internal_profs%s, coreprof%ti%value(:,ispec), &
            internal_profs%ion(output_index)%temperature_perpendicular, return_status)
       if (return_status /= 0) then
          write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_coreprof_to_internal_profs'
          write(*,*)'Failed to generate spline curve for '// &
               'internal_profs%ion(output_index)%temperature_perpendicular'
       end if
    end do

  end subroutine add_coreprof_to_internal_profs


  !> Add data from corefast to the global variable internal_profs, defined in LION_profiles.
  !>
  !> by Thomas 2017-12-10
  subroutine add_corefast_to_internal_profs(corefast, start_index, output_index, return_status)
    use GLOBALS, only: RKIND, cmp
    use euitm_schemas, only: type_corefast
    use LION_profiles, only: internal_profs, set_splinecurve
    use interpos_module, only: interpos

    implicit none

    ! Input
    type(type_corefast), intent(in) :: corefast
    integer, intent(in)  :: start_index
    integer, intent(out) :: output_index
    integer, intent(out) :: return_status

    ! Internal
    real(RKIND), parameter :: to_ev = 1.0_RKIND / 1.60217662e-19_RKIND
    real(RKIND), parameter :: min_temperature = 50.0_RKIND
    integer :: ind, nr_fast, jion, jnuc, js
    integer :: npsi, ns
    real(RKIND), allocatable :: rho(:), dens(:), pressure(:), pressure_para(:), temp_para(:), temp_perp(:)

    output_index = start_index
    return_status = 0

    if (.not. associated(corefast%values)) return
    if (.not. associated(corefast%compositions%ions)) return
    if (.not. allocated(internal_profs%s)) return
    if (.not. allocated(internal_profs%ion)) return

    ns = size(internal_profs%s)

    LOOP_VALUES: do ind=1,size(corefast%values)
       HAS_NI: if (associated(corefast%values(ind)%ni)) then
          VALID_SIZE_NI: if (size(corefast%values(ind)%ni,2) == size(corefast%compositions%ions)) then                   
             LOOP_IONS: do jion=1,size(corefast%compositions%ions)
                VALID_ION_CONTRIBUTION: if (valid_ion_index_in_core_fast_values(corefast%values(ind),jion)) then

                   output_index = output_index + 1

                   jnuc = corefast%compositions%ions(jion)%nucindex
                   internal_profs%ion(output_index)%mass           = corefast%compositions%nuclei(jnuc)%amn
                   internal_profs%ion(output_index)%charge_nuclear = corefast%compositions%nuclei(jnuc)%zn
                   internal_profs%ion(output_index)%charge         = corefast%compositions%ions(jion)%zion
                   internal_profs%ion(output_index)%ion_index          = jion
                   internal_profs%ion(output_index)%impurity_index     = -999999999
                   internal_profs%ion(output_index)%is_fast_population = .true.

                   npsi = size(corefast%values(ind)%psi)

                   if (allocated(rho)) deallocate(rho)
                   allocate(rho(npsi))
                   rho = (corefast%values(ind)%psi - corefast%values(ind)%psi(1)) / &
                        (corefast%values(ind)%psi(npsi) - corefast%values(ind)%psi(1))

                   if (allocated(dens)) deallocate(dens)
                   allocate(dens(ns))
                   call interpos(rho, &
                        corefast%values(ind)%ni(:,jion) , &
                        npsi, &
                        nout=ns, &
                        xout=internal_profs%s, &
                        yout=dens)
                   do js=1,ns
                      if (dens(js) < 1.0_RKIND) dens(js) = 1.0_RKIND
                   end do

                   if (allocated(pressure)) deallocate(pressure)
                   allocate(pressure(ns))
                   call interpos(rho, &
                        corefast%values(ind)%pi(:,jion) , &
                        npsi, &
                        nout=ns, &
                        xout=internal_profs%s, &
                        yout=pressure)

                   if (allocated(pressure_para)) deallocate(pressure_para)
                   if (associated(corefast%values(ind)%pi_para)) then
                      if ( (size(corefast%values(ind)%pi_para,1)==npsi) .and. &
                           (size(corefast%values(ind)%pi_para,1)==size(corefast%values(ind)%pi_para,1)) ) then
                         allocate(pressure_para(ns))
                         call interpos( &
                              (corefast%values(ind)%psi - corefast%values(ind)%psi(1)) / &
                              (corefast%values(ind)%psi(npsi) - corefast%values(ind)%psi(1)), &
                              corefast%values(ind)%pi_para(:,jion) , &
                              npsi, &
                              nout=ns, &
                              xout=internal_profs%s, &
                              yout=pressure_para)
                      end if
                   end if

                   call set_splinecurve(internal_profs%s, dens, &
                        internal_profs%ion(output_index)%density, return_status)
                   if (return_status /= 0) then
                      write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_corefast_to_internal_profs'
                      write(*,*)'Failed to generate spline curve for internal_profs%electrons%density'
                   end if
                   internal_profs%ion(output_index)%density_on_axis = &
                        internal_profs%ion(output_index)%density%value(1)

                   if (allocated(temp_para)) deallocate(temp_para)
                   allocate(temp_para(ns))
                   if (allocated(temp_perp)) deallocate(temp_perp)
                   allocate(temp_perp(ns))

                   if (allocated(pressure_para)) then
                      temp_para = to_ev * pressure_para / dens
                      temp_perp = to_ev * (1.5_RKIND * pressure - 0.5_RKIND * pressure_para ) / dens
                   else
                      temp_para = to_ev * pressure / dens
                      temp_perp = temp_para
                   end if

                   ! Remove tiny values for the temperatures
                   do js=1,ns
                      if (js < ns) then
                         if (temp_para(js) < 0.1_RKIND * temp_para(js+1)) temp_para(js) = 0.1_RKIND * temp_para(js+1)
                      end if
                      if (js > 1) then
                         if (temp_para(js) < 0.1_RKIND * temp_para(js-1)) temp_para(js) = 0.1_RKIND * temp_para(js-1)
                      end if
                      if (temp_para(js) < 1.0_RKIND) temp_para(js) = min_temperature
                   end do
                   do js=1,ns
                      if (js < ns) then
                         if (temp_perp(js) < 0.1_RKIND * temp_perp(js+1)) temp_perp(js) = 0.1_RKIND * temp_perp(js+1)
                      end if
                      if (js > 1) then
                         if (temp_perp(js) < 0.1_RKIND * temp_perp(js-1)) temp_perp(js) = 0.1_RKIND * temp_perp(js-1)
                      end if
                      if (temp_perp(js) < 1.0_RKIND) temp_perp(js) = min_temperature
                   end do

                   call set_splinecurve(internal_profs%s, temp_para, &
                        internal_profs%ion(output_index)%temperature_parallel, return_status)
                   if (return_status /= 0) then
                      write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_corefast_to_internal_profs'
                      write(*,*)'Failed to generate spline curve for internal_profs%electrons%temperature_parallel'
                   end if

                   call set_splinecurve(internal_profs%s, temp_perp, &
                        internal_profs%ion(output_index)%temperature_perpendicular, return_status)
                   if (return_status /= 0) then
                      write(*,*)'ERROR in LION_profile_tools_ITM.f90::add_corefast_to_internal_profs'
                      write(*,*)'Failed to generate spline curve for internal_profs%electrons%temperature_perpendicular'
                   end if

                end if VALID_ION_CONTRIBUTION
             end do LOOP_IONS
          end if VALID_SIZE_NI
       end if HAS_NI
    end do LOOP_VALUES

    if (allocated(rho))           deallocate(rho)
    if (allocated(dens))          deallocate(dens)
    if (allocated(pressure))      deallocate(pressure)
    if (allocated(pressure_para)) deallocate(pressure_para)
    if (allocated(temp_para))     deallocate(temp_para)
    if (allocated(temp_perp))     deallocate(temp_perp)
  end subroutine add_corefast_to_internal_profs


  !> Add data from coreimpur to the global variable internal_profs, defined in LION_profiles.
  !>
  !> by Thomas 2017-12-10
  subroutine add_coreimpur_to_internal_profs(coreimpur, start_index, output_index, return_status)
    use GLOBALS, only: RKIND, cmp
    use euitm_schemas, only: type_coreimpur
    use LION_profiles, only: internal_profs, set_splinecurve

    implicit none

    ! Input
    type(type_coreimpur), intent(in) :: coreimpur
    integer, intent(in)  :: start_index
    integer, intent(out) :: output_index
    integer, intent(out) :: return_status

    output_index = start_index
    return_status = 0
  end subroutine add_coreimpur_to_internal_profs

end module LION_profile_tools_ITM
