C---------------------------------------------------------------------
CL                  C5.1     CONTROL VARIABLES
C VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
       COMMON/COMCON/
     R   AHEIGT,   ALARG ,   ANGLET,   ARSIZE,   ASYMB ,
     R   WNTDEL,
     I   NCONTR,   NCUT  ,   NPLTYP,   NRUN,     NTORSP,   MFL,      
     I	 MFU,
     L   NLDISO,   NLPHAS,   NLOTP0,   NLOTP1,   NLOTP2,   NLOTP3,   
     L   NLOTP4,   NLOTP5,   NLPLO5
       LOGICAL
     L   NLDISO,   NLPHAS,   NLOTP0,   NLOTP1,   NLOTP2,   NLOTP3,   
     L   NLOTP4,   NLOTP5,   NLPLO5
       DIMENSION
     R   ANGLET(16),
     L   NLOTP1(4),          NLOTP2(5),          NLOTP3(2),
     L   NLOTP4(5),          NLOTP5(40),         NLPLO5(25)
