C---------------------------------------------------------------------
CL                  C2.1     GENERAL PHYSICS VARIABLES
C VERSION 16     LDV      MAY 1995           CRPP LAUSANNE
       COMMON/COMPHY/
     R   ACHARG,   AD,       AMASS ,   AMASSE,   ANTRAD,   ANTRADMAX,
     R   ANTUP ,   ANU   ,   ASPCT ,   ATE,      ATI,      ATIP,     
     R	 BETA  ,   BETAP ,   BETAS ,   BNOT  ,   CEN0,
     R   CENDEN,   CENTE,    CENTI ,   CENTIP,   CEOMCI,   CPSRF ,
     R   CURASY,   CURSYM,   DELTA ,   DELTAF,   ELLIPT,   EQALFD,
     R   EQDENS,   EQFAST,   EQKAPD,   EQKAPF,   EQKAPT,   EQKPTE,
     R   EQTE,     EQTI  ,   FEEDUP,   FRAC  ,   FRCEN ,   FRDEL ,
     R   FREQCY,   GAMMA ,   OMEGA ,   QS,       QTILDA,   QIAXE ,
     R   RMAJOR,   SAMIN,    SAMAX,    SIGMA ,   THANT,    THANTW,
     R   VBIRTH,   WALRAD,   
     R   WNTORO,
     I   MANCMP,   MPOLWN,   NANTSHEET,NANTYP,   NBCASE,   NBTYPE,   
     I   NDARG,    NDDEG,    NDENS,    NFAKAP,   NHARM,    NRSPEC,   
     I	 NSADDL,   NTEMP,
     L   NLCOLD,   NLCOLE,   NLDIP,    NLFAST,	 NLTTMP
       LOGICAL
     L   NLCOLD,   NLCOLE,   NLDIP,    NLFAST,	 NLTTMP
       DIMENSION
     R   ACHARG(MDSPEC),     AD(MDAD),           AMASS(MDSPEC),
     R	 ATE(MDAD),          ATI(MDAD),          ATIP(MDAD),
     R   CEN0(MDBCAS),
     R   CENDEN(MDSPEC),     CENTI(MDSPEC),      CENTIP(MDSPEC),
     R   CEOMCI(MDSPEC),     CURASY(MDPANT),     CURSYM(MDPANT),
     R   EQKAPF(MDFAKA),     EQKAPT(MDSPEC),     EQTI(MDSPEC),
     R   FRAC(MDSPEC),       FRCEN(MDSPEC),      FRDEL(MDSPEC),
     R	 QS(MDPSI1),         QTILDA(MDPSI1),     THANT(MDTANT),
     I   MPOLWN(MDPANT)
