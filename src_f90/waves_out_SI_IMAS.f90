subroutine waves_out_si
  !        ----------------
  !
  !  5.2.2. construct desired physical quantities from solution vector
  !-----------------------------------------------------------------------
  use ids_schemas, only: R8=>ids_real
  use globals
  use interpos_module
  ! use ids_grid_common, only: coordtype_rhotor, coordtype_theta
  ! use ids_grid_structured, only: gridsetupstructuredsep, gridstructwritedata, grid_struct_nodes
  !
  IMPLICIT NONE
  !
  integer :: j, npsi_out
  real(rkind) :: tens_def
  real(rkind), allocatable :: sqrtpsinorm(:)
  real(rkind) :: norm_Efield_SI  ! Normalization factor; when multiplied by the normalized electric field yields it yields the field in SI units.
  real(rkind) :: ZMDEN           ! Mass density
  real(rkind) :: ZPOWER          ! Total heating power
  real(rkind) :: ZALFSP          ! Alfven speed
  complex(R8) :: ZI                  ! Imaginary unit
  complex(R8), allocatable :: efield(:,:)
  !
  npsi_out = size(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi)
  !
  allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(nrspec))
  allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%electrons%power_thermal_n_tor(1))
  !
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_inside(npsi_out))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal(npsi_out))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal_n_tor(npsi_out,1))
  !
  allocate(sqrtpsinorm(npsi_out))
  sqrtpsinorm(1:npsi_out) = sqrt((wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(1:npsi_out) &
    &                             - wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(1)) &
    & / (wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(npsi_out) &
    &    - wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%psi(1)))
  !
  ! integrated powers
  !
  tens_def = -0.1_rkind
  DO J=1,NRSPEC
    wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(j)%power_thermal = FLUPSP(NPSI+1,J) / FLUPOW(NPSI+1) &
      & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
    allocate(wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(j)%power_thermal_n_tor(1))
    wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(j)%power_thermal_n_tor(1) = &
      & wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%ion(j)%power_thermal
    ! add point at s=1 since value known there thus can be used as BC. At 0 should probably use 1st derv=0
    allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal(npsi_out))
    allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal_n_tor(npsi_out,1))
    call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPSP(1:npsi,J),FLUPSP(NPSI+1,J)/),npsi+1,nout=npsi_out,xout=sqrtpsinorm(1:npsi_out), &
      & tension=tens_def,yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal(1:npsi_out), &
      & nbc=(/0,2/),ybc=(/0._RKIND, FLUPSP(NPSI+1,J)/))
    wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal(1:npsi_out) = &
      & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal(1:npsi_out) / FLUPOW(NPSI+1) &
      & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
    wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal_n_tor(1:npsi_out,1) = &
      & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_inside_thermal(1:npsi)
  END DO
  wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%electrons%power_thermal = FLUPSP(NPSI+1,NRSPEC+1) / FLUPOW(NPSI+1) &
    & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
  wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%electrons%power_thermal_n_tor(1) = &
    & wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%electrons%power_thermal
  wavesLION_out%coherent_wave(1)%wave_solver_type%index = 2
  !
  call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPOW(1:npsi),FLUPOW(NPSI+1)/),npsi+1,nout=npsi_out,xout=sqrtpsinorm(1:npsi_out), &
    & tension=tens_def,yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_inside(1:npsi_out),nbc=(/0,2/), &
    & ybc=(/0._RKIND, FLUPOW(NPSI+1)/))
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_inside(1:npsi_out) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_inside(1:npsi_out) / FLUPOW(NPSI+1) &
    & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
  !
  call interpos((/CCS(1:NPSI), 1._RKIND /),(/FLUPSP(1:npsi,NRSPEC+1),FLUPSP(NPSI+1,NRSPEC+1)/),npsi+1,nout=npsi_out, &
    & xout=sqrtpsinorm(1:npsi_out),tension=tens_def, &
    & yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal(1:npsi_out),nbc=(/0,2/), &
    & ybc=(/0._RKIND, FLUPSP(NPSI+1,NRSPEC+1)/))
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal(1:npsi_out) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal(1:npsi_out) / FLUPOW(NPSI+1) &
    & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
  !
  !! wavesLION_out(iout_time_slice)%coherentwave(1)%profiles_1d%pow_ntor(1:npsi_out,1,1) = wavesLION_out(iout_time_slice)%coherentwave(1)%profiles_1d%pow_tot(1:npsi)  --> Wrong dimensions in schema: Removed temporarily
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal_n_tor(1:npsi_out,1) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_inside_thermal(1:npsi)
  !
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density(npsi_out))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density_n_tor(npsi_out,1))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal(npsi_out))
  allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal_n_tor(npsi_out,1))
  !
  call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPOS(1:npsi),DEPOS(NPSI)/),npsi+1,nout=npsi_out,xout=sqrtpsinorm(1:npsi_out), &
    & tension=tens_def,yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density(1:npsi_out),nbc=(/0,2/), &
    & ybc=(/0._RKIND, DEPOS(NPSI)/))
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density(1:npsi_out) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density(1:npsi_out) &
    & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
  !
  do j=1,NRSPEC
    allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal(npsi_out))
    allocate(wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal_n_tor(npsi_out,1))
    call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPPSP(1:npsi,J),DEPPSP(NPSI,J)/),npsi+1,nout=npsi_out,xout=sqrtpsinorm(1:npsi_out), &
      & tension=tens_def,yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal(1:npsi_out), &
      & nbc=(/0,2/),ybc=(/0._RKIND, DEPPSP(NPSI,J)/))
    wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal(1:npsi_out) = &
      & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal(1:npsi_out) &
      & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
    wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal_n_tor(1:npsi_out,1) = &
      & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%ion(j)%power_density_thermal(1:npsi_out)
  end do
  !
  call interpos((/CCS(1:NPSI), 1._RKIND /),(/DEPPSP(1:npsi,NRSPEC+1),DEPPSP(NPSI,NRSPEC+1)/),npsi+1,nout=npsi_out, &
    & xout=sqrtpsinorm(1:npsi_out),tension=tens_def, &
    & yout=wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal(1:npsi_out),nbc=(/0,2/), &
    & ybc=(/0._RKIND, DEPPSP(NPSI,NRSPEC+1)/))
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal(1:npsi_out) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal(1:npsi_out) &
    & * wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power
  !
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density_n_tor(1:npsi_out,1) = &
    & wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%power_density(1:npsi_out)
  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal_n_tor(1:npsi_out,1) = &
    &  wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%electrons%power_density_thermal(1:npsi_out)


!!!----------------------------------------------------------
!!! START: UNDER DEVELOPMENT - 2D WAVE FIELD OUTPUT WITH GGD
!!!----------------------------------------------------------

  ZI = CMPLX(0._RKIND,1._RKIND)
  if (.False.) then

!!! from Olivier 16/7, 2013
!!! 
!!! Efield_phys = sqrt(mu0 * v_A0 * P[watt] / Rmajor**2 / flupow(npsi+1) ) * Efield_LION
!!! 
!!! with v_A0 the alfven speed, Rmajor, Bnot calculated in LION.f90
!!! 
!!! And we have the fields in LION units as in OUTP5(12, 13, 14=E+, etc) using
!!! E_norm = EN(js,ichi)
!!! E_binorm = EP(js,ichi)
!!! 
!!! BN, BP, BPAR
!!! 
!!! The s-mesh is CCS and the chi mesh is CCHI
!!! 
!!! This should be added to waves_out_SI.f90...

!!$  OS to add moduleOS  call gridSetupStructuredSep(wavesLION_out%coherent_wave(1)%full_wave(iout_time_slice)%grid , &
!!$      ndim = 2, &
!!$      c1 = COORDTYPE_RHOTOR, &
!!$      x1 = wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor(1:NPSI) , &
!!$      c2 = COORDTYPE_THETA , &
!!$      x2 = CCHI(1:NPOL)    , & !!! CAREFUL!!! Should we use a different poloidal angle??
!!$      id = '2d_structured' )

    !     TOTAL MASS DENSITY ON MAGNETIC AXIS
    ZMDEN = 0._RKIND
    DO J=1,NRSPEC
      ZMDEN = ZMDEN + CENDEN(J)*AMASS(J)*CMP
    END DO

    !     ALFVEN SPEED ON MAGNETIC AXIS
    ZALFSP = BNOT / SQRT(CMU0*ZMDEN)

    !     Power [watt]
    ZPOWER=wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power

    !     Factor for transforming electric fields in LION-unit to SI units
    norm_Efield_SI = sqrt(cmu0 * ZALFSP * ZPOWER / RMAJOR**2 / abs(flupow(npsi+1)) )

    allocate( efield(NPSI,NPOL) )
    !
    ! Write 
    efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) + ZI*EP(1:NPSI,1:NPOL))
! OS TO ADD MODULE
!!$    call gridStructWriteData( wavesLION_out%coherent_wave(1)%full_wave(iout_time_slice)%grid , &
!!$      & wavesLION_out%coherent_wave(1)%full_wave(iout_time_slice)%e_field%plus , &
!!$      & GRID_STRUCT_NODES , &
!!$      & efield )
!!$
    efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) - ZI*EP(1:NPSI,1:NPOL))
!!$    call gridStructWriteData( wavesLION_out%coheren_twave(1)%full_wave(iout_time_slice)%grid , &
!!$      & wavesLION_out%coherentwave(1)%full_wave%e_components%e_minus , &
!!$      & GRID_STRUCT_NODES , &
!!$      & efield )

    deallocate( efield )

  endif !!! if (.False.)

!!!--------------------------------------------------------
!!! END: UNDER DEVELOPMENT - 2D WAVE FIELD OUTPUT WITH GGD
!!!--------------------------------------------------------


!!!----------------------------------------------------------------------
!!! START: UNDER DEVELOPMENT - OLD FORMAT VERSION OF 2D WAVE FIELD OUTPUT
!!!----------------------------------------------------------------------

  !     TOTAL MASS DENSITY ON MAGNETIC AXIS
  ZMDEN = 0._RKIND
  DO J=1,NRSPEC
    ZMDEN = ZMDEN + CENDEN(J)*AMASS(J)*CMP
  END DO

  !     ALFVEN SPEED ON MAGNETIC AXIS
  ZALFSP = BNOT / SQRT(CMU0*ZMDEN)

  !     Power [watt]
  ZPOWER=wavesLION_out%coherent_wave(1)%global_quantities(iout_time_slice)%power

  !     Factor for transforming electric fields in LION-unit to SI units
  norm_Efield_SI = sqrt(cmu0 * ZALFSP * ZPOWER / RMAJOR**2 / abs(flupow(npsi+1)) )

  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice))
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%rho_tor(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%theta_straight(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%r(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%z(NPSI,NPOL) )

!!$  allocate( wavesLION_out%coherent_wave(1)%fullwave%local%e_plus(1,NPSI,NPOL) )
!!$  allocate( wavesLION_out(iout_time_slice)%coherentwave(1)%fullwave%local%e_minus(1,NPSI,NPOL) )
!!$  allocate( wavesLION_out(iout_time_slice)%coherentwave(1)%fullwave%local%e_plus_ph(1,NPSI,NPOL) )
!!$  allocate( wavesLION_out(iout_time_slice)%coherentwave(1)%fullwave%local%e_minus_ph(1,NPSI,NPOL) )
  ! full_wave looks too complicated at this stage for e_field ...
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%plus%amplitude(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%plus%phase(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%minus%amplitude(NPSI,NPOL) )
  allocate( wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%minus%phase(NPSI,NPOL) )

  ! Fill grid_2d
  do j=1,NPSI
    wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%rho_tor( j , 1:NPOL ) = &
      wavesLION_out%coherent_wave(1)%profiles_1d(iout_time_slice)%grid%rho_tor(j)
  enddo
  do j=1,NPOL
!!$    wavesLION_out(iout_time_slice)%coherentwave(1)%grid_2d%theta( 1:NPSI , j ) = CCHI(j)
    wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%theta_straight( 1:NPSI , j ) = EQ(24,j,1:NPSI)
  enddo
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%r( 1:NPSI , 1:NPOL ) = CCR( 1:NPSI , 1:NPOL )
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%z( 1:NPSI , 1:NPOL ) = CCZ( 1:NPSI , 1:NPOL )

  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%grid%type%index=2 ! Here 2 represents a rectangular grid in (rho_tor,thetastraight)

  ! Fill fullwave%local
  allocate( efield(NPSI,NPOL) )
  efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) - ZI*EP(1:NPSI,1:NPOL))
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%minus%amplitude(:,:) = abs(efield)
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%minus%phase(:,:) = atan2(aimag(efield),real(efield))

  efield = 0.5_RKIND*norm_Efield_SI*(EN(1:NPSI,1:NPOL) + ZI*EP(1:NPSI,1:NPOL))
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%plus%amplitude(:,:) = abs(efield)
  wavesLION_out%coherent_wave(1)%profiles_2d(iout_time_slice)%e_field_n_tor(1)%plus%phase(:,:) = atan2(aimag(efield),real(efield))
  deallocate( efield )

!!!--------------------------------------------------------------------
!!! END: UNDER DEVELOPMENT - OLD FORMAT VERSION OF 2D WAVE FIELD OUTPUT
!!!--------------------------------------------------------------------

  return
end SUBROUTINE waves_out_SI
