#!/usr/bin/perl
#
#   convert.pl parses all .f files in current dir, eliminates all
#   the include files processed by globals.pl and put the
#   "USE globals" in each program units. The modified files
#   are stored in the directory specified previously in globals.pl
#
die "Usage: $0 \n" if ($#ARGV > 0);
$ENV{"PATH"} = "$ENV{'HOME'}/bin/f2f90:".$ENV{"PATH"};
### List of include files
open(INC, "<.incfiles") || die "Cannot open .incfiles\n";
@incfiles = <INC>;
chop($toDir = shift(@incfiles));
foreach (@incfiles) {
    chop;
    $incfiles .= "$_ ";
}
#
foreach $origfile (<*.f>) {
    ($newfile = $origfile) =~ s/.*\///;
    $newfile = "$toDir/".$newfile;
    open(IN, "< $origfile") || die "Cannot open $origfile\n";
    open(OUT, "> $newfile") || die "Cannot create $newfile\n";
    $nglobals = 0;
    $iinc = 0;
    $buffer = "";
    while (<IN>) {
	if (/^\s+INCLUDE\s*[\"\'](\S+)[\"\']/) {
	    $f = $1;
	    $iinc = 1;
	    if ( index($incfiles,$f)  < 0 ) {
		if ( $nglobals == 0 ) {
		    print OUT "         USE globals\n";
		    $nglobals = 1;
		}
	    } else {
		$buffer .= $_;
	    }
	} else {
	    if ($iinc) {
		$buffer .= $_;
	    } else {
		print OUT $_;
	    }
	}
    }
    print OUT $buffer;
    close(IN);
    close(OUT);
    system("fix2free.pl $newfile") ;
}

