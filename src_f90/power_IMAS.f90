SUBROUTINE POWER
  !        ----------------
  !
  !  4.2.08. CALCULATE TOTAL COMPLEX POWER
  !>>>       MODIFIED TO INCLUDE OPTION NLPHAS
  !----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMOUT ! INCLUDE 'COMOUT.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMVEC ! INCLUDE 'COMVEC.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
!!$  USE COMAUX ! INCLUDE 'COMAUX.inc'
!!$  USE COMIVI ! INCLUDE 'COMIVI.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
  IMPLICIT NONE
  !
  INTEGER :: IN2, IS, I, J, ILEFTV, JROW, JX1, icount_diag
  REAL(RKIND) :: ZQB, ZTB, ZTRA, ZARG, ZREPOW, ZIMPOW, ZCOUPL, ZINV, ZXDTH
  COMPLEX &
       &   CDOTC,   CDOTU,   ZCON,    ZPOWER,  ZPLPOW,   ZVAPOW &
       &  ,ZSCR,    ZSPOW,   ZWPOW,   ZI,      ZXJX1
  !----------------------------------------------------------------------
  !L                   1.  READ
  !
  !     READ SOURCE VECTOR (INTO U), REACTANCE SCALAR (REACS),
  !     OHM-VECTOR (XOHMR) AND VACUUM MATRIX (VAC)
  !
  CALL IODSK4 (8)
  ZSCR = REACS
  ZQB  = RQB
  ZTB  = RTB
  IN2  = NPOL
  IS = IN2
  !
  !     SOLUTION ON THE BOUNDARY: COPY NPOL LAST ELEMENTS OF XT TO X
  !
  I = NCOMP - IN2
  !
  DO J=1,IN2
    I=I+1
    X(J) = XT(I)
  END DO
  !
  !     PRINTOUT OF OHM-VECTOR AND SOLUTION ON THE BOUNDARY
  !
  IF (NLOTP4(2)) THEN
    WRITE (NPRNT,1002)
    WRITE (NPRNT,1000)
    WRITE (NPRNT,1010) (J,XDCHI(J),XDTH(J),XOHMR(J),J=1,IS)
  END IF
  !
  IF (NLOTP4(3)) THEN
    WRITE (NPRNT,1003)
    WRITE (NPRNT,1040) (J,X(J),X(J+1),J=1,IN2,2)
    WRITE (NPRNT,1051)
    WRITE (NPRNT,1055) (J,XT(J),SOURCT(J),J=1,NCOMP)
  END IF
  !
  !----------------------------------------------------------------------
  !L                2. CALCULATE POWER AT PLASMA SURFACE
  !                    USING EQ.(3.51) OF REF.(1)
  !
  ZI = CMPLX(0._RKIND,1._RKIND)
  !
  IF (NANTYP.GE.0) THEN
    ZSPOW = CDOTC (IN2,X(1),1,U(1),1) *ZI/OMEGA
  ELSE
    ZSPOW = CDOTC (NCOMP,XT(1),1,SOURCT(1),1) *ZI/OMEGA
  END IF
  !
  DO JROW = 1,IN2
    ILEFTV = (JROW-1) * IN2 + 1
    U(JROW) = CDOTU (IN2,VAC(ILEFTV),1,X(1),1)
  END DO
  !
  ZWPOW = CDOTC (IN2,X(1),1,U(1),1) *ZI/OMEGA
  !
  !     NORMALISATION
  !
  ZSPOW = ZSPOW * 0.5_RKIND * CPI
  ZWPOW =-ZWPOW * 0.5_RKIND * CPI
  ZPOWER = ZSPOW + ZWPOW
  IF (AIMAG(ZWPOW).NE.0._RKIND) THEN
    ZTRA = REAL(ZWPOW,RKIND) / AIMAG(ZWPOW)
  ELSE
    ZTRA = 0.0_RKIND
  END IF
  WRITE (NPRNT,9100) ZSPOW,ZTRA
  ! write output to output_diag as comments (nb extra lines prepared in write_to_output_diag)
  ! not available in IMAS so use wave_solver_type%description
  if (.not. associated(wavesLION_out%coherent_wave(1)%wave_solver_type%description)) then
    print *,'.not. associated(wavesLION_out%coherent_wave(1)%wave_solver_type%description)'
    allocate(wavesLION_out%coherent_wave(1)%wave_solver_type%description(30-3+2))
    wavesLION_out%coherent_wave(1)%wave_solver_type%description(:) = ''
  end if
  icount_diag = size(wavesLION_out%coherent_wave(1)%wave_solver_type%description) - 30 + 3
  print *,'icount_diag= ',icount_diag
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,1P2E16.5,A)') '<!--', &
    & 'COMPLEX POWER AT PLASMA SURFACE: ',ZSPOW,'-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,1PE16.5,A)') '<!--', &
    & 'NON-HERMICITY OF VACUUM MATRIX: ',ZTRA,'-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A)') '<!-- -->'  
  !
  !-----------------------------------------------------------------------
  !L                3. CALCULATE POWER AT THE ANTENNA
  !                    USING EQ.(3.35) OF REF.(1)
  !
  !lvC     READ EQ QUANTITIES ON (NPSI+1)-TH SURFACE = PLASMA SURFACE (S=1).
  !lvC
  !lv         CALL IODSK4 (4)         
  !
  !     (ROT E)-SUB-N * DSIGMA / DELTATHETA
  !
  DO J = 1,IN2
    !
    JX1 = J + 1
    !
    !     PERIODICITY
    IF (J.EQ.IN2) THEN
      JX1 = 1
    END IF
    !
    !     NORMAL CASE (NO POLOIDAL PHASE EXTRACTION)
    !
    IF (.NOT.NLPHAS) THEN
      !
      ZXDTH = XDTH(J)
      ! Thomas 2018-10-10
      IF (ABS(ZXDTH).LT.1E-10_RKIND) ZXDTH = 1E-10_RKIND
      U(J) = EQ(19,J,npsi+1) * ( X(JX1) - X(J) &
           &          + CMPLX(EQ(20,J,npsi+1), WNTORO*ZTB/EQ(19,J,npsi+1)) &
           &          * (X(JX1) + X(J)) * 0.5_RKIND * XDCHI(J) ) / ZXDTH
      !
      !     CASE WITH POLOIDAL PHASE EXTRACTION
      !
    ELSE
      !
      !     JUMP CONDITION NEAR CHI=PI
      IF (J.EQ.NCHI) THEN
        ZARG = WNTORO * ZQB * C2PI
        ZXJX1 = X(JX1) * CMPLX (COS(ZARG), SIN(ZARG))
      ELSE
        ZXJX1 = X(JX1)
      END IF
      !
      !     PHASE EXTRACTION
      ZARG = - WNTORO * ZQB * EQ(24,J,npsi+1)
      IF (J.GT.NCHI) THEN
        ZARG = ZARG + WNTORO * ZQB * C2PI
      END IF
      !
      U(J) = EQ(19,J,npsi+1) * ( ZXJX1 - X(J) + EQ(20,J,npsi+1) &
           &                                            * (ZXJX1 + X(J)) * &
           &          0.5_RKIND * XDCHI(J) ) * CMPLX (COS(ZARG), SIN(ZARG)) / &
           &          XDTH(J)
      !
    END IF
    !
  END DO
  !
  !     FACTORS FOR POWER
  ZCON= CPI * 0.5_RKIND
  !
  !     VACUUM POWER
  ZVAPOW = ZI * OMEGA * ZCON * ZSCR
  !
  !     PLASMA POWER
  ZPLPOW = CDOTC (IN2,U(1),1,XOHMR(1),1)
  ZPLPOW = ZCON * ZPLPOW
  !
  !     TOTAL POWER
  ZPOWER = ZPLPOW + ZVAPOW
  IF (REAL(ZPOWER).NE.0._RKIND) THEN
    ZCOUPL = AIMAG(ZPOWER) / REAL(ZPOWER)
    IF (ZCOUPL.NE.0._RKIND) THEN
      ZINV = 1._RKIND/ZCOUPL
    ELSE
      ZINV   = 0._RKIND
    END IF
  ELSE
    ZCOUPL = 0._RKIND
    ZINV   = 0._RKIND
  END IF
  !
  WRITE (NPRNT,9200) ZPLPOW,ZVAPOW,ZPOWER,ZCOUPL,ZINV
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,A)') '<!--', &
    & 'COMPLEX POWER AT ANTENNA: ','-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,1P2E16.5,A)') '<!--', &
    & 'PLASMA(ANTENNA): ',ZPLPOW,'-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,1P2E16.5,A)') '<!--', &
    & 'VACUUM         : ',ZVAPOW,'-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,A,1P2E16.5,A)') '<!--', &
    & 'TOTAL          : ',ZPOWER,'-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A)') '<!-- -->'  
  !
  ZREPOW = REAL(ZSPOW,RKIND)
  ZIMPOW = AIMAG (ZSPOW) + AIMAG(ZVAPOW)
  !
  WRITE (NPRNT,9210) OMEGA, FREQCY, ZREPOW, ZIMPOW
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag), &
    & '(A,7X,A,10X,A,6X,A,6X,A,A)') '<!--','OMEGA','FREQUENCY','RE(POWER)','IM(POWER)','-->'
  icount_diag = icount_diag + 1
  write(wavesLION_out%coherent_wave(1)%wave_solver_type%description(icount_diag),'(A,1X,1P4E16.5,A)') '<!--', &
    & OMEGA, FREQCY, ZREPOW, ZIMPOW,'-->'
!!$  wavesLION_out(iout_time_slice)%coherentwave(1)%global_param%power_tot = ZREPOW
  !
  !----------------------------------------------------------------------
1000 FORMAT (///,' OUTPUT FROM POWER',//,' DC, D-TH, OHMR, OHMI',/)
1002 FORMAT (//,' OUTPUT 4.2',/)
1003 FORMAT (//,' OUTPUT 4.3',/)
1010 FORMAT (1X,I5,1P4E15.3)
1040 FORMAT (//,1X,' X ON PLASMA BOUNDARY',//,(1X,I5,1P4E15.3))
1051 FORMAT (//,'  JS   SOLUTION VECTOR            SOURCE VECTOR',/)
1055 FORMAT (1X,I5,1P4E12.3)
  !
9100 FORMAT (///,1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COMPLEX POWER AT PLASMA SURFACE',29X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),14X,'PLASMA(SURFACE)',1PE16.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'NON-HERMICITY OF VACUUM MATRIX',1PE15.5,15X, &
       &   3('*')/,1X,3('*'),62X,3('*'))
  !
9200 FORMAT (1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COMPLEX POWER AT ANTENNA       ',29X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),14X,'PLASMA(ANTENNA)',1PE16.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),14X,'VACUUM',1PE25.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),14X,'TOTAL ',1PE25.5,1PE15.5,2X,3('*')/, &
       &   1X,3('*'),62X,3('*')/, &
       &   1X,68('*')/1X,3('*'),62X,3('*')/, &
       &   1X,3('*'),2X,'COUPLING (IM POWER/RE POWER)',1PE17.5,15X, &
       &   3('*')/,1X,3('*'),2X,'INVERSE COUPLING',1PE29.5,15X,3('*')/, &
       &   1X,3('*'),62X,3('*')/,1X,68('*')//)
  !
9210 FORMAT (//,7X,'OMEGA',10X,'FREQUENCY',6X,'RE(POWER)',6X, &
       &	 'IM(POWER)',/,1X,1P4E15.5,'  987654321.0'/)
  !
  !----------------------------------------------------------------------
  RETURN
END SUBROUTINE POWER
