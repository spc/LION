subroutine load_itm_coreprof(coreprof_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals ! contains ids_schemas
  use ids_routines
  IMPLICIT NONE
  type(ids_core_profiles) :: coreprof_out
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*13  :: signal_name ='core_profiles'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call imas_open(citmtree,kitmshot,kitmrun,idx)
  call ids_get(idx,signal_name,coreprof_out)

  if (associated(coreprof_out%ids_properties%comment)) then
    do i=1,size(coreprof_out%ids_properties%comment)
      write(6,'(A)') trim(coreprof_out%ids_properties%comment(i))
    end do
  end if

  if (coreprof_out%ids_properties%homogeneous_time .eq. 1) then
    write(*,*) ' coreprof_out%time(1)= ',coreprof_out%time(1)
  else
    write(*,*) ' coreprof_out%profiles_1d(1)%time= ',coreprof_out%profiles_1d(1)%time
  end if

  return
end subroutine load_itm_coreprof
