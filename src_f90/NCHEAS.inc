!----------------------------------------------------------------------
!L                 N2        NAMELIST FROM CHEASE
!                  VERSION   27.02.91  H. LUTJENS CRPP LAUSANNE
!
!!$       DIMENSION &
!!$     &   WALL(10), ANGLE(16)
!!$!
!!$       LOGICAL &
!!$     &   NLDIAG(1:30),       NLEINQ,   NLGREN,   NLSYM
!
       NAMELIST /NEWRUN/ &
     &   AL0,      ANGLE,    ARROW,    BETA,     BETAP,    CPSRF, &
     &   EPSCON,   EPSMAC,   QIAXE,    QSURF,    REXT,     RITOT, &
     &   TSURF,    WNTORE,   WALL,     RINOR,    QCYL,     P0, &
     &	 NAL0AUTO, &
     &   NER,      NEGP,     ITEST,    MEQ,      NCHI,     NDES, &
     &   NFIG,     NITMAX,   NPRNT,    NPSI,     NSAVE,    NUA1, &
     &   NUA2,     NUB1,     NUB2,     NUX,      NUPL,     NUSG, &
     &   NV,       NUWA,     NVAC,     NVIT,     NWALL, &
     &   NLDIAG,   NLEINQ,   NLGREN,   NLSYM
