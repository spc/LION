#!/bin/tcsh -ef
#
# Build libraries needed for generating Kepler actors in CPO based version of LION
#
make -C src_f90/ veryclean
make -C src_f90/ libraries DEBUG=yes

make -C src_f90/ clean_o_and_mod
make -C src_f90/ libraries DEBUG=no

echo "To generated the LION actor: fc2k fc2k/LIONslice.xml"
