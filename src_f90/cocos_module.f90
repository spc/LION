MODULE COCOS_module
  !
  ! From COCOS paper: O. Sauter and S. Yu. Medvevdev, "Tokamak Coordinate Conventions: COCOS", Comput. Physics Commun. 184 (2013) 293
  !
  ! Contains subroutines:
  !
  ! cocos: to get the cocos values as in the table of the paper
  ! cocos_values_cefficients: to get the cocos values and the coefficients required to transform quantities as in Appendic C (without normalizations)
  !
  PUBLIC COCOS_values_coefficients, COCOS
  !
CONTAINS
  !
  subroutine COCOS_values_coefficients(COCOS_in, COCOS_out, Ip_in, B0_in, &
    & sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, exp_Bp_eff, &
    & fact_psi, fact_q, fact_dpsi, fact_dtheta,  Ipsign_out, B0sign_out)
    !
    ! Provide Transformation values for a set of quantities for a given pair of input/output Cocos numbers
    !
    ! Can ask for a specific sign of Ip and/or B0 in output as well
    !
    ! It follows the general transformation rules specified in Appendix C of COCOS paper (O. Sauter and S. Yu. Medvevdev, Comput. Physics Commun. 184 (2013) 293)
    ! without the normalization factors
    !
    ! Inputs:
    !   COCOS_in, COCOS_in: assumed input COCOS and desired output COCOS (use 13->11 (EUITM->ITER) if default integers provided)
    !   Ip_in, B0_in: (real) values of Ip and B0 (use +1. if default real provided)
    !   Ipsign_out, B0sign_out (optional): desired signs of Ip and B0 in output (computes factors to obtain these signs)
    !                                      (default) if not provided, compute the signs following the transformation cocos_in->coco_out from the values Ip_in, B0_in
    !
    ! Returns COCOS related values (from table 1 in COCOS paper):
    !   sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, exp_Bp_eff
    !
    ! Returns also coefficients factors:
    !   fact_psi (for psi terms), fact_q, fact_dpsi (for d/dpsi terms), fact_dtheta (for dtheta and d/dtheta terms)
    !
    use ids_schemas, only: r8=>ids_real, ids_int_invalid, ids_real_invalid

    IMPLICIT NONE
    REAL(R8), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_R8

    integer,  intent(IN) :: COCOS_in
    integer,  intent(IN) :: COCOS_out
    real(r8), Intent (in):: Ip_in 
    real(r8), Intent (in):: B0_in 
    real(R8), INTENT (OUT) :: sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff
    real(R8), INTENT (OUT) :: exp_Bp_eff, fact_psi, fact_q, fact_dpsi, fact_dtheta

    integer, optional, intent(IN) :: Ipsign_out
    integer, optional, intent(IN) :: B0sign_out

    !
    integer :: icocos_in, iexp_Bp_in,isigma_Bp_in,isigma_RphiZ_in,isigma_rhothetaphi_in,isign_q_pos_in,isign_pprime_pos_in
    integer :: icocos_out, iexp_Bp_out,isigma_Bp_out,isigma_RphiZ_out,isigma_rhothetaphi_out,isign_q_pos_out,isign_pprime_pos_out
    integer :: iIpsign_out, iB0sign_out
    real(R8) :: sigma_Ip_in, sigma_B0_in, sigma_Ip_out, sigma_B0_out
    !
    ! Default outputs
    !
    sigma_Ip_eff = 1.0_r8
    sigma_B0_eff = 1.0_r8
    sigma_Bp_eff = 1.0_r8
    sigma_rhothetaphi_eff = 1.0_r8
    sigma_RphiZ_eff = 1.0_r8
    exp_Bp_eff = 1.0_r8
    fact_psi = 1.0_r8
    fact_q = 1.0_r8
    fact_dpsi = 1.0_r8
    fact_dtheta = 1.0_r8
    !
    ! Check inputs
    !
    icocos_in = 13
    if (COCOS_in .NE. ids_int_invalid) icocos_in = COCOS_in
    icocos_out = icocos_in
    if (COCOS_out .NE. ids_int_invalid) icocos_out = COCOS_out
    sigma_Ip_in = 1.0_r8
    if (Ip_in .NE. ids_real_invalid) sigma_Ip_in = sign(1.0_R8, Ip_in)
    sigma_B0_in = 1.0_r8
    if (B0_in .NE. ids_real_invalid) sigma_B0_in = sign(1.0_R8, B0_in)
    !
    ! if Ipsign_out undefined means use input value transformed to new COCOS
    iIpsign_out = ids_int_invalid
    if (present(Ipsign_out)) THEN
      iIpsign_out = Ipsign_out
      if (iIpsign_out .eq. 0) iIpsign_out = ids_int_invalid ! assume 0 means use from Ip input
      if (iIpsign_out .gt. 0) iIpsign_out = 1
    end if
    ! if B0sign_out undefined means use input value transformed to new COCOS
    iB0sign_out = ids_int_invalid
    if (present(B0sign_out)) THEN
      iB0sign_out = B0sign_out
      if (iB0sign_out .eq. 0) iB0sign_out = ids_int_invalid ! assume 0 means use from B0 input
      if (iB0sign_out .gt. 0) iB0sign_out = 1
    end if
    !
    ! Get COCOS related parameters
    call COCOS(cocos_in,iexp_Bp_in,isigma_Bp_in,isigma_RphiZ_in,isigma_rhothetaphi_in,isign_q_pos_in,isign_pprime_pos_in)
    call COCOS(cocos_out,iexp_Bp_out,isigma_Bp_out,isigma_RphiZ_out,isigma_rhothetaphi_out,isign_q_pos_out,isign_pprime_pos_out)
    !
    ! Define effective variables: sigma_Ip_eff, si1gma_B0_eff, sigma_Bp_eff, exp_Bp_eff as in Appendix C
    !
    sigma_RphiZ_eff  = REAL(isigma_RphiZ_out * isigma_RphiZ_in,R8)
    !
    ! sign(Ip) in output:
    IF (iIpsign_out .LE. -2) THEN
      ! sign folllowing transformation
      sigma_Ip_eff = sigma_RphiZ_eff
    ELSE
      sigma_Ip_eff = sigma_Ip_in * REAL(iIpsign_out,R8)
    END IF
    sigma_Ip_out = sigma_Ip_in * sigma_Ip_eff
    !
    ! sign(B0) in output:
    IF (iB0sign_out .LE. -2) THEN
      ! sign folllowing transformation
      sigma_B0_eff = REAL(isigma_RphiZ_in * isigma_RphiZ_out,R8)
    ELSE
      sigma_B0_eff = sigma_B0_in * REAL(iB0sign_out,R8)
    END IF
    sigma_B0_out = sigma_B0_in * sigma_B0_eff
    !
    sigma_Bp_eff = REAL(isigma_Bp_out * isigma_Bp_in,R8)
    exp_Bp_eff = REAL(iexp_Bp_out - iexp_Bp_in,R8)
    sigma_rhothetaphi_eff  = REAL(isigma_rhothetaphi_out * isigma_rhothetaphi_in,R8)
    !
    ! Note that sign(sigma_RphiZ*sigma_rhothetaphi) gives theta in clockwise or counter-clockwise respectively
    ! Thus sigma_RphiZ_eff*sigma_rhothetaphi_eff negative if the direction of theta has changed from cocos_in to _out
    !

    fact_psi = sigma_Ip_eff * sigma_Bp_eff * TWOPI**exp_Bp_eff
    fact_dpsi  = sigma_Ip_eff * sigma_Bp_eff / TWOPI**exp_Bp_eff
    fact_q = sigma_Ip_eff * sigma_B0_eff * sigma_rhothetaphi_eff
    fact_dtheta  = sigma_RphiZ_eff * sigma_rhothetaphi_eff
    !
    return
    !
  end subroutine COCOS_Values_Coefficients
  !
  !
  SUBROUTINE COCOS(KCOCOS,Kexp_Bp,Ksigma_Bp,Ksigma_RphiZ,Ksigma_rhothetaphi,Ksign_q_pos,Ksign_pprime_pos,Ktheta_sign_clockwise)
    !
    ! return values of exp_Bp, sigma_Bp, sigma_rhothetaphi, sign_q_pos, sign_pprime_pos
    ! optional: theta_sign_clockwise = sigma_RphiZ*sigma_rhothetaphi: +1 if theta clockwise and -1 if counter-clockwise
    ! "standard" mathematical polar direction is counter-clockwise thus theta_sign_clockwise=-1
    !
    ! from the input value KCOCOS and according to the paper:
    ! O. Sauter and S. Yu. Medvevdev, "Tokamak coordinate conventions: COCOS", Comput. Physics Commun. 184 (2013) 293 and Table I
    ! (see paper in chease directory)
    !
    IMPLICIT NONE
    INTEGER, intent(in) :: KCOCOS
    INTEGER, intent(out) :: Kexp_Bp, Ksigma_Bp, Ksigma_RphiZ, Ksigma_rhothetaphi, Ksign_q_pos, Ksign_pprime_pos
    INTEGER, intent(out), optional :: Ktheta_sign_clockwise
    !
    ! cocos=i or 10+i have similar coordinate conventions except psi/2pi for cocos=i and psi for cocos=10+i
    !
    ! 2 pi factor:
    Kexp_Bp = 0
    if (KCOCOS .ge. 11) Kexp_Bp = 1
    !
    ! Other parameters from Table I
    !
    SELECT CASE (KCOCOS)
    CASE (1, 11)
      ! ITER, Boozer are cocos=11
      Ksigma_Bp = +1
      Ksigma_RphiZ = +1
      Ksigma_rhothetaphi = +1
      Ksign_q_pos = +1
      Ksign_pprime_pos = -1
      !
    CASE (2, 12)
      ! CHEASE, ONETWO, Hinton-Hazeltine, LION is cocos=2
      Ksigma_Bp = +1
      Ksigma_RphiZ = -1
      Ksigma_rhothetaphi = +1
      Ksign_q_pos = +1
      Ksign_pprime_pos = -1
      !
    CASE (3, 13)
      ! Freidberg, CAXE, KINX are cocos=3
      ! EU-ITM up to end of 2011 is COCOS=13
      Ksigma_Bp = -1
      Ksigma_RphiZ = +1
      Ksigma_rhothetaphi = -1
      Ksign_q_pos = -1
      Ksign_pprime_pos = +1
      !
    CASE (4, 14)
      ! 
      Ksigma_Bp = -1
      Ksigma_RphiZ = -1
      Ksigma_rhothetaphi = -1
      Ksign_q_pos = -1
      Ksign_pprime_pos = +1
      !
    CASE (5, 15)
      ! 
      Ksigma_Bp = +1
      Ksigma_RphiZ = +1
      Ksigma_rhothetaphi = -1
      Ksign_q_pos = -1
      Ksign_pprime_pos = -1
      !
    CASE (6, 16)
      ! 
      Ksigma_Bp = +1
      Ksigma_RphiZ = -1
      Ksigma_rhothetaphi = -1
      Ksign_q_pos = -1
      Ksign_pprime_pos = -1
      !
    CASE (7, 17)
      ! TCV psitbx is cocos=7
      Ksigma_Bp = -1
      Ksigma_RphiZ = +1
      Ksigma_rhothetaphi = +1
      Ksign_q_pos = +1
      Ksign_pprime_pos = +1
      !
    CASE (8, 18)
      ! 
      Ksigma_Bp = -1
      Ksigma_RphiZ = -1
      Ksigma_rhothetaphi = +1
      Ksign_q_pos = +1
      Ksign_pprime_pos = +1
      !
    CASE DEFAULT
      ! SAhould not be here since all cases defined
      PRINT *,' ERROR IN COCOS: COCOS = ',KCOCOS,' DOES NOT EXIST'
      RETURN
    END SELECT
    !
    if (present(Ktheta_sign_clockwise)) Ktheta_sign_clockwise = Ksigma_RphiZ * Ksigma_rhothetaphi
    !
  END SUBROUTINE COCOS
!
end MODULE COCOS_module
