# Start from clean environment
module purge

# IMAS and IWRAP
module load IMAS iWrap

# Actor folder
export ACTOR_FOLDER=~/public/PYTHON_ACTORS
mkdir -p $ACTOR_FOLDER

# Need to remove the stack limit to avoid segmentation fault inside codes
ulimit -Ss unlimited

# Intel as default compiler
export FCOMPILER=ifort

# Libraries needed for the compilation of the H&CD codes themselves
module load XMLlib INTERPOS

# EXTEND PYTHON PATH AND AVOID DOUBLONS
export PYTHONPATH=$ACTOR_FOLDER:$PYTHONPATH
export PYTHONPATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PYTHONPATH}))')"


