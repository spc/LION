SUBROUTINE EQ_from_equil(equil_in_slice,in_time_slice, error_flag,output_message)
  !
  ! Compute EQs from equil_in CPO
  !
  ! Use equil_metric_derivatives(equil_in,d_ddim1,d_ddim2,d_dsqrtdim1norm) to compute metrics derivatives
  !
  use ids_schemas, only: R8=>ids_real
  use globals
  use interpos_module
  use cocos_module
  use sort_module, only: simple_sort
  !
  IMPLICIT NONE
  !
  type(ids_equilibrium), intent(in)      :: equil_in_slice
  integer,               intent(in)      :: in_time_slice
  integer,               intent(out)     :: error_flag
  character(len=:), allocatable, intent(out) :: output_message
  !
  interface
     !
     SUBROUTINE equil_metric_derivatives(equil_in,in_time_slice,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag,output_message)
       use ids_schemas                       ! module containing the equilibrium type definitions
       type(ids_equilibrium), intent(in)          :: equil_in
       type(ids_equilibrium_coordinate_system), intent(out) :: d_ddim1, d_ddim2, d_dsqrtdim1norm
       integer, intent(in)                         :: in_time_slice
       integer, intent(out)                        :: error_flag
       character(len=:), allocatable, intent(out) :: output_message
     end SUBROUTINE equil_metric_derivatives
     !
     subroutine write_itm2(equilibrium_out,kitmopt,kitmshot,kitmrun,citmtree)
       use ids_routines
       IMPLICIT NONE
       type(ids_equilibrium) :: equilibrium_out
       character*120         :: citmtree
       integer               :: kitmopt, kitmshot, kitmrun
     end subroutine write_itm2
  end interface
  !
  type(ids_equilibrium_coordinate_system) :: d_ddim1, d_ddim2, d_dsqrtdim1norm
  !
  real(R8) :: tens_def, abspsimin, R0EXP, B0EXP, mu0, ZZ, zchi_a, zsign_psi, zrmag, zzmag, zgamma
  real(R8), allocatable :: sqrtdim1norm(:), dq_dsqrtdim1norm(:)
  real(R8), allocatable :: thetastraight(:,:), lnR2oJ(:,:), dlnR2oJ_ddim1(:,:), dlnR2oJ_ddim2(:,:), zz2(:), &
    & dim2plus(:), ZTETA(:), ZRHO(:)
  integer :: nbequil, iequil, idim1, idim2, inum
  integer :: iexp_Bp, isigma_Bp, isigma_RphiZ, isigma_rhothetaphi, isign_q_pos, isign_pprime_pos
  integer :: ndim1, ndim2
  character*120  :: citmtree='ids'
  integer, allocatable :: ind_sort1(:), ind_sort2(:), ind_sort3(:)
  real(R8), allocatable :: tmpvec(:)
  !  
  ! Compute metrics derivatives
  !
  call equil_metric_derivatives(equil_in_slice,in_time_slice,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag,output_message)
  if (error_flag < 0) then
    write(0,*)'in EQ_from_equil: error recieved from equil_metric_derivatives'
  endif
  tens_def = -0.1_R8
  !
  ! Use only 1st equil_in
  !
  ndim1 = size(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1)
  ndim2 = size(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2)
  !
  ! assume cocos=11 on ITER ids
  cocos_in = 11
  CALL COCOS(COCOS_IN,iexp_Bp,isigma_Bp,isigma_RphiZ,isigma_rhothetaphi,isign_q_pos,isign_pprime_pos)
  ! To have increasing psi (dim1) we need: d|psi-psi_axis| = sigma_Ip sigma_Bp dpsi
  zsign_psi = real(isigma_Bp * sign(1._R8,equil_in_slice%time_slice(in_time_slice)%global_quantities%ip),R8)
  !
  ! Find the indexes, ind_sort1 and ind_sort2, that orders the arrays ...grid%dim1*zsign_psi and ...grid%dim2
  allocate( ind_sort1(ndim1) )
  allocate( ind_sort2(ndim2) )
  call simple_sort(zsign_psi*equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1, ind_sort1)
  call simple_sort(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2, ind_sort2)
  if (minval(ind_sort1) /= 1) then
     error_flag=-1
     output_message='ERROR in EQ_from_equil_IMAS, minval(ind_sort1) /= 1'
     return
  end if
  if (maxval(ind_sort1) /= ndim1) then
     error_flag=-1
     output_message='ERROR in EQ_from_equil_IMAS, maxval(ind_sort1) /= ndim1'
     return
  end if
  if (minval(ind_sort2) /= 1) then
     error_flag=-1
     output_message='ERROR in EQ_from_equil_IMAS, minval(ind_sort2) /= 1'
     return
  end if
  if (maxval(ind_sort2) /= ndim2) then
     error_flag=-1
     output_message='ERROR in EQ_from_equil_IMAS, maxval(ind_sort2) /= ndim2'
     return
  end if
  !
  ! Values for normalization
  abspsimin = abs(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(ndim1) &
    & - equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(1))
  mu0 = 2.e-07_R8 * twopi
  ! R0EXP = equil_in_slice%time_slice(in_time_slice)%global_quantities%magnetic_axis%r
  ! B0EXP = equil_in_slice%time_slice(in_time_slice)%global_quantities%magnetic_axis%b_field_tor &
  !   & * equil_in_slice%vacuum_toroidal_field%r0 / R0EXP
  R0EXP = equil_in_slice%time_slice(in_time_slice)%global_quantities%magnetic_axis%r
  B0EXP = equil_in_slice%time_slice(in_time_slice)%global_quantities%magnetic_axis%b_field_tor
  !
  ! Assume equil_in CPO has psi, chi as coord_sys, thus first check
  ! Should know from grid_type, but not yet sure so check with profiles_1d.psi
  zz = sum(abs(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1 &
    & - equil_in_slice%time_slice(in_time_slice)%profiles_1d%psi))
  if (zz .gt. 1.E-4*abs(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(1) &
    & + equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(ndim1))) then
    write(0,*) ' It seems equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1 is not psi, ', &
      & 'since different from equil_in_slice%time_slice(in_time_slice)%profiles_1d%psi'
    write(0,*) 'equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1 = ', &
      & equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1
    write(0,*) 'equil_in_slice%time_slice(in_time_slice)%profiles_1d%psi = ', &
      & equil_in_slice%time_slice(in_time_slice)%profiles_1d%psi
    return
  end if
  !
  ! Pre-compute some arrays
  !
  allocate(sqrtdim1norm(ndim1))
  allocate(dq_dsqrtdim1norm(ndim1))
  sqrtdim1norm(1:ndim1) = sqrt((equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(1:ndim1) &
    &                - equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(1)) &
    & / (equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(ndim1) &
    &    - equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(1)))
  !
  ! straight-field line coordinate thetastar = 1/q int(0,chi) F_dia Jacobian_psichiphi/R^2 dchi
  ! chi being dim2
  allocate(thetastraight(ndim1,ndim2))
  do idim1=1,ndim1
    call interpos(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort2(1:ndim2)), &
      & equil_in_slice%time_slice(in_time_slice)%profiles_1d%F(idim1) &
      & * equil_in_slice%time_slice(in_time_slice)%coordinate_system%jacobian(idim1,ind_sort2(1:ndim2)) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(idim1,ind_sort2(1:ndim2))**2, &
      & ndim2,tension=tens_def, youtint=thetastraight(idim1,ind_sort2(1:ndim2)),nbc=-1,ybc=twopi)
    thetastraight(idim1,ind_sort2(1:ndim2)) = thetastraight(idim1,ind_sort2(1:ndim2)) &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(idim1)
  end do
  !
  ! d ln(R^2/J)
  !
  allocate(lnR2oJ(ndim1,ndim2))
  allocate(dlnR2oJ_ddim1(ndim1,ndim2))
  allocate(dlnR2oJ_ddim2(ndim1,ndim2))
  allocate(zz2(ndim2))
  do idim1=1,ndim1
    EQ(19,1:ndim2,idim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(idim1,1:ndim2)**2 &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%jacobian(idim1,1:ndim2)/B0EXP/R0EXP
    lnR2oJ(idim1,1:ndim2) = log(EQ(19,1:ndim2,idim1))
    call interpos(equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort2(1:ndim2)), &
      & lnR2oJ(idim1,ind_sort2(1:ndim2)),ndim2,tension=tens_def,youtp=dlnR2oJ_ddim2(idim1,ind_sort2(1:ndim2)),nbc=-1,ybc=twopi)
  end do
  do idim2=1,ndim2
    call interpos(zsign_psi*equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort1), &
      & zsign_psi*lnr2oj(ind_sort1(1:ndim1),idim2),ndim1,tension=tens_def, &
      & youtp=dlnr2oj_ddim1(ind_sort1(1:ndim1),idim2),nbc=(/2,2/),ybc=(/lnr2oj(ind_sort1(1),idim2), lnr2oj(ind_sort1(ndim1),idim2)/))
  end do
  !
  ! dim2 related. Cannot find exact EQ(2) and (4) but assume almost equidistant near chi(1)=-delta
  zchi_a = (twopi - equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2(ndim2)) / 2._R8
  EQ(2,1,1:ndim1) = -zchi_a;
  EQ(4,ndim2,1:ndim1) = twopi - zchi_a;
  EQ(6,1,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2(1)
  do idim2=2,ndim2
    ! chim has been put in dim2
    EQ(6,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim2(idim2)
    EQ(2,idim2,1:ndim1) = 2._R8*EQ(6,idim2-1,1:ndim1) - EQ(2,idim2-1,1:ndim1)
    EQ(4,idim2-1,1:ndim1) = EQ(2,idim2,1:ndim1)
  end do
  !
  ! dim1 related
  ZGAMMA     = 5._R8 / 3._R8
  DO idim2=1,ndim2
    EQ(1,idim2,1:ndim1) = sqrtdim1norm(1:ndim1)
    EQ(3,idim2,1:ndim1-1) = sqrtdim1norm(2:ndim1)
    EQ(5,idim2,1:ndim1-1) = 0.5_R8*(sqrtdim1norm(1:ndim1-1) + sqrtdim1norm(2:ndim1))
    EQ(5,idim2,ndim1) = sqrtdim1norm(ndim1)
    EQ(8,idim2,1:ndim1) = zgamma*equil_in_slice%time_slice(in_time_slice)%profiles_1d%pressure &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1)/abspsimin*mu0*R0EXP**2/B0EXP
    EQ(9,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%profiles_1d%F(1:ndim1) / R0EXP / B0EXP
    ! 10: free but seems psi was in there, good to have
!!$    EQ(10,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1
    EQ(11,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1:ndim1) &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1)
    EQ(12,idim2,2:ndim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(2:ndim1) &
      & * equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(2:ndim1,idim2)**2 &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      & / R0EXP**2 * B0EXP
    EQ(13,idim2,2:ndim1) = 2._R8 * sqrtdim1norm * abspsimin &
      & * equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,2) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1)
    EQ(14,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2)**2 &
      & / R0EXP**2
    EQ(15,idim2,1:ndim1) = 2._R8 * d_dsqrtdim1norm%r(1:ndim1,idim2) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2)
    EQ(16,idim2,1:ndim1) = 2._R8 * d_ddim2%r(1:ndim1,idim2) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2) 
    EQ(27,idim2,1:ndim1) = - (equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2) &
      &    * mu0 * equil_in_slice%time_slice(in_time_slice)%profiles_1d%dpressure_dpsi(1:ndim1) &
      & + equil_in_slice%time_slice(in_time_slice)%profiles_1d%f_df_dpsi(1:ndim1) &
      &    / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2)) * R0EXP / B0EXP
    !
    ! d/d_|n = d/ddim1_at_cst_dim2 + g_12/g_11 d/ddim2_at_cst_dim1
    EQ(28,idim2,2:ndim1) = 0.5_R8 / equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      & * ( d_ddim1%tensor_contravariant(2:ndim1,idim2,1,1) + &
      &    equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,2) &
      &    / equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      &      * d_ddim2%tensor_contravariant(2:ndim1,idim2,1,1) ) * R0EXP**2 * B0EXP
    EQ(29,idim2,1:ndim1) = dlnR2oJ_ddim1(1:ndim1,idim2) * R0EXP**2 * B0EXP
!!$    EQ(29,idim2,1:ndim1) = 2._R8 / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2) * &
!!$      & d_ddim1%r(1:ndim1,idim2)*R0EXP**2 * B0EXP - &
!!$      & R0EXP**2 * B0EXP / equil_in_slice%time_slice(in_time_slice)%coordinate_system%jacobian(1:ndim1,idim2) * d_ddim1%jacobian(1:ndim1,idim2)
    EQ(17,idim2,2:ndim1) = - 2._R8 * sqrtdim1norm(2:ndim1) * abspsimin/R0EXP**2/B0EXP &
      & * (equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(2:ndim1,idim2) &
      & * EQ(27,idim2,2:ndim1)/equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      & * R0EXP*B0EXP**2 - EQ(29,idim2,2:ndim1))
    EQ(18,idim2,2:ndim1) = 2._R8*equil_in_slice%time_slice(in_time_slice)%coordinate_system%grid%dim1(2:ndim1)/R0EXP**2/B0EXP &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1) * ( &
      & EQ(27,idim2,2:ndim1)**2/equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      &   * R0EXP**2 * B0EXP**2 &
      & - EQ(27,idim2,2:ndim1)/equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(2:ndim1,idim2)*R0EXP &
      &   * EQ(28,idim2,2:ndim1) &
      & - equil_in_slice%time_slice(in_time_slice)%profiles_1d%dpressure_dpsi(2:ndim1)*mu0*R0EXP**4*(d_ddim1%r(2:ndim1,idim2) &
      &   + equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,2) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,1) &
      &   * d_ddim2%r(1:ndim1,idim2)) )
    EQ(20,idim2,1:ndim1) = dlnR2oJ_ddim2(1:ndim1,idim2)
    call interpos(sqrtdim1norm,equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1:ndim1)*thetastraight(1:ndim1,idim2),ndim1, &
      & tension=tens_def,youtp=EQ(21,idim2,1:ndim1),nbc=(/2,2/), &
      & ybc=(/equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1)*thetastraight(1,idim2), &
      &       equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(ndim1)*thetastraight(ndim1,idim2)/))
    EQ(23,idim2,1:ndim1) = d_ddim2%tensor_contravariant(1:ndim1,idim2,1,1) / R0EXP**2 / B0EXP**2
    EQ(24,idim2,1:ndim1) = thetastraight(1:ndim1,idim2)
    EQ(26,idim2,1:ndim1) = equil_in_slice%time_slice(in_time_slice)%profiles_1d%f_df_dpsi(1:ndim1) &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%F(1:ndim1) * R0EXP
  END DO
  do idim2=1,ndim2-1
    ! 25: chistraight at ichi+1
    EQ(25,idim2,1:ndim1) = EQ(24,idim2+1,1:ndim1)
  end do
  EQ(25,ndim2,1:ndim1) = twopi
  ! Special case for EQ(21):
  call interpos(sqrtdim1norm,equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1:ndim1),ndim1,tension=tens_def, &
    & youtp=dq_dsqrtdim1norm(1:ndim1),nbc=(/2,2/), &
    & ybc=(/equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1), equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(ndim1)/))
  if (mod(ndim2,2) .EQ. 0) then
    do idim2=ndim2/2+2,ndim2
      EQ(21,idim2,1:ndim1) = EQ(21,idim2,1:ndim1) - twopi * dq_dsqrtdim1norm(1:ndim1)
    END do
  else
    do idim2=(ndim2+1)/2+2,ndim2
      EQ(21,idim2,1:ndim1) = EQ(21,idim2,1:ndim1) - twopi * dq_dsqrtdim1norm(1:ndim1)
    END do
  end if
  ! 22 not used but fill in while checking. It is OK
  do idim2=1,ndim2
    call interpos(sqrtdim1norm(1:ndim1),thetastraight(1:ndim1,idim2),ndim1,tension=tens_def, &
      & youtp=EQ(22,idim2,1:ndim1),nbc=(/2,2/), ybc=(/thetastraight(1,idim2), thetastraight(ndim1,idim2)/))
    EQ(22,idim2,1:ndim1) = EQ(22,idim2,1:ndim1) + &
      & EQ(13,idim2,1:ndim1) * equil_in_slice%time_slice(in_time_slice)%profiles_1d%F(1:ndim1) &
      & / equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(1:ndim1) &
      & * equil_in_slice%time_slice(in_time_slice)%coordinate_system%jacobian(1:ndim1,idim2) &
      & / equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2)**2
  end do
  !
  EQ(7,1:ndim2,1:ndim1) = 1.0_R8
  !
  ! Values on axis
  !
  DO idim2=1,ndim2
    call interpos(EQ(1,idim2,2:ndim1),EQ(12,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(12,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(12,idim2,2), EQ(12,idim2,ndim1)/))
    ! Special treatment of 13. Here we need to replace both EQ(13,:,1) and EQ(13,:,2)
    call interpos(EQ(1,idim2,3:ndim1),EQ(13,idim2,3:ndim1),ndim1-1,xscal=EQ(1,idim2,2),tension=tens_def, &
      & yscal=EQ(13,idim2,2),nbcscal=(/2,2/),ybcscal=(/EQ(13,idim2,3), EQ(13,idim2,ndim1)/))
    call interpos(EQ(1,idim2,3:ndim1),EQ(13,idim2,3:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(13,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(13,idim2,3), EQ(13,idim2,ndim1)/))
    !
    call interpos(EQ(1,idim2,2:ndim1),EQ(17,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(17,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(17,idim2,2), EQ(17,idim2,ndim1)/))
    call interpos(EQ(1,idim2,2:ndim1),EQ(18,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(18,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(18,idim2,2), EQ(18,idim2,ndim1)/))
    call interpos(EQ(1,idim2,2:ndim1),EQ(22,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(22,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(22,idim2,2), EQ(22,idim2,ndim1)/))
    call interpos(EQ(1,idim2,2:ndim1),EQ(28,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(28,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(28,idim2,2), EQ(28,idim2,ndim1)/))
    call interpos(EQ(1,idim2,2:ndim1),EQ(29,idim2,2:ndim1),ndim1-1,xscal=EQ(1,idim2,1),tension=tens_def, &
      & yscal=EQ(29,idim2,1),nbcscal=(/2,2/),ybcscal=(/EQ(29,idim2,2), EQ(29,idim2,ndim1)/))
    !write(*,*)
    !write(*,*)'EQ_from_equil_ITM.f90: What does the limit for r->0 look like? For idim2=', idim2
    !write(*,'(A20,5F10.4)')'   EQ(12,idim2,1:5)=', EQ(12,idim2,1:5)
    !write(*,'(A20,5F10.4)')'   EQ(13,idim2,1:5)=', EQ(13,idim2,1:5)
    !write(*,'(A20,5F10.0)')'   EQ(28,idim2,1:5)=', EQ(28,idim2,1:5)
    !write(*,'(A20,5F10.2)')'   EQ(17,idim2,1:5)=', EQ(17,idim2,1:5)
    !write(*,'(A20,5F10.4)')'   EQ(18,idim2,1:5)=', EQ(18,idim2,1:5)
  END DO
  !
  ! Values transferred via NVAC (since needs dR/dchi)
  !
  SR(1:ndim2) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(ndim1,1:ndim2) / R0EXP
  SZ(1:ndim2) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%z(ndim1,1:ndim2) / R0EXP
  SR(ndim2+1) = SR(1)
  SZ(ndim2+1) = SZ(1)
  CHI(1:ndim2) = EQ(2,1:ndim2,ndim1)
  CHI(ndim2+1) = twopi + EQ(2,1,ndim1)
  QB = equil_in_slice%time_slice(in_time_slice)%profiles_1d%q(ndim1)
  TB = equil_in_slice%time_slice(in_time_slice)%profiles_1d%F(ndim1) / R0EXP / B0EXP
  R2J(1:ndim2) = EQ(19,1:ndim2,ndim1)
  DCR2J(1:ndim2) = EQ(20,1:ndim2,ndim1)
  CHIOLD(1:ndim2) = EQ(24,1:ndim2,ndim1)
  CHIOLD(ndim2+1) = CHIOLD(1) + twopi
  ! Compute on normlized quantities
  ZRMAG = equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(1,1) / R0EXP
  ZZMAG = equil_in_slice%time_slice(in_time_slice)%coordinate_system%z(1,1) / R0EXP
  ! T is theta at (R,Z) on EQ(6) mid-points
  T(1) = atan2(SZ(1)-ZZMAG,SR(1)-ZRMAG)
  do idim2=2,ndim2
    T(idim2) = atan2(SZ(idim2)-ZZMAG,SR(idim2)-ZRMAG)
    if (T(idim2) .LT. 0._R8) T(idim2) = T(idim2) + twopi
  end do
  T(ndim2+1) = T(1) + twopi
  ! TH is theta on EQ(2) points, need to interpolate
  allocate(dim2plus(ndim2+2))
  dim2plus(1) = EQ(6,ndim2,ndim1) - twopi
  dim2plus(2:ndim2+1) = EQ(6,1:ndim2,ndim1)
  dim2plus(ndim2+2) = EQ(6,1,ndim1) + twopi
  !
  allocate(ind_sort3(ndim2+2))
  call simple_sort(dim2plus(1:ndim2+2),ind_sort3)
  call interpos(dim2plus(ind_sort3(1:ndim2+2)), &
       (/ T(ind_sort3(ndim2))-twopi, T(ind_sort3(1:ndim2+1)) /), &
       NIN=ndim2+2, &
       tension=tens_def, &
       xout=EQ(2,ind_sort3(1:ndim2),ndim1), &
       yout=TH(ind_sort3(1:ndim2)),nbc=(/2, 2/), &
       ybc=(/T(ind_sort3(ndim2))-twopi, T(ind_sort3(ndim2+1))/))
  deallocate(ind_sort3)
  !
  TH(ndim2+1) = TH(1)+ twopi
  ROMID(1:ndim2) = sqrt((SR(1:ndim2)-ZRMAG)**2 + (SZ(1:ndim2)-ZZMAG)**2)
  ROMID(ndim2+1) = ROMID(1)

  call interpos(EQ(6,ind_sort2(1:ndim2),ndim1),ROMID(ind_sort2(1:ndim2)),NIN=ndim2,tension=tens_def, &
        & xout=EQ(2,ind_sort2(1:ndim2),ndim1), yout=ROEDGE(ind_sort2(1:ndim2)),nbc=-1,ybc=twopi)

  ROEDGE(ndim2+1) = ROEDGE(1)
  ! on Gaussian points for TH intervals
  THINT(1:ndim2,2) = 0.5_R8 * (TH(1:ndim2) + TH(2:ndim2+1))
  THINT(1:ndim2,1) = THINT(1:ndim2+1,2) - (TH(2:ndim2+1)-TH(1:ndim2))/sqrt(6._R8)
  THINT(1:ndim2,3) = THINT(1:ndim2+1,2) + (TH(2:ndim2+1)-TH(1:ndim2))/sqrt(6._R8)
  call interpos(T(1:ndim2),ROMID(1:ndim2),NIN=ndim2,tension=tens_def, &
        & xout=THINT(1:ndim2,1), yout=ROINT(1:ndim2,1),youtp=DROINT(1:ndim2,1),nbc=-1,ybc=twopi)
  call interpos(T(1:ndim2),ROMID(1:ndim2),NIN=ndim2,tension=tens_def, &
        & xout=THINT(1:ndim2,2), yout=ROINT(1:ndim2,2),youtp=DROINT(1:ndim2,2),nbc=-1,ybc=twopi)
  call interpos(T(1:ndim2),ROMID(1:ndim2),NIN=ndim2,tension=tens_def, &
        & xout=THINT(1:ndim2,3), yout=ROINT(1:ndim2,3),youtp=DROINT(1:ndim2,3),nbc=-1,ybc=twopi)
  !  write(23,'(i4,1p9e14.6)') (idim2,THINT(idim2,1),THINT(idim2,2),THINT(idim2,3),ROINT(idim2,1),ROINT(idim2,2),ROINT(idim2,3),DROINT(idim2,1),DROINT(idim2,2),DROINT(idim2,3),idim2=1,ndim2)
  !
  ! Values transferred via NDES
  !
  DO idim1=1,NPSI
    CCR(idim1,1:NPOL) = 0.5_R8*(equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(idim1,1:NPOL) &
      & + equil_in_slice%time_slice(in_time_slice)%coordinate_system%r(idim1+1,1:NPOL))
    CCZ(idim1,1:NPOL) = 0.5_R8*(equil_in_slice%time_slice(in_time_slice)%coordinate_system%z(idim1,1:NPOL) &
      & + equil_in_slice%time_slice(in_time_slice)%coordinate_system%z(idim1+1,1:NPOL))
  END DO
  CCR = CCR / R0EXP
  CCZ = CCZ / R0EXP
  DO idim1=1,NPSI+1
    CNR2(idim1,1:NPOL) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,1:ndim2,1,1) &
      &   * d_ddim1%r(idim1,1:ndim2) &
      & + equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,1:ndim2,1,2) &
      &   * d_ddim2%r(idim1,1:ndim2)
    CNZ2(idim1,1:NPOL) = equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,1:ndim2,1,1) &
      &   * d_ddim1%z(idim1,1:ndim2) &
      & + equil_in_slice%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,1:ndim2,1,2) &
      &   * d_ddim2%z(idim1,1:ndim2)
  END DO
  DO idim1=1,NPSI
    CNR(idim1,1:NPOL) = 0.5_R8*(CNR2(idim1,1:NPOL)+CNR2(idim1+1,1:NPOL)) /R0EXP/B0EXP
    CNZ(idim1,1:NPOL) = 0.5_R8*(CNZ2(idim1,1:NPOL)+CNZ2(idim1+1,1:NPOL)) /R0EXP/B0EXP
  END DO
!!$  write(23,'(1p4e14.6)') (CCR(20,idim2), CCZ(20,idim2), CNR(20,idim2), CNZ(20,idim2), idim2=1,ndim2)
  !
  INUM = 12*40
  ALLOCATE(ZTETA(INUM))
  ALLOCATE(ZRHO(INUM))
  ZTETA(1:INUM) = TWOPI/REAL(INUM,R8) * (/ (REAL(IDIM1,R8) -1.5_R8, IDIM1=1,INUM) /)
  call interpos(T(1:ndim2),ROMID(1:ndim2),NIN=ndim2,tension=tens_def, &
        & xout=ZTETA(1:INUM), yout=ZRHO(1:INUM),nbc=-1,ybc=twopi)
  XS(1:INUM) = ZRMAG + ZRHO(1:INUM) * cos(ZTETA(1:INUM))
  YS(1:INUM) = ZZMAG + ZRHO(1:INUM) * sin(ZTETA(1:INUM))
!  write(23,'(1p2e14.6)') (XS(idim2), YS(idim2), idim2=1,inum)
  !
  deallocate(ind_sort1)
  deallocate(ind_sort2)
  !
END SUBROUTINE EQ_from_equil
