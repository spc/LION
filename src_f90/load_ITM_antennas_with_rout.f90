subroutine load_itm_antennas(antennas_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the antennas type definitions
  use euITM_routines
  IMPLICIT NONE
  type(type_antennas),pointer      :: antennas_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='antennas'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call euitm_open(citmtree,kitmshot,kitmrun,idx)
  call euitm_get(idx,signal_name,antennas_out)

  if (size(antennas_out) .ge. 1) then
    if (associated(antennas_out(1)%datainfo%comment)) then
      do i=1,size(antennas_out(1)%datainfo%comment)
        write(6,'(A)') trim(antennas_out(1)%datainfo%comment(i))
      end do
    end if
    
    print *,' antennas_out(1)%time= ',antennas_out(1)%time
  end if
  return
end subroutine load_itm_antennas
