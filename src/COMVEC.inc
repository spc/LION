C---------------------------------------------------------------------
CL                  C3.4     VECTORS FOR LION34
C VERSION 16     LDV      MAY 1995           CRPP LAUSANNE
       COMMON/COMVEC/
     C   SOURCT,
     C   U     ,   UT    ,   VA    ,   X     ,   XOHMR ,   XT    ,
     R   XDCHI ,   XDTH
       COMPLEX
     C   SOURCT,
     C   U     ,   UT    ,   VA    ,   X     ,   XOHMR ,   XT
       DIMENSION
     C   SOURCT(MDCOMP),
     C   U( MDCOL),          VA( MDCOL),         X( MDCOL),
     C   XOHMR( MDPOL),      XT(MDCOMP),
     R   XDCHI( MDPOL),      XDTH( MDPOL)
