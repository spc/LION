C---------------------------------------------------------------------
CL                 N1        NAMELIST
C VERSION 16    LDV       MAY 1995           CRPP   LAUSANNE
C
       NAMELIST /NEWRUN/
     R   ACHARG,   AD,       AHEIGT,   ALARG,    AMASS,    AMASSE,   
     R   ANGLET,   ANTRAD,   ANTRADMAX,ANTUP,    ANU,      ARSIZE,      
     R   ASPCT,    ASYMB,    ATE,      ATI,      ATIP,     BNOT,
     R	 CEN0,     CENDEN,   CENTE,    CENTI,    CENTIP,   
     R   CEOMCI,   CPSRF,    CURASY,   CURSYM,   DELTA,    DELTAF,   
     R   ELLIPT,   EPSMAC,   EQALFD,   EQDENS,   EQFAST,   EQKAPD,   
     R   EQKAPF,   EQKAPT,   EQKPTE,   EQTE,     EQTI,     FEEDUP,   
     R   FRAC,     FRCEN,    FRDEL,    FREQCY,   OMEGA,    QIAXE,    
     R   RMAJOR,   SAMIN,    SAMAX,    SIGMA,    THANT,    THANTW,   
     R   VBIRTH,   WALRAD,   WNTDEL,   WNTORO,
     I   LENGTH,   MANCMP,   MEQ,      MFL,      MPOLWN,   NANTSHEET,
     I	 NANTYP,   NBCASE,   NBTYPE, 
     I   NCHI,     NCOLMN,   NCONTR,   NCUT,     NDA,      NDARG,      
     I   NDDEG,    NDENS,    NDES,     NDLT,     NDS,      NELDTTMP, 
     I   NFAKAP,   NHARM,    NPLTYP,   NPOL,     NPRNT,    NPSI,    
     I   NREAD,    NRSPEC,   NRUN,     NSADDL,   NSAVE,    
     I   NSOURC,   NTEMP,    NTORSP,   NUMBER,
     L   NVAC,     NLCOLD,   NLCOLE,   NLDIP,    NLDISO,   NLPHAS,
     L   NLFAST,   NLOTP0,   NLOTP1,   NLOTP2,   NLOTP3,   NLOTP4,   
     L	 NLOTP5,   NLPLO5,   NLTTMP
