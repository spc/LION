function [matrix_size_Gbytes]=MATRIXSIZE(MDPSI,MDPOL,MDSTORE)

% Note that MDPOL = 2*MDCHI-2

MDCOL = 3*MDPOL;

dimrow=1-MDSTORE+2*MDPOL*MDCOL*MDSTORE
dimcol=1-MDSTORE+MDPSI*MDSTORE

matrixsize_Mwords=dimrow*dimcol*2/2^20 % *2 because it is complex
matrixsize_Gbytes=matrixsize_Mwords*8/2^10 % *8 because double precision

%blocksize_words=MDCOL*MDCOL*2
%blocksize_Gbytes=blocksize_words*8/2^30
