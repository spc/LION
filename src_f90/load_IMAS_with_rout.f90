subroutine load_itm(equil_in,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals ! contains ids_schemas
  use ids_routines
  IMPLICIT NONE
  type(ids_equilibrium)      :: equil_in
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='equilibrium'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call imas_open(citmtree,kitmshot,kitmrun,idx)
  call ids_get(idx,signal_name,equil_in)

  if (associated(equil_in%ids_properties%comment)) then
    do i=1,size(equil_in%ids_properties%comment)
      write(6,'(A)') trim(equil_in%ids_properties%comment(i))
    end do
  end if
    
  if (equil_in%ids_properties%homogeneous_time .eq. 1) then
    if (nverbose .ge. 3) write(*,*) ' equil_in%time(1) = ',equil_in%time(1)
  else
    if (nverbose .ge. 3) write(*,*) ' equil_in%time_slice(1)%time = ',equil_in%time_slice(1)%time
  end if

  return
end subroutine load_itm
