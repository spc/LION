C--------------------------------------------------------------------
CL                 C9.2      VACUUM QUANTITIES FOR LION2
C VERSION 12     LDV      MAY 1991           CRPP LAUSANNE
       COMMON / /
     R   CA,       CANTEN,   CB,       CC,       CHIOLD,   DC,           
     R   DELTH,    DROINT,   DRODT,    DROPDT,   D2ROP,    G,               
     R   QB,       RO,       ROEDGE,   ROINT,    ROMID,    ROP,            
     R   RO2,      RO2P,     SR,       SZ,       T,        TB,              
     R   TH,       THINT,    TP,       TT,       DCR2J,    R2J,
     +   ANTR,     ANTRX,    SAUTR,    SAUTX,    SOURCE,   OHMR,     
     +   REASCR,   WVAC,
     +   NDIM
       COMPLEX
     +   ANTR,     ANTRX,    SAUTR,    SAUTX,    OHMR,     REASCR,   
     +   SOURCE,   WVAC
       DIMENSION
     +   CHIOLD(MDPOL+1),
     +   DC(MD2CP2),    DELTH(MD2CP2), DRODT(4),      DROPDT(4),    
     +   G(MDPOL,MDPOL,13),  RO(4),    ROEDGE(MD2CP2),ROMID(MD2CP2),
     +   ROP(4),        SR(MD2CP2),    SZ(MD2CP2),    T(MD2CP2),    
     +   TH(MD2CP2),    TP(4),         TT(4),
     +   ANTR(MDPOL),   ANTRX(MDPOL),  SAUTR(MDPOL),  SAUTX(MDPOL),  
     +   SOURCE(MDPOL),
     +   GFAT(MDPOL,MDPOL),  HFAT(MDPOL,MDPOL),  RFAT(MDPOL,MDPOL),
     +   FATK(MDPOL,MDPOL),  FATL(MDPOL,MDPOL),  FATM(MDPOL,MDPOL),
     +   FATN(MDPOL,MDPOL),  OHMR(MDPOL),
     +   A(MDPOL,MDPOL),     B(MDPOL,MDPOL),     C(MDPOL,MDPOL),
     +   D(MDPOL,MDPOL),     E(MDPOL,MDPOL),     F(MDPOL,MDPOL),
     +   WVAC(MDPOL,MDPOL),
     +   DROINT(MDPOL,3),    ROINT(MDPOL,3),     THINT(MDPOL,3),
     +   DCR2J(MDPOL),       R2J(MDPOL)
       EQUIVALENCE
     +   (A(1,1),G(1,1,1)),(B(1,1),G(1,1,2)),(C(1,1),G(1,1,3)),
     +   (D(1,1),G(1,1,4)),(E(1,1),G(1,1,5)),(F(1,1),G(1,1,6)),
     +   (GFAT,G(1,1,7)),    (HFAT,G(1,1,8)),    (FATK,G(1,1,9)),
     +   (FATL,G(1,1,10)),   (FATM,G(1,1,11)),   (FATN,G(1,1,12)),
     +   (RFAT,G(1,1,13))
