!CC*DECK P5C2S02
SUBROUTINE PHYSQ
  !        ----------------
  !
  !  5.2.2. CONSTRUCT DESIRED PHYSICAL QUANTITIES FROM SOLUTION VECTOR
  !-----------------------------------------------------------------------
  USE GLOBALS
!!$  USE COMPLO ! INCLUDE 'COMPLO.inc'
!!$  USE COMPHY ! INCLUDE 'COMPHY.inc'
!!$  USE COMEQU ! INCLUDE 'COMEQU.inc'
!!$  USE COMESH ! INCLUDE 'COMESH.inc'
!!$  USE COMCON ! INCLUDE 'COMCON.inc'
  IMPLICIT NONE
  !
  INTEGER :: J, ISPC2, JS, JCHI, JSPEC, JM, icount_diag
  REAL(RKIND) :: ZTRA(MDSPC2),    ZFLPSP(MDSPC2), ZFLPW, ZOMCI, ZSL, ZSU, &
       & ZFLCON, ZZ, ZOMCH, ZFAZ, ZMHD2, ZNE0, ZHALFP, ZDCHI
  !-----------------------------------------------------------------------
  !
  !     INITIALIZE THE POWER FLUXES AT EDGES OF S=CONST. CELLS.
  !     ZFLPW = TOTAL FLUX. ZFLPSP() = FLUXES PER SPECIES.
  ISPC2 = NRSPEC + 2
  ZFLPW = 0._RKIND
  DO J=1,ISPC2
    ZFLPSP(J) = 0._RKIND
  END DO
  !
  !--------------------------------------------------------------------
  !L            LOOP ON MAGNETIC SURFACES ("RADIAL") INDEX JS
  !--------------------------------------------------------------------
  !
  DO JS=1,NPSI
    !lvC
    !lvC     READ EQUILIBRIUM QUANTITIES
    !lv         CALL IODSK5(3)
    !
    !     COUNTERS FOR ABSORPTION PER SPECIES
    DO J=1,ISPC2
      ZTRA(J) = 0._RKIND
    END DO
    !
    !-------------------------------------------------------------------
    !              LOOP ON POLOIDAL DIRECTION INDEX JCHI
    !-------------------------------------------------------------------
    DO JCHI=1,NPOL
      !
      !     CONSTRUCT LOCAL (CELL) QUANTITIES
      CALL QUAEQU (JS,JCHI)
      CALL CENTER (JS,JCHI)
      CALL ELECTR
      CALL LOCPOW
      CALL MAGNET
      CALL POYNTI
      !
      !       STORE THE NECESSARY INFORMATION IN ARRAYS
      BN(JS,JCHI)     = BNL
      BB(JS,JCHI)     = BPL
      BPAR(JS,JCHI)   = BPARL
      EN(JS,JCHI)     = ENL   ! Normal (radial) componenet of electric field
      EP(JS,JCHI)     = EPL   ! Perpendicular (poloidal) componenet of electric field
      DENPOW(JS,JCHI) = DPL
      CELPOW(JS,JCHI) = CPL
      SN(JS,JCHI)     = SNL
      SPERP(JS,JCHI)  = SPL
      SPAR(JS,JCHI)   = SPARL
      DSPREL(JS,JCHI) = REAL(WEPS) - WNTORO**2/WR2
      AIMEPS(JS,JCHI) = AIMAG(WEPS)
      COST(JS,JCHI)   = SQRT (WBTOR2) / WBTOT
      SINT(JS,JCHI)   = SQRT (WBPOL2) / WBTOT
      !
      ZOMCI = OMEGA - WOMCI(1)
      DO JSPEC=2,NRSPEC
        IF (WOMCI(JSPEC).NE.WOMCI(JSPEC-1)) THEN
          ZOMCI = ZOMCI * (OMEGA - WOMCI(JSPEC))
        END IF
      END DO
      OMCLIN(JS,JCHI) = ZOMCI
      !
      !     SURFACE-INTEGRATED POWER ABSORPTION
      ABSPOW(JS) = ABSPOW(JS) + CPL/WDS
      DO J = 1, ISPC2
        ABSPSP(JS,J) = ABSPSP(JS,J) + CPLJ(J)/WDS
      END DO
      CHIPOW(JCHI) = CHIPOW(JCHI) + CPL/WDCHI
      !
      !     INTEGRATE POYNTING FLUX THROUGH MAGNETIC SURFACE
      CALL SFLINT (JS)
      !
      !     INTEGRATE D (VOLUME INSIDE MAGN.SURF.) / D (PSI)
      SURPSI(JS) = SURPSI(JS) + WJAC*WDCHI*C2PI
      !
      !     ABSORPTION PER SPECIES (SUM FOR PSI=CONST. CELLS)
      DO J=1,ISPC2
        ZTRA(J) = ZTRA(J) + CPLJ(J)
      END DO
      !
      CCHI(JCHI) = WCHI
      !
    END DO
    !-------------------------------------------------------------------
    !                  END OF JCHI LOOP
    !-------------------------------------------------------------------
    !
    !     PSI=CONST. DIAGNOSTICS
    !
    CCS(JS) = WS
    ZSL = EQ(1,1,js)
    ZSU = EQ(3,1,js)
    !
    !     POWER FLUXES AT CENTER OF THE CELLS
    ZFLCON = ABSPOW(JS) * WDS
    ZZ = (WS*WS - ZSL*ZSL) / (ZSU*ZSU - ZSL*ZSL)
    FLUPOW(JS) = ZFLPW + ZFLCON * ZZ
    DO J=1,ISPC2
      FLUPSP(JS,J) = ZFLPSP(J) + ZTRA(J) * ZZ
    END DO
    !
    !     POWER FLUXES AT EDGE OF CELLS
    ZFLPW  = ZFLPW + ZFLCON
    DO J=1,ISPC2
      ZFLPSP(J) = ZFLPSP(J) + ZTRA(J)
    END DO
    !
    !     AMPLITUDE AND PHASE OF ELECTRIC FIELD ON OUTER EQUATORIAL
    !     MIDPLANE (CHI=0) (ASSUMED TO CORRESPOND TO JCHI=1)
    !
    ENCHI0(JS) = CABS ( EN(JS,1) )
    EPCHI0(JS) = CABS ( EP(JS,1) )
    IF ( REAL(EN(JS,1)) .NE. 0._RKIND ) THEN
      ENPHI0(JS) = ATAN2 ( AIMAG(EN(JS,1)), REAL(EN(JS,1)) )
    ELSE
      ENPHI0(JS) = CPI * 0.5_RKIND
    END IF
    IF ( REAL(EP(JS,1)) .NE. 0._RKIND ) THEN
      EPPHI0(JS) = ATAN2 ( AIMAG(EP(JS,1)), REAL(EP(JS,1)) )
    ELSE
      EPPHI0(JS) = CPI * 0.5_RKIND
    END IF
    !
    !     NORMALIZED ALFVEN CONTINUUM FREQUENCIES FOR M = MFL .. MFU
    !     FOR OMEGA << OMEGA_CI (2ND ORDER) , IN THE APPROXIMATION OF NO
    !     POLOIDAL MODE COUPLING. ASSUMES T_axis=1.0.
    !
    ZOMCH = WOMCI(1) * AMASS(1) / ACHARG(1)
    ZFAZ  = 0.0_RKIND
    !
    DO JSPEC = 1, NRSPEC
      ZFAZ = ZFAZ + WFRAC(JSPEC) * &
           &            ( AMASS(JSPEC) / ACHARG(JSPEC) )**2
    END DO
    !
    DO JM = MFL, MFU
      ZMHD2 = ( WT * (WNTORO + JM/WQ) )**2 / WRHO
      FRALF(JM-MFL+1,JS) = &
           &         SQRT ( ZMHD2 / (1._RKIND + ZFAZ * ZMHD2 / ZOMCH**2 ) )
    END DO
    !
    !     MAJOR AND MINOR RADII, MASS AND ELECTRON DENSITIES, SAFETY 
    !     FACTOR, CENTER OF TAE GAP VS S ON LOW FIELD SIDE EQUATORIAL 
    !     PLANE (CHI=0)
    !     
    EQRDEN(1,JS) = CCR(JS,1)
    EQRDEN(2,JS) = ( CCR(JS,1) - 1._RKIND ) / ( XS(1) - 1._RKIND)
    EQRDEN(3,JS) = CCR(JS,1) * RMAJOR
    EQRDEN(4,JS) = ( CCR(JS,1) - 1._RKIND ) * RMAJOR
    EQRDEN(5,JS) = WRHO
    EQRDEN(6,JS) = WQ
    EQRDEN(7,JS) = WT / (2.0_RKIND*WQ*SQRT(WRHO))
    ZNE0 = 0._RKIND
    DO JSPEC = 1, NRSPEC
      ZNE0 = ZNE0 + CENDEN(JSPEC) * ACHARG(JSPEC)
    END DO
    EQRDEN(8,JS) = WRHO * ZNE0
    !
  END DO
  !---------------------------------------------------------------------
  !                  END OF JS LOOP
  !---------------------------------------------------------------------
  !
  !     POLOIDAL FOURIER DECOMPOSITION OF ELECTRIC FIELD
  !
  CALL FOURIE
  !
  !     ELECTRON,ION LANDAU+TTMP DAMPING OF ALFVEN WAVE
  !     AND FAST PARTICLE DKE DRIVE
  !
  IF (NLFAST) THEN
    CALL LANDAU
  END IF
  !
  !     FLUX AT PLASMA SURFACE
  !
  FLUPOW(NPSI+1) = ZFLPW
  DO J=1,ISPC2
    FLUPSP(NPSI+1,J) = ZFLPSP(J)
  END DO
  !
  !     SMEAN = RADIUS OF HALF POWER ABSORBED
  !
  ZHALFP = FLUPOW(NPSI+1) * 0.5_RKIND
  DO JS=2,NPSI
    IF (FLUPOW(JS) .GE. ZHALFP) THEN
      SMEAN = (CCS(JS)-CCS(JS-1)) * (ZHALFP-FLUPOW(JS-1)) / &
           &              (FLUPOW(JS)-FLUPOW(JS-1))  +  CCS(JS-1)
      GO TO 1101
    END IF
  END DO
1101 CONTINUE
  !
  !     POWER ABSORPTION DENSITY AVERAGED OVER PSI=CONST SURFACE
  !     MULTIPLY WITH TOTAL POWER (WATTS) TO OBTAIN WATTS/M**3
  !
  DO JS=1,NPSI
    DEPOS(JS) = ABSPOW(JS) / (SURPSI(JS) &
         &       * RMAJOR**3 * FLUPOW(NPSI+1) * 2._RKIND * CPSRF * CCS(JS))
    DO J = 1, ISPC2
      DEPPSP(JS,J) = ABSPSP(JS,J) / (SURPSI(JS) &
           &        * RMAJOR**3 * FLUPOW(NPSI+1) * 2._RKIND * CPSRF * CCS(JS))
    END DO

  END DO
  !     
  !     CMEAN = MEAN ANGLE CHI OF ABSORBED POWER
  !     CDEVIA = CHI WIDTH OF POWER ABSORPTION
  !
  DO JCHI=1,NPOL
    ZDCHI = ABS( EQ(4,JCHI,1) - EQ(2,JCHI,1) )
    CMEAN =  CMEAN  + CCHI(JCHI)    * CHIPOW(JCHI) * ZDCHI
    CDEVIA = CDEVIA + CCHI(JCHI)**2 * CHIPOW(JCHI) * ZDCHI
  END DO
  !
  CMEAN = CMEAN / FLUPOW(NPSI)
  CDEVIA = CDEVIA / FLUPOW(NPSI)
  !CCCC    CDEVIA = SQRT(CDEVIA-CMEAN**2)
  !
  icount_diag = size(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag)-1
  write(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag(icount_diag),'(A)') '<!-- -->'  
  write(wavesLION_out(iout_time_slice)%coherentwave(1)%codeparam%output_diag(icount_diag+1),'(A,1PE14.7,A)') '<!--  Power Flux: ',FLUPOW(NPSI+1),'-->'  

  !
  !-------------------------------------------------------------------
  !
  RETURN
END SUBROUTINE PHYSQ
