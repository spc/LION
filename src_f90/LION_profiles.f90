!> Module for spline interpolation of plasma profiles.
module LION_profiles

  use GLOBALS, only: RKIND, cmp

  implicit none

!  integer, private, save :: verbosity = 0
  real(RKIND), private, save :: interpos_tension = 1e-6_RKIND

  !> Representation of the spline-curve in terms of the original value-array,
  !> the smoothed values and it's second derivatives.
  type splinecurve
     real(RKIND), allocatable :: value(:)
     real(RKIND), allocatable :: smoothed_value(:)
     real(RKIND), allocatable :: second_derivative(:)
  end type splinecurve

  type internal_core_profiles_ion
     !> The "density_fraction" is the ratio between the normalised density
     !> profile of a single ion and the normalised mass density profile. 
     !> Here normalised means divided by the on axis value.
     type(splinecurve) :: density
     type(splinecurve) :: density_fraction
     type(splinecurve) :: temperature_parallel
     type(splinecurve) :: temperature_perpendicular
     real(RKIND) :: mass
     real(RKIND) :: charge
     real(RKIND) :: charge_nuclear
     real(RKIND) :: density_on_axis
     integer :: ion_index
     integer :: impurity_index
     logical :: is_fast_population
  end type internal_core_profiles_ion

  type internal_core_profiles_electrons
     type(splinecurve) :: temperature
  end type internal_core_profiles_electrons
 
  type internal_core_profiles
     !> The array "s" is the square root of the poloidal flux, normalised 
     !> to go from zero on axis and unity at the last closed flux surface.
     real(RKIND), allocatable :: s(:)
     !> The "mass-density" describes the plasma mass density profile 
     !> the normalised to unity on axis.
     type(splinecurve) :: mass_density
     type(internal_core_profiles_electrons) :: electrons
     type(internal_core_profiles_ion), allocatable :: ion(:)
  end type internal_core_profiles


  !> Global parameters for storing all needed info about the thermal 
  !> plasma, including the list of species and their kinetic profiles.
  type(internal_core_profiles) :: internal_profs

  real(RKIND) :: MIN_RELATIVE_TEMPERATURE = 0.005_RKIND !< Relative to the on axis temperature
  real(RKIND) :: MIN_RELATIVE_DENSITY = 1e-5_RKIND !< Relative to the on axis temperature

contains

  subroutine free_internal_splinecurve(in)
    type(splinecurve) :: in
    if (allocated(in%value)) deallocate(in%value)
    if (allocated(in%smoothed_value)) deallocate(in%smoothed_value)
    if (allocated(in%second_derivative)) deallocate(in%second_derivative)
  end subroutine free_internal_splinecurve

  subroutine free_internal_core_profiles(in)
    type(internal_core_profiles) :: in
    integer :: ind
    if (allocated(in%s)) deallocate(in%s)
    call free_internal_splinecurve(in%mass_density)
    call free_internal_splinecurve(in%electrons%temperature)

    if (allocated(in%ion)) then
       do ind=1,size(in%ion)
          call free_internal_splinecurve(in%ion(ind)%density)
          call free_internal_splinecurve(in%ion(ind)%density_fraction)
          call free_internal_splinecurve(in%ion(ind)%temperature_parallel)
          call free_internal_splinecurve(in%ion(ind)%temperature_perpendicular)
       end do
       deallocate(in%ion)
    end if
  end subroutine free_internal_core_profiles


  subroutine set_splinecurve(grid, values, spline, return_status)
    use interpos_module, only: interpos
    real(RKIND), intent(in) :: grid(:)
    real(RKIND), intent(in) :: values(:)
    type(splinecurve), intent(inout) :: spline
    integer, intent(out) :: return_status

    ! Local
    integer :: strlen
    integer :: grid_size

    if (size(grid) /= size(values)) then
       write(*,*)'ERROR in set_splinecurve'
       write(*,*)'  size(grid)=,',size(grid),', size(values)=',size(values)
       return_status=-1
       return
    endif
    return_status=0

    allocate(spline%value(size(grid)))
    allocate(spline%smoothed_value(size(grid)))
    allocate(spline%second_derivative(size(grid)))

    spline%value=values

    call interpos( &
         grid , &
         values , &
         spline%smoothed_value , &
         spline%second_derivative , &
         size(grid) , &
         tension=interpos_tension)

  end subroutine set_splinecurve


  subroutine interpolate_profile_at_scalar(profile,xout,yout,min_relative)
    use interpos_module, only: interpos
    type(splinecurve) :: profile
    real(RKIND) :: xout
    real(RKIND) :: yout
    real(RKIND) :: xoutv(1)
    real(RKIND) :: youtv(1)
    real(RKIND), optional :: min_relative

    xoutv(1)=xout
    call interpos( internal_profs%s, &
         profile%smoothed_value , &
         profile%second_derivative, &
         size(internal_profs%s), &
         size(xoutv), &
         xoutv, &
         youtv)
    if (present(min_relative)) then
       yout=max( youtv(1) , min_relative * profile%value(1) )
    else
       yout=youtv(1)
    end if
  end subroutine interpolate_profile_at_scalar


  !> Update the fields DATA%mass_density%*
  !>
  !> This requires that the following fields are already filled:
  !>  - DATA%ion(*)%density%*
  !>  - DATA%ion(*)%mass
  !>
  subroutine update_mass_density(DATA,return_status)
    type(internal_core_profiles), intent(inout) :: DATA
    integer, intent(out) :: return_status

    ! Internal
    real(RKIND), allocatable :: massdens(:)
    integer :: ind, j

    do ind=1,size(DATA%ion)
       if (allocated(DATA%ion(ind)%density%value)) then
          if (.not. allocated(massdens)) then
             allocate(massdens(size(DATA%s)))
             massdens(:) = 0.0_RKIND
          end if
          massdens(:) = massdens(:) + cmp * DATA%ion(ind)%mass * DATA%ion(ind)%density%value(:)
       end if
    end do

    if (.not. allocated(massdens)) then
       write(*,*)'ERROR in update_mass_density, no ions found in DATA%ion(:)'
       return_status = -1
       return
    end if

    call free_internal_splinecurve(DATA%mass_density)
    call set_splinecurve(DATA%s, massdens, DATA%mass_density, return_status)
    if (return_status<0) then
       write(*,*)'in update_mass_density, error recieved from set_splinecurve'
    end if
    deallocate(massdens)
  end subroutine update_mass_density


  !> Update the fields DATA%ion(*)%density_fraction%*
  !>
  !> This requires that the following fields are already filled:
  !>  - DATA%ion(*)%density%*
  !>  - DATA%ion(*)%mass
  !>  - DATA%mass_density
  !>
  !> Note that both DATA%ion(*)%density%value(1) and DATA%mass_density%value(1)
  !> has to be non-zero if or else it will not be possible to normalise
  !> the profiles to the on axis value.
  !>  - DATA%ion(*)%density%*
  !>
  subroutine update_density_fractions(DATA,return_status)
    type(internal_core_profiles), intent(inout) :: DATA
    integer, intent(out) :: return_status

    ! Internal
    real(RKIND), allocatable :: ratio(:)
    integer :: jion, js

    if (.not. allocated(DATA%s)) then
       write(*,*)'ERROR in update_density_fractions, DATA%s not allocated'
       return_status = -1
    end if
    if (.not. allocated(DATA%mass_density%value)) then
       write(*,*)'ERROR in update_density_fractions, DATA%mass_density%value not allocated'
       return_status = -1
    end if
    allocate(ratio(size(DATA%s)))
    
    do jion=1,size(DATA%ion)
       if (allocated(DATA%ion(jion)%density%value)) then

          ratio(:) = 0.0_RKIND
          do js=1,size(DATA%s)
             if (DATA%mass_density%value(js) > 1e-30_RKIND) then
                ratio(js) = DATA%ion(jion)%mass * DATA%ion(jion)%density%value(js) / DATA%mass_density%value(js)
             else
                ratio(js) = 0.0_RKIND
             end if
          end do

          if (ratio(1) < 1e-30_RKIND) then
             return_status = -1
             write(*,*)'ERROR in update_density_fractions, on axis density is zero - we cannot normalise the on axis value'
             return
          end if

          call free_internal_splinecurve(DATA%ion(jion)%density_fraction)
          call set_splinecurve(DATA%s, ratio/ratio(1), DATA%ion(jion)%density_fraction, return_status)

          if (return_status<0) then
             write(*,*)'in update_density_fraction, error recieved from set_splinecurve'
          end if
       end if
    end do

    if (allocated(ratio)) then
       deallocate(ratio)
    end if
  end subroutine update_density_fractions



  subroutine push_internal_profs_to_globals(status_flag)
    use globals, only: MDSPEC, NRSPEC, AMASS, ACHARG, &
         CENDEN, CENTE, CENTI, CENTIP, &
         EQDENS, EQKAPD, &
         EQTI,   EQKAPT, &
         EQTE,   EQKPTE
    ! Output
    integer, intent(out) :: status_flag

    ! Internal
    integer :: jion, ns
    real(RKIND) :: edge_value, min_value

    status_flag = -1
    if (.not. allocated(internal_profs%s)) return
    if (.not. allocated(internal_profs%ion)) return

    ns = size(internal_profs%s)

    !--------------- ION SPEICES -----------------------------------
    write(*,*)'UPDATE: Number of ions:', NRSPEC,' -> ', size(internal_profs%ion)
    NRSPEC = min( size(internal_profs%ion) , MDSPEC )
    if (size(internal_profs%ion) > MDSPEC) then
       write(*,*)'=========================================================='
       write(*,*)'WARNING in LION_profiles::push_internal_profs_to_globals'
       write(*,*)'    Cannot include all ion species in LION.'
       write(*,*)'    Max nr. species in LION:', MDSPEC
       write(*,*)'    Nr. species in coreprof, corefast, coreimpur:',size(internal_profs%ion)
       write(*,*)'=========================================================='
    end if

    write(*,'(A20,999F7.3)')'UPDATE: AMASS from: ', AMASS(1:NRSPEC)
    do jion=1,NRSPEC
       AMASS(jion) = internal_profs%ion(jion)%mass
    end do
    write(*,'(A20,999F7.3)')' -> ', AMASS(1:NRSPEC)

    write(*,'(A20,999F7.3)')'UPDATE: ACHARG from:', ACHARG(1:NRSPEC)
    do jion=1,NRSPEC
       ACHARG(jion) = internal_profs%ion(jion)%charge
    end do
    write(*,'(A20,999F7.3)')' -> ', ACHARG(1:NRSPEC)

    !--------------- CHECK PROFILES -----------------------------------
    do jion=1,NRSPEC
       if (.not. allocated(internal_profs%ion(jion)%density%value)) return
       if (.not. allocated(internal_profs%ion(jion)%temperature_parallel%value)) return
       if (.not. allocated(internal_profs%ion(jion)%temperature_perpendicular%value)) return
    end do

    !--------------- ON AXIS VALUES -----------------------------------
    CENTE = internal_profs%electrons%temperature%value(1)
    do jion=1,NRSPEC
       CENTI(jion)  = internal_profs%ion(jion)%temperature_parallel%value(1)
       CENTIP(jion) = internal_profs%ion(jion)%temperature_perpendicular%value(1)
       CENDEN(jion) = internal_profs%ion(jion)%density%value(1)
    end do

    !--------------- PROFILE FACTORS -----------------------------------
    ! From input xml: EQKPTE     = 2.0_RKIND
    ! From input xml: EQKAPT(:)  = 2.0_RKIND
    ! From input xml: EQKAPD     = 0.5_RKIND
    EQDENS     = 1._RKIND - (0.99_RKIND)**(1_RKIND/EQKAPD)

    edge_value = internal_profs%electrons%temperature%value(ns-4)
    min_value  = 1e-4_RKIND*maxval(internal_profs%electrons%temperature%value)
    EQTE       = 1._RKIND - (max(edge_value,min_value)/CENTE)**(1_RKIND/EQKPTE)

    do jion=1,NRSPEC
       edge_value = internal_profs%ion(jion)%temperature_perpendicular%value(ns-4)
       min_value  = 1e-4_RKIND*maxval(internal_profs%ion(jion)%temperature_perpendicular%value)
       EQTI(jion) = 1._RKIND - (max(edge_value, min_value)/CENTI(jion))**(1_RKIND/EQKAPT(jion))
    end do

    !write(*,*)'SETTING: CENTE  set to:', CENTE
    !write(*,*)'SETTING: CENDEN set to:', CENDEN(1:NRSPEC)
    !write(*,*)'SETTING: CENTI  set to:', CENTI(1:NRSPEC)
    !write(*,*)'SETTING: CENTIP set to:', CENTIP(1:NRSPEC)
    !write(*,*)'SETTING: EQDENS set to:', EQDENS
    !write(*,*)'SETTING: EQTE   set to:', EQTE
    !write(*,*)'SETTING: EQTI   set to:', EQTI(1:NRSPEC)
    !write(*,*)'SETTING: EQKAPD set to:', EQKAPD
    !write(*,*)'SETTING: EQKPTE set to:', EQKPTE
    !write(*,*)'SETTING: EQKAPT set to:', EQKAPT(1:NRSPEC)

    status_flag = 0

  end subroutine push_internal_profs_to_globals



  ! Count nuclei
  integer function number_of_nuclei(DATA)
    type(internal_core_profiles) :: DATA
    integer :: jion1,jion2
    logical :: same
    number_of_nuclei = 0
    if (allocated(DATA%ion)) then
       do jion1=1,size(DATA%ion)
          do jion2=jion1+1,size(DATA%ion)
             if (same_nuclei( DATA%ion(jion1)%mass , DATA%ion(jion2)%mass , &
                  DATA%ion(jion1)%charge_nuclear   , DATA%ion(jion2)%charge_nuclear)) then
                number_of_nuclei = number_of_nuclei + 1
             end if
          end do
       end do
    end if
  end function number_of_nuclei



  ! Count ions
  integer function number_of_ions(DATA)
    type(internal_core_profiles) :: DATA
    integer :: jion1,jion2
    logical :: same
    number_of_ions = 0
    if (allocated(DATA%ion)) then
       do jion1=1,size(DATA%ion)
          do jion2=jion1+1,size(DATA%ion)
             if (same_ion( DATA%ion(jion1)%mass  , DATA%ion(jion2)%mass , &
                  DATA%ion(jion1)%charge_nuclear , DATA%ion(jion2)%charge_nuclear , &
                  DATA%ion(jion1)%charge         , DATA%ion(jion2)%charge)) then
                number_of_ions = number_of_ions + 1
             end if
          end do
       end do
    end if
  end function number_of_ions


  logical function same_nuclei(mass1,mass2,charge1,charge2)
    real(RKIND) :: mass1,charge1,mass2,charge2
    same_nuclei = (abs(mass1-mass2) < 0.2) .and. (abs(charge1-charge2) < 0.2)
  end function same_nuclei


  logical function same_ion(mass1,mass2,nuc_charge1,nuc_charge2,ion_charge1,ion_charge2)
    real(RKIND) :: mass1,mass2,nuc_charge1,nuc_charge2,ion_charge1,ion_charge2
    same_ion = same_nuclei(mass1,mass2,nuc_charge1,nuc_charge2) .and. &
         (abs(ion_charge1-ion_charge2) < 0.2)
  end function same_ion


  ! Write internal profiles, stored in the global internal_profs within the module LION_profiles, to unit.
  subroutine write_internal_profs(unit,indent)
    integer, intent(in) :: unit
    character(*), intent(in), optional :: indent
    character(len=:), allocatable :: indent0
    integer :: j, jion

    if (present(indent)) then
       indent0 = indent
    else
       indent0 = ''
    end if

    write(unit,*)'# LION, internal profiles:'
    call write_yaml_array(unit,'s',internal_profs%s,indent0)
    call write_yaml_array(unit,'mass_density',internal_profs%mass_density%value,indent0)
    write(unit,*)indent0,'electrons:'
    call write_yaml_array(unit,'temperature',internal_profs%electrons%temperature%value,indent0//'  ')
    write(unit,*)indent0//'ion:'
    if (allocated(internal_profs%ion)) then
       do jion=1,size(internal_profs%ion)
          write(unit,*)indent0//' - list_index: ', jion
          write(unit,*)indent0//'   ion_index: ', internal_profs%ion(jion)%ion_index
          write(unit,*)indent0//'   impurity_index: ', internal_profs%ion(jion)%impurity_index
          if (internal_profs%ion(jion)%is_fast_population) then
             write(unit,*)indent0//'   is_fast_population: True'
          else
             write(unit,*)indent0//'   is_fast_population: False'
          end if
          write(unit,*)indent0//'   mass: ', internal_profs%ion(jion)%mass
          write(unit,*)indent0//'   charge: ', internal_profs%ion(jion)%charge
          write(unit,*)indent0//'   density_on_axis: ', internal_profs%ion(jion)%density_on_axis
          call write_yaml_array(unit,'density',internal_profs%ion(jion)%density%value,indent0//'   ')
          call write_yaml_array(unit,'density_fraction',internal_profs%ion(jion)%density_fraction%value,indent0//'   ')
          call write_yaml_array(unit,'temperature_parallel',internal_profs%ion(jion)%temperature_parallel%value,indent0//'   ')
          call write_yaml_array(unit,'temperature_perpendicular',internal_profs%ion(jion)%temperature_perpendicular%value,indent0//'   ')
       end do
    end if

  end subroutine write_internal_profs


  subroutine write_yaml_array(unit,name,values,indent)
    integer, intent(in) :: unit
    character(*), intent(in) :: name
    real(RKIND), intent(in) :: values(:)
    character(*), intent(in) :: indent
    integer :: j
    write(unit,*)indent,name,':'
    do j=1,size(values)
       write(unit,*)indent,' -',values(j)
    end do
  end subroutine write_yaml_array

end module LION_profiles
