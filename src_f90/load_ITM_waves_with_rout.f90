subroutine load_itm_waves(waves_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the waves type definitions
  use euITM_routines
  IMPLICIT NONE
  type(type_waves),pointer      :: waves_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='waves'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call euitm_open(citmtree,kitmshot,kitmrun,idx)
  call euitm_get(idx,signal_name,waves_out)

  if (size(waves_out) .ge. 1) then
    if (associated(waves_out(1)%datainfo%comment)) then
      do i=1,size(waves_out(1)%datainfo%comment)
        write(6,'(A)') trim(waves_out(1)%datainfo%comment(i))
      end do
    end if
    
    print *,' waves_out(1)%time= ',waves_out(1)%time
  end if
  return
end subroutine load_itm_waves
