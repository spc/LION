module LION_profile_tools_IMAS

  implicit none

contains

  !> Initialises the global variable internal_profs, defined in LION_profiles 
  !> using input from the IMAS IDS core_profiles.
  !>
  !> by Thomas 2017-12-10
  subroutine init_internal_profs(core_profiles, time_index, return_status)
    use ids_schemas, only: ids_core_profiles
    use GLOBALS, only: RKIND, cmp
    use LION_profiles, only: internal_profs, free_internal_core_profiles, &
         update_mass_density, update_density_fractions, &
         set_splinecurve

    implicit none

    ! Input
    type(ids_core_profiles) :: core_profiles
    integer, intent(in) :: time_index
    integer, intent(out) :: return_status

    ! Internal
    integer :: npsi
    integer :: ispec, countspec
    integer :: nrspec

    ! Tidy up if anything is allocated already
    call free_internal_core_profiles(internal_profs)

    if (.not. associated(core_profiles%profiles_1d)) then
       write(*,*)'ERROR in LION_profile_tools_IMAS::init_internal_profs, core_profiles%profiles_1d not associated'
       return_status = -1
       return
    end if

    npsi   = size( core_profiles%profiles_1d(time_index)%grid%psi )

    nrspec = 0
    LOOP_IONS_1: do ispec=1,size(core_profiles%profiles_1d(time_index)%ion)
       IF_SINGLE_STATE_1: if (core_profiles%profiles_1d(time_index)%ion(ispec)%multiple_states_flag == 0) then
          nrspec = nrspec + 1
       end if IF_SINGLE_STATE_1
    end do LOOP_IONS_1
    allocate( internal_profs%s(npsi) )
    allocate( internal_profs%ion(nrspec) ) 
    !
    ! Initialise the grid in s=rho_pol=sqrt(psi_normalised)
    internal_profs%s = sqrt( ( &
         core_profiles%profiles_1d(time_index)%grid%psi - &
         core_profiles%profiles_1d(time_index)%grid%psi(1) &
         ) / ( &
         core_profiles%profiles_1d(time_index)%grid%psi(npsi) - &
         core_profiles%profiles_1d(time_index)%grid%psi(1) ) )
    !
    ! Set electron temperature
    call set_splinecurve(internal_profs%s, &
         core_profiles%profiles_1d(time_index)%electrons%temperature, &
         internal_profs%electrons%temperature, return_status)
    if (return_status /= 0) then
       write(*,*)'ERROR in LION_profile_tools_IMAS.f90::init_internal_profs'
       write(*,*)'Failed to generate spline curve for internal_profs%electrons%temperature'
       stop
    end if
    !
    ! Set ion profiles
    countspec = 0
    LOOP_IONS: do ispec=1,size(core_profiles%profiles_1d(time_index)%ion)

       IF_SINGLE_STATE: if (core_profiles%profiles_1d(time_index)%ion(ispec)%multiple_states_flag == 0) then

          countspec = countspec + 1

          internal_profs%ion(countspec)%ion_index = ispec
          internal_profs%ion(countspec)%impurity_index = 0
          internal_profs%ion(countspec)%is_fast_population = .false.

          internal_profs%ion(countspec)%mass = core_profiles%profiles_1d(time_index)%ion(ispec)%element(1)%a
          internal_profs%ion(countspec)%charge_nuclear = core_profiles%profiles_1d(time_index)%ion(ispec)%element(1)%z_n
          internal_profs%ion(countspec)%charge = core_profiles%profiles_1d(time_index)%ion(ispec)%z_ion
          internal_profs%ion(countspec)%density_on_axis = core_profiles%profiles_1d(time_index)%ion(ispec)%density(1)

          call set_splinecurve(internal_profs%s, &
               core_profiles%profiles_1d(time_index)%ion(ispec)%density, &
               internal_profs%ion(countspec)%density, return_status)
          if (return_status /= 0) then
             write(*,*)'ERROR in LION_profile_tools_IMAS.f90::init_internal_profs'
             write(*,*)'Failed to generate spline curve for internal_profs%ion(countspec)%density'
          end if

          call set_splinecurve(internal_profs%s, &
               core_profiles%profiles_1d(time_index)%ion(ispec)%temperature, &
               internal_profs%ion(countspec)%temperature_parallel, return_status)
          if (return_status /= 0) then
             write(*,*)'ERROR in LION_profile_tools_IMAS.f90::init_internal_profs'
             write(*,*)'Failed to generate spline curve for internal_profs%ion(countspec)%temperature_parallel'
             stop
          end if

          call set_splinecurve(internal_profs%s, &
               core_profiles%profiles_1d(time_index)%ion(ispec)%temperature, &
               internal_profs%ion(countspec)%temperature_perpendicular, return_status)
          if (return_status /= 0) then
             write(*,*)'ERROR in LION_profile_tools_IMAS.f90::init_internal_profs'
             write(*,*)'Failed to generate spline curve for internal_profs%ion(countspec)%temperature_perpendicular'
             stop
          end if
       end if IF_SINGLE_STATE

    end do LOOP_IONS

    call update_mass_density(internal_profs,return_status)
    if (return_status /= 0) then
       write(*,*)'in init_internal_profs, error recieved from update_mass_density'
       return
    end if
    call update_density_fractions(internal_profs,return_status)
    if (return_status /= 0) then
       write(*,*)'in init_internal_profs(ITM), error recieved from update_density_fractions'
       return
    end if

  end subroutine init_internal_profs


!!!!!!!!!!! ONLY FOR TESTING !!!!!!!!!!!!!!!!


  subroutine test_species_array(ion)
    use ids_schemas, only: ids_core_profile_ions
    type(ids_core_profile_ions), pointer, intent(in) :: ion(:)
    integer, allocatable :: ion_index(:)
    logical, allocatable :: is_fast(:)

    call get_species_array(ion, ion_index, is_fast)

    write(*,*)'ion_index=',ion_index
    write(*,*)'is_fast=',is_fast
    deallocate(ion_index)
    deallocate(is_fast)
  end subroutine test_species_array

  subroutine get_species_array(ion, ion_index, is_fast)
    use ids_schemas, only: ids_core_profile_ions
    type(ids_core_profile_ions), pointer, intent(in) :: ion(:)
    integer, allocatable :: ion_index(:)
    logical, allocatable :: is_fast(:)

    ! Internal
    integer :: jion

    if (.not. associated(ion)) return
    do jion=1,size(ion)
       if (valid_thermal(ion(jion))) then
          call append_integer( ion_index , jion )
          call append_logical( is_fast , .false. )
       end if
       if (valid_fast(ion(jion))) then
          call append_integer( ion_index , jion )
          call append_logical( is_fast , .true. )
       end if
    end do

  end subroutine get_species_array

  logical function valid_thermal(ion)
    use ids_schemas, only: ids_core_profile_ions
    type(ids_core_profile_ions), intent(in) :: ion
    valid_thermal = .true.
  end function valid_thermal

  logical function valid_fast(ion)
    use ids_schemas, only: ids_core_profile_ions
    type(ids_core_profile_ions), intent(in) :: ion
    valid_fast = .true.
  end function valid_fast

  subroutine append_integer(arr,val)
    integer, allocatable, intent(inout) :: arr(:)
    integer, intent(in) :: val
    ! Internal
    integer, allocatable :: tmp(:)

    if (allocated(arr)) then
       allocate(tmp(size(arr)))
       tmp = arr
       deallocate(arr)
       allocate(arr(size(tmp)+1))
       arr(1:size(tmp)) = tmp
       arr(size(arr)) = val
    else
       allocate(arr(1))
       arr = val
    end if
  end subroutine append_integer

  subroutine append_logical(arr,val)
    logical, allocatable, intent(inout) :: arr(:)
    logical, intent(in) :: val
    ! Internal
    logical, allocatable :: tmp(:)

    if (allocated(arr)) then
       allocate(tmp(size(arr)))
       tmp = arr
       deallocate(arr)
       allocate(arr(size(tmp)+1))
       arr(1:size(tmp)) = tmp
       arr(size(arr)) = val
    else
       allocate(arr(1))
       arr = val
    end if
  end subroutine append_logical

end module LION_profile_tools_IMAS
