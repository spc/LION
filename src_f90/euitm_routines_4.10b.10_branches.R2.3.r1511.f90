
module euITM_routines

use euITM_schemas
 
use topinfo_euitm_module
use amns_euitm_module
use antennas_euitm_module
use bb_shield_euitm_module
use compositionc_euitm_module
use coredelta_euitm_module
use corefast_euitm_module
use coreneutrals_euitm_module
use coreimpur_euitm_module
use coreprof_euitm_module
use coresource_euitm_module
use coretransp_euitm_module
use cxdiag_euitm_module
use distribution_euitm_module
use distsource_euitm_module
use ecediag_euitm_module
use edge_euitm_module
use efcc_euitm_module
use equilibrium_euitm_module
use fusiondiag_euitm_module
use halphadiag_euitm_module
use heat_sources_euitm_module
use interfdiag_euitm_module
use ironmodel_euitm_module
use langmuirdiag_euitm_module
use launchs_euitm_module
use lithiumdiag_euitm_module
use mhd_euitm_module
use magdiag_euitm_module
use msediag_euitm_module
use nbi_euitm_module
use ntm_euitm_module
use neoclassic_euitm_module
use orbit_euitm_module
use pellets_euitm_module
use pfsystems_euitm_module
use polardiag_euitm_module
use power_conv_euitm_module
use reflectomet_euitm_module
use rfadiag_euitm_module
use sawteeth_euitm_module
use scenario_euitm_module
use solcurdiag_euitm_module
use temporary_euitm_module
use toroidfield_euitm_module
use tsdiag_euitm_module
use turbulence_euitm_module
use wall_euitm_module
use waves_euitm_module

contains

subroutine euitm_get_times(idx,path,time)
implicit none

integer :: idx, status
character*(*) :: path
real(EUITM_R8), pointer :: time(:)

integer :: ndims,dim1,dim2,dim3,dim4,dum1,dum2,dum3,dum4, dim5, dim6, dim7, lentime

call get_dimension(idx,path,"time",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
lentime = dim1

call begin_cpo_get(idx, path,1,dum1)

allocate(time(lentime))
call get_vect1d_double(idx,path,"time",time,lentime,dum1,status)

call end_cpo_get(idx, path)

end subroutine
end module
