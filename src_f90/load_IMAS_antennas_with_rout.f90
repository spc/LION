subroutine load_itm_antennas(antennas_out,kitmopt,kitmshot,kitmrun,citmtree)
  !
  use globals ! contains ids_schemas
  use ids_routines
  IMPLICIT NONE
  type(ids_ic_antennas)      :: antennas_out
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun
  integer        :: idx, i
  !
  character*11  :: signal_name ='ic_antennas'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  print *,'signal_name= ',signal_name
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun= ',kitmshot,kitmrun
  call imas_open(citmtree,kitmshot,kitmrun,idx)
  call ids_get(idx,signal_name,antennas_out)

  if (associated(antennas_out%ids_properties%comment)) then
    do i=1,size(antennas_out%ids_properties%comment)
      write(6,'(A)') trim(antennas_out%ids_properties%comment(i))
    end do
  end if
    
  if (antennas_out%ids_properties%homogeneous_time .eq. 1) then
    print *,' antennas_out%time(1)= ',antennas_out%time(1)
  else
    print *,' antennas_out%antenna(1)%power%time(1)= ',antennas_out%antenna(1)%power_launched%time(1)
    print *,' antennas_out%antenna(1)%power%data(1)= ',antennas_out%antenna(1)%power_launched%data(1)
  end if
  return
end subroutine load_itm_antennas
