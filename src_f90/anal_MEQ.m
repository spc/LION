
npsi1=129;
nchi=100;
neq=29;
% EQ00 = textread('MEQ_out_ascii_ITM','%f');
fid=fopen('MEQ_out_ascii_ITM','r');
for ipsi=1:npsi1
  for jchi=1:nchi
    EQ2(1:neq,jchi,ipsi) = fscanf(fid,'%f',neq);
    %     EQ2(1:neq,jchi,ipsi) = EQ00((ipsi-1)*neq*nchi+(jchi-1)*neq+[1:neq]);
  end
end

fid=fopen('MEQ_out_midmesh','r');
for ipsi=1:npsi1
  for jchi=1:nchi
    EQ(1:neq,jchi,ipsi) = fscanf(fid,'%f',neq);
    %     EQ(1:neq,jchi,ipsi) = EQ00((ipsi-1)*neq*nchi+(jchi-1)*neq+[1:neq]);
  end
end

ss2(1:npsi1)=EQ2(1,1,:);
imidmesh=1;
if imidmesh
  ss(1:npsi1)=EQ(5,1,:);
else
  ss(1:npsi1)=EQ(1,1,:);
end
for ieq=1:neq
  figure(ieq);clf
  aa2(1:nchi,1:npsi1)=EQ2(ieq,:,:);
  aa(1:nchi,1:npsi1)=EQ(ieq,:,:);
  plot(ss2,aa2','b')
  hold on
  plot(ss,aa','r--')
  %  pause
end
