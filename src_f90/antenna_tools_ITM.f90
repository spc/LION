!> antennas_tools - Routines to map the CPO antennas to antenna parameterizations
!>
!> \author Thomas Johnson
!> \date   20130302
!
! Internal routines:
!
! set_global_antenna_param: Fills the antenna parameters in LION_common
!                   To map detailed antenna geometry in CPO to the simpler 
!                   geometry used in LION, the routine "antenna_position" is used.
!
! antenna_position: Simplified antenna parameterization. The distances between the
!                   magnetic axis and the antenna/wall are both calculated at 
!                   the minimum axis-antennas distance (considering points on each
!                   of the antenna-straps.)
!
!---------------------------------------------------------------------
module antenna_tools

  use prec_const, only: rkind, pi
  use euitm_schemas, only: type_equilibrium, type_antennas, type_antenna_ic

  implicit none

  real(rkind), parameter :: MIN_WALL_ANT_DISTANCE_NORM = 0.02_RKIND

contains

  !---------------------------------------------------------------------
  !> set_global_antenna_param
  !>
  !> Fills the antenna parameters in LION_common
  !> To map detailed antenna geometry in CPO to the simpler 
  !> geometry used in LION, the routine "antenna_position" is used.
  !>
  !> 
  !> 
  !---------------------------------------------------------------------
  subroutine set_global_antenna_param(antenna_ic,equilibrium,WNTOR,FEEDER_TILT,ierror)

    use itm_types, only: itm_is_valid
    use globals, only: NANTYP, CURSYM, THANT, NANTSHEET, ANTRAD, ANTRADMAX, WALRAD, &
         FREQCY, DELTAF, NRUN, WNTORO, WNTDEL, NTORSP, &
         NVERBOSE

    ! Input/Output
    type(type_equilibrium), intent(in) :: equilibrium !< Equilibrium-CPO
    type(type_antenna_ic),  intent(in) :: antenna_ic  !< IC-structure from the antennas-CPO
    real(rkind), intent(in) :: WNTOR            !< Toroidal mode number
    real(rkind), intent(in) :: FEEDER_TILT !< The tilt of the feeder given as the difference
                                           !< in poloidal (in degrees) where the feeder join 
                                           !< the strap and where it joins the wall.
    integer, intent(out) :: ierror         !< Error flag. For no error: ierror==0

    ! Local
    real(rkind) :: distance_antenna_axis, distance_wall_axis
    real(rkind) :: theta1_antenna
    real(rkind) :: theta2_antenna
    real(rkind) :: aminor
    integer :: jstrap
    integer :: jant
    integer :: count


    if (.not. itm_is_valid(antenna_ic%frequency%value)) then
       write(0,*)'ERROR in set_global_antenna_param' 
       write(0,*)'itm_is_valid(antenna_ic%frequency%value) -> .FALSE.'
       ierror = -1
       return
    endif
    if (.not. associated(antenna_ic%setup%straps)) then
       write(0,*)'ERROR in set_global_antenna_param'
       write(0,*)'antenna_ic%setup%straps not associated'
       ierror = -2
       return
    endif
    do jstrap=1,size(antenna_ic%setup%straps)
       if (.not. associated(antenna_ic%setup%straps(jstrap)%coord_strap%r)) then
          write(0,*)'ERROR in set_global_antenna_param'
          write(0,*)'antenna_ic%setup%straps(jstrap)%coord_strap%r not associated'
          write(0,*)'for jstrap=',jstrap
          ierror = -3
          return
       endif
       if (.not. associated(antenna_ic%setup%straps(jstrap)%coord_strap%z)) then
          write(0,*)'ERROR in set_global_antenna_param'
          write(0,*)'antenna_ic%setup%straps(jstrap)%coord_strap%z not associated'
          write(0,*)'for jstrap=',jstrap
          ierror = -4
          return
       endif
       if (.not. itm_is_valid(antenna_ic%setup%straps(jstrap)%dist2wall)) then
          write(0,*)'ERROR in set_global_antenna_param'
          write(0,*)'itm_is_valid(antenna_ic%setup%straps(jstrap)%dist2wall) -> .FALSE.'
          write(0,*)'for jstrap=',jstrap
          ierror = -5
          return
       endif
    enddo

    if (.not. associated(equilibrium%profiles_1d%r_outboard)) then
       write(0,*)'ERROR in set_global_antenna_param'
       write(0,*)'equilibrium%profiles_1d%r_outboard not associated'
       ierror = -6
       return
    endif
    if (.not. itm_is_valid(equilibrium%global_param%mag_axis%position%r)) then
       write(0,*)'ERROR in set_global_antenna_param'
       write(0,*)'equilibrium%global_param%mag_axis%position%r is invalid'
       ierror = -7
       return
    endif
    if (.not. itm_is_valid(equilibrium%global_param%mag_axis%position%z)) then
       write(0,*)'ERROR in set_global_antenna_param'
       write(0,*)'equilibrium%global_param%mag_axis%position%z is invalid'
       ierror = -8
       return
    endif
    ierror = 0

    call antenna_position(antenna_ic,equilibrium,&
       distance_antenna_axis, distance_wall_axis, &
       theta1_antenna, theta2_antenna, aminor)

    if (NVERBOSE > 2) then
       write(*,*)'from antenna_position:'
       write(*,*)'-distance_antenna_axis=',distance_antenna_axis
       write(*,*)'-distance_wall_axis=',distance_wall_axis
       write(*,*)'-theta1_antenna=',theta1_antenna
       write(*,*)'-theta2_antenna=',theta2_antenna
       write(*,*)'-aminor=' ,aminor
    endif

  !     NANTYP = 2 ====> LFS OR HFS ANTENNA. SPECIFIED BY THE INPUT
  !                      PARAMETERS THANT(J), J=1,4 AND CURSYM(1).
  !
  !                      THANT(J) ARE ANGLES GIVEN IN DEGREES, WITH VALUES
  !                      BETWEEN 0 AND 360. THANT(J) ARE MEASURED FROM THE
  !                      MAGNETIC AXIS HORIZONTAL.
  !                      THE LFS OR HFS ANTENNA IS A CURRENT SHEET WHICH,
  !                      BETWEEN THETA = THANT(2) AND THANT(3), IS AT A
  !                      CONSTANT DISTANCE OF THE PLASMA SURFACE AND
  !                      CARRIES CONSTANT PURE POLOIDAL CURRENTS :
  !
  !                         SAUTR(THETA) = CURSYM(1)
  !
  !                      BETWEEN THETA = THANT(1) AND THETA = THANT(2)
  !                      AND THETA = THANT(3) AND THETA = THANT(4) ARE
  !                      THE FEEDERS, WHERE THE DISTANCE FROM THE
  !                      PLASMA SURFACE INCREASES SMOOTHLY UP TO THE
  !                      WALL SURFACE.
  !
  !                      THE LFS ANTENNA EXTENDS ACROSS THE THETA=0
  !                      LINE. THEREFORE THANT(3) < THANT(4) < THANT(1)
  !                      < THANT(2).
  !                      THE HFS ANTENNA CANNOT CROSS THE THETA=0
  !                      LINE. THEREFORE THANT(1) < THANT(2) < THANT(3)
  !                      < THANT(4).
  !
  !                      THE SELECTION OF EITHER LFS OR HFS ANTENNA
  !                      AUTOMATIC :
  !
  !                            THANT(3).LT.THANT(2) SELECTS LFS ANTENNA
  !                            THANT(2).GT.THANT(3) SELECTS HFS ANTENNA
  !
  !                      NOTE THAT WE MUST HAVE THANT(1) < THANT(2)
  !                      AND THANT(3) < THANT(4).
    NANTYP = 2
    CURSYM(1) = 1.0_RKIND
    THANT(1) = canonicalangle( theta1_antenna - FEEDER_TILT ,  360.0_RKIND )
    THANT(2) = canonicalangle( theta1_antenna               ,  360.0_RKIND )
    THANT(3) = canonicalangle( theta2_antenna               ,  360.0_RKIND )
    THANT(4) = canonicalangle( theta2_antenna + FEEDER_TILT ,  360.0_RKIND )

  !
  !     'NANTSHEET' Number of antenna current sheets. For NANTSHEET>1, 
  !                 the "power at antenna" might be wrong ...
  !                 and hopefully the "power at plasma surface" is right.
  !                 The current sheets are placed equidistantly between 
  !                 ANTRAD and ANTRADMAX. The current distribution as fct 
  !                 of theta is identical for all sheets.
    NANTSHEET = 1
  !
  !     'ANTRAD' DISTANCE ANTENNA-MAGNETIC AXIS IN UNITS OF THE MINOR 
  !              RADIUS IN THE Z=0 PLANE, I.E. (R_ANT-R_AXIS)/AMINOR.
    ANTRAD = distance_antenna_axis / aminor
  !
  !     'ANTRADMAX' Max distance antenna-magnetic axis
    ANTRADMAX = ANTRAD
  !
  !     'WALRAD' DISTANCE WALL-MAGNETIC AXIS IN UNITS OF THE MINOR
  !              RADIUS IN THE Z=0 PLANE. 
    WALRAD = distance_wall_axis / aminor
    if ( WALRAD < ANTRAD + MIN_WALL_ANT_DISTANCE_NORM ) then
       write(0,*)'WARNING in set_global_antenna_param'
       write(0,*)'  Too small value: WALRAD < ANTRAD + MIN_WALL_ANT_DISTANCE_NORM'
       write(0,*)'  WALRAD=',WALRAD
       write(0,*)'  ANTRAD=',ANTRAD 
       write(0,*)'  MIN_WALL_ANT_DISTANCE_NORM=',MIN_WALL_ANT_DISTANCE_NORM
       write(0,*)'  jstrap=',jstrap
       write(0,*)'  Setting: WALRAD=',ANTRAD + MIN_WALL_ANT_DISTANCE_NORM
       WALRAD = ANTRAD + MIN_WALL_ANT_DISTANCE_NORM
       ierror = 1   ! Non-fatal error
    endif

  !
  !     'FREQCY' IS THE GENERATOR FREQUENCY IN HZ.
    FREQCY = antenna_ic%frequency%value
  !
  !     'DELTAF' IS THE FREQUENCY INCREMENT FOR FREQUENCY TRACES.
    DELTAF = 0_RKIND
  !
  !     'NRUN'   IS THE NUMBER OF RUNS FOR FREQUENCY TRACES.
    NRUN = 1
  !
  !     'WNTORO' IS THE TOROIDAL WAVE NUMBER.
    WNTORO = WNTOR
  !
  !     'WNTDEL' IS THE TOROIDAL WAVENUMBER INCREMENT FOR TOROIDAL WN SCANS
    WNTDEL = 0
  !
  !     'NTORSP' IS THE NUMBER OF TOROIDAL WN'S FOR TOROIDAL WN SCANS
    NTORSP = 1

  end subroutine set_global_antenna_param


  !---------------------------------------------------------------------
  !> antenna_position: 
  !>
  !> Simplified antenna parameterization. The distances between the
  !> magnetic axis and the antenna/wall are both calculated at 
  !> the minimum axis-antennas distance (considering points on each
  !> of the antenna-straps).
  !> Also returns the minor radius \c aminor.
  !---------------------------------------------------------------------
  subroutine antenna_position(antenna_ic,equilibrium,&
       distance_antenna_axis,distance_wall_axis,theta1,theta2,aminor)

    use prec_const

    ! Input/Output
    type(type_equilibrium), intent(in) :: equilibrium !< Equilibrium-CPO
    type(type_antenna_ic),  intent(in) :: antenna_ic  !< IC-structure from the antennas-CPO
    real(rkind), intent(out) :: distance_antenna_axis !< Minimum distance between the magnetic axis and the antenna
    real(rkind), intent(out) :: distance_wall_axis    !< Sum of \c distance_antenna_axis and antenna-wall distance
    real(rkind), intent(out) :: theta1  !< Geometrical poloidal angle at the bottom corner of the antenna
    real(rkind), intent(out) :: theta2  !< Geometrical poloidal angle at the top corner of the antenna
    real(rkind), intent(out) :: aminor  !< Minor radius

    ! Local variables
    real(rkind) :: Raxis   ! Major radius at the magnetic axis
    real(rkind) :: Zaxis   ! Vertical coordinate at the magnetic axis
    real(rkind) :: dist
    real(rkind), allocatable :: angle(:)
    real(rkind) :: central_angle
    real(rkind) :: theta1sum, theta2sum

    real(RKIND) :: direction_sign
    real(rkind) :: sum_dist2wall
    integer :: jstrap=1 ! Antenna current strap index
    integer :: j        ! Dummy for loops

    ! Geometry parameters for the plasma
    Raxis = equilibrium%global_param%mag_axis%position%r
    Zaxis = equilibrium%global_param%mag_axis%position%z
    aminor = equilibrium%profiles_1d%r_outboard( &
         size(equilibrium%profiles_1d%r_outboard) ) - Raxis

    ! Search through all grid-coordinates for the minimum antenna-axis distance
    sum_dist2wall=0_rkind
    do jstrap=1,size(antenna_ic%setup%straps)

       sum_dist2wall = sum_dist2wall + antenna_ic%setup%straps(jstrap)%dist2wall

       do j=1,size(antenna_ic%setup%straps(jstrap)%coord_strap%r)

          dist = sqrt( (antenna_ic%setup%straps(jstrap)%coord_strap%r(j) - Raxis)**2 &
               +       (antenna_ic%setup%straps(jstrap)%coord_strap%z(j) - Zaxis)**2 )
          
          if (j==1) then
             distance_antenna_axis = dist
          else
             if (dist < distance_antenna_axis) then
                distance_antenna_axis = dist
             endif
          endif

       enddo
    enddo

    distance_wall_axis = distance_antenna_axis + &
         sum_dist2wall / dble(size(antenna_ic%setup%straps))

    ! Evaluate the poloidal range of the antenna as a mean over the straps
    theta1sum = 0.0_RKIND
    theta2sum = 0.0_RKIND
    do jstrap=1,size(antenna_ic%setup%straps)
       allocate(angle(size(antenna_ic%setup%straps(jstrap)%coord_strap%r)))

       angle(:) = atan2( &
            (antenna_ic%setup%straps(jstrap)%coord_strap%z(:)-Zaxis), &
            (antenna_ic%setup%straps(jstrap)%coord_strap%r(:)-Raxis) )

       ! To avoid problems with the jumpin theta at -PI and +PI:
       ! Map the angles to be in the range ( central_angle-PI , central_angle+PI )
       central_angle = angle( int( dble(size(angle))/2.0 ) )
       angle = canonicalangle_vec( angle - central_angle + PI, 2.0_RKIND * PI) + central_angle - PI

       theta1sum = theta1sum + minval(angle) * (180.0_RKIND / PI)
       theta2sum = theta2sum + maxval(angle) * (180.0_RKIND / PI)

       deallocate(angle)
    enddo

    theta1 = theta1sum / dble(size(antenna_ic%setup%straps))
    theta2 = theta2sum / dble(size(antenna_ic%setup%straps))

  end subroutine antenna_position


  function canonicalangle(in,periodicity) result(out)
    real(RKIND) :: in
    real(RKIND) :: periodicity
    real(RKIND) :: out

    out=in
    do while (out > periodicity)
       out = out - periodicity
    enddo
    do while (out < 0.0_RKIND)
       out = out + periodicity
    enddo
  end function canonicalangle


  function canonicalangle_vec(in,periodicity) result(out)
    real(RKIND) :: in(:)
    real(RKIND) :: periodicity
    real(RKIND) :: out(size(in))
    integer :: j

    out=in
    do j=1,size(in)
       do while (out(j) > periodicity)
          out(j) = out(j) - periodicity
       enddo
       do while (out(j) < 0.0_RKIND)
          out(j) = out(j) + periodicity
       enddo
    enddo
  end function canonicalangle_vec


  subroutine print_commonblock_antenna(iounit)

    use globals
    integer, intent(in) :: iounit
    write(iounit,*)'NANTYP   =',NANTYP
    write(iounit,*)'CURSYM   =',CURSYM
    write(iounit,*)'THANT    =',THANT
    write(iounit,*)'NANTSHEET=',NANTSHEET
    write(iounit,*)'ANTRAD   =',ANTRAD
    write(iounit,*)'ANTRADMAX=',ANTRADMAX
    write(iounit,*)'WALRAD   =',WALRAD
    write(iounit,*)'FREQCY   =',FREQCY
    write(iounit,*)'DELTAF   =',DELTAF
    write(iounit,*)'NRUN     =',NRUN
    write(iounit,*)'WNTORO   =',WNTORO
    write(iounit,*)'WNTDEL   =',WNTDEL
    write(iounit,*)'NTORSP   =',NTORSP
  end subroutine print_commonblock_antenna

end module antenna_tools

