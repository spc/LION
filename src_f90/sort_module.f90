module sort_module

  use globals, only: RKIND

  implicit none

contains

   subroutine simple_sort(arr,ind)
     real(RKIND), intent(in) :: arr(:)
     integer, intent(out) :: ind(size(arr))
     ! Internal
     integer :: ndim
     integer :: jstart, j, k
     integer :: tmp(size(arr))

     ndim = size(arr)
     ind(1) = 1
     TO_ADD: do j=2,ndim
        COMPARE: do k=1,j-1
           if (arr(j) < arr(k)) then
              tmp(k:j-1) = ind(k:j-1)
              ind(k+1:j) = tmp(k:j-1)
              ind(k) = j
              exit COMPARE
           end if
           if (k==j-1) then
              ind(j)=j
           end if
        end do COMPARE
     end do TO_ADD

   end subroutine
end module sort_module
