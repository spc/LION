SUBROUTINE equil_metric_derivatives(equil_in,in_time_slice,d_ddim1,d_ddim2,d_dsqrtdim1norm,error_flag,output_message)
  !
  ! Computes d_metrics_d_dim1/dim2/sqrt(dim1/dim1(end)) from equil_in(i).coord_sys for each i equilibrium input
  ! Thus if dim1, dim2 are psi, chi. These give derivatives vs psi_at_fixed_chi, chi_at_fixed_psi, rhopol_norm_at_fixed_chi
  !
  ! Note: If you need the straight-field line coordinate "thetastar" you can do:
  ! straight-field line coordinate thetastar = 1/q int(0,chi) [F_dia Jacobian_psichiphi/R^2 dchi]
  ! chi being dim2
  !! allocate(thetastraight(ndim1(ieq),ndim2(ieq))
  !! do idim1=1,ndim1(ieq)
  !!   call interpos(equil_in(ieq)%coord_sys%grid%dim2, &
  !!     & equil_in(ieq)%profiles_1d%F_dia *equil_in(ieq)%coord_sys%jacobian(idim1,:)/equil_in(ieq)%coord_sys%position%r(idim1,:)**2, &
  !!     & ndim2(ieq),tension=tens_def, youtint=thetastraight(:,idim2),nbc=(/-1, -1/),ybc=(/ twopi, twopi /))
  !!   thetastraight(:,idim2) = thetastraight(:,idim2) / equil_in(ieq)%profiles_1d%q
  !! end do
  !
  use ids_schemas, only: R8=>ids_real, ids_equilibrium, ids_equilibrium_coordinate_system
  USE interpos_module
  use cocos_module
  use sort_module, only: simple_sort
  !
  IMPLICIT NONE
  !
  REAL(R8), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_R8
  type(ids_equilibrium), intent(in)                    :: equil_in
  type(ids_equilibrium_coordinate_system), intent(out) :: d_ddim1, d_ddim2, d_dsqrtdim1norm
  integer, intent(in)                                  :: in_time_slice
  integer, intent(out)                                 :: error_flag
  character(len=:), allocatable,           intent(out) :: output_message
  !
  real(R8) :: tens_def, tens_def2, deltapsi, zsign_psi
  real(R8), allocatable :: sqrtdim1norm(:)
  integer :: nbequil, ieq, ilen, idim1, idim2
  integer :: cocos_in, iexp_Bp, isigma_Bp, isigma_RphiZ, isigma_rhothetaphi, isign_q_pos, isign_pprime_pos
  integer :: ndim1, ndim2
  integer, allocatable :: ind_sort(:)
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  error_flag=0
  if (.not. associated(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1)) then
     output_message = 'ERROR in equil_metric_derivatices.f90: '// &
          '(.not. asociated(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1))'
     write(*,*)output_message
     error_flag=-1
  endif
  if (.not. associated(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2)) then
     output_message = 'ERROR in equil_metric_derivatices.f90: '// &
          '(.not. asociated(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2))'
     write(*,*)output_message
     error_flag=-2
  endif
  !
  tens_def = -0.1_R8
  ! tens_def2 to get stronger smoothing. Needed for g12, but may be others
  tens_def2 = -3.0_R8
  ndim1 = size(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1)
  ndim2 = size(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2)
  !
  ! Could assume cocos convention same for each equilibrium index
  cocos_in = 11
  CALL COCOS(COCOS_IN,iexp_Bp,isigma_Bp,isigma_RphiZ,isigma_rhothetaphi,isign_q_pos,isign_pprime_pos)
  ! To have increasing psi (dim1) we need: d|psi-psi_axis| = sigma_Ip sigma_Bp dpsi
  zsign_psi = real(isigma_Bp * sign(1._R8,equil_in%time_slice(in_time_slice)%global_quantities%ip),R8)
  !
  ilen = size(equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%description)
  allocate(d_ddim1%grid_type%description(ilen))
  allocate(d_ddim2%grid_type%description(ilen))
  allocate(d_dsqrtdim1norm%grid_type%description(ilen))
  d_ddim1%grid_type%description = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%description
  ilen = size(equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%name)
  allocate(d_ddim1%grid_type%name(ilen))
  allocate(d_ddim2%grid_type%name(ilen))
  allocate(d_dsqrtdim1norm%grid_type%name(ilen))
  d_ddim1%grid_type%name = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%name
  d_ddim1%grid_type%index = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%index
  d_ddim1%grid = equil_in%time_slice(in_time_slice)%coordinate_system%grid
  !
  d_ddim2%grid_type%description = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%description
  d_ddim2%grid_type%name = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%name
  d_ddim2%grid_type%index = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%index
  d_ddim2%grid = equil_in%time_slice(in_time_slice)%coordinate_system%grid
  !
  d_dsqrtdim1norm%grid_type%description = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%description
  d_dsqrtdim1norm%grid_type%name = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%name
  d_dsqrtdim1norm%grid_type%index = equil_in%time_slice(in_time_slice)%coordinate_system%grid_type%index
  d_dsqrtdim1norm%grid = equil_in%time_slice(in_time_slice)%coordinate_system%grid
  if (allocated(sqrtdim1norm)) deallocate(sqrtdim1norm)
  allocate(sqrtdim1norm(ndim1))
  deltapsi =  equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ndim1)&
    & - equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(1)
  sqrtdim1norm(1:ndim1) = sqrt(( equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(1:ndim1)&
    & - equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(1))/deltapsi)
  !
  ! d/d_dim1 at fixed dim2. Assumes dim1 is psi like (thus rho^2 and at magnetic axis for dim1(1)
  ! can check if dim1(1) is small to see if dim1 is R, but should use grid_type (normally grid_type(2) integer value...)
  ! At this stage, assumes dim1 is psi as standard from CHEASE output
  !
  ! d/d_dim1:
  ! for those leading to infinite values with d/dim1 and not with d/sqrt(dim1), do on the latter, than extrapolate for central value
  ! this is for R, Z, J, g_12 and g_33 as far as I know
  allocate(d_ddim1%jacobian(ndim1,ndim2))
  allocate(d_ddim1%tensor_contravariant(ndim1,ndim2,3,3))
  allocate(d_ddim1%r(ndim1,ndim2))
  allocate(d_ddim1%z(ndim1,ndim2))
  !
  do idim2=1,ndim2
    ! Jacobian
     !if (.not. associated(equil_in%time_slice(in_time_slice)%coordinate_system%jacobian)) stop 'Abort. coord-sys/jacobian not associated'
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(1:ndim1,idim2),ndim1,tension=tens_def, &
      & youtp=d_ddim1%jacobian(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(ndim1,idim2)/))
    d_ddim1%jacobian(2:ndim1,idim2) = d_ddim1%jacobian(2:ndim1,idim2) / 2._R8 / sqrtdim1norm(2:ndim1) / deltapsi
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(2:ndim1,idim2),n=ndim1-1, &
      & xscal=sqrtdim1norm(1),tension=tens_def,yscal=d_ddim1%jacobian(1,idim2),nbcscal=(/2,2/), &
      & ybcscal=(/d_ddim1%jacobian(2,idim2), d_ddim1%jacobian(ndim1,idim2)/))
    ! g_ij, multiply psi and function by zsign_psi to have same derivative, when grid%dim1 is used
    !
    allocate( ind_sort(ndim1) )
    call simple_sort(zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1,ind_sort)
    !
    call interpos(zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort), &
      & zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort,idim2,1,1), &
      & ndim1,tension=tens_def,youtp=d_ddim1%tensor_contravariant(ind_sort,idim2,1,1),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(1),idim2,1,1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(ndim1),idim2,1,1)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,1,2),ndim1, &
      & tension=tens_def2,youtp=d_ddim1%tensor_contravariant(1:ndim1,idim2,1,2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,1,2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,1,2)/))
    d_ddim1%tensor_contravariant(2:ndim1,idim2,1,2) = d_ddim1%tensor_contravariant(2:ndim1,idim2,1,2) / 2._R8 / sqrtdim1norm(2:ndim1) / deltapsi
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,1,2),n=ndim1-1, &
      & xscal=sqrtdim1norm(1),tension=tens_def,yscal=d_ddim1%tensor_contravariant(1,idim2,1,2),nbcscal=(/2,2/), &
      & ybcscal=(/d_ddim1%tensor_contravariant(2,idim2,1,2), d_ddim1%tensor_contravariant(ndim1,idim2,1,2)/))

    call interpos(zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort(1:ndim1)), &
      & zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(1:ndim1),idim2,1,3), &
      & ndim1,tension=tens_def,youtp=d_ddim1%tensor_contravariant(ind_sort(1:ndim1),idim2,1,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(1),idim2,1,3), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(ndim1),idim2,1,3)/))
    ! Assume g_22=g_22tild/dim1^2 (if dim1 is psi, could put dim^1, but use dim1^2 to work even if dim1 is rho-like) to construct derivative better close to axis
    call interpos(zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort(2:ndim1)), &
      & zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(2:ndim1),idim2,2,2) &
      & * equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort(2:ndim1))**2, &
      & nin=ndim1-1, tension=tens_def, youtp=d_ddim1%tensor_contravariant(ind_sort(2:ndim1),idim2,2,2), nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(2),idim2,2,2) &
      & * equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort(2))**2, &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(ndim1),idim2,2,2) &
      & * equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort(ndim1))**2/))
    d_ddim1%tensor_contravariant(2:ndim1,idim2,2,2) = d_ddim1%tensor_contravariant(2:ndim1,idim2,2,2) &
      & / equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(2:ndim1)**2 &
      & - 2._R8 * equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,2,2) &
      & / equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(2:ndim1)
    call interpos(sqrtdim1norm(2:ndim1),log(sign(1._R8,d_ddim1%tensor_contravariant(2,idim2,2,2)) &
      & * d_ddim1%tensor_contravariant(2:ndim1,idim2,2,2)),N=ndim1-1,xscal=sqrtdim1norm(1),tension=tens_def, &
      & yscal=d_ddim1%tensor_contravariant(1,idim2,2,2),nbcscal=(/2,2/), &
      & ybcscal=(/log(sign(1._R8,d_ddim1%tensor_contravariant(2,idim2,2,2)) &
      & * d_ddim1%tensor_contravariant(2,idim2,2,2)), &
      &           log(sign(1._R8,d_ddim1%tensor_contravariant(2,idim2,2,2)) &
      & * d_ddim1%tensor_contravariant(ndim1,idim2,2,2)) /))
    d_ddim1%tensor_contravariant(1,idim2,2,2) =  &
      & sign(1._R8,d_ddim1%tensor_contravariant(2,idim2,2,2))*exp(d_ddim1%tensor_contravariant(1,idim2,2,2))
    !
    call interpos(zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim1(ind_sort), &
      & zsign_psi*equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(1:ndim1),idim2,2,3), &
      & ndim1,tension=tens_def,youtp=d_ddim1%tensor_contravariant(ind_sort(1:ndim1),idim2,2,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(1),idim2,2,3), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ind_sort(ndim1),idim2,2,3)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,3,3),ndim1, &
      & tension=tens_def,youtp=d_ddim1%tensor_contravariant(1:ndim1,idim2,3,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,3,3), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,3,3)/))
    d_ddim1%tensor_contravariant(2:ndim1,idim2,3,3) = &
      & d_ddim1%tensor_contravariant(2:ndim1,idim2,3,3) / 2._R8 / sqrtdim1norm(2:ndim1) / deltapsi
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,3,3),n=ndim1-1, &
      & xscal=sqrtdim1norm(1),tension=tens_def,yscal=d_ddim1%tensor_contravariant(1,idim2,3,3),nbcscal=(/2,2/), &
      & ybcscal=(/d_ddim1%tensor_contravariant(2,idim2,3,3), &
      &           d_ddim1%tensor_contravariant(ndim1,idim2,3,3)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2),ndim1,tension=tens_def, &
      & youtp=d_ddim1%r(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%r(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%r(ndim1,idim2)/))
    d_ddim1%r(2:ndim1,idim2) = d_ddim1%r(2:ndim1,idim2) / 2._R8 / sqrtdim1norm(2:ndim1) / deltapsi
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%r(2:ndim1,idim2),n=ndim1-1,xscal=sqrtdim1norm(1), &
      & tension=tens_def, yscal=d_ddim1%r(1,idim2),nbcscal=(/2,2/), &
      & ybcscal=(/d_ddim1%r(2,idim2), d_ddim1%r(ndim1,idim2)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%z(1:ndim1,idim2),ndim1,tension=tens_def, &
      & youtp=d_ddim1%z(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%z(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%z(ndim1,idim2)/))
    d_ddim1%z(2:ndim1,idim2) = d_ddim1%z(2:ndim1,idim2) / 2._R8 / sqrtdim1norm(2:ndim1) / deltapsi
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%z(2:ndim1,idim2),n=ndim1-1,xscal=sqrtdim1norm(1), &
      & tension=tens_def, yscal=d_ddim1%z(1,idim2),nbcscal=(/2,2/), &
      & ybcscal=(/d_ddim1%z(2,idim2), d_ddim1%z(ndim1,idim2)/))
    !
    deallocate( ind_sort )
  end do
  !
  ! d/d_sqrtdim1norm with sqrtdim1norm = sqrt((dim1-dim1(1))/(dim1(end)-dim1(1)))
  !
  allocate(d_dsqrtdim1norm%jacobian(ndim1,ndim2))
  allocate(d_dsqrtdim1norm%tensor_contravariant(ndim1,ndim2,3,3))
  allocate(d_dsqrtdim1norm%r(ndim1,ndim2))
  allocate(d_dsqrtdim1norm%z(ndim1,ndim2))
  do idim2=1,ndim2
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(1:ndim1,idim2),ndim1,tension=tens_def, &
      & youtp=d_dsqrtdim1norm%jacobian(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(ndim1,idim2)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,1,1),ndim1, &
      & tension=tens_def,youtp=d_dsqrtdim1norm%tensor_contravariant(1:ndim1,idim2,1,1),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,1,1), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,1,1)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,1,2),ndim1, &
      & tension=tens_def2,youtp=d_dsqrtdim1norm%tensor_contravariant(1:ndim1,idim2,1,2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,1,2), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,1,2)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,1,3),ndim1, &
      & tension=tens_def,youtp=d_dsqrtdim1norm%tensor_contravariant(1:ndim1,idim2,1,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,1,3), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,1,3)/))
    ! Assume g_22=g_22tild/sqrtdim1^2 to construct derivative better close to axis
    call interpos(sqrtdim1norm(2:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,2,2) &
      & * sqrtdim1norm(2:ndim1)**2,nin=ndim1-1,tension=tens_def, &
      & youtp=d_dsqrtdim1norm%tensor_contravariant(2:ndim1,idim2,2,2), nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2,idim2,2,2) &
      & * sqrtdim1norm(2)**2, &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,2,2) &
      & * sqrtdim1norm(ndim1)**2/))
    d_dsqrtdim1norm%tensor_contravariant(2:ndim1,idim2,2,2) = &
      & d_dsqrtdim1norm%tensor_contravariant(2:ndim1,idim2,2,2) / sqrtdim1norm(2:ndim1)**2 - &
      & 2._R8 * equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(2:ndim1,idim2,2,2) &
      &  / sqrtdim1norm(2:ndim1)
    call interpos(sqrtdim1norm(2:ndim1),log(abs(d_dsqrtdim1norm%tensor_contravariant(2:ndim1,idim2,2,2))), &
      & N=ndim1-1,xscal=sqrtdim1norm(1), &
      & yscal=d_dsqrtdim1norm%tensor_contravariant(1,idim2,2,2),tension=tens_def,nbcscal=(/2,2/), &
      & ybcscal=(/log(abs(d_dsqrtdim1norm%tensor_contravariant(2,idim2,2,2))), &
      &           log(abs(d_dsqrtdim1norm%tensor_contravariant(ndim1,idim2,2,2))) /))
    d_dsqrtdim1norm%tensor_contravariant(1,idim2,2,2) = &
      & sign(1._R8,d_dsqrtdim1norm%tensor_contravariant(2,idim2,2,2)) &
      & * exp(d_dsqrtdim1norm%tensor_contravariant(1,idim2,2,2))
    !
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,2,3),ndim1, &
      & tension=tens_def,youtp=d_dsqrtdim1norm%tensor_contravariant(1:ndim1,idim2,2,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,2,3), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,2,3)/))
    call interpos(sqrtdim1norm(1:ndim1), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1:ndim1,idim2,3,3),ndim1, &
      & tension=tens_def,youtp=d_dsqrtdim1norm%tensor_contravariant(1:ndim1,idim2,3,3),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(1,idim2,3,3), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(ndim1,idim2,3,3)/))
    call interpos(sqrtdim1norm(1:ndim1),equil_in%time_slice(in_time_slice)%coordinate_system%r(1:ndim1,idim2), &
      & ndim1,tension=tens_def,youtp=d_dsqrtdim1norm%r(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%r(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%r(ndim1,idim2)/))
    call interpos(sqrtdim1norm(1:ndim1),equil_in%time_slice(in_time_slice)%coordinate_system%z(1:ndim1,idim2), &
      & ndim1,tension=tens_def,youtp=d_dsqrtdim1norm%z(1:ndim1,idim2),nbc=(/2,2/), &
      & ybc=(/equil_in%time_slice(in_time_slice)%coordinate_system%z(1,idim2), &
      &       equil_in%time_slice(in_time_slice)%coordinate_system%z(ndim1,idim2)/))
  end do
  !
  ! d/d_dim2 at fixed dim2. Assumes dim1 is psi like (thus rho^2 and at magnetic axis for dim1(1)
  ! can check if dim1(1) is small to see if dim1 is R, but should use grid_type (normally grid_type(2) integer value...)
  ! At this stage, assumes dim1 is psi as standard from CHEASE output
  !
  ! d/d_dim2:
  allocate(d_ddim2%jacobian(ndim1,ndim2))
  allocate(d_ddim2%tensor_contravariant(ndim1,ndim2,3,3))
  allocate(d_ddim2%r(ndim1,ndim2))
  allocate(d_ddim2%z(ndim1,ndim2))
  !
  allocate( ind_sort(ndim2) )
  do idim1=1,ndim1
     call simple_sort(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(1:ndim2),ind_sort)
     !
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%jacobian(idim1,ind_sort),ndim2,tension=tens_def, &
      & youtp=d_ddim2%jacobian(idim1,ind_sort),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,1,1),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,1,1),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,1,2),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,1,2),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,1,3),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,1,3),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,2,2),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,2,2),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,2,3),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,2,3),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%tensor_contravariant(idim1,ind_sort,3,3),ndim2, &
      & tension=tens_def,youtp=d_ddim2%tensor_contravariant(idim1,ind_sort,3,3),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%r(idim1,ind_sort),ndim2,tension=tens_def, &
      & youtp=d_ddim2%r(idim1,ind_sort),nbc=-1,ybc=twopi)
    call interpos(equil_in%time_slice(in_time_slice)%coordinate_system%grid%dim2(ind_sort), &
      & equil_in%time_slice(in_time_slice)%coordinate_system%z(idim1,ind_sort),ndim2,tension=tens_def, &
      & youtp=d_ddim2%z(idim1,ind_sort),nbc=-1,ybc=twopi)
  end do
  deallocate( ind_sort )
  !
end SUBROUTINE equil_metric_derivatives
